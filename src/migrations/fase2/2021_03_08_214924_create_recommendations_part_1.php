<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRecommendationsPart1 extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('OADH_Institution', function (Blueprint $table) {
			$table->increments('id');
			$table->string('abbreviation', 20);
			$table->string('name');

			$table->unsignedInteger('organization_id')->index();
			
			$table->timestamps();
		});

		Schema::create('OADH_Right', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			
			$table->unsignedInteger('organization_id')->index();
			$table->timestamps();
		});

		Schema::create('OADH_Recommendation_Temp', function (Blueprint $table) {
			$table->increments('id');
			$table->string('system', 200)->nullable();
			$table->longText('recommendation')->nullable();
			$table->string('code', 100)->nullable();
			$table->date('date')->nullable();
			$table->string('mechanism')->nullable();
			$table->text('related_rights')->nullable();
			$table->text('responsible_institutions')->nullable();
			$table->text('source')->nullable();
			$table->longText('legal_base')->nullable();
			$table->longText('observations')->nullable();
			$table->date('last_modified_date')->nullable();
			$table->char('status', 1);
			
			// $table->text('treaty')->nullable();
			// $table->string('population', 100)->nullable();
			// $table->string('instance', 100)->nullable();
			// $table->string('reference')->nullable();
			// $table->longText('context')->nullable();
			
			//foreign Keys
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
			$table->unsignedInteger('organization_id')->index();
			
			$table->timestamps();
		});

		Schema::create('OADH_Recommendation', function (Blueprint $table) {
			$table->increments('id');
			$table->string('system', 200)->nullable();
			$table->longText('recommendation')->nullable();
			$table->string('code', 100)->nullable();
			$table->date('date')->nullable();
			$table->string('mechanism')->nullable();
			$table->text('related_rights')->nullable();
			$table->text('responsible_institutions')->nullable();
			$table->text('source')->nullable();
			$table->longText('legal_base')->nullable();
			$table->longText('observations')->nullable();
			$table->date('last_modified_date')->nullable();

			//foreign Keys
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
			$table->unsignedInteger('organization_id')->index();
			
			$table->timestamps();
		});

		Schema::create('OADH_Recommendation_Institution', function (Blueprint $table) {
			$table->increments('id');

			//foreign Keys
			$table->unsignedInteger('recommendation_id');
			$table->foreign('recommendation_id')->references('id')->on('OADH_Recommendation');
			$table->unsignedInteger('institution_id');
			$table->foreign('institution_id')->references('id')->on('OADH_Institution');
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
			$table->unsignedInteger('organization_id')->index();
			
			$table->timestamps();
		});

		Schema::create('OADH_Recommendation_Right', function (Blueprint $table) {
			$table->increments('id');
			
			//foreign Keys
			$table->unsignedInteger('recommendation_id');
			$table->foreign('recommendation_id')->references('id')->on('OADH_Recommendation');
			$table->unsignedInteger('right_id');
			$table->foreign('right_id')->references('id')->on('OADH_Right');
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
			$table->unsignedInteger('organization_id')->index();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('OADH_Recommendation_Right');
		Schema::drop('OADH_Recommendation_Institution');
		Schema::drop('OADH_Recommendation');
		Schema::drop('OADH_Recommendation_Temp');
		Schema::drop('OADH_Right');
		Schema::drop('OADH_Institution');
	}
}
