<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRightsPart2 extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('OADH_CR_Amnesty_Temp', function (Blueprint $table) {
			$table->increments('id');
			$table->date('date');
			$table->text('summary');
			$table->text('link')->nullable();
			$table->char('status', 1);
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_CR_Amnesty', function (Blueprint $table) {
			$table->increments('id');
			$table->date('date');
			$table->text('summary');
			$table->text('link')->nullable();
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('OADH_CR_Amnesty_Temp');
		Schema::drop('OADH_CR_Amnesty');
	}
}
