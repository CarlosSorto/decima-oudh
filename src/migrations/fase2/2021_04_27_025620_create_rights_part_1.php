<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRightsPart1 extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// parte 1
		Schema::create('OADH_CR_Budget_Temp', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->string('institution');
			$table->text('budget_unit');
			$table->float('budget_amount');
			$table->char('status', 1);
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_CR_Budget', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->string('institution');
			$table->text('budget_unit');
			$table->float('budget_amount');
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_CR_Allegations_Temp', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->string('institution'); //01, 02, ... 12
			$table->string('department',25); //departamento
			$table->integer('allegations');
			$table->char('status', 1);
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_CR_Allegations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->string('institution'); //01, 02, ... 12
			$table->string('department',25); //departamento
			$table->integer('allegations');
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_CR_Fisdl_Beneficiaries_Temp', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->string('department',25); //departamento
			$table->integer('men_total');
			$table->integer('women_total');
			$table->char('status', 1);
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_CR_Fisdl_Beneficiaries', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->string('department',25); //departamento
			$table->integer('men_total');
			$table->integer('women_total');
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		// parte 2
		Schema::create('OADH_JA_Constitutional_Proccess_Temp', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->integer('month'); //01, 02, ... 12
			$table->integer('protection_committals');
			$table->integer('protection_resolved');
			$table->integer('habeas_corpus_committals');
			$table->integer('habeas_corpus_resolved');
			$table->integer('unconstitutional_committals');
			$table->integer('unconstitutional_resolved');
			$table->char('status', 1);
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_JA_Constitutional_Proccess', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->integer('month'); //01, 02, ... 12
			$table->integer('protection_committals');
			$table->integer('protection_resolved');
			$table->integer('habeas_corpus_committals');
			$table->integer('habeas_corpus_resolved');
			$table->integer('unconstitutional_committals');
			$table->integer('unconstitutional_resolved');
			// $table->char('status', 1);

			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_JA_Crime_Temp', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->integer('month'); //1, 2, ... 12
			$table->string('department',25); //departamento
			$table->string('municipality',60); //municipio
			$table->string('stage', 60); //I = Ingresadas, J = judicializadas ... etc
			$table->string('crime_type');
			$table->string('crime');
			$table->string('victim_gender', 60)->nullable(); //H = Hombre, M = mujer ... etc
			$table->string('victim_age_range', 100);
			$table->string('result')->nullable(); //promedio de 15 caracteres por registro
			$table->integer('value');
			$table->char('status', 1);
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('OADH_JA_Crime', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('year'); //2020, 2021
			$table->integer('month'); //1, 2, ... 12
			$table->string('department',25); //departamento
			$table->string('municipality',60); //municipio
			$table->string('stage', 60); //I = Ingresadas, J = judicializadas ... etc
			$table->string('crime_type');
			$table->string('crime');
			$table->string('victim_gender', 60)->nullable(); //H = Hombre, M = mujer ... etc
			$table->string('victim_age_range', 100);
			$table->string('result')->nullable(); //promedio de 15 caracteres por registro
			$table->integer('value');
			
			$table->unsignedInteger('file_id')->nullable();
			$table->foreign('file_id')->references('id')->on('FILE_File');
      $table->unsignedInteger('organization_id')->index();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('OADH_JA_Crime');
		Schema::drop('OADH_CR_Fisdl_Beneficiaries');
		Schema::drop('OADH_CR_Allegations');
		Schema::drop('OADH_CR_Budget');
		Schema::drop('OADH_CR_Constitutional_Proccess');
	}
}
