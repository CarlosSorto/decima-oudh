<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart10 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('OADH_CMS_Access', function (Blueprint $table)
      {
        $table->increments('id');
        $table->datetime('datetime');
        $table->string('right_to');
        $table->string('name');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_CMS_Access');
    }
}
