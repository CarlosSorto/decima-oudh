<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart8 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('OADH_CMS_Publication', function (Blueprint $table)
      {
        $table->increments('id');
        $table->char('type', 1);
        $table->date('date');
        $table->string('title');
        $table->text('description');
        $table->string('author');
        $table->char('lang', 2);
        $table->text('tags')->nullable();
        $table->text('image_url');
        $table->text('download_url');
        $table->text('summary_url');
        $table->text('infographic_url');
        $table->tinyInteger('is_highlighted')->default(0);

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_CMS_Activity', function (Blueprint $table)
      {
        $table->increments('id');
        $table->date('date');
        $table->string('title');
        $table->string('place');
        $table->text('description');
        $table->char('lang', 2);
        $table->text('tags')->nullable();
        $table->text('image_url');
        $table->tinyInteger('is_highlighted')->default(0);

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_CMS_Activity_Image', function (Blueprint $table)
      {
        $table->increments('id');
        $table->text('image_url');
        $table->string('description');

        $table->unsignedInteger('activity_id')->nullable();
        $table->foreign('activity_id')->references('id')->on('OADH_CMS_Activity');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_CMS_Multimedia', function (Blueprint $table)
      {
        $table->increments('id');
        $table->date('date');
        $table->string('title');
        $table->text('description');
        $table->char('lang', 2);
        $table->text('tags')->nullable();
        $table->text('image_url');
        $table->tinyInteger('is_highlighted')->default(0);

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_CMS_Key_Value', function (Blueprint $table)
      {
        $table->increments('id');
        $table->char('key', 4);
        // $table->longText('value');
        $table->longText('es_value');
        $table->longText('en_value');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_CMS_Subscription', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('email');
        $table->datetime('datetime');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_CMS_Suscription');
      Schema::dropIfExists('OADH_CMS_Key_Value');
      Schema::dropIfExists('OADH_CMS_Multimedia');
      Schema::dropIfExists('OADH_CMS_Activity_Image');
      Schema::dropIfExists('OADH_CMS_Activity');
      Schema::dropIfExists('OADH_CMS_Publication');

    }
}
