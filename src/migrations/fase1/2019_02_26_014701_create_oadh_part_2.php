<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('OADH_News_News', function (Blueprint $table)
      {
        $table->increments('id');

        //Foreign Key
        $table->unsignedInteger('related_news_id');
        $table->foreign('related_news_id')->references('id')->on('OADH_News');
        $table->unsignedInteger('news_id');
        $table->foreign('news_id')->references('id')->on('OADH_News');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_News_News');

    }
}
