<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Ya
      Schema::create('OADH_FREE_Habeas_Corpus_Request_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("court");
        $table->integer("year");
        $table->string("sex")->nullable();//FALTO
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Habeas_Corpus_Request', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("court");
        $table->string("sex")->nullable();//FALTO

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

//Ya
      Schema::create('OADH_FREE_People_Detained_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("state");
        $table->integer("year");
        $table->string("departament")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_People_Detained', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("state");
        $table->integer("year");
        $table->string("departament")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Criminal_Cases_Trials_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("phase");
        $table->string("court");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Criminal_Cases_Trials', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("phase");
        $table->string("court");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Mass_Trials_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("sex")->nullable();
        $table->string("court");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Mass_Trials', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("sex")->nullable();
        $table->string("court");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Procedural_Fraud_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("gender")->nullable();
        $table->string("crime");
        $table->integer("year");
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Procedural_Fraud', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("gender")->nullable();
        $table->string("crime");
        $table->integer("year");
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Capture_Orders_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->integer("year");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Capture_Orders', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->integer("year");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Disappearances_Victims_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->integer("year");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Disappearances_Victims', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->integer("year");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Accused_Liberty_Deprivation_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->string("profession");
        $table->string("gender")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Accused_Liberty_Deprivation', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->string("profession");
        $table->string("gender")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Victims_Liberty_Deprivation_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->integer("year");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Victims_Liberty_Deprivation', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->integer("year");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Forced_Disappearances_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("gender")->nullable();
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->string("profession");
        $table->string("crime");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Forced_Disappearances', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("gender")->nullable();
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->string("profession");
        $table->string("crime");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Deprived_Persons_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("right_to");
        $table->string("violated_fact");
        $table->string("sex")->nullable();
        $table->integer("age");
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Deprived_Persons', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("right_to");
        $table->string("violated_fact");
        $table->string("sex")->nullable();
        $table->integer("age");
        $table->string("departament")->nullable();
        $table->string("municipality")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Acute_Diseases_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("disease");
        $table->integer("quantity");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Acute_Diseases', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("disease");
        $table->integer("quantity");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Chronic_Diseases_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("chronic_disease");
        $table->string("sex")->nullable();
        $table->integer("age");
        $table->string("jail");
        $table->integer("year");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Chronic_Diseases', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("chronic_disease");
        $table->string("sex")->nullable();
        $table->integer("age");
        $table->string("jail");
        $table->integer("year");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_FREE_Jails_Occupation_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("departament")->nullable();
        $table->string("station");
        $table->string("unit");
        $table->integer("year");
        $table->integer("capacity");
        $table->integer("man_population");
        $table->integer("women_population");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_FREE_Jails_Occupation', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("departament")->nullable();
        $table->string("station");
        $table->string("unit");
        $table->integer("year");
        $table->integer("capacity");
        $table->integer("man_population");
        $table->integer("women_population");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_FREE_Jails_Occupation');
      Schema::dropIfExists('OADH_FREE_Jails_Occupation_Temp');

      Schema::dropIfExists('OADH_FREE_Chronic_Diseases');
      Schema::dropIfExists('OADH_FREE_Chronic_Diseases_Temp');

      Schema::dropIfExists('OADH_FREE_Acute_Diseases');
      Schema::dropIfExists('OADH_FREE_Acute_Diseases_Temp');

      Schema::dropIfExists('OADH_FREE_Deprived_Persons');
      Schema::dropIfExists('OADH_FREE_Deprived_Persons_Temp');

      Schema::dropIfExists('OADH_FREE_Forced_Disappearances');
      Schema::dropIfExists('OADH_FREE_Forced_Disappearances_Temp');

      Schema::dropIfExists('OADH_FREE_Victims_Liberty_Deprivation');
      Schema::dropIfExists('OADH_FREE_Victims_Liberty_Deprivation_Temp');

      Schema::dropIfExists('OADH_FREE_Accused_Liberty_Deprivation');
      Schema::dropIfExists('OADH_FREE_Accused_Liberty_Deprivation_Temp');

      Schema::dropIfExists('OADH_FREE_Disappearances_Victims');
      Schema::dropIfExists('OADH_FREE_Disappearances_Victims_Temp');

      Schema::dropIfExists('OADH_FREE_Capture_Orders');
      Schema::dropIfExists('OADH_FREE_Capture_Orders_Temp');

      Schema::dropIfExists('OADH_FREE_Procedural_Fraud');
      Schema::dropIfExists('OADH_FREE_Procedural_Fraud_Temp');

      Schema::dropIfExists('OADH_FREE_Mass_Trials');
      Schema::dropIfExists('OADH_FREE_Mass_Trials_Temp');

      Schema::dropIfExists('OADH_FREE_Criminal_Cases_Trials');
      Schema::dropIfExists('OADH_FREE_Criminal_Cases_Trials_Temp');

      Schema::dropIfExists('OADH_FREE_People_Detained');
      Schema::dropIfExists('OADH_FREE_People_Detained_Temp');

      Schema::dropIfExists('OADH_FREE_Habeas_Corpus_Request');
      Schema::dropIfExists('OADH_FREE_Habeas_Corpus_Request_Temp');
    }
}
