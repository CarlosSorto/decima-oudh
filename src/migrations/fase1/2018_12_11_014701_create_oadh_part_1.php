<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('OADH_Settings', function (Blueprint $table)
      {
        $table->increments('id');
        $table->text('population_affected')->nullable();
        $table->text('note_locations')->nullable();
        $table->text('journalistic_genre')->nullable();
        $table->text('sources')->nullable();//nuevo
        $table->text('sexual_diversities')->nullable();//nuevo
        $table->text('guns_types')->nullable();//nuevo
        $table->boolean('is_configured')->nullable();

        //Foreign Key
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_Digital_Media', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('name')->nullable();
        $table->char('type', 1)->nullable();
        $table->char('abbreviation', 5)->nullable();

        //Foreign Key
        $table->unsignedInteger('parent_id')->nullable();
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::table('OADH_Digital_Media', function(Blueprint $table)
      {
        //Foreign Key
        $table->foreign('parent_id')->references('id')->on('OADH_Digital_Media');
      });


      Schema::create('OADH_News', function(Blueprint $table)
      {
        $table->increments('id');
        $table->date('date')->nullable();
        $table->string('title')->nullable();
        $table->string('code', 20)->nullable();

        $table->integer('page')->nullable();

        $table->time('time')->nullable();
        $table->string('journalistic_genre')->nullable();
        $table->text('summary')->nullable();
        $table->text('new_link')->nullable();
        $table->text('note_locations')->nullable();

        $table->text('twitter_link')->nullable();
        $table->text('facebook_link')->nullable();

        //Foreign Key
        $table->unsignedInteger('digital_media_id');
        $table->foreign('digital_media_id')->references('id')->on('OADH_Digital_Media');
        $table->unsignedInteger('section_id');
        $table->foreign('section_id')->references('id')->on('OADH_Digital_Media');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });


      Schema::create('OADH_Source', function(Blueprint $table)
      {
        $table->increments('id');
        $table->text('source')->nullable();
        $table->string('instance')->nullable();
        $table->string('responsable')->nullable();

        //Foreign Key
        $table->unsignedInteger('news_id');
        $table->foreign('news_id')->references('id')->on('OADH_News');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_Producedure', function(Blueprint $table)
      {
        $table->increments('id');
        $table->string('place')->nullable();
        $table->string('weapon')->nullable();
        $table->text('hypothesis_fact')->nullable();
        $table->text('context')->nullable();
        $table->string('accident_type')->nullable();
        $table->text('summary')->nullable();

        //Foreign Key
        $table->unsignedInteger('news_id');
        $table->foreign('news_id')->references('id')->on('OADH_News');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_Context', function(Blueprint $table)
      {
        $table->increments('id');
        $table->string('department')->nullable();
        $table->string('municipality')->nullable();
        $table->string('neighborhood')->nullable();

        //Foreign Key
        $table->unsignedInteger('news_id');
        $table->foreign('news_id')->references('id')->on('OADH_News');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_Human_Right', function (Blueprint $table)
      {
        $table->increments('id');
        $table->text('name');
        $table->text('es_name');
        $table->text('en_name');
        $table->text('es_description');
        $table->text('en_description');
        $table->char('type', 1)->nullable();
        $table->tinyInteger('is_web_visible')->default(0);

        //Foreign Key
        $table->unsignedInteger('parent_id')->nullable();
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::table('OADH_Human_Right', function(Blueprint $table)
      {
        //Foreign Key
        $table->foreign('parent_id')->references('id')->on('OADH_Human_Right');
      });


      Schema::create('OADH_News_Human_Rights', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('population_affected')->nullable();
        $table->tinyInteger('qualification');
        $table->string('justification');

        //Foreign Key
        $table->unsignedInteger('tracing_type_id');
        $table->foreign('tracing_type_id')->references('id')->on('OADH_Human_Right');
        $table->unsignedInteger('right_id');
        $table->foreign('right_id')->references('id')->on('OADH_Human_Right');
        $table->unsignedInteger('topic_id')->nullable();
        $table->foreign('topic_id')->references('id')->on('OADH_Human_Right');
        $table->unsignedInteger('violated_fact_id')->nullable();
        $table->foreign('violated_fact_id')->references('id')->on('OADH_Human_Right');
        $table->unsignedInteger('news_id');
        $table->foreign('news_id')->references('id')->on('OADH_News');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_People', function (Blueprint $table)
      {
        $table->increments('id');
        $table->char('type', 1); //Victima o Victimario
        $table->char('subtype', 1); //Primaria o secundaria
        $table->string('first_name')->nullable();
        $table->string('last_name')->nullable();
        $table->integer('age')->nullable();
        $table->string('profesion')->nullable();
        $table->string('sexual_diversity')->nullable();
        $table->char('gender', 1)->nullable();
        // $table->boolean('relation', 40)->nullable();
        $table->boolean('victimizer_relation')->nullable();//Relacion con el victimario (aplica para ambos formularios)
        $table->boolean('main_victim_relation')->nullable();//Relacion con la victima principal
        $table->boolean('victim_relation')->nullable();//Relacion con la victima

        //Foreign Key
        $table->unsignedInteger('news_id');
        $table->foreign('news_id')->references('id')->on('OADH_News');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_People');
      Schema::dropIfExists('OADH_News_Human_Rights');
      Schema::dropIfExists('OADH_Human_Right');
      Schema::dropIfExists('OADH_Context');
      Schema::dropIfExists('OADH_Source');
      Schema::dropIfExists('OADH_Producedure');
      Schema::dropIfExists('OADH_News');
      Schema::dropIfExists('OADH_Digital_Media');
      Schema::dropIfExists('OADH_Settings');
    }
}
