<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart11 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      /* Primeras graficas a agregar, oudh 2.0
         Tablas para derecho a la vida*/
      
         //Muertes en accidentes de transito

      Schema::create('OADH_LIFE_Deaths_Traffic_Accidents_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('month');//MES
        $table->integer('age')->nullable();//EDAD
        $table->string('sex')->nullable();//SEXO
        $table->string('municipality'); //municipio
        $table->string('department'); //departamento

        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_LIFE_Deaths_Traffic_Accidents', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('month');//MES
        $table->integer('age')->nullable();//EDAD
        $table->string('sex')->nullable();//SEXO
        $table->string('municipality'); //municipio
        $table->string('department'); //departamento

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

        //Causas de accidentes de transito mortales

      Schema::create('OADH_LIFE_Causes_Traffic_Accidents_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('month');//MES
        $table->string('day');//dia
        $table->time('time_accident'); //hora del accidente
        $table->string('cause');//causa
        $table->string('municipality')->nullable(); //municipio
        $table->string('department')->nullable(); //departamento

        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_LIFE_Causes_Traffic_Accidents', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('month');//MES
        $table->string('day');//dia
        $table->time('time_accident'); //hora del accidente
        $table->string('cause');//causa
        $table->string('municipality')->nullable(); //municipio
        $table->string('department')->nullable(); //departamento

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      //Homicidios multiples

      Schema::create('OADH_LIFE_Multiple_Homicides_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('municipality'); //municipio
        $table->string('department'); //departamento
        $table->string('type');//tipo (doble, triple...)
        $table->integer('total');//Total
        $table->string('month');//Mes

        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_LIFE_Multiple_Homicides', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('municipality'); //municipio
        $table->string('department'); //departamento
        $table->string('type');//tipo (doble, triple...)
        $table->integer('total');//Total
        $table->string('month');//Mes


        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

        /*Tablas para Derecho a La Libertad
          Población privada de libertad en Centros penales  */

      Schema::create('OADH_FREE_Deprived_Freedom_Penal_Center_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('sex')->nullable();//SEXO
        $table->string('age_range');//rango de edad
        $table->string('legal_status'); //situación juridica
        $table->string('penal_center'); //centro penal
        
        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_FREE_Deprived_Freedom_Penal_Center', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('sex')->nullable();//SEXO
        $table->string('age_range');//rango de edad
        $table->string('legal_status'); //situación juridica
        $table->string('penal_center'); //centro penal
        
        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      
      //denuncias al derecho de no desaparecer 

      Schema::create('OADH_FREE_Right_Not_Dissappear_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('sex')->nullable();//SEXO
        $table->integer('age');//Edad
        $table->string('department'); //departamento
        $table->string('municipality'); //municipio
        $table->string('protected_right'); //derecho protegido
        $table->string('violated_fact'); //hecho violatorio
        
        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_FREE_Right_Not_Dissappear', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('sex')->nullable();//SEXO
        $table->integer('age');//Edad
        $table->string('department'); //departamento
        $table->string('municipality'); //municipio
        $table->string('protected_right'); //derecho protegido
        $table->string('violated_fact'); //hecho violatorio
        
        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      //denuncias al derecho a la libertad

      Schema::create('OADH_FREE_Complaints_Right_Freedom_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('sex')->nullable();//SEXO
        $table->integer('age');//Edad
        $table->string('department'); //departamento
        $table->string('municipality'); //municipio
        $table->string('protected_right'); //derecho protegido
        $table->string('violated_fact'); //hecho violatorio
        
        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_FREE_Complaints_Right_Freedom', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('year');//AÑO
        $table->string('sex')->nullable();//SEXO
        $table->integer('age');//Edad
        $table->string('department'); //departamento
        $table->string('municipality'); //municipio
        $table->string('protected_right'); //derecho protegido
        $table->string('violated_fact'); //hecho violatorio
        
        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

            //detenciones por delito

            Schema::create('OADH_FREE_Crime_Detentions_Temp', function (Blueprint $table)
            {
              $table->increments('id');
              $table->integer('year');//AÑO
              $table->string('sex')->nullable();//SEXO              
              $table->string('department'); //departamento
              $table->string('municipality'); //municipio
              $table->string('crime'); //delito
              
              $table->char('status', 1);
      
              $table->unsignedInteger('file_id')->nullable();
              $table->foreign('file_id')->references('id')->on('FILE_File');
      
              $table->unsignedInteger('organization_id')->index();
      
              $table->timestamps();
              $table->softDeletes();
            });
      
            Schema::create('OADH_FREE_Crime_Detentions', function (Blueprint $table)
            {
              $table->increments('id');
              $table->integer('year');//AÑO
              $table->string('sex')->nullable();//SEXO              
              $table->string('department'); //departamento
              $table->string('municipality'); //municipio
              $table->string('crime'); //delito
              
              $table->unsignedInteger('file_id')->nullable();
              $table->foreign('file_id')->references('id')->on('FILE_File');
      
              $table->unsignedInteger('organization_id')->index();
      
              $table->timestamps();
              $table->softDeletes();
            });

      /*Derecho a la Integridad
        */

      Schema::create('OADH_INT_Extortions_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('department'); //departamento
        $table->string('municipality'); //municipio
        $table->string('day');//DIA
        $table->string('weapon_type'); //tipo de arma
        $table->integer('year'); //año
        
        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Extortions', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('department'); //departamento
        $table->string('municipality'); //municipio
        $table->string('day');//DIA
        $table->string('weapon_type'); //tipo de arma
        $table->integer('year'); //año
        
        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_LIFE_Deaths_Traffic_Accidents');
      Schema::dropIfExists('OADH_LIFE_Deaths_Traffic_Accidents_Temp');
      Schema::dropIfExists('OADH_LIFE_Causes_Traffic_Accidents');
      Schema::dropIfExists('OADH_LIFE_Causes_Traffic_Accidents_Temp');
      Schema::dropIfExists('OADH_LIFE_Multiple_Homicides_Temp');
      Schema::dropIfExists('OADH_LIFE_Multiple_Homicides');

      Schema::dropIfExists('OADH_FREE_Deprived_Freedom_Temp');
      Schema::dropIfExists('OADH_FREE_Deprived_Freedom');
      Schema::dropIfExists('OADH_FREE_Right_Not_Dissappear_Temp');
      Schema::dropIfExists('OADH_FREE_Right_Not_Dissappear');     
      Schema::dropIfExists('OADH_FREE_Complaints_Right_Freedom_Temp');
      Schema::dropIfExists('OADH_FREE_Complaints_Right_Freedom');
      Schema::dropIfExists('OADH_FREE_Crime_Detentions_Temp');
      Schema::dropIfExists('OADH_FREE_Crime_Detentions');

      Schema::dropIfExists('OADH_INT_Extortions_Temp');
      Schema::dropIfExists('OADH_INT_Extortions');

    }
}

/*
DB_DRIVER=pgsql
DB_SCHEMA=public
DB_HOST=sndbdev.uca.edu.sv
DB_DATABASE=OUDH_DEV1
DB_USERNAME=OUDH_SUPPORT
DB_PASSWORD=salbutamol
*/