<?php

/**
 * @file
 * Application Routes.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

/*
|--------------------------------------------------------------------------
| Package Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// var_dump(URL::current());
// 'http://localhost:8000/oadh/transactions/media-monitoring' (length=56)

// Route::group(array('middleware' => array('auth', 'check.first.time.access', 'check.access', 'csrf'), 'prefix' => 'module/setup'), function()
// {
// 	// Route::controller('/initial-Decimaoadh-setup', 'Mgallegos\Mgallegos\DecimaOadh\Controllers\SettingManager');
// });
$oadh =  function ()
{
	//Freedom
	Route::post('/cms/freedom/habeas-corpus-request', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getHabeasCorpus'
	]);

	Route::post('/cms/freedom/people-detained', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getPeopleDetained'
	]);

	Route::post('/cms/freedom/criminal-cases-trials', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getCriminalCasesTrials'
	]);

	Route::post('/cms/freedom/mass-trials', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getMassTrials'
	]);

	Route::post('/cms/freedom/procedural-fraud', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getProceduralFraud'
	]);

	Route::post('/cms/freedom/capture-orders', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getCaptureOrders'
	]);

	Route::post('/cms/freedom/disappearances-victims', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getDisappearancesVictims'
	]);

	Route::post('/cms/freedom/accused-liberty-deprivation', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getAccusedLibertyDeprivation'
	]);

	Route::post('/cms/freedom/victims-liberty-deprivation', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getVictimsLibertyDeprivation'
	]);

	Route::post('/cms/freedom/forced-disappearances', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getForcedDisappearances'
	]);

	Route::post('/cms/freedom/jails-occupation', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getJailsOccupation'
	]);

	Route::post('/cms/freedom/deprived-persons', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getDeprivedPersons'
	]);

	Route::post('/cms/freedom/acute-diseases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getAcuteDiseases'
	]);

	Route::post('/cms/freedom/chronic-diseases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomQueryManager@getChronicDiseases'
	]);

	//Integrity
	Route::post('/cms/integrity/crimes-victims', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getCrimesVictims'
	]);

	Route::post('/cms/integrity/crimes-accused', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getCrimesAccused'
	]);

	Route::post('/cms/integrity/working-officials', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getWorkingOfficials'
	]);

	Route::post('/cms/integrity/gender-trafficking', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getGenderTrafficking'
	]);

	Route::post('/cms/integrity/type-trafficking', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getTypeTrafficking'
	]);

	Route::post('/cms/integrity/age-trafficking', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getAgeTrafficking'
	]);

	Route::post('/cms/integrity/movement-freedom', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getMovementFreedom'
	]);

	Route::post('/cms/integrity/age-women', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getAgeWomen'
	]);

	Route::post('/cms/integrity/type-women', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getTypeWomen'
	]);

	Route::post('/cms/integrity/state-women', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getStateWomen'
	]);

	Route::post('/cms/integrity/type-sexual', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getTypeSexual'
	]);

	Route::post('/cms/integrity/gender-sexual', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getGenderSexual'
	]);

	Route::post('/cms/integrity/age-sexual', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getAgeSexual'
	]);

	Route::post('/cms/integrity/state-injuries', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getStateInjuries'
	]);

	Route::post('/cms/integrity/age-injuries', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getAgeInjuries'
	]);

	Route::post('/cms/integrity/schedule-injuries', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getScheduleInjuries'
	]);

	Route::post('/cms/integrity/agent-investigation', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getAgentInvestigation'
	]);

	Route::post('/cms/integrity/integrity-violation', [
	'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityQueryManager@getIntegrityViolation'
	]);

	//Life
	Route::post('/cms/life/homicides-incident', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesIncident'
	]);

	Route::post('/cms/life/homicides-rate', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesRate'
	]);

	Route::post('/cms/life/homicides-of-women', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getWomenHomicidesIncident'
	]);

	Route::post('/cms/life/homicides-rate-of-women', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getWomenHomicidesRate'
	]);

	Route::post('/cms/life/homicides-illegitimate-agressions', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getIllegimitateAgressionsIncident'
	]);

	Route::post('/cms/life/weapon-type-agressions', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getWeaponTypeAgressions'
	]);

	Route::post('/cms/life/gender-agressions', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getGenderAgressions'
	]);

	Route::post('/cms/life/homicides-incident-by-age', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesByAgeRange'
	]);

	Route::post('/cms/life/homicides-rate-by-age', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesRateByAge'
	]);

	Route::post('/cms/life/homicides-incident-on-women', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesOnWomenByAgeAndYear'
	]);

	Route::post('/cms/life/homicides-incident-by-time-aggression', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesIncidentByTimeAggression'
	]);

	Route::post('/cms/life/homicides-incident-by-illegitimate-aggression-by-year', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesByIllegitimateAggressionByYear'
	]);

	Route::post('/cms/life/homicides-incident-by-illegitimate-aggression-by-week-day', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@gethomicidesByIllegitimateAgressionsByWeekDayAndYear'
	]);

	Route::post('/cms/life/homicides-incident-by-illegitimate-aggression-by-time', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@gethomicidesIncidentByIllegitimateAggressionByTime'
	]);

	Route::post('/cms/life/homicides-incident-by-illegitimate-aggression-by-gang', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesByIllegitimateAgressionsByGang'
	]);

	Route::post('/cms/life/police-investigated-by-homicide-feminicide', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getPoliceInvestigatedByHomicideAndFeminicide'
	]);

	Route::post('/cms/life/homicides-incident-by-deceased-cops', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesByIllegitimateAgressionsOnCopsByYear'
	]);

	Route::post('/cms/life/police-investigated-by-crimes', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getPoliceInvestigatedByCrimes'
	]);

	Route::post('/cms/life/homicides-incident-by-month', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesIncidentByMonth'
	]);

	Route::post('/cms/life/homicides-incident-on-women-by-month', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getHomicidesIncidentOnWomenByMonth'
	]);

	Route::post('/cms/life/proportional-ratio-on-illegitimate-aggressions', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getProportionalRatioOnIllegitimateAggressions'
	]);

	Route::post('/cms/life/homicides-by-deceased', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@homicidesByIllegitimateAggressionsByDeceased'
	]);

	Route::post('/cms/life/officers-investigated-by-type', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeQueryManager@getOfficersInvestigatedByInvestigationType'
	]);

	// Access to Justice
	Route::post('/cms/justice-access/victims-of-domestic-violence', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getVictimsOfDomesticViolence'
	]);

	Route::post('/cms/justice-access/victims-in-leiv', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getVictimsInLeiv'
	]);

	Route::post('/cms/justice-access/victims-sexual-assault', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getVictimsSexualAssault'
	]);

	Route::post('/cms/justice-access/victims-feminicides', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getVictimsFeminicides'
	]);

	Route::post('/cms/justice-access/domestic-violence-on-women-registered-by-fgr', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getDomesticViolenceOnWomenRegisteredByFgr'
	]);

	Route::post('/cms/justice-access/domestic-violence-on-women-processed-cases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getDomesticViolenceOnWomenProcessedCases'
	]);

	Route::post('/cms/justice-access/domestic-violence-on-women-judgments', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getDomesticViolenceOnWomenJudgments'
	]);

	Route::post('/cms/justice-access/sexual-assault-on-women-registered-by-fgr', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getSexualAssaultOnWomenRegisteredByFgr'
	]);

	Route::post('/cms/justice-access/sexual-assault-on-women-processed-cases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getSexualAssaultOnWomenProcessedCases'
	]);

	Route::post('/cms/justice-access/sexual-assault-on-women-judgments', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getSexualAssaultOnWomenJudgments'
	]);
	
	Route::post('/cms/justice-access/crimes-on-women-on-leiv-registered-by-fgr', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getCrimesOnWomenOnLeivRegisteredByFgr'
	]);
	
	Route::post('/cms/justice-access/crimes-on-women-on-leiv-processed-cases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getCrimesOnWomenOnLeivProcessedCases'
	]);
	
	Route::post('/cms/justice-access/crimes-on-women-on-leiv-judgments', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getCrimesOnWomenOnLeivJudgments'
	]);
	
	Route::post('/cms/justice-access/feminicide-victims-registered-by-fgr', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getFeminicideVictimsRegisteredByFgr'
	]);
	
	Route::post('/cms/justice-access/feminicide-victims-processed-cases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getFeminicideVictimsProcessedCases'
	]);
	
	Route::post('/cms/justice-access/feminicide-victims-judgments', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getFeminicideVictimsJudgments'
	]);
	
	Route::post('/cms/justice-access/total-anual-domestic-violence', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getTotalAnualDomesticViolence'
	]);
	
	Route::post('/cms/justice-access/total-anual-sexual-assault', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getTotalAnualSexualAssault'
	]);
	
	Route::post('/cms/justice-access/total-anual-on-leiv', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getTotalAnualOnLeiv'
	]);
	
	Route::post('/cms/justice-access/total-anual-feminicides', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getTotalAnualFeminicides'
	]);
	
	Route::post('/cms/justice-access/processed-resolved-protection-cases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getProcessedResolvedProtectionCases'
	]);
	
	Route::post('/cms/justice-access/habeas-corpus-resolved-protection-cases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getHabeasCorpusResolvedProtectionCases'
	]);
	
	Route::post('/cms/justice-access/unconstitutional-resolved-protection-cases', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getUnconstitutionalResolvedProtectionCases'
	]);
	
	Route::post('/cms/justice-access/get-anual-comparison-domestic-violence', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getAnualComparisonDomesticViolence'
	]);
	
	Route::post('/cms/justice-access/get-anual-comparison-sexual-assault', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getAnualComparisonSexualAssault'
	]);
	
	Route::post('/cms/justice-access/get-anual-comparison-leiv', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getAnualComparisonLeiv'
	]);
	
	Route::post('/cms/justice-access/get-anual-comparison-feminicides', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeQueryManager@getAnualComparisonFeminicides'
	]);

	//end access to justice

	//compensation right
	Route::post('/cms/compensation-right/veterans-ex-combatants-mgdt', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getVeteransExCombatantsMgdt'
	]);

	Route::post('/cms/compensation-right/veterans-ex-combatants-fisdl', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getVeteransExCombatantsFisdl'
	]);

	Route::post('/cms/compensation-right/veterans-vs-victims', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getVeteransVsVictims'
	]);

	Route::post('/cms/compensation-right/mgdt-vs-fisdl', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getMgdtVsFisdl'
	]);

	Route::post('/cms/compensation-right/fisdl-beneficiaries', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getTotalFisdlBeneficiaries'
	]);

	Route::post('/cms/compensation-right/national-search-comission', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getNationalSearchComission'
	]);

	Route::post('/cms/compensation-right/allegations-cnb', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getAllegationsCnb'
	]);

	Route::post('/cms/compensation-right/allegations-conabusqueda', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getAllegationsConabusqueda'
	]);

	Route::post('/cms/compensation-right/compensation-timeline', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\CompensationQueryManager@getTimeline'
	]);

	//end compensation right

	Route::post('/cms/setting/update-key-value', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\SettingManager@updateKeyValue'
	]);

	Route::post('/cms/setting/create-subscription', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\SettingManager@createSubscription'
	]);

	Route::post('/cms/publicaciones/create-download', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\SettingManager@createDownloads'
	]);

	Route::post('/cms/derechos/create-access', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\SettingManager@createAccess'
	]);

	Route::post('/cms/derechos/download-data-xlsx', [
		'uses' => 'Mgallegos\DecimaOadh\Oadh\Controllers\SettingManager@downloadXlsxData'
	]);

	$lang = !empty( Input::get('lang') ) ? Input::get('lang') : substr(Input::server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
	$lang = empty($lang) ? 'es' : $lang;
	$lang = ($lang != 'es' &&  $lang != 'en') ? 'en' : $lang;

	Route::get('/cms/lang', function() use ($lang)
	{
		var_dump(Input::server('HTTP_ACCEPT_LANGUAGE'));
		dd($lang);
	});

	Route::get('/cms/inicio', function() use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		$OadhLifeQueryService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\LifeQueryManagement\LifeQueryManagementInterface');

		return View::make('decima-oadh::front-end/home')
			->with('keyValues', $OadhSettingService->getKeyValues($lang))
			->with('lang', $lang)
			->with('mapData', array())
			->with('mapData', $OadhLifeQueryService->getHomicides(array('filter' => array('year' => '', 'weapon_type' => ''))));
	});

	Route::get('/cms/acerca-de', function() use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');

		return View::make('decima-oadh::front-end/about')
			->with('lang', $lang)
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::get('/cms/derechos/libertad', function() use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		
		return View::make('decima-oadh::front-end/liberty')
			->with('lang', $lang)
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::get('/cms/derechos/vida', function() use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		
		return View::make('decima-oadh::front-end/life')
			->with('lang', $lang)
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::get('/cms/derechos/integridad', function() use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		
		return View::make('decima-oadh::front-end/integrity')
			->with('lang', $lang)
			->with('keyValues', $OadhSettingService->getKeyValues($lang));;
	});

	Route::get('/cms/derechos/acceso-a-la-justicia', function() use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');

		return View::make('decima-oadh::front-end/justice-access')
			->with('lang', $lang)
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::get('/cms/derechos/reparacion-integral-de-victimas', function() use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');

		return View::make('decima-oadh::front-end/compensation-right')
			->with('lang', $lang)
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::get('/cms/publicaciones', function() use ($lang)
	{
		$app = $this->app;
		$OadhPublicationsManagerService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\PublicationManagement\PublicationManagementInterface');
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		
		return View::make('decima-oadh::front-end/entries')
			->with('lang', $lang)
			->with('entries', $OadhPublicationsManagerService->publications($lang))
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::group(array('prefix' => '/cms/recomendaciones'), function() use ($lang)
	{
		AdvancedRoute::controller('/', 'Mgallegos\DecimaOadh\Oadh\Controllers\RecommendationManager');
	});

	Route::get('/cms/recomendaciones/fichas/{id}', function($id) use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		$OadhRecommendationService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManagementInterface');
		
		return View::make('decima-oadh::front-end/record-card')
			->with('lang', $lang)
			->with('rec', $OadhRecommendationService->getRecommendation($id))
			->with('systems', $OadhRecommendationService->getSystems())
			->with('mechanisms', $OadhRecommendationService->getMechanisms())
			->with('institutions', $OadhRecommendationService->getInstitutions())
			->with('rights', $OadhRecommendationService->getRights())
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::group(array('prefix' => '/cms/medios-de-prensa'), function() use ($lang)
	{
		AdvancedRoute::controller('/', 'Mgallegos\DecimaOadh\Oadh\Controllers\NewsManager');
	});

	Route::get('/cms/acerca-monitoreo-medios', function() use ($lang)
	{
		$app = $this->app;
		$OadhCmsMultimediaManagerService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\MultimediaManagement\MultimediaManagementInterface');
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		
		return View::make('decima-oadh::front-end/press01')
			->with('lang', $lang)
			->with('videos', $OadhCmsMultimediaManagerService->multimedias('', $lang))
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::get('/cms/monitoreo-de-medios', function() use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		$OadhHumanRightManagerService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManagementInterface');
		
		return View::make('decima-oadh::front-end/press02')
			->with('lang', $lang)
			->with('keyValues', $OadhSettingService->getKeyValues($lang))
			->with('humanRights', $OadhHumanRightManagerService->getWebVisibleHumanRightsWithTopicAndViolatedFact($lang));
	});

	Route::get('/cms/medios-de-prensa/fichas/{id}', function($id) use ($lang)
	{
		$app = $this->app;
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		$OadhNewsService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\NewsManagement\NewsManagementInterface');
		
		return View::make('decima-oadh::front-end/new-card')
			->with('lang', $lang)
			->with('new', $OadhNewsService->getAdvancedNews($id, $lang))
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::get('/cms/videos', function() use ($lang)
	{
		$app = $this->app;
		$OadhCmsMultimediaManagerService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\MultimediaManagement\MultimediaManagementInterface');
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		
		return View::make('decima-oadh::front-end/videos')
			->with('lang', $lang)
			->with('videos', $OadhCmsMultimediaManagerService->multimedias('', $lang))
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});

	Route::post('/cms/videos-card', function()
	{
		$app = $this->app;
		$lang = Input::json('lang');
		$OadhCmsMultimediaManagerService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\MultimediaManagement\MultimediaManagementInterface');
		
		return View::make('decima-oadh::front-end/videos-card')
			->with('lang', $lang)
			->with('videos', $OadhCmsMultimediaManagerService->multimedias(Input::json('filter'), $lang));
	});

	Route::get('/cms/actividades', function() use ($lang)
	{
		$app = $this->app;
		$OadhCmsActivityManagerService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\ActivityManagement\ActivityManagementInterface');
		$OadhSettingService = $app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface');
		
		return View::make('decima-oadh::front-end/activities')
			->with('lang', $lang)
			->with('activity', $OadhCmsActivityManagerService->getActivitiesWithGalleries($lang))
			->with('keyValues', $OadhSettingService->getKeyValues($lang));
	});
};

Route::group(['domain' => 'localhost'], $oadh);
Route::group(['domain' => '201.131.110.215'], $oadh);
Route::group(['domain' => 'oudh.uca.edu.sv'], $oadh);
Route::group(['domain' => 'oadh.decimaerp.com'], $oadh);

Route::group(array('middleware' => array('auth'), 'prefix' => 'ucaoadh'), function()
{
	Route::group(array('middleware' => array('check.access', 'csrf'), 'prefix' => '/setup'), function()
	{
		AdvancedRoute::controller('/initial-setup', 'Mgallegos\DecimaOadh\Oadh\Controllers\SettingManager');
		AdvancedRoute::controller('/digital-media-management', 'Mgallegos\DecimaOadh\Oadh\Controllers\DigitalMediaManager');
		AdvancedRoute::controller('/human-right-management', 'Mgallegos\DecimaOadh\Oadh\Controllers\HumanRightManager');
	});

	Route::group(array('prefix' => '/transactions'), function()
	{
		Route::group(array('middleware' => array('check.access', 'csrf')), function()
		{
			AdvancedRoute::controller('/media-monitoring', 'Mgallegos\DecimaOadh\Oadh\Controllers\MediaMonitoringManager');
			// AdvancedRoute::controller('/media-monitoring', 'Mgallegos\DecimaOadh\Oadh\Controllers\MediaMonitoringManager');
		});
	});

	Route::group(array('prefix' => '/cms'), function()
	{
		Route::group(array('middleware' => array('check.access', 'csrf')), function()
		{
			AdvancedRoute::controller('/activity-management', 'Mgallegos\DecimaOadh\Oadh\Controllers\ActivityManager');
			AdvancedRoute::controller('/multimedia-management', 'Mgallegos\DecimaOadh\Oadh\Controllers\MultimediaManager');
			AdvancedRoute::controller('/publication-management', 'Mgallegos\DecimaOadh\Oadh\Controllers\PublicationManager');
			AdvancedRoute::controller('/subscribers', 'Mgallegos\DecimaOadh\Oadh\Controllers\SubscriberManager');
			AdvancedRoute::controller('/statistics', 'Mgallegos\DecimaOadh\Oadh\Controllers\StatisticManager');
		});
	});

	Route::group(array('prefix' => '/recommendations'), function()
	{
		Route::group(array('middleware' => array('check.access', 'csrf')), function()
		{
			AdvancedRoute::controller('/recommendations-management', 'Mgallegos\DecimaOadh\Oadh\Controllers\RecommendationAdminManager');
			AdvancedRoute::controller('/institutions-management', 'Mgallegos\DecimaOadh\Oadh\Controllers\InstitutionManager');
			AdvancedRoute::controller('/right-management', 'Mgallegos\DecimaOadh\Oadh\Controllers\RightManager');
			AdvancedRoute::controller('/recommendations-import', 'Mgallegos\DecimaOadh\Oadh\Controllers\RecommendationImportManager');
		});
	});

	Route::post('/file', function()
	{
      $app = $this->app;
			GridEncoder::encodeRequestedData(new \Mgallegos\DecimaOadh\Oadh\Repositories\File\EloquentFileGridRepository($app['db'], $app->make("App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface")), Request::all());
	});

	Route::group(array('prefix' => '/freedom'), function()
	{
		AdvancedRoute::controller('/freedom-uploader', 'Mgallegos\DecimaOadh\Oadh\Controllers\FreedomUploaderManager');

		Route::get('/habeas-corpus-request', function()
		{
			// var_dump(Config::get('folders.habeas-corpus-request'));die();
			return View::make('decima-oadh::freedom/habeas-corpus-request')
			->with('prefix', 'free-hcr-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/people-detained', function()
		{
			return View::make('decima-oadh::freedom/people-detained')
			->with('prefix', 'free-pd-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/criminal-cases-trials', function()
		{
			return View::make('decima-oadh::freedom/criminal-cases-trials')
			->with('prefix', 'free-cct-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/mass-trials', function()
		{
			return View::make('decima-oadh::freedom/mass-trials')
			->with('prefix', 'free-mt-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/procedural-fraud', function()
		{
			return View::make('decima-oadh::freedom/procedural-fraud')
			->with('prefix', 'free-pf-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/capture-orders', function()
		{
			return View::make('decima-oadh::freedom/capture-orders')
			->with('prefix', 'free-co-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/disapparences-victims', function()
		{
			return View::make('decima-oadh::freedom/disapparences-victims')
			->with('prefix', 'free-dv-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/accused-liberty-deprivation', function()
		{
			return View::make('decima-oadh::freedom/accused-liberty-deprivation')
			->with('prefix', 'free-ald-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/victim-liberty-deprivation', function()
		{
			return View::make('decima-oadh::freedom/victim-liberty-deprivation')
			->with('prefix', 'free-vld-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/forced-disappearances', function()
		{
			return View::make('decima-oadh::freedom/forced-disappearances')
			->with('prefix', 'free-fd-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/jails-occupation', function()
		{
			return View::make('decima-oadh::freedom/jails-occupation')
			->with('prefix', 'free-jo-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/deprived-persons', function()
		{
			return View::make('decima-oadh::freedom/deprived-persons')
			->with('prefix', 'free-dp-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/acute-disease', function()
		{
			return View::make('decima-oadh::freedom/acute-disease')
			->with('prefix', 'free-ad-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/chronic-diseases', function()
		{
			return View::make('decima-oadh::freedom/chronic-diseases')
			->with('prefix', 'free-cd-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/template', function()
		{
			return View::make('decima-oadh::freedom/template')
			->with('prefix', 'oadh-free-prefx-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});
	});

	Route::group(array('prefix' => '/integrity'), function()
	{
		AdvancedRoute::controller('/integrity-uploader', 'Mgallegos\DecimaOadh\Oadh\Controllers\IntegrityUploaderManager');

		Route::get('/crimes-victims', function()
		{
			return View::make('decima-oadh::integrity/crimes-victims')
			->with('prefix', 'inte-cv-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/crimes-accused', function()
		{
			return View::make('decima-oadh::integrity/crimes-accused')
			->with('prefix', 'inte-ca-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/working-officials', function()
		{
			return View::make('decima-oadh::integrity/working-officials')
			->with('prefix', 'inte-wo-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/gender-trafficking', function()
		{
			return View::make('decima-oadh::integrity/gender-trafficking')
			->with('prefix', 'inte-gt-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/type-trafficking', function()
		{
			return View::make('decima-oadh::integrity/type-trafficking')
			->with('prefix', 'inte-tt-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/age-trafficking', function()
		{
			return View::make('decima-oadh::integrity/age-trafficking')
			->with('prefix', 'inte-at-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/movement-freedom', function()
		{
			return View::make('decima-oadh::integrity/movement-freedom')
			->with('prefix', 'inte-mf-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/age-women', function()
		{
			return View::make('decima-oadh::integrity/age-women')
			->with('prefix', 'inte-aw-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/type-women', function()
		{
			return View::make('decima-oadh::integrity/type-women')
			->with('prefix', 'inte-tw-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/state-women', function()
		{
			return View::make('decima-oadh::integrity/state-women')
			->with('prefix', 'inte-sw-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/type-sexual', function()
		{
			return View::make('decima-oadh::integrity/type-sexual')
			->with('prefix', 'inte-ts-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/gender-sexual', function()
		{
			return View::make('decima-oadh::integrity/gender-sexual')
			->with('prefix', 'inte-gs-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/age-sexual', function()
		{
			return View::make('decima-oadh::integrity/age-sexual')
			->with('prefix', 'inte-as-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/state-injuries', function()
		{
			return View::make('decima-oadh::integrity/state-injuries')
			->with('prefix', 'inte-si-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/age-injuries', function()
		{
			return View::make('decima-oadh::integrity/age-injuries')
			->with('prefix', 'inte-ai-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/schedule-injuries', function()
		{
			return View::make('decima-oadh::integrity/schedule-injuries')
			->with('prefix', 'inte-ci-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/agent-investigation', function()
		{
			return View::make('decima-oadh::integrity/agent-investigation')
			->with('prefix', 'inte-gi-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/integrity-violation', function()
		{
			return View::make('decima-oadh::integrity/integrity-violation')
			->with('prefix', 'inte-iv-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/template', function()
		{
			return View::make('decima-oadh::integrity/template')
			->with('prefix', 'oadh-inte-prefix-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});
	});

	Route::group(array('prefix' => '/life'), function()
	{
		AdvancedRoute::controller('/life-uploader', 'Mgallegos\DecimaOadh\Oadh\Controllers\LifeUploaderManager');

		Route::get('/homicides-rates', function()
		{
			return View::make('decima-oadh::life/homicides-rates')
			->with('prefix', 'oadh-life-hr-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/officers-investigated', function()
		{
			return View::make('decima-oadh::life/officers-investigated')
			->with('prefix', 'oadh-life-oi-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/aggression-weapon', function()
		{
			return View::make('decima-oadh::life/aggressions-weapon')
			->with('prefix', 'oadh-life-aw-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/aggressions-death', function()
		{
			return View::make('decima-oadh::life/aggressions-death')
			->with('prefix', 'oadh-life-ad-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});
		/*
		Route::get('/template', function()
		{
			return View::make('decima-oadh::life/template')
			->with('prefix', 'oadh-life-prefix-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		}); */
	});

	Route::group(array('prefix' => '/justice'), function()
	{
		AdvancedRoute::controller('/justice-uploader', 'Mgallegos\DecimaOadh\Oadh\Controllers\JusticeUploaderManager');

		Route::get('/crimes', function()
		{
			// var_dump(Config::get('folders.habeas-corpus-request'));die();
			return View::make('decima-oadh::justice/crimes')
			->with('prefix', 'oadh-justice-cm-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/constitutional-proccess', function()
		{
			// var_dump(Config::get('folders.habeas-corpus-request'));die();
			return View::make('decima-oadh::justice/constitutional-proccess')
			->with('prefix', 'oadh-justice-cp-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});
	});

	Route::group(array('prefix' => '/reparation'), function()
	{
		AdvancedRoute::controller('/reparation-uploader', 'Mgallegos\DecimaOadh\Oadh\Controllers\ReparationUploaderManager');

		Route::get('/budget', function()
		{
			return View::make('decima-oadh::reparation/budget')
			->with('prefix', 'oadh-reparation-bt-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/allegations', function()
		{
			return View::make('decima-oadh::reparation/allegations')
			->with('prefix', 'oadh-reparation-as-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/fisdl-beneficiaries', function()
		{
			return View::make('decima-oadh::reparation/fisdl-beneficiaries')
			->with('prefix', 'oadh-reparation-fb-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('//amnesty-law-news', function()
		{
			return View::make('decima-oadh::reparation//amnesty-law-news')
			->with('prefix', 'oadh-reparation-aln-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});
	});

	Route::group(array('prefix' => '/population'), function()
	{
		AdvancedRoute::controller('/population-uploader', 'Mgallegos\DecimaOadh\Oadh\Controllers\PopulationUploaderManager');

		Route::get('/age-population', function()
		{
			return View::make('decima-oadh::population/age-population')
			->with('prefix', 'oadh-pop-ap-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/territory-population', function()
		{
			return View::make('decima-oadh::population/territory-population')
			->with('prefix', 'oadh-pop-tp-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});

		Route::get('/template', function()
		{
			return View::make('decima-oadh::population/template')
			->with('prefix', 'oadh-pop-prefx-')
			->with('appInfo', AppManager::getAppInfo())
			->with('userOrganizations', UserManager::getUserOrganizations())
			->with('userAppPermissions', UserManager::getUserAppPermissions())
			->with('userActions', UserManager::getUserActions())
			->with('organizationJournals', OrganizationManager::getOrganizationJournals());
		});
	});
});
