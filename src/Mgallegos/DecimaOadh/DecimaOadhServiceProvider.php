<?php

namespace Mgallegos\DecimaOadh;

use Carbon\Carbon;

use Illuminate\Support\ServiceProvider;

class DecimaOadhServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	* Bootstrap any application services.
	*
	* @return void
	*/
	public function boot()
	{
		include __DIR__.'/../../routes.php';

		// include __DIR__.'/../../helpers.php';

		$this->loadViewsFrom(__DIR__.'/../../views', 'decima-oadh');

		$this->loadTranslationsFrom(__DIR__.'/../../lang', 'decima-oadh');

		$this->publishes([
				__DIR__ . '/../../config/config.php' => config_path('oadh-general.php'),
		], 'config');

		$this->mergeConfigFrom(
				__DIR__ . '/../../config/config.php', 'oadh-general'
		);

		$this->publishes([
				__DIR__ . '/../../config/journal.php' => config_path('oadh-journal.php'),
		], 'config');

		$this->mergeConfigFrom(
				__DIR__ . '/../../config/journal.php', 'oadh-journal'
		);

		// $this->publishes([
		// 		__DIR__ . '/../../config/folders.php' => config_path('oadh-folders.php'),
		// ], 'config');

		$this->mergeConfigFrom(
				__DIR__ . '/../../config/folders.php', 'folders'
		);

		$this->publishes([
    __DIR__.'/../../migrations/' => database_path('/migrations')
		], 'migrations');

		$this->registerJournalConfiguration();
		$this->registerSettingInterface();
		$this->registerSettingManagementInterface();
		$this->registerContextInterface();
		$this->registerHumanRightsInterface();
		$this->registerPeopleInterface();
		$this->registerSourceInterface();
		$this->registerProducedureInterface();
		$this->registerNewsInterface();
		$this->registerNewsNewsInterface();
		$this->registerNewsManagementInterface();
		$this->registerDigitalMediaInterface();
		$this->registerDigitalMediaManagementInterface();
		$this->registerHumanRightInterface();
		$this->registerHumanRightManagementInterface();
		$this->registerRightToInterface();
		$this->registerActivityInterface();
		$this->registerActivityImageInterface();
		$this->registerActivityManagementInterface();
		$this->registerMultimediaInterface();
		$this->registerMultimediaManagementInterface();
		$this->registerPublicationInterface();
		$this->registerPublicationManagementInterface();
		$this->registerUploaderManagementInterface();
		$this->registerFreedomUploaderManagementInterface();
		$this->registerIntegrityUploaderManagementInterface();
		$this->registerLifeUploaderManagementInterface();
		$this->registerPopulationUploaderManagementInterface();
		$this->registerFreedomQueryInterface();
		$this->registerIntegrityQueryInterface();
		$this->registerIntegrityQueryManagementInterface();
		$this->registerLifeQueryInterface();
		$this->registerLifeQueryManagementInterface();
		$this->registerFreedomQueryManagementInterface();
		$this->registerJusticeQueryInterface();
		$this->registerJusticeQueryManagementInterface();
		$this->registerCompensationQueryInterface();
		$this->registerCompensationQueryManagementInterface();
		$this->registerInstitutionInterface();
		$this->registerRightInterface();
		$this->registerRecommendationInterface();
		$this->registerRecommendationRightInterface();
		$this->registerRecommendationImportInterface();
		$this->registerRecommendationInstitutionInterface();
		$this->registerRecommendationManagementInterface();
		$this->registerInstitutionManagementInterface();
		$this->registerRightManagementInterface();
		$this->registerJusticeUploaderManagementInterface();
		$this->registerReparationUploaderManagementInterface();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	* Register journal configuration.
	*
	* @return void
	*/
	protected function registerJournalConfiguration()
	{
		$journalConfiguration = $this->app->make('AppJournalConfigurations');

		$this->app->instance('AppJournalConfigurations', array_merge($journalConfiguration, $this->app['config']->get('oadh-journal')));
	}

	/**
	* Register a Setting interface instance.
	*
	* @return void
	*/
	protected function registerSettingInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Setting\SettingInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentSetting( new \Mgallegos\DecimaOadh\Oadh\Setting(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a Setting interface instance.
	*
	* @return void
	*/
	protected function registerSettingManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Setting\SettingInterface'),
				new \Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentSettingGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new \Mgallegos\DecimaOadh\Oadh\Repositories\Download\EloquentDownloadGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new \Mgallegos\DecimaOadh\Oadh\Repositories\Download\EloquentNewsDownloadGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new \Mgallegos\DecimaOadh\Oadh\Repositories\Download\EloquentRecommendationDownloadGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new \Mgallegos\DecimaOadh\Oadh\Repositories\Subscriber\EloquentSubscriberGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new \Mgallegos\DecimaOadh\Oadh\Repositories\Access\EloquentAccessGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('App\Kwaai\Security\Repositories\User\UserInterface'),
				$app['validator'],
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a news interface instance.
	*
	* @return void
	*/
	protected function registerNewsInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\News\NewsInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\News\EloquentNews( new \Mgallegos\DecimaOadh\Oadh\News() , $AuthenticationManager->getCurrentUserOrganizationConnection(), $app['db']);
		});
	}

	/**
	* Register a news interface instance.
	*
	* @return void
	*/
	protected function registerNewsNewsInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\News\NewsNewsInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\News\EloquentNewsNews( new \Mgallegos\DecimaOadh\Oadh\NewsNews() , $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a context interface instance.
	*
	* @return void
	*/
	protected function registerContextInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Context\ContextInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Context\EloquentContext( new \Mgallegos\DecimaOadh\Oadh\Context() , $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a human rights interface instance.
	*
	* @return void
	*/
	protected function registerHumanRightsInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights\HumanRightsInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights\EloquentHumanRights( new \Mgallegos\DecimaOadh\Oadh\HumanRights(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a human rights interface instance.
	*
	* @return void
	*/
	protected function registerPeopleInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\People\PeopleInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\People\EloquentPeople(new \Mgallegos\DecimaOadh\Oadh\People() ,$AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a Source interface instance.
	*
	* @return void
	*/
	protected function registerSourceInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Source\SourceInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Source\EloquentSource( new \Mgallegos\DecimaOadh\Oadh\Source() , $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a Producedure interface instance.
	*
	* @return void
	*/
	protected function registerProducedureInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Producedure\ProducedureInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Producedure\EloquentProducedure( new \Mgallegos\DecimaOadh\Oadh\Producedure() , $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerNewsManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\NewsManagement\NewsManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\NewsManagement\NewsManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\News\EloquentNewsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\News\EloquentNewsNewsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\News\NewsInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\News\NewsNewsInterface'),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Context\EloquentContextGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Context\ContextInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Setting\SettingInterface'),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights\EloquentHumanRightsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights\HumanRightsInterface'),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\People\EloquentVictimGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\People\EloquentVictimizerGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\People\PeopleInterface'),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Source\EloquentSourceGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Source\SourceInterface'),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Producedure\EloquentProducedureGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Producedure\ProducedureInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\DigitalMediaInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\HumanRightInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['Illuminate\Contracts\Routing\ResponseFactory']
			);
		});
	}


	/**
	* Register a Digital Media interface instance.
	*
	* @return void
	*/
	protected function registerDigitalMediaInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\DigitalMediaInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\EloquentDigitalMedia( new \Mgallegos\DecimaOadh\Oadh\DigitalMedia() , $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a Digital Media interface instance.
	*
	* @return void
	*/
	protected function registerDigitalMediaManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement\DigitalMediaManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement\DigitalMediaManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\EloquentDigitalMediaGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\DigitalMediaInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	/**
	* Register a Human Right interface instance.
	*
	* @return void
	*/
	protected function registerHumanRightInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\HumanRightInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\EloquentHumanRight( new \Mgallegos\DecimaOadh\Oadh\HumanRight() , $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a Digital Media interface instance.
	*
	* @return void
	*/
	protected function registerHumanRightManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\EloquentHumanRightGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\HumanRightInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	/**
	* Register a human rights interface instance.
	*
	* @return void
	*/
	protected function registerRightToInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\RightTo\RightToInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\RightTo\EloquentRightTo(
				$app['db'],
				$AuthenticationManager->getCurrentUserOrganizationConnection()
			);
		});
	}

	/**
	* Register a Activity interface instance.
	*
	* @return void
	*/
	protected function registerActivityInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Activity\ActivityInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Activity\EloquentActivity( new \Mgallegos\DecimaOadh\Oadh\Activity(), $app['db'] , $AuthenticationManager->getCurrentUserOrganizationConnection());

		});
	}

	/**
	* Register a Activity Image interface instance.
	*
	* @return void
	*/
	protected function registerActivityImageInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\ActivityImage\ActivityImageInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\ActivityImage\EloquentActivityImage( new \Mgallegos\DecimaOadh\Oadh\ActivityImage(), $app['db'] , $AuthenticationManager->getCurrentUserOrganizationConnection());

		});
	}

	/**
	* Register a Activity management interface instance.
	*
	* @return void
	*/
	protected function registerActivityManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\ActivityManagement\ActivityManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\ActivityManagement\ActivityManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Activity\EloquentActivityGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\ActivityImage\EloquentActivityImageGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Activity\ActivityInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\ActivityImage\ActivityImageInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a Multimedia interface instance.
	*
	* @return void
	*/
	protected function registerMultimediaInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Multimedia\MultimediaInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Multimedia\EloquentMultimedia( new \Mgallegos\DecimaOadh\Oadh\Multimedia(), $app['db'] , $AuthenticationManager->getCurrentUserOrganizationConnection());

		});
	}

	/**
	* Register a Multimedia management interface instance.
	*
	* @return void
	*/
	protected function registerMultimediaManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\MultimediaManagement\MultimediaManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\MultimediaManagement\MultimediaManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Multimedia\EloquentMultimediaGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Multimedia\MultimediaInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a Publication interface instance.
	*
	* @return void
	*/
	protected function registerPublicationInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Publication\PublicationInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Publication\EloquentPublication( new \Mgallegos\DecimaOadh\Oadh\Publication(), $app['db'] , $AuthenticationManager->getCurrentUserOrganizationConnection());

		});
	}

	/**
	* Register a Publication management interface instance.
	*
	* @return void
	*/
	protected function registerPublicationManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\PublicationManagement\PublicationManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\PublicationManagement\PublicationManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Publication\EloquentPublicationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Publication\PublicationInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a Digital Media interface instance.
	*
	* @return void
	*/
	protected function registerUploaderManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\RightTo\RightToInterface'),
				new Carbon(),
				$app->make('Maatwebsite\Excel\Excel'),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	/**
	* Register a Digital Media interface instance.
	*
	* @return void
	*/
	protected function registerFreedomUploaderManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\FreedomUploaderManagement\FreedomUploaderManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\FreedomUploaderManagement\FreedomUploaderManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface'),
				//Habeas Corpus
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeHabeasCorpusRequestGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeHabeasCorpusRequestTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//People Detained
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreePeopleDetainedGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreePeopleDetainedTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Criminal Cases Trials
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeCriminalCasesTrialsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeCriminalCasesTrialsTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Mass Trials
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeMassTrialsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeMassTrialsTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Producedural Fault
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeProceduralFraudGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeProceduralFraudTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Capture Orders
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeCaptureOrdersGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeCaptureOrdersTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Disappearances Victims
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeDisappearancesVictimsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeDisappearancesVictimsTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Accused Liberty Deprivation
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeAccusedLibertyDeprivationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeAccusedLibertyDeprivationTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Victims Liberty Deprivation
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeVictimsLibertyDeprivationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeVictimsLibertyDeprivationTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Forced Disappearances
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeForcedDisappearancesGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeForcedDisappearancesTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Jails Occupation
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeJailsOccupationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeJailsOccupationTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Deprived Persons
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeDeprivedPersonsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeDeprivedPersonsTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Acute Diseases
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeAcuteDiseasesGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeAcuteDiseasesTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Chronic Diseases
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeChronicDiseasesGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeChronicDiseasesTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new Carbon(),
				$app->make('Maatwebsite\Excel\Excel'),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	/**
	* Register a Integrity Uploader Management interface instance.
	*
	* @return void
	*/
	protected function registerIntegrityUploaderManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\IntegrityUploaderManagement\IntegrityUploaderManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\IntegrityUploaderManagement\IntegrityUploaderManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface'),
				//Crimes victims
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteCrimesVictimsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteCrimesVictimsTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Crimes Accudes
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteCrimesAccusedGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteCrimesAccusedTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Working Oficials
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteWorkingOfficialsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteWorkingOfficialsTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Gender Trafficking
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteGenderTraffickingGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteGenderTraffickingTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Type Traffcking
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeTraffickingGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeTraffickingTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeTraffickingGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeTraffickingTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteMovementFreedomGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteMovementFreedomTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeWomenGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeWomenTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeWomenGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeWomenTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteStateWomenGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteStateWomenTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeSexualGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeSexualTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteGenderSexualGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteGenderSexualTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeSexualGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeSexualTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteStateInjuriesGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteStateInjuriesTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeInjuriesGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeInjuriesTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteScheduleInjuriesGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteScheduleInjuriesTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgentInvestigationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgentInvestigationTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteIntegrityViolationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteIntegrityViolationTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				new Carbon(),
				$app->make('Maatwebsite\Excel\Excel'),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	/**
	* Register a Life Uploader Management interface instance.
	*
	* @return void
	*/
	protected function registerLifeUploaderManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\LifeUploaderManagement\LifeUploaderManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\LifeUploaderManagement\LifeUploaderManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface'),

				//Homicides Rates
				 new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeHomicidesRatesGridRepository(
				 	$app['db'],
				 	$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				 	$app['translator']
				 ),
				 new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeHomicidesRatesTempGridRepository(
				 	$app['db'],
				 	$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				 	$app['translator']
				 ),

				//Officers Investigated
				 new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeOfficersInvestigatedGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeOfficersInvestigatedTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				//Aggressions Weapon
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeAggressionsWeaponGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeAggressionsWeaponTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				//Aggressions Death
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeAggressionsDeathGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeAggressionsDeathTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),

				//Crimes victims
				// new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentInteCrimesVictimsGridRepository(
				// 	$app['db'],
				// 	$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				// 	$app['translator']
				// ),
				// new	\Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentInteCrimesVictimsTempGridRepository(
				// 	$app['db'],
				// 	$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				// 	$app['translator']
				// ),
				new Carbon(),
				$app->make('Maatwebsite\Excel\Excel'),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	/**
	* Register a Population Uploader Management interface instance.
	*
	* @return void
	*/
	protected function registerPopulationUploaderManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\PopulationUploaderManagement\PopulationUploaderManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\PopulationUploaderManagement\PopulationUploaderManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface'),
				//TerritoryPopulation
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Population\EloquentPopTerritoryPopulationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Population\EloquentPopTerritoryPopulationTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//AgePopulation
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Population\EloquentPopAgePopulationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Population\EloquentPopAgePopulationTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new Carbon(),
				$app->make('Maatwebsite\Excel\Excel'),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	/**
	* Register a human rights interface instance.
	*
	* @return void
	*/
	protected function registerFreedomQueryInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\FreedomQueryInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreedomQuery(
				$app['db'],
				$AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a FreedomQuery Manager interface instance.
	*
	* @return void
	*/
	protected function registerFreedomQueryManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\FreedomQueryManagement\FreedomQueryManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\FreedomQueryManagement\FreedomQueryManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\FreedomQueryInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a human rights interface instance.
	*
	* @return void
	*/
	protected function registerIntegrityQueryInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\IntegrityQueryInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentIntegrityQuery(
				$app['db'],
				$AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a IntegrityQuery Manager interface instance.
	*
	* @return void
	*/
	protected function registerIntegrityQueryManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\IntegrityQueryManagement\IntegrityQueryManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\IntegrityQueryManagement\IntegrityQueryManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\IntegrityQueryInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a human rights interface instance.
	*
	* @return void
	*/
	protected function registerLifeQueryInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Life\LifeQueryInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeQuery(
				$app['db'],
				$AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a LifeQuery Manager interface instance.
	*
	* @return void
	*/
	protected function registerLifeQueryManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\LifeQueryManagement\LifeQueryManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\LifeQueryManagement\LifeQueryManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Life\LifeQueryInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a human rights interface instance.
	*
	* @return void
	*/
	protected function registerJusticeQueryInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Justice\JusticeQueryInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeQuery(
				$app['db'],
				$AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a JusticeQuery Manager interface instance.
	*
	* @return void
	*/
	protected function registerJusticeQueryManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\JusticeQueryManagement\JusticeQueryManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\JusticeQueryManagement\JusticeQueryManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Justice\JusticeQueryInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a human rights interface instance.
	*
	* @return void
	*/
	protected function registerCompensationQueryInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Compensation\CompensationQueryInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');

			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Compensation\EloquentCompensationQuery(
				$app['db'],
				$AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a CompensationQuery Manager interface instance.
	*
	* @return void
	*/
	protected function registerCompensationQueryManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\CompensationQueryManagement\CompensationQueryManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\CompensationQueryManagement\CompensationQueryManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Compensation\CompensationQueryInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerInstitutionInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Institution\InstitutionInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');
	
			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Institution\EloquentInstitution( new \Mgallegos\DecimaOadh\Oadh\Institution(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerRightInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Right\RightInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');
	
			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Right\EloquentRight( new \Mgallegos\DecimaOadh\Oadh\Right(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerRecommendationInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation\RecommendationInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');
	
			return new \Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation\EloquentRecommendation( new \Mgallegos\DecimaOadh\Oadh\Recommendation(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerRecommendationRightInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight\RecommendationRightInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');
	
			return new \Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight\EloquentRecommendationRight( new \Mgallegos\DecimaOadh\Oadh\RecommendationRight(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerRecommendationImportInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport\RecommendationImportInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');
	
			return new \Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport\EloquentRecommendationImport( new \Mgallegos\DecimaOadh\Oadh\RecommendationImport(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerRecommendationInstitutionInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationInstitution\RecommendationInstitutionInterface', function($app)
		{
			$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');
	
			return new \Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationInstitution\EloquentRecommendationInstitution( new \Mgallegos\DecimaOadh\Oadh\RecommendationInstitution(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
		});
	}	

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerRecommendationManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation\EloquentRecommendationGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationInstitution\EloquentRecommendationInstitutionGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight\EloquentRecommendationRightGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport\EloquentRecommendationImportGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation\RecommendationInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationInstitution\RecommendationInstitutionInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight\RecommendationRightInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport\RecommendationImportInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Institution\InstitutionInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Right\RightInterface'),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Setting\SettingInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerInstitutionManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\InstitutionManagement\InstitutionManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\InstitutionManagement\InstitutionManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Institution\EloquentInstitutionGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Institution\InstitutionInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a ... interface instance.
	*
	* @return void
	*/
	protected function registerRightManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\RightManagement\RightManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\RightManagement\RightManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Right\EloquentRightGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\Right\RightInterface'),
				new Carbon(),
				$app['db'],
				$app['translator'],
				$app['config'],
				$app['cache']
			);
		});
	}

	/**
	* Register a Digital Media interface instance.
	*
	* @return void
	*/
	protected function registerJusticeUploaderManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\JusticeUploaderManagement\JusticeUploaderManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\JusticeUploaderManagement\JusticeUploaderManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface'),
				//Crime
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeCrimeGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeCrimeTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//ConstitutionalProccess
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeConstitutionalProccessGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeConstitutionalProccessTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new Carbon(),
				$app->make('Maatwebsite\Excel\Excel'),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	/**
	* Register a Digital Media interface instance.
	*
	* @return void
	*/
	protected function registerReparationUploaderManagementInterface()
	{
		$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\ReparationUploaderManagement\ReparationUploaderManagementInterface', function($app)
		{
			return new \Mgallegos\DecimaOadh\Oadh\Services\ReparationUploaderManagement\ReparationUploaderManager(
				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
				$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
				$app->make('Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface'),
				$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
				new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
				$app->make('Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface'),
				//Allegations
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationAllegationsGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationAllegationsTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Budget
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationBudgetGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationBudgetTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//FisdlBeneficiaries
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationFisdlBeneficiariesGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationFisdlBeneficiariesTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				//Amnesty
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationAmnestyGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new	\Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationAmnestyTempGridRepository(
					$app['db'],
					$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
					$app['translator']
				),
				new Carbon(),
				$app->make('Maatwebsite\Excel\Excel'),
				$app['db'],
				$app['translator'],
				$app['config']
			);
		});
	}

	// /**
	// * Register a ... interface instance.
	// *
	// * @return void
	// */
	// protected function registerModuleTableNameInterface()
	// {
	// 	$this->app->bind('Mgallegos\DecimaOadh\Oadh\Repositories\ModuleTableName\ModuleTableNameInterface', function($app)
	// 	{
	// 		$AuthenticationManager = $app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface');
	//
	// 		return new \Mgallegos\DecimaOadh\Oadh\Repositories\ModuleTableName\EloquentModuleTableName( new \Mgallegos\DecimaOadh\Oadh\ModuleTableName(), $app['db'], $AuthenticationManager->getCurrentUserOrganizationConnection());
	// 	});
	// }

	// /**
	// * Register a ... interface instance.
	// *
	// * @return void
	// */
	// protected function registerRecommendationManagementInterface()
	// {
	// 	$this->app->bind('Mgallegos\DecimaOadh\Oadh\Services\ModuleTableNameManagement\RecommendationManagementInterface', function($app)
	// 	{
	// 		return new \Mgallegos\DecimaOadh\Oadh\Services\ModuleTableNameManagement\RecommendationManager(
	// 			$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
	// 			$app->make('App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface'),
	// 			$app->make('App\Kwaai\Security\Repositories\Journal\JournalInterface'),
	// 			new	\Mgallegos\LaravelJqgrid\Encoders\JqGridJsonEncoder($app->make('excel')),
	// 			new	\Mgallegos\DecimaOadh\Oadh\Repositories\ModuleTableName\EloquentModuleTableNameGridRepository(
	// 				$app['db'],
	// 				$app->make('App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface'),
	// 				$app['translator']
	// 			),
	// 			$app->make('Mgallegos\DecimaOadh\Oadh\Repositories\ModuleTableName\ModuleTableNameInterface'),
	// 			new Carbon(),
	// 			$app['db'],
	// 			$app['translator'],
	// 			$app['config'],
	// 			$app['cache']
	// 		);
	// 	});
	// }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

}
