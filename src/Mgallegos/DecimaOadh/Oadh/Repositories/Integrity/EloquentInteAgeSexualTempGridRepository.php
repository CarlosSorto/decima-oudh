<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteAgeSexualTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Age_Sexual_Temp AS ase')
			->where('ase.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'ase.id AS oadh_ase_id',
			'ase.year AS oadh_ase_year',
      'ase.crime AS oadh_ase_crime',
			'ase.age_range AS oadh_ase_age_range',
			'ase.status AS oadh_ase_status',
			'ase.file_id AS oadh_ase_file_id'
		);

		$this->orderBy = array(array('ase.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'as.parent_id';

		// $this->leafColumn = 'oadh_as_is_leaf';
	}
}
