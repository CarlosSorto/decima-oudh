<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use Illuminate\Database\DatabaseManager;

use Illuminate\Database\Eloquent\Collection;

class EloquentIntegrityQuery implements IntegrityQueryInterface {

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(DatabaseManager $DB, $databaseConnectionName)
  {
    $this->DB = $DB;

    $this->databaseConnectionName = $databaseConnectionName;
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->EloquentIntegrityQuery->getTable();
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function crimesVictimsCrimeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Crimes_Victims AS cv')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function crimesVictimsSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Crimes_Victims AS cv')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function crimesVictimsAgeRangeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Crimes_Victims AS cv')
        ->select(
          $this->DB->raw('DISTINCT(age_range) AS age_range')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function crimesVictimsByCrimeBySexByAgeRangeByStatus($crime = null, $sex = null, $ageRange = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Crimes_Victims AS cv')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($ageRange))
    {
      $Query->whereIn('age_range', $ageRange);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function crimesAccusedCategoryFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Crimes_Accused AS ca')
        ->select(
          $this->DB->raw('DISTINCT(category) AS category')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function crimesAccusedCrimeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Crimes_Accused AS ca')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function crimesAccusedDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Crimes_Accused AS ca')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function crimesAccusedMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Crimes_Accused AS ca')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function crimesAccusedByCategoryByDepartmentByMunicipalityByStatus($category = null, $crime = null, $department = null, $municipality = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Crimes_Accused AS cv')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($category))
    {
      $Query->whereIn('category', $category);
    }

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }    

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function workingOfficialsCrimeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Working_Officials AS wo')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function workingOfficialsCategoryFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Working_Officials AS wo')
        ->select(
          $this->DB->raw('DISTINCT(category) AS category')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function workingOfficialsSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Working_Officials AS wo')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function workingOfficialsDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Working_Officials AS wo')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function workingOfficialsByCrimeByCategoryBySexByDepartmentByStatus($crime = null, $category = null, $sex = null, $department = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Working_Officials AS wo')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime)) 
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($category))
    {
      $Query->whereIn('category', $category);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function genderTraffickingSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Gender_Trafficking AS gt')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function genderTraffickingBySexByStatus($gender = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Gender_Trafficking AS gt')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($gender))
    {
      $Query->whereIn('sex', $gender);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function typeTraffickingCrimeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Type_Trafficking AS tt')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function typeTraffickingByCrimeByStatus($crime = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Type_Trafficking AS tt')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }


  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function ageTraffickingAgeRangeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Age_Trafficking AS tt')
        ->select(
          $this->DB->raw('DISTINCT(age_range) AS age_range')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function ageTraffickingByAgeRangeByStatus($ageRange = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Age_Trafficking AS at')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($ageRange))
    {
      $Query->whereIn('age_range', $ageRange);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }


  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function movementFreedomCrimeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Movement_Freedom AS mf')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function movementFreedomDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Movement_Freedom AS mf')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function movementFreedomMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Movement_Freedom AS mf')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function movementFreedomAgeRangeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Movement_Freedom AS mf')
        ->select(
          $this->DB->raw('DISTINCT(age_range) AS age_range')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function movementFreedomSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Movement_Freedom AS mf')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->whereIn('sex', ['MASCULINO', 'FEMENINO', 'N/D'])
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function movementFreedomByCrimeByDepartmentByMunicipalityByAgeRangeBySexByStatus ($crime = null, $department = null, $municipality = null, $ageRange = null, $sex = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Movement_Freedom AS mf')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($ageRange))
    {
      $Query->whereIn('age_range', $ageRange);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function ageWomanAgeRangeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Age_Women AS aw')
        ->select(
          $this->DB->raw('DISTINCT(age_range) AS age_range')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function ageWomanByAgeRangeByStatus ($ageRange = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Age_Women AS aw')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($ageRange))
    {
      $Query->whereIn('age_range', $ageRange);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function typeWomanCrimeFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Type_Women AS tw')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function typeWomanByCrimeByStatus ($crime = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Type_Women AS tw')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function stateWomanDepartmentFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_State_Women AS sw')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function stateWomanMunicipalityFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_State_Women AS sw')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function stateWomanByDepartmentByMunicipalityByStatus ($department = null, $municipality = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_State_Women AS tw')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */

  public function typeSexualCrimeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Type_Sexual AS ts')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function typeSexualDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Type_Sexual AS ts')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function typeSexualMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Type_Sexual AS ts')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function typeSexualByCrimeByDepartmentByMunicipalityByStatus ($crime = null, $department = null, $municipality = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Type_Sexual AS ts')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function genderSexualCrimeFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Gender_Sexual AS gs')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function genderSexualSexFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Gender_Sexual AS gs')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function genderSexualByCrimeBySexByStatus ($crime = null, $sex = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Gender_Sexual AS gs')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function ageSexualCrimeFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Age_Sexual AS ias')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function ageSexualAgeRangeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Age_Sexual AS ias')
        ->select(
          $this->DB->raw('DISTINCT(age_range) AS age_range')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function ageSexualByCrimeByAgeRangeByStatus ($crime = null, $ageRange = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Age_Sexual AS ias')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($ageRange))
    {
      $Query->whereIn('age_range', $ageRange);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function stateInjuriesDepartmentFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_State_Injuries AS si')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function stateInjuriesMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_State_Injuries AS si')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function stateInjuriesByDepartmentByMunicipalityByStatus ($department = null, $municipality = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_State_Injuries AS si')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function ageInjuriesSexFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Age_Injuries AS ai')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function ageInjuriesAgeRangeFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Age_Injuries AS ai')
        ->select(
          $this->DB->raw('DISTINCT(age_range) AS age_range')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function ageInjuriesCrimeFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Age_Injuries AS ai')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function ageInjuriesBySexByAgeRangeByStatus ($ageRange = null, $crime = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Age_Injuries AS ai')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

      if (!empty($ageRange)) 
      {
        $Query->whereIn('age_range', $ageRange);
      }

      if (!empty($crime)) 
      {
        $Query->whereIn('crime', $crime);
      }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function scheduleInjuriesWeaponTypeFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Schedule_Injuries AS ssi')
        ->select(
          $this->DB->raw('DISTINCT(weapon_type) AS weapon_type')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function scheduleInjuriesTimeFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Schedule_Injuries AS ssi')
        ->select(
          $this->DB->raw('DISTINCT(time) AS time')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function scheduleInjuriesByAggressorTypeByTimeByStatus ($weaponType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Schedule_Injuries AS ssi')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($weaponType))
    {
      $Query->whereIn('weapon_type', $weaponType);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function agentInvestigationCrimeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Agent_Investigation AS ain')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function agentInvestigationCategoryFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Agent_Investigation AS ain')
        ->select(
          $this->DB->raw('DISTINCT(category) AS category')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function agentInvestigationSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Agent_Investigation AS ain')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function agentInvestigationPlaceFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Agent_Investigation AS ain')
        ->select(
          $this->DB->raw('DISTINCT(place) AS place')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function agentInvestigationDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Agent_Investigation AS ain')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function agentInvestigationMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Agent_Investigation AS ain')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function agentInvestigationStateFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Agent_Investigation AS ain')
        ->select(
          $this->DB->raw('DISTINCT(state) AS state')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function agentInvestigationByCrimeByCategoryBySexByPlaceByDepartmentByMunicipalityByStateByStatus ($crime = null, $category = null, $sex = null, $place = null, $department = null, $municipality = null, $state = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Agent_Investigation AS ain')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($category))
    {
      $Query->whereIn('category', $category);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($state))
    {
      $Query->whereIn('state', $state);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */

  public function integrityViolationSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Integrity_Violation AS iv')
        ->select(
          $this->DB->raw('DISTINCT(btrim(sex)) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $age
   *
   * @return integer
  */

  public function integrityViolationAgeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Integrity_Violation AS iv')
        ->select(
          $this->DB->raw('DISTINCT(age) AS age')
        )
        ->get()
    );
  }
  
  /**
   * Retrieve filter
   *
   * @param array $department
   *
   * @return integer
  */

  public function integrityViolationDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Integrity_Violation AS iv')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $municipality
   *
   * @return integer
  */

  public function integrityViolationMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Integrity_Violation AS iv')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $right_to
   *
   * @return integer
  */
  public function integrityViolationRightToFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Integrity_Violation AS iv')
        ->select(
          $this->DB->raw('DISTINCT(right_to) AS "right"')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $violated_fact
   *
   * @return integer
  */
  public function integrityViolationViolatedFactFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Integrity_Violation AS iv')
        ->select(
          $this->DB->raw('DISTINCT(violated_fact) AS violated_fact')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $institutions
   *
   * @return integer
  */

  public function integrityViolationInstitutionsFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Integrity_Violation AS iv')
        ->select(
          $this->DB->raw('DISTINCT(institutions) AS institutions')
        )
        ->get()
    );
  }


  /**
   * Retrieve filter
   *
   * @param array $violated_fact
   *
   * @return integer
  */

  public function integrityViolationDependenciesFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_INT_Integrity_Violation AS iv')
        ->select(
          $this->DB->raw('DISTINCT(dependencies) AS dependencies')
        )
        ->get()
    );
  }

   /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function integrityViolationBySexByAgeByDepartmentByMunicipalityByRightToByViolatedFactByInstitutionsByDependenciesByStatus ($sex = null, $age = null, $department = null, $municipality = null, $violatedFact = null, $dependencies = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Integrity_Violation AS iv')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($age))
    {
      $Query->whereBetween('age', $age);
    }

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($violatedFact))
    {
      $Query->whereIn('violated_fact', $violatedFact);
    }

    if(!empty($dependencies)) 
    {
      $Query->whereIn('dependencies', $dependencies);
    }

    return new Collection (
      $Query->get()
    );
  }
}
