<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteAgeWomenTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Age_Women_Temp AS aw')
			->where('aw.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'aw.id AS oadh_aw_id',
			'aw.year AS oadh_aw_year',
      'aw.age_range AS oadh_aw_age_range',
			'aw.status AS oadh_aw_status',
			'aw.file_id AS oadh_aw_file_id'
		);

		$this->orderBy = array(array('aw.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'aw.parent_id';

		// $this->leafColumn = 'oadh_aw_is_leaf';
	}
}
