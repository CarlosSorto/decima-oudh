<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteGenderSexualGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Gender_Sexual AS gs')
			->where('gs.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'gs.id AS oadh_gs_id',
			'gs.year AS oadh_gs_year',
			'gs.crime AS oadh_gs_crime',
      'gs.sex AS oadh_gs_sex',
			'gs.file_id AS oadh_gs_file_id'
		);

		$this->orderBy = array(array('gs.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'gs.parent_id';

		// $this->leafColumn = 'oadh_gs_is_leaf';
	}
}
