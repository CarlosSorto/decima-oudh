<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteScheduleInjuriesTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Schedule_Injuries_Temp AS ci')
			->where('ci.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'ci.id AS oadh_ci_id',
			'ci.year AS oadh_ci_year',
      'ci.weapon_type AS oadh_ci_weapon_type',
      // 'ci.aggressor_type AS oadh_ci_aggressor_type',
      // 'ci.time AS oadh_ci_time',
      // 'ci.total AS oadh_ci_total',
			'ci.status AS oadh_ci_status',
			'ci.file_id AS oadh_ci_file_id'
		);

		$this->orderBy = array(array('ci.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ci.parent_id';

		// $this->leafColumn = 'oadh_ci_is_leaf';
	}
}
