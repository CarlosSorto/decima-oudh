<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteWorkingOfficialsTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Working_Officials_Temp AS wo')
			->where('wo.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'wo.id AS oadh_wo_id',
			'wo.year AS oadh_wo_year',
      'wo.crime AS oadh_wo_crime',
			'wo.category AS oadh_wo_category',
			'wo.sex AS oadh_wo_sex',
			'wo.department AS oadh_wo_department',
			'wo.status AS oadh_wo_status',
			'wo.file_id AS oadh_wo_file_id'
		);

		$this->orderBy = array(array('wo.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'wo.parent_id';

		// $this->leafColumn = 'oadh_wo_is_leaf';
	}
}
