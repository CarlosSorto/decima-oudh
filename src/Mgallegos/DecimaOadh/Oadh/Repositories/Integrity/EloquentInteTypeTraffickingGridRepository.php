<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteTypeTraffickingGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Type_Trafficking AS tt')
			->where('tt.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'tt.id AS oadh_tt_id',
			'tt.year AS oadh_tt_year',
			'tt.crime AS oadh_tt_crime',
			'tt.file_id AS oadh_tt_file_id'
		);

		$this->orderBy = array(array('tt.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'tt.parent_id';

		// $this->leafColumn = 'oadh_tt_is_leaf';
	}
}
