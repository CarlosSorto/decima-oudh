<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteCrimesAccusedGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Crimes_Accused AS ca')
			->where('ca.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'ca.id AS oadh_ca_id',
			'ca.year AS oadh_ca_year',
			'ca.category AS oadh_ca_category',
			'ca.crime AS oadh_ca_crime',
			'ca.department AS oadh_ca_department',
			'ca.municipality AS oadh_ca_municipality',
			'ca.file_id AS oadh_ca_file_id'
		);

		$this->orderBy = array(array('ca.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ca.parent_id';

		// $this->leafColumn = 'oadh_ca_is_leaf';
	}
}
