<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteTypeSexualTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Type_Sexual_Temp AS ts')
			->where('ts.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'ts.id AS oadh_ts_id',
			'ts.year AS oadh_ts_year',
			'ts.crime AS oadh_ts_crime',
			'ts.department AS oadh_ts_department',
      'ts.municipality AS oadh_ts_municipality',
			'ts.status AS oadh_ts_status',
			'ts.file_id AS oadh_ts_file_id'
		);

		$this->orderBy = array(array('ts.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ts.parent_id';

		// $this->leafColumn = 'oadh_ts_is_leaf';
	}
}
