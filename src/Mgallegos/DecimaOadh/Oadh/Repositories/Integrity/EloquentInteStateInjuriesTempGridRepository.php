<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteStateInjuriesTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_State_Injuries_Temp AS si')
			->where('si.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'si.id AS oadh_si_id',
			'si.year AS oadh_si_year',
      'si.department AS oadh_si_department',
      'si.municipality AS oadh_si_municipality',
      // 'si.total AS oadh_si_total',
			'si.status AS oadh_si_status',
			'si.file_id AS oadh_si_file_id'
		);

		$this->orderBy = array(array('si.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'si.parent_id';

		// $this->leafColumn = 'oadh_si_is_leaf';
	}
}
