<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteAgeInjuriesGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Age_Injuries AS ai')
			->where('ai.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'ai.id AS oadh_ai_id',
			'ai.year AS oadh_ai_year',
			'ai.sex AS oadh_ai_sex',
			'ai.crime AS oadh_ai_crime',
			'ai.age_range AS oadh_ai_age_range',
			// 'ai.total AS oadh_ai_total',
			'ai.file_id AS oadh_ai_file_id'
		);

		$this->orderBy = array(array('ai.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ai.parent_id';

		// $this->leafColumn = 'oadh_ai_is_leaf';
	}
}
