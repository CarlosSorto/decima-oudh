<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteAgentInvestigationTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Agent_Investigation_Temp AS gi')
			->where('gi.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'gi.id AS oadh_gi_id',
			'gi.code AS oadh_gi_code',
			'gi.year AS oadh_gi_year',
			'gi.crime AS oadh_gi_crime',
			'gi.category AS oadh_gi_category',
			'gi.sex AS oadh_gi_sex',
			'gi.place AS oadh_gi_place',
			'gi.department AS oadh_gi_department',
			'gi.municipality AS oadh_gi_municipality',
			'gi.state AS oadh_gi_state',
			'gi.status AS oadh_gi_status',
			'gi.file_id AS oadh_gi_file_id'
		);

		$this->orderBy = array(array('gi.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'gi.parent_id';

		// $this->leafColumn = 'oadh_gi_is_leaf';
	}
}
