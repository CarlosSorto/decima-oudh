<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteGenderTraffickingGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Gender_Trafficking AS gt')
			->where('gt.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'gt.id AS oadh_gt_id',
			'gt.year AS oadh_gt_year',
      'gt.sex AS oadh_gt_sex',
			'gt.file_id AS oadh_gt_file_id'
		);

		$this->orderBy = array(array('gt.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'gt.parent_id';

		// $this->leafColumn = 'oadh_gt_is_leaf';
	}
}
