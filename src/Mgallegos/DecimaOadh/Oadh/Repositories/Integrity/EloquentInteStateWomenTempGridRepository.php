<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteStateWomenTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_State_Women_Temp AS sw')
			->where('sw.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'sw.id AS oadh_sw_id',
			'sw.year AS oadh_sw_year',
			'sw.department AS oadh_sw_department',
      'sw.municipality AS oadh_sw_municipality',
			'sw.status AS oadh_sw_status',
			'sw.file_id AS oadh_sw_file_id'
		);

		$this->orderBy = array(array('sw.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'sw.parent_id';

		// $this->leafColumn = 'oadh_sw_is_leaf';
	}
}
