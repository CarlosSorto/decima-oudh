<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteIntegrityViolationGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Integrity_Violation AS iv')
			->where('iv.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'iv.id AS oadh_iv_id',
			'iv.year AS oadh_iv_year',
			// 'iv.date AS oadh_iv_date',
      'iv.sex AS oadh_iv_sex',
      'iv.age AS oadh_iv_age',
      'iv.department AS oadh_iv_department',
      'iv.municipality AS oadh_iv_municipality',
      'iv.right_to AS oadh_iv_right_to',
      'iv.institutions AS oadh_iv_institutions',
      'iv.dependencies AS oadh_iv_dependencies',
			'iv.file_id AS oadh_iv_file_id'
		);

		$this->orderBy = array(array('iv.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'iv.parent_id';

		// $this->leafColumn = 'oadh_iv_is_leaf';
	}
}
