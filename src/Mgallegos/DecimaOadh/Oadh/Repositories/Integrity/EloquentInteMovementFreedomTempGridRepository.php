<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteMovementFreedomTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Movement_Freedom_Temp AS mf')
			->where('mf.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'mf.id AS oadh_mf_id',
			'mf.year AS oadh_mf_year',
			'mf.crime AS oadh_mf_crime',
			'mf.department AS oadh_mf_department',
			'mf.municipality AS oadh_mf_municipality',
			'mf.age_range AS oadh_mf_age_range',
      'mf.sex AS oadh_mf_sex',
			'mf.status AS oadh_mf_status',
			'mf.file_id AS oadh_mf_file_id'
		);

		$this->orderBy = array(array('mf.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'mf.parent_id';

		// $this->leafColumn = 'oadh_mf_is_leaf';
	}
}
