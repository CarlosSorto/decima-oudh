<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Integrity;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentInteCrimesVictimsTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_INT_Crimes_Victims_Temp AS cv')
			->where('cv.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'cv.id AS oadh_cv_id',
			'cv.year AS oadh_cv_year',
			'cv.crime AS oadh_cv_crime',
			'cv.sex AS oadh_cv_sex',
			'cv.age_range AS oadh_cv_age_range',
			'cv.status AS oadh_cv_status',
			'cv.file_id AS oadh_cv_file_id'
		);

		$this->orderBy = array(array('cv.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'cv.parent_id';

		// $this->leafColumn = 'oadh_cv_is_leaf';
	}
}
