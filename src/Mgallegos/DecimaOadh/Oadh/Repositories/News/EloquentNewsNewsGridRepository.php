<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\News;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentNewsNewsGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager, Translator $Lang)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->organizationId = $AuthenticationManager->getCurrentUserOrganizationId();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_News_News AS nn')
			->Join('OADH_News AS n', 'n.id', '=', 'nn.related_news_id')
			->leftjoin('OADH_Digital_Media AS dg', 'dg.id', '=', 'n.digital_media_id')
			->where('n.organization_id', '=', $this->organizationId);

		$this->visibleColumns = array(
			'nn.id AS oadh_news_news_id',
			'n.id AS oadh_news_news_related_news_id',
			$DB->raw('CONCAT(n.code, \' - \', n.title, \' (\', dg.name, \')\') AS oadh_news_news_related_news_label')
		);

		$this->orderBy = array(array('nn.id', 'desc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'parent_id';

		// $this->leafColumn = 'is_leaf';
	}

	/**
	 * Calculate the number of rows. It's used for paging the result.
	 *
	 * @param  array $filters
	 *  An array of filters, example: array(array('field'=>'column index/name 1','op'=>'operator','data'=>'searched string column 1'), array('field'=>'column index/name 2','op'=>'operator','data'=>'searched string column 2'))
	 *  The 'field' key will contain the 'index' column property if is set, otherwise the 'name' column property.
	 *  The 'op' key will contain one of the following operators: '=', '<', '>', '<=', '>=', '<>', '!=','like', 'not like', 'is in', 'is not in'.
	 *  when the 'operator' is 'like' the 'data' already contains the '%' character in the appropiate position.
	 *  The 'data' key will contain the string searched by the user.
	 * @return integer
	 *  Total number of rows
	 */
	public function getTotalNumberOfRows(array $filters = array())
	{
		return  intval($this->Database->whereNested(function($query) use ($filters)
		{
			foreach ($filters as $filter)
			{

				if($filter['field'] == 'source')
				{
					$query->whereRaw("n.id IN (SELECT so.news_id FROM \"OADH_Source\" so WHERE so.source LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND so.organization_id = " . $this->organizationId . ")");
					// $query->whereRaw("n.id IN (SELECT ssd.news_id FROM OADH_Source ssd, INV_Article AS sa WHERE ssod.article_id = sa.id AND sa.type IN ('" . $filter['data'] . "') AND ssod.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'instance')
				{
					$query->whereRaw("n.id IN (SELECT so.news_id FROM \"OADH_Source\" so WHERE so.instance LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND so.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'responsable')
				{
					$query->whereRaw("n.id IN (SELECT so.news_id FROM \"OADH_Source\" so WHERE so.responsable LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND so.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'place')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.place LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'weapon')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.weapon LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'hypothesis_fact')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.hypothesis_fact LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'accident_type')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.accident_type LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'context')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.context LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'summary')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.summary LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_first_name')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.first_name LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_last_name')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.last_name LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_profesion')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.profesion LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_gender')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.gender LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_subtype')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.subtype LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_age')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.age >= " . $filter['data'][1] . "  AND suq.age <= " . $filter['data'][2] . " AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_age_from')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.age >= " . $filter['data'][1] . " AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_age_to')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.age <= " . $filter['data'][1] . " AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'neighborhood')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE suq.neighborhood LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'tracing_type')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE suq.tracing_type_id IN (" . $filter['data'] . ") AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'right')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE suq.right_id IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'topic')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE suq.topic_id IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'violated_fact')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE suq.violated_fact_id IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'department')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE suq.department IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'municipality')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE suq.municipality IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'neighborhood')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE suq.neighborhood LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}



				if($filter['op'] == 'is in')
				{
					$query->whereIn($filter['field'], explode(',',$filter['data']));
					continue;
				}

				if($filter['op'] == 'is not in')
				{
					$query->whereNotIn($filter['field'], explode(',',$filter['data']));
					continue;
				}

				if($filter['op'] == 'is null')
				{
					$query->whereNull($filter['field']);
					continue;
				}

				if($filter['op'] == 'is not null')
				{
					$query->whereNotNull($filter['field']);
					continue;
				}

				if($filter['op'] == 'between')
				{
					if(strpos($filter['data'], ' - ') !== false)
					{
						list($from, $to) = explode(' - ', $filter['data'], 2);

						if(!$from or !$to)
						{
							throw new \Exception('Invalid between format');
						}
					}
					else
					{
						throw new \Exception('Invalid between format');
					}

					if( $from == $to)
					{
						$query->where($filter['field'], $from);
					}else
					{
						//$query->whereBetween($filter['field'], array($from, $to));
						$query->where($filter['field'], '>=', $from);
						$query->where($filter['field'], '<=', $to);
					}

					continue;
				}

				$query->where($filter['field'], $filter['op'], $filter['data']);
			}
		})
		->count());
	}


	/**
	 * Get the rows data to be shown in the grid.
	 *
	 * @param  integer $limit
	 *	Number of rows to be shown into the grid
	 * @param  integer $offset
	 *	Start position
	 * @param  string $orderBy
	 *	Column name to order by.
	 * @param  string $sord
	 *	Sorting order
	 * @param  array $filters
	 *	An array of filters, example: array(array('field'=>'column index/name 1','op'=>'operator','data'=>'searched string column 1'), array('field'=>'column index/name 2','op'=>'operator','data'=>'searched string column 2'))
	 *	The 'field' key will contain the 'index' column property if is set, otherwise the 'name' column property.
	 *	The 'op' key will contain one of the following operators: '=', '<', '>', '<=', '>=', '<>', '!=','like', 'not like', 'is in', 'is not in'.
	 *	when the 'operator' is 'like' the 'data' already contains the '%' character in the appropiate position.
	 *	The 'data' key will contain the string searched by the user.
	 * @param  string $nodeId
	 *	Node id (used only when the treeGrid option is set to true)
	 * @param  string $nodeLevel
	 *	Node level (used only when the treeGrid option is set to true)
	 * @param  boolean $exporting
	 *	Flag that determines if the data will be exported (used only when the treeGrid option is set to true)
	 * @return array
	 *	An array of array, each array will have the data of a row.
	 *  Example: array(array("column1" => "1-1", "column2" => "1-2"), array("column1" => "2-1", "column2" => "2-2"))
	 */
	public function getRows($limit, $offset, $orderBy = null, $sord = null, array $filters = array(), $nodeId = null, $nodeLevel = null, $exporting)
	{
		$orderByRaw = null;

		if(!is_null($orderBy) || !is_null($sord))
		{
			$found = false;
			$pos = strpos($orderBy, 'desc');

			if ($pos !== false)
			{
				$found = true;
			}
			else
			{
				$pos = strpos($orderBy, 'asc');

				if ($pos !== false)
				{
					$found = true;
				}
			}

			if($found)
			{
				$orderBy = rtrim($orderBy);

				if(substr($orderBy, -1) == ',')
				{
					$orderBy = substr($orderBy, 0, -1);
				}
				else
				{
					$orderBy .= " $sord";
				}

				$orderByRaw = $orderBy;
			}
			else
			{
				$this->orderBy = array(array($orderBy, $sord));
			}
		}

		if($limit == 0)
		{
			$limit = 1;
		}

		if(empty($orderByRaw))
		{
			$orderByRaw = array();

			foreach ($this->orderBy as $orderBy)
			{
				array_push($orderByRaw, implode(' ',$orderBy));
			}

			$orderByRaw = implode(',',$orderByRaw);
		}

		$rows = $this->Database->whereNested(function($query) use ($filters, $nodeId, $exporting)
		{
			foreach ($filters as $filter)
			{
				if($filter['field'] == 'source')
				{
					$query->whereRaw("n.id IN (SELECT so.news_id FROM \"OADH_Source\" so WHERE so.source LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND so.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'instance')
				{
					$query->whereRaw("n.id IN (SELECT so.news_id FROM \"OADH_Source\" so WHERE so.instance LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND so.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'responsable')
				{
					$query->whereRaw("n.id IN (SELECT so.news_id FROM \"OADH_Source\" so WHERE so.responsable LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND so.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'place')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.place LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'weapon')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.weapon LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'hypothesis_fact')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.hypothesis_fact LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'accident_type')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.accident_type LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'context')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.context LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'summary')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE suq.summary LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_first_name')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.first_name LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_last_name')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.last_name LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_profesion')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.profesion LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_gender')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.gender LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_subtype')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.subtype LIKE '%" . str_replace(' ', '%', $filter['data'][1]) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_age')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.age >= " . $filter['data'][1] . "  AND suq.age <= " . $filter['data'][2] . " AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_age_from')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.age >= " . $filter['data'][1] . " AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'vic_age_to')
				{
					$filter['data'] = explode('|', $filter['data']);

					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_People\" suq WHERE suq.type = '" . $filter['data'][0] . "' AND suq.age <= " . $filter['data'][1] . " AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'tracing_type')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE suq.tracing_type_id IN (" . $filter['data'] . ") AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'right')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE suq.right_id IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'topic')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE suq.topic_id IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'violated_fact')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE suq.violated_fact_id IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'department')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE suq.department IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'municipality')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE suq.municipality IN ('" . $filter['data'] . "') AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}

				if($filter['field'] == 'neighborhood')
				{
					$query->whereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE suq.neighborhood LIKE '%" . str_replace(' ', '%', $filter['data']) . "%' AND suq.organization_id = " . $this->organizationId . ")");

					continue;
				}





				if($filter['op'] == 'is in')
				{
					$query->whereIn($filter['field'], explode(',',$filter['data']));
					continue;
				}

				if($filter['op'] == 'is not in')
				{
					$query->whereNotIn($filter['field'], explode(',',$filter['data']));
					continue;
				}

				if($filter['op'] == 'is null')
				{
					$query->whereNull($filter['field']);
					continue;
				}

				if($filter['op'] == 'is not null')
				{
					$query->whereNotNull($filter['field']);
					continue;
				}

				if($filter['op'] == 'between')
				{
					if(strpos($filter['data'], ' - ') !== false)
					{
						list($from, $to) = explode(' - ', $filter['data'], 2);

						if(!$from or !$to)
						{
							throw new \Exception('Invalid between format');
						}
					}
					else
					{
						throw new \Exception('Invalid between format');
					}

					if( $from == $to)
					{
						$query->where($filter['field'], $from);
					}else
					{
						//$query->whereBetween($filter['field'], array($from, $to));
						$query->where($filter['field'], '>=', $from);
						$query->where($filter['field'], '<=', $to);
					}
					continue;
				}

				$query->where($filter['field'], $filter['op'], $filter['data']);
			}

			if($this->treeGrid && !$exporting)
			{
				if(empty($nodeId))
				{
					$query->whereNull($this->parentColumn);
				}
				else
				{
					$query->where($this->parentColumn, '=', $nodeId);
				}
			}
		})
		->take($limit)
		->skip($offset)
		->orderByRaw($orderByRaw)
		->get($this->visibleColumns);

		if(!is_array($rows))
		{
			$rows = $rows->toArray();
		}

		foreach ($rows as &$row)
		{
			$row = (array) $row;

			if($this->treeGrid && !$exporting)
			{
				if(is_null($nodeLevel))
				{
					$row['level'] = 0;
				}
				else
				{
					$row['level'] = (int)$nodeLevel + 1;
				}

				if($row[$this->leafColumn] == 0)
				{
					$row[$this->leafColumn] = false;
				}
				else
				{
					$row[$this->leafColumn] = true;
				}
			}
		}

		return $rows;
	}
}
