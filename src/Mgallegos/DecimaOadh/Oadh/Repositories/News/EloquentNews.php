<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\News;

use Illuminate\Database\Eloquent\Model;
use Mgallegos\DecimaOadh\Oadh\News;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\DatabaseManager;

class EloquentNews implements NewsInterface {

  /**
   * News
   *
   * @var Mgallegos\DecimaOadh\Oadh\News;
   *
   */
  protected $News;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  public function __construct(Model $News, $databaseConnectionName, DatabaseManager $DB)
  {
    $this->News = $News;

    $this->databaseConnectionName = $databaseConnectionName;

    $this->News->setConnection($databaseConnectionName);

    $this->DB = $DB;
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->News->getTable();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->News->on($databaseConnectionName)->find($id);
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getContextByNewsId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
      ->table('OADH_News AS n')
      ->leftJoin('OADH_Context AS con', 'n.id', '=', 'con.news_id')
      ->where('con.news_id', '=', $id)
      ->get(array(
        'con.*',
          )
        )
    );
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getVictimByNewsId($id, $type, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_News AS n')
    ->leftJoin('OADH_People AS peo', 'n.id', '=', 'peo.news_id')
    ->where('peo.type', '=', $type)
    ->where('peo.news_id', '=', $id)
    ->get(array(
      'peo.*',
        )
      )
    );
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getSourceByNewsId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_News AS n')
    ->leftJoin('OADH_Source AS sou', 'n.id', '=', 'sou.news_id')
    ->where('sou.news_id', '=', $id)
    ->get(array(
      'sou.*',
        )
      )
    );
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getNewsByHumanRightId($columnName, $id, $dateFrom = null, $dateTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_News AS n')
      ->join('OADH_Digital_Media AS dm', 'n.digital_media_id', '=', 'dm.id')
      ->groupBy('dm.name')
      ->whereRaw("n.id IN (
        SELECT suq.news_id 
        FROM \"OADH_News_Human_Rights\" suq 
        WHERE $columnName = $id
      )")
      ->orderBy('dm.name');
    
    if(!empty($dateFrom))
    {
      $query->where('n.date', '>=', $dateFrom);
    }

    if(!empty($dateTo))
    {
      $query->where('n.date', '<=', $dateTo);
    }

    return new Collection(
      $query->get(array(
        'dm.name AS grid_col_1',
        $this->DB->raw('COUNT(1) AS grid_col_2'),
      )));
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getSourcesByHumanRightId($columnName, $id, $dateFrom = null, $dateTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      // ->table('OADH_Source AS dm')
      ->table('OADH_News AS n')
      ->join('OADH_Source AS dm', 'n.id', '=', 'dm.news_id')
      ->groupBy('dm.source')
      ->whereRaw("dm.news_id IN (
        SELECT suq.news_id 
        FROM \"OADH_News_Human_Rights\" suq 
        WHERE $columnName = $id
      )")
      ->whereNotNull('dm.source')
      ->orderBy('dm.source');
    
    if(!empty($dateFrom))
    {
      $query->where('n.date', '>=', $dateFrom);
    }

    if(!empty($dateTo))
    {
      $query->where('n.date', '<=', $dateTo);
    }

    return new Collection(
      $query->get(array(
        'dm.source AS grid_col_1',
        $this->DB->raw('COUNT(1) AS grid_col_2'),
      )));

    // return new Collection($this->DB->connection($databaseConnectionName)
    //   ->table('OADH_News AS n')
    //   ->join('OADH_Source AS dm', 'n.id', '=', 'dm.news_id')
    //   ->groupBy('dm.source')
    //   ->whereRaw("n.id IN (
    //     SELECT suq.news_id 
    //     FROM \"OADH_News_Human_Rights\" suq 
    //     WHERE $columnName = $id
    //   )")
    //   ->whereNotNull('dm.source')
    //   ->get(array(
    //     'dm.source AS grid_col_1',
    //     $this->DB->raw('COUNT(1) AS grid_col_2'),
    //   )));
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getPeopleAgeByTypeAndByHumanRightId($columnName, $id, $type, $dateFrom = null, $dateTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }
    
    $query = $this->DB->connection($databaseConnectionName)
      // ->table('OADH_People AS dm')
      ->table('OADH_News AS n')
      ->join('OADH_People AS dm', 'n.id', '=', 'dm.news_id')
      ->groupBy('dm.age')
      ->whereRaw("dm.news_id IN (
        SELECT suq.news_id 
        FROM \"OADH_News_Human_Rights\" suq 
        WHERE $columnName = $id
      )")
      ->whereNotNull('dm.age')
      ->where('dm.type', '=', $type)
      ->orderBy('dm.age');
    
    if(!empty($dateFrom))
    {
      $query->where('n.date', '>=', $dateFrom);
    }

    if(!empty($dateTo))
    {
      $query->where('n.date', '<=', $dateTo);
    }

    return new Collection(
      $query->get(array(
        'dm.age AS grid_col_1',
        $this->DB->raw('COUNT(1) AS grid_col_2'),
      )));
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getPeopleGenderByTypeAndByHumanRightId($columnName, $id, $type, $dateFrom = null, $dateTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }
    
    $query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_News AS n')
      ->join('OADH_People AS dm', 'n.id', '=', 'dm.news_id')
      ->groupBy('dm.gender')
      ->whereRaw("dm.news_id IN (
        SELECT suq.news_id 
        FROM \"OADH_News_Human_Rights\" suq 
        WHERE $columnName = $id
      )")
      ->whereNotNull('dm.gender')
      ->where('dm.type', '=', $type)
      ->orderBy('dm.gender');
    
    if(!empty($dateFrom))
    {
      $query->where('n.date', '>=', $dateFrom);
    }

    if(!empty($dateTo))
    {
      $query->where('n.date', '<=', $dateTo);
    }

    return new Collection(
      $query->get(array(
        'dm.gender AS grid_col_1',
        $this->DB->raw('COUNT(1) AS grid_col_2'),
      )));
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getPopulationAffectedByHumanRightId($columnName, $id, $dateFrom = null, $dateTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      // ->table('OADH_News_Human_Rights AS dm')
      ->table('OADH_News AS n')
      ->join('OADH_News_Human_Rights AS dm', 'n.id', '=', 'dm.news_id')
      ->groupBy('dm.population_affected')
      // ->whereRaw("dm.news_id IN (
      //   SELECT suq.news_id 
      //   FROM \"OADH_News_Human_Rights\" suq 
      //   WHERE $columnName = $id
      // )")
      ->where($columnName, '=', $id)
      ->whereNotNull('dm.population_affected')
      ->orderBy('dm.population_affected');
    
    if(!empty($dateFrom))
    {
      $query->where('n.date', '>=', $dateFrom);
    }

    if(!empty($dateTo))
    {
      $query->where('n.date', '<=', $dateTo);
    }
    
    return new Collection(
      $query->get(array(
        'dm.population_affected AS grid_col_1',
        $this->DB->raw('COUNT(1) AS grid_col_2'),
      )));
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getDepartmentsByHumanRightId($columnName, $id, $dateFrom = null, $dateTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      // ->table('OADH_Context AS dm')
      ->table('OADH_News AS n')
      ->join('OADH_Context AS dm', 'n.id', '=', 'dm.news_id')
      ->groupBy('dm.department')
      ->whereRaw("dm.news_id IN (
        SELECT suq.news_id 
        FROM \"OADH_News_Human_Rights\" suq 
        WHERE $columnName = $id
      )")
      ->whereNotNull('dm.department')
      ->orderBy('dm.department');
    
    if(!empty($dateFrom))
    {
      $query->where('n.date', '>=', $dateFrom);
    }

    if(!empty($dateTo))
    {
      $query->where('n.date', '<=', $dateTo);
    }

    return new Collection(
      $query->get(array(
        'dm.department AS grid_col_1',
        $this->DB->raw('COUNT(1) AS grid_col_2'),
      )));
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\News
   */
  public function getWeaponsByHumanRightId($columnName, $id, $dateFrom = null, $dateTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      // ->table('OADH_Producedure AS dm')
      ->table('OADH_News AS n')
      ->join('OADH_Producedure AS dm', 'n.id', '=', 'dm.news_id')
      ->groupBy('dm.weapon')
      ->whereRaw("dm.news_id IN (
        SELECT suq.news_id 
        FROM \"OADH_News_Human_Rights\" suq 
        WHERE $columnName = $id
      )")
      ->whereNotNull('dm.weapon')
      ->orderBy('dm.weapon');
    
    if(!empty($dateFrom))
    {
      $query->where('n.date', '>=', $dateFrom);
    }

    if(!empty($dateTo))
    {
      $query->where('n.date', '<=', $dateTo);
    }
    
    return new Collection(
      $query->get(array(
        'dm.weapon AS grid_col_1',
        $this->DB->raw('COUNT(1) AS grid_col_2'),
      )));
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->News->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  /**
   * Count by code
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function countByCode($code, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->News->setConnection($databaseConnectionName)->where('code', '=', $code)->count('code');
  }

  /**
   * Get search modal table rows
   *
   * @param int $organizationId
   *
   * @return Collection
   */
  public function searchModalTableRows($id = null, $organizationId, $count = false, $limit = null, $offset = null, $filter = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_News AS n')
      ->leftjoin('OADH_Digital_Media AS dg', 'dg.id', '=', 'n.digital_media_id')
      ->leftjoin('OADH_Digital_Media AS sec', 'sec.id', '=', 'n.section_id')
      ->whereNull('n.deleted_at')
      ->where('n.organization_id', '=', $organizationId);

    if(!empty($ids))
    {
      $query->where('n.id', '=', $ids);
    }
    
    if(!empty($filter))
    {
      $query->where(function($dbQuery) use ($filter)
      {
        foreach (array('n.code', 'n.title', 'n.summary', 'dg.name', 'sec.name') as $key => $value)
        {
          // $dbQuery->orWhere($value, 'like', '%' . str_replace(' ', '%', $filter) . '%');
          $dbQuery->orWhereRaw('lower(' . $value . ') LIKE \'%' . mb_strtolower(
            str_replace(' ', '%', $filter)
          ) . '%\'');
        }
      });
    }

    if($count)
    {
      return $query->count();
    }

    if(!empty($limit))
    {
      $query->take($limit);
    }

    if(!empty($offset) && $offset != 0)
    {
      $query->skip($offset);
    }

    return new Collection(
      $query->get(
        array(
          'n.id AS oadh_id',
          'n.title AS oadh_title',
          'n.date AS oadh_date',
          'n.code AS oadh_code',
          'n.page AS oadh_page',
          'n.time AS oadh_time',
          'n.journalistic_genre AS oadh_journalistic_genre',
          'n.summary AS oadh_summary',
          'n.new_link AS oadh_new_link',
          'n.note_locations AS oadh_note_locations',
          'dg.name AS oadh_digital_media_label',
          'n.digital_media_id AS oadh_digital_media_id',
          'sec.name AS oadh_section_label',
          'n.section_id AS oadh_section_id',
          'n.twitter_link AS oadh_links_twitter_link',
          'n.facebook_link AS oadh_links_facebook_link'
        )
      )
    );
  }

  public function searchAdvancedModalTableRows($id = null, $filters = null, $paginate = true, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_News AS n')
      ->leftjoin('OADH_Digital_Media AS dg', 'dg.id', '=', 'n.digital_media_id')
      ->whereNull('n.deleted_at')
      ->orderBy('n.date', 'desc');
      // array(array('n.id', 'desc'));
      // ->orderBy('t1.column_name1', 'asc')

    if(!empty($id))
    {
      $query->where('n.id', '=', $id);
    }

    if(!empty($filters))
    {
      foreach ($filters as $key => $value) 
      {
        switch ($value['op'])
        {
          case 'eq':
            $query->where($value['field'], '=', $value['data']);
            break;
          case 'like':
            $query->where(function($dbQuery) use ($value)
            {
              foreach (array('title', 'summary', 'dg.name') as $key => $column)
              {
                // $dbQuery->orWhere($column, 'like', '%' . str_replace(' ', '%', $value['data']) . '%');
                $dbQuery->orWhereRaw('lower(' . $column . ') LIKE \'%' . mb_strtolower(
                  str_replace(' ', '%', $value['data'])
                ) . '%\'');
              }

              $dbQuery->orWhereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq, \"OADH_Human_Right\" hr WHERE suq.right_id = hr.id AND lower(hr.name) LIKE '%" . str_replace(' ', '%', mb_strtolower($value['data'])) . "%')");
              $dbQuery->orWhereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq, \"OADH_Human_Right\" hr WHERE suq.topic_id = hr.id AND lower(hr.name) LIKE '%" . str_replace(' ', '%', mb_strtolower($value['data'])) . "%')");
              $dbQuery->orWhereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq, \"OADH_Human_Right\" hr WHERE suq.violated_fact_id = hr.id AND lower(hr.name) LIKE '%" . str_replace(' ', '%', mb_strtolower($value['data'])) . "%')");
              $dbQuery->orWhereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE lower(suq.department) LIKE '%" . str_replace(' ', '%', mb_strtolower($value['data'])) . "%')");
              $dbQuery->orWhereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Context\" suq WHERE lower(suq.municipality) LIKE '%" . str_replace(' ', '%', mb_strtolower($value['data'])) . "%')");
              $dbQuery->orWhereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Source\" suq WHERE lower(suq.source) LIKE '%" . str_replace(' ', '%', mb_strtolower($value['data'])) . "%')");
              $dbQuery->orWhereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_News_Human_Rights\" suq WHERE lower(suq.population_affected) LIKE '%" . str_replace(' ', '%', mb_strtolower($value['data'])) . "%')");
              $dbQuery->orWhereRaw("n.id IN (SELECT suq.news_id FROM \"OADH_Producedure\" suq WHERE lower(suq.weapon) LIKE '%" . str_replace(' ', '%', mb_strtolower($value['data'])) . "%')");
            });
            break;
          case 'media_in':
            $query->whereIn('n.digital_media_id', $value['data']);
            break;
          case 'population_affected_in':
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('hur.news_id')
              ->from('OADH_News_Human_Rights AS hur')
              ->whereIn('hur.population_affected', $value['data']);
            });
            break;
          case 'department_in':
            if(!in_array('Todos los departamentos', $value['data']))
            {
              $query->whereIn('n.id', function($query) use($value) 
              {
                $query->select('con.news_id')
                  ->from('OADH_Context AS con')
                  ->whereIn('con.department', $value['data']);
              });
            }
            break;
          case 'municipality_in':
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('con.news_id')
              ->from('OADH_Context AS con')
              ->whereIn('con.municipality', $value['data']);
            });
            break;
          case 'right_in':
            // $query->whereRaw('n.id IN (SELECT hur.news_id FROM public."OADH_News_Human_Rights" as hur WHERE hur.right_id IN (' . implode(',', $value['data']) . '))');
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('hur.news_id')
              ->from('OADH_News_Human_Rights AS hur')
              ->whereIn('hur.right_id', $value['data']);
            });
            break;
          case 'source_in':
            // $query->whereRaw('n.id IN (SELECT sou.news_id FROM public."OADH_Source" as sou WHERE sou.source SIMILAR TO (\'%' . implode(',', $value['data']) . '%\'))');
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('sou.news_id')
              ->from('OADH_Source AS sou')
              ->whereIn('sou.source', $value['data']);
            });
            break;
          case 'topic_in':
            // $query->whereRaw('n.id IN (SELECT hur.news_id FROM public."OADH_News_Human_Rights" as hur WHERE hur.topic_id IN (' . implode(',', $value['data']) . '))');
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('hur.news_id')
              ->from('OADH_News_Human_Rights AS hur')
              ->whereIn('hur.topic_id', $value['data']);
            });
            break;
          case 'violated_in':
            // $query->whereRaw('n.id IN (SELECT hur.news_id FROM public."OADH_News_Human_Rights" as hur WHERE hur.violated_fact_id IN (' . implode(',', $value['data']) . '))');
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('hur.news_id')
              ->from('OADH_News_Human_Rights AS hur')
              ->whereIn('hur.violated_fact_id', $value['data']);
            });
            break;
          case 'victim_gender_in':
            // $query->whereRaw("n.id IN (SELECT peo.news_id FROM \"OADH_People\" as peo WHERE peo.type = '" . $value['type'] . "' AND peo.gender SIMILAR TO ('%" . implode(',', $value['data']) . "%'))");
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('peo.news_id')
              ->from('OADH_People AS peo')
              ->where('peo.type', '=', $value['type'])
              ->whereIn('peo.gender', $value['data']);
            });
            break;
          case 'victim_age_in':
            // $query->whereRaw("n.id IN (SELECT peo.news_id FROM \"OADH_People\" as peo WHERE peo.type = '" . $value['type'] . "' AND peo.age IN (" . implode(',', $value['data']) . "))");
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('peo.news_id')
              ->from('OADH_People AS peo')
              ->where('peo.type', '=', $value['type']);

              if(!empty($value['data_from']))
              {
                $query->where('peo.age', '>=', $value['data_from']);
                
              }

              if(!empty($value['data_to']))
              {
                $query->where('peo.age', '<=', $value['data_to']);
              }
            });
            break;
          case 'weapon_type_in':
            // $query->whereRaw('n.id IN (SELECT prod.news_id FROM public."OADH_Producedure" as prod WHERE prod.weapon SIMILAR TO (\'%' . implode(',', $value['data']) . '%\'))');
            $query->whereIn('n.id', function($query) use($value) 
            {
              $query->select('prod.news_id')
              ->from('OADH_Producedure AS prod')
              ->whereIn('prod.weapon', $value['data']);
            });
            break;
          case 'ge':
            $query->where($value['field'], '>=', $value['data']);
            break;
          case 'le':
            $query->where($value['field'], '<=', $value['data']);
            break;
          default:
            break;
        }
      }
    }

    if(!empty($paginate))
    {
      return $query->select(
        array(
          'n.id AS oadh_id',
          'n.digital_media_id AS oadh_digital_media_id',
          'dg.name AS oadh_digital_media_label',
          'n.title AS oadh_title',
          'n.date AS oadh_date',
          'n.new_link AS oadh_new_link',
          'n.summary AS oadh_summary',
        )
      )->paginate(6);
    }

    return $query->get(
      array(
        'n.id AS oadh_id',
        'n.digital_media_id AS oadh_digital_media_id',
        'dg.name AS oadh_digital_media_label',
        'n.title AS oadh_title',
        'n.date AS oadh_date',
        'n.new_link AS oadh_new_link',
        'n.summary AS oadh_summary',
      )
    );
  }

  public function getNewsFiles($id, $databaseConnectionName)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }
    
    $query = $this->DB->connection($databaseConnectionName)
    ->table('FILE_File AS file')
    ->where('file.system_reference_type', '=', 'OADH_News')
    ->where('file.system_reference_id', '=', $id);


    return new Collection(
      $query->get(
        array(
          'file.*'
        )
      )
    );
  }

  /**
   * Retrieve News with information
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function withInformation($organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $DB = $this->DB->connection($databaseConnectionName)
      ->table('OADH_News AS n')
      ->leftjoin('OADH_Digital_Media AS dg', 'dg.id', '=', 'n.digital_media_id')
      ->leftjoin('OADH_Digital_Media AS sec', 'sec.id', '=', 'n.section_id')
      ->where('n.organization_id', '=', $organizationId)
      ->select(
        'n.id',
  			'n.title',
  			'n.date',
  			'n.code',
  			'n.page',
  			'n.time',
  			'n.journalistic_genre',
  			'n.summary',
  			'n.new_link',
  			'n.note_locations',
  			'dg.name AS digital_media_label',
  			'n.digital_media_id',
  			'sec.name AS section_label',
  			'n.section_id',
  			'n.twitter_link',
  			'n.facebook_link'
      )
      ->get()
    );
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function getMaxId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->News->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('id');
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $News = new News();
    $News->setConnection($databaseConnectionName);
    $News->fill($data)->save();

    return $News;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\News $News
   *
   * @return boolean
   */
  public function update(array $data, $News = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($News))
    {
      $News = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      if(!empty($value))
      {
        $News->$key = $value;
      }
      else
      {
        $News->$key = null;
      }
    }

    return $News->save();
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $News = $this->byId($id, $databaseConnectionName);
      $News->delete();
    }
    // $this->Account->destroy($data);

    return true;
  }

}
