<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Justice;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentJusticeConstitutionalProccessTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			// ->table('OADH_CR_Constitutional_Proccess_Temp AS jcm')
			->table('OADH_JA_Constitutional_Proccess_Temp AS jcm')
			->where('jcm.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'jcm.id AS oadh_justice_cp_id',
			'jcm.year AS oadh_justice_cp_year',
			'jcm.month AS oadh_justice_cp_month',
			'jcm.protection_committals AS oadh_justice_cp_protection_committals',
			'jcm.protection_resolved AS oadh_justice_cp_protection_resolved',
			'jcm.habeas_corpus_committals AS oadh_justice_cp_habeas_corpus_committals',
			'jcm.habeas_corpus_resolved AS oadh_justice_cp_habeas_corpus_resolved',
			'jcm.unconstitutional_committals AS oadh_justice_cp_unconstitutional_committals',
			'jcm.unconstitutional_resolved AS oadh_justice_cp_unconstitutional_resolved',
			'jcm.status AS oadh_justice_cp_status',
			'jcm.file_id AS oadh_justice_cp_file_id'
		);

		$this->orderBy = array(array('jcm.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'jcm.file_id';

		// $this->leafColumn = 'oadh_justice_cp_is_leaf';
	}
}
