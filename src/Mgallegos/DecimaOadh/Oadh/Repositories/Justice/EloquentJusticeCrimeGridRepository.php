<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Justice;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentJusticeCrimeGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_JA_Crime AS jcm')
			->where('jcm.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'jcm.id AS oadh_justice_cm_id',
			'jcm.year AS oadh_justice_cm_year',
			'jcm.month AS oadh_justice_cm_month',
			'jcm.department AS oadh_justice_cm_department',
			'jcm.municipality AS oadh_justice_cm_municipality',
			'jcm.stage AS oadh_justice_cm_stage',
			'jcm.crime_type AS oadh_justice_cm_crime_type',
			'jcm.crime AS oadh_justice_cm_crime',
			'jcm.victim_gender AS oadh_justice_cm_victim_gender',
			'jcm.victim_age_range AS oadh_justice_cm_victim_age_range',
			'jcm.result AS oadh_justice_cm_result',
			'jcm.value AS oadh_justice_cm_value',
			'jcm.file_id AS oadh_justice_cm_file_id'
		);

		$this->orderBy = array(array('jcm.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'jcm.parent_id';

		// $this->leafColumn = 'oadh_justice_cm_is_leaf';
	}
}
