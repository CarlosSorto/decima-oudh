<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Justice;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Collection;

class EloquentJusticeQuery implements JusticeQueryInterface {

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(DatabaseManager $DB, $databaseConnectionName)
  {
    $this->DB = $DB;
    $this->databaseConnectionName = $databaseConnectionName;
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->JusticeQuery->getTable();
  }

  /**
   * Get search modal table rows
   *
   * @param int $organizationId
   *
   * @return Collection
   */
  public function searchModalTableRows($id = null, $organizationId, $count = false, $limit = null, $offset = null, $filter = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      ->table('Table_Name0 AS t0')
      ->join('Table_Name1 AS t1', 't1.column_name', '=', 't0.column_name')
      // ->orderBy('t1.column_name0', 'desc')
      // ->orderBy('t1.column_name1', 'asc')
      // ->whereIn('t1.id', $ids)
      ->where('t0.organization_id', '=', $organizationId);

    if(!empty($id))
    {
      $query->where('t0.id', '=', $id);
    }

    if(!empty($filter))
    {
      $query->where(function($dbQuery) use ($filter)
      {
        foreach (array('column1', 'column2') as $key => $value)
        {
          $dbQuery->orWhere($value, 'like', '%' . str_replace(' ', '%', $filter) . '%');
        }
      });
    }

    if($count)
    {
      return $query->count();
    }

    if(!empty($limit))
    {
      $query->take($limit);
    }

    if(!empty($offset) && $offset != 0)
    {
      $query->skip($offset);
    }

    return new Collection(
      $query->get(
        array(
          't0.*'
        )
      )
    );
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function maxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->JusticeQuery->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Retrieve by filed id and by organization id
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byFileIdAndByOrganizationId($fileId, $organizationId, $notSync = true, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->JusticeQuery->setConnection($databaseConnectionName)
      ->where('file_id', '=', $fileId)
      ->where('organization_id', '=', $organizationId);

    if (isset($notSync))
    {
      if ($notSync == true)
      {
        $query->where('syncronized', '=', 0);
      }
      else if ($notSync == false)
      {
        $query->where('syncronized', '=', 1);
      }
    }

    return $query->get();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\JusticeQuery
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->JusticeQuery->on($databaseConnectionName)->find($id);
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byIds($ids, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
        ->table('Table_Name0 AS t0')
        ->join('Table_Name1 AS t1', 't1.column_name', '=', 't0.column_name')
        // ->where('p.id', '=', $ids)
        ->whereIn('t1.id', $ids)
        // ->orderBy('t1.column_name0', 'desc')
        // ->orderBy('t1.column_name1', 'asc')
        ->get(array('t0.*'))
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->JusticeQuery->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  public function getProcessedResolvedProtectionCasesByYear($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Constitutional_Proccess')
    ->select(
      $this->DB->raw('DISTINCT(year) AS year')
    )
    ->orderBy('year')
    ->get()
    );
  }

  public function getProcessedResolvedProtectionCasesByFields(array $filters, $field, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Constitutional_Proccess')
    ->select("$field as count", 'month')
    ->groupBy($field)
    ->groupBy('month')
    ->orderBy('month');

    if(!empty($filters['year']))
    {
      $query->where('year', '=', $filters['year']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getJusticeYearFilterByCrimeType($crimeType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select(
      $this->DB->raw('DISTINCT(year) AS year')
    )
    ->where('crime_type', '=', $crimeType)
    ->orderBy('year')
    ->get()
    );
  }

  public function getJusticeMonthFilterByCrimeType($crimeType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select(
      $this->DB->raw('DISTINCT(month) AS month')
    )
    ->where('crime_type', '=', $crimeType)
    ->orderBy('month')
    ->get()
    );
  }

  public function getJusticeDepartmentFilterByCrimeType($crimeType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select(
      $this->DB->raw('DISTINCT(department) AS department')
    )
    ->where('crime_type', '=', $crimeType)
    ->get()
    );
  }

  public function getJusticeMunicipalityFilterByCrimeType($crimeType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select(
      $this->DB->raw('DISTINCT(municipality) AS municipality')
    )
    ->where('crime_type', '=', $crimeType)
    ->get()
    );
  }

  public function getJusticeAgeRangeFilterByCrimeType($crimeType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select(
      $this->DB->raw('DISTINCT(victim_age_range) AS victim_age_range')
    )
    ->where('crime_type', '=', $crimeType)
    ->get()
    );
  }

  public function getJusticeCrimeFilterByCrimeType($crimeType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select(
      $this->DB->raw('DISTINCT(crime) AS crime')
    )
    ->where('crime_type', '=', $crimeType)
    ->get()
    );
  }


  public function getCrimesByYearCrimeTypeAndStage($year = null, $yearFrom = null, $yearTo = null, $filters, $crimeType, $stage, $databaseConnectionName = null)
  {

    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select('month', $this->DB->raw('SUM(value) AS count'))
    ->where('crime_type', '=', $crimeType)
    ->where('stage', '=', $stage)
    ->groupBy('month')
    ->orderBy('month');
    
    if(!empty($year))
    {
      $query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $query->where('year', '<=', $yearTo);
    }

    if(!empty($filters['department']))
    {
      $query->whereIn('department', $filters['department']);
    }

    if(!empty($filters['crime']))
    {
      $query->whereIn('crime', $filters['crime']);
    }

    if(!empty($filters['municipality']))
    {
      $query->whereIn('municipality', $filters['municipality']);
    }

    if(!empty($filters['ageRange']))
    {
      $query->whereIn('victim_age_range', $filters['ageRange']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getCrimesByDepartmentYearCrimeTypeAndStage($year = null, $yearFrom = null, $yearTo = null, $filters, $crimeType, $stage, $databaseConnectionName = null)
  {

    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select('department', $this->DB->raw('SUM(value) AS count'))
    ->where('crime_type', '=', $crimeType)
    ->where('stage', '=', $stage)
    ->groupBy('department')
    ->orderBy('department');
    
    if(!empty($year))
    {
      $query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $query->where('year', '<=', $yearTo);
    }

    if(!empty($filters['month']))
    {
      $query->whereIn('month', $filters['month']);
    }
    
    if(!empty($filters['crime']))
    {
      $query->whereIn('crime', $filters['crime']);
    }

    if(!empty($filters['ageRange']))
    {
      $query->whereIn('victim_age_range', $filters['ageRange']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getCrimesByDepartmentResultYearCrimeTypeAndStage($year = null, $yearFrom = null, $yearTo = null, $filters, $crimeType, $stage, $databaseConnectionName = null)
  {

    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select('department', 'result', $this->DB->raw('SUM(value) AS count'))
    ->where('crime_type', '=', $crimeType)
    ->where('stage', '=', $stage)
    ->groupBy('department')
    ->groupBy('result')
    ->orderBy('department');
    
    if(!empty($year))
    {
      $query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $query->where('year', '<=', $yearTo);
    }

    if(!empty($filters['month']))
    {
      $query->whereIn('month', $filters['month']);
    }
    
    if(!empty($filters['crime']))
    {
      $query->whereIn('crime', $filters['crime']);
    }

    if(!empty($filters['ageRange']))
    {
      $query->whereIn('victim_age_range', $filters['ageRange']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getCrimesByMonthResultYearCrimeTypeAndStage($year = null, $yearFrom = null, $yearTo = null, $filters, $crimeType, $stage, $databaseConnectionName = null)
  {

    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select('month', 'result', $this->DB->raw('SUM(value) AS count'))
    ->where('crime_type', '=', $crimeType)
    ->where('stage', '=', $stage)
    ->groupBy('month')
    ->groupBy('result')
    ->orderBy('month');
    
    if(!empty($year))
    {
      $query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $query->where('year', '<=', $yearTo);
    }

    if(!empty($filters['month']))
    {
      $query->whereIn('month', $filters['month']);
    }
    
    if(!empty($filters['crime']))
    {
      $query->whereIn('crime', $filters['crime']);
    }

    if(!empty($filters['ageRange']))
    {
      $query->whereIn('victim_age_range', $filters['ageRange']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getCrimesByMunicipalityYearCrimeTypeAndStage($year = null, $yearFrom = null, $yearTo = null, $filters, $crimeType, $stage, $databaseConnectionName = null)
  {

    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select('department', 'municipality', $this->DB->raw('SUM(value) AS count'))
    ->where('crime_type', '=', $crimeType)
    ->where('stage', '=', $stage)
    ->groupBy('department')
    ->groupBy('municipality')
    ->orderBy('department');
    
    if(!empty($year))
    {
      $query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $query->where('year', '<=', $yearTo);
    }

    if(!empty($filters['month']))
    {
      $query->whereIn('month', $filters['month']);
    }
    
    if(!empty($filters['crime']))
    {
      $query->whereIn('crime', $filters['crime']);
    }

    if(!empty($filters['ageRange']))
    {
      $query->whereIn('victim_age_range', $filters['ageRange']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getStageValueSumByYearAndCrimeType($year = null, $yearFrom = null, $yearTo = null, $filters, $crimeType, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select('stage', $this->DB->raw('SUM(value) AS count'))
    ->where('crime_type', '=', $crimeType)
    ->groupBy('stage')
    ->orderBy($this->DB->raw(
      "CASE WHEN(
        stage = 'Sentencias')
        THEN 'Sentencias'
        WHEN(
        stage = 'Judicializadas')
        THEN 'Judicializadas'
        ELSE 'Ingresadas'
        END
        "
      ), 'DESC');

    if(!empty($year))
    {
      $query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $query->where('year', '<=', $yearTo);
    }

    if(!empty($filters['month']))
    {
      $query->whereIn('month', $filters['month']);
    }
    
    if(!empty($filters['department']))
    {
      $query->whereIn('department', $filters['department']);
    }

    if(!empty($filters['municipality']))
    {
      $query->whereIn('municipality', $filters['municipality']);
    }

    if(!empty($filters['ageRange']))
    {
      $query->whereIn('victim_age_range', $filters['ageRange']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getAnualComparisonClusteredByStageFilteredByCrimeType($yearFrom, $yearTo, $filters, $crimeType, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_JA_Crime')
    ->select('year', 'stage', $this->DB->raw('SUM(value) AS count'))
    ->where('crime_type', '=', $crimeType)
    ->groupBy('year')
    ->groupBy('stage');

    if(!empty($yearFrom))
    {
      $query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $query->where('year', '<=', $yearTo);
    }

    if(!empty($filters['department']))
    {
      $query->whereIn('department', $filters['department']);
    }

    if(!empty($filters['municipality']))
    {
      $query->whereIn('municipality', $filters['municipality']);
    }

    if(!empty($filters['ageRange']))
    {
      $query->whereIn('victim_age_range', $filters['ageRange']);
    }

    if(!empty($filters['crime']))
    {
      $query->whereIn('crime', $filters['crime']);
    }

    if(!empty($filters['month']))
    {
      $query->whereIn('month', $filters['month']);
    }

    $query->orderByRaw('year, stage');

    return new Collection(
      $query->get()
    );
  }

  /**
   * Mass create
   *
   * @param array of arrays
   *
   *
   * @return boolean
   */
  public function massCreate($data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->insert($data);

    return true;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\JusticeQuery $JusticeQuery
   *
   * @return boolean
   */
  public function update(array $data, $JusticeQuery = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($JusticeQuery))
    {
      $JusticeQuery = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      $JusticeQuery->$key = $value;
    }

    return $JusticeQuery->save();
  }

  /**
   * Update by column name
   *
   * @param int $columnNameOldValue
   * @param integer $organizationId
   * @param string $databaseConnectionName
   *
   * @return boolean
   */
  public function updateByColumnNameEloquent($columsToBeUpdated, $filteredValue, $organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->JusticeQuery->setConnection($databaseConnectionName)
      ->where('column_name', '=', $filteredValue)
      ->where('organization_id', '=', $organizationId)
      ->update($columsToBeUpdated);

    return true;
  }

  /**
   * Update by column name
   *
   * @param array $columsToBeUpdated
   * @param mixed $filteredValue
   * @param string $databaseConnectionName
   *
   * @return boolean
   */
  public function updateByCustomColumnQueryBuilder($columsToBeUpdated, $filteredValue, $organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->where('column_name', '=', $filteredValue)
      ->where('organization_id', '=', $organizationId)
      ->update($columsToBeUpdated);

    return true;
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $JusticeQuery = $this->byId($id, $databaseConnectionName);
      $JusticeQuery->delete();
    }

    return true;
  }

  /**
   * Mass detele
   *
   * @param integer $fileId
   *
   *
   * @return boolean
   */
  public function massDelete($ids, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->whereIn('id', '=', $ids)
      ->delete();

    return true;
  }

  /**
   * Mass detele by column
   *
   * @param integer $fileId
   *
   *
   * @return boolean
   */
  public function massDeleteByColumn($column, $value, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->where($column, '=', $value)
      ->delete();

    return true;
  }

}
