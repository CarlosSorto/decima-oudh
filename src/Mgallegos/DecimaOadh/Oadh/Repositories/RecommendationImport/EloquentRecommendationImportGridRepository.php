<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use Illuminate\Database\DatabaseManager;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use Illuminate\Translation\Translator;

class EloquentRecommendationImportGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_Recommendation_Temp AS rt');
			// ->leftJoin('MODULE_Table1 AS t1p', 't1.id', '=', 't1p.parent_id')
			// ->join('MODULE_Table2 AS t2', 't2.id', '=', 't1.table2_id')
			// ->where('ri.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'id AS oadh_rrt_id',
			'system AS oadh_rrt_system',
			'recommendation AS oadh_rrt_recommendation',
			'code AS oadh_rrt_code',
			'date AS oadh_rrt_date',
			'mechanism AS oadh_rrt_mechanism',
			'related_rights AS oadh_rrt_related_rights',
			'responsible_institutions AS oadh_rrt_responsible_institutions',
			'source AS oadh_rrt_source',
			'legal_base AS oadh_rrt_legal_base',
			'observations AS oadh_rrt_observations',
			'last_modified_date AS oadh_rrt_last_modified_date',
			'status AS oadh_rrt_status',
			'file_id AS oadh_rrt_file_id',
			// 'reference AS oadh_rrt_reference',
			// 'treaty AS oadh_rrt_treaty',
			// 'syncronized AS oadh_rrt_syncronized',
			// $DB->raw('CASE t1.field0 WHEN 1 THEN 0 ELSE 1 END AS oadh_rrt_field0'),
			// $DB->raw('CONCAT(\'#\', LPAD(t1.field0, 6, 0), \' \', t1.field1) AS oadh_rrt_field1'),
		);

		$this->orderBy = array(array('oadh_rrt_id', 'asc'));

		// $this->treeGrid = true;
		// $this->parentColumn = 'parent_id';
		// $this->leafColumn = 'is_leaf';
	}
}
