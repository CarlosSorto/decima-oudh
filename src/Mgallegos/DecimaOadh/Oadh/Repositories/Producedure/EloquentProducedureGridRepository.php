<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Producedure;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentProducedureGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_Producedure AS pro')
			->where('pro.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'pro.id AS oadh_producedure_id',
			'pro.place AS oadh_producedure_place',
			'pro.weapon AS oadh_producedure_weapon',
			'pro.hypothesis_fact AS oadh_producedure_hypothesis_fact',
			'pro.context AS oadh_producedure_context',
			'pro.accident_type AS oadh_producedure_accident_type',
			'pro.summary AS oadh_producedure_summary',
			'pro.news_id AS oadh_producedure_news_id'
		);

		$this->orderBy = array(array('pro.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'parent_id';

		// $this->leafColumn = 'is_leaf';
	}
}
