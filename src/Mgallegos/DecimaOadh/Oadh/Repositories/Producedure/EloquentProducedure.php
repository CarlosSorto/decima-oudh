<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Producedure;

use Illuminate\Database\Eloquent\Model;

use Mgallegos\DecimaOadh\Oadh\Producedure;

class EloquentProducedure implements ProducedureInterface {

  /**
   * Producedure
   *
   * @var Mgallegos\DecimaOadh\Oadh\Producedure;
   *
   */
  protected $Producedure;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(Model $Producedure, $databaseConnectionName)
  {
    $this->Producedure = $Producedure;

    $this->databaseConnectionName = $databaseConnectionName;

    $this->Producedure->setConnection($databaseConnectionName);
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->Producedure->getTable();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\Producedure
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->Producedure->on($databaseConnectionName)->find($id);
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Producedure->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function getMaxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Producedure->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Producedure = new Producedure();
    $Producedure->setConnection($databaseConnectionName);
    $Producedure->fill($data)->save();

    return $Producedure;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\Producedure $Producedure
   *
   * @return boolean
   */
  public function update(array $data, $Producedure = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($Producedure))
    {
      $Producedure = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      if(!empty($value))
      {
        $Producedure->$key = $value;
      }
      else
      {
        $Producedure->$key = null;
      }
    }

    return $Producedure->save();
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $Producedure = $this->byId($id, $databaseConnectionName);
      $Producedure->delete();
    }
    // $this->Account->destroy($data);

    return true;
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function massDelete($newsId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Producedure->setConnection($databaseConnectionName)
      ->where('news_id', '=', $newsId)
      ->delete();

    return true;
  }

  public function getProducedureByNewsId($newsId, $databaseConnectionName)
  {
    return $this->Producedure->setConnection($databaseConnectionName)
    ->where('news_id', '=', $newsId)->get();
  }
}
