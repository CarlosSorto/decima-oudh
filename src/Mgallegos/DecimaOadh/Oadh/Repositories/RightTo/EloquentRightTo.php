<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\RightTo;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\DatabaseManager;

use Illuminate\Database\Eloquent\Collection;

class EloquentRightTo implements RightToInterface {

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
  */

  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
  */

  protected $databaseConnectionName;

  public function __construct(DatabaseManager $DB, $databaseConnectionName)
  {
    $this->DB = $DB;

    $this->databaseConnectionName = $databaseConnectionName;
  }

  /**
   * Mass create from temp table
   *
   * @param string $table
   * @param integer $fileId
   *
   * @return boolean
   */
  public function massCreate($table, array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($table)
      ->insert($data);

    return true;
  }

  /**
   * Mass create from temp table
   *
   * @param string $table
   * @param integer $fileId
   * @param aray $data
   *
   * @return boolean
   */
  public function massUpdate($table, $fileId, array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($table)
      ->where('file_id', '=', $fileId)
      ->update($data);
      // ->update([ 'status' => 'P' ]);

    return true;
  }

  /**
   * Mass detele from temp table
   *
   * @param string $table
   * @param integer $fileId
   *
   * @return boolean
   */
  public function massDelete($table, $fileId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($table)
      ->where('file_id', '=', $fileId)
      ->delete();

    return true;
  }

  /**
   * Execute statement
   *
   * @param string $table
   * @param integer $fileId
   *
   * @return boolean
   */
  public function executeStatement($statement, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->DB->connection($databaseConnectionName)
      ->statement($statement);
  }

  /**
   * Execute statement
   *
   * @param string $table
   * @param integer $fileId
   *
   * @return boolean
   */
  public function countRequest($tableName, $fileId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->DB->connection($databaseConnectionName)
        ->table($tableName)
        ->where('file_id', '=', $fileId)
        ->count();
  }
}
