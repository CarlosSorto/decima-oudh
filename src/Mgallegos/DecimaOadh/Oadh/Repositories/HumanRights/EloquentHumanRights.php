<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Collection;

use Mgallegos\DecimaOadh\Oadh\HumanRights;

use Illuminate\Database\DatabaseManager;

class EloquentHumanRights implements HumanRightsInterface {

  /**
   * HumanRights
   *
   * @var Mgallegos\DecimaOadh\Oadh\HumanRights;
   *
   */
  protected $HumanRights;
  
    /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(Model $HumanRights, DatabaseManager $DB, $databaseConnectionName)
  {
    $this->HumanRights = $HumanRights;

    $this->DB = $DB;

    $this->databaseConnectionName = $databaseConnectionName;

    $this->HumanRights->setConnection($databaseConnectionName);
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->HumanRights->getTable();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\HumanRights
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->HumanRights->on($databaseConnectionName)->find($id);
  }

  public function getTopicByNewsId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_News_Human_Rights AS hr')
    ->leftJoin('OADH_Human_Right AS top', 'top.id', '=', 'hr.topic_id')
    ->where('hr.news_id', '=', $id)
    ->get(array(
      'top.*',
        )
      )
    );
  }

  public function getRightByNewsId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_News_Human_Rights AS hr')
    ->leftJoin('OADH_Human_Right AS r', 'r.id', '=', 'hr.right_id')
    ->where('hr.news_id', '=', $id)
    ->get(array(
      'r.*',
        )
      )
    );
  }

  public function getTracingRightAndViolatedFactByNewsId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_News_Human_Rights AS hr')
    ->leftJoin('OADH_Human_Right AS tra', 'tra.id', '=', 'hr.tracing_type_id')
    ->leftJoin('OADH_Human_Right AS r', 'r.id', '=', 'hr.right_id')
    ->leftJoin('OADH_Human_Right AS vio', 'vio.id', '=', 'hr.violated_fact_id')
    ->where('hr.news_id', '=', $id)
    ->get(array(
      'r.*',
      'tra.name as tracing_type',
      'vio.name as violated_fact',
        )
      )
    );
  }

  public function getAffectedPeopleByNewsId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_News_Human_Rights AS hr')
    ->where('hr.news_id', '=', $id)
    ->get(array(
      'hr.*',
        )
      )
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRights->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function getMaxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRights->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $HumanRights = new HumanRights();
    $HumanRights->setConnection($databaseConnectionName);
    $HumanRights->fill($data)->save();

    return $HumanRights;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\HumanRights $HumanRights
   *
   * @return boolean
   */
  public function update(array $data, $HumanRights = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($HumanRights))
    {
      $HumanRights = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      if(!empty($value) || $key == 'qualification')
      {
        $HumanRights->$key = $value;
      }
      else
      {
        $HumanRights->$key = null;
      }
    }

    return $HumanRights->save();
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $HumanRights = $this->byId($id, $databaseConnectionName);
      $HumanRights->delete();
    }
    // $this->Account->destroy($data);

    return true;
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function massDelete($newsId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRights->setConnection($databaseConnectionName)
      ->where('news_id', '=', $newsId)
      ->delete();

    return true;
  }

}
