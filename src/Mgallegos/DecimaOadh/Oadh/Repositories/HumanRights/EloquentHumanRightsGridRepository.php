<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentHumanRightsGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_News_Human_Rights AS hr')
			->leftJoin('OADH_Human_Right AS tra', 'tra.id', '=', 'hr.tracing_type_id')
			->leftJoin('OADH_Human_Right AS r', 'r.id', '=', 'hr.right_id')
			->leftJoin('OADH_Human_Right AS top', 'top.id', '=', 'hr.topic_id')
			->leftJoin('OADH_Human_Right AS vio', 'vio.id', '=', 'hr.violated_fact_id')
			->where('hr.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'hr.id AS oadh_human_right_id',
			'hr.news_id AS oadh_human_right_news_id',
			'hr.population_affected AS oadh_human_right_population_affected',
			'hr.qualification AS oadh_human_right_qualification',
			'hr.justification AS oadh_human_right_justification',
			'hr.tracing_type_id AS oadh_human_right_tracing_type_id',
			'tra.name AS oadh_human_right_tracing_type_label',
			'hr.right_id AS oadh_human_right_right_id',
			'r.name AS oadh_human_right_right_label',
			'hr.topic_id AS oadh_human_right_topic_id',
			'top.name AS oadh_human_right_topic_label',
			'hr.violated_fact_id AS oadh_human_right_violated_fact_id',
			'vio.name AS oadh_human_right_violated_fact_label'
		);

		$this->orderBy = array(array('hr.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'parent_id';

		// $this->leafColumn = 'is_leaf';
	}
}
