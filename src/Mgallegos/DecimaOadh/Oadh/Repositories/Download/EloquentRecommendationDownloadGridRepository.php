<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Download;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentRecommendationDownloadGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_CMS_Download AS dl')
			->join('OADH_Recommendation AS pb', 'dl.recommendation_id', '=', 'pb.id')
			->groupBy('pb.system', 'pb.recommendation')
			->where('dl.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

			$this->visibleColumns = array(
			'pb.system AS oadh_st_dl_type',
			'pb.recommendation AS oadh_st_dl_title',
			$DB->raw('COUNT(1) AS oadh_st_dl_downloads'),
		);

		$this->orderBy = array(array('oadh_st_dl_type', 'asc'));

		// $this->treeGrid = true;
		// $this->parentColumn = 'parent_id';
		// $this->leafColumn = 'is_leaf';
	}
}
