<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Download;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use Illuminate\Database\DatabaseManager;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use Illuminate\Translation\Translator;

class EloquentNewsDownloadGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_CMS_Download AS dl')
			->join('OADH_News AS pb', 'dl.news_id', '=', 'pb.id')
			->join('OADH_Digital_Media AS dm', 'dm.id', '=', 'pb.digital_media_id')
			->groupBy('dm.name', 'pb.title')
			->where('dl.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

			$this->visibleColumns = array(
			'dm.name AS oadh_st_dl_type',
			'pb.title AS oadh_st_dl_title',
			$DB->raw('COUNT(1) AS oadh_st_dl_downloads'),
			// $DB->raw('CASE dl.field0 WHEN 1 THEN 0 ELSE 1 END AS oadh_st_field0'),
			// $DB->raw('CONCAT(\'#\', LPAD(dl.field0, 6, 0), \' \', dl.field1) AS oadh_st_field1'),
		);

		$this->orderBy = array(array('oadh_st_dl_type', 'asc'));

		// $this->treeGrid = true;
		// $this->parentColumn = 'parent_id';
		// $this->leafColumn = 'is_leaf';
	}
}
