<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Reparation;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentReparationFisdlBeneficiariesTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_CR_Fisdl_Beneficiaries_Temp AS jcm')
			->where('jcm.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'jcm.id AS oadh_reparation_fb_id',
			'jcm.year AS oadh_reparation_fb_year',
			'jcm.department AS oadh_reparation_fb_department',
			'jcm.men_total AS oadh_reparation_fb_men_total',
			'jcm.women_total AS oadh_reparation_fb_women_total',
			'jcm.status AS oadh_reparation_fb_status',
			'jcm.file_id AS oadh_reparation_fb_file_id'
		);

		$this->orderBy = array(array('jcm.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'jcm.file_id';

		// $this->leafColumn = 'oadh_reparation_fb_is_leaf';
	}
}
