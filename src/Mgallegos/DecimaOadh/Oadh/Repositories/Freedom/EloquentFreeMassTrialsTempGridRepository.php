<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeMassTrialsTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Mass_Trials_Temp AS mt')
			->where('mt.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'mt.id AS oadh_mt_id',
			'mt.year AS oadh_mt_year',
			'mt.sex AS oadh_mt_sex',
			'mt.court AS oadh_mt_court',
			'mt.status AS oadh_mt_status',
			'mt.file_id AS oadh_mt_file_id'
		);

		$this->orderBy = array(array('mt.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'mt.parent_id';

		// $this->leafColumn = 'oadh_mt_is_leaf';
	}
}
