<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeChronicDiseasesGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Chronic_Diseases AS cd')
			->where('cd.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'cd.id AS oadh_cd_id',
			'cd.chronic_disease AS oadh_cd_chronic_disease',
			'cd.sex AS oadh_cd_sex',
			'cd.age AS oadh_cd_age',
			'cd.jail AS oadh_cd_jail',
			'cd.year AS oadh_cd_year',
			'cd.file_id AS oadh_cd_file_id'
		);

		$this->orderBy = array(array('cd.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'cd.parent_id';

		// $this->leafColumn = 'oadh_cd_is_leaf';
	}
}
