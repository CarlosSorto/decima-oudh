<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeForcedDisappearancesTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Forced_Disappearances_Temp AS fd')
			->where('fd.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'fd.id AS oadh_fd_id',
			'fd.year AS oadh_fd_year',
			'fd.gender AS oadh_fd_gender',
			'fd.departament AS oadh_fd_departament',
			'fd.municipality AS oadh_fd_municipality',
			'fd.profession AS oadh_fd_profession',
			'fd.crime AS oadh_fd_crime',
			'fd.status AS oadh_fd_status',
			'fd.file_id AS oadh_fd_file_id'
		);

		$this->orderBy = array(array('fd.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'fd.parent_id';

		// $this->leafColumn = 'oadh_fd_is_leaf';
	}
}
