<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeCaptureOrdersTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Capture_Orders_Temp AS co')
			->where('co.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'co.id AS oadh_co_id',
			'co.departament AS oadh_co_departament',
			'co.municipality AS oadh_co_municipality',
			'co.year AS oadh_co_year',
			'co.status AS oadh_co_status',
			'co.file_id AS oadh_co_file_id'
		);

		$this->orderBy = array(array('co.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'co.parent_id';

		// $this->leafColumn = 'oadh_co_is_leaf';
	}
}
