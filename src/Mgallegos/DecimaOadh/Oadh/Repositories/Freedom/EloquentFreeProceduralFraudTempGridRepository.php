<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeProceduralFraudTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Procedural_Fraud_Temp AS pf')
			->where('pf.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'pf.id AS oadh_pf_id',
			'pf.gender AS oadh_pf_gender',
			'pf.crime AS oadh_pf_crime',
			'pf.year AS oadh_pf_year',
			'pf.departament AS oadh_pf_departament',
			'pf.municipality AS oadh_pf_municipality',
			'pf.status AS oadh_pf_status',
			'pf.file_id AS oadh_pf_file_id'
		);

		$this->orderBy = array(array('pf.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'pf.parent_id';

		// $this->leafColumn = 'oadh_pf_is_leaf';
	}
}
