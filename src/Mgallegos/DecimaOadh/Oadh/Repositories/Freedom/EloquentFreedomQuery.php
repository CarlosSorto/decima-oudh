<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use Illuminate\Database\DatabaseManager;

use Illuminate\Database\Eloquent\Collection;

class EloquentFreedomQuery implements FreedomQueryInterface {

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(DatabaseManager $DB, $databaseConnectionName)
  {
    $this->DB = $DB;

    $this->databaseConnectionName = $databaseConnectionName;
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->EloquentFreedomQuery->getTable();
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function habeasCorpusCourtFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Habeas_Corpus_Request AS hcr')
        ->select(
          $this->DB->raw('DISTINCT(court) AS court')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function habeasCorpusSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Habeas_Corpus_Request AS hcr')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function habeasCorpusByCourtBySexByStatus($court = null, $sex = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Habeas_Corpus_Request AS hcr')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($court))
    {
      $Query->whereIn('court', $court);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get state filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function peopleDetainedStatusFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_People_Detained AS pd')
        ->select(
          $this->DB->raw('DISTINCT(state) AS state')
        )
        ->get()
    );
  }

  /**
   * Get department filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function peopleDetainedDepartmentFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_People_Detained AS pd')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $court
   *
   * @return integer
  */

  public function peopleDetainedByStateByDepartmentByStatus ($state = null, $department = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_People_Detained AS pd')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($state))
    {
      $Query->whereIn('state', $state);
    }

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get state filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function criminalCasesTrialsPhaseFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Criminal_Cases_Trials AS cct')
        ->select(
          $this->DB->raw('DISTINCT(phase) AS phase')
        )
        ->get()
    );
  }

  /**
   * Get department filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function criminalCasesTrialsCourtFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Criminal_Cases_Trials AS cct')
        ->select(
          $this->DB->raw('DISTINCT(court) AS court')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $court
   *
   * @return integer
  */

  public function criminalCasesTrailsByPhaseByCourtByStatus ($court = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Criminal_Cases_Trials AS cct')
      ->select(
        'phase',
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year', 'phase')
      ->orderBy('year')
      ->orderBy('phase');

    if(!empty($court))
    {
      $Query->whereIn('court', $court);
    }

    return new Collection (
      $Query->get()
    );
  }

   /**
   * Get state filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function massTrialsSexFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Mass_Trials AS mt')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Get department filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function massTrialsCourtFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Mass_Trials AS mt')
        ->select(
          $this->DB->raw('DISTINCT(court) AS court')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $court
   *
   * @return integer
  */

  public function massTrialsBySexByCourtByStatus ($sex = null, $court = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Mass_Trials AS mt')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($court))
    {
      $Query->whereIn('court', $court);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get gender filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function proceduralFraudGenderFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Procedural_Fraud AS pf')
        ->select(
          $this->DB->raw('DISTINCT(gender) AS gender')
        )
        ->get()
    );
  }

  /**
   * Get gender filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function proceduralFraudCrimeFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Procedural_Fraud AS pf')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

  /**
   * Get gender filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function proceduralFraudMunicipalityFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Procedural_Fraud AS pf')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Get department filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function proceduralFraudDepartmentFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Procedural_Fraud AS pf')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $court
   *
   * @return integer
  */

  public function proceduralFraudByGenderByCrimeByDepartmentByMunicipalityByStatus ($gender = null, $department = null, $crime = null, $municipality = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Procedural_Fraud AS pf')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($gender))
    {
      $Query->whereIn('gender', $gender);
    }

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    return new Collection (
      $Query->get()
    );
  }

   /**
   * Get municipality filter
   *
   * @param array $municipality
   *
   * @return integer
  */

  public function captureOrdersMunicipalityFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Capture_Orders AS co')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $department
   *
   * @return integer
  */

  public function captureOrdersDepartmentFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Capture_Orders AS co')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function captureOrdersByDepartmentByMunicipalityByStatus($department = null, $municipality = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Capture_Orders AS co')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get court filter
   *
   * @param array $department
   *
   * @return integer
  */

  public function disappearancesVictimsDepartmentFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Disappearances_Victims AS dv')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $sex
   *
   * @return integer
  */

  public function disappearancesVictimsMunicipalityFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Disappearances_Victims AS dv')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function disappearancesVictimsByDepartmentByMunicipalityByStatus ($department = null, $municipality = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Disappearances_Victims AS dv')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }


  /**
   * Get court filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function accusedLibertyDeprivationDepartmentFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Accused_Liberty_Deprivation AS ald')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $sex
   *
   * @return integer
  */

  public function accusedLibertyDeprivationMunicipalityFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Accused_Liberty_Deprivation AS ald')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $sex
   *
   * @return integer
  */

  public function accusedLibertyDeprivationProfessionFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Accused_Liberty_Deprivation AS ald')
        ->select(
          $this->DB->raw('DISTINCT(profession) AS profession')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $sex
   *
   * @return integer
  */

  public function accusedLibertyDeprivationGenderFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Accused_Liberty_Deprivation AS ald')
        ->select(
          $this->DB->raw('DISTINCT(gender) AS gender')
        )
        ->get()
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function accusedLibertyDeprivationByDepartmentByMunicipalityByProfessionByGenderByStatus ($department = null, $municipality = null, $profession = null, $gender = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Accused_Liberty_Deprivation AS dv')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($profession))
    {
      $Query->whereIn('profession', $profession);
    }

    if(!empty($gender))
    {
      $Query->whereIn('gender', $gender);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get court filter
   *
   * @param array $court
   *
   * @return integer
  */

  public function victimsLibertyDeprivationDepartmentFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Victims_Liberty_Deprivation AS vld')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $sex
   *
   * @return integer
  */

  public function victimsLibertyDeprivationMunicipalityFilter ($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Victims_Liberty_Deprivation AS vld')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function victimsLibertyDeprivationByDepartmentByMunicipalityByStatus($department = null, $municipality = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Victims_Liberty_Deprivation AS dv')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function forcedDisappearancesGenderFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Forced_Disappearances AS fd')
        ->select(
          $this->DB->raw('DISTINCT(gender) AS gender')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function forcedDisappearancesDepartmentFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Forced_Disappearances AS fd')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function forcedDisappearancesMunicipalityFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Forced_Disappearances AS fd')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function forcedDisappearancesProfessionFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Forced_Disappearances AS fd')
        ->select(
          $this->DB->raw('DISTINCT(profession) AS profession')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function forcedDisappearancesCrimeFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Forced_Disappearances AS fd')
        ->select(
          $this->DB->raw('DISTINCT(crime) AS crime')
        )
        ->get()
    );
  }

   /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function forcedDisappearancesByDepartmentByMunicipalityByProfessionByGenderByCrimeByStatus ($department = null, $municipality = null, $profession = null, $gender = null, $crime = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Forced_Disappearances AS fd')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($profession))
    {
      $Query->whereIn('profession', $profession);
    }

    if(!empty($gender))
    {
      $Query->whereIn('gender', $gender);
    }

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function jailsOccupationDepartmentFilter($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Jails_Occupation AS jo')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function jailsOccupationStationFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Jails_Occupation AS jo')
        ->select(
          $this->DB->raw('DISTINCT(station) AS station')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function jailsOccupationUnitFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Jails_Occupation AS jo')
        ->select(
          $this->DB->raw('DISTINCT(unit) AS unit')
        )
        ->get()
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function jailsOccupationByDepartmentByStationByUnitByStatus ($department = null, $station = null, $unit = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Jails_Occupation AS jo')
      ->select(
        'year',
        $this->DB->raw('SUM(women_population) as women_count'),
        $this->DB->raw('SUM(man_population) as man_count')
        // $this->DB->raw('SUM(women_population) + SUM(man_population) as count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    if(!empty($station))
    {
      $Query->whereIn('station', $station);
    }

    if(!empty($unit))
    {
      $Query->whereIn('unit', $unit);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function deprivedPersonsRightFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Deprived_Persons AS ofdp')
        ->select(
          $this->DB->raw('DISTINCT("right_to") AS "right"')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function deprivedPersonsViolatedFactFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Deprived_Persons AS ofdp')
        ->select(
          $this->DB->raw('DISTINCT("violated_fact") AS "violatedFact"')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function deprivedPersonsGenderFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Deprived_Persons AS ofdp')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS gender')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function deprivedPersonsDepartmentFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Deprived_Persons AS ofdp')
        ->select(
          $this->DB->raw('DISTINCT(departament) AS department')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function deprivedPersonsMunicipalityFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Deprived_Persons AS ofdp')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function deprivedPersonsByRightByViolatedFactByGenderByDepartmentByMunicipalityStatus ($right = null, $violatedFact = null, $gender = null, $department = null, $municipality = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Deprived_Persons AS ofdp')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($right))
    {
      $Query->whereIn('right_to', $right);
    }

    if(!empty($violatedFact))
    {
      $Query->whereIn('violated_fact', $violatedFact);
    }

    if(!empty($gender))
    {
      $Query->whereIn('sex', $gender);
    }

    if(!empty($department))
    {
      $Query->whereIn('departament', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function acuteDiseasesDiseaseFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Acute_Diseases AS ad')
        ->select(
          $this->DB->raw('DISTINCT(disease) AS disease')
        )
        ->get()
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function acuteDiseasesByDiseaseByStatus ($disease = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Acute_Diseases AS ad')
      ->select(
        'year',
        $this->DB->raw('SUM(quantity) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($disease))
    {
      $Query->whereIn('disease', $disease);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $chronic_disease
   *
   * @return integer
  */

  public function chronicDiseasesChronicDiseaseFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Chronic_Diseases AS cd')
        ->select(
          $this->DB->raw('DISTINCT(chronic_disease) AS chronicDisease')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function chronicDiseasesGenderFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Chronic_Diseases AS cd')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS gender')
        )
        ->get()
    );
  }

  /**
   * Get max number
   *
   * @param array $gender
   *
   * @return integer
  */

  public function chronicDiseasesJailFilter ($databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_FREE_Chronic_Diseases AS cd')
        ->select(
          $this->DB->raw('DISTINCT(jail) AS jail')
        )
        ->get()
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
  */
  public function chronicDiseasesByChronicDiseaseByGenderByJailByStatus ($chronicDisease = null, $gender = null, $jail = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_FREE_Chronic_Diseases AS cd')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($chronicDisease))
    {
      $Query->whereIn('chronic_disease', $chronicDisease);
    }

    if(!empty($gender))
    {
      $Query->whereIn('sex', $gender);
    }

    if(!empty($jail))
    {
      $Query->whereIn('jail', $jail);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }
}
