<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeDeprivedPersonsGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Deprived_Persons AS dp')
			->where('dp.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'dp.id AS oadh_dp_id',
			'dp.year AS oadh_dp_year',
			'dp.right_to AS oadh_dp_right_to',
			'dp.violated_fact AS oadh_dp_violated_fact',
			'dp.sex AS oadh_dp_sex',
			'dp.age AS oadh_dp_age',
			'dp.departament AS oadh_dp_departament',
			'dp.municipality AS oadh_dp_municipality',
			'dp.file_id AS oadh_dp_file_id'
		);

		$this->orderBy = array(array('dp.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'dp.parent_id';

		// $this->leafColumn = 'oadh_dp_is_leaf';
	}
}
