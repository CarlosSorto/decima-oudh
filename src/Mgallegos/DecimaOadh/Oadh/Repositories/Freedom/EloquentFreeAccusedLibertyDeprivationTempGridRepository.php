<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeAccusedLibertyDeprivationTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Accused_Liberty_Deprivation_Temp AS ald')
			->where('ald.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'ald.id AS oadh_ald_id',
			'ald.year AS oadh_ald_year',
			'ald.departament AS oadh_ald_departament',
			'ald.municipality AS oadh_ald_municipality',
			'ald.profession AS oadh_ald_profession',
			'ald.gender AS oadh_ald_gender',
			'ald.status AS oadh_ald_status',
			'ald.file_id AS oadh_ald_file_id'
		);

		$this->orderBy = array(array('ald.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ald.parent_id';

		// $this->leafColumn = 'oadh_ald_is_leaf';
	}
}
