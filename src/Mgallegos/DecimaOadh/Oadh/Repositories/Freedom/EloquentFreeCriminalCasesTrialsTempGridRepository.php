<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeCriminalCasesTrialsTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Criminal_Cases_Trials_Temp AS cct')
			->where('cct.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'cct.id AS oadh_cct_id',
			'cct.year AS oadh_cct_year',
			'cct.phase AS oadh_cct_phase',
			'cct.court AS oadh_cct_court',
			'cct.status AS oadh_cct_status',
			'cct.file_id AS oadh_cct_file_id'
		);

		$this->orderBy = array(array('cct.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'cct.parent_id';

		// $this->leafColumn = 'oadh_cct_is_leaf';
	}
}
