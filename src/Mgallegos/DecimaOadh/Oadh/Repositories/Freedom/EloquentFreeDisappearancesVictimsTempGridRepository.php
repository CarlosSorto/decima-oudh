<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeDisappearancesVictimsTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Disappearances_Victims_Temp AS dv')
			->where('dv.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'dv.id AS oadh_dv_id',
			'dv.departament AS oadh_dv_departament',
			'dv.municipality AS oadh_dv_municipality',
			'dv.year AS oadh_dv_year',
			'dv.file_id AS oadh_dv_file_id'
		);

		$this->orderBy = array(array('dv.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'dv.parent_id';

		// $this->leafColumn = 'oadh_dv_is_leaf';
	}
}
