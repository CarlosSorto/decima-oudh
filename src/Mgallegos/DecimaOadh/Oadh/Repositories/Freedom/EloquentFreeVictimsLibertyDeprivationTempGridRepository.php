<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeVictimsLibertyDeprivationTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Victims_Liberty_Deprivation_Temp AS vld')
			->where('vld.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'vld.id AS oadh_vld_id',
			'vld.departament AS oadh_vld_departament',
			'vld.municipality AS oadh_vld_municipality',
			'vld.year AS oadh_vld_year',
			'vld.status AS oadh_vld_status',
			'vld.file_id AS oadh_vld_file_id'
		);

		$this->orderBy = array(array('vld.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'vld.parent_id';

		// $this->leafColumn = 'oadh_vld_is_leaf';
	}
}
