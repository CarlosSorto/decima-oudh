<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeAcuteDiseasesTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Acute_Diseases_Temp AS ad')
			->where('ad.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'ad.id AS oadh_ad_id',
			'ad.year AS oadh_ad_year',
			'ad.disease AS oadh_ad_disease',
			'ad.quantity AS oadh_ad_quantity',
			'ad.status AS oadh_ad_status',
			'ad.file_id AS oadh_ad_file_id'
		);

		$this->orderBy = array(array('ad.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ad.parent_id';

		// $this->leafColumn = 'oadh_ad_is_leaf';
	}
}
