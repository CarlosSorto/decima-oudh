<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeJailsOccupationGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Jails_Occupation AS jo')
			->where('jo.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'jo.id AS oadh_jo_id',
			'jo.departament AS oadh_jo_departament',
			'jo.station AS oadh_jo_station',
			'jo.unit AS oadh_jo_unit',
			'jo.year AS oadh_jo_year',
			'jo.capacity AS oadh_jo_capacity',
			'jo.man_population AS oadh_jo_man_population',
			'jo.women_population AS oadh_jo_women_population',
			'jo.file_id AS oadh_jo_file_id'
		);

		$this->orderBy = array(array('jo.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'jo.parent_id';

		// $this->leafColumn = 'oadh_jo_is_leaf';
	}
}
