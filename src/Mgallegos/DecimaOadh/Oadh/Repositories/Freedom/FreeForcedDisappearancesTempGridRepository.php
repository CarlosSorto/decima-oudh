<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\FreeForcedDisappearancesTemp;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreeForcedDisappearancesTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_Habeas_Corpus_Request AS hcr')
			->where('hcr.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'hcr.id AS oadh_hcr_id',
			'hcr.year AS oadh_hcr_year',
			'hcr.sex AS oadh_hcr_sex',
			'hcr.court AS oadh_hcr_court',
			'hcr.file_id AS oadh_hcr_file_id'
		);

		$this->orderBy = array(array('hcr.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'hcr.parent_id';

		// $this->leafColumn = 'oadh_hcr_is_leaf';
	}
}
