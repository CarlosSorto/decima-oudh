<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Freedom;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFreePeopleDetainedGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_FREE_People_Detained AS pd')
			->where('pd.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'pd.id AS oadh_pd_id',
      'pd.state AS oadh_pd_state',
			'pd.year AS oadh_pd_year',
			'pd.departament AS oadh_pd_departament',
			'pd.file_id AS oadh_pd_file_id'
		);

		$this->orderBy = array(array('pd.id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'hcr.parent_id';

		// $this->leafColumn = 'oadh_pd_is_leaf';
	}
}
