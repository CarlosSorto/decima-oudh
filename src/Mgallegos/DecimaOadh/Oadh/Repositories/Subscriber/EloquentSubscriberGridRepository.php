<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Subscriber;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentSubscriberGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_CMS_Subscription AS st')
			// ->leftJoin('MODULE_Table1 AS sp', 's.id', '=', 'sp.parent_id')
			// ->join('MODULE_Table2 AS t2', 't2.id', '=', 's.table2_id')
			->groupBy($DB->raw('DATE(st.datetime)'))
			->where('st.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			// 'st.id AS oadh_st_id',
      // 'st.email AS oadh_st_email',
      // 'st.datetime AS oadh_st_datetime',
			$DB->raw('DATE(st.datetime) AS oadh_st_datetime'),
			$DB->raw('COUNT(1) AS oadh_st_count'),
			// $DB->raw('CASE s.field0 WHEN 1 THEN 0 ELSE 1 END AS module_app_field0'),
			// $DB->raw('CONCAT(\'#\', LPAD(s.field0, 6, 0), \' \', s.field1) AS module_app_field1'),
		);

		$this->orderBy = array(array('oadh_st_datetime', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'parent_id';

		// $this->leafColumn = 'is_leaf';
	}
}
