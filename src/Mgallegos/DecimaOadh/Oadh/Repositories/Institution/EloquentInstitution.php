<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyInstitution by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYInstitution and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Institution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Collection;
use Mgallegos\DecimaOadh\Oadh\Institution;

class EloquentInstitution implements InstitutionInterface {

  /**
   * Institution
   *
   * @var Mgallegos\DecimaOadh\Oadh\Institution;
   *
   */
  protected $Institution;

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(Model $Institution, DatabaseManager $DB, $databaseConnectionName)
  {
    $this->Institution = $Institution;
    $this->DB = $DB;
    $this->databaseConnectionName = $databaseConnectionName;
    $this->Institution->setConnection($databaseConnectionName);
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->Institution->getTable();
  }

  /**
   * Get search modal table rows
   *
   * @param int $organizationId
   *
   * @return Collection
   */
  public function searchModalTableRows($id = null, $organizationId, $count = false, $limit = null, $offset = null, $filter = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_Institution AS t0')
      // ->join('Table_Name1 AS t1', 't1.column_name', '=', 't0.column_name')
      // ->orderBy('t1.column_name0', 'desc')
      // ->orderBy('t1.column_name1', 'asc')
      // ->whereIn('t1.id', $ids)
      ->where('t0.organization_id', '=', $organizationId);

    if(!empty($id))
    {
      $query->where('t0.id', '=', $id);
    }

    if(!empty($filter))
    {
      $query->where(function($dbQuery) use ($filter)
      {
        foreach (array('name', 'abbreviation') as $key => $value)
        {
          $dbQuery->orWhere($value, 'like', '%' . str_replace(' ', '%', $filter) . '%');
        }
      });
    }

    if($count)
    {
      return $query->count();
    }

    if(!empty($limit))
    {
      $query->take($limit);
    }

    if(!empty($offset) && $offset != 0)
    {
      $query->skip($offset);
    }

    return new Collection(
      $query->get(
        array(
          't0.*'
        )
      )
    );
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\Institution
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->Institution->on($databaseConnectionName)->find($id);
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byIds($ids, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
        ->table('Table_Name0 AS t0')
        ->join('Table_Name1 AS t1', 't1.column_name', '=', 't0.column_name')
        // ->where('p.id', '=', $ids)
        ->whereIn('t1.id', $ids)
        // ->orderBy('t1.column_name0', 'desc')
        // ->orderBy('t1.column_name1', 'asc')
        ->get(array('t0.*'))
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Institution->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function getMaxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Institution->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Institution = new Institution();
    $Institution->setConnection($databaseConnectionName);
    $Institution->fill($data)->save();

    return $Institution;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\Institution $Institution
   *
   * @return boolean
   */
  public function update(array $data, $Institution = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($Institution))
    {
      $Institution = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      $Institution->$key = $value;
    }

    return $Institution->save();
  }

  /**
   * Update by column name
   *
   * @param int $columnNameOldValue
   * @param integer $organizationId
   * @param string $databaseConnectionName
   *
   * @return boolean
   */
  public function updateByColumnNameEloquent($columnNameOldValue, $organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->Institution->setConnection($databaseConnectionName)
      ->where('column_name', '=', $columnNameOldValue)
      ->where('organization_id', '=', $organizationId)
      ->update(array('column_name_to_be_updated' => $newValue));

    return true;
  }

  /**
   * Update by column name
   *
   * @param int $columnNameOldValue
   * @param integer $organizationId
   * @param string $databaseConnectionName
   *
   * @return boolean
   */
  public function updateByColumnNameQueryBuilder($columnNameOldValue, $organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->where('column_name', '=', $columnNameOldValue)
      ->where('organization_id', '=', $organizationId)
      ->update(array('column_name_to_be_updated' => $newValue));

    return true;
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $Institution = $this->byId($id, $databaseConnectionName);
      $Institution->delete();
    }

    return true;
  }

  /**
   * Mass detele
   *
   * @param integer $fileId
   *
   *
   * @return boolean
   */
  public function massDelete($ids, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->whereIn('id', '=', $ids)
      ->delete();

    return true;
  }

  /**
   * Mass detele by column
   *
   * @param integer $fileId
   *
   *
   * @return boolean
   */
  public function massDeleteByColumn($column, $value, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->where($column, '=', $value)
      ->delete();

    return true;
  }

}
