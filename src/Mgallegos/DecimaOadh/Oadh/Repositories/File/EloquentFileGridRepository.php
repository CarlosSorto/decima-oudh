<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\File;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentFileGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('FILE_File AS f')
			->where('f.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

			$this->visibleColumns = array(
				'f.id AS file_id',
				'f.system_route AS file_system_route',
				'f.name AS file_name',
				'f.url_html AS file_url_html',
				'f.icon_html AS file_icon_html',
				'f.is_public AS file_is_public',
			);

		$this->orderBy = array(array('f.id', 'desc'));
	}
}
