<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Setting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\DatabaseManager;
use Mgallegos\DecimaSale\Sale\Setting;
use Illuminate\Database\Eloquent\Collection;

class EloquentSetting implements SettingInterface {

  /**
   * Setting
   *
   * @var Mgallegos\DecimaSale\Sale\Setting;
   *
   */
  protected $Setting;

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(Model $Setting, DatabaseManager $DB, $databaseConnectionName)
  {
    $this->Setting = $Setting;

    $this->DB = $DB;

    $this->databaseConnectionName = $databaseConnectionName;

    $this->Setting->setConnection($databaseConnectionName);
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->Setting->getTable();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaSale\Sale\Setting
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->Setting->on($databaseConnectionName)->find($id);
  }

  /**
   * Retrieve setting by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function getSettings($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Setting->setConnection($databaseConnectionName)->get();
  }

  /**
   * Retrieve setting by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Setting->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  /**
   * Get key-values
   *
   * @param  int $id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function keyValues($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_CMS_Key_Value AS kv')
        // ->where('p.id', '=', $ids)
        // ->whereIn('t1.id', $ids)
        // ->orderBy('t1.column_name0', 'desc')
        // ->orderBy('t1.column_name1', 'asc')
        ->get(array('kv.*'))
    );
  }

  /**
   * Retrieve ... by email and by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byEmailAndByOrganization($email, $id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Setting->setConnection($databaseConnectionName)->where('email', '=', $email)->where('organization_id', '=', $id)->get();
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Setting = new Setting();
    $Setting->setConnection($databaseConnectionName);
    $Setting->fill($data)->save();

    return $Setting;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaSale\Sale\Setting $Setting
   *
   * @return boolean
   */
  public function update(array $data, $Setting = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($Setting))
    {
      $Setting = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      if(!empty($value))
      {
        $Setting->$key = $value;
      }
      else
      {
        $Setting->$key = null;
      }
    }

    return $Setting->save();
  }

  /**
   * Update key value
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaSale\Sale\Setting $Setting
   *
   * @return boolean
   */
  public function updateKeyValue($key, $value, $lang)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->table('OADH_CMS_Key_Value AS ckv')
      ->where('key', '=', $key)
      ->update(array($lang . '_value' => $value));

    return true;
  }

  /**
   * Update key value
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaSale\Sale\Setting $Setting
   *
   * @return boolean
   */
  public function createSubscription(Array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->table('OADH_CMS_Subscription AS cs')
      ->insert($data);

    return true;
  }


  /**
   * Update key value
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaSale\Sale\Setting $Setting
   *
   * @return boolean
   */
  public function createAccess(Array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->table('OADH_CMS_Access AS acc')
      ->insert($data);

    return true;
  }

  /**
   * Update key value
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaSale\Sale\Setting $Setting
   *
   * @return boolean
   */
  public function createDownload(Array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->table('OADH_CMS_Download AS cd')
      ->insert($data);

    return true;
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data)
  {
    foreach ($data as $key => $id)
    {
      $Setting = $this->byId($id);
      $Setting->delete();
    }
    // $this->Account->destroy($data);

    return true;
  }

  /**
   * Change database connection
   *
   * @param string $databaseConnectionName
   *
   * @return void
   */
  public function changeDatabaseConnection($databaseConnectionName)
  {
    $this->databaseConnectionName = $databaseConnectionName;

    $this->Setting->setConnection($databaseConnectionName);
  }

}
