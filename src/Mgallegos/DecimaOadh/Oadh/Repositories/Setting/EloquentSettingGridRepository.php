<?php

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Setting;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentSettingGridRepository extends EloquentRepositoryAbstract
{
  public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
  {
    // $this->DB = $DB;
    // $this->DB->connection()->enableQueryLog();

    $this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
      ->table('OADH_CMS_Subscription AS s')
      ->where('s.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

    $this->visibleColumns = array(
      's.id AS oadh_s_id',
      's.email AS oadh_s_email',
      's.datetime AS oadh_s_datetime',
      // $DB->raw('CASE t1.field0 WHEN 1 THEN 0 ELSE 1 END AS module_app_field0'),
      // $DB->raw('CONCAT(\'#\', LPAD(t1.field0, 6, 0), \' \', t1.field1) AS module_app_field1'),
    );

    $this->orderBy = array(array('oadh_s_id', 'asc'));

    // $this->treeGrid = true;

    // $this->parentColumn = 'parent_id';

    // $this->leafColumn = 'is_leaf';
  }
}
