<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use Illuminate\Database\DatabaseManager;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use Illuminate\Translation\Translator;

class EloquentRecommendationRightGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_Recommendation_Right AS t0')
			// ->leftJoin('MODULE_Table1 AS t1p', 't1.id', '=', 't1p.parent_id')
			->join('OADH_Right AS t1', 't1.id', '=', 't0.right_id')
			->where('t0.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			// 't0.id AS oadh_rrm_id',
			't0.id AS oadh_rrm_rig_detail_id',
			't1.name AS oadh_rrm_rig_detail_name',
			// $DB->raw('CASE t1.field0 WHEN 1 THEN 0 ELSE 1 END AS module_app_field0'),
			// $DB->raw('CONCAT(\'#\', LPAD(t1.field0, 6, 0), \' \', t1.field1) AS module_app_field1'),
		);

		$this->orderBy = array(array('oadh_rrm_rig_detail_name', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'parent_id';

		// $this->leafColumn = 'is_leaf';
	}
}
