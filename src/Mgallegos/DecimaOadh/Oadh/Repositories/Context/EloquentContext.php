<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Context;

use Illuminate\Database\Eloquent\Model;

use Mgallegos\DecimaOadh\Oadh\Context;

class EloquentContext implements ContextInterface {

  /**
   * Context
   *
   * @var Mgallegos\DecimaOadh\Oadh\Context;
   *
   */
  protected $Context;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(Model $Context, $databaseConnectionName)
  {
    $this->Context = $Context;

    $this->databaseConnectionName = $databaseConnectionName;

    $this->Context->setConnection($databaseConnectionName);
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->Context->getTable();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\Context
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->Context->on($databaseConnectionName)->find($id);
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Context->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function getMaxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Context->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Context = new Context();
    $Context->setConnection($databaseConnectionName);
    $Context->fill($data)->save();

    return $Context;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\Context $Context
   *
   * @return boolean
   */
  public function update(array $data, $Context = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($Context))
    {
      $Context = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      if(!empty($value))
      {
        $Context->$key = $value;
      }
      else
      {
        $Context->$key = null;
      }
    }

    return $Context->save();
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $Context = $this->byId($id, $databaseConnectionName);
      $Context->delete();
    }
    // $this->Account->destroy($data);

    return true;
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function massDelete($newsId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Context->setConnection($databaseConnectionName)
      ->where('news_id', '=', $newsId)
      ->delete();

    return true;
  }

}
