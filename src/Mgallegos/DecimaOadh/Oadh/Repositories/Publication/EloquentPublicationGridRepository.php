<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Publication;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentPublicationGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_CMS_Publication AS p')
			->where('p.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'p.id AS oadh_pm_id',
			'p.type AS oadh_pm_type',
			'p.date AS oadh_pm_date',
			'p.title AS oadh_pm_title',
			'p.description AS oadh_pm_description',
			'p.author AS oadh_pm_author',
			'p.tags AS oadh_pm_tags',
			'p.lang AS oadh_pm_lang',
			'p.image_url AS oadh_pm_image_url',
			'p.download_url AS oadh_pm_download_url',
			'p.summary_url AS oadh_pm_summary_url',
			'p.infographic_url AS oadh_pm_infographic_url',
			'p.is_highlighted AS oadh_pm_is_highlighted'
			
			// $DB->raw('CASE t1.field0 WHEN 1 THEN 0 ELSE 1 END AS module_app_field0'),
			// $DB->raw('CONCAT(\'#\', LPAD(t1.field0, 6, 0), \' \', t1.field1) AS module_app_field1'),
		);

		$this->orderBy = array(array('oadh_pm_id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'parent_id';

		// $this->leafColumn = 'is_leaf';
	}
}
