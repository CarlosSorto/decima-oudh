<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentHumanRightGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_Human_Right AS hr')
			->leftJoin('OADH_Human_Right AS hrp', 'hrp.id', '=', 'hr.parent_id')
			->where('hr.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'hr.id AS oadh_hrm_id',
			'hr.name AS oadh_hrm_name',
			'hr.es_name AS oadh_hrm_es_name',
			'hr.en_name AS oadh_hrm_en_name',
			'hr.es_description AS oadh_hrm_es_description',
			'hr.en_description AS oadh_hrm_en_description',
			'hr.type AS oadh_hrm_type',
			'hr.position AS oadh_hrm_position',
			'hr.is_web_visible AS oadh_hrm_is_web_visible',
			'hr.parent_id AS oadh_hrm_parent_id',
			'hrp.name AS oadh_hrm_parent_label',
			$DB->raw('CASE WHEN hr.type is null THEN 0 ELSE 1 END AS oadh_hrm_is_leaf')
		);

		$this->orderBy = array(array('hr.id', 'asc'));

		$this->treeGrid = true;

		$this->parentColumn = 'hr.parent_id';

		$this->leafColumn = 'oadh_hrm_is_leaf';
	}
}
