<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight;

use Illuminate\Database\Eloquent\Model;

use Mgallegos\DecimaOadh\Oadh\HumanRight;

class EloquentHumanRight implements HumanRightInterface {

  /**
   * HumanRight
   *
   * @var Mgallegos\DecimaOadh\Oadh\HumanRight;
   *
   */
  protected $HumanRight;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(Model $HumanRight, $databaseConnectionName)
  {
    $this->HumanRight = $HumanRight;

    $this->databaseConnectionName = $databaseConnectionName;

    $this->HumanRight->setConnection($databaseConnectionName);
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->HumanRight->getTable();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\HumanRight
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->HumanRight->on($databaseConnectionName)->find($id);
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRight->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }


  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganizationAndParent($parent, $id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRight->setConnection($databaseConnectionName)->where('parent_id', '=', $parent)->where('organization_id', '=', $id)->get();
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganizationAndType($type, $id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRight->setConnection($databaseConnectionName)->where("parent_id", "!=", null)->where('type', '=', $type)->where('organization_id', '=', $id)->get();
  }

  /**
   * Retrieve ... by type
   *
   * @param  string $type
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byType($type, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRight->setConnection($databaseConnectionName)
      ->where("parent_id", "!=", null)
      ->where('type', '=', $type)
      ->get();
  }

  /**
   * Retrieve ... by type and parent id
   *
   * @param  string $type
   * @param integer $parentId
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byTypeAndParentId($type, $parentId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->HumanRight->setConnection($databaseConnectionName)
      ->where('type', '=', $type);

    if(!empty($parentId))
    {
      $query->whereIn("parent_id", $parentId);
    }

    return $query->get();
  }

  /**
   * Retrieve ... by type
   *
   * @param  string $type
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function webVisibleByType($type, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRight->setConnection($databaseConnectionName)
      ->where("parent_id", "!=", null)
      ->where("is_web_visible", "=", 1)
      ->where('type', '=', $type)
      ->orderBy('position')
      ->get();
  }

  /**
   * Retrieve ... by type and parent id
   *
   * @param  string $type
   * @param integer $parentId
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function webVisibleByTypesAndParentId($types, $parentId, $lang, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->HumanRight->setConnection($databaseConnectionName)
      ->where("is_web_visible", "=", 1)  
      ->whereIn('type', $types)
      ->orderBy($lang . '_name');

    if(!empty($parentId))
    {
      $query->whereIn("parent_id", $parentId);
    }

    return $query->get();
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganizationAndParentAndType($parent, $type,  $id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRight->setConnection($databaseConnectionName)
      ->where('parent_id', '=', $parent)
      ->where("type", "=", $type)
      ->where('organization_id', '=', $id)
      ->get();
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function getMaxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->HumanRight->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $HumanRight = new HumanRight();
    $HumanRight->setConnection($databaseConnectionName);
    $HumanRight->fill($data)->save();

    return $HumanRight;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\HumanRight $HumanRight
   *
   * @return boolean
   */
  public function update(array $data, $HumanRight = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($HumanRight))
    {
      $HumanRight = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      if(!empty($value))
      {
        $HumanRight->$key = $value;
      }
      else
      {
        $HumanRight->$key = null;
      }
    }

    return $HumanRight->save();
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $HumanRight = $this->byId($id, $databaseConnectionName);
      $HumanRight->delete();
    }
    // $this->Account->destroy($data);

    return true;
  }

}
