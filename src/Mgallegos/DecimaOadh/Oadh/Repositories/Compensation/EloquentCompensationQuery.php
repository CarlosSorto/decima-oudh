<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Compensation;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Collection;

class EloquentCompensationQuery implements CompensationQueryInterface {

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(DatabaseManager $DB, $databaseConnectionName)
  {
    $this->DB = $DB;
    $this->databaseConnectionName = $databaseConnectionName;
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->CompensationQuery->getTable();
  }

  /**
   * Get search modal table rows
   *
   * @param int $organizationId
   *
   * @return Collection
   */
  public function searchModalTableRows($id = null, $organizationId, $count = false, $limit = null, $offset = null, $filter = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      ->table('Table_Name0 AS t0')
      ->join('Table_Name1 AS t1', 't1.column_name', '=', 't0.column_name')
      // ->orderBy('t1.column_name0', 'desc')
      // ->orderBy('t1.column_name1', 'asc')
      // ->whereIn('t1.id', $ids)
      ->where('t0.organization_id', '=', $organizationId);

    if(!empty($id))
    {
      $query->where('t0.id', '=', $id);
    }

    if(!empty($filter))
    {
      $query->where(function($dbQuery) use ($filter)
      {
        foreach (array('column1', 'column2') as $key => $value)
        {
          $dbQuery->orWhere($value, 'like', '%' . str_replace(' ', '%', $filter) . '%');
        }
      });
    }

    if($count)
    {
      return $query->count();
    }

    if(!empty($limit))
    {
      $query->take($limit);
    }

    if(!empty($offset) && $offset != 0)
    {
      $query->skip($offset);
    }

    return new Collection(
      $query->get(
        array(
          't0.*'
        )
      )
    );
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function maxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->CompensationQuery->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Retrieve by filed id and by organization id
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byFileIdAndByOrganizationId($fileId, $organizationId, $notSync = true, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->CompensationQuery->setConnection($databaseConnectionName)
      ->where('file_id', '=', $fileId)
      ->where('organization_id', '=', $organizationId);

    if (isset($notSync))
    {
      if ($notSync == true)
      {
        $query->where('syncronized', '=', 0);
      }
      else if ($notSync == false)
      {
        $query->where('syncronized', '=', 1);
      }
    }

    return $query->get();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\CompensationQuery
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->CompensationQuery->on($databaseConnectionName)->find($id);
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byIds($ids, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
        ->table('Table_Name0 AS t0')
        ->join('Table_Name1 AS t1', 't1.column_name', '=', 't0.column_name')
        // ->where('p.id', '=', $ids)
        ->whereIn('t1.id', $ids)
        // ->orderBy('t1.column_name0', 'desc')
        // ->orderBy('t1.column_name1', 'asc')
        ->get(array('t0.*'))
    );
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->CompensationQuery->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  public function getCompensationYearFilterByInstitution($institution = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Budget')
    ->select(
      $this->DB->raw('DISTINCT(year) AS year')
    )
    ->where('institution', '=', $institution)
    ->orderBy('year')
    ->get()
    );
  }

  public function getCompensationYearFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Budget')
    ->select(
      $this->DB->raw('DISTINCT(year) AS year')
    )
    ->orderBy('year')
    ->get()
    );
  }

  public function getFisdlYearFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Fisdl_Beneficiaries')
    ->select(
      $this->DB->raw('DISTINCT(year) AS year')
    )
    ->orderBy('year')
    ->get()
    );
  }

  public function getAllegationsYearFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection($this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Allegations')
    ->select(
      $this->DB->raw('DISTINCT(year) AS year')
    )
    ->orderBy('year')
    ->get()
    );
  }

  public function getVeteransExCombatantsBudgetByInstitution($filters, $institution, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Budget')
    ->select('budget_unit', 'budget_amount')
    ->where('institution', '=', $institution);

    if(!empty($filters['year']))
    {
      $query->whereIn('year', $filters['year']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getVeteransExCombatantsByUnitBudgetSum($filters = array(), $unitBudget = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Budget')
    ->select('budget_amount')
    ->where('budget_unit', '=', $unitBudget);

    if(!empty($filters['year']))
    {
      $query->whereIn('year', $filters['year']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getFisdlBeneficiariesByYear($yearFrom = null, $yearTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Fisdl_Beneficiaries')
    ->select('department', $this->DB->raw('SUM(men_total + women_total) AS count'))
    ->where('year', '>=', $yearFrom)
    ->where('year', '<=', $yearTo)
    ->groupBy('department');

    return new Collection(
      $query->get()
    );
  }

  public function getAllegationsByInstitution($filters, $institution, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Allegations')
    ->select('department', $this->DB->raw('allegations AS count'))
    ->where('institution', '=', $institution);

    if(!empty($filters['year']))
    {
      $query->whereIn('year', $filters['year']);
    }

    return new Collection(
      $query->get()
    );
  }

  public function getInstitutionYearlyBudgetUnit($unit, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Budget')
    ->select('year', 'budget_unit', $this->DB->raw('SUM(budget_amount) AS count'))
    ->where('budget_unit', 'like', "%$unit%")
    ->groupBy('year')
    ->groupBy('budget_unit')
    ->orderBy('year');

    return new Collection(
      $query->get()
    );
  }

  public function getExtractedYearFromDate($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Amnesty')
    ->select($this->DB->raw('DISTINCT EXTRACT(year from date) as year, EXTRACT(month FROM date) as month'))
    ->orderBy('month')
    ->orderBy('year');

    return new Collection(
      $query->get()
    );
  }

  public function getAmnestyDataByStartAndEndOfDate($start, $end, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_CR_Amnesty')
    ->where('date', '>=', $start)
    ->where('date', '<=', $end)
    ->orderBy('date');

    return new Collection(
      $query->get()
    );
  }

  /**
   * Mass create
   *
   * @param array of arrays
   *
   *
   * @return boolean
   */
  public function massCreate($data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->insert($data);

    return true;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\CompensationQuery $CompensationQuery
   *
   * @return boolean
   */
  public function update(array $data, $CompensationQuery = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($CompensationQuery))
    {
      $CompensationQuery = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      $CompensationQuery->$key = $value;
    }

    return $CompensationQuery->save();
  }

  /**
   * Update by column name
   *
   * @param int $columnNameOldValue
   * @param integer $organizationId
   * @param string $databaseConnectionName
   *
   * @return boolean
   */
  public function updateByColumnNameEloquent($columsToBeUpdated, $filteredValue, $organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->CompensationQuery->setConnection($databaseConnectionName)
      ->where('column_name', '=', $filteredValue)
      ->where('organization_id', '=', $organizationId)
      ->update($columsToBeUpdated);

    return true;
  }

  /**
   * Update by column name
   *
   * @param array $columsToBeUpdated
   * @param mixed $filteredValue
   * @param string $databaseConnectionName
   *
   * @return boolean
   */
  public function updateByCustomColumnQueryBuilder($columsToBeUpdated, $filteredValue, $organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->where('column_name', '=', $filteredValue)
      ->where('organization_id', '=', $organizationId)
      ->update($columsToBeUpdated);

    return true;
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $CompensationQuery = $this->byId($id, $databaseConnectionName);
      $CompensationQuery->delete();
    }

    return true;
  }

  /**
   * Mass detele
   *
   * @param integer $fileId
   *
   *
   * @return boolean
   */
  public function massDelete($ids, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->whereIn('id', '=', $ids)
      ->delete();

    return true;
  }

  /**
   * Mass detele by column
   *
   * @param integer $fileId
   *
   *
   * @return boolean
   */
  public function massDeleteByColumn($column, $value, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->where($column, '=', $value)
      ->delete();

    return true;
  }

}
