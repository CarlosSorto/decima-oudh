<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia;

use Illuminate\Database\Eloquent\Model;

use Mgallegos\DecimaOadh\Oadh\DigitalMedia;

class EloquentDigitalMedia implements DigitalMediaInterface {

  /**
   * DigitalMedia
   *
   * @var Mgallegos\DecimaOadh\Oadh\DigitalMedia;
   *
   */
  protected $DigitalMedia;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(Model $DigitalMedia, $databaseConnectionName)
  {
    $this->DigitalMedia = $DigitalMedia;

    $this->databaseConnectionName = $databaseConnectionName;

    $this->DigitalMedia->setConnection($databaseConnectionName);
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->DigitalMedia->getTable();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\DigitalMedia
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->DigitalMedia->on($databaseConnectionName)->find($id);
  }

  public function getAllWithParent($id = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->DigitalMedia->setConnection($databaseConnectionName)->where('parent_id', '=', $id)->get();
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->DigitalMedia->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganizationAndParent($Parent, $id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->DigitalMedia->setConnection($databaseConnectionName)->where('parent_id', '=', $Parent)->where('organization_id', '=', $id)->get();
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function getMaxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->DigitalMedia->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $DigitalMedia = new DigitalMedia();
    $DigitalMedia->setConnection($databaseConnectionName);
    $DigitalMedia->fill($data)->save();

    return $DigitalMedia;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\DigitalMedia $DigitalMedia
   *
   * @return boolean
   */
  public function update(array $data, $DigitalMedia = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($DigitalMedia))
    {
      $DigitalMedia = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      if(!empty($value))
      {
        $DigitalMedia->$key = $value;
      }
      else
      {
        $DigitalMedia->$key = null;
      }
    }

    return $DigitalMedia->save();
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $DigitalMedia = $this->byId($id, $databaseConnectionName);
      $DigitalMedia->delete();
    }
    // $this->Account->destroy($data);

    return true;
  }

}
