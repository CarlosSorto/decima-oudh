<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentDigitalMediaGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_Digital_Media AS dm')
			->leftJoin('OADH_Digital_Media AS dmc', 'dmc.id', '=', 'dm.parent_id')
			->where('dm.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'dm.id AS oadh_dmm_id',
			'dm.name AS oadh_dmm_name',
			'dm.type AS oadh_dmm_type',
			'dm.abbreviation AS oadh_dmm_abbreviation',
			'dmc.name AS oadh_dmm_parent_label',
			'dm.parent_id AS oadh_dmm_parent_id',
			$DB->raw('CASE dm.type WHEN \'M\' THEN 0 ELSE 1 END AS oadh_dmm_is_leaf')
		);

		$this->orderBy = array(array('dm.id', 'asc'));

		$this->treeGrid = true;

		$this->parentColumn = 'dm.parent_id';

		$this->leafColumn = 'oadh_dmm_is_leaf';
	}
}
