<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Collection;
use Mgallegos\DecimaOadh\Oadh\Recommendation;

class EloquentRecommendation implements RecommendationInterface {

  /**
   * Recommendation
   *
   * @var Mgallegos\DecimaOadh\Oadh\Recommendation;
   *
   */
  protected $Recommendation;

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(Model $Recommendation, DatabaseManager $DB, $databaseConnectionName)
  {
    $this->Recommendation = $Recommendation;
    $this->DB = $DB;
    $this->databaseConnectionName = $databaseConnectionName;
    $this->Recommendation->setConnection($databaseConnectionName);
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->Recommendation->getTable();
  }

  public function getSystems($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
    ->table('OADH_Recommendation AS rec')
    ->select($this->DB->raw('DISTINCT(rec.system) as system'))->get()
    );
  }

  public function getMechanisms($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
    ->table('OADH_Recommendation AS rec')
    ->select($this->DB->raw('DISTINCT(rec.mechanism) as mechanism'))->get()
    );
  }

  public function getMechanismBySystem($system = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_Recommendation AS rec');
    
    if(!empty($system))
    {
      $query->where('rec.system', '=', $system);
    }

    $query->select($this->DB->raw('DISTINCT(rec.mechanism) as mechanism'))->get();

    return new Collection(
      $query->get()
    );
  }

  public function getInstitutions($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
    ->table('OADH_Institution AS ins')->get(array('ins.id as institution_id', 'ins.name as institution', 'ins.abbreviation as abbreviation'))
    );
  }

  public function getRights($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
    ->table('OADH_Right AS rgt')->get(array('rgt.id as right_id', 'rgt.name as right'))
    );
  }

  /**
   * Get search modal table rows
   *
   * @param int $organizationId
   *
   * @return Collection
   */
  public function searchModalTableRows($id = null, $organizationId, $count = false, $limit = null, $offset = null, $filter = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_Recommendation AS rec');
      // ->join('Table_Name1 AS t1', 't1.column_name', '=', 't0.column_name')
      // ->orderBy('t1.column_name0', 'desc')
      // ->orderBy('t1.column_name1', 'asc')
      // ->whereIn('t1.id', $ids)
      // ->where('rec.organization_id', '=', $organizationId);

    if(!empty($id))
    {
      $query->where('rec.id', '=', $id);
    }

    if(!empty($filter))
    {
      $query->where(function($dbQuery) use ($filter)
      {
        foreach (array('column1', 'column2') as $key => $value)
        {
          $dbQuery->orWhere($value, 'like', '%' . str_replace(' ', '%', $filter) . '%');
        }
      });
    }

    if($count)
    {
      return $query->count();
    }

    if(!empty($limit))
    {
      $query->take($limit);
    }

    if(!empty($offset) && $offset != 0)
    {
      $query->skip($offset);
    }

    return new Collection(
      $query->get(
        array(
          'rec.*'
        )
      )
    );
  }

  public function searchAdvancedModalTableRows($id = null, $filters = null, $paginate = true, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_Recommendation AS rec');
      // ->orderBy('t1.column_name0', 'desc')
      // ->orderBy('t1.column_name1', 'asc')

    if(!empty($id))
    {
      $query->where('rec.id', '=', $id);
    }

    if(!empty($filters))
    {
      foreach ($filters as $key => $value) 
      {
        switch ($value['op']) 
        {
          case 'like':
            $query->where(function($dbQuery) use ($value)
            {
              foreach (array('system', 
                'recommendation', 
                'code', 
                'mechanism',
                'related_rights', 
                'responsible_institutions', 
                'source', 
                'legal_base', 
                'observations') as $key => $column)
              {
                // $dbQuery->orWhere($column, 'like', '%' . str_replace(' ', '%', $value['data']) . '%');
                // $dbQuery->orWhereRaw('lower(' . $column . ') LIKE \'%' . mb_strtolower($value['data'] . '%\''));

                $dbQuery->orWhereRaw('lower(' . $column . ') LIKE \'%' . mb_strtolower(
                  str_replace(' ', '%', $value['data'])
                ) . '%\'');
              }
            });
            break;
          case 'eq':
            $query->where($value['field'], '=', $value['data']);
            break;
          case 'eq_in':
            $query->whereIn($value['field'], $value['data']);
            break;
          case 'institution_in':
            $query->whereRaw('rec.id IN (SELECT rin.recommendation_id FROM public."OADH_Recommendation_Institution" as rin WHERE rin.institution_id IN (' . implode(',', $value['data']) . '))');
            break;
          case 'right_in':
            $query->whereRaw('rec.id IN (SELECT rri.recommendation_id FROM public."OADH_Recommendation_Right" as rri WHERE rri.right_id IN (' . implode(',', $value['data']) . '))');
            break;
          case 'date_range':
            
            if(!empty($value['data_from']))
            {
              $query->where($value['field'], '>=', $value['data_from']);
            }
            
            if(!empty($value['data_to']))
            {
              $query->where($value['field'], '<=', $value['data_to']);
            }
            break;
          case 'ge':
            $query->where($value['field'], '>=', $value['data']);
            break;
          case 'le':
            $query->where($value['field'], '<=', $value['data']);
            break;
          default:
            break;
        }
      }
    }

    if(!empty($paginate))
    {
      return $query->paginate(6);
    }

    return $query->get();
  }

  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Mgallegos\DecimaOadh\Oadh\Recommendation
   */
  public function byId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

  	return $this->Recommendation->on($databaseConnectionName)->find($id);
  }

  public function getInstitutionsByRecommendationId($recommendationId = null, $databaseConnectionName)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_Recommendation AS rec')
        ->leftJoin('OADH_Recommendation_Institution AS ori', 'ori.recommendation_id', '=', 'rec.id')
        ->leftJoin('OADH_Institution AS ins', 'ori.institution_id', '=', 'ins.id')
        ->where('rec.id', '=', $recommendationId)
        ->get(array(
          'ins.*',
        )
      )
    );
  }

  public function getRightsByRecommendationId($recommendationId = null, $databaseConnectionName)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_Recommendation AS rec')
        ->leftJoin('OADH_Recommendation_Right AS orr', 'orr.recommendation_id', '=', 'rec.id')
        ->leftJoin('OADH_Right AS rgh', 'orr.right_id', '=', 'rgh.id')
        ->where('rec.id', '=', $recommendationId)
        ->get(array(
          'rgh.*',
        )
      )
    );
  }


  /**
   * Get a ... by ID
   *
   * @param  int $id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byIds($ids, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection(
      $this->DB->connection($databaseConnectionName)
        ->table('Table_Name0 AS t0')
        ->join('Table_Name1 AS t1', 't1.column_name', '=', 't0.column_name')
        // ->where('p.id', '=', $ids)
        ->whereIn('t1.id', $ids)
        // ->orderBy('t1.column_name0', 'desc')
        // ->orderBy('t1.column_name1', 'asc')
        ->get(array('t0.*'))
    );
  }

  /**
   * Retrieve by file id
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byFileId($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Recommendation->setConnection($databaseConnectionName)->where('file_id', '=', $id);
  }

  /**
   * Retrieve ... by organization
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function byOrganization($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Recommendation->setConnection($databaseConnectionName)->where('organization_id', '=', $id)->get();
  }

  /**
   * Get max number
   *
   * @param  int $id Organization id
   *
   * @return integer
   */
  public function getMaxNumber($id, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return $this->Recommendation->setConnection($databaseConnectionName)
      ->where('organization_id', '=', $id)
      ->max('number');
  }

  /**
   * Create a new ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @return boolean
   */
  public function create(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Recommendation = new Recommendation();
    $Recommendation->setConnection($databaseConnectionName);
    $Recommendation->fill($data)->save();

    return $Recommendation;
  }

  /**
   * Update an existing ...
   *
   * @param array $data
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
   * @param Mgallegos\DecimaOadh\Oadh\Recommendation $Recommendation
   *
   * @return boolean
   */
  public function update(array $data, $Recommendation = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    if(empty($Recommendation))
    {
      $Recommendation = $this->byId($data['id'], $databaseConnectionName);
    }

    foreach ($data as $key => $value)
    {
      $Recommendation->$key = $value;
    }

    return $Recommendation->save();
  }

  /**
   * Update by column name
   *
   * @param int $columnNameOldValue
   * @param integer $organizationId
   * @param string $databaseConnectionName
   *
   * @return boolean
   */
  public function updateByColumnNameEloquent($columnNameOldValue, $organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->Recommendation->setConnection($databaseConnectionName)
      ->where('column_name', '=', $columnNameOldValue)
      ->where('organization_id', '=', $organizationId)
      ->update(array('column_name_to_be_updated' => $newValue));

    return true;
  }

  /**
   * Update by column name
   *
   * @param int $columnNameOldValue
   * @param integer $organizationId
   * @param string $databaseConnectionName
   *
   * @return boolean
   */
  public function updateByColumnNameQueryBuilder($columnNameOldValue, $organizationId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->where('column_name', '=', $columnNameOldValue)
      ->where('organization_id', '=', $organizationId)
      ->update(array('column_name_to_be_updated' => $newValue));

    return true;
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $data
   * 	An array as follows: array($id0, $id1,…);
   * @return boolean
   */
  public function delete(array $data, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    foreach ($data as $key => $id)
    {
      $Recommendation = $this->byId($id, $databaseConnectionName);
      $Recommendation->delete();
    }

    return true;
  }

  /**
   * Mass detele from temp table
   *
   * @param string $table
   * @param integer $fileId
   *
   * @return boolean
   */
  public function massDelete($table, $fileId, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($table)
      ->where('file_id', '=', $fileId)
      ->delete();

    return true;
  }

  /**
   * Mass detele by column
   *
   * @param integer $fileId
   *
   *
   * @return boolean
   */
  public function massDeleteByColumn($column, $value, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $this->DB->connection($databaseConnectionName)
      ->table($this->getTable())
      ->where($column, '=', $value)
      ->delete();

    return true;
  }

}
