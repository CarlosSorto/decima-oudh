<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\People;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentVictimizerGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_People AS p')
			->where('p.type', '=', 'R')
			->where('p.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'p.id AS oadh_victimizer_id',
			'p.type AS oadh_victimizer_type',
			'p.subtype AS oadh_victimizer_subtype',
			'p.first_name AS oadh_victimizer_first_name',
			'p.last_name AS oadh_victimizer_last_name',
			'p.age AS oadh_victimizer_age',
			'p.profesion AS oadh_victimizer_profesion',
			'p.gender AS oadh_victimizer_gender',
			'p.victim_relation AS oadh_victim_relation',
			'p.victimizer_relation AS oadh_victimizer_relation',
			'p.news_id AS oadh_victimizer_news_id',
			'p.sexual_diversity AS oadh_victimizer_sexual_diversity'
		);

		$this->orderBy = array(array('p.id', 'asc'));
	}
}
