<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\People;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentVictimGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_People AS p')
			->where('p.type', '=', 'V')
			->where('p.organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'p.id AS oadh_victim_id',
			'p.type AS oadh_victim_type',
			'p.subtype AS oadh_victim_subtype',
			'p.first_name AS oadh_victim_first_name',
			'p.last_name AS oadh_victim_last_name',
			'p.age AS oadh_victim_age',
			'p.profesion AS oadh_victim_profesion',
			'p.gender AS oadh_victim_gender',
			'p.victimizer_relation AS oadh_victimizer_relation',
			'p.main_victim_relation AS oadh_main_victim_relation',
			'p.news_id AS oadh_victim_news_id',
			'p.sexual_diversity AS oadh_victim_sexual_diversity'
		);

		$this->orderBy = array(array('p.id', 'asc'));
	}
}
