<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Life;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentLifeAggressionsDeathTempGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_LIFE_Aggression_Death_Temp AS tnt')
			->where('organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'id AS oadh_derecho_prefijo_id',
			'department AS oadh_life_ad_department',
			'municipality AS oadh_life_ad_municipality',
			'day AS oadh_life_ad_day',
			'day_number AS oadh_life_ad_day_number',
			'month AS oadh_life_ad_month',
			'year AS oadh_life_ad_year',
			'date AS oadh_life_ad_date',
			'time AS oadh_life_ad_time',
			'gang AS oadh_life_ad_gang',
			'division AS oadh_life_ad_division',
			'deceased_policemen AS oadh_life_ad_deceased_policemen',
			'aggressions AS oadh_life_ad_aggressions',
			'status AS oadh_derecho_prefijo_status',
			'file_id AS oadh_derecho_prefijo_file_id'
		);

		$this->orderBy = array(array('id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ald.parent_id';

		// $this->leafColumn = 'oadh_ald_is_leaf';
	}
}
