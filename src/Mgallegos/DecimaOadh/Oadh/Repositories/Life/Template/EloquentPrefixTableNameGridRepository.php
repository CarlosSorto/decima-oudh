<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Derecho;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentPrefixTableNameGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_PREFIX_Table_Name AS tn')
			->where('organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'id AS oadh_derecho_prefijo_id',
			'column AS oadh_derecho_prefijo_column',
			'status AS oadh_derecho_prefijo_status',
			'file_id AS oadh_derecho_prefijo_file_id'
		);

		$this->orderBy = array(array('id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ald.parent_id';

		// $this->leafColumn = 'oadh_ald_is_leaf';
	}
}
