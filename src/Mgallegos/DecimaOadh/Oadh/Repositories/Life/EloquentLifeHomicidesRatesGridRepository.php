<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Life;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentLifeHomicidesRatesGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_LIFE_Homicides_Rates AS hr')
			->where('organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'id AS oadh_life_hr_id',
			'record AS oadh_life_hr_record',
			'year AS oadh_life_hr_year',
			'month AS oadh_life_hr_month',
			'day AS oadh_life_hr_day',
			'time_aggression AS oadh_life_hr_time_aggression',
			'age AS oadh_life_hr_age',
			'sex AS oadh_life_hr_sex',
			'occupation AS oadh_life_hr_occupation',
			'municipality AS oadh_life_hr_municipality',
			'department AS oadh_life_hr_department',
			'weapon_type AS oadh_life_hr_weapon_type',
			'file_id AS oadh_life_hr_file_id'

		);

		$this->orderBy = array(array('id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'parent_id';

		// $this->leafColumn = 'oadh_hr_is_leaf';
	}
}
