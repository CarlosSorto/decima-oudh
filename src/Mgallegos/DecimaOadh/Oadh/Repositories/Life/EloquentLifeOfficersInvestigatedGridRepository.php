<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Life;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentLifeOfficersInvestigatedGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_LIFE_Officers_Investigated AS oi')
			->where('organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'id AS oadh_life_oi_id',
			'record AS oadh_life_oi_record',
			'investigation_type AS oadh_life_oi_investigation_type',
			'year AS oadh_life_oi_year',
			'crime AS oadh_life_oi_crime',
			'category AS oadh_life_oi_category',
			'position AS oadh_life_oi_position',
			'officer_sex AS oadh_life_oi_officer_sex',
			'location_place AS oadh_life_oi_location_place',
			'location_department AS oadh_life_oi_location_department',
			'location_municipality AS oadh_life_oi_location_municipality', 
			'date AS oadh_life_oi_date',
			'act_place AS oadh_life_oi_act_place',
			'act_department AS oadh_life_oi_act_department',
			'act_municipality AS oadh_life_oi_act_municipality',
			'weapon_type AS oadh_life_oi_weapon_type',  
			'victim_quantity AS oadh_life_oi_victim_quantity',
			'victim_sex AS oadh_life_oi_victim_sex',
			'criminal_justice_process AS oadh_life_oi_criminal_justice_process',
			
			'file_id AS oadh_life_oi_file_id'
		);

		$this->orderBy = array(array('id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ald.parent_id';

		// $this->leafColumn = 'oadh_ald_is_leaf';
	}
}
