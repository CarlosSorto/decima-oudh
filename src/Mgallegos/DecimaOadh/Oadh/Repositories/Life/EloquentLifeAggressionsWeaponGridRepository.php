<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Life;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use Illuminate\Database\DatabaseManager;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

use Illuminate\Translation\Translator;

class EloquentLifeAggressionsWeaponGridRepository extends EloquentRepositoryAbstract {

	public function __construct(DatabaseManager $DB, AuthenticationManagementInterface $AuthenticationManager)
	{
		// $this->DB = $DB;
		// $this->DB->connection()->enableQueryLog();

		$this->Database = $DB->connection($AuthenticationManager->getCurrentUserOrganizationConnection())
			->table('OADH_LIFE_Aggression_Weapon AS aw')
			->where('organization_id', '=', $AuthenticationManager->getCurrentUserOrganizationId());

		$this->visibleColumns = array(
			'id AS oadh_life_aw_id',
			'department AS oadh_life_aw_department',
			'municipality AS oadh_life_aw_municipality',
			'day AS oadh_life_aw_day',
			'day_number AS oadh_life_aw_day_number',
			'month AS oadh_life_aw_month',
			'year AS oadh_life_aw_year',
			'date AS oadh_life_aw_date',
			'time AS oadh_life_aw_time',
			'gang AS oadh_life_aw_gang',
			'division AS oadh_life_aw_division',
			'aggressions AS oadh_life_aw_aggressions',
			'deceased AS oadh_life_aw_deceased',
			//'status AS oadh_life_aw_status',
			'file_id AS oadh_life_aw_file_id'
		);

		$this->orderBy = array(array('id', 'asc'));

		// $this->treeGrid = true;

		// $this->parentColumn = 'ald.parent_id';

		// $this->leafColumn = 'oadh_ald_is_leaf';
	}
}
