<?php
/**
 * @file
 * Description of the script.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Repositories\Life;

use Illuminate\Database\DatabaseManager;

use Illuminate\Database\Eloquent\Collection;

class EloquentLifeQuery implements LifeQueryInterface {

  /**
   * DB
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Database Connection
   *
   * @var string
   *
   */
  protected $databaseConnectionName;

  public function __construct(DatabaseManager $DB, $databaseConnectionName)
  {
    $this->DB = $DB;

    $this->databaseConnectionName = $databaseConnectionName;
  }

  /**
   * Get table name
   *
   * @return string
   */
  public function getTable()
  {
    return $this->EloquentLifeQuery->getTable();
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function homicidesDepartmentsByYearAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $weaponType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_LIFE_Homicides_Rates')
      ->select('department', $this->DB->raw('COUNT(1) AS count'))
      ->groupBy('department');

    if(!empty($year))
    {
      $Query->where('year', '=' , $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if(!empty($weaponType))
    {
      $Query->whereIn('weapon_type', $weaponType);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function homicidesOfWomenDepartmentsByYearAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $weaponType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $gender['gender'] = 'F';

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_LIFE_Homicides_Rates')
      ->select('department', $this->DB->raw('COUNT(1) AS count'))
      ->groupBy('department');

      if(!empty($year))
      {
        $Query->where('year', '=', $year);
      }
  
      if(!empty($yearFrom))
      {
        $Query->where('year', '>=', $yearFrom);
      }
  
      if(!empty($yearTo))
      {
        $Query->where('year', '<=', $yearTo);
      }
  
      if(!empty($weaponType))
      {
        $Query->whereIn('weapon_type', $weaponType);
      }

    $Query->whereIn('sex', $gender);

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function homicidesMunicipalitiesByYearAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $weaponType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_LIFE_Homicides_Rates')
      ->select('department', 'municipality', $this->DB->raw('COUNT(1) AS count'))
      ->groupBy('department')
      ->groupBy('municipality')
      ->orderBy('department', 'ASC')
      ->orderBy('count', 'DESC');
      // ->orderBy('municipality', 'desc');

    if(!empty($year))
    {
      $Query->where('year', '=', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if(!empty($weaponType))
    {
      $Query->whereIn('weapon_type', $weaponType);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function homicidesOfWomenMunicipalitiesByYearAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $weaponType = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $sex['sex'] = 'F';

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_LIFE_Homicides_Rates')
      ->select('department', 'municipality', $this->DB->raw('COUNT(1) AS count'))
      ->groupBy('department')
      ->groupBy('municipality')
      ->orderBy('department', 'ASC')
      ->orderBy('count', 'DESC');

      if(!empty($year))
      {
        $Query->where('year', '=', $year);
      }
  
      if(!empty($yearFrom))
      {
        $Query->where('year', '>=', $yearFrom);
      }
  
      if(!empty($yearTo))
      {
        $Query->where('year', '<=', $yearTo);
      }
  
      if(!empty($weaponType))
      {
        $Query->whereIn('weapon_type', $weaponType);
      }

    $Query->whereIn('sex', $sex);

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function homicidesByIllegitimateAgressionsDepartmentsByYear($yearFrom = null, $yearTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_LIFE_Aggression_Weapon')
      ->select('department', $this->DB->raw('COUNT(1) AS count'))
      ->groupBy('department')
      ->orderBy('department');

      if(!empty($yearFrom))
      {
        $Query->where('year', '>=', $yearFrom);
      }
  
      if(!empty($yearTo))
      {
        $Query->where('year', '<=', $yearTo);
      }
  
    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function homicidesByIllegitimateAgressionsMunicipalitiesByYear($yearFrom = null, $yearTo = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_LIFE_Aggression_Weapon')
      ->select('department', 'municipality', $this->DB->raw('COUNT(1) AS count'))
      ->groupBy('department')
      ->groupBy('municipality')
      ->orderBy('department', 'ASC')
      ->orderBy('count', 'DESC');

      if(!empty($yearFrom))
      {
        $Query->where('year', '>=', $yearFrom);
      }
  
      if(!empty($yearTo))
      {
        $Query->where('year', '<=', $yearTo);
      }


    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function territoryPopulationByDepartment($year = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_POP_Territory_Population')
      ->select('department', $this->DB->raw('SUM(population)'))
      ->groupBy('department');

    if(!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function territoryWomenPopulationByDepartment($year = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $sex['sex'] = 'M';

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_POP_Territory_Population')
      ->select('department', $this->DB->raw('SUM(population)'))
      ->groupBy('department');

    if(!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    return new Collection (
      $Query->get()
    );
  }


  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function territoryPopulationByMunicipality($year = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_POP_Territory_Population')
      ->select('department', 'municipality', $this->DB->raw('SUM(population) AS population'))
      ->groupBy('department')
      ->groupBy('municipality')
      ->orderBy('department');

    if(!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    return new Collection (
      $Query->get()
    );

  }

  /**
   * Retrieve filter
   *
   * @param array $court
   *
   * @return integer
  */
  public function territoryWomenPopulationByMunicipality($year = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $sex['sex'] = 'M';

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_POP_Territory_Population')
      ->select('department', 'municipality', $this->DB->raw('SUM(population) as population'))
      ->groupBy('department')
      ->groupBy('municipality')
      ->groupBy('population')
      ->orderBy('department');

    if(!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    return new Collection (
      $Query->get()
    );

  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentByWeaponTypeAndYearAndGenderAndDepartmentAndMunicipality($year = null, $yearFrom = null, $yearTo = null, $gender = null, $department = null, $municipality = null, $databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_LIFE_Homicides_Rates')
    ->select('weapon_type as weapons', $this->DB->raw('COUNT(1) as value'))
    ->groupBy('weapon_type');

    if(!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if(!empty($gender))
    {
      $Query->whereIn('sex', $gender);
    }

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    return new Collection (
      $Query->get()
    );
  }


  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentBySexAndYearAndMunicipalityAndDepartmentAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $municipality = null, $department = null, $weaponType = null, $databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_LIFE_Homicides_Rates')
    ->select('sex', $this->DB->raw('COUNT(1) as value'))
    ->groupBy('sex');

    if(!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($weaponType))
    {
      $Query->whereIn('weapon_type', $weaponType);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentByMonthAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $gender = null, $department = null, $municipality = null, $weaponType = null, $databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_LIFE_Homicides_Rates')
    ->select('month', $this->DB->raw('COUNT(1) as value'))
    ->groupBy('month');

    if(!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if(!empty($gender))
    {
      $Query->whereIn('sex', $gender);
    }
    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }
    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }
    if(!empty($weaponType))
    {
      $Query->whereIn('weapon_type', $weaponType);
    }

    $Query->orderByRaw(
      "CASE
      WHEN (month = 'ENERO') then 1
      WHEN (month = 'FEBRERO') then 2
      WHEN (month = 'MARZO') then 3
      WHEN (month = 'ABRIL') then 4
      WHEN (month = 'MAYO') then 5
      WHEN (month = 'JUNIO') then 6
      WHEN (month = 'JULIO') then 7
      WHEN (month = 'AGOSTO') then 8
      WHEN (month = 'SEPTIEMBRE') then 9
      WHEN (month = 'OCTUBRE') then 10
      WHEN (month = 'NOVIEMBRE') then 11
      WHEN (month = 'DICIEMBRE') then 12
      end");

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentOnWomenByMonthAndYearAndDepartmentAndMunicipalityAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $department = null, $municipality = null, $weaponType = null, $databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $sex['sex'] = 'F';

    $Query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_LIFE_Homicides_Rates')
    ->select('month', $this->DB->raw('COUNT(1) as value'))
    ->groupBy('month');

    if(!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    $Query->whereIn('sex', $sex);

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if(!empty($weaponType))
    {
      $Query->whereIn('weapon_type', $weaponType);
    }

    

    $Query->orderByRaw(
      "CASE
      WHEN (month = 'ENERO') then 1
      WHEN (month = 'FEBRERO') then 2
      WHEN (month = 'MARZO') then 3
      WHEN (month = 'ABRIL') then 4
      WHEN (month = 'MAYO') then 5
      WHEN (month = 'JUNIO') then 6
      WHEN (month = 'JULIO') then 7
      WHEN (month = 'AGOSTO') then 8
      WHEN (month = 'SEPTIEMBRE') then 9
      WHEN (month = 'OCTUBRE') then 10
      WHEN (month = 'NOVIEMBRE') then 11
      WHEN (month = 'DICIEMBRE') then 12
      end");

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesByIllegitimateAggressionsOnNaturalPeopleByDepartmentAndMunicipality($department = null, $municipality = null,$databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_LIFE_Aggression_Weapon')
    ->select('year', $this->DB->raw('SUM(deceased)'))
    ->where('deceased', '>', '0')
    ->groupBy('year')
    ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesByIllegitimateAggressionsOnCopsByDepartmentAndMunicipality($department = null, $municipality = null, $databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_LIFE_Aggression_Death')
    ->select('year', $this->DB->raw('SUM(deceased_policemen)'))
    ->where('deceased_policemen', '>', '0')
    ->groupBy('year')
    ->orderBy('year');

    if(!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if(!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesByIllegitimateAggressionsByDeceasedAndYearAndDepartmentAndMunicipality($year=null, $yearFrom  = null, $yearTo = null, $department = null, $municipality = null, $databaseConnectionName = null)
  {
    if (empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_LIFE_Aggression_Weapon')
    ->select('deceased', $this->DB->raw('COUNT(1) as eventos'))
    ->groupBy('deceased')
    ->orderBy('deceased');

    if (!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if (!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if (!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    return new Collection (
      $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentYearFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Homicides_Rates')
        ->select(
          $this->DB->raw('DISTINCT(year) AS year')
        )
        ->orderBy('year')
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function illegitimateAggressionYearFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Weapon')
        ->select(
          $this->DB->raw('DISTINCT(year)')
        )
        ->orderBy('year')
        ->get()
    );
  }

    /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function investigatedOfficersYearFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Officers_Investigated')
        ->select(
          $this->DB->raw('DISTINCT(year)')
        )
        ->orderBy('year')
        ->get()
    );
  }

    /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function investigatedOfficersByYearByDepartmentAndByMunicipality($year = null, $yearFrom = null, $yearTo = null, $department = null, $municipality = null,$databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }


    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Officers_Investigated')
        ->select('crime',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('crime')
        ->orderBy('crime');

        if (!empty($year))
        {
          $Query->whereIn('year', $year);
        }

        if(!empty($yearFrom))
        {
          $Query->where('year', '>=', $yearFrom);
        }
    
        if(!empty($yearTo))
        {
          $Query->where('year', '<=', $yearTo);
        }

        if (!empty($department))
        {
          $Query->whereIn('location_department', $department);
        }
    
        if (!empty($municipality))
        {
          $Query->whereIn('location_municipality', $municipality);
        }

    return new Collection (

        $Query->get()
    );
  }

    /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function aggressionDeathYearFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Death')
        ->select(
          $this->DB->raw('DISTINCT(year)')
        )
        ->orderBy('year')
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Homicides_Rates')
        ->select(
          $this->DB->raw('DISTINCT(sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function investigatedOfficersSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Officers_Investigated')
        ->select(
          $this->DB->raw('DISTINCT(victim_sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Homicides_Rates')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function investigatedOfficersMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Officers_Investigated')
        ->select(
          $this->DB->raw('DISTINCT(location_municipality) AS municipality')
        )
        ->where('location_municipality', '!=', 'DESCONOCIDO')
        ->whereRaw('location_municipality is not null')
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function aggresionDeathMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Death')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function illegitimateAggressionsMunicipalityFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Weapon')
        ->select(
          $this->DB->raw('DISTINCT(municipality) AS municipality')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Homicides_Rates')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function investigatedOfficersDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Officers_Investigated')
        ->select(
          $this->DB->raw('DISTINCT(location_department) AS department')
        )
        ->where('location_department', '!=', 'DESCONOCIDO')
        ->whereRaw('location_department is not null')
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function investigatedOfficersOfficersSexFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Officers_Investigated')
        ->select(
          $this->DB->raw('DISTINCT(officer_sex) AS sex')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function aggressionDeathDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Death')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function illegitimateAggressionsDepartmentFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Weapon')
        ->select(
          $this->DB->raw('DISTINCT(department) AS department')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentWeaponTypeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Homicides_Rates')
        ->select(
          $this->DB->raw('DISTINCT(weapon_type) AS weapon_type')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function investigatedOfficersWeaponTypeFilter($databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    return new Collection (
      $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Officers_Investigated')
        ->select(
          $this->DB->raw('DISTINCT(weapon_type) AS weapon_type')
        )
        ->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $gender = null, $department = null, 
  $municipality = null, $weaponType = null, $minAge = null, $maxAge = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Homicides_Rates')
        ->select('age',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('age')
        ->orderBy('age')
        ->whereBetween('age', [$minAge, $maxAge]);

    if (!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if (!empty($gender))
    {
      $Query->whereIn('sex', $gender);
    }

    if (!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if (!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if (!empty($weaponType))
    {
      $Query->whereIn('weapon_type', $weaponType);
    }

    return new Collection (

        $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentOnWomenByAgeAndYearAndDepartmentAndMunicipalityAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $department = null, $municipality = null, $weaponType = null, 
  $minAge = null, $maxAge = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $sex['sex'] = 'F';

    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Homicides_Rates')
        ->select('age',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('age')
        ->orderBy('age')
        ->whereBetween('age', [$minAge, $maxAge])
        ->whereIn('sex', $sex);

        if (!empty($year))
        {
          $Query->whereIn('year', $year);
        }

        if(!empty($yearFrom))
        {
          $Query->where('year', '>=', $yearFrom);
        }
    
        if(!empty($yearTo))
        {
          $Query->where('year', '<=', $yearTo);
        }
    
        if (!empty($department))
        {
          $Query->whereIn('department', $department);
        }
    
        if (!empty($municipality))
        {
          $Query->whereIn('municipality', $municipality);
        }
    
        if (!empty($weaponType))
        {
          $Query->whereIn('weapon_type', $weaponType);
        }

    return new Collection (

        $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentByTimeAgressionsAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType($year = null, $yearFrom = null, $yearTo = null, $gender = null, $department = null, $municipality = null, $weaponType = null, 
  $fromTime = null, $toTime = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }


    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Homicides_Rates')
        ->select('time_aggression',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('time_aggression')
        ->orderBy('time_aggression')
        ->whereBetween('time_aggression', [$fromTime, $toTime]);

    if (!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if (!empty($gender))
    {
      $Query->whereIn('sex', $gender);
    }

    if (!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if (!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    if (!empty($weaponType))
    {
      $Query->whereIn('weapon_type', $weaponType);
    }

    return new Collection (

        $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesIncidentByIllegitimateAggressionByTimeAndYearAndDepartmentAndMunicipality($year = null, $yearFrom = null, $yearTo = null, $department = null, $municipality = null, $fromTime = null, $toTime = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }


    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Weapon')
        ->select('time',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('time')
        ->orderBy('time')
        ->whereBetween('time', [$fromTime, $toTime]);

    if (!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    if(!empty($yearFrom))
    {
      $Query->where('year', '>=', $yearFrom);
    }

    if(!empty($yearTo))
    {
      $Query->where('year', '<=', $yearTo);
    }

    if (!empty($department))
    {
      $Query->whereIn('department', $department);
    }

    if (!empty($municipality))
    {
      $Query->whereIn('municipality', $municipality);
    }

    return new Collection (

        $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesByIllegitimateAgressionsByYear($department = null, $municipality = null,$databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }


    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Weapon')
        ->select('year',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('year')
        ->orderBy('year');

    if (!empty($department)) 
    {
      $Query->whereIn('department', $department);
    }

    if (!empty($municipality)) 
    {
      $Query->whereIn('municipality', $municipality);
    }


    return new Collection (

        $Query->get()
    );
  }

  public function homicidesByIllegitimateAgressionsOnCopsByYearAndDepartmentAndMunicipality($department = null, $municipality = null,$databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }


    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Death')
        ->select('year',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('year')
        ->orderBy('year');


        if (!empty($department))
        {
          $Query->whereIn('department', $department);
        }
    
        if (!empty($municipality))
        {
          $Query->whereIn('municipality', $municipality);
        }

    return new Collection (

        $Query->get()
    );
  }

  public function policeInvestigatedByCrimesOnYearsByDepartmentAndMunicipality($department = null, $municipality = null,$databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }


    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Officers_Investigated')
        ->select('year',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('year')
        ->orderBy('year');

        if (!empty($department))
        {
          $Query->whereIn('location_department', $department);
        }
    
        if (!empty($municipality))
        {
          $Query->whereIn('location_municipality', $municipality);
        }

    return new Collection (

        $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesByIllegitimateAgressionsByGangAndYearAndDepartmentAndMunicipality($year = null, $yearFrom = null, $yearTo = null, $department = null, $municipality = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }


    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Weapon')
        ->select('gang',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('gang')
        ->orderBy('gang');

        if (!empty($year))
        {
          $Query->whereIn('year', $year);
        }

        if(!empty($yearFrom))
        {
          $Query->where('year', '>=', $yearFrom);
        }
    
        if(!empty($yearTo))
        {
          $Query->where('year', '<=', $yearTo);
        }

        if (!empty($department))
        {
          $Query->whereIn('department', $department);
        }

        if (!empty($municipality))
        {
          $Query->whereIn('municipality', $municipality);
        }

    return new Collection (

        $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function homicidesByIllegitimateAgressionsByWeekDayAndYear($year = null, $yearFrom = null, $yearTo = null, $department = null, $municipality = null,$databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }


    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_LIFE_Aggression_Weapon')
        ->select('day',
          $this->DB->raw('COUNT(1)')
        )
        ->groupBy('day');

        if (!empty($year))
        {
          $Query->whereIn('year', $year);
        }

        if(!empty($yearFrom))
        {
          $Query->where('year', '>=', $yearFrom);
        }
    
        if(!empty($yearTo))
        {
          $Query->where('year', '<=', $yearTo);
        }

        if (!empty($department))
        {
          $Query->whereIn('department', $department);
        }
    
        if (!empty($municipality))
        {
          $Query->whereIn('municipality', $municipality);
        }

        $Query->orderByRaw(
          "CASE
          WHEN (day = 'Lunes') then 1
          WHEN (day = 'Martes') then 2
          WHEN (day = 'Miércoles') then 3
          WHEN (day = 'Jueves') then 4
          WHEN (day = 'Viernes') then 5
          WHEN (day = 'Sábado') then 6
          WHEN (day = 'Domingo') then 7
          end");

    return new Collection (

        $Query->get()
    );
  }

  /**
   * Retrieve filter
   *
   * @param array $sex
   *
   * @return integer
  */
  public function populationByAgeAndYear($year = null, $yearFrom = null, $yearTo = null, $minAge = null, $maxAge = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
        ->table('OADH_POP_Age_Population')
        ->select('age',
          $this->DB->raw('SUM(population)')
        )
        ->groupBy('age')
        ->orderBy('age')
        ->whereBetween('age', [$minAge, $maxAge]);

    if (!empty($year))
    {
      $Query->whereIn('year', $year);
    }

    return new Collection (

        $Query->get()
    );
  }

  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */

  public function policeInvestigatedByHomicideAndFeminicide($department = null, $municipality = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
    ->table('OADH_LIFE_Officers_Investigated')
    ->select('year', 'victim_sex',
    $this->DB->raw('COUNT(1)')
    )
    ->groupBy('year')
    ->groupBy('victim_sex')
    ->orderBy('year')
    ->whereIn('crime', array('homicidio', 'Homicidio', 'HOMICIDIO','feminicidio','Feminicidio','FEMINICIDIO'))
    ->whereNotNull('victim_sex')
    ->whereIn('victim_sex', array('hombre', 'Hombre', 'HOMBRE', 'mujer', 'Mujer', 'MUJER'));

    if (!empty($department))
    {
      $Query->whereIn('location_department', $department);
    }

    if (!empty($municipality))
    {
      $Query->whereIn('location_municipality', $municipality);
    }

    return new Collection(
      $Query->get()
    );
  }
  /**
   * Retrieve data by filters
   *
   * @param  int $id Organization id
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function crimesVictimsByCrimeBySexByAgeRangeByStatus($crime = null, $sex = null, $ageRange = null, $status = null, $databaseConnectionName = null)
  {
    if(empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->databaseConnectionName;
    }

    $Query = $this->DB->connection($databaseConnectionName)
      ->table('OADH_INT_Crimes_Victims AS cv')
      ->select(
        'year',
        $this->DB->raw('COUNT(1) AS count')
      )
      ->groupBy('year')
      ->orderBy('year');

    if(!empty($crime))
    {
      $Query->whereIn('crime', $crime);
    }

    if(!empty($sex))
    {
      $Query->whereIn('sex', $sex);
    }

    if(!empty($ageRange))
    {
      $Query->whereIn('age_range', $ageRange);
    }

    if(!empty($status))
    {
      $Query->whereIn('status', $status);
    }

    return new Collection (
      $Query->get()
    );
  }
}
