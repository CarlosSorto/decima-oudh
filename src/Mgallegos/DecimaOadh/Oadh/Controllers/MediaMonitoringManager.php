<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\NewsManagement\NewsManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement\DigitalMediaManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class MediaMonitoringManager extends Controller {

	/**
	 * News Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\NewsManagement\NewsManagementInterface
	 *
	 */
	protected $NewsManagerService;

	/**
	 * DigitalMedia Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement\DigitalMediaManagementInterface
	 *
	 */
	protected $DigitalMediaManagerService;

	/**
	 * Setting Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface
	 *
	 */
	protected $SettingManagerService;

	/**
	 * Human Right Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManagementInterface
	 *
	 */
	protected $HumanRightManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		NewsManagementInterface $NewsManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		DigitalMediaManagementInterface $DigitalMediaManagerService,
		SettingManagementInterface $SettingManagerService,
		HumanRightManagementInterface $HumanRightManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->NewsManagerService = $NewsManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->DigitalMediaManagerService = $DigitalMediaManagerService;
		$this->SettingManagerService = $SettingManagerService;
		$this->HumanRightManagerService = $HumanRightManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::media-monitoring/media-monitoring')
			->with('prefix', 'oadh-')
			->with('newNewsAction', $this->Session->get('newNewsAction', false))
			->with('editNewsAction', $this->Session->get('editNewsAction', false))
			->with('deleteNewsAction', $this->Session->get('deleteNewsAction', false))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('digitalMedias', $this->DigitalMediaManagerService->getDigitalMedias())
			// ->with('news', $this->NewsManagerService->getNewsWithInformation())
			->with('sections', $this->DigitalMediaManagerService->getSections())
			->with('humanRights', $this->HumanRightManagerService->getHumanRights())
			->with('journalisticGenres', $this->SettingManagerService->getJournalisticGenre())
			->with('populationAffected', $this->SettingManagerService->getPopulationAffected())
			->with('sources', $this->SettingManagerService->getSources())
			->with('sexualDiversities', $this->SettingManagerService->getSexualDiversities())
			->with('gunsTypes', $this->SettingManagerService->getGunsTypes())
			->with('departments', $this->SettingManagerService->getDepartmentAndMunicipality())
			->with('rights', $this->HumanRightManagerService->getOnlyHumanRight())
			->with('topics', $this->HumanRightManagerService->getTopics())
			->with('violatedFacts', $this->HumanRightManagerService->getViolatedFact())
			->with('noteLocations', $this->SettingManagerService->getNoteLocations())
			->with('municipality', $this->SettingManagerService->getMunicipality())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	//Master functions
	public function postGridDataMaster()
	{
		return $this->NewsManagerService->getGridDataMaster($this->Input->all());
	}

	public function postCreateMaster()
	{
		return $this->NewsManagerService->createMaster($this->Input->json()->all());
	}

	public function postUpdateMaster()
	{
		return $this->NewsManagerService->updateMaster($this->Input->json()->all());
	}

	public function postDeleteMaster()
	{
		return $this->NewsManagerService->deleteMaster($this->Input->json()->all());
	}

	public function postNews()
	{
		return $this->NewsManagerService->getNewsInformation($json = true);
	}

	//Human Rights functions
	public function postGridDataHumanRights()
	{
		return $this->NewsManagerService->getGridDataHumanRights($this->Input->all());
	}

	public function postCreateHumanRights()
	{
		return $this->NewsManagerService->createHumanRights($this->Input->json()->all());
	}
	public function postUpdateHumanRights()
	{
		return $this->NewsManagerService->updateHumanRights($this->Input->json()->all());
	}

	public function postDeleteHumanRights()
	{
		return $this->NewsManagerService->deleteHumanRights($this->Input->json()->all());
	}

	//Context
	public function postGridDataContext()
	{
		return $this->NewsManagerService->getGridDataContext($this->Input->all());
	}

	public function postCreateContext()
	{
		return $this->NewsManagerService->createContext($this->Input->json()->all());
	}
	public function postUpdateContext()
	{
		return $this->NewsManagerService->updateContext($this->Input->json()->all());
	}

	public function postDeleteContext()
	{
		return $this->NewsManagerService->deleteContext($this->Input->json()->all());
	}

	//Source
	public function postGridDataSource()
	{
		return $this->NewsManagerService->getGridDataSource($this->Input->all());
	}

	public function postCreateSource()
	{
		return $this->NewsManagerService->createSource($this->Input->json()->all());
	}
	public function postUpdateSource()
	{
		return $this->NewsManagerService->updateSource($this->Input->json()->all());
	}

	public function postDeleteSource()
	{
		return $this->NewsManagerService->deleteSource($this->Input->json()->all());
	}

	//Source
	public function postGridDataVictim()
	{
		return $this->NewsManagerService->getGridDataVictim($this->Input->all());
	}

	public function postGridDataVictimizer()
	{
		return $this->NewsManagerService->getGridDataVictimizer($this->Input->all());
	}

	public function postCreatePeople()
	{
		return $this->NewsManagerService->createPeople($this->Input->json()->all());
	}
	public function postUpdatePeople()
	{
		return $this->NewsManagerService->updatePeople($this->Input->json()->all());
	}

	public function postDeletePeople()
	{
		return $this->NewsManagerService->deletePeople($this->Input->json()->all());
	}

	//Producedure
	public function postGridDataProducedure()
	{
		return $this->NewsManagerService->getGridDataProducedure($this->Input->all());
	}

	public function postCreateProducedure()
	{
		return $this->NewsManagerService->createProducedure($this->Input->json()->all());
	}

	public function postUpdateProducedure()
	{
		return $this->NewsManagerService->updateProducedure($this->Input->json()->all());
	}

	public function postDeleteProducedure()
	{
		return $this->NewsManagerService->deleteProducedure($this->Input->json()->all());
	}

	//News News functions
	public function postGridDataNewsNews()
	{
		return $this->NewsManagerService->getGridDataNewsNews($this->Input->all());
	}

	public function postCreateNewsNews()
	{
		return $this->NewsManagerService->createNewsNews($this->Input->json()->all());
	}

	public function postDeleteNewsNews()
	{
		return $this->NewsManagerService->deleteNewsNews($this->Input->json()->all());
	}

	public function postSmtRows()
	{
		return $this->NewsManagerService->getSearchModalTableRows(null, $this->Input->json()->all(), true);
	}

}
