<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\PopulationUploaderManagement\PopulationUploaderManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class PopulationUploaderManager extends Controller {


	/**
	 * PopulationUpload Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\PopulationUploadManagement\PopulationUploadManagementInterface
	 *
	 */
	protected $PopulationUploaderManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		PopulationUploaderManagementInterface $PopulationUploaderManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->PopulationUploadManagerService = $PopulationUploaderManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	//AgePopulation
	public function postAgePopulationTempGridData()
	{
		return $this->PopulationUploadManagerService->getAgePopulationTempGridData($this->Input->all());
	}

	public function postAgePopulationProdGridData()
	{
		return $this->PopulationUploadManagerService->getAgePopulationGridData($this->Input->all());
	}

	public function postAgePopulationProcess()
	{
		return $this->PopulationUploadManagerService->agePopulationProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAgePopulationCopyToProduction()
	{
		return $this->PopulationUploadManagerService->agePopulationCopyToProduction($this->Input->json()->all());
	}

	public function postAgePopulationDeleteFromProduction()
	{
		return $this->PopulationUploadManagerService->agePopulationDeleteFromProduction($this->Input->json()->all());
	}

	public function postAgePopulationDeleteFile()
	{
		return $this->PopulationUploadManagerService->agePopulationDeleteFile($this->Input->json()->all());
	}

	//TerritoryPopulation
	public function postTerritoryPopulationTempGridData()
	{
		return $this->PopulationUploadManagerService->getTerritoryPopulationTempGridData($this->Input->all());
	}

	public function postTerritoryPopulationProdGridData()
	{
		return $this->PopulationUploadManagerService->getTerritoryPopulationGridData($this->Input->all());
	}

	public function postTerritoryPopulationProcess()
	{
		return $this->PopulationUploadManagerService->territoryPopulationProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTerritoryPopulationCopyToProduction()
	{
		return $this->PopulationUploadManagerService->territoryPopulationCopyToProduction($this->Input->json()->all());
	}

	public function postTerritoryPopulationDeleteFromProduction()
	{
		return $this->PopulationUploadManagerService->territoryPopulationDeleteFromProduction($this->Input->json()->all());
	}

	public function postTerritoryPopulationDeleteFile()
	{
		return $this->PopulationUploadManagerService->territoryPopulationDeleteFile($this->Input->json()->all());
	}

	//TableName
	public function postTableNameTempGridData()
	{
		return $this->PopulationUploadManagerService->getTableNameTempGridData($this->Input->all());
	}

	public function postTableNameProdGridData()
	{
		return $this->PopulationUploadManagerService->getTableNameGridData($this->Input->all());
	}

	public function postTableNameProcess()
	{
		return $this->PopulationUploadManagerService->tableNameProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTableNameCopyToProduction()
	{
		return $this->PopulationUploadManagerService->tableNameCopyToProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFromProduction()
	{
		return $this->PopulationUploadManagerService->tableNameDeleteFromProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFile()
	{
		return $this->PopulationUploadManagerService->tableNameDeleteFile($this->Input->json()->all());
	}
}
