<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\ReparationUploaderManagement\ReparationUploaderManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class ReparationUploaderManager extends Controller {


	/**
	 * ReparationUpload Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\ReparationUploadManagement\ReparationUploadManagementInterface
	 *
	 */
	protected $ReparationUploaderManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		ReparationUploaderManagementInterface $ReparationUploaderManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->ReparationUploadManagerService = $ReparationUploaderManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	//Allegations
	public function postAllegationsTempGridData()
	{
		return $this->ReparationUploadManagerService->getAllegationsRequestTempGridData($this->Input->all());
	}

	public function postAllegationsProdGridData()
	{
		return $this->ReparationUploadManagerService->getAllegationsRequestGridData($this->Input->all());
	}

	public function postAllegationsProcess()
	{
		return $this->ReparationUploadManagerService->allegationsProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAllegationsCopyToProduction()
	{
		return $this->ReparationUploadManagerService->allegationsCopyToProduction($this->Input->json()->all());
	}

	public function postAllegationsDeleteFromProduction()
	{
		return $this->ReparationUploadManagerService->allegationsDeleteFromProduction($this->Input->json()->all());
	}

	public function postAllegationsDeleteFile()
	{
		return $this->ReparationUploadManagerService->allegationsDeleteFile($this->Input->json()->all());
	}

	//Budget
	public function postBudgetTempGridData()
	{
		return $this->ReparationUploadManagerService->getBudgetRequestTempGridData($this->Input->all());
	}

	public function postBudgetProdGridData()
	{
		return $this->ReparationUploadManagerService->getBudgetRequestGridData($this->Input->all());
	}

	public function postBudgetProcess()
	{
		return $this->ReparationUploadManagerService->budgetProcessSpreadsheet($this->Input->json()->all());
	}

	public function postBudgetCopyToProduction()
	{
		return $this->ReparationUploadManagerService->budgetCopyToProduction($this->Input->json()->all());
	}

	public function postBudgetDeleteFromProduction()
	{
		return $this->ReparationUploadManagerService->budgetDeleteFromProduction($this->Input->json()->all());
	}

	public function postBudgetDeleteFile()
	{
		return $this->ReparationUploadManagerService->budgetDeleteFile($this->Input->json()->all());
	}

	//FisdlBeneficiaries
	public function postFisdlBeneficiariesTempGridData()
	{
		return $this->ReparationUploadManagerService->getFisdlBeneficiariesRequestTempGridData($this->Input->all());
	}

	public function postFisdlBeneficiariesProdGridData()
	{
		return $this->ReparationUploadManagerService->getFisdlBeneficiariesRequestGridData($this->Input->all());
	}

	public function postFisdlBeneficiariesProcess()
	{
		return $this->ReparationUploadManagerService->fisdlBeneficiariesProcessSpreadsheet($this->Input->json()->all());
	}

	public function postFisdlBeneficiariesCopyToProduction()
	{
		return $this->ReparationUploadManagerService->fisdlBeneficiariesCopyToProduction($this->Input->json()->all());
	}

	public function postFisdlBeneficiariesDeleteFromProduction()
	{
		return $this->ReparationUploadManagerService->fisdlBeneficiariesDeleteFromProduction($this->Input->json()->all());
	}

	public function postFisdlBeneficiariesDeleteFile()
	{
		return $this->ReparationUploadManagerService->fisdlBeneficiariesDeleteFile($this->Input->json()->all());
	}

	//Amnesty
	public function postAmnestyTempGridData()
	{
		return $this->ReparationUploadManagerService->getAmnestyRequestTempGridData($this->Input->all());
	}

	public function postAmnestyProdGridData()
	{
		return $this->ReparationUploadManagerService->getAmnestyRequestGridData($this->Input->all());
	}

	public function postAmnestyProcess()
	{
		return $this->ReparationUploadManagerService->amnestyProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAmnestyCopyToProduction()
	{
		return $this->ReparationUploadManagerService->amnestyCopyToProduction($this->Input->json()->all());
	}

	public function postAmnestyDeleteFromProduction()
	{
		return $this->ReparationUploadManagerService->amnestyDeleteFromProduction($this->Input->json()->all());
	}

	public function postAmnestyDeleteFile()
	{
		return $this->ReparationUploadManagerService->amnestyDeleteFile($this->Input->json()->all());
	}

	//TableName
	public function postTableNameTempGridData()
	{
		return $this->ReparationUploadManagerService->getTableNameTempGridData($this->Input->all());
	}

	public function postTableNameProdGridData()
	{
		return $this->ReparationUploadManagerService->getTableNameGridData($this->Input->all());
	}

	public function postTableNameProcess()
	{
		return $this->ReparationUploadManagerService->tableNameProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTableNameCopyToProduction()
	{
		return $this->ReparationUploadManagerService->tableNameCopyToProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFromProduction()
	{
		return $this->ReparationUploadManagerService->tableNameDeleteFromProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFile()
	{
		return $this->ReparationUploadManagerService->tableNameDeleteFile($this->Input->json()->all());
	}
}
