<?php
/**
 * @file
 * Recommendation Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class RecommendationManager extends Controller {

	/**
	 * Recommendation Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManagementInterface
	 *
	 */
	protected $RecommendationManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	protected $SettingManagementInterface;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		RecommendationManagementInterface $RecommendationManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		SettingManagementInterface $SettingManagementInterface,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->RecommendationManagerService = $RecommendationManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->SettingManagementInterface = $SettingManagementInterface;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		$lang = !empty( $this->Input->get('lang') ) ? $this->Input->get('lang') : substr($this->Input->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
		$lang = empty($lang) ? 'es' : $lang;
		$lang = ($lang != 'es' &&  $lang != 'en') ? 'en' : $lang;

		return $this->View->make('decima-oadh::front-end/recommendations')
			->with('lang', $lang)
			->with('recs', $this->RecommendationManagerService->getAdvancedSearchModalTableRows(null, $this->Input->json()->all()))
			->with('systems', $this->RecommendationManagerService->getSystems())
			->with('mechanisms', $this->RecommendationManagerService->getMechanisms())
			->with('institutions', $this->RecommendationManagerService->getInstitutions())
			->with('rights', $this->RecommendationManagerService->getRights())
			->with('keyValues', $this->SettingManagementInterface->getKeyValues( $lang ));
	}

	public function postAdvancedSmtRows()
	{
		$html = $this->View->make('decima-oadh::front-end/recommendation-cards')
			->with('lang', $this->Input->json('lang'))
			->with('recs', $this->RecommendationManagerService->getAdvancedSearchModalTableRows(null, $this->Input->json()->all()))
			->with('keyValues', $this->SettingManagementInterface->getKeyValues( $this->Input->json('lang') ))
			->render();

		return response()->json(array('success' => true, 'html'=>$html));
	}

	public function postdownloadAll()
	{
		return $this->RecommendationManagerService->downloadAllExcel($this->Input->all());
	}

	public function postdownloadSingle()
	{
		return $this->RecommendationManagerService->downloadSinglePdf($this->Input->all());
	}
}
