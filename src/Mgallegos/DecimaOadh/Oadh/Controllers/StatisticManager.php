<?php
/**
 * @file
 * Statistic Manager Controller.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class StatisticManager extends Controller {

	/**
	 * Statistic Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface
	 *
	 */
	protected $SettingManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		SettingManagementInterface $SettingManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->SettingManagerService = $SettingManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::statistics-management')
			->with('prefix', 'oadh-st-')
			->with('newStatisticAction', $this->Session->get('newStatisticAction', false))
			->with('editStatisticAction', $this->Session->get('editStatisticAction', false))
			->with('deleteStatisticAction', $this->Session->get('deleteStatisticAction', false))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	public function postGridDataMaster()
	{
		return $this->SettingManagerService->getGridDataMaster($this->Input->all());
	}

	public function postGridDataDetail()
	{
		return $this->SettingManagerService->getGridDataDetail($this->Input->all());
	}

	public function postGridDataDetail2()
	{
		return $this->SettingManagerService->getGridDataDetail2($this->Input->all());
	}

	public function postGridDataDetail3()
	{
		return $this->SettingManagerService->getGridDataDetail3($this->Input->all());
	}

	public function postGridDataDetail4()
	{
		return $this->SettingManagerService->getGridDataDetail4($this->Input->all());
	}

	// public function postSmtRows()
	// {
	// 	return $this->SettingManagerService->getSearchModalTableRows();
	// }
}
