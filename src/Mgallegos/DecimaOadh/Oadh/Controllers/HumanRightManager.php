<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class HumanRightManager extends Controller {


	/**
	 * HumanRight Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManagementInterface
	 *
	 */
	protected $HumanRightManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		HumanRightManagementInterface $HumanRightManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->HumanRightManagerService = $HumanRightManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::human-right-management')
			->with('newNewsAction', $this->Session->get('newNewsAction', false))
			->with('editNewsAction', $this->Session->get('editNewsAction', false))
			->with('deleteNewsAction', $this->Session->get('deleteNewsAction', false))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('tracings', $this->HumanRightManagerService->getTracings())
			->with('humanRights', $this->HumanRightManagerService->getOnlyHumanRight())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	// functions
	public function postGridData()
	{
		return $this->HumanRightManagerService->getGridData($this->Input->all());
	}

	public function postCreate()
	{
		return $this->HumanRightManagerService->create($this->Input->json()->all());
	}

	public function postUpdate()
	{
		return $this->HumanRightManagerService->update($this->Input->json()->all());
	}

	public function postDelete()
	{
		return $this->HumanRightManagerService->delete($this->Input->json()->all());
	}
}
