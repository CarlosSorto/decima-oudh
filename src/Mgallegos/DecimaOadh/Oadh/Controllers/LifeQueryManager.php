<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\LifeQueryManagement\LifeQueryManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class LifeQueryManager extends Controller {


	/**
	 * LifeQuery Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\LifeQueryManagement\LifeQueryManagementInterface
	 *
	 */
	protected $LifeQueryManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		LifeQueryManagementInterface $LifeQueryManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->LifeQueryManagerService = $LifeQueryManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	public function getHomicidesIncident()
	{
		return $this->LifeQueryManagerService->getHomicides($this->Input->json()->all());
	}

	public function getHomicidesRate()
	{
		return $this->LifeQueryManagerService->getHomicidesRate($this->Input->json()->all());
	}

	public function getWomenHomicidesIncident()
	{
		return $this->LifeQueryManagerService->getWomenHomicides($this->Input->json()->all());
	}

	public function getWomenHomicidesRate()
	{
		return $this->LifeQueryManagerService->getWomenHomicidesRates($this->Input->json()->all());
	}

	public function getIllegimitateAgressionsIncident()
	{
		return $this->LifeQueryManagerService->getIllegitimateAgressions($this->Input->json()->all());
	}

	public function getWeaponTypeAgressions()
	{
		return $this->LifeQueryManagerService->getWeaponTypeAgressions($this->Input->json()->all());
	}

	public function getGenderAgressions()
	{
		return $this->LifeQueryManagerService->getGenderAgressions($this->Input->json()->all());
	}

	public function getHomicidesByAgeRange()
	{
		return $this->LifeQueryManagerService->getHomicidesIncidentByAge($this->Input->json()->all());
	}

	public function getHomicidesRateByAge()
	{
		return $this->LifeQueryManagerService->getHomicidesRateByAge($this->Input->json()->all());
	}
	
	public function getHomicidesOnWomenByAgeAndYear()
	{
		return $this->LifeQueryManagerService->getHomicidesOnWomenByAgeAndYear($this->Input->json()->all());
	}
	
	public function getHomicidesIncidentByTimeAggression()
	{
		return $this->LifeQueryManagerService->getHomicidesIncidentByTimeAggression($this->Input->json()->all());
	}
	
	public function getHomicidesByIllegitimateAggressionByYear()
	{
		return $this->LifeQueryManagerService->getHomicidesByIllegitimateAggressionByYear($this->Input->json()->all());
	}
	
	public function gethomicidesByIllegitimateAgressionsByWeekDayAndYear()
	{
		return $this->LifeQueryManagerService->gethomicidesByIllegitimateAgressionsByWeekDayAndYear($this->Input->json()->all());
	}
	
	public function gethomicidesIncidentByIllegitimateAggressionByTime()
	{
		return $this->LifeQueryManagerService->gethomicidesIncidentByIllegitimateAggressionByTime($this->Input->json()->all());
	}
	
	public function getHomicidesByIllegitimateAgressionsByGang()
	{
		return $this->LifeQueryManagerService->getHomicidesByIllegitimateAgressionsByGang($this->Input->json()->all());
	}
	public function getPoliceInvestigatedByHomicideAndFeminicide()
	{
		return $this->LifeQueryManagerService->getPoliceInvestigatedByHomicideAndFeminicide($this->Input->json()->all());
	}
	
	public function getHomicidesByIllegitimateAgressionsOnCopsByYear()
	{
		return $this->LifeQueryManagerService->getHomicidesByIllegitimateAgressionsOnCopsByYear($this->Input->json()->all());
	}
	
	public function getPoliceInvestigatedByCrimes()
	{
		return $this->LifeQueryManagerService->getPoliceInvestigatedByCrimes($this->Input->json()->all());
	}
	
	public function getHomicidesIncidentByMonth()
	{
		return $this->LifeQueryManagerService->getHomicidesIncidentByMonth($this->Input->json()->all());
	}
	
	public function getHomicidesIncidentOnWomenByMonth()
	{
		return $this->LifeQueryManagerService->getHomicidesIncidentOnWomenByMonth($this->Input->json()->all());
	}
	
	public function getProportionalRatioOnIllegitimateAggressions()
	{
		return $this->LifeQueryManagerService->getProportionalRatioOnIllegitimateAggressions($this->Input->json()->all());
	}
	
	public function homicidesByIllegitimateAggressionsByDeceased()
	{
		return $this->LifeQueryManagerService->homicidesByIllegitimateAggressionsByDeceased($this->Input->json()->all());
	}
	
	public function getOfficersInvestigatedByInvestigationType()
	{
		return $this->LifeQueryManagerService->getOfficersInvestigatedByInvestigationType($this->Input->json()->all());
	}
}
