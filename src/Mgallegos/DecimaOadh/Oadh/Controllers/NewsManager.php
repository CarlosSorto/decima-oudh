<?php
/**
 * @file
 * News Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\NewsManagement\NewsManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement\DigitalMediaManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class NewsManager extends Controller {

	/**
	 * News Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\NewsManagement\NewsManagementInterface
	 *
	 */
	protected $NewsManagerService;

	/**
	 * DigitalMedia Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement\DigitalMediaManagementInterface
	 *
	 */
	protected $DigitalMediaManagerService;

	/**
	 * Human Right Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement\HumanRightManagementInterface
	 *
	 */
	protected $HumanRightManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	* App Manager Service
	*
	* @var Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
	*
	*/
	protected $SettingManagementInterface;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		NewsManagementInterface $NewsManagerService,
		DigitalMediaManagementInterface $DigitalMediaManagerService,
		HumanRightManagementInterface $HumanRightManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		SettingManagementInterface $SettingManagementInterface,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->NewsManagerService = $NewsManagerService;
		$this->DigitalMediaManagerService = $DigitalMediaManagerService;
		$this->HumanRightManagerService = $HumanRightManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->SettingManagementInterface = $SettingManagementInterface;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		$lang = !empty( $this->Input->get('lang') ) ? $this->Input->get('lang') : substr($this->Input->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
		$lang = empty($lang) ? 'es' : $lang;
		$lang = ($lang != 'es' &&  $lang != 'en') ? 'en' : $lang;

		return $this->View->make('decima-oadh::front-end/news')
			->with('lang', $lang)
			->with('news', $this->NewsManagerService->getAdvancedSearchModalTableRows())
			->with('digitalMedias', $this->DigitalMediaManagerService->getFrontendDigitalMedias())
			->with('populations', $this->SettingManagementInterface->getFrontendPopulationAffected())
			->with('sources', $this->SettingManagementInterface->getFrontendSources())
			->with('weapons', $this->SettingManagementInterface->getFrontendGunsTypes())
			->with('rights', $this->HumanRightManagerService->getHumanRightsWithTopicAndViolatedFact())
			->with('departments', $this->SettingManagementInterface->getDepartmentAndMunicipality())
			->with('keyValues', $this->SettingManagementInterface->getKeyValues( $lang ));
	}

	public function postAdvancedSmtRows()
	{
		$html = $this->View->make('decima-oadh::front-end/news-cards')
			->with('lang', $this->Input->json('lang'))
			->with('news', $this->NewsManagerService->getAdvancedSearchModalTableRows(null, $this->Input->json()->all()))
			->with('keyValues', $this->SettingManagementInterface->getKeyValues( $this->Input->json('lang') ))
			->render();

		return response()->json(array('success' => true, 'html'=>$html));
	}

	public function postPressStatisticsRows()
	{
		return $this->NewsManagerService->getPressStatisticsRows($this->Input->json()->all());
	}

	public function postdownloadAll()
	{
		return $this->NewsManagerService->downloadAllExcel($this->Input->all());
	}

	public function postdownloadSingle()
	{
		return $this->NewsManagerService->downloadSinglePdf($this->Input->all());
	}

	public function postdownloadFile()
	{
		return $this->NewsManagerService->downloadFile($this->Input->all());
	}

	public function postDownloadPressStatistic()
	{
		return $this->NewsManagerService->exportPressStatistic($this->Input->all());
	}

	public function postGetTopicsByRight()
	{
		return $this->HumanRightManagerService->getFrontendTopicByParentId($this->Input->json()->all());
	}

	public function postGetViolatedFactByTopic()
	{
		return $this->HumanRightManagerService->getFrontendViolatedFactByParentId($this->Input->json()->all());
	}
}
