<?php
/**
 * @file
 * Right Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\RightManagement\RightManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class RightManager extends Controller {

	/**
	 * Right Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\RightManagement\RightManagementInterface
	 *
	 */
	protected $RightManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		RightManagementInterface $RightManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->RightManagerService = $RightManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::recommendation/right-management')
			->with('prefix', 'oadh-rrm-')
			->with('newRightAction', $this->Session->get('newRightAction', false))
			->with('editRightAction', $this->Session->get('editRightAction', false))
			->with('deleteRightAction', $this->Session->get('deleteRightAction', false))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	public function postGridData()
	{
		return $this->RightManagerService->getGridData($this->Input->all());
	}

	public function postSmtRows()
	{
		return $this->RightManagerService->getSearchModalTableRows();
	}

	public function postCreate()
	{
		return $this->RightManagerService->create($this->Input->json()->all());
	}

	public function postUpdate()
	{
		return $this->RightManagerService->update($this->Input->json()->all());
	}

	public function postDelete()
	{
		return $this->RightManagerService->delete($this->Input->json()->all());
	}
}
