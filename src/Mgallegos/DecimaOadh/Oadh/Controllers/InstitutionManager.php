<?php
/**
 * @file
 * Institution Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\InstitutionManagement\InstitutionManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class InstitutionManager extends Controller {

	/**
	 * Institution Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\InstitutionManagement\InstitutionManagementInterface
	 *
	 */
	protected $InstitutionManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		InstitutionManagementInterface $InstitutionManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->InstitutionManagerService = $InstitutionManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::recommendation/institution-management')
			->with('prefix', 'oadh-rim-')
			->with('newInstitutionAction', $this->Session->get('newInstitutionAction', false))
			->with('editInstitutionAction', $this->Session->get('editInstitutionAction', false))
			->with('deleteInstitutionAction', $this->Session->get('deleteInstitutionAction', false))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	public function postGridData()
	{
		return $this->InstitutionManagerService->getGridData($this->Input->all());
	}

	public function postSmtRows()
	{
		return $this->InstitutionManagerService->getSearchModalTableRows();
	}

	public function postCreate()
	{
		return $this->InstitutionManagerService->create($this->Input->json()->all());
	}

	public function postUpdate()
	{
		return $this->InstitutionManagerService->update($this->Input->json()->all());
	}

	public function postDelete()
	{
		return $this->InstitutionManagerService->delete($this->Input->json()->all());
	}
}
