<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\IntegrityQueryManagement\IntegrityQueryManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class IntegrityQueryManager extends Controller {


	/**
	 * IntegrityQuery Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\IntegrityQueryManagement\IntegrityQueryManagementInterface
	 *
	 */
	protected $IntegrityQueryManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		IntegrityQueryManagementInterface $IntegrityQueryManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->IntegrityQueryManagerService = $IntegrityQueryManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	public function getCrimesVictims()
	{
		return $this->IntegrityQueryManagerService->getCrimesVictims($this->Input->json()->all());
	}

	public function getCrimesAccused()
	{
		return $this->IntegrityQueryManagerService->getCrimesAccused($this->Input->json()->all());
	}

	public function getWorkingOfficials()
	{
		return $this->IntegrityQueryManagerService->getWorkingOfficials($this->Input->json()->all());
	}

	public function getGenderTrafficking()
	{
		return $this->IntegrityQueryManagerService->getGenderTrafficking($this->Input->json()->all());
	}

	public function getTypeTrafficking()
	{
		return $this->IntegrityQueryManagerService->getTypeTrafficking($this->Input->json()->all());
	}

	public function getAgeTrafficking()
	{
		return $this->IntegrityQueryManagerService->getAgeTrafficking($this->Input->json()->all());
	}

	public function getMovementFreedom()
	{
		return $this->IntegrityQueryManagerService->getMovementFreedom($this->Input->json()->all());
	}

	public function getAgeWomen()
	{
		return $this->IntegrityQueryManagerService->getAgeWomen($this->Input->json()->all());
	}

	public function getTypeWomen()
	{
		return $this->IntegrityQueryManagerService->getTypeWomen($this->Input->json()->all());
	}

	public function getStateWomen()
	{
		return $this->IntegrityQueryManagerService->getStateWomen($this->Input->json()->all());
	}

	public function getTypeSexual()
	{
		return $this->IntegrityQueryManagerService->getTypeSexual($this->Input->json()->all());
	}

	public function getGenderSexual()
	{
		return $this->IntegrityQueryManagerService->getGenderSexual($this->Input->json()->all());
	}

	public function getAgeSexual()
	{
		return $this->IntegrityQueryManagerService->getAgeSexual($this->Input->json()->all());
	}

	public function getStateInjuries()
	{
		return $this->IntegrityQueryManagerService->getStateInjuries($this->Input->json()->all());
	}


	public function getAgeInjuries()
	{
		return $this->IntegrityQueryManagerService->getAgeInjuries($this->Input->json()->all());
	}

	public function getScheduleInjuries()
	{
		return $this->IntegrityQueryManagerService->getScheduleInjuries($this->Input->json()->all());
	}

	public function getAgentInvestigation()
	{
		return $this->IntegrityQueryManagerService->getAgentInvestigation($this->Input->json()->all());
	}

	public function getIntegrityViolation()
	{
		return $this->IntegrityQueryManagerService->getIntegrityViolation($this->Input->json()->all());
	}

	//TableName
	public function postTableNameTempGridData()
	{
		return $this->IntegrityQueryManagerService->getTableNameTempGridData($this->Input->all());
	}

	public function postTableNameProdGridData()
	{
		return $this->IntegrityQueryManagerService->getTableNameGridData($this->Input->all());
	}

	public function postTableNameProcess()
	{
		return $this->IntegrityQueryManagerService->tableNameProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTableNameCopyToProduction()
	{
		return $this->IntegrityQueryManagerService->tableNameCopyToProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFromProduction()
	{
		return $this->IntegrityQueryManagerService->tableNameDeleteFromProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFile()
	{
		return $this->IntegrityQueryManagerService->tableNameDeleteFile($this->Input->json()->all());
	}
}
