<?php
/**
 * @file
 * Activity Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\ActivityManagement\ActivityManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class ActivityManager extends Controller {

	/**
	 * Activity Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\ActivityManagement\ActivityManagementInterface
	 *
	 */
	protected $ActivityManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		ActivityManagementInterface $ActivityManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->ActivityManagerService = $ActivityManagerService;

		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::activity-management')
			->with('prefix', 'oadh-am-')
			->with('newActivityAction', $this->Session->get('newActivityAction', false))
			->with('editActivityAction', $this->Session->get('editActivityAction', false))
			->with('deleteActivityAction', $this->Session->get('deleteActivityAction', false))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	public function postGridDataMaster()
	{
		return $this->ActivityManagerService->getGridDataMaster($this->Input->all());
	}

	public function postGridDataDetail()
	{
		return $this->ActivityManagerService->getGridDataDetail($this->Input->all());
	}

	public function postSmtRows()
	{
		return $this->ActivityManagerService->getSearchModalTableRows();
	}

	public function postCreateMaster()
	{
		return $this->ActivityManagerService->createMaster($this->Input->json()->all());
	}

	public function postCreateDetail()
	{
		return $this->ActivityManagerService->createDetail($this->Input->json()->all());
	}

	public function postUpdateMaster()
	{
		return $this->ActivityManagerService->updateMaster($this->Input->json()->all());
	}

	public function postUpdateDetail()
	{
		return $this->ActivityManagerService->updateDetail($this->Input->json()->all());
	}

	public function postDeleteMaster()
	{
		return $this->ActivityManagerService->deleteMaster($this->Input->json()->all());
	}

	public function postDeleteDetails()
	{
		return $this->ActivityManagerService->deleteDetails($this->Input->json()->all());
	}
}
