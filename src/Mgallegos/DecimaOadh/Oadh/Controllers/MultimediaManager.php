<?php
/**
 * @file
 * Multimedia Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\MultimediaManagement\MultimediaManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class MultimediaManager extends Controller {

	/**
	 * Multimedia Manager Service
	 *
	 * @var Vendor\DecimaModule\Module\Services\MultimediaManagement\MultimediaManagementInterface
	 *
	 */
	protected $MultimediaManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		MultimediaManagementInterface $MultimediaManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->MultimediaManagerService = $MultimediaManagerService;

		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::multimedia-management')
			->with('newMultimediaAction', $this->Session->get('newMultimediaAction', false))
			->with('editMultimediaAction', $this->Session->get('editMultimediaAction', false))
			->with('deleteMultimediaAction', $this->Session->get('deleteMultimediaAction', false))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('prefix', 'oadh-mm-')
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	public function postGridData()
	{
		return $this->MultimediaManagerService->getGridData($this->Input->all());
	}

	public function postCreate()
	{
		return $this->MultimediaManagerService->create($this->Input->json()->all());
	}

	public function postUpdate()
	{
		return $this->MultimediaManagerService->update($this->Input->json()->all());
	}

	public function postDelete()
	{
		return $this->MultimediaManagerService->delete($this->Input->json()->all());
	}
}
