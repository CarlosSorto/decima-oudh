<?php
/**
 * @file
 * CompensationQuery Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\CompensationQueryManagement\CompensationQueryManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class CompensationQueryManager extends Controller {

	/**
	 * CompensationQuery Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\CompensationQueryManagement\CompensationQueryManagementInterface
	 *
	 */
	protected $CompensationQueryManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		CompensationQueryManagementInterface $CompensationQueryManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->CompensationQueryManagerService = $CompensationQueryManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getVeteransExCombatantsMgdt()
	{
		return $this->CompensationQueryManagerService->getVeteransExCombatantsByInstitution($this->Input->json()->all(), 'MGDT');
	}

	public function getVeteransExCombatantsFisdl()
	{
		return $this->CompensationQueryManagerService->getVeteransExCombatantsByInstitution($this->Input->json()->all(), 'FISDL');
	}

	public function getVeteransVsVictims()
	{
		return $this->CompensationQueryManagerService->getVeteransVsVictims($this->Input->json()->all());
	}

	public function getMgdtVsFisdl()
	{
		return $this->CompensationQueryManagerService->getMgdtVsFisdl($this->Input->json()->all());
	}

	public function getTotalFisdlBeneficiaries()
	{
		return $this->CompensationQueryManagerService->getFisdlBeneficiaries($this->Input->json()->all());
	}

	public function getNationalSearchComission()
	{
		return $this->CompensationQueryManagerService->getNationalSearchComission($this->Input->json()->all());
	}

	public function getAllegationsCnb()
	{
		return $this->CompensationQueryManagerService->getAllegationsByInstitution($this->Input->json()->all(), 'CNB');
	}

	public function getAllegationsConabusqueda()
	{
		return $this->CompensationQueryManagerService->getAllegationsByInstitution($this->Input->json()->all(), 'CONABÚSQUEDA');
	}
	
	public function getTimeline()
	{
		return $this->CompensationQueryManagerService->getTimeline($this->Input->json()->all());
	}
}
