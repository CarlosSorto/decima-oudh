<?php
/**
 * @file
 * RecommendationImport Manager Controller.
 *
 * All DecimaInventory code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
// use Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class RecommendationImportManager extends Controller {

	/**
	 * RecommendationImport Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManagementInterface
	 *
	 */
	protected $RecommendationManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * Setting Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface
	 *
	 */
	// protected $SettingManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		RecommendationManagementInterface $RecommendationManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		// SettingManagementInterface $SettingManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->RecommendationManagerService = $RecommendationManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		// $this->SettingManagerService = $SettingManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::recommendation/recommendation-import')
			->with('prefix', 'oadh-rrt-')
			// ->with('currentSettingConfiguration', array(
			// 	'cl_folder_id' => 1,
			// 	'app_folder_id' => 1,
			// ))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	public function postTempGridData()
	{
		return $this->RecommendationManagerService->getGridDataImport($this->Input->all());
	}

	public function postProdGridData()
	{
		return $this->RecommendationManagerService->getGridDataMaster($this->Input->all());
	}

	public function postProcess()
	{
		return $this->RecommendationManagerService->recommendationProcessSpreadsheet($this->Input->json()->all());
	}

	public function postCopyToProduction()
	{
		return $this->RecommendationManagerService->recommendationCopyToProduction($this->Input->json()->all());
	}

	public function postDeleteFromProduction()
	{
		return $this->RecommendationManagerService->recommendationDeleteFromProduction($this->Input->json()->all());
	}

	public function postDeleteFile()
	{
		return $this->RecommendationManagerService->recommendationDeleteFile($this->Input->json()->all());
	}
}
