<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\JusticeUploaderManagement\JusticeUploaderManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class JusticeUploaderManager extends Controller {


	/**
	 * JusticeUpload Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\JusticeUploadManagement\JusticeUploadManagementInterface
	 *
	 */
	protected $JusticeUploaderManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		JusticeUploaderManagementInterface $JusticeUploaderManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->JusticeUploadManagerService = $JusticeUploaderManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	//Crimes
	public function postCrimesTempGridData()
	{
		return $this->JusticeUploadManagerService->getCrimeRequestTempGridData($this->Input->all());
	}

	public function postCrimesProdGridData()
	{
		return $this->JusticeUploadManagerService->getCrimeRequestGridData($this->Input->all());
	}

	public function postCrimesProcess()
	{
		return $this->JusticeUploadManagerService->crimeProcessSpreadsheet($this->Input->json()->all());
	}

	public function postCrimesCopyToProduction()
	{
		return $this->JusticeUploadManagerService->crimeCopyToProduction($this->Input->json()->all());
	}

	public function postCrimesDeleteFromProduction()
	{
		return $this->JusticeUploadManagerService->crimeDeleteFromProduction($this->Input->json()->all());
	}

	public function postCrimesDeleteFile()
	{
		return $this->JusticeUploadManagerService->crimeDeleteFile($this->Input->json()->all());
	}

	//ConstitutionalProccess
	public function postConstitutionalProccessTempGridData()
	{
		return $this->JusticeUploadManagerService->getConstitutionalProccessRequestTempGridData($this->Input->all());
	}

	public function postConstitutionalProccessProdGridData()
	{
		return $this->JusticeUploadManagerService->getConstitutionalProccessRequestGridData($this->Input->all());
	}

	public function postConstitutionalProccessProcess()
	{
		return $this->JusticeUploadManagerService->constitutionalProccessProcessSpreadsheet($this->Input->json()->all());
	}

	public function postConstitutionalProccessCopyToProduction()
	{
		return $this->JusticeUploadManagerService->constitutionalProccessCopyToProduction($this->Input->json()->all());
	}

	public function postConstitutionalProccessDeleteFromProduction()
	{
		return $this->JusticeUploadManagerService->constitutionalProccessDeleteFromProduction($this->Input->json()->all());
	}

	public function postConstitutionalProccessDeleteFile()
	{
		return $this->JusticeUploadManagerService->constitutionalProccessDeleteFile($this->Input->json()->all());
	}

	//TableName
	public function postTableNameTempGridData()
	{
		return $this->JusticeUploadManagerService->getTableNameTempGridData($this->Input->all());
	}

	public function postTableNameProdGridData()
	{
		return $this->JusticeUploadManagerService->getTableNameGridData($this->Input->all());
	}

	public function postTableNameProcess()
	{
		return $this->JusticeUploadManagerService->tableNameProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTableNameCopyToProduction()
	{
		return $this->JusticeUploadManagerService->tableNameCopyToProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFromProduction()
	{
		return $this->JusticeUploadManagerService->tableNameDeleteFromProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFile()
	{
		return $this->JusticeUploadManagerService->tableNameDeleteFile($this->Input->json()->all());
	}
}
