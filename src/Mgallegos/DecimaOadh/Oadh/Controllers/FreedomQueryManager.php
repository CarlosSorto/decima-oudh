<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\FreedomQueryManagement\FreedomQueryManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class FreedomQueryManager extends Controller {


	/**
	 * FreedomQuery Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\FreedomQueryManagement\FreedomQueryManagementInterface
	 *
	 */
	protected $FreedomQueryManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		FreedomQueryManagementInterface $FreedomQueryManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->FreedomQueryManagerService = $FreedomQueryManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	public function getHabeasCorpus()
	{
		return $this->FreedomQueryManagerService->getHabeasCorpus($this->Input->json()->all());
	}

	public function getPeopleDetained()
	{
		return $this->FreedomQueryManagerService->getPeopleDetained($this->Input->json()->all());
	}

	public function getCriminalCasesTrials()
	{
		return $this->FreedomQueryManagerService->getCriminalCasesTrials($this->Input->json()->all());
	}

	public function getMassTrials()
	{
		return $this->FreedomQueryManagerService->getMassTrials($this->Input->json()->all());
	}

	public function getProceduralFraud()
	{
		return $this->FreedomQueryManagerService->getProceduralFraud($this->Input->json()->all());
	}

	public function getCaptureOrders()
	{
		return $this->FreedomQueryManagerService->getCaptureOrders($this->Input->json()->all());
	} 

	public function getDisappearancesVictims()
	{
		return $this->FreedomQueryManagerService->getDisappearancesVictims($this->Input->json()->all());
	}

	public function getAccusedLibertyDeprivation()
	{
		return $this->FreedomQueryManagerService->getAccusedLibertyDeprivation($this->Input->json()->all());
	}

	public function getVictimsLibertyDeprivation()
	{
		return $this->FreedomQueryManagerService->getVictimsLibertyDeprivation($this->Input->json()->all());
	}

	public function getForcedDisappearances()
	{
	    return $this->FreedomQueryManagerService->getForcedDisappearances($this->Input->json()->all());
	}

	public function getJailsOccupation()
	{
	    return $this->FreedomQueryManagerService->getJailsOccupation($this->Input->json()->all());
	}

	public function getDeprivedPersons()
	{
	    return $this->FreedomQueryManagerService->getDeprivedPersons($this->Input->json()->all());
	}

	public function getAcuteDiseases()
	{
	    return $this->FreedomQueryManagerService->getAcuteDiseases($this->Input->json()->all());
	}

	public function getChronicDiseases()
	{
	    return $this->FreedomQueryManagerService->getChronicDiseases($this->Input->json()->all());
	}
}
