<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\FreedomUploaderManagement\FreedomUploaderManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class FreedomUploaderManager extends Controller {


	/**
	 * FreedomUpload Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\FreedomUploadManagement\FreedomUploadManagementInterface
	 *
	 */
	protected $FreedomUploaderManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		FreedomUploaderManagementInterface $FreedomUploaderManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->FreedomUploadManagerService = $FreedomUploaderManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	//Habeas Corpus Request
	public function postHabeasCorpusRequestTempGridData()
	{
		return $this->FreedomUploadManagerService->gethabeasCorpusRequestTempGridData($this->Input->all());
	}

	public function postHabeasCorpusRequestProdGridData()
	{
		return $this->FreedomUploadManagerService->gethabeasCorpusRequestGridData($this->Input->all());
	}

	public function postHabeasCorpusRequestProcess()
	{
		return $this->FreedomUploadManagerService->habeasCorpusRequestProcessSpreadsheet($this->Input->json()->all());
	}

	public function postHabeasCorpusRequestCopyToProduction()
	{
		return $this->FreedomUploadManagerService->habeasCorpusRequestCopyToProduction($this->Input->json()->all());
	}

	public function postHabeasCorpusRequestDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->habeasCorpusRequestDeleteFromProduction($this->Input->json()->all());
	}

	public function postHabeasCorpusRequestDeleteFile()
	{
		return $this->FreedomUploadManagerService->habeasCorpusRequestDeleteFile($this->Input->json()->all());
	}

	//Peopple detained
	public function postPeopleDetainedTempGridData()
	{
		return $this->FreedomUploadManagerService->getpeopleDetainedTempGridData($this->Input->all());
	}

	public function postPeopleDetainedProdGridData()
	{
		return $this->FreedomUploadManagerService->getpeopleDetainedGridData($this->Input->all());
	}

	public function postPeopleDetainedProcess()
	{
		return $this->FreedomUploadManagerService->peopleDetainedProcessSpreadsheet($this->Input->json()->all());
	}

	public function postPeopleDetainedCopyToProduction()
	{
		return $this->FreedomUploadManagerService->peopleDetainedCopyToProduction($this->Input->json()->all());
	}

	public function postPeopleDetainedDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->peopleDetainedDeleteFromProduction($this->Input->json()->all());
	}

	public function postPeopleDetainedDeleteFile()
	{
		return $this->FreedomUploadManagerService->peopleDetainedDeleteFile($this->Input->json()->all());
	}

	//Criminal Cases Trials
	public function postCriminalCasesTrialsTempGridData()
	{
		return $this->FreedomUploadManagerService->getcriminalCasesTrialsTempGridData($this->Input->all());
	}

	public function postCriminalCasesTrialsProdGridData()
	{
		return $this->FreedomUploadManagerService->getcriminalCasesTrialsGridData($this->Input->all());
	}

	public function postCriminalCasesTrialsProcess()
	{
		return $this->FreedomUploadManagerService->criminalCasesTrialsProcessSpreadsheet($this->Input->json()->all());
	}

	public function postCriminalCasesTrialsCopyToProduction()
	{
		return $this->FreedomUploadManagerService->criminalCasesTrialsCopyToProduction($this->Input->json()->all());
	}

	public function postCriminalCasesTrialsDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->criminalCasesTrialsDeleteFromProduction($this->Input->json()->all());
	}

	public function postCriminalCasesTrialsDeleteFile()
	{
		return $this->FreedomUploadManagerService->criminalCasesTrialsDeleteFile($this->Input->json()->all());
	}

	//Mass Trials
	public function postMassTrialsTempGridData()
	{
		return $this->FreedomUploadManagerService->getmassTrialsTempGridData($this->Input->all());
	}

	public function postMassTrialsProdGridData()
	{
		return $this->FreedomUploadManagerService->getmassTrialsGridData($this->Input->all());
	}

	public function postMassTrialsProcess()
	{
		return $this->FreedomUploadManagerService->massTrialsProcessSpreadsheet($this->Input->json()->all());
	}

	public function postMassTrialsCopyToProduction()
	{
		return $this->FreedomUploadManagerService->massTrialsCopyToProduction($this->Input->json()->all());
	}

	public function postMassTrialsDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->massTrialsDeleteFromProduction($this->Input->json()->all());
	}

	public function postMassTrialsDeleteFile()
	{
		return $this->FreedomUploadManagerService->massTrialsDeleteFile($this->Input->json()->all());
	}

	//Producedural Fraud
	public function postProceduralFraudTempGridData()
	{
		return $this->FreedomUploadManagerService->getproceduralFraudTempGridData($this->Input->all());
	}

	public function postProceduralFraudProdGridData()
	{
		return $this->FreedomUploadManagerService->getproceduralFraudGridData($this->Input->all());
	}

	public function postProceduralFraudProcess()
	{
		return $this->FreedomUploadManagerService->proceduralFraudProcessSpreadsheet($this->Input->json()->all());
	}

	public function postProceduralFraudCopyToProduction()
	{
		return $this->FreedomUploadManagerService->proceduralFraudCopyToProduction($this->Input->json()->all());
	}

	public function postProceduralFraudDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->proceduralFraudDeleteFromProduction($this->Input->json()->all());
	}

	public function postProceduralFraudDeleteFile()
	{
		return $this->FreedomUploadManagerService->proceduralFraudDeleteFile($this->Input->json()->all());
	}

	//Capture Orders
	public function postCaptureOrdersTempGridData()
	{
		return $this->FreedomUploadManagerService->getcaptureOrdersTempGridData($this->Input->all());
	}

	public function postCaptureOrdersProdGridData()
	{
		return $this->FreedomUploadManagerService->getcaptureOrdersGridData($this->Input->all());
	}

	public function postCaptureOrdersProcess()
	{
		return $this->FreedomUploadManagerService->captureOrdersProcessSpreadsheet($this->Input->json()->all());
	}

	public function postCaptureOrdersCopyToProduction()
	{
		return $this->FreedomUploadManagerService->captureOrdersCopyToProduction($this->Input->json()->all());
	}

	public function postCaptureOrdersDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->captureOrdersDeleteFromProduction($this->Input->json()->all());
	}

	public function postCaptureOrdersDeleteFile()
	{
		return $this->FreedomUploadManagerService->captureOrdersDeleteFile($this->Input->json()->all());
	}

	//Disappearances Victims
	public function postDisapparencesVictimsTempGridData()
	{
		return $this->FreedomUploadManagerService->getdisappearancesVictimsTempGridData($this->Input->all());
	}

	public function postDisapparencesVictimsProdGridData()
	{
		return $this->FreedomUploadManagerService->getdisappearancesVictimsGridData($this->Input->all());
	}

	public function postDisapparencesVictimsProcess()
	{
		return $this->FreedomUploadManagerService->disappearancesVictimsProcessSpreadsheet($this->Input->json()->all());
	}

	public function postDisapparencesVictimsCopyToProduction()
	{
		return $this->FreedomUploadManagerService->disappearancesVictimsCopyToProduction($this->Input->json()->all());
	}

	public function postDisapparencesVictimsDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->disappearancesVictimsDeleteFromProduction($this->Input->json()->all());
	}

	public function postDisapparencesVictimsDeleteFile()
	{
		return $this->FreedomUploadManagerService->disappearancesVictimsDeleteFile($this->Input->json()->all());
	}

	//Accused Liberty Deprivation
	public function postAccusedLibertyDeprivationTempGridData()
	{
		return $this->FreedomUploadManagerService->getaccusedLibertyDeprivationTempGridData($this->Input->all());
	}

	public function postAccusedLibertyDeprivationProdGridData()
	{
		return $this->FreedomUploadManagerService->getaccusedLibertyDeprivationGridData($this->Input->all());
	}

	public function postAccusedLibertyDeprivationProcess()
	{
		return $this->FreedomUploadManagerService->accusedLibertyDeprivationProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAccusedLibertyDeprivationCopyToProduction()
	{
		return $this->FreedomUploadManagerService->accusedLibertyDeprivationCopyToProduction($this->Input->json()->all());
	}

	public function postAccusedLibertyDeprivationDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->accusedLibertyDeprivationDeleteFromProduction($this->Input->json()->all());
	}

	public function postAccusedLibertyDeprivationDeleteFile()
	{
		return $this->FreedomUploadManagerService->accusedLibertyDeprivationDeleteFile($this->Input->json()->all());
	}

	//Victim Liberty Deprivation
	public function postVictimLibertyDeprivationTempGridData()
	{
		return $this->FreedomUploadManagerService->getvictimLibertyDeprivationTempGridData($this->Input->all());
	}

	public function postVictimLibertyDeprivationProdGridData()
	{
		return $this->FreedomUploadManagerService->getvictimLibertyDeprivationGridData($this->Input->all());
	}

	public function postVictimLibertyDeprivationProcess()
	{
		return $this->FreedomUploadManagerService->victimLibertyDeprivationProcessSpreadsheet($this->Input->json()->all());
	}

	public function postVictimLibertyDeprivationCopyToProduction()
	{
		return $this->FreedomUploadManagerService->victimLibertyDeprivationCopyToProduction($this->Input->json()->all());
	}

	public function postVictimLibertyDeprivationDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->victimLibertyDeprivationDeleteFromProduction($this->Input->json()->all());
	}

	public function postVictimLibertyDeprivationDeleteFile()
	{
		return $this->FreedomUploadManagerService->victimLibertyDeprivationDeleteFile($this->Input->json()->all());
	}

	//Forced Disappearances
	public function postForcedDisappearancesTempGridData()
	{
		return $this->FreedomUploadManagerService->getforcedDisappearancesTempGridData($this->Input->all());
	}

	public function postForcedDisappearancesProdGridData()
	{
		return $this->FreedomUploadManagerService->getforcedDisappearancesGridData($this->Input->all());
	}

	public function postForcedDisappearancesProcess()
	{
		return $this->FreedomUploadManagerService->forcedDisappearancesProcessSpreadsheet($this->Input->json()->all());
	}

	public function postForcedDisappearancesCopyToProduction()
	{
		return $this->FreedomUploadManagerService->forcedDisappearancesCopyToProduction($this->Input->json()->all());
	}

	public function postForcedDisappearancesDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->forcedDisappearancesDeleteFromProduction($this->Input->json()->all());
	}

	public function postForcedDisappearancesDeleteFile()
	{
		return $this->FreedomUploadManagerService->forcedDisappearancesDeleteFile($this->Input->json()->all());
	}

	//Jails Occupation
	public function postJailsOccupationTempGridData()
	{
		return $this->FreedomUploadManagerService->getjailsOccupationTempGridData($this->Input->all());
	}

	public function postJailsOccupationProdGridData()
	{
		return $this->FreedomUploadManagerService->getjailsOccupationGridData($this->Input->all());
	}

	public function postJailsOccupationProcess()
	{
		return $this->FreedomUploadManagerService->jailsOccupationProcessSpreadsheet($this->Input->json()->all());
	}

	public function postJailsOccupationCopyToProduction()
	{
		return $this->FreedomUploadManagerService->jailsOccupationCopyToProduction($this->Input->json()->all());
	}

	public function postJailsOccupationDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->jailsOccupationDeleteFromProduction($this->Input->json()->all());
	}

	public function postJailsOccupationDeleteFile()
	{
		return $this->FreedomUploadManagerService->jailsOccupationDeleteFile($this->Input->json()->all());
	}

	//Deprived People
	public function postDeprivedPersonsTempGridData()
	{
		return $this->FreedomUploadManagerService->getdeprivedPersonsTempGridData($this->Input->all());
	}

	public function postDeprivedPersonsProdGridData()
	{
		return $this->FreedomUploadManagerService->getdeprivedPersonsGridData($this->Input->all());
	}

	public function postDeprivedPersonsProcess()
	{
		return $this->FreedomUploadManagerService->deprivedPersonsProcessSpreadsheet($this->Input->json()->all());
	}

	public function postDeprivedPersonsCopyToProduction()
	{
		return $this->FreedomUploadManagerService->deprivedPersonsCopyToProduction($this->Input->json()->all());
	}

	public function postDeprivedPersonsDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->deprivedPersonsDeleteFromProduction($this->Input->json()->all());
	}

	public function postDeprivedPersonsDeleteFile()
	{
		return $this->FreedomUploadManagerService->deprivedPersonsDeleteFile($this->Input->json()->all());
	}

	//acute-disease
	public function postAcuteDiseaseTempGridData()
	{
		return $this->FreedomUploadManagerService->getacuteDiseaseTempGridData($this->Input->all());
	}

	public function postAcuteDiseaseProdGridData()
	{
		return $this->FreedomUploadManagerService->getacuteDiseaseGridData($this->Input->all());
	}

	public function postAcuteDiseaseProcess()
	{
		return $this->FreedomUploadManagerService->acuteDiseaseProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAcuteDiseaseCopyToProduction()
	{
		return $this->FreedomUploadManagerService->acuteDiseaseCopyToProduction($this->Input->json()->all());
	}

	public function postAcuteDiseaseDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->acuteDiseaseDeleteFromProduction($this->Input->json()->all());
	}

	public function postAcuteDiseaseDeleteFile()
	{
		return $this->FreedomUploadManagerService->acuteDiseaseDeleteFile($this->Input->json()->all());
	}

	//Chronic Diseases
	public function postChronicDiseasesTempGridData()
	{
		return $this->FreedomUploadManagerService->getchronicDiseasesTempGridData($this->Input->all());
	}

	public function postChronicDiseasesProdGridData()
	{
		return $this->FreedomUploadManagerService->getchronicDiseasesGridData($this->Input->all());
	}

	public function postChronicDiseasesProcess()
	{
		return $this->FreedomUploadManagerService->chronicDiseasesProcessSpreadsheet($this->Input->json()->all());
	}

	public function postChronicDiseasesCopyToProduction()
	{
		return $this->FreedomUploadManagerService->chronicDiseasesCopyToProduction($this->Input->json()->all());
	}

	public function postChronicDiseasesDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->chronicDiseasesDeleteFromProduction($this->Input->json()->all());
	}

	public function postChronicDiseasesDeleteFile()
	{
		return $this->FreedomUploadManagerService->chronicDiseasesDeleteFile($this->Input->json()->all());
	}

	//TableName
	public function postTableNameTempGridData()
	{
		return $this->FreedomUploadManagerService->getTableNameTempGridData($this->Input->all());
	}

	public function postTableNameProdGridData()
	{
		return $this->FreedomUploadManagerService->getTableNameGridData($this->Input->all());
	}

	public function postTableNameProcess()
	{
		return $this->FreedomUploadManagerService->tableNameProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTableNameCopyToProduction()
	{
		return $this->FreedomUploadManagerService->tableNameCopyToProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFromProduction()
	{
		return $this->FreedomUploadManagerService->tableNameDeleteFromProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFile()
	{
		return $this->FreedomUploadManagerService->tableNameDeleteFile($this->Input->json()->all());
	}
}
