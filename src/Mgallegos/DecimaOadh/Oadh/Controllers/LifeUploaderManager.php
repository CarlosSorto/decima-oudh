<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\LifeUploaderManagement\LifeUploaderManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class LifeUploaderManager extends Controller {


	/**
	 * LifeUpload Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\LifeUploadManagement\LifeUploadManagementInterface
	 *
	 */
	protected $LifeUploaderManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		LifeUploaderManagementInterface $LifeUploaderManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->LifeUploadManagerService = $LifeUploaderManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	//Homicides Rates
	public function postHomicidesRatesTempGridData()
	{
		return $this->LifeUploadManagerService->getHomicidesRatesTempGridData($this->Input->all());
	}

	public function postHomicidesRatesProdGridData()
	{
		return $this->LifeUploadManagerService->getHomicidesRatesGridData($this->Input->all());
	}

	public function postHomicidesRatesProcess()
	{
		return $this->LifeUploadManagerService->homicidesRatesProcessSpreadsheet($this->Input->json()->all());
	}

	public function postHomicidesRatesCopyToProduction()
	{
		return $this->LifeUploadManagerService->homicidesRatesCopyToProduction($this->Input->json()->all());
	}

	public function postHomicidesRatesDeleteFromProduction()
	{
		return $this->LifeUploadManagerService->homicidesRatesDeleteFromProduction($this->Input->json()->all());
	}

	public function postHomicidesRatesDeleteFile()
	{
		return $this->LifeUploadManagerService->homicidesRatesDeleteFile($this->Input->json()->all());
	}

	//Officers Investigated
	public function postOfficersInvestigatedTempGridData()
	{
		return $this->LifeUploadManagerService->getOfficersInvestigatedTempGridData($this->Input->all());
	}

	public function postOfficersInvestigatedProdGridData()
	{
		return $this->LifeUploadManagerService->getOfficersInvestigatedGridData($this->Input->all());
	}

	public function postOfficersInvestigatedProcess()
	{
		return $this->LifeUploadManagerService->officersInvestigatedProcessSpreadsheet($this->Input->json()->all());
	}

	public function postOfficersInvestigatedCopyToProduction()
	{
		return $this->LifeUploadManagerService->officersInvestigatedCopyToProduction($this->Input->json()->all());
	}

	public function postOfficersInvestigatedDeleteFromProduction()
	{
		return $this->LifeUploadManagerService->officersInvestigatedDeleteFromProduction($this->Input->json()->all());
	}

	public function postOfficersInvestigatedDeleteFile()
	{
		return $this->LifeUploadManagerService->officersInvestigatedDeleteFile($this->Input->json()->all());
	}

	//Aggression Weapon
	public function postAggressionWeaponTempGridData()
	{
		return $this->LifeUploadManagerService->getAggressionsWeaponTempGridData($this->Input->all());
	}

	public function postAggressionWeaponProdGridData()
	{
		return $this->LifeUploadManagerService->getAggressionsWeaponGridData($this->Input->all());
	}

	public function postAggressionWeaponProcess()
	{
		return $this->LifeUploadManagerService->aggressionsWeaponProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAggressionWeaponCopyToProduction()
	{
		return $this->LifeUploadManagerService->aggressionsWeaponCopyToProduction($this->Input->json()->all());
	}

	public function postAggressionWeaponDeleteFromProduction()
	{
		return $this->LifeUploadManagerService->aggressionsWeaponDeleteFromProduction($this->Input->json()->all());
	}

	public function postAggressionWeaponDeleteFile()
	{
		return $this->LifeUploadManagerService->aggressionsWeaponDeleteFile($this->Input->json()->all());
	}

	//Aggressions Death

	public function postAggressionsDeathTempGridData()
	{
		return $this->LifeUploadManagerService->getAggressionsDeathTempGridData($this->Input->all());
	}

	public function postAggressionsDeathProdGridData()
	{
		return $this->LifeUploadManagerService->getAggressionsDeathGridData($this->Input->all());
	}

	public function postAggressionsDeathProcess()
	{
		return $this->LifeUploadManagerService->aggressionsDeathProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAggressionsDeathCopyToProduction()
	{
		return $this->LifeUploadManagerService->aggressionsDeathCopyToProduction($this->Input->json()->all());
	}

	public function postAggressionsDeathDeleteFromProduction()
	{
		return $this->LifeUploadManagerService->aggressionsDeathDeleteFromProduction($this->Input->json()->all());
	}

	public function postAggressionsDeathDeleteFile()
	{
		return $this->LifeUploadManagerService->aggressionsDeathDeleteFile($this->Input->json()->all());
	}

	/*
	//TableName
	public function postTableNameTempGridData()
	{
		return $this->LifeUploadManagerService->getTableNameTempGridData($this->Input->all());
	}

	public function postTableNameProdG
	ridData()
	{
		return $this->LifeUploadManagerService->getTableNameGridData($this->Input->all());
	}

	public function postTableNameProcess()
	{
		return $this->LifeUploadManagerService->tableNameProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTableNameCopyToProduction()
	{
		return $this->LifeUploadManagerService->tableNameCopyToProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFromProduction()
	{
		return $this->LifeUploadManagerService->tableNameDeleteFromProduction($this->Input->json()->all());
	}

	public function postTableNameDeleteFile()
	{
		return $this->LifeUploadManagerService->tableNameDeleteFile($this->Input->json()->all());
	}
	*/

}
