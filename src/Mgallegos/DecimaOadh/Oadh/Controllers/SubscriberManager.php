<?php
/**
 * @file
 * Account Manager Controller.
 *
 * All DecimaAccounting code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use App\Kwaai\Organization\Services\OrganizationManagement\OrganizationManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class SubscriberManager extends Controller {

	/**
	 * Setting Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface
	 *
	 */
	protected $SettingManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * Organization Manager Service
	 *
	 * @var App\Kwaai\Organization\Services\OrganizationManagement\OrganizationManagementInterface
	 *
	 */
	protected $OrganizationManagerService;


	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		SettingManagementInterface $SettingManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		OrganizationManagementInterface $OrganizationManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session)
	{
		$this->SettingManagerService = $SettingManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->OrganizationManagerService = $OrganizationManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::subscriber-management')
			->with('newSubscriberAction', $this->Session->get('newSubscriberAction', false))
			->with('editSubscriberAction', $this->Session->get('editSubscriberAction', false))
			->with('deleteSubscriberAction', $this->Session->get('deleteSubscriberAction', false))
			->with('currentSettingConfiguration', $this->SettingManagerService->getCurrentSettingConfiguration())
			->with('journals', $this->SettingManagerService->getSettingJournals())
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('prefix', 'oadh-s-')
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	public function postGridData()
	{
		return $this->SettingManagerService->getGridData($this->Input->all());
	}
}
