<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\IntegrityUploaderManagement\IntegrityUploaderManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class IntegrityUploaderManager extends Controller {


	/**
	 * IntegrityUpload Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\IntegrityUploadManagement\IntegrityUploadManagementInterface
	 *
	 */
	protected $IntegrityUploaderManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		IntegrityUploaderManagementInterface $IntegrityUploaderManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->IntegrityUploadManagerService = $IntegrityUploaderManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	//Crimes Victims
	public function postCrimesVictimsTempGridData()
	{
		return $this->IntegrityUploadManagerService->getcrimesVictimsTempGridData($this->Input->all());
	}

	public function postCrimesVictimsProdGridData()
	{
		return $this->IntegrityUploadManagerService->getcrimesVictimsGridData($this->Input->all());
	}

	public function postCrimesVictimsProcess()
	{
		return $this->IntegrityUploadManagerService->crimesVictimsProcessSpreadsheet($this->Input->json()->all());
	}

	public function postCrimesVictimsCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->crimesVictimsCopyToProduction($this->Input->json()->all());
	}

	public function postCrimesVictimsDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->crimesVictimsDeleteFromProduction($this->Input->json()->all());
	}

	public function postCrimesVictimsDeleteFile()
	{
		return $this->IntegrityUploadManagerService->crimesVictimsDeleteFile($this->Input->json()->all());
	}

	//Crimes Accused
	public function postCrimesAccusedTempGridData()
	{
		return $this->IntegrityUploadManagerService->getcrimesAccusedTempGridData($this->Input->all());
	}

	public function postCrimesAccusedProdGridData()
	{
		return $this->IntegrityUploadManagerService->getcrimesAccusedGridData($this->Input->all());
	}

	public function postCrimesAccusedProcess()
	{
		return $this->IntegrityUploadManagerService->crimesAccusedProcessSpreadsheet($this->Input->json()->all());
	}

	public function postCrimesAccusedCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->crimesAccusedCopyToProduction($this->Input->json()->all());
	}

	public function postCrimesAccusedDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->crimesAccusedDeleteFromProduction($this->Input->json()->all());
	}

	public function postCrimesAccusedDeleteFile()
	{
		return $this->IntegrityUploadManagerService->crimesAccusedDeleteFile($this->Input->json()->all());
	}

	//Working Officials
	public function postWorkingOfficialsTempGridData()
	{
		return $this->IntegrityUploadManagerService->getworkingOfficialsTempGridData($this->Input->all());
	}

	public function postWorkingOfficialsProdGridData()
	{
		return $this->IntegrityUploadManagerService->getworkingOfficialsGridData($this->Input->all());
	}

	public function postWorkingOfficialsProcess()
	{
		return $this->IntegrityUploadManagerService->workingOfficialsProcessSpreadsheet($this->Input->json()->all());
	}

	public function postWorkingOfficialsCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->workingOfficialsCopyToProduction($this->Input->json()->all());
	}

	public function postWorkingOfficialsDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->workingOfficialsDeleteFromProduction($this->Input->json()->all());
	}

	public function postWorkingOfficialsDeleteFile()
	{
		return $this->IntegrityUploadManagerService->workingOfficialsDeleteFile($this->Input->json()->all());
	}

	//Gender trafficking
	public function postGenderTraffickingTempGridData()
	{
		return $this->IntegrityUploadManagerService->getgenderTraffickingTempGridData($this->Input->all());
	}

	public function postGenderTraffickingProdGridData()
	{
		return $this->IntegrityUploadManagerService->getgenderTraffickingGridData($this->Input->all());
	}

	public function postGenderTraffickingProcess()
	{
		return $this->IntegrityUploadManagerService->genderTraffickingProcessSpreadsheet($this->Input->json()->all());
	}

	public function postGenderTraffickingCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->genderTraffickingCopyToProduction($this->Input->json()->all());
	}

	public function postGenderTraffickingDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->genderTraffickingDeleteFromProduction($this->Input->json()->all());
	}

	public function postGenderTraffickingDeleteFile()
	{
		return $this->IntegrityUploadManagerService->genderTraffickingDeleteFile($this->Input->json()->all());
	}

	//Type trafficking
	public function postTypeTraffickingTempGridData()
	{
		return $this->IntegrityUploadManagerService->gettypeTraffickingTempGridData($this->Input->all());
	}

	public function postTypeTraffickingProdGridData()
	{
		return $this->IntegrityUploadManagerService->gettypeTraffickingGridData($this->Input->all());
	}

	public function postTypeTraffickingProcess()
	{
		return $this->IntegrityUploadManagerService->typeTraffickingProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTypeTraffickingCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->typeTraffickingCopyToProduction($this->Input->json()->all());
	}

	public function postTypeTraffickingDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->typeTraffickingDeleteFromProduction($this->Input->json()->all());
	}

	public function postTypeTraffickingDeleteFile()
	{
		return $this->IntegrityUploadManagerService->typeTraffickingDeleteFile($this->Input->json()->all());
	}

	//Age trafficking
	public function postAgeTraffickingTempGridData()
	{
		return $this->IntegrityUploadManagerService->getageTraffickingTempGridData($this->Input->all());
	}

	public function postAgeTraffickingProdGridData()
	{
		return $this->IntegrityUploadManagerService->getageTraffickingGridData($this->Input->all());
	}

	public function postAgeTraffickingProcess()
	{
		return $this->IntegrityUploadManagerService->ageTraffickingProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAgeTraffickingCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->ageTraffickingCopyToProduction($this->Input->json()->all());
	}

	public function postAgeTraffickingDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->ageTraffickingDeleteFromProduction($this->Input->json()->all());
	}

	public function postAgeTraffickingDeleteFile()
	{
		return $this->IntegrityUploadManagerService->ageTraffickingDeleteFile($this->Input->json()->all());
	}

	//Movement Freedom
	public function postMovementFreedomTempGridData()
	{
		return $this->IntegrityUploadManagerService->getmovementFreedomTempGridData($this->Input->all());
	}

	public function postMovementFreedomProdGridData()
	{
		return $this->IntegrityUploadManagerService->getmovementFreedomGridData($this->Input->all());
	}

	public function postMovementFreedomProcess()
	{
		return $this->IntegrityUploadManagerService->movementFreedomProcessSpreadsheet($this->Input->json()->all());
	}

	public function postMovementFreedomCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->movementFreedomCopyToProduction($this->Input->json()->all());
	}

	public function postMovementFreedomDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->movementFreedomDeleteFromProduction($this->Input->json()->all());
	}

	public function postMovementFreedomDeleteFile()
	{
		return $this->IntegrityUploadManagerService->movementFreedomDeleteFile($this->Input->json()->all());
	}

	//Age Women
	public function postAgeWomenTempGridData()
	{
		return $this->IntegrityUploadManagerService->getageWomenTempGridData($this->Input->all());
	}

	public function postAgeWomenProdGridData()
	{
		return $this->IntegrityUploadManagerService->getageWomenGridData($this->Input->all());
	}

	public function postAgeWomenProcess()
	{
		return $this->IntegrityUploadManagerService->ageWomenProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAgeWomenCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->ageWomenCopyToProduction($this->Input->json()->all());
	}

	public function postAgeWomenDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->ageWomenDeleteFromProduction($this->Input->json()->all());
	}

	public function postAgeWomenDeleteFile()
	{
		return $this->IntegrityUploadManagerService->ageWomenDeleteFile($this->Input->json()->all());
	}

	//Type Women
	public function postTypeWomenTempGridData()
	{
		return $this->IntegrityUploadManagerService->gettypeWomenTempGridData($this->Input->all());
	}

	public function postTypeWomenProdGridData()
	{
		return $this->IntegrityUploadManagerService->gettypeWomenGridData($this->Input->all());
	}

	public function postTypeWomenProcess()
	{
		return $this->IntegrityUploadManagerService->typeWomenProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTypeWomenCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->typeWomenCopyToProduction($this->Input->json()->all());
	}

	public function postTypeWomenDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->typeWomenDeleteFromProduction($this->Input->json()->all());
	}

	public function postTypeWomenDeleteFile()
	{
		return $this->IntegrityUploadManagerService->typeWomenDeleteFile($this->Input->json()->all());
	}

	//State Women
	public function postStateWomenTempGridData()
	{
		return $this->IntegrityUploadManagerService->getstateWomenTempGridData($this->Input->all());
	}

	public function postStateWomenProdGridData()
	{
		return $this->IntegrityUploadManagerService->getstateWomenGridData($this->Input->all());
	}

	public function postStateWomenProcess()
	{
		return $this->IntegrityUploadManagerService->stateWomenProcessSpreadsheet($this->Input->json()->all());
	}

	public function postStateWomenCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->stateWomenCopyToProduction($this->Input->json()->all());
	}

	public function postStateWomenDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->stateWomenDeleteFromProduction($this->Input->json()->all());
	}

	public function postStateWomenDeleteFile()
	{
		return $this->IntegrityUploadManagerService->stateWomenDeleteFile($this->Input->json()->all());
	}

	//Type Sexual
	public function postTypeSexualTempGridData()
	{
		return $this->IntegrityUploadManagerService->gettypeSexualTempGridData($this->Input->all());
	}

	public function postTypeSexualProdGridData()
	{
		return $this->IntegrityUploadManagerService->gettypeSexualGridData($this->Input->all());
	}

	public function postTypeSexualProcess()
	{
		return $this->IntegrityUploadManagerService->typeSexualProcessSpreadsheet($this->Input->json()->all());
	}

	public function postTypeSexualCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->typeSexualCopyToProduction($this->Input->json()->all());
	}

	public function postTypeSexualDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->typeSexualDeleteFromProduction($this->Input->json()->all());
	}

	public function postTypeSexualDeleteFile()
	{
		return $this->IntegrityUploadManagerService->typeSexualDeleteFile($this->Input->json()->all());
	}

	//Type Sexual
	public function postGenderSexualTempGridData()
	{
		return $this->IntegrityUploadManagerService->getgenderSexualTempGridData($this->Input->all());
	}

	public function postGenderSexualProdGridData()
	{
		return $this->IntegrityUploadManagerService->getgenderSexualGridData($this->Input->all());
	}

	public function postGenderSexualProcess()
	{
		return $this->IntegrityUploadManagerService->genderSexualProcessSpreadsheet($this->Input->json()->all());
	}

	public function postGenderSexualCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->genderSexualCopyToProduction($this->Input->json()->all());
	}

	public function postGenderSexualDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->genderSexualDeleteFromProduction($this->Input->json()->all());
	}

	public function postGenderSexualDeleteFile()
	{
		return $this->IntegrityUploadManagerService->genderSexualDeleteFile($this->Input->json()->all());
	}

	//Age Sexual
	public function postAgeSexualTempGridData()
	{
		return $this->IntegrityUploadManagerService->getageSexualTempGridData($this->Input->all());
	}

	public function postAgeSexualProdGridData()
	{
		return $this->IntegrityUploadManagerService->getageSexualGridData($this->Input->all());
	}

	public function postAgeSexualProcess()
	{
		return $this->IntegrityUploadManagerService->ageSexualProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAgeSexualCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->ageSexualCopyToProduction($this->Input->json()->all());
	}

	public function postAgeSexualDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->ageSexualDeleteFromProduction($this->Input->json()->all());
	}

	public function postAgeSexualDeleteFile()
	{
		return $this->IntegrityUploadManagerService->ageSexualDeleteFile($this->Input->json()->all());
	}

	//State Injuries
	public function postStateInjuriesTempGridData()
	{
		return $this->IntegrityUploadManagerService->getstateInjuriesTempGridData($this->Input->all());
	}

	public function postStateInjuriesProdGridData()
	{
		return $this->IntegrityUploadManagerService->getstateInjuriesGridData($this->Input->all());
	}

	public function postStateInjuriesProcess()
	{
		return $this->IntegrityUploadManagerService->stateInjuriesProcessSpreadsheet($this->Input->json()->all());
	}

	public function postStateInjuriesCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->stateInjuriesCopyToProduction($this->Input->json()->all());
	}

	public function postStateInjuriesDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->stateInjuriesDeleteFromProduction($this->Input->json()->all());
	}

	public function postStateInjuriesDeleteFile()
	{
		return $this->IntegrityUploadManagerService->stateInjuriesDeleteFile($this->Input->json()->all());
	}

	//Age Injuries
	public function postAgeInjuriesTempGridData()
	{
		return $this->IntegrityUploadManagerService->getageInjuriesTempGridData($this->Input->all());
	}

	public function postAgeInjuriesProdGridData()
	{
		return $this->IntegrityUploadManagerService->getageInjuriesGridData($this->Input->all());
	}

	public function postAgeInjuriesProcess()
	{
		return $this->IntegrityUploadManagerService->ageInjuriesProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAgeInjuriesCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->ageInjuriesCopyToProduction($this->Input->json()->all());
	}

	public function postAgeInjuriesDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->ageInjuriesDeleteFromProduction($this->Input->json()->all());
	}

	public function postAgeInjuriesDeleteFile()
	{
		return $this->IntegrityUploadManagerService->ageInjuriesDeleteFile($this->Input->json()->all());
	}

	//Age Injuries
	public function postScheduleInjuriesTempGridData()
	{
		return $this->IntegrityUploadManagerService->getscheduleInjuriesTempGridData($this->Input->all());
	}

	public function postScheduleInjuriesProdGridData()
	{
		return $this->IntegrityUploadManagerService->getscheduleInjuriesGridData($this->Input->all());
	}

	public function postScheduleInjuriesProcess()
	{
		return $this->IntegrityUploadManagerService->scheduleInjuriesProcessSpreadsheet($this->Input->json()->all());
	}

	public function postScheduleInjuriesCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->scheduleInjuriesCopyToProduction($this->Input->json()->all());
	}

	public function postScheduleInjuriesDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->scheduleInjuriesDeleteFromProduction($this->Input->json()->all());
	}

	public function postScheduleInjuriesDeleteFile()
	{
		return $this->IntegrityUploadManagerService->scheduleInjuriesDeleteFile($this->Input->json()->all());
	}

	//Agent Investigatioon
	public function postAgentInvestigationTempGridData()
	{
		return $this->IntegrityUploadManagerService->getagentInvestigationTempGridData($this->Input->all());
	}

	public function postAgentInvestigationProdGridData()
	{
		return $this->IntegrityUploadManagerService->getagentInvestigationGridData($this->Input->all());
	}

	public function postAgentInvestigationProcess()
	{
		return $this->IntegrityUploadManagerService->agentInvestigationProcessSpreadsheet($this->Input->json()->all());
	}

	public function postAgentInvestigationCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->agentInvestigationCopyToProduction($this->Input->json()->all());
	}

	public function postAgentInvestigationDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->agentInvestigationDeleteFromProduction($this->Input->json()->all());
	}

	public function postAgentInvestigationDeleteFile()
	{
		return $this->IntegrityUploadManagerService->agentInvestigationDeleteFile($this->Input->json()->all());
	}

	//Integrity Violation
	public function postIntegrityViolationTempGridData()
	{
		return $this->IntegrityUploadManagerService->getintegrityViolationTempGridData($this->Input->all());
	}

	public function postIntegrityViolationProdGridData()
	{
		return $this->IntegrityUploadManagerService->getintegrityViolationGridData($this->Input->all());
	}

	public function postIntegrityViolationProcess()
	{
		return $this->IntegrityUploadManagerService->integrityViolationProcessSpreadsheet($this->Input->json()->all());
	}

	public function postIntegrityViolationCopyToProduction()
	{
		return $this->IntegrityUploadManagerService->integrityViolationCopyToProduction($this->Input->json()->all());
	}

	public function postIntegrityViolationDeleteFromProduction()
	{
		return $this->IntegrityUploadManagerService->integrityViolationDeleteFromProduction($this->Input->json()->all());
	}

	public function postIntegrityViolationDeleteFile()
	{
		return $this->IntegrityUploadManagerService->integrityViolationDeleteFile($this->Input->json()->all());
	}



}
