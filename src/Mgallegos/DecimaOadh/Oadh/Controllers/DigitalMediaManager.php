<?php
/**
 * @file
 * Media Monitoring Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement\DigitalMediaManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class DigitalMediaManager extends Controller {


	/**
	 * DigitalMedia Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement\DigitalMediaManagementInterface
	 *
	 */
	protected $DigitalMediaManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
   *
	 * @var Illuminate\Http\Request
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		DigitalMediaManagementInterface $DigitalMediaManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->UserManagerService = $UserManagerService;

		$this->AppManagerService = $AppManagerService;

		$this->DigitalMediaManagerService = $DigitalMediaManagerService;

		$this->View = $View;

		$this->Input = $Input;

		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::digital-media-management')
			->with('newNewsAction', $this->Session->get('newNewsAction', false))
			->with('editNewsAction', $this->Session->get('editNewsAction', false))
			->with('deleteNewsAction', $this->Session->get('deleteNewsAction', false))
			->with('digitalMedias', $this->DigitalMediaManagerService->getDigitalMedias())
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	// functions
	public function postGridData()
	{
		return $this->DigitalMediaManagerService->getGridData($this->Input->all());
	}

	public function postCreate()
	{
		return $this->DigitalMediaManagerService->create($this->Input->json()->all());
	}

	public function postUpdate()
	{
		return $this->DigitalMediaManagerService->update($this->Input->json()->all());
	}

	public function postDelete()
	{
		return $this->DigitalMediaManagerService->delete($this->Input->json()->all());
	}
}
