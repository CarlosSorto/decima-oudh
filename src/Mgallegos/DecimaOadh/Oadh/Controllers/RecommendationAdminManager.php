<?php
/**
 * @file
 * Recommendation Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\InstitutionManagement\InstitutionManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\RightManagement\RightManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class RecommendationAdminManager extends Controller {

	/**
	 * Recommendation Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement\RecommendationManagementInterface
	 *
	 */
	protected $RecommendationManagerService;

	/**
	 * Institution Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\InstitutionManagement\InstitutionManagementInterface
	 *
	 */
	protected $InstitutionManagerService;

	/**
	 * Right Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\RightManagement\RightManagementInterface
	 *
	 */
	protected $RightManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		RecommendationManagementInterface $RecommendationManagerService,
		InstitutionManagementInterface $InstitutionManagerService,
		RightManagementInterface $RightManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->RecommendationManagerService = $RecommendationManagerService;
		$this->InstitutionManagerService = $InstitutionManagerService;
		$this->RightManagerService = $RightManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getIndex()
	{
		return $this->View->make('decima-oadh::recommendation/recommendation-management')
			->with('prefix', 'oadh-rrm-')
			->with('institutions', $this->InstitutionManagerService->getInstitutions())
			->with('rights', $this->RightManagerService->getRights())
			->with('newRecommendationAction', $this->Session->get('newRecommendationAction', false))
			->with('editRecommendationAction', $this->Session->get('editRecommendationAction', false))
			->with('deleteRecommendationAction', $this->Session->get('deleteRecommendationAction', false))
			->with('appInfo', $this->AppManagerService->getAppInfo())
			->with('userOrganizations', $this->UserManagerService->getUserOrganizations())
			->with('userAppPermissions', $this->UserManagerService->getUserAppPermissions())
			->with('userActions', $this->UserManagerService->getUserActions());
	}

	public function postGridDataMaster()
	{
		return $this->RecommendationManagerService->getGridDataMaster($this->Input->all());
	}

	public function postGridDataInsDetail()
	{
		return $this->RecommendationManagerService->getGridDataInstitution($this->Input->all());
	}

	public function postGridDataRigDetail()
	{
		return $this->RecommendationManagerService->getGridDataRight($this->Input->all());
	}

	public function postSmtRows()
	{
		return $this->RecommendationManagerService->getSearchModalTableRows();
	}

	public function postCreateMaster()
	{
		return $this->RecommendationManagerService->createMaster($this->Input->json()->all());
	}

	public function postCreateInsDetail()
	{
		return $this->RecommendationManagerService->createInsDetail($this->Input->json()->all());
	}

	public function postCreateRigDetail()
	{
		return $this->RecommendationManagerService->createRigDetail($this->Input->json()->all());
	}

	public function postUpdateMaster()
	{
		return $this->RecommendationManagerService->updateMaster($this->Input->json()->all());
	}

	// public function postUpdateDetail()
	// {
	// 	return $this->RecommendationManagerService->updateDetail($this->Input->json()->all());
	// }

	// public function postAuthorizeMaster()
	// {
	// 	return $this->RecommendationManagerService->authorizeMaster($this->Input->json()->all());
	// }

	// public function postVoidMaster()
	// {
	// 	return $this->RecommendationManagerService->voidMaster($this->Input->json()->all());
	// }

	public function postDeleteMaster()
	{
		return $this->RecommendationManagerService->deleteMaster($this->Input->json()->all());
	}

	public function postDeleteInsDetails()
	{
		return $this->RecommendationManagerService->deleteInsDetails($this->Input->json()->all());
	}

	public function postDeleteRigDetails()
	{
		return $this->RecommendationManagerService->deleteRigDetails($this->Input->json()->all());
	}
}
