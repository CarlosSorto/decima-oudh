<?php
/**
 * @file
 * JusticeQuery Manager Controller.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */
namespace Mgallegos\DecimaOadh\Oadh\Controllers;

use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use Mgallegos\DecimaOadh\Oadh\Services\JusticeQueryManagement\JusticeQueryManagementInterface;
use App\Kwaai\Security\Services\UserManagement\UserManagementInterface;
use App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
use Illuminate\View\Factory;
use App\Http\Controllers\Controller;

class JusticeQueryManager extends Controller {

	/**
	 * JusticeQuery Manager Service
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Services\JusticeQueryManagement\JusticeQueryManagementInterface
	 *
	 */
	protected $JusticeQueryManagerService;

	/**
	* User Manager Service
	*
	* @var App\Kwaai\Security\Services\UserManagement\UserManagementInterface
	*
	*/
	protected $UserManagerService;

	/**
	* App Manager Service
	*
	* @var App\Kwaai\Security\Services\AppManagement\AppManagementInterface;
	*
	*/
	protected $AppManagerService;

	/**
	 * View
	 *
	 * @var Illuminate\View\Factory
	 *
	 */
	protected $View;

	/**
	 * Input
	 *
	 * @var Illuminate\Http\Request
	 *
	 */
	protected $Input;

	/**
	 * Session
	 *
	 * @var Illuminate\Session\SessionManager
	 *
	 */
	protected $Session;

	public function __construct(
		JusticeQueryManagementInterface $JusticeQueryManagerService,
		UserManagementInterface $UserManagerService,
		AppManagementInterface $AppManagerService,
		Factory $View,
		Request $Input,
		SessionManager $Session
	)
	{
		$this->JusticeQueryManagerService = $JusticeQueryManagerService;
		$this->UserManagerService = $UserManagerService;
		$this->AppManagerService = $AppManagerService;
		$this->View = $View;
		$this->Input = $Input;
		$this->Session = $Session;
	}

	public function getVictimsOfDomesticViolence()
	{
		return $this->JusticeQueryManagerService->getVictimsByCrimeType($this->Input->json()->all(), 'Violencia Intrafamiliar');
	}

	public function getVictimsInLeiv()
	{
		return $this->JusticeQueryManagerService->getVictimsByCrimeType($this->Input->json()->all(), 'LEIV');
	}

	public function getVictimsSexualAssault()
	{
		return $this->JusticeQueryManagerService->getVictimsByCrimeType($this->Input->json()->all(), 'Sexuales');
	}

	public function getVictimsFeminicides()
	{
		return $this->JusticeQueryManagerService->getVictimsByCrimeType($this->Input->json()->all(), 'Feminicidio');
	}

	public function getDomesticViolenceOnWomenRegisteredByFgr()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByCrimeTypeAndStage($this->Input->json()->all(), 'Violencia Intrafamiliar', 'Ingresadas');
	}

	public function getDomesticViolenceOnWomenProcessedCases()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByCrimeTypeAndStage($this->Input->json()->all(), 'Violencia Intrafamiliar', 'Judicializadas');
	}

	public function getDomesticViolenceOnWomenJudgments()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByMonthCrimeTypeResultAndStage($this->Input->json()->all(), 'Violencia Intrafamiliar', 'Sentencias');
	}

	public function getSexualAssaultOnWomenRegisteredByFgr()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByCrimeTypeAndStage($this->Input->json()->all(), 'Sexuales', 'Ingresadas');
	}

	public function getSexualAssaultOnWomenProcessedCases()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByCrimeTypeAndStage($this->Input->json()->all(), 'Sexuales', 'Judicializadas');
	}

	public function getSexualAssaultOnWomenJudgments()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByMonthCrimeTypeResultAndStage($this->Input->json()->all(), 'Sexuales', 'Sentencias');
	}

	public function getCrimesOnWomenOnLeivRegisteredByFgr()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByCrimeTypeAndStage($this->Input->json()->all(), 'LEIV', 'Ingresadas');
	}

	public function getCrimesOnWomenOnLeivProcessedCases()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByCrimeTypeAndStage($this->Input->json()->all(), 'LEIV', 'Judicializadas');
	}

	public function getCrimesOnWomenOnLeivJudgments()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByMonthCrimeTypeResultAndStage($this->Input->json()->all(), 'LEIV', 'Sentencias');
	}

	public function getFeminicideVictimsRegisteredByFgr()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByCrimeTypeAndStage($this->Input->json()->all(), 'Feminicidio', 'Ingresadas');
	}

	public function getFeminicideVictimsProcessedCases()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByCrimeTypeAndStage($this->Input->json()->all(), 'Feminicidio', 'Judicializadas');
	}

	public function getFeminicideVictimsJudgments()
	{
		return $this->JusticeQueryManagerService->getDomesticViolenceOnWomenByMonthCrimeTypeResultAndStage($this->Input->json()->all(), 'Feminicidio', 'Sentencias');
	}

	public function getTotalAnualDomesticViolence()
	{
		return $this->JusticeQueryManagerService->getTotalAnualDomesticViolenceByCrimeTypeAndYear($this->Input->json()->all(), 'Violencia Intrafamiliar');
	}

	public function getTotalAnualSexualAssault()
	{
		return $this->JusticeQueryManagerService->getTotalAnualDomesticViolenceByCrimeTypeAndYear($this->Input->json()->all(), 'Sexuales');
	}

	public function getTotalAnualOnLeiv()
	{
		return $this->JusticeQueryManagerService->getTotalAnualDomesticViolenceByCrimeTypeAndYear($this->Input->json()->all(), 'LEIV');
	}

	public function getTotalAnualFeminicides()
	{
		return $this->JusticeQueryManagerService->getTotalAnualDomesticViolenceByCrimeTypeAndYear($this->Input->json()->all(), 'Feminicidio');
	}

	public function getProcessedResolvedProtectionCases()
	{
		return $this->JusticeQueryManagerService->getProcessedResolvedProtectionCasesByFields($this->Input->json()->all(), 'protection_committals', 'protection_resolved', array('Amparos ingresos', 'Amparos resueltos'));
	}

	public function getHabeasCorpusResolvedProtectionCases()
	{
		return $this->JusticeQueryManagerService->getProcessedResolvedProtectionCasesByFields($this->Input->json()->all(), 'habeas_corpus_committals', 'habeas_corpus_resolved', array('Hábeas Corpus ingresos', 'Hábeas Corpus resueltos'));
	}

	public function getUnconstitutionalResolvedProtectionCases()
	{
		return $this->JusticeQueryManagerService->getProcessedResolvedProtectionCasesByFields($this->Input->json()->all(), 'unconstitutional_committals', 'unconstitutional_resolved', array('Inconstitucionalidades ingresos', 'Inconstitucionalidades resueltos'));
	}

	public function getAnualComparisonDomesticViolence()
	{
		return $this->JusticeQueryManagerService->getAnualComparisonClusteredByStageFilteredByCrimeType($this->Input->json()->all(), 'Violencia Intrafamiliar');
	}

	public function getAnualComparisonSexualAssault()
	{
		return $this->JusticeQueryManagerService->getAnualComparisonClusteredByStageFilteredByCrimeType($this->Input->json()->all(), 'Sexuales');
	}

	public function getAnualComparisonLeiv()
	{
		return $this->JusticeQueryManagerService->getAnualComparisonClusteredByStageFilteredByCrimeType($this->Input->json()->all(), 'LEIV');
	}

	public function getAnualComparisonFeminicides()
	{
		return $this->JusticeQueryManagerService->getAnualComparisonClusteredByStageFilteredByCrimeType($this->Input->json()->all(), 'Feminicidio');
	}
}
