<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\MenuTableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use App\Kwaai\Security\Module;
use App\Kwaai\Security\Menu;
use App\Kwaai\Security\Permission;
use Illuminate\Database\Seeder;

class Menu3TableSeeder extends Seeder {

	public function run()
	{
		$moduleId = DB::table('SEC_Module')->max('id');

		Menu::create(array('name' => 'CMS', 'lang_key' => 'decima-oadh::menu.cms', 'url' => null, 'icon' => 'fa fa-file-text-o', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		$parentMenuId = DB::table('SEC_Menu')->max('id');

		Menu::create(array('name' => 'Gestión de publicaciones', 'lang_key' => 'decima-oadh::menu.publicationManagement', 'url' => '/ucaoadh/cms/publication-management', 'action_button_id' => 'oadh-pm-btn-close', 'action_lang_key' => 'decima-oadh::menu.publicationManagementAction', 'icon' => 'fa fa-file-pdf-o', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Gestión de actividades', 'lang_key' => 'decima-oadh::menu.activityManagement', 'url' => '/ucaoadh/cms/activity-management', 'action_button_id' => 'oadh-am-btn-close', 'action_lang_key' => 'decima-oadh::menu.activityManagementAction', 'icon' => 'fa fa-file-image-o', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Gestión de videos', 'lang_key' => 'decima-oadh::menu.multimediaManagement', 'url' => '/ucaoadh/cms/multimedia-management', 'action_button_id' => 'oadh-mm-btn-close', 'action_lang_key' => 'decima-oadh::menu.multimediaManagementAction', 'icon' => 'fa fa-file-video-o', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Suscriptores', 'lang_key' => 'decima-oadh::menu.subscribers', 'url' => '/ucaoadh/cms/subscribers', 'action_button_id' => 'oadh-s-btn-close', 'action_lang_key' => 'decima-oadh::menu.subscribersAction', 'icon' => 'fa fa-users', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Estadísticas', 'lang_key' => 'decima-oadh::menu.statistics', 'url' => '/ucaoadh/cms/statistics', 'action_button_id' => 'oadh-st-btn-close', 'action_lang_key' => 'decima-oadh::menu.statisticsAction', 'icon' => 'fa fa-bar-chart', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
	}

}
