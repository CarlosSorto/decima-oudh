<?php

namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class PublicationsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker::create();

    $randomDateH = Carbon::now()->subWeek(rand(10, 55));

    DB::table('OADH_CMS_Publication')->insert(
      [
      'type' => 'B',
      'date' => $randomDateH,
      'title' => $faker->sentence(4),
      'description' => $faker->sentence(100),
      'author' => $faker->name,
      'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
      'image_url' => 'http://lorempixel.com/760/350/people/' . mt_rand(1, 10) . '/',
      'download_url' => 'https://www.soundczech.cz/temp/lorem-ipsum.pdf',
      'is_highlighted' => 1,
      'organization_id' => 1,
      ]);

    DB::table('OADH_CMS_Publication')->insert(
      [
      'type' => 'F',
      'date' => $randomDateH,
      'title' => $faker->sentence(4),
      'description' => $faker->sentence(100),
      'author' => $faker->name,
      'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
      'image_url' => 'http://lorempixel.com/760/350/people/' . mt_rand(1, 10) . '/',
      'download_url' => 'https://www.soundczech.cz/temp/lorem-ipsum.pdf',
      'is_highlighted' => 1,
      'organization_id' => 1,
      ]);

    DB::table('OADH_CMS_Publication')->insert(
      [
      'type' => 'V',
      'date' => $randomDateH,
      'title' => $faker->sentence(4),
      'description' => $faker->sentence(100),
      'author' => $faker->name,
      'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
      'image_url' => 'http://lorempixel.com/760/350/people/' . mt_rand(1, 10) . '/',
      'download_url' => 'https://www.soundczech.cz/temp/lorem-ipsum.pdf',
      'is_highlighted' => 1,
      'organization_id' => 1,
      ]);

    DB::table('OADH_CMS_Publication')->insert(
      [
      'type' => 'I',
      'date' => $randomDateH,
       'title' => $faker->sentence(4),
       'description' => $faker->sentence(100),
       'author' => $faker->name,
       'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
       'image_url' => 'http://lorempixel.com/760/350/people/' . mt_rand(1, 10) . '/',
       'download_url' => 'https://www.soundczech.cz/temp/lorem-ipsum.pdf',
       'is_highlighted' => 1,
       'organization_id' => 1,
       ]);    



    foreach (range(1, 10) as $index)
    {
      $randomDate = Carbon::now()->subWeek(rand(10, 55));

      DB::table('OADH_CMS_Publication')->insert(
        [
        'type' => 'B',
        'date' => $randomDate,
        'title' => $faker->sentence(4),
        'description' => $faker->sentence(100),
        'author' => $faker->name,
        'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
        'image_url' => 'http://lorempixel.com/760/350/people/' . mt_rand(1, 10) . '/',
        'download_url' => 'https://www.soundczech.cz/temp/lorem-ipsum.pdf',
        'is_highlighted' => 0,
        'organization_id' => 1,
        ]);
    }

    foreach (range(1, 10) as $index)
    {
      $randomDate = Carbon::now()->subWeek(rand(10, 55));

      DB::table('OADH_CMS_Publication')->insert(
        [
        'type' => 'V',
        'date' => $randomDate,
        'title' => $faker->sentence(4),
        'description' => $faker->sentence(100),
        'author' => $faker->name,
        'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
        'image_url' => 'http://lorempixel.com/760/350/people/' . mt_rand(1, 10) . '/',
        'download_url' => 'https://www.soundczech.cz/temp/lorem-ipsum.pdf',
        'is_highlighted' => 0,
        'organization_id' => 1,
        ]);
    }

    foreach (range(1, 10) as $index)
    {
      $randomDate = Carbon::now()->subWeek(rand(10, 55));

      DB::table('OADH_CMS_Publication')->insert(
        [
        'type' => 'F',
        'date' => $randomDate,
        'title' => $faker->sentence(4),
        'description' => $faker->sentence(100),
        'author' => $faker->name,
        'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
        'image_url' => 'http://lorempixel.com/760/350/people/' . mt_rand(1, 10) . '/',
        'download_url' => 'https://www.soundczech.cz/temp/lorem-ipsum.pdf',
        'is_highlighted' => 0,
        'organization_id' => 1,
        ]);
    }

    foreach (range(1, 10) as $index)
    {
      $randomDate = Carbon::now()->subWeek(rand(10, 55));

      DB::table('OADH_CMS_Publication')->insert(
        [
        'type' => 'I',
        'date' => $randomDate,
        'title' => $faker->sentence(4),
        'description' => $faker->sentence(100),
        'author' => $faker->name,
        'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
        'image_url' => 'http://lorempixel.com/760/350/people/' . mt_rand(1, 10) . '/',
        'download_url' => 'https://www.soundczech.cz/temp/lorem-ipsum.pdf',
        'is_highlighted' => 0,
        'organization_id' => 1,
        ]);
    }
  }
}
