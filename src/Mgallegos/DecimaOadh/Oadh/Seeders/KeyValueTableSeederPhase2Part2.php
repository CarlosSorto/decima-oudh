<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\KeyValueTablePhase2Part2Seeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Mgallegos\DecimaOadh\Oadh\KeyValue;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class KeyValueTablePhase2Part2Seeder extends Seeder
{

	public function run()
	{
		/**
		 * BLOQUE 2 Fase 2
		*/

		// Derecho a la justicia

		KeyValue::create(
			array(
				'key' => 'J001',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J002',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J003',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J004',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J005',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J006',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J007',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J008',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J009',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J010',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J011',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J012',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J013',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J014',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J015',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J016',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J017',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J018',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J019',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J020',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J021',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J022',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J023',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J024',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J025',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J026',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J027',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J028',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J029',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J030',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J031',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J032',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J033',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J034',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J035',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J036',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J037',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J038',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J039',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J040',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J041',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J042',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J043',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J044',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J045',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J046',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J047',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J048',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J049',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J050',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J051',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J052',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J053',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J054',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J055',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J056',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J057',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J058',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J059',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J060',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J061',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'J062',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);
	}
}
