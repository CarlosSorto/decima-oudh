<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\Menu6TableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;

use App\Kwaai\Security\Module;

use App\Kwaai\Security\Menu;

use App\Kwaai\Security\Permission;

use Illuminate\Database\Seeder;

class Menu6TableSeeder extends Seeder {

	public function run()
	{
		$moduleId = DB::table('SEC_Module')->max('id');
		// Menu::create(array('name' => 'Derecho a la reparación integral de víctimas', 'lang_key' => 'decima-oadh::menu.rightToReparation', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Derecho a la reparación integral', 'lang_key' => 'decima-oadh::menu.rightToReparation', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		$parentMenuId = DB::table('SEC_Menu')->max('id');

		Menu::create(array('name' => 'Presupuestos', 'lang_key' => 'decima-oadh::menu.budget', 'url' => '/ucaoadh/reparation/budget', 'action_button_id' => 'oadh-reparation-bt-btn-close', 'action_lang_key' => 'decima-oadh::menu.budgetAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Denuncias', 'lang_key' => 'decima-oadh::menu.allegations', 'url' => '/ucaoadh/reparation/allegations', 'action_button_id' => 'oadh-reparation-as-btn-close', 'action_lang_key' => 'decima-oadh::menu.allegationsAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Beneficiarios FISDL', 'lang_key' => 'decima-oadh::menu.fisdlBeneficiaries', 'url' => '/ucaoadh/reparation/fisdl-beneficiaries', 'action_button_id' => 'oadh-reparation-fb-btn-close', 'action_lang_key' => 'decima-oadh::menu.fisdlBeneficiariesAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		
		//Menús de derecho a la libertad
		// $parentMenuId = 11;
		//Menús de derecho a la integridad
		// $parentMenuId = 13;
		//Menús de derecho a la vida
		// $parentMenuId = 14;
		//Menús de estimaciones de Población
		// $parentMenuId = 55;
	}

}
