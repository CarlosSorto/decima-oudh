<?php

namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class MultimediaTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker::create();

    $videosUrl = array(
      'https://www.youtube.com/embed/thuViaxRd_w',
      'https://www.youtube.com/embed/6v2L2UGZJAM',
      'https://www.youtube.com/embed/pvuN_WvF1to',
      'https://www.youtube.com/embed/libKVRa01L8',
      'https://www.youtube.com/embed/z8aBZZnv6y8',
      'https://www.youtube.com/embed/bU1QPtOZQZU',
      'https://www.youtube.com/embed/uD4izuDMUQA',
      'https://www.youtube.com/embed/GoW8Tf7hTGA',
      'https://www.youtube.com/embed/HdPzOWlLrbE',
    );

    foreach (range(1, 30) as $index)
    {
      DB::table('OADH_CMS_Multimedia')->insert(
        [
          'title' => $faker->sentence(4),
          'description' => $faker->sentence(15),
          'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
          // 'image_url' => 'http://lorempixel.com/690/390/people/' . mt_rand(1, 10) . '/',
          'image_url' => $videosUrl[mt_rand(0, 8)],
          'is_highlighted' => 0,
          'organization_id' => 1,
        ]
      );
    }

    // foreach (range(1, 30) as $index)
    // {
    //   DB::table('OADH_CMS_Subscription')->insert(
    //     [
    //       'email' => $faker->safeEmail,
    //       'organization_id' => 1,
    //     ]
    //   );
    // }
  }
}
