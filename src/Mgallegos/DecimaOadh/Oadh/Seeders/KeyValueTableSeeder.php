<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\KeyValueTableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Mgallegos\DecimaOadh\Oadh\KeyValue;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class KeyValueTableSeeder extends Seeder
{

	public function run()
	{
		KeyValue::create(
			array(
				'key' => 'H001',
				'value' => 'Observatorio Universitario de Derechos Humanos',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'H002',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'H003',
				'value' => 'Boletines, investigaciones e informes anuales.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'H004',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'H005',
				'value' => 'Boletín informativo OUDH',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'H006',
				'value' => 'mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet.',
				'organization_id' => 1
			)
		);

		// Derecho a la libertad

		KeyValue::create(
			array(
				'key' => 'L001',
				'value' => 'Este indicador muestra el número de personas que solicitaron el recurso constitucional de "Habeas Corpus" o exhibición personal, cuyo objetivo es la verificación, por parte del sistema judicial, que la detención de dichas personas no es ilegal o arbitraria.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L002',
				'value' => 'Este indicador muestra el número de personas a las que se ha decretado detención provisional. El objetivo de la medida es privar de libertad a la persona para asegurar su presencia en un proceso penal, por lo que debe ser considerado como una medida excepcional, y no una regla general.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L003',
				'value' => 'Este indicador muestra el número de casos que ingresan al sistema judicial anualmente. Se trata específicamente de los casos judicializados en materia penal.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L004',
				'value' => 'Este indicador muestra el número de personas que han sido capturados y procesados judicialmente de manera colectiva. Se considera como captura colectiva aquella en donde se detiene a más de diez personas a la vez.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L005',
				'value' => 'Este indicador muestra el número de personas de profesión policía que han sido procesados por cometer los delitos de Fraude Procesal y de Simulación de Delitos. Ambos conceptos son asociados a la aplicación de detenciones ilegales o arbitrarias.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L006',
				'value' => 'Este indicador muestra el número de personas que anualmente han sido reportadas como desaparecidas ante la Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L007',
				'value' => 'Este indicador muestra el número de personas procesadas judicialmente por cometer el delito de Privación de Libertad.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L008',
				'value' => 'Este indicador muestra el número de personas que anualmente han sido víctimas del delito de Privación de Libertad.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L009',
				'value' => 'Este indicador muestra el número de personas procesadas por cometer el delito de Desaparición Forzada. En El Salvador se considera que es un delito contra la humanidad, y para tipificarlo debe cumplir el requisito de ser cometido por un funcionario o agente de gobierno, o por una persona particular bajo instrucción del mismo.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L010',
				'value' => 'Este indicador muestra la situación de capacidad de alojamiento y la ocupación por personas detenidas, al interior de las bartolinas policiales a nivel nacional.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L011',
				'value' => 'Este indicador muestran el número de denuncias registras por transgresiones a diferentes derechos de la población de privados de libertad en el sistema penitenciario.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L012',
				'value' => 'Este indicador muestran las diez enfermedades agudas más frecuentes que han afectado a la población de privados de libertad en el sistema penitenciario.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L013',
				'value' => 'Este indicador muestra el número de personas privadas de libertad que han sido afectadas por enfermedades crónicas al interior del sistema penitenciario.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L014',
				'value' => 'Este indicador muestra el número de personas que anualmente han sido reportadas como desaparecidas ante la Policía Nacional Civil.',
				'organization_id' => 1
			)
		);

		// Derecho a la integridad

		KeyValue::create(
			array(
				'key' => 'I001',
				'value' => 'El indicador se basa en visibilizar las denuncias que recibe la Fiscalía General de la República sobre agresiones y tratos inhumanos que sufren las personas desde una dimensión física y psicológica, sobre todo en lo referente a extorsiones, amenazas y torturas.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I002',
				'value' => 'El indicador pretende tener un dato sobre el comportamiento a través del tiempo de la cantidad de imputados que registra la FGR acerca de agentes PNC y miembros de la FAES.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I003',
				'value' => 'Este indicador pretende conocer el funcionamiento institucional y de investigación respecto a elementos policiales que continuaron laborando durante fases de investigaciones que no habían concluido.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I004',
				'value' => 'El indicador pretende mostrar la cantidad de casos que se dan en torno a los delitos de trata y tráfico de personas como modo de afectación psicológica y emocional a la integridad personal de las víctimas.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I005',
				'value' => 'El indicador pretende mostrar la cantidad de casos que se dan en torno a los delitos de trata y tráfico de personas como modo de afectación psicológica y emocional a la integridad personal de las víctimas.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I006',
				'value' => 'El indicador pretende mostrar la cantidad de casos que se dan en torno a los delitos de trata y tráfico de personas como modo de afectación psicológica y emocional a la integridad personal de las víctimas.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I007',
				'value' => 'El indicador pretende mostrar la cantidad de casos que se dan en torno a los delitos de trata y tráfico de personas como modo de afectación psicológica y emocional a la integridad personal de las víctimas.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I008',
				'value' => 'A partir de la La Ley Especial Integral para una vida Libre de Violencia (LEIV), que se constituyó como un mecanismo de protección, sanción, resguardo y reparación hacia todas las mujeres que son víctimas de tratos y agresiones arbitrarias, el indicador muestra los delitos más comunes que han sido registrados en la Unidad de Atención Especializada para las Mujeres, concretamente aquellos que están vinculados a transgresiones a la integridad física y moral.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I009',
				'value' => 'A partir de la La Ley Especial Integral para una vida Libre de Violencia (LEIV), que se constituyó como un mecanismo de protección, sanción, resguardo y reparación hacia todas las mujeres que son víctimas de tratos y agresiones arbitrarias, el indicador muestra los delitos más comunes que han sido registrados en la Unidad de Atención Especializada para las Mujeres, concretamente aquellos que están vinculados a transgresiones a la integridad física y moral.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I010',
				'value' => 'A partir de la La Ley Especial Integral para una vida Libre de Violencia (LEIV), que se constituyó como un mecanismo de protección, sanción, resguardo y reparación hacia todas las mujeres que son víctimas de tratos y agresiones arbitrarias, el indicador muestra los delitos más comunes que han sido registrados en la Unidad de Atención Especializada para las Mujeres, concretamente aquellos que están vinculados a transgresiones a la integridad física y moral.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I011',
				'value' => 'Este indicador se fundamenta en explorar y analizar los delitos de orden sexual, especialmente aquellos relacionados hacia las mujeres. La violencia y agresión sexual son dos elementos bastante evidentes que se encuentran inmersos en una cultura predominantemente patriarcal, violentando en mayor medida los derechos a la integridad física y moral.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I012',
				'value' => 'Este indicador se fundamenta en explorar y analizar los delitos de orden sexual, especialmente aquellos relacionados hacia las mujeres. La violencia y agresión sexual son dos elementos bastante evidentes que se encuentran inmersos en una cultura predominantemente patriarcal, violentando en mayor medida los derechos a la integridad física y moral.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I013',
				'value' => 'Este indicador se fundamenta en explorar y analizar los delitos de orden sexual, especialmente aquellos relacionados hacia las mujeres. La violencia y agresión sexual son dos elementos bastante evidentes que se encuentran inmersos en una cultura predominantemente patriarcal, violentando en mayor medida los derechos a la integridad física y moral.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I014',
				'value' => 'Este indicador busca llevar un registro del número de agresiones que se registran contra la integridad física de la población.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I015',
				'value' => 'Este indicador busca llevar un registro del número de agresiones que se registran contra la integridad física de la población.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I016',
				'value' => 'Este indicador busca llevar un registro del número de agresiones que se registran contra la integridad física de la población.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I017',
				'value' => 'Este indicador busca llevar un registro del número de agresiones que se registran contra la integridad física de la población.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I018',
				'value' => 'Este indicador busca llevar un registro del número de agresiones que se registran contra la integridad física de la población.',
				'organization_id' => 1
			)
		);
		// Derecho a la vida

		KeyValue::create(
			array(
				'key' => 'V001',
				'value' => 'Número de homicidios a nivel departamental (mapa) y municipal (tablas) con opción para filtrar sexo y año.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V002',
				'value' => 'Número de homicidios por cada 100,000 habitantes a nivel minucipal, departamental y nacional.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V003',
				'value' => 'Número de homicidios por rangos de edad a nivel nacional.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V004',
				'value' => 'Número de homicidios por cada 100,000 habitantes por rangos de edad.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V005',
				'value' => 'Relación de homicidios causado con arma de fuego en relación a otro tipo de armas.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V006',
				'value' => 'Relación de las víctimas de homicidios de sexo masculino y femenino en relación al total de homicidios.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V007',
				'value' => 'Comportamiento de homicidios por mes de año.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V008',
				'value' => 'Rangos horario de incidencia de homicidios.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V009',
				'value' => 'Número total de muertes violentas de mujeres y feminicidios registrados por el Instituto de Medicina Legal.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V010',
				'value' => 'Número de muertes violentas de mujeres y feminicidios por cada 100,000 mujeres.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V011',
				'value' => 'Rangos de edades de las víctimas de muertes violentas de mujeres y feminicidios.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V012',
				'value' => 'Comportamiento de muertes violentas de mujeres y feminicidios por mes del año.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V013',
				'value' => 'Número de "Agresiones ilegítimas" reportadas por la PNC por año.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V014',
				'value' => 'Número de "Agresiones ilegítimas" desagregadas por municipio y departamento.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V015',
				'value' => 'Incidencia de "agresiones ilegítimas" según día de la semana donde se reporta.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V016',
				'value' => 'Número de "agresiones ilegítimas" según las horas de ocurrencia del evento.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V017',
				'value' => 'Número de eventos según el tipo de presunto grupo delincuencial involucrado.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V018',
				'value' => 'mattis vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut consequat semper viverra nam libero justo laoreet sit amet cursus sit amet dictum sit amet.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V019',
				'value' => 'Número de agresiones ilegítimas donde se reportan personal policíal fallecido.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V020',
				'value' => 'Relación proporcional del número de personas fallecidas en "Agresiones ilegítimas" por cada policía fallecido.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V021',
				'value' => 'Se pretende visibilizar la cantidad de agentes que son investigados por el cometimiento de diversos delitos.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V022',
				'value' => 'Principales delitos por los que son investigados.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V023',
				'value' => 'Número de policías investigados por la Inspectoría General de Seguridad Pública por delitos de homicidios y feminicidio.',
				'organization_id' => 1
			)
		);

		/**
		 * BLOQUE 2
		 */

		// FUENTE VIDA
		KeyValue::create(
			array(
				'key' => 'V024',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V025',
				'value' => 'Fuente: Instituto de Medicina Legal y Dirección General de Estadisticas y Censos',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V026',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V027',
				'value' => 'Fuente: Instituto de Medicina Legal y Dirección General de Estadisticas y Censos',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V028',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V029',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V030',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V031',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V032',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V033',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V034',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V035',
				'value' => 'Fuente: Instituto de Medicina Legal',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V036',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V037',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V038',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V039',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V040',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V041',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V042',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V043',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V044',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V045',
				'value' => 'Fuente: Inspectoria General de Seguridad Pública',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V046',
				'value' => 'Fuente: Inspectoria General de Seguridad Pública',
				'organization_id' => 1
			)
		);

		// FUENTE LIBERTAD

		KeyValue::create(
			array(
				'key' => 'L015',
				'value' => 'Fuente: Corte Suprema de Justicia',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L016',
				'value' => 'Fuente: Corte Suprema de Justicia',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L017',
				'value' => 'Fuente: Corte Suprema de Justicia',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L018',
				'value' => 'Fuente: Corte Suprema de Justicia',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L019',
				'value' => 'Fuente: Corte Suprema de Justicia',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L020',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L021',
				'value' => 'Fuente: Fiscalía General de la República',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L022',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L023',
				'value' => 'Fuente: Fiscalía General de la República',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L024',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L025',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L026',
				'value' => 'Fuente: Procuraduría para la Defensa de los Derechos Humanos',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L027',
				'value' => 'Fuente: Dirección General de Centros Penales',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L028',
				'value' => 'Fuente: Dirección General de Centros Penales',
				'organization_id' => 1
			)
		);


		//FUENTE INTEGRIDAD

		KeyValue::create(
			array(
				'key' => 'I019',
				'value' => 'Fuente: Policía Nacional Civil.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I020',
				'value' => 'Fuente: Policía Nacional Civil.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I021',
				'value' => 'Fuente: Policía Nacional Civil.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I022',
				'value' => 'Fuente: Policía Nacional Civil.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I023',
				'value' => 'Fuente: Policía Nacional Civil.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I024',
				'value' => 'Fuente: Policía Nacional Civil.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I025',
				'value' => 'Fuente: Policía Nacional Civil.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I026',
				'value' => 'Fuente: Fiscalía General de la República.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I027',
				'value' => 'Fuente: Fiscalía General de la República.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I028',
				'value' => 'Fuente: Fiscalía General de la República.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I029',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I030',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I031',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I032',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I033',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I034',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I035',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I036',
				'value' => 'Fuente: Policía Nacional Civil',
				'organization_id' => 1
			)
		);

		// FOOTER
		KeyValue::create(
			array(
				'key' => 'P001',
				'value' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis voluptatibus beatae nobis earum, accusantium eligendi amet praesentium voluptates aspernatur ipsum nihil odio aliquam debitis ea esse natus quae alias officia! ',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'P002',
				'value' => 'Contáctanos',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'P003',
				'value' => 'info@oudh.com',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'P004',
				'value' => '(+503) 7777-7777',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'P005',
				'value' => 'Bulevar Los Próceres',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'A001',
				'value' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'A002',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie, tortor vitae vestibulum imperdiet,
        libero tortor bibendum augue, ut rhoncus justo nisi feugiat elit. Nunc odio tortor, egestas a tincidunt semper,
        volutpat eget arcu. Nam elementum turpis leo, euismod euismod leo cursus vel. Nunc nisl nisi, molestie id libero
        non, scelerisque convallis nunc. Etiam quis eleifend orci. Sed congue dolor quis pretium convallis. Suspendisse
        malesuada dapibus diam, ac vehicula lacus porttitor eu. Nullam sit amet euismod risus. Ut ut enim nibh.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'A003',
				'value' => 'Fusce sit amet nulla orci. Integer nibh sem, tristique quis rutrum sed, consectetur sed ligula. Suspendisse ac
        velit lacinia, luctus felis eu, tempus orci. Etiam augue tellus, volutpat ut magna a, pellentesque ultricies
        dolor. Nullam eu fringilla neque, vitae eleifend turpis. Aenean pulvinar felis et leo fermentum, quis tristique
        nisi tincidunt. Proin blandit tempor mauris, vel accumsan mi dignissim id. Donec nec feugiat diam, eget blandit
        arcu. Suspendisse feugiat, sem vitae hendrerit tristique, massa felis sodales nunc, a commodo massa enim eu
        arcu. Mauris vel mattis felis. Vestibulum ac est sapien. Curabitur sed scelerisque nulla. Sed pretium tortor
        eget odio imperdiet, et accumsan risus elementum. Nunc eget pulvinar neque, eget dignissim neque.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'A004',
				'value' => 'Con el apoyo de',
				'organization_id' => 1
			)
		);

		/**
		 * BLOQUE 3
		 */

		 //Integridad

		KeyValue::create(
			array(
				'key' => 'I037',
				'value' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I038',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie, tortor vitae vestibulum imperdiet,
        libero tortor bibendum augue, ut rhoncus justo nisi feugiat elit. Nunc odio tortor, egestas a tincidunt semper,
        volutpat eget arcu. Nam elementum turpis leo, euismod euismod leo cursus vel. Nunc nisl nisi, molestie id libero
        non, scelerisque convallis nunc. Etiam quis eleifend orci. Sed congue dolor quis pretium convallis. Suspendisse
        malesuada dapibus diam, ac vehicula lacus porttitor eu. Nullam sit amet euismod risus. Ut ut enim nibh.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'I039',
				'value' => 'Fusce sit amet nulla orci. Integer nibh sem, tristique quis rutrum sed, consectetur sed ligula. Suspendisse ac
        velit lacinia, luctus felis eu, tempus orci. Etiam augue tellus, volutpat ut magna a, pellentesque ultricies
        dolor. Nullam eu fringilla neque, vitae eleifend turpis. Aenean pulvinar felis et leo fermentum, quis tristique
        nisi tincidunt. Proin blandit tempor mauris, vel accumsan mi dignissim id. Donec nec feugiat diam, eget blandit
        arcu. Suspendisse feugiat, sem vitae hendrerit tristique, massa felis sodales nunc, a commodo massa enim eu
        arcu. Mauris vel mattis felis. Vestibulum ac est sapien. Curabitur sed scelerisque nulla. Sed pretium tortor
        eget odio imperdiet, et accumsan risus elementum. Nunc eget pulvinar neque, eget dignissim neque.',
				'organization_id' => 1
			)
		);

		 //Vida

		KeyValue::create(
			array(
				'key' => 'V047',
				'value' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V048',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie, tortor vitae vestibulum imperdiet,
        libero tortor bibendum augue, ut rhoncus justo nisi feugiat elit. Nunc odio tortor, egestas a tincidunt semper,
        volutpat eget arcu. Nam elementum turpis leo, euismod euismod leo cursus vel. Nunc nisl nisi, molestie id libero
        non, scelerisque convallis nunc. Etiam quis eleifend orci. Sed congue dolor quis pretium convallis. Suspendisse
        malesuada dapibus diam, ac vehicula lacus porttitor eu. Nullam sit amet euismod risus. Ut ut enim nibh.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'V049',
				'value' => 'Fusce sit amet nulla orci. Integer nibh sem, tristique quis rutrum sed, consectetur sed ligula. Suspendisse ac
        velit lacinia, luctus felis eu, tempus orci. Etiam augue tellus, volutpat ut magna a, pellentesque ultricies
        dolor. Nullam eu fringilla neque, vitae eleifend turpis. Aenean pulvinar felis et leo fermentum, quis tristique
        nisi tincidunt. Proin blandit tempor mauris, vel accumsan mi dignissim id. Donec nec feugiat diam, eget blandit
        arcu. Suspendisse feugiat, sem vitae hendrerit tristique, massa felis sodales nunc, a commodo massa enim eu
        arcu. Mauris vel mattis felis. Vestibulum ac est sapien. Curabitur sed scelerisque nulla. Sed pretium tortor
        eget odio imperdiet, et accumsan risus elementum. Nunc eget pulvinar neque, eget dignissim neque.',
				'organization_id' => 1
			)
		);

		 //Libertad

		KeyValue::create(
			array(
				'key' => 'L029',
				'value' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L030',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent molestie, tortor vitae vestibulum imperdiet,
        libero tortor bibendum augue, ut rhoncus justo nisi feugiat elit. Nunc odio tortor, egestas a tincidunt semper,
        volutpat eget arcu. Nam elementum turpis leo, euismod euismod leo cursus vel. Nunc nisl nisi, molestie id libero
        non, scelerisque convallis nunc. Etiam quis eleifend orci. Sed congue dolor quis pretium convallis. Suspendisse
        malesuada dapibus diam, ac vehicula lacus porttitor eu. Nullam sit amet euismod risus. Ut ut enim nibh.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'L031',
				'value' => 'Fusce sit amet nulla orci. Integer nibh sem, tristique quis rutrum sed, consectetur sed ligula. Suspendisse ac
        velit lacinia, luctus felis eu, tempus orci. Etiam augue tellus, volutpat ut magna a, pellentesque ultricies
        dolor. Nullam eu fringilla neque, vitae eleifend turpis. Aenean pulvinar felis et leo fermentum, quis tristique
        nisi tincidunt. Proin blandit tempor mauris, vel accumsan mi dignissim id. Donec nec feugiat diam, eget blandit
        arcu. Suspendisse feugiat, sem vitae hendrerit tristique, massa felis sodales nunc, a commodo massa enim eu
        arcu. Mauris vel mattis felis. Vestibulum ac est sapien. Curabitur sed scelerisque nulla. Sed pretium tortor
        eget odio imperdiet, et accumsan risus elementum. Nunc eget pulvinar neque, eget dignissim neque.',
				'organization_id' => 1
			)
		);

	}
}
