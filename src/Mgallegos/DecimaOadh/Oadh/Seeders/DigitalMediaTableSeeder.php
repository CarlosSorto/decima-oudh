<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\MenuTableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;

use Mgallegos\DecimaOadh\Oadh\DigitalMedia;

use Illuminate\Database\Seeder;

class DigitalMediaTableSeeder extends Seeder {

	public function run()
	{
		DigitalMedia::create(
			array(
				'name' => 'El Diario de Hoy',
				'type' => 'M',
				'abbreviation' => 'EDH',
				'organization_id' => 1
			)
		);

    $parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'En Portada',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

	DigitalMedia::create(
			array(
				'name' => 'Nacional',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Internacional',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Opinión y editoriales',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Negocios',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

	DigitalMedia::create(
			array(
				'name' => 'Comunidades',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Trends',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'EDH Deportes',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);


		DigitalMedia::create(
			array(
				'name' => 'La Prensa Gráfica',
				'type' => 'M',
				'abbreviation' => 'LGP',
				'organization_id' => 1
			)
		);

    $parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'Tema del día',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);


	DigitalMedia::create(
			array(
				'name' => 'Nación',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Política',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Judicial',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Social',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Fotográfica',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Mundo',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Opinión',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Economía',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Departamentos',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Zona Oriente',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Zona Occidente',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Zona Paracentral',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Plus',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'LGP Deportes',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Diario Co-latino',
				'type' => 'M',
				'abbreviation' => 'COL',
				'organization_id' => 1
			)
		);

    $parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'Nacionales',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

	DigitalMedia::create(
			array(
				'name' => 'Internacionales',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Sociedad',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Suplemento deportivo',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Opinión',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Letras y arte cuarto creciente',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Cartelera',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Economía',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Diario El Mundo',
				'type' => 'M',
				'abbreviation' => 'DEM',
				'organization_id' => 1
			)
		);

    $parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'Politica',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'En foco',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Nacional',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Opinión',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
      array(
				'name' => 'Economía',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
      array(
				'name' => 'Internacional',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
      array(
				'name' => 'Deportes',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
      array(
				'name' => 'Entretenimiento',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'El Faro',
				'type' => 'M',
				'abbreviation' => 'PEF',
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'ContraPunto',
				'type' => 'M',
				'abbreviation' => 'PCP',
				'organization_id' => 1
			)
		);

		$parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'Portada',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Opinión',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Política',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Internacionales',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Sociedad',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Economía',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Cultura',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Transparencia Activa',
				'type' => 'M',
				'abbreviation' => 'PTA',
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Revista Factum',
				'type' => 'M',
				'abbreviation' => 'RDF',
				'organization_id' => 1
			)
		);

		$parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'Destacado',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Miscelánea',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Especiales',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Multimedia',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Televisión legislativa',
				'type' => 'M',
				'abbreviation' => 'TVL',
				'organization_id' => 1
			)
		);

		$parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'Noticia',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Sesión plenaria',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Debate',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Opinión',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Otros',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'TVX',
				'type' => 'M',
				'abbreviation' => 'TVX',
				'organization_id' => 1
			)
		);

		$parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'Foco',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Opinión',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Debate',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Encuesta',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Noticia',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Otros',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Canal 10',
				'type' => 'M',
				'abbreviation' => 'C10',
				'organization_id' => 1
			)
		);

		$parent_id = DB::table('OADH_Digital_Media')->max('id');

		DigitalMedia::create(
			array(
				'name' => 'Entrevista',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Noticia',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Debate',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Opinión',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);

		DigitalMedia::create(
			array(
				'name' => 'Otros',
				'type' => 'S',
				'parent_id' => $parent_id,
				'organization_id' => 1
			)
		);
	}
}
