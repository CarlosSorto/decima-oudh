<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\KeyValueTablePhase2Part3Seeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Mgallegos\DecimaOadh\Oadh\KeyValue;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class KeyValueTablePhase2Part3Seeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		/**
		 * BLOQUE 2 Fase 2
		 */

		// Derecho a la justicia

		KeyValue::create(
			array(
				'key' => 'C001',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C002',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C003',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C004',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C005',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C006',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C007',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C008',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C009',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C010',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C011',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C012',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C013',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C014',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C015',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C016',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C017',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C018',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C019',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C020',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C021',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C022',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C023',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C024',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'C025',
				'value' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium, expedita ex. Vitae, natus a! Omnis fuga aspernatur expedita suscipit impedit in sit rerum recusandae iusto deleniti obcaecati accusantium, non minus?',
				'organization_id' => 1
			)
		);
	}
}
