<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\AlterTableSeederPhase2Part1
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Mgallegos\DecimaOadh\Oadh\KeyValue;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class AlterTableSeederPhase2Part1 extends Seeder
{
	public function run()
	{
		DB::statement('
			ALTER TABLE public."SEC_User" 
			ADD COLUMN dashboard_custom_view VARCHAR(255) NULL DEFAULT NULL;
		');
		
		DB::statement('
			ALTER TABLE public."OADH_CMS_Download" ADD COLUMN recommendation_id INTEGER NULL DEFAULT NULL, ADD COLUMN news_id INTEGER NULL DEFAULT NULL;
		');
	}
}
