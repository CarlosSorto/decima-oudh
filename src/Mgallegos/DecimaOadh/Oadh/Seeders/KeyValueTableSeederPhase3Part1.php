<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\KeyValueTablePhase3Part1Seeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Mgallegos\DecimaOadh\Oadh\KeyValue;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class KeyValueTablePhase3Part1Seeder extends Seeder
{

	public function run()
	{
		/**
		 * BLOQUE 1 Fase 3
		*/

		// // Prensa Menú 01
		// KeyValue::create(
		// 	array(
		// 		'key' => 'N003',
		// 		'value' => '¿Qué es la sección?',
		// 		'en_value' => '¿Qué es la sección?',
		// 		'es_value' => '¿Qué es la sección?',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N004',
		// 		'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N005',
		// 		'value' => '¿Cómo usarla?',
		// 		'en_value' => '¿Cómo usarla?',
		// 		'es_value' => '¿Cómo usarla?',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N006',
		// 		'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N007',
		// 		'value' => '¿Porqué es importante?',
		// 		'en_value' => '¿Porqué es importante?',
		// 		'es_value' => '¿Porqué es importante?',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N008',
		// 		'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N009',
		// 		'value' => '¿Qué DDHH y tema hay?',
		// 		'en_value' => '¿Qué DDHH y tema hay?',
		// 		'es_value' => '¿Qué DDHH y tema hay?',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N010',
		// 		'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N011',
		// 		'value' => 'Este es un item',
		// 		'en_value' => 'Este es un item',
		// 		'es_value' => 'Este es un item',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N012',
		// 		'value' => 'Tutorial',
		// 		'en_value' => 'Tutorial',
		// 		'es_value' => 'Tutorial',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N013',
		// 		'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'organization_id' => 1
		// 	)
		// );

		// // Prensa Menú 02
		// KeyValue::create(
		// 	array(
		// 		'key' => 'N014',
		// 		'value' => 'Derechos Prensa',
		// 		'en_value' => 'Derechos Prensa',
		// 		'es_value' => 'Derechos Prensa',
		// 		'organization_id' => 1
		// 	)
		// );

		// KeyValue::create(
		// 	array(
		// 		'key' => 'N015',
		// 		'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
		// 		'organization_id' => 1
		// 	)
		// );

		KeyValue::create(
			array(
				'key' => 'N016',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'N017',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'N018',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'N019',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'N020',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'N021',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'N022',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'en_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'es_value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua.',
				'organization_id' => 1
			)
		);


	}
}
