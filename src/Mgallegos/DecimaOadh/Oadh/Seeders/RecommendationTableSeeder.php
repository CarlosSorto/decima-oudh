<?php

namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

// php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\RecommendationTableSeeder
class RecommendationTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 40) as $key => $value)
		{
			DB::table('OADH_Recommendation')->insert(
					[
					'system' => $faker->text(100),
					'recommendation' => $faker->realText($faker->numberBetween(300, 400)),
					'code' => str_replace(' ', '-', $faker->words(4, true)),
					'date' => $faker->date(),
					'mechanism' => $faker->words(3, true),
					'related_rights' => str_replace(' ', ', ', $faker->words($faker->numberBetween(2, 5), true)),
					'responsible_institutions' => str_replace(' ', ', ', $faker->words($faker->numberBetween(2, 5), true)),
					'source' => $faker->text(100),
					'legal_base' => $faker->realText($faker->numberBetween(300, 600)),
					'observations' => $faker->realText($faker->numberBetween(300, 600)),
					'last_modified_date' => $faker->date(),
					'organization_id' => 1,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now()
				]
			);
		}

		//get each institute from recommendations
		$recs = DB::table('OADH_Recommendation')->get();
		$recsByIdAndInst =  $recsByRight = $result = $result2 = array();

		foreach ($recs as $key => $value) 
		{
			$recsByIdAndInst[] = explode(', ', $value->responsible_institutions);
			$recsByRight[] = explode(', ', $value->related_rights);
		}

		foreach($recsByIdAndInst as $key => $value)
		{
			$result = array_merge($result, $value);
		}

		foreach($recsByRight as $key => $value)
		{
			$result2 = array_merge($result2, $value);
		}

		$resultUnq = array_unique($result);
		$resultUnq2 = array_unique($result2);
		
		foreach($resultUnq as $key => $value)
		{
			DB::table('OADH_Institution')->insert([
				'name' => $value,
				'abbreviation' => mb_substr($value, 0, 2),
				'organization_id' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
			]);
		}
		
		foreach($resultUnq2 as $key => $value)
		{
			DB::table('OADH_Right')->insert([
				'name' => $value,
				'organization_id' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
			]);
		}

		$instsIds = DB::table('OADH_Institution')->pluck('id');
		$rightsIds = DB::table('OADH_Right')->pluck('id');
		$recsIds = DB::table('OADH_Recommendation')->pluck('id');

		foreach ($recsIds as $key => $value) {
			DB::table('OADH_Recommendation_Right')->insert([
				'recommendation_id' => $faker->randomElement($recsIds),
				'right_id' => $faker->randomElement($rightsIds),
				'organization_id' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
			]);
		}

		foreach ($recsIds as $key => $value) {
			DB::table('OADH_Recommendation_Institution')->insert([
				'recommendation_id' => $faker->randomElement($recsIds),
				'institution_id' => $faker->randomElement($instsIds),
				'organization_id' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()
			]);
		}		
	}
}
