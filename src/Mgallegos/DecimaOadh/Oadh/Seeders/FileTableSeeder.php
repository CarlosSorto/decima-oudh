<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\MenuTableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Schema;
use App\Kwaai\Security\Module;
use App\Kwaai\Security\Menu;
use Mgallegos\DecimaFile\File\File;
use App\Kwaai\Security\Permission;
use Illuminate\Database\Seeder;

class FileTableSeeder extends Seeder {

	public function run()
	{
		Schema::disableForeignKeyConstraints();
		DB::statement('TRUNCATE TABLE public."FILE_File" CASCADE;');
		// DB::statement('SELECT setval(\'FILE_File_id_seq\', 0);');
		DB::statement('ALTER SEQUENCE "FILE_File_id_seq" RESTART WITH 1;');

		//Derecho a la libertad
		File::create(array('name' => 'Solicitudes de habeas corpus', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Personas detenidas e internadas provisionalmente', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Cantidad de Juicios en Materia Penal', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Personas detenidas y procesadas en masa', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Imputados por los delitos de fraude procesal y simulación de delitos', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Número de detenciones a nivel nacional', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Víctimas de desaparición', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Imputados por el delito de privación de libertad', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Víctimas de privación de libertad', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Imputados por Desaparición Forzada', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Ocupación en bartolinas policiales', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Derechos violentados a privados de libertad', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Enfermedades agudas más comunes en centros penales', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Enfermedades crónicas más comunes en centros penales', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));

		// Derecho a la integridad personal
		File::create(array('name' => 'Víctimas ingresadas por ...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Imputados de profesión PNC y ...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Agentes que continuaron...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según sexo - Casos...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según tipo de delito - Casos...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según rango de edad - Casos...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Víctimas ingresadas por...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según rango de edad - Víctimas...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según tipo de delito - Víctimas...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según depto. y mpio. - Víctimas...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según tipo, depto. y mpio. - Denuncias...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según sexo - Denuncias interpuestas...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según rango de edad - Denuncias...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según depto. y mpio. - Agresiones...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según sexo y rango de edad...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Según rango de horario y tipo...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Investigaciones de agentes PNC...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Denuncias reportadas en la PDDH...', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));

		// Derecho a la vida
		File::create(array('name' => 'Homicidios', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Policías investigados', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Agresiones ilegítimas con arma de fuego', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Policías fallecidos durantes agresiones ilegítimas', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));

		// Estimaciones de población
		File::create(array('name' => 'Población por edad', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
		File::create(array('name' => 'Población por territorio', 'type' => '', 'icon' => 'fa fa-folder-o', 'icon_html' => '<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>', 'organization_id' => 1));
	}

}
