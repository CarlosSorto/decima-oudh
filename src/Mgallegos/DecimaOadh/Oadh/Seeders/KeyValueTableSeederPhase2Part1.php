<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\KeyValueTablePhase2Part1Seeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Mgallegos\DecimaOadh\Oadh\KeyValue;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class KeyValueTablePhase2Part1Seeder extends Seeder
{

	public function run()
	{
		/**
		 * BLOQUE 1 Fase 2
		*/

		// Recomendaciones

		KeyValue::create(
			array(
				'key' => 'R001',
				'value' => 'Título recomendaciones',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'R002',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'N001',
				'value' => 'Título medios de prensa',
				'organization_id' => 1
			)
		);

		KeyValue::create(
			array(
				'key' => 'N002',
				'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei It\'s my button! usmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat dfdsffdfsfs.',
				'organization_id' => 1
			)
		);
	}
}
