<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\MenuTableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;

use Mgallegos\DecimaOadh\Oadh\HumanRight;

use Illuminate\Database\Seeder;

class HumanRightTableSeeder extends Seeder {

	public function run()
	{


		HumanRight::create(
			array(
				'name' => 'La efectividad de una vida segura y sin amenazas',
				'organization_id' => 1
			)
		);
    $eje_id = DB::table('OADH_Human_Right')->max('id');

		HumanRight::create(
			array(
				'name' => 'Derecho a la vida',
				'parent_id' => $eje_id,
				'organization_id' => 1
			)
		);
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Homicidios',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Feminicidios',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Crímenes de odio',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Ejecuciones sumarias',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Torturas',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Accidente de tránsito',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Tráfico de armas de fuego',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Muerte arbitraria o extralegal consumada',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Muerte arbitraria o extralegal fallida',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Amenazas de muerte',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Desapariciones forzadas o involuntarias',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Enfrentamientos armados',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );


    HumanRight::create(
      array(
        'name' => 'Derecho a la integridad física y moral',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Hacinamiento carcelario',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Amenazas',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Maltrato físico y psicológico ',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Programas de reinserción',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Tráfico de personas',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Uso desproporcionado de la fuerza',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Tortura',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Tratos o penas crueles, inhumanas o degradantes',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Malos tratos',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Uso desproporcionado de la fuerza',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Trato inhumano a los detenidos',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Violencia sexual',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Derecho a la seguridad física y jurídica',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Extorsión',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
         'name' => 'Tortura',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Exclusión',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Inequidad',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Acoso por defender derechos',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Actuaciones de las instituciones del Estado ante la vulnerabilidad de los defensores de derechos humanos',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Inclusión',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );


    HumanRight::create(
      array(
        'name' => 'Derecho a la libertad personal',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Detenciones ilegales y arbitrarias',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Detenciones masivas',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Garantías para un juicio justo',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Garantías de los derechos de los detenidos',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Detención arbitraria',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Detención por falta de policía',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Falta de garantías procesales',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Detención por hechos no tipificados en la ley',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Reclutamiento arbitrario',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
		HumanRight::create(
			array(
				'name' => 'Desapariciones forzadas o involuntarias',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'No aplica',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);



		HumanRight::create(
			array(
				'name' => 'El acceso a la justicia',
				'organization_id' => 1
			)
		);
    $eje_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Derecho a la justicia pronta y cumplida',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Actuaciones del ministerio público y de la PGR',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Funcionamiento del sistema de justicia',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Corrupción',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Mora judicial',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Privación de libertad',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Denegación de justicia ordinaria',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Denegación de justicia constitucional (hábeas corpus, amparo, recurso de inconstitucionalidad)',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Retardación de justicia',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Derecho a la integridad de la persona detenida',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Condicionesde infraestructura',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Enfermedades en el sistema penitenciario',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Programas de reinserción a los privados de libertad',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
			array(
				'name' => 'La existencia de un medio ambiente de calidad',
				'organization_id' => 1
			)
		);
    $eje_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Derecho al medio ambiente',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Conservación y protección del agua',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Acceso al agua potable',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Tala de árboles',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Desechos sólidos ',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Minería',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Erosión',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Aprobación de leyes',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Presentación de propuestas de ley',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Contaminación atmosférica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación sónica (ruidos y vibraciones)',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación por gases, humos, partículas, suspendidas y olores',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación electromagnética',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación por agroquímicos y plaguicidas',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación de aguas',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación aguas superficiales',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación ríos y arroyos',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación quebradas',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación lagos y lagunas',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación del suelo',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación del suelo por agroquímicos y pesticidas',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Contaminación del suelo por desechos peligrosos',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Vertidos de desechos sólidos',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Uso no sostenible de recursos naturales',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Modificación del territorio',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Carencia negligente de instrucción, planes y programas encaminadas a la mitigación y reducción de los efectos negativos de desastres',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Transporte, depósitos, almacenamiento, procesamiento y uso de sustancias peligrosas sin tomar las medidas precautorias requeridas',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
			array(
				'name' => 'El acceso a la educación en los niveles inicial, parvularia, básica y media',
				'organization_id' => 1
			)
		);
    $eje_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Derecho a la educación',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Sistema educativo',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Programas de alimentación',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Alfabetización ',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Deserción escolar',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Deserción escolar',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Discriminación educacional',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Denegación del derecho a la educación parvularia, básica y especial gratuita',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Establecimiento de colegiaturas y pagos obligatorios ilegales',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Cierre ilegal o arbitrario de centros de enseñanza e instrucción públicos',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Retiro y reducción ilícita de suministros a centros educativos estatales y falta de condiciones mínimas de infraestructura en sus instalaciones',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Obstaculización ilegal del proceso de enseñanza- aprendizaje en los centros educativos',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'El acceso al trabajo',
        'organization_id' => 1
      )
    );
    $eje_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Derecho al trabajo',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Condiciones laborales',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Empleo',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Subempleo',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Tolerancia estatal ante despidos ilegales en centros de trabajo públicos y privado',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Denegación arbitraria de los derechos o prestaciones laborales reconocidas por la ley',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Discriminación laboral',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Actos ilegales o arbitrarios atentatorios contra la estabilidad laboral de trabajadores públicos o privados',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Inobservancia de las medidas de seguridad e higiene obligatorias en los centros de trabajo',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Incumplimiento de los contratos colectivos de trabajo',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Derecho al salario y las prestaciones sociales',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Salario mínimo',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Escalafón salarial',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Trabajo en el hogar',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Derecho a la sindicalización',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Irrespeto a fuero sindical',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Amenazas',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Maltrato',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Acoso',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Denegación o cancelación arbitraria de la persona jurídica, a sindicatos o asociaciones de trabajadores',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Obstrucción ilegal de la actividad sindical o de asociación laboral',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Actos ilícitos que afectan los derechos de dirigentes laborales',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Perturbaciones del derecho de organización y filiación sindical o gremial',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'La experiencia de redes de protección social',
        'organization_id' => 1
      )
    );
    $eje_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Derecho a la seguridad social',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Trabajadoras domésticas',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Desprotección ilegal o arbitraria en materia de seguridad social',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Denegación ilícita de los beneficios o prestaciones de seguridad social por parte de instituciones estatales encargadas de la materia',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
		HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );


    HumanRight::create(
      array(
        'name' => 'Derecho a la jubilación y pensión',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Sistema de pensiones',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Derecho a la vivienda del propietario y no propietario',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Desalojos',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'Discriminación en la participación activa en el diseño de las políticas nacionales',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Exclusión o restricción para la obtención de créditos bancarios por motivos de estado familiar',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Discriminación en la tenencia y propiedad de la tierra y en los procesos de reparto de tierras',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Discriminación en la prestación de servicios básicos de transporte',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

    HumanRight::create(
      array(
        'name' => 'La cultura del diálogo, equidad, tolerancia e inclusión',
        'organization_id' => 1
      )
    );
    $eje_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Derecho a la reparación de las víctimas, especialmente de los grupos más vulnerables',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );
    $derecho_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Casos de violaciones de derechos humanos durante el conflicto armado',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Memoria histórica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'T',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Emisión de leyes que imposibilitan la investigación y sanción a los responsables de graves violaciones a derechos humanos durante el conflicto armado',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'Omisión, negligencia o retardación en la investigación de graves violaciones a derechos humanos (ejecuciones extralegales, desapariciones forzadas, torturas u otras)',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );
    HumanRight::create(
      array(
        'name' => 'No aplica',
        'type' => 'H',
        'parent_id' => $derecho_id,
        'organization_id' => 1
      )
    );

		HumanRight::create(
      array(
        'name' => 'El acceso a la información',
        'organization_id' => 1
      )
    );
    $eje_id = DB::table('OADH_Human_Right')->max('id');

    HumanRight::create(
      array(
        'name' => 'Derecho de acceso a la información pública',
        'parent_id' => $eje_id,
        'organization_id' => 1
      )
    );

    $derecho_id = DB::table('OADH_Human_Right')->max('id');

		HumanRight::create(
			array(
				'name' => 'Transparencia de las instituciones',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Rendición de cuentas',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Probidad',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'No aplica',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Denegación de información',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Irrespeto de plazos de respuesta de las instituciones',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'No aplica',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);

		HumanRight::create(
			array(
				'name' => 'El acceso a la salud',
				'organization_id' => 1
			)
		);
		$eje_id = DB::table('OADH_Human_Right')->max('id');

		HumanRight::create(
			array(
				'name' => 'Derecho a la salud',
				'parent_id' => $eje_id,
				'organization_id' => 1
			)
		);

		$derecho_id = DB::table('OADH_Human_Right')->max('id');

		HumanRight::create(
			array(
				'name' => 'Infraestructura hospitalaria',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Funcionamiento del sistema nacional de salud',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Acceso a medicamentos',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Trato al paciente',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Embarazo en adolescentes',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'No aplica',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Negligencia hospitalaria',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Negación de atencion hospitalaria',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Mala praxis',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'No aplica',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);

		HumanRight::create(
			array(
				'name' => 'Libertad de circulacion',
				'organization_id' => 1
			)
		);
		$eje_id = DB::table('OADH_Human_Right')->max('id');

		HumanRight::create(
			array(
				'name' => 'Derecho a la libertad de circulación dentro del territorio',
				'parent_id' => $eje_id,
				'organization_id' => 1
			)
		);

		$derecho_id = DB::table('OADH_Human_Right')->max('id');

		HumanRight::create(
			array(
				'name' => 'Amenaza',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Extorción',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Disparos de arma de fuego',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'No aplica',
				'type' => 'T',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Restricciones ilegales o arbitrarias a la libre circulación.',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Cambio arbitrario del domicilio o residencia',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Restricciones ilegales o arbitrarias para entrar, permanecer o salir del territorio',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'Restricciones ilegales o arbitrarias a la libre circulación de manifestaciones sociales',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
		HumanRight::create(
			array(
				'name' => 'No aplica',
				'type' => 'H',
				'parent_id' => $derecho_id,
				'organization_id' => 1
			)
		);
	}
}
