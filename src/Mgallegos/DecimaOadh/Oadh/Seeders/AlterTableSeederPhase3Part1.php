<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\AlterTableSeederPhase3Part1
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Mgallegos\DecimaOadh\Oadh\KeyValue;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class AlterTableSeederPhase3Part1 extends Seeder
{
	public function run()
	{	

		DB::statement('
			ALTER TABLE public."OADH_Human_Right" 
			ADD COLUMN position INTEGER NULL DEFAULT 0;
		');

		// DB::statement('
		// 	UPDATE public."OADH_CMS_Key_Value" SET es_value = \'temp\', en_value = \'temp\' WHERE key = \'N010\';
		// ');

		// DB::statement('
		// 	UPDATE public."OADH_CMS_Key_Value" SET en_value = \'temp\' WHERE key = \'A002\';
		// ');

		// DB::statement('
		// 	ALTER TABLE public."OADH_Human_Right" 
		// 	ADD COLUMN es_description TEXT NULL DEFAULT NULL,
		// 	ADD COLUMN en_description TEXT NULL DEFAULT NULL;
		// ');

		// DB::statement('
		// 	UPDATE public."OADH_Human_Right"  SET 
		// 	es_description = \'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei Its my button! usmod tempor incididunt ut labore et dolore magna aliqua.\', 
		// 	en_description = \'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei Its my button! usmod tempor incididunt ut labore et dolore magna aliqua.\' 
		// 	WHERE id >= \'1\';
		// ');

		// DB::statement('
		// 	ALTER TABLE public."OADH_Human_Right" 
		// 	ADD COLUMN es_name TEXT NULL DEFAULT NULL,
		// 	ADD COLUMN en_name TEXT NULL DEFAULT NULL,
		// 	ADD COLUMN is_web_visible INTEGER NULL DEFAULT 0;
		// ');

		// DB::statement('
		// 	UPDATE public."OADH_Human_Right" SET en_name = name, es_name = name WHERE id >= \'1\';
		// ');

		// DB::statement('
		// 	ALTER TABLE public."OADH_CMS_Multimedia" 
		// 	ADD COLUMN lang CHAR(2) NULL DEFAULT \'es\',
		// 	ADD COLUMN date DATE NULL DEFAULT NULL;
		// ');

		// DB::statement('
		// 	ALTER TABLE public."OADH_CMS_Activity" 
		// 	ADD COLUMN lang CHAR(2) NULL DEFAULT \'es\';
		// ');

		// DB::statement('
		// 	ALTER TABLE public."OADH_CMS_Publication" 
		// 	ADD COLUMN lang CHAR(2) NULL DEFAULT \'es\',
		// 	ADD COLUMN summary_url TEXT NULL DEFAULT NULL,
		// 	ADD COLUMN infographic_url TEXT NULL DEFAULT NULL;
		// ');
		
		// DB::statement('
		// 	ALTER TABLE public."OADH_CMS_Key_Value" 
		// 	ADD COLUMN es_value TEXT NULL DEFAULT NULL,
		// 	ADD COLUMN en_value TEXT NULL DEFAULT NULL;
		// ');
		
		// DB::statement('
		// 	UPDATE public."OADH_CMS_Key_Value" SET es_value = value, en_value = value WHERE id >= \'1\';
		// ');

		// DB::statement('
		// 	ALTER TABLE public."SEC_Menu" 
		// 	ADD COLUMN is_app INTEGER NULL DEFAULT 0;
		// ');
	}
}
