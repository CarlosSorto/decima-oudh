<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\RightSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Schema;
use Mgallegos\DecimaOadh\Oadh\Right;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class RightSeeder extends Seeder
{

	public function run()
	{
		/**
		 * BLOQUE 1 Fase 2
		*/
		Schema::disableForeignKeyConstraints();
		DB::statement('TRUNCATE TABLE public."OADH_Right" CASCADE;');
		DB::statement('ALTER SEQUENCE "OADH_Right_id_seq" RESTART WITH 1;');

		// Derechos
		Right::create(array('organization_id' => 1, 'name' => 'Derecho de acceso a la justicia'));
		Right::create(array('organization_id' => 1, 'name' => 'Adopción de convenciones y protocolos facultativos'));
		Right::create(array('organization_id' => 1, 'name' => 'Marco jurídico y reforma legal'));
		Right::create(array('organization_id' => 1, 'name' => 'Fortalecimiento institucional'));
		Right::create(array('organization_id' => 1, 'name' => 'Alcance de restricciones'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho de las víctimas de violaciones graves de derechos humanos y del derecho internacional humanitario'));
		Right::create(array('organization_id' => 1, 'name' => 'Cláusula federal'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a defender derechos humanos'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la integridad física y moral'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la educación'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la protección de los intereses morales y materiales'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la identidad de género'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho de igualdad ante la ley'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la protección contra la discriminación'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a libertad de pensamiento y de expresión'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la libertad personal'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la seguridad personal'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la nacionalidad'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la libre circulación y a elegir libremente su residencia'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos políticos'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la vida cultural, artística y científica'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de los grupos étnicos (pueblos indígenas o población afrodescendiente)'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la propiedad privada'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la salud'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos a la salud sexual y reproductiva'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la seguridad social'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la vida'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a un nivel de vida adecuado'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a cuidados y asistencias especiales'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a libertad de conciencia y de religión'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a no ser sometido a torturas ni a otros tratos o penas crueles, inhumanos o degradantes'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a protección de la honra y la dignididad'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a rectificación o respuesta'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la reintegración social'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho al medio ambiente'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho al nombre'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho al principio de legalidad y de retroactividad'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho al reconocimiento de la personalidad jurídica'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho al secreto profesional'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho al trabajo'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho a la libertad de reunión y de asociación pacíficas'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de la niñez y adolescencia'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho al matrimonio libre y protección de la familia'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho de protección judicial'));
		Right::create(array('organization_id' => 1, 'name' => 'Derecho humano al agua y al saneamiento'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de la población LGBTI'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de las mujeres'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de las personas migrantes, desplazadas y deportadas'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de las personas con discapacidad'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de las personas privadas de libertad'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de las personas víctimas de la trata'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos económicos, sociales y culturales'));
		Right::create(array('organization_id' => 1, 'name' => 'Desarrollo progresivo'));
		Right::create(array('organization_id' => 1, 'name' => 'Normas de interpretación'));
		Right::create(array('organization_id' => 1, 'name' => 'Reconocimiento de otros derechos'));
		Right::create(array('organization_id' => 1, 'name' => 'Derechos de las personas en situaciones de riesgo y emergencias humanitarias'));
		Right::create(array('organization_id' => 1, 'name' => 'Suspensión de garantías'));
		Right::create(array('organization_id' => 1, 'name' => 'Difusión y promoción de derechos humanos'));
	}
}
