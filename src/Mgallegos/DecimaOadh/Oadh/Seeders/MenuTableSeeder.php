<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\MenuTableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;

use App\Kwaai\Security\Module;

use App\Kwaai\Security\Menu;

use App\Kwaai\Security\Permission;

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder {

	public function run()
	{
		Module::create(array('name' => 'OUDH', 'lang_key' => 'decima-oadh::menu.module', 'icon' => 'fa fa-search', 'created_by' => 1));
		$moduleId = DB::table('SEC_Module')->max('id');

		Menu::create(array('name' => 'Medios de comunicación', 'lang_key' => 'decima-oadh::menu.setup', 'url' => null, 'icon' => 'fa fa-gear', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		$parentMenuId = DB::table('SEC_Menu')->max('id');

		Menu::create(array('name' => 'Configuración inicial', 'lang_key' => 'decima-oadh::menu.initialSetup', 'url' => '/ucaoadh/setup/initial-setup', 'action_button_id' => 'oadh-is-btn-close', 'action_lang_key' => 'decima-oadh::menu.initialSetupAction', 'icon' => 'fa fa-gear', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Gestión de derechos humanos', 'lang_key' => 'decima-oadh::menu.humanRightManagement', 'url' => '/ucaoadh/setup/human-right-management', 'action_button_id' => 'oadh-hrm-btn-close', 'action_lang_key' => 'decima-oadh::menu.humanRightManagementAction', 'icon' => 'fa fa-gear', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Gestión de medios', 'lang_key' => 'decima-oadh::menu.mediaManagement', 'url' => '/ucaoadh/setup/digital-media-management', 'action_button_id' => 'rh-dmm-btn-close', 'action_lang_key' => 'decima-oadh::menu.mediaManagementAction', 'icon' => 'fa fa-gear', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Monitoreo de medios', 'lang_key' => 'decima-oadh::menu.mediaMonitoring', 'url' => '/ucaoadh/transactions/media-monitoring', 'action_button_id' => 'oadh-mmo-btn-close', 'action_lang_key' => 'decima-oadh::menu.mediaMonitoringAction', 'icon' => 'fa fa-eye', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
	}

}
