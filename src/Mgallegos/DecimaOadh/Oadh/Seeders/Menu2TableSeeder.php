<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\MenuTableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;

use App\Kwaai\Security\Module;

use App\Kwaai\Security\Menu;

use App\Kwaai\Security\Permission;

use Illuminate\Database\Seeder;

class Menu2TableSeeder extends Seeder {

	public function run()
	{
		$moduleId = DB::table('SEC_Module')->max('id');

		Menu::create(array('name' => 'Derecho a la libertad', 'lang_key' => 'decima-oadh::menu.rightToFreedom', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		$parentMenuId = DB::table('SEC_Menu')->max('id');

		Menu::create(array('name' => 'Solicitudes de habeas corpus', 'lang_key' => 'decima-oadh::menu.habeasCorpusRequest', 'url' => '/ucaoadh/freedom/habeas-corpus-request', 'action_button_id' => 'oadh-free-hcr-btn-close', 'action_lang_key' => 'decima-oadh::menu.habeasCorpusRequestAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Personas detenidas e internadas provisionalmente', 'lang_key' => 'decima-oadh::menu.peopleDetained', 'url' => '/ucaoadh/freedom/people-detained', 'action_button_id' => 'oadh-free-pd-btn-close', 'action_lang_key' => 'decima-oadh::menu.peopleDetainedAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Cantidad de Juicios en Materia Penal', 'lang_key' => 'decima-oadh::menu.criminalCases', 'url' => '/ucaoadh/freedom/criminal-cases-trials', 'action_button_id' => 'oadh-free-cct-btn-close', 'action_lang_key' => 'decima-oadh::menu.criminalCasesAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Personas detenidas y procesadas en masa', 'lang_key' => 'decima-oadh::menu.massTrials', 'url' => '/ucaoadh/freedom/mass-trials', 'action_button_id' => 'oadh-free-mt-btn-close', 'action_lang_key' => 'decima-oadh::menu.massTrialsAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Imputados por los delitos de fraude procesal y simulación de delitos', 'lang_key' => 'decima-oadh::menu.proceduralFraud', 'url' => '/ucaoadh/freedom/procedural-fraud', 'action_button_id' => 'oadh-free-pf-btn-close', 'action_lang_key' => 'decima-oadh::menu.proceduralFraudAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Número de detenciones a nivel nacional', 'lang_key' => 'decima-oadh::menu.captureOrders', 'url' => '/ucaoadh/freedom/capture-orders', 'action_button_id' => 'oadh-free-co-btn-close', 'action_lang_key' => 'decima-oadh::menu.proceduralFraudAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Víctimas de desaparición', 'lang_key' => 'decima-oadh::menu.disapparencesVictims', 'url' => '/ucaoadh/freedom/disapparences-victims', 'action_button_id' => 'oadh-free-dv-btn-close', 'action_lang_key' => 'decima-oadh::menu.disapparencesVictimsAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Imputados por el delito de privación de libertad', 'lang_key' => 'decima-oadh::menu.accusedlibertyDeprivation', 'url' => '/ucaoadh/freedom/accused-liberty-deprivation', 'action_button_id' => 'oadh-free-llp-btn-close', 'action_lang_key' => 'decima-oadh::menu.accusedLibertyDeprivationAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Víctimas de privación de libertad', 'lang_key' => 'decima-oadh::menu.victimLibertyDeprivation', 'url' => '/ucaoadh/freedom/victim-liberty-deprivation', 'action_button_id' => 'oadh-free-vlp-btn-close', 'action_lang_key' => 'decima-oadh::menu.victimLibertyDeprivationAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Imputados por Desaparición Forzada', 'lang_key' => 'decima-oadh::menu.forcedDisappearances', 'url' => '/ucaoadh/freedom/forced-disappearances', 'action_button_id' => 'oadh-free-fd-btn-close', 'action_lang_key' => 'decima-oadh::menu.libertyDeprivationAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Ocupación en bartolinas policiales', 'lang_key' => 'decima-oadh::menu.jailsOccupation', 'url' => '/ucaoadh/freedom/jails-occupation', 'action_button_id' => 'oadh-free-fd-btn-close', 'action_lang_key' => 'decima-oadh::menu.jailsOccupationAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Derechos violentados a privados de libertad', 'lang_key' => 'decima-oadh::menu.deprivedPersons', 'url' => '/ucaoadh/freedom/deprived-persons', 'action_button_id' => 'oadh-free-dp-btn-close', 'action_lang_key' => 'decima-oadh::menu.deprivedPersonsAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Enfermedades agudas más comunes en centros penales', 'lang_key' => 'decima-oadh::menu.acuteDiseases', 'url' => '/ucaoadh/freedom/acute-disease', 'action_button_id' => 'oadh-free-ad-btn-close', 'action_lang_key' => 'decima-oadh::menu.acuteIllnessAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Enfermedades crónicas más comunes en centros penales', 'lang_key' => 'decima-oadh::menu.chronicDiseases', 'url' => '/ucaoadh/freedom/chronic-diseases', 'action_button_id' => 'oadh-free-cd-btn-close', 'action_lang_key' => 'decima-oadh::menu.chronicDiseasesAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));

		Menu::create(array('name' => 'Derecho a la integridad personal', 'lang_key' => 'decima-oadh::menu.rightToPersonalIntegrity', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		$parentMenuId = DB::table('SEC_Menu')->max('id');

		Menu::create(array('name' => 'Víctimas ingresadas por medio de denuncias ante los delitos de de lesiones, amenazas, disparo de arma de fuego, extorsión y tortura', 'lang_key' => 'decima-oadh::menu.crimesVictims', 'url' => '/ucaoadh/integrity/crimes-victims', 'action_button_id' => 'oadh-inte-cv-btn-close', 'action_lang_key' => 'decima-oadh::menu.crimesVictimsAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Imputados de profesión PNC y miembros de FAES ante el delito de lesiones, disparo de arma de fuego, extorsión y tortura', 'lang_key' => 'decima-oadh::menu.crimesAccused', 'url' => '/ucaoadh/integrity/crimes-accused', 'action_button_id' => 'oadh-inte-ca-btn-close', 'action_lang_key' => 'decima-oadh::menu.crimesAccusedAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Agentes que continuaron laborando durante fases de investigaciones abiertas', 'lang_key' => 'decima-oadh::menu.workingOfficials', 'url' => '/ucaoadh/integrity/working-officials', 'action_button_id' => 'oadh-inte-wo-btn-close', 'action_lang_key' => 'decima-oadh::menu.workingOfficialsAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según sexo - Casos reportados por el delito de trata y tráfico de personas', 'lang_key' => 'decima-oadh::menu.genderTrafficking', 'url' => '/ucaoadh/integrity/gender-trafficking', 'action_button_id' => 'oadh-inte-gt-btn-close', 'action_lang_key' => 'decima-oadh::menu.genderTraffickingAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según tipo de delito - Casos reportados por el delito de trata y tráfico de personas', 'lang_key' => 'decima-oadh::menu.typeTrafficking', 'url' => '/ucaoadh/integrity/type-trafficking', 'action_button_id' => 'oadh-inte-tt-btn-close', 'action_lang_key' => 'decima-oadh::menu.typeTraffickingAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según rango de edad - Casos reportados por el delito de trata y tráfico de personas', 'lang_key' => 'decima-oadh::menu.ageTrafficking', 'url' => '/ucaoadh/integrity/age-trafficking', 'action_button_id' => 'oadh-inte-at-btn-close', 'action_lang_key' => 'decima-oadh::menu.ageTraffickingAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Víctimas ingresadas por el delito de limitación ilegal a la libertad de circulación', 'lang_key' => 'decima-oadh::menu.movementFreedom', 'url' => '/ucaoadh/integrity/movement-freedom', 'action_button_id' => 'oadh-inte-mf-btn-close', 'action_lang_key' => 'decima-oadh::menu.movementFreedomAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según rango de edad - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres', 'lang_key' => 'decima-oadh::menu.ageWomen', 'url' => '/ucaoadh/integrity/age-women', 'action_button_id' => 'oadh-inte-aw-btn-close', 'action_lang_key' => 'decima-oadh::menu.ageWomenAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según tipo de delito - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres', 'lang_key' => 'decima-oadh::menu.typeWomen', 'url' => '/ucaoadh/integrity/type-women', 'action_button_id' => 'oadh-inte-tw-btn-close', 'action_lang_key' => 'decima-oadh::menu.typeWomenAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según depto. y mpio. - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres', 'lang_key' => 'decima-oadh::menu.StateWomen', 'url' => '/ucaoadh/integrity/state-women', 'action_button_id' => 'oadh-inte-sw-btn-close', 'action_lang_key' => 'decima-oadh::menu.stateWomenAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según tipo, depto. y mpio. - Denuncias interpuestas por delitos atentatorios contra la libertad sexual', 'lang_key' => 'decima-oadh::menu.typeSexual', 'url' => '/ucaoadh/integrity/type-sexual', 'action_button_id' => 'oadh-inte-ts-btn-close', 'action_lang_key' => 'decima-oadh::menu.typeSexualAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según sexo - Denuncias interpuestas por delitos atentatorios contra la libertad sexual', 'lang_key' => 'decima-oadh::menu.genderSexual', 'url' => '/ucaoadh/integrity/gender-sexual', 'action_button_id' => 'oadh-inte-gs-btn-close', 'action_lang_key' => 'decima-oadh::menu.genderSexualAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según rango de edad - Denuncias interpuestas por delitos atentatorios contra la libertad sexual', 'lang_key' => 'decima-oadh::menu.ageSexual', 'url' => '/ucaoadh/integrity/age-sexual', 'action_button_id' => 'oadh-inte-as-btn-close', 'action_lang_key' => 'decima-oadh::menu.ageSexualAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según depto. y mpio. - Agresiones registradas por el delito de lesiones', 'lang_key' => 'decima-oadh::menu.stateInjuries', 'url' => '/ucaoadh/integrity/state-injuries', 'action_button_id' => 'oadh-inte-si-btn-close', 'action_lang_key' => 'decima-oadh::menu.stateInjuriesAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según sexo y rango de edad - Agresiones registradas por el delito de lesiones', 'lang_key' => 'decima-oadh::menu.ageInjuries', 'url' => '/ucaoadh/integrity/age-injuries', 'action_button_id' => 'oadh-inte-ai-btn-close', 'action_lang_key' => 'decima-oadh::menu.ageInjuriesAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		// Menu::create(array('name' => 'Según rango de horario y tipo de víct. - Agresiones registradas por el delito de lesiones', 'lang_key' => 'decima-oadh::menu.scheduleInjuries', 'url' => '/ucaoadh/integrity/schedule-injuries', 'action_button_id' => 'oadh-inte-ci-btn-close', 'action_lang_key' => 'decima-oadh::menu.scheduleInjuriesAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Según tipo de arma - Agresiones registradas por el delito de lesiones', 'lang_key' => 'decima-oadh::menu.scheduleInjuries', 'url' => '/ucaoadh/integrity/schedule-injuries', 'action_button_id' => 'oadh-inte-ci-btn-close', 'action_lang_key' => 'decima-oadh::menu.scheduleInjuriesAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Investigaciones de agentes PNC por delitos relativo al abuso de la fuerza y uso ilegal de armas de fuego', 'lang_key' => 'decima-oadh::menu.agentInvestigation', 'url' => '/ucaoadh/integrity/agent-investigation', 'action_button_id' => 'oadh-inte-gi-btn-close', 'action_lang_key' => 'decima-oadh::menu.agentInvestigationAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Denuncias reportadas en la PDDH por la vulneración del derecho a la integridad personal', 'lang_key' => 'decima-oadh::menu.integrityViolation', 'url' => '/ucaoadh/integrity/integrity-violation', 'action_button_id' => 'oadh-inte-iv-btn-close', 'action_lang_key' => 'decima-oadh::menu.integrityViolationAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));

		Menu::create(array('name' => 'Derecho a la vida', 'lang_key' => 'decima-oadh::menu.rightToLife', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		//Menús de derecho a la vida
		$parentMenuId = DB::table('SEC_Menu')->max('id');

		Menu::create(array('name' => 'Homicidios', 'lang_key' => 'decima-oadh::menu.homicidesRates', 'url' => '/ucaoadh/life/homicides-rates', 'action_button_id' => 'oadh-life-hr-btn-close', 'action_lang_key' => 'decima-oadh::menu.homicidesRatesAction', 'icon' => 'fa fa-database', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Policías investigados', 'lang_key' => 'decima-oadh::menu.officersInvestigated', 'url' => '/ucaoadh/life/officers-investigated', 'action_button_id' => 'oadh-life-oi-btn-close', 'action_lang_key' => 'decima-oadh::menu.officersInvestigatedAction', 'icon' => 'fa fa-database', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Agresiones ilegítimas con arma de fuego', 'lang_key' => 'decima-oadh::menu.aggressionsWeapon', 'url' => '/ucaoadh/life/aggression-weapon', 'action_button_id' => 'oadh-life-aw-btn-close', 'action_lang_key' => 'decima-oadh::menu.aggressionsWeaponAction', 'icon' => 'fa fa-database', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Policías fallecidos durantes agresiones ilegítimas', 'lang_key' => 'decima-oadh::menu.aggressionsDeath', 'url' => '/ucaoadh/life/aggressions-death', 'action_button_id' => 'oadh-life-ad-btn-close', 'action_lang_key' => 'decima-oadh::menu.aggressionsDeathAction', 'icon' => 'fa fa-database', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));

		//Menús de estimaciones de Población
		Menu::create(array('name' => 'Estimaciones de Población', 'lang_key' => 'decima-oadh::menu.populationEstimates', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		$parentMenuId = DB::table('SEC_Menu')->max('id');

		Menu::create(array('name' => 'Población por edad', 'lang_key' => 'decima-oadh::menu.agePopulation', 'url' => '/ucaoadh/population/age-population', 'action_button_id' => 'btn-close', 'action_lang_key' => 'decima-oadh::menu.agePopulationAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Población por territorio', 'lang_key' => 'decima-oadh::menu.territoryPopulation', 'url' => '/ucaoadh/population/territory-population', 'action_button_id' => 'btn-close', 'action_lang_key' => 'decima-oadh::menu.territoryPopulation', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));

		//Menús de derecho a la libertad
		// $parentMenuId = 11;
		//Menús de derecho a la integridad
		// $parentMenuId = 13;
		//Menús de derecho a la vida
		// $parentMenuId = 14;
		//Menús de estimaciones de Población
		// $parentMenuId = 55;
	}

}
