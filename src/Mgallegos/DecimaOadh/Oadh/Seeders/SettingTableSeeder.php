<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\MenuTableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;

use Mgallegos\DecimaOadh\Oadh\Setting;

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder {

	public function run()
	{
		Setting::create(
			array(
				'population_affected' => 'Las niñas,Los niños,Los adolescentes,Los jóvenes,Las mujeres,Los hombres,Los adultos,Los adultos mayores,Colectivos LGTBI,Grupos étnicos (indígenas- población afrodescendiente- etc.),Personas con discapacidad,Víctimas del conflicto armado',
				'note_locations' => 'Primera plana,Página par,Página inpar,Página posterior,Titulares,Desarrollo del programa',
				'journalistic_genre' => 'Noticia,Reportaje,Crónica,Entrevista,Editorial,Artículo de opinión,No aplica',
				'guns_types' => 'Arma de fuego,Arma blanca,Objeto cortocontundente,Asfixia,Otros',
				'sexual_diversities' => 'Lesbiana,Gay,Transexual,Bisexual',
				'sources' => 'PDDH,FGR,PNC,IML,MINSAL,ISSS,MJSP,IGSP,IAIP,MARN,MTPS,MINED,DGME,CSJ,Asamblea Legislativa',
				'organization_id' => 1
			)
		);
	}

}
