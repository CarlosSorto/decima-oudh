<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\Menu4TableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use App\Kwaai\Security\Module;
use App\Kwaai\Security\Menu;
use App\Kwaai\Security\Permission;
use Illuminate\Database\Seeder;

class Menu4TableSeeder extends Seeder {

	public function run()
	{
		//Catálogos
		$moduleId = DB::table('SEC_Module')->max('id');		

		//Recomendaciones
		Menu::create(array('name' => 'Recomendaciones', 'lang_key' => 'decima-oadh::menu.recommendations', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		$parentMenuId = DB::table('SEC_Menu')->max('id');

		Menu::create(array('name' => 'Gestión de recomendaciones', 'lang_key' => 'decima-oadh::menu.recommendationsManagement', 'url' => '/ucaoadh/recommendations/recommendations-management', 'action_button_id' => 'oadh-rrm-btn-close', 'action_lang_key' => 'decima-oadh::menu.recommendationsManagementAction', 'icon' => 'fa fa-pencil-square-o', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Gestión de instituciones', 'lang_key' => 'decima-oadh::menu.institutionsManagement', 'url' => '/ucaoadh/recommendations/institutions-management', 'action_button_id' => 'oadh-rim-btn-close', 'action_lang_key' => 'decima-oadh::menu.institutionsManagementAction', 'icon' => 'fa fa-gear', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Gestión de derechos', 'lang_key' => 'decima-oadh::menu.rightManagement', 'url' => '/ucaoadh/recommendations/right-management', 'action_button_id' => 'oadh-rrm-btn-close', 'action_lang_key' => 'decima-oadh::menu.rightManagementAction', 'icon' => 'fa fa-gear', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		Menu::create(array('name' => 'Importación de recomendaciones', 'lang_key' => 'decima-oadh::menu.recommendationsImport', 'url' => '/ucaoadh/recommendations/recommendations-import', 'action_button_id' => 'oadh-rrt-btn-close', 'action_lang_key' => 'decima-oadh::menu.recommendationsImportAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
	}

}
