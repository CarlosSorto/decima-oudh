<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\Menu6TableSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;

use App\Kwaai\Security\Module;

use App\Kwaai\Security\Menu;

use App\Kwaai\Security\Permission;

use Illuminate\Database\Seeder;

class Menu7TableSeeder extends Seeder {

	public function run()
	{
		$moduleId = DB::table('SEC_Module')->max('id');
		// Menu::create(array('name' => 'Derecho a la reparación integral de víctimas', 'lang_key' => 'decima-oadh::menu.rightToReparation', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));
		// Menu::create(array('name' => 'Derecho a la reparación integral', 'lang_key' => 'decima-oadh::menu.rightToReparation', 'url' => null, 'icon' => 'fa fa-database', 'parent_id' => null, 'module_id' => $moduleId, 'created_by' => 1));

		$parentMenuId = 67;

		Menu::create(array('name' => 'Noticias Ley de Amnistía', 'lang_key' => 'decima-oadh::menu.amnestyLawNews', 'url' => '/ucaoadh/reparation/amnesty-law-news', 'action_button_id' => 'oadh-reparation-aln-btn-close', 'action_lang_key' => 'decima-oadh::menu.amnestyLawNewsAction', 'icon' => 'fa fa-upload', 'parent_id' => $parentMenuId, 'module_id' => $moduleId, 'created_by' => 1));
		
		//Menús de derecho a la libertad
		// $parentMenuId = 11;
		//Menús de derecho a la integridad
		// $parentMenuId = 13;
		//Menús de derecho a la vida
		// $parentMenuId = 14;
		//Menús de estimaciones de Población
		// $parentMenuId = 55;
	}

}
