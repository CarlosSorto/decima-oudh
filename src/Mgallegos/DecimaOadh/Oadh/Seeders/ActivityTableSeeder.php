<?php

namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class ActivityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();

      foreach (range(1, 7) as $index)
      {
        $randomDate = Carbon::now()->subWeek(rand(10, 55));

        DB::table('OADH_CMS_Activity')->insert(
          [
            'date' => $randomDate,
            'title' => $faker->sentence(4),
            'place' => $faker->sentence(2),
            'description' => $faker->sentence(25),
            'tags' => $faker->word . ', ' . $faker->word . ', ' . $faker->word,
            'image_url' => 'http://lorempixel.com/1100/440/people/' . mt_rand(2, 10) . '/',
            'is_highlighted' => 0,
            'organization_id' => 1,
          ]);
      }

      //5

      $activityId = DB::table('OADH_CMS_Activity')->pluck('id');

      foreach (range(1, 90) as $index)
      {

        DB::table('OADH_CMS_Activity_Image')->insert(
          [
            'image_url' => 'http://lorempixel.com/275/250/people/' . mt_rand(1, 9) . '/',
            'description' => $faker->sentence(15),
            'activity_id' => $faker->randomElement($activityId),
            'organization_id' => 1,
          ]);
      }

      //4
    }
}
