<?php
/**
 * @file
 * SEC_User Table Seeder
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 * Run: php artisan db:seed --class=Mgallegos\\DecimaOadh\\Oadh\\Seeders\\InstitutionSeeder
 */
namespace Mgallegos\DecimaOadh\Oadh\Seeders;

use DB;
use Schema;
use Mgallegos\DecimaOadh\Oadh\Institution;
use App\Kwaai\Security\Module;
use Illuminate\Database\Seeder;

class InstitutionSeeder extends Seeder
{

	public function run()
	{
		/**
		 * BLOQUE 1 Fase 2
		*/

		Schema::disableForeignKeyConstraints();
		DB::statement('TRUNCATE TABLE public."OADH_Institution" CASCADE;');
		DB::statement('ALTER SEQUENCE "OADH_Institution_id_seq" RESTART WITH 1;');

		// Recomendaciones
		Institution::create(array('organization_id' => 1, 'name' =>'Academia Nacional de Seguridad Pública', 'abbreviation' => 'ANSP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Administración Nacional de Acueductos y Alcantarillados', 'abbreviation' => 'ANDA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Alcaldías Municipales', 'abbreviation' => 'Alcaldías'));
		Institution::create(array('organization_id' => 1, 'name' =>'Asamblea Legislativa', 'abbreviation' => 'AL'));
		Institution::create(array('organization_id' => 1, 'name' =>'Asociaciones de Promoción y Asistencia a los Derechos de la Niñez y Adolescencia', 'abbreviation' => 'APA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Banco Central de Reserva de El Salvador', 'abbreviation' => 'BCR'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comisión Coordinadora del Sector Justicia', 'abbreviation' => 'CCSJ'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comisión Ejecutiva Hidroeléctrica del Río Lempa', 'abbreviation' => 'CEL'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comisión Nacional de Búsqueda de Niñas y Niños Desaparecidos durante el Conflicto Armado Interno de El Salvador', 'abbreviation' => 'CNB'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comisión Nacional de Búsqueda de Personas Adultas Desaparecidas en el contexto del conflicto armado de El Salvador', 'abbreviation' => 'CONABÚSQUEDA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comisión Nacional de Protección Civil, Prevención y Mitigación de Desastres', 'abbreviation' => 'CNPCPMD'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comisión para la Determinación de la condición de Personas Refugiadas', 'abbreviation' => 'CODER'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comité Nacional de Prevención del Embarazo en Niñas y en Adolescentes', 'abbreviation' => 'CNPENA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comité Técnico del Consejo Nacional contra la Trata de Personas', 'abbreviation' => 'CTCNCTP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comités Locales de Derechos de la Niñez y de la Adolscencia', 'abbreviation' => 'CAL'));
		Institution::create(array('organization_id' => 1, 'name' =>'Comité Local de Derechos de la Niñez y Adolescencia', 'abbreviation' => 'CLD'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo de Ética Policial', 'abbreviation' => 'CEP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo de Ministros', 'abbreviation' => 'Consejo de Ministros'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo Nacional contra la Trata de Personas', 'abbreviation' => 'CNCTP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo Nacional de Atención Integral a la Persona con Discapacidad,', 'abbreviation' => 'CONAIPD'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo Nacional de Atención Integral a los Programas de los Adultos Mayores', 'abbreviation' => 'CONAIPAM'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo Nacional de la Judicatura', 'abbreviation' => 'CNJ'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo Nacional de la Niñez y de la Adolescencia', 'abbreviation' => 'CONNA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo Nacional para la Protección y Desarrollo de la Persona Migrante y su Familia', 'abbreviation' => 'CONMIGRANTES'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo Superior de Salud Pública', 'abbreviation' => 'CSSP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Consejo Superior del Trabajo', 'abbreviation' => 'CST'));
		Institution::create(array('organization_id' => 1, 'name' =>'Corte Suprema de Justicia', 'abbreviation' => 'CSJ'));
		Institution::create(array('organization_id' => 1, 'name' =>'Dirección General de Centros Penales', 'abbreviation' => 'DGCP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Dirección General de Estadística y Censos', 'abbreviation' => 'DIGESTYC'));
		Institution::create(array('organization_id' => 1, 'name' =>'Dirección General de Migración y Extranjería', 'abbreviation' => 'DGME'));
		Institution::create(array('organization_id' => 1, 'name' =>'Dirección Nacional de Atención a Víctimas y Migración Forzada.', 'abbreviation' => 'DNAVMF'));
		Institution::create(array('organization_id' => 1, 'name' =>'Escuela de Capacitación Fiscal', 'abbreviation' => 'ECF'));
		Institution::create(array('organization_id' => 1, 'name' =>'Escuela de Capacitación Judicial', 'abbreviation' => 'ECJ'));
		Institution::create(array('organization_id' => 1, 'name' =>'Escuela Militar "Capitán General Gerardo Barrios"', 'abbreviation' => 'EMCGGB'));
		Institution::create(array('organization_id' => 1, 'name' =>'Escuela Penitenciaria', 'abbreviation' => 'EP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Estado Mayor Conjunto de la Fuerza Armada', 'abbreviation' => 'EMC'));
		Institution::create(array('organization_id' => 1, 'name' =>'La Federación de Asociaciones de Abogados de El Salvador', 'abbreviation' => 'FEDAES'));
		Institution::create(array('organization_id' => 1, 'name' =>'Fiscalía General de la República', 'abbreviation' => 'FGR'));
		Institution::create(array('organization_id' => 1, 'name' =>'Fondo Ambiental de El Salvador', 'abbreviation' => 'FONAES'));
		Institution::create(array('organization_id' => 1, 'name' =>'Fondo de Inversión Social para el Desarrollo Local', 'abbreviation' => 'FISDL'));
		Institution::create(array('organization_id' => 1, 'name' =>'Fondo Nacional de la Vivienda Popular', 'abbreviation' => 'FONAVIPO'));
		Institution::create(array('organization_id' => 1, 'name' =>'Fondo Solidario para la Familia Microempresaria', 'abbreviation' => 'FOSOFAMILIA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Fuerza Armada de El Salvador', 'abbreviation' => 'FAES'));
		Institution::create(array('organization_id' => 1, 'name' =>'El Grupo Parlamentario de Mujeres', 'abbreviation' => 'GPM'));
		Institution::create(array('organization_id' => 1, 'name' =>'Inspectoría General de la Policía Nacional Civil', 'abbreviation' => 'IGSP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto de Acceso a la Información Púbica', 'abbreviation' => 'IAIP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto de Legalización de la Propiedad', 'abbreviation' => 'ILP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto de Medicina Legal', 'abbreviation' => 'IML'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto Nacional de la Juventud', 'abbreviation' => 'INJUVE'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto Salvadoreño de Formación Profesional', 'abbreviation' => 'INSAFORP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto Salvadoreño del Seguro Social', 'abbreviation' => 'ISSS'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto Salvadoreño para el Desarrollo de la Mujer', 'abbreviation' => 'ISDEMU'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto Salvadoreño para el Desarrollo Integral de la Niñez y la Adolescencia', 'abbreviation' => 'ISNA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Instituto Salvadoreño para ell Desarrollo Integral de la Niñez y la Adolescencia', 'abbreviation' => 'ISNA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Juntas de Protección de la Niñez y de la Adolescencia', 'abbreviation' => 'JPNA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Mecanismo Interinstitucional para la Atención, Implementación y Seguimiento de los Compromisos Internacionales del Estado en Materia de Derechos Humanos', 'abbreviation' => 'MIASCIEMDH'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Agricultura y Ganadería', 'abbreviation' => 'MAG'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Cultura', 'abbreviation' => 'MICULTURA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Defensa Nacional', 'abbreviation' => 'MDN'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Desarrollo Local', 'abbreviation' => 'MINDEL'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Economía', 'abbreviation' => 'MINEC'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Educación, Ciencia y Tecnología', 'abbreviation' => 'MINEDUCYT'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Gobernación y Desarrollo Territorial', 'abbreviation' => 'MGDT'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Hacienda', 'abbreviation' => 'MH'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Justicia y Seguridad Pública', 'abbreviation' => 'MJSP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Medio Ambiente y Recursos Naturales', 'abbreviation' => 'MARN'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Obras Públicas y de Transporte', 'abbreviation' => 'MOPT'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Relaciones Exteriores', 'abbreviation' => 'MRREE'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Salud', 'abbreviation' => 'MINSAL'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Trabajo y Prevision Social', 'abbreviation' => 'MTPS'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Turismo', 'abbreviation' => 'MITUR'));
		Institution::create(array('organization_id' => 1, 'name' =>'Ministerio de Vivienda', 'abbreviation' => 'MIVI'));
		Institution::create(array('organization_id' => 1, 'name' =>'Organismo de Inteligencia del Estado', 'abbreviation' => 'OIE'));
		Institution::create(array('organization_id' => 1, 'name' =>'Presidencia de la República', 'abbreviation' => 'PR'));
		Institution::create(array('organization_id' => 1, 'name' =>'Policía Nacional Civil', 'abbreviation' => 'PNC'));
		Institution::create(array('organization_id' => 1, 'name' =>'Procuraduría General de la República', 'abbreviation' => 'PGR'));
		Institution::create(array('organization_id' => 1, 'name' =>'Procuraduría para la Defensa de los Derechos Humanos', 'abbreviation' => 'PDDH'));
		Institution::create(array('organization_id' => 1, 'name' =>'Red de Atención Compartida', 'abbreviation' => 'RAC'));
		Institution::create(array('organization_id' => 1, 'name' =>'Registro Nacional de las Personas Naturales', 'abbreviation' => 'RNPR'));
		Institution::create(array('organization_id' => 1, 'name' =>'Sistema Nacional de Protección Integral de la Niñez y de la Adolescencia', 'abbreviation' => 'SNPINA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Sistema  Nacional  de  Atención  y  Protección  Integral  a  Personas  en  Condición  de Desplazamiento Forzado Interno', 'abbreviation' => 'SINAPI'));
		Institution::create(array('organization_id' => 1, 'name' =>'Sistema Nacional de Desarrollo, Protección e Inclusión Social', 'abbreviation' => 'SNDPI'));
		Institution::create(array('organization_id' => 1, 'name' =>'Sistema Nacional de Gestión del Medio Ambiente', 'abbreviation' => 'SINAMA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Sistema Nacional de Información sobre Trata de Personas', 'abbreviation' => 'SNITP'));
		Institution::create(array('organization_id' => 1, 'name' =>'Sistema Nacional Integrado de Salud', 'abbreviation' => 'SNIS'));
		Institution::create(array('organization_id' => 1, 'name' =>'Tribunal de Apelaciones', 'abbreviation' => 'TA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Tribunal Disciplinario Tribunal de Apelaciones', 'abbreviation' => 'TD /TA'));
		Institution::create(array('organization_id' => 1, 'name' =>'Tribunal Supremo Electoral', 'abbreviation' => 'TSE'));
		Institution::create(array('organization_id' => 1, 'name' =>'Unidad de Derechos Humanos', 'abbreviation' => 'UDH'));
		Institution::create(array('organization_id' => 1, 'name' =>'Unidad Técnica Ejecutiva del Sector Justicia', 'abbreviation' => 'UTE'));
		Institution::create(array('organization_id' => 1, 'name' =>'Universidad de El Salvador', 'abbreviation' => 'UES'));
		Institution::create(array('organization_id' => 1, 'name' =>'Viceministerio de Cooperación para el Desarrollo', 'abbreviation' => 'VCD'));
		Institution::create(array('organization_id' => 1, 'name' =>'Centros de Formación Municipal', 'abbreviation' => 'CFM'));
	}
}
