<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\LifeQueryManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;

use App\Kwaai\Security\Repositories\Journal\JournalInterface;

use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\LifeQueryInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\FreeVictimsLibertyDeprivation\FreeVictimsLibertyDeprivationInterface;

use Carbon\Carbon;

use Illuminate\Database\DatabaseManager;

use Illuminate\Translation\Translator;

use Illuminate\Config\Repository;

use Illuminate\Cache\CacheManager;
use function GuzzleHttp\json_encode;

class LifeQueryManager extends AbstractLaravelValidator implements LifeQueryManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
   *  LifeQuery Interface
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Life\LifeQueryInterface
   *
   */
  protected $LifeQuery;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    LifeQueryInterface $LifeQuery,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->Journal = $Journal;

    $this->GridEncoder = $GridEncoder;

    $this->LifeQuery = $LifeQuery;

    $this->Carbon = $Carbon;

    $this->DB = $DB;

		$this->Lang = $Lang;

		$this->Config = $Config;

    $this->mapIds = array(
      'sonsonate' => 'SV-SO',
      'santa ana' => 'SV-SA',
      'ahuachapan' => 'SV-Ah',
      'ahuachapán' => 'SV-Ah',
      'chalatenango' => 'SV-CH',
      'la libertad' => 'SV-LI',
      'san salvador' => 'SV-SS',
      'la paz' => 'SV-PA',
      'cuscatlan' => 'SV-CU',
      'cuscatlán' => 'SV-CU',
      'cabañas' => 'SV-CA',
      'san miguel' => 'SV-SM',
      'usulutan' => 'SV-US',
      'usulután' => 'SV-US',
      'morazan' => 'SV-MO',
      'morazán' => 'SV-MO',
      'san vicente' => 'SV-SV',
      'la union' => 'SV-UN',
      'la unión' => 'SV-UN',
    );

    // $ids = [];
  }

  function removeAccent($string)
  {
    $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
    $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
    $texto = str_replace($no_permitidas, $permitidas ,$string);
    return $texto;
  }

  function substrwords($text, $maxchar, $end='.') 
  {
    if (strlen($text) > $maxchar || $text == '') 
    {
      $words = preg_split('/\s/', $text);
      $output = '';
      $i = 0;
      while (1) 
      {
        $length = strlen($output)+strlen($words[$i]);
        if ($length > $maxchar) 
        {
          break;
        } 
        else 
        {
          $output .= " " . $words[$i];
          ++$i;
        }
      }
      $output .= $end;
    } 
    else 
    {
      $output = $text;
    }
    return $output;
  }

  function homicidesRatesYearFilter()
  {
    $data = array();

    $this->LifeQuery->homicidesIncidentYearFilter()->each(function ($LifeQuery) use(&$data)
    {
      $data[] = $LifeQuery->year;
    });

    return $data;
  }

  function illegitimateAggressionYearFilter()
  {
    $data = array();

    $this->LifeQuery->illegitimateAggressionYearFilter()->each(function ($LifeQuery) use(&$data)
    {
      $data[] = $LifeQuery->year;
    });

    return $data;
  }

  function investigatedOfficersYearFilter()
  {
    $data = array();

    $this->LifeQuery->investigatedOfficersYearFilter()->each(function ($LifeQuery) use(&$data)
    {
      $data[] = $LifeQuery->year;
    });

    return $data;
  }

  function aggressionDeathYearFilter()
  {
    $data = array();

    $this->LifeQuery->aggressionDeathYearFilter()->each(function ($LifeQuery) use(&$data)
    {
      $data[] = $LifeQuery->year;
    });

    return $data;
  }

  function homicidesRatesGenderFilter()
  {
    $data = array();

    $this->LifeQuery->homicidesIncidentSexFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->sex, 'value' => $LifeQuery->sex);
    });

    return $data;
  }

  function investigatedOfficersGenderFilter()
  {
    $data = array();

    $this->LifeQuery->investigatedOfficersSexFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->sex, 'value' => $LifeQuery->sex);
    });

    return $data;
  }

  function investigatedOfficersOfficersSexFilter()
  {
    $data = array();

    $this->LifeQuery->investigatedOfficersOfficersSexFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->sex, 'value' => $LifeQuery->sex);
    });

    return $data;
  }

  function homicidesRatesDepartmentFilter()
  {
    $data = array();

    $this->LifeQuery->homicidesIncidentDepartmentFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->department, 'value' => $LifeQuery->department);
    });

    return $data;
  }

  function illegitimateAggressionsDepartmentFilter()
  {
    $data = array();

    $this->LifeQuery->illegitimateAggressionsDepartmentFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->department, 'value' => $LifeQuery->department);
    });

    return $data;
  }

  function aggressionDeathDepartmentFilter()
  {
    $data = array();

    $this->LifeQuery->aggressionDeathDepartmentFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->department, 'value' => $LifeQuery->department);
    });

    return $data;
  }

  function investigatedOfficersDepartmentFilter()
  {
    $data = array();

    $this->LifeQuery->investigatedOfficersDepartmentFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->department, 'value' => $LifeQuery->department);
    });

    return $data;
  }

  function homicidesRatesMunicipalityFilter()
  {
    $data = array();

    $this->LifeQuery->homicidesIncidentMunicipalityFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->municipality, 'value' => $LifeQuery->municipality);
    });

    return $data;
  }

  function illegitimateAggressionsMunicipalityFilter()
  {
    $data = array();

    $this->LifeQuery->illegitimateAggressionsMunicipalityFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->municipality, 'value' => $LifeQuery->municipality);
    });

    return $data;
  }

  function aggresionDeathMunicipalityFilter()
  {
    $data = array();

    $this->LifeQuery->aggresionDeathMunicipalityFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->municipality, 'value' => $LifeQuery->municipality);
    });

    return $data;
  }

  function investigatedOfficersMunicipalityFilter()
  {
    $data = array();

    $this->LifeQuery->investigatedOfficersMunicipalityFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->municipality, 'value' => $LifeQuery->municipality);
    });

    return $data;
  }

  function homicidesRatesWeaponTypeFilter()
  {
    $data = array();

    $this->LifeQuery->homicidesIncidentWeaponTypeFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->weapon_type, 'value' => $LifeQuery->weapon_type);
    });

    return $data;
  }

  function investigatedOfficersWeaponTypeFilter()
  {
    $data = array();

    $this->LifeQuery->investigatedOfficersWeaponTypeFilter()->each(function ($LifeQuery) use (&$data) {
      $data[] = array('label' => $LifeQuery->weapon_type, 'value' => $LifeQuery->weapon_type);
    });

    return $data;
  }

  /**
   *  Incidencia de homicidios a nivel nacional
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicides(array $input)
  {
    $years = $this->homicidesRatesYearFilter();
    $data = $excel = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $headers = [
      'grid_col_1' => ['label' => 'Municipio', 'width' => '70%'],
      'grid_col_2' => ['label' => 'Incidentes de homicidios', 'width' => '30%', 'align' => 'center'],
    ];

    $excel['header'] = array(
      'department' => 'Departamento',
      'municipality' => 'Municipio',
      'homicides' => 'Homicidios',
    );

    $this->LifeQuery->homicidesDepartmentsByYearAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['weapon_type'])->each(function ($LifeQuery) use (&$data, $headers)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));

      $data['data'][] = array(
        'id' => $this->mapIds[mb_strtolower($departmentName)],
        'department' => $departmentName,
        'value' => $LifeQuery->count,
        'headers' => $headers,
        'legend' => ' Homicidios'
      );
    });

    if (empty($data)) 
    {
      $data['data'] = array();
    }

    $homicidesByMunicipality = array();

    $this->LifeQuery->homicidesMunicipalitiesByYearAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['weapon_type'])->each(function ($LifeQuery) use (&$homicidesByMunicipality, &$excel)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $homicidesByMunicipality[mb_strtolower($departmentName)][] = array(
          'grid_col_1' => $LifeQuery->municipality,
          'grid_col_2' => $LifeQuery->count
      );

      $excel['details']['department'][] = mb_strtoupper($departmentName);
      $excel['details']['municipality'][] = mb_strtoupper($LifeQuery->municipality);
      $excel['details']['homicides'][] = $LifeQuery->count;
    });

    if (!empty($data['data']))
    {
      foreach ($data['data'] as $key => &$department)
      {
        if(!empty($homicidesByMunicipality[mb_strtolower($department['department'])]))
        {
          $department['rows'] = $homicidesByMunicipality[mb_strtolower($department['department'])];
        }
        else
        {
          $department['rows'] = array();
        }
      }
    }
    else
    {
      $data['data'] = array();
    }

    $data['year'] = $years;
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesRate(array $input)
  {
    $years = $this->homicidesRatesYearFilter();
    $data = $excel = array();

    $headers = [
      'grid_col_1' => ['label' => 'Municipio', 'width' => '70%'],
      'grid_col_2' => ['label' => 'Tasa de homicidios', 'width' => '30%', 'align' => 'center'],
    ];

    $maxYear = max($years);

    if (empty($input['filter']['year'])) 
    {
      $input['filter']['year'] = array($maxYear);
    }

    $this->LifeQuery->homicidesDepartmentsByYearAndWeaponType($input['filter']['year'], null, null, null)->each(function ($LifeQuery) use (&$data, $headers)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $data['data'][] = array(
        'id' => $this->mapIds[mb_strtolower($departmentName)],
        'department' => $departmentName,
        'value' => $LifeQuery->count,
        'headers' => $headers,
        'legend' => ' homicidios por cada 100,000 habitantes');
    });

    $populationByDepartment = array();

    $this->LifeQuery->territoryPopulationByDepartment($input['filter']['year'])->each(function ($LifeQuery) use (&$populationByDepartment)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $populationByDepartment[mb_strtolower($departmentName)] = $LifeQuery->sum;

      if (mb_strtolower($departmentName) != $this->removeAccent(mb_strtolower($departmentName)))
      {
        $populationByDepartment[$this->removeAccent(mb_strtolower($departmentName))] = $LifeQuery->sum;
      }
    });
    if (!empty($data['data']))
    {
      foreach ($data['data'] as $key => &$department)
      {
        if (!empty($department['value']))
        {
          $department['value'] = round(($department['value'] / $populationByDepartment[mb_strtolower($department['department'])]) * 100000, 2);
        }
      }
    }else
    {
      $data['data'] = array();
    }

    $excel['header'] = array(
      'department' => 'Departamento',
      'municipality' => 'Municipio',
      'homicidesRate' => 'Tasa de homicidios por cada 100,000 habitantes',
    );

    $homicidesByMunicipality = array();

    $this->LifeQuery->homicidesMunicipalitiesByYearAndWeaponType($input['filter']['year'], null, null, null)->each(function ($LifeQuery) use (&$homicidesByMunicipality, &$excel)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $municipalityName = rtrim(ltrim($LifeQuery->municipality));
      $homicidesByMunicipality[mb_strtolower($departmentName)][] = array(
        'grid_col_1' => $municipalityName,
        'grid_col_2' => $LifeQuery->count
      );

      // $excel['details']['department'][] = mb_strtoupper($departmentName);
      // $excel['details']['municipality'][] = mb_strtoupper($LifeQuery->municipality);
      // $excel['details']['homicidesRate'][] = $LifeQuery->count;

    });

    foreach ($data['data'] as $key => &$department)
    {
      if(!empty($homicidesByMunicipality[mb_strtolower($department['department'])]))
      {
        $department['rows'] = $homicidesByMunicipality[mb_strtolower($department['department'])];
      }
      else
      {
        $department['rows'] = array();
      }
    }

    $municipalityPopulation = array();

    $this->LifeQuery->territoryPopulationByMunicipality($input['filter']['year'])->each(function($LifeQuery) use (&$municipalityPopulation)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $municipalityName = rtrim(ltrim($LifeQuery->municipality));
      $municipalityPopulation[mb_strtolower($departmentName)][mb_strtolower($municipalityName)] = $LifeQuery->population;

      if (mb_strtolower($municipalityName) != $this->removeAccent($municipalityName)) 
      {
        $municipalityPopulation[mb_strtolower($departmentName)][$this->removeAccent(mb_strtolower($municipalityName))] = $LifeQuery->population;
      }

      if(mb_strtolower($departmentName) != $this->removeAccent($departmentName))
      {
        $municipalityPopulation[$this->removeAccent(mb_strtolower($departmentName))][mb_strtolower($municipalityName)] = $LifeQuery->population;

        if (mb_strtolower($municipalityName) != $this->removeAccent(mb_strtolower($municipalityName)))
        {
          $municipalityPopulation[$this->removeAccent(mb_strtolower($departmentName))][$this->removeAccent(mb_strtolower($municipalityName))] = $LifeQuery->population;
        }
      }
    });

    foreach ($data['data'] as $key => &$department)
    {
      foreach ($department['rows'] as $key => &$municipality)
      {
        $municipality['grid_col_2'] = round(($municipality['grid_col_2'] / $municipalityPopulation[mb_strtolower($department['department'])][mb_strtolower($municipality['grid_col_1'])]) * 100000, 2);
        $excel['details']['department'][] = $department['department'];
        $excel['details']['municipality'][] = $municipality['grid_col_1'];
        $excel['details']['homicidesRate'][] = $municipality['grid_col_2'];
      }
    }

    foreach ($data['data'] as $key => &$data2) 
    {
      $sorting = collect($data2['rows'])->sortByDesc('grid_col_2')->toArray();
      $data2['rows'] = array_values($sorting);
    }

    $data['year'] = $years;
    $data['gender'] = $this->homicidesRatesGenderFilter();
    $data['municipality'] = $this->homicidesRatesDepartmentFilter();
    $data['department'] = $this->homicidesRatesMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getWomenHomicides(array $input)
  {

    $years = $this->homicidesRatesYearFilter();

    $data = $excel = array();

    $headers = [
      'grid_col_1' => ['label' => 'Municipio', 'width' => '70%'],
      'grid_col_2' => ['label' => 'Incidente de Feminicidios', 'width' => '30%', 'align' => 'center'],
    ];

    $excel['header'] = array(
      'department' => 'Departamento',
      'municipality' => 'Municipio',
      'homicides' => 'Incidente de Feminicidios',
    );

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $this->LifeQuery->homicidesOfWomenDepartmentsByYearAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['weapon_type'])->each(function($LifeQuery) use (&$data, $headers) {
      $departmentName = rtrim(ltrim($LifeQuery->department));

      $data['data'][] = array(
        'id' => $this->mapIds[mb_strtolower($departmentName)],
        'department' => $departmentName,
        'value' => $LifeQuery->count,
        'headers' => $headers,
        'legend' => ' Feminicidios'
      );
    });

    $womenHomicidesMunicipalities = array();

    $this->LifeQuery->homicidesOfWomenMunicipalitiesByYearAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['weapon_type'])->each(function($LifeQuery) use(&$womenHomicidesMunicipalities, &$excel)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $womenHomicidesMunicipalities[mb_strtolower($departmentName)][] = array(
        'grid_col_1' => $LifeQuery->municipality,
        'grid_col_2' => $LifeQuery->count
      );

      $excel['details']['department'][] = mb_strtoupper($departmentName);
      $excel['details']['municipality'][] = mb_strtoupper($LifeQuery->municipality);
      $excel['details']['homicides'][] = $LifeQuery->count;
    });

    if(!empty($data['data']))
    {
      foreach ($data['data'] as $key => &$department)
      {
        if(!empty($womenHomicidesMunicipalities[mb_strtolower($department['department'])]))
        {
          $department['rows'] = $womenHomicidesMunicipalities[mb_strtolower($department['department'])];
        }else
        {
          $department['rows'] = array();
        }
      }
    }
    else
    {
      $data['data'] = array();
    }

    $data['year'] = $years;
    $data['gender'] = $this->homicidesRatesGenderFilter();
    $data['municipality'] = $this->homicidesRatesDepartmentFilter();
    $data['department'] = $this->homicidesRatesMunicipalityFilter();
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getWomenHomicidesRates(array $input)
  {

    $years = $this->homicidesRatesYearFilter();

    $maxYear = max($years);

    if (empty($input['filter']['year'])) 
    {
      $input['filter']['year'] = array($maxYear);
    }

    $data = $excel = array();

    $headers = [
      'grid_col_1' => ['label' => 'Municipio', 'width' => '70%'],
      'grid_col_2' => ['label' => 'Tasa de Feminicidios', 'width' => '30%', 'align' => 'center'],
    ];

    $excel['header'] = array(
      'department' => 'Departamento',
      'municipality' => 'Municipio',
      'homicidesRate' => 'Tasa de Feminicidios',
    );

    $this->LifeQuery->homicidesOfWomenDepartmentsByYearAndWeaponType($input['filter']['year'], null, null, $input['filter']['weapon_type'])->each(function($LifeQuery) use(&$data, $headers)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));

      $data['data'][] = array(
        'id' => $this->mapIds[mb_strtolower($departmentName)],
        'department' => $departmentName,
        'value' => $LifeQuery->count,
        'headers' => $headers,
        'legend' => ' Feminicidios por cada 100,000 habitantes'
      );
    });

    $womenPopulation = array();

    $this->LifeQuery->territoryWomenPopulationByDepartment($input['filter']['year'])->each(function($LifeQuery) use(&$womenPopulation)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $womenPopulation[mb_strtolower($departmentName)] = $LifeQuery->sum;

      if (mb_strtolower($departmentName) != $this->removeAccent(mb_strtolower($departmentName)))
      {
        $womenPopulation[$this->removeAccent(mb_strtolower($departmentName))] = $LifeQuery->sum;
      }

    });


    if(!empty($data['data']))
    {
      foreach ($data['data'] as $key => &$department)
      {
        if (!empty($department['value']))
        {
          $department['value'] = round(($department['value'] / $womenPopulation[mb_strtolower($department['department'])]) * 100000, 2);
        }else
        {
          $department['value'] = array();
        }
      }
    }
    else
    {
      $data['data'] = array();
    }

    $womenHomicidesByMunicipality = array();

    $this->LifeQuery->homicidesOfWomenMunicipalitiesByYearAndWeaponType($input['filter']['year'], null, null, $input['filter']['weapon_type'])->each(function($LifeQuery) use(&$womenHomicidesByMunicipality)
    {
      $departmentName = ltrim(rtrim($LifeQuery->department));
      $municipalityName = rtrim(ltrim($LifeQuery->municipality));
      $womenHomicidesByMunicipality[mb_strtolower($departmentName)][] = array(
        'grid_col_1' => $municipalityName,
        'grid_col_2' => $LifeQuery->count
      );
    });

    foreach ($data['data'] as $key => &$department)
    {
      if(!empty($womenHomicidesByMunicipality[mb_strtolower($department['department'])]))
      {
        $department['rows'] = $womenHomicidesByMunicipality[mb_strtolower($department['department'])];
      }
      else
      {
        $department['rows'] = array();
      }
    }

    $womenPopulationByMunicipality = array();

    $this->LifeQuery->territoryWomenPopulationByMunicipality($input['filter']['year'])->each(function($LifeQuery) use(&$womenPopulationByMunicipality)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $municipalityName = rtrim(ltrim($LifeQuery->municipality));
      $womenPopulationByMunicipality[mb_strtolower($departmentName)][mb_strtolower($municipalityName)] = $LifeQuery->population;

      if (mb_strtolower($municipalityName) != $this->removeAccent(mb_strtolower($municipalityName)))
      {
        $womenPopulationByMunicipality[mb_strtolower($departmentName)][$this->removeAccent(mb_strtolower($municipalityName))] = $LifeQuery->population;
      }


      if (mb_strtolower($departmentName) != $this->removeAccent(mb_strtolower($departmentName)))
      {
        $womenPopulationByMunicipality[$this->removeAccent(mb_strtolower($departmentName))][mb_strtolower($municipalityName)] = $LifeQuery->population;

        if (mb_strtolower($municipalityName) != $this->removeAccent(mb_strtolower($municipalityName)))
        {
          $womenPopulationByMunicipality[$this->removeAccent(mb_strtolower($departmentName))][$this->removeAccent(mb_strtolower($municipalityName))] = $LifeQuery->population;
        }
      }
    });

    foreach ($data['data'] as $key => &$department)
    {
      foreach ($department['rows'] as $key => &$municipality)
      {
        $municipality['grid_col_2'] = round(($municipality['grid_col_2'] / $womenPopulationByMunicipality[mb_strtolower($department['department'])][mb_strtolower($municipality['grid_col_1'])]) * 100000, 2);

        $excel['details']['department'][] = $department['department'];
        $excel['details']['municipality'][] = $municipality['grid_col_1'];
        $excel['details']['homicidesRate'][] = $municipality['grid_col_2'];
      }
    }

    foreach ($data['data'] as $key => &$data2) 
    {
      $sorting = collect($data2['rows'])->sortByDesc('grid_col_2')->toArray();
      $data2['rows'] = array_values($sorting);
    }

    $data['year'] = $years;
    $data['gender'] = $this->homicidesRatesGenderFilter();
    $data['municipality'] = $this->homicidesRatesDepartmentFilter();
    $data['department'] = $this->homicidesRatesMunicipalityFilter();
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getIllegitimateAgressions (Array $input)
  {

    $years = $this->illegitimateAggressionYearFilter();

    $data = $excel = array();

    $headers = [
      'grid_col_1' => ['label' => 'Municipio', 'width' => '70%'],
      'grid_col_2' => ['label' => 'Incidencia de agresiones ilegítimas', 'width' => '30%', 'align' => 'center'],
    ];

    $excel['header'] = array(
      'department' => 'Departamento',
      'municipality' => 'Municipio',
      'aggressions' => 'Incidencia de agresiones ilegítimas',
    );

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $this->LifeQuery->homicidesByIllegitimateAgressionsDepartmentsByYear($input['filter']['yearFrom'], $input['filter']['yearTo'])->each(function($LifeQuery) use(&$data, $headers)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));

      $data['data'][] = array(
        'id' => $this->mapIds[mb_strtolower($departmentName)],
        'department' => $departmentName,
        'value' => $LifeQuery->count,
        'headers' => $headers,
        'legend' => ' Agresiones ilegítimas'
      );
    });

    $illegimitateAgressionsByMunicipality = array();

    $this->LifeQuery->homicidesByIllegitimateAgressionsMunicipalitiesByYear($input['filter']['yearFrom'], $input['filter']['yearTo'])->each(function($LifeQuery) use(&$illegimitateAgressionsByMunicipality, &$excel)
    {
      $departmentName = rtrim(ltrim($LifeQuery->department));
      $illegimitateAgressionsByMunicipality[mb_strtolower($departmentName)][] = array(
        'grid_col_1' => $LifeQuery->municipality,
        'grid_col_2' => $LifeQuery->count,
      );

      $excel['details']['department'][] = mb_strtoupper($departmentName);
      $excel['details']['municipality'][] = mb_strtoupper($LifeQuery->municipality);
      $excel['details']['aggressions'][] = $LifeQuery->count;
    });

    foreach ($data['data'] as $key => &$department) {
      $department['rows'] = $illegimitateAgressionsByMunicipality[mb_strtolower($department['department'])];
    }

    $data['year'] = $years;
    $data['gender'] = $this->homicidesRatesGenderFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getWeaponTypeAgressions(Array $input)
  {

    $years = $this->homicidesRatesYearFilter();

    $data = $excel = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'category' => 'Armas',
      'homicides' => 'Porcentaje de homicidios',
    );

    $this->LifeQuery->homicidesIncidentByWeaponTypeAndYearAndGenderAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'],
    $input['filter']['department'], $input['filter']['municipality'])->each(function($LifeQuery) use(&$data, &$excel)
    {
      $weapons = rtrim(ltrim($LifeQuery->weapons));

      $data['data'][] = array(
        'category' => $weapons,
        'value' => $LifeQuery->value
      );

      $excel['details']['category'][] = $weapons;
      $excel['details']['homicides'][] = $LifeQuery->value;
    });

    $totalHomicides = array_sum($excel['details']['homicides']);

    foreach ($excel['details']['homicides'] as $key => &$value) 
    {
      $percent = round(($value / $totalHomicides) * 100, 2);

      $value = "$percent% ($value)";
    }

    $data['year'] = $years;
    $data['gender'] = $this->homicidesRatesGenderFilter();
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['municipality'] = $this->homicidesRatesMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getGenderAgressions(Array $input)
  {

    $years = $this->homicidesRatesYearFilter();
    $data = $excel = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'category' => 'Género',
      'homicides' => 'Porcentaje de homicidios',
    );

    $this->LifeQuery->homicidesIncidentBySexAndYearAndMunicipalityAndDepartmentAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['municipality'],
    $input['filter']['department'], $input['filter']['weapon_type'])->each(function($LifeQuery) use(&$data, &$excel)
    {

      $data['data'][] = array(
        'category' => $LifeQuery->sex,
        'value' => $LifeQuery->value
      );

      $gender = '';

      if($LifeQuery->sex == 'M')
      {
        $gender = 'Masculino';
      }
      else if($LifeQuery->sex == 'F')
      {
        $gender = 'Femenino';
      }
      else if($LifeQuery->sex == 'I')
      {
        $gender = 'Indeterminado';
      }
      else 
      {
        $gender = $LifeQuery->sex;
      }

      $excel['details']['category'][] = $gender;
      $excel['details']['homicides'][] = $LifeQuery->value;
    });

    $totalHomicides = array_sum($excel['details']['homicides']);

    foreach ($excel['details']['homicides'] as $key => &$value) 
    {
      $percent = round(($value / $totalHomicides) * 100, 2);

      $value = "$percent% ($value)";
    }

    $data['year'] = $years;
    $data['genderColor'] = true;
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['municipality'] = $this->homicidesRatesMunicipalityFilter();
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesIncidentByAge(Array $input)
  {
    $data = $excel = array();
    $years = $this->homicidesRatesYearFilter();
    $ageRange = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'age' => 'Rango de edades',
      'homicides' => 'Homicidios',
    );

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 0, 11)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Niñez (0-11)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 12, 14)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Adolescencia (12-14)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 15, 29)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Juventud (15-29)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 30, 59)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Adultez (30-59)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 60, 150)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Adulto Mayor (60++)'][] = $LifeQuery->count;
    });

    foreach ($ageRange as $key => $age)
    {
      $data['data'][] = array(
        'label' => $key,
        'value' => array_sum($age),
      );

      $excel['details']['age'][] = $key;
      $excel['details']['homicides'][] = array_sum($age);
    }

    $data['labels']['categoryAxis'] = 'Rango de edad';
    $data['labels']['valueAxis'] = 'Número de homicidios';
    $data['labels']['categoryAxisRotation'] = 270;
    $data['year'] = $years;
    $data['gender'] = $this->homicidesRatesGenderFilter();
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['municipality'] =  $this->homicidesRatesMunicipalityFilter();
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesRateByAge(Array $input)
  {
    $data = $excel = array();
    $years = $this->homicidesRatesYearFilter();
    $ageRange = array();

    $maxYear = max($years);

    if (empty($input['filter']['year'])) 
    {
      $input['filter']['year'] = array($maxYear);
    }

    $excel['header'] = array(
      'age' => 'Rango de edades',
      'homicidesRate' => 'Tasa de homicidios por 100,000 habitantes',
    );

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType($input['filter']['year'], null, null, $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], null, 0, 11)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Niñez (0-11)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType($input['filter']['year'], null, null, $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], null, 12, 14)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Adolescencia (12-14)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType($input['filter']['year'], null, null, $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], null, 15, 29)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Juventud (15-29)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType($input['filter']['year'], null, null, $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], null, 30, 59)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Adultez (30-59)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentByAgeAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType($input['filter']['year'], null, null, $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], null, 60, 150)->each(function($LifeQuery) use(&$ageRange)
    {
      $ageRange['Adulto Mayor (60++)'][] = $LifeQuery->count;
    });

    foreach ($ageRange as $key => $age)
    {
      $data['data'][] = array(
        'label' => $key,
        'value' => array_sum($age),
      );

      $excel['details']['age'][] = $key;
    }

    $data['labels']['categoryAxis'] = 'Rangos de edad';
    $data['labels']['categoryAxisRotation'] = 270;
    $data['labels']['valueAxis'] = 'Tasa de homicidios por 100,000 habitantes';
    $populationByAge = array();

    $this->LifeQuery->populationByAgeAndYear($input['filter']['year'], null, null, 0, 11)->each(function($LifeQuery) use(&$populationByAge)
    {
      $populationByAge['Niñez (0-11)'][] = $LifeQuery->sum;
    });

    $this->LifeQuery->populationByAgeAndYear($input['filter']['year'], null, null, 12, 14)->each(function($LifeQuery) use(&$populationByAge)
    {
      $populationByAge['Adolescencia (12-14)'][] = $LifeQuery->sum;
    });

    $this->LifeQuery->populationByAgeAndYear($input['filter']['year'], null, null, 15, 29)->each(function($LifeQuery) use(&$populationByAge)
    {
      $populationByAge['Juventud (15-29)'][] = $LifeQuery->sum;
    });

    $this->LifeQuery->populationByAgeAndYear($input['filter']['year'], null, null, 30, 59)->each(function($LifeQuery) use(&$populationByAge)
    {
      $populationByAge['Adultez (30-59)'][] = $LifeQuery->sum;
    });

    $this->LifeQuery->populationByAgeAndYear($input['filter']['year'], null, null, 60, 150)->each(function($LifeQuery) use(&$populationByAge)
    {
      $populationByAge['Adulto Mayor (60++)'][] = $LifeQuery->sum;
    });

    foreach ($data['data'] as $key => &$population)
    {
      $population['value'] = round(($population['value'] / array_sum($populationByAge[$population['label']])) * 100000, 2);
      $excel['details']['homicidesRate'][] = $population['value'] . "%";
    }

    $data['year'] = $years;
    $data['gender'] = $this->homicidesRatesGenderFilter();
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['municipality'] =  $this->homicidesRatesMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesOnWomenByAgeAndYear(Array $input)
  {
    $data = $excel = array();
    $years = $this->homicidesRatesYearFilter();

    $homicidesOnWomenByYear = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'age' => 'Rango de edades',
      'homicides' => 'Incidencia de muertes violentas de mujeres'
    );

    $this->LifeQuery->homicidesIncidentOnWomenByAgeAndYearAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 0, 11)->each(function($LifeQuery) use (&$homicidesOnWomenByYear)
    {
      $homicidesOnWomenByYear['Niñez (0-11)'][] = $LifeQuery->count;
    });

    $this->LifeQuery->homicidesIncidentOnWomenByAgeAndYearAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 12, 14)->each(function($LifeQuery) use (&$homicidesOnWomenByYear)
    {
      $homicidesOnWomenByYear['Adolescencia (12-14)'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentOnWomenByAgeAndYearAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 15, 29)->each(function($LifeQuery) use (&$homicidesOnWomenByYear)
    {
      $homicidesOnWomenByYear['Juventud (15-29)'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentOnWomenByAgeAndYearAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 30, 59)->each(function($LifeQuery) use (&$homicidesOnWomenByYear)
    {
      $homicidesOnWomenByYear['Adultez (30-59)'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentOnWomenByAgeAndYearAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], 60, 150)->each(function($LifeQuery) use (&$homicidesOnWomenByYear)
    {
      $homicidesOnWomenByYear['Adulto Mayor (60++)'][] = $LifeQuery->count;
    });

    foreach ($homicidesOnWomenByYear as $key => $age)
    {
      $data['data'][] = array(
        'label' => $key,
        'value' => array_sum($age),
      );

      $excel['details']['age'][] = $key;
      $excel['details']['homicides'][] = array_sum($age);
    }

    $data['labels']['categoryAxis'] = 'Rangos de edad' ;
    $data['labels']['valueAxis'] = 'Incidencia de muertes violentas de mujeres' ;
    $data['year'] = $years;
    $data['labels']['categoryAxisRotation'] = 270;
    $data['municipality'] = $this->homicidesRatesMunicipalityFilter();
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesIncidentByTimeAggression(Array $input)
  {

    $years = $this->homicidesRatesYearFilter();
    $data = $excel = array();
    $timeAggression = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'time' => 'Rango de horario',
      'homicides' => 'Número de homicidios'
    );

    $this->LifeQuery->homicidesIncidentByTimeAgressionsAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], '00:00:00', '03:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['00:00 - 03:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByTimeAgressionsAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], '04:00:00', '07:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['04:00 - 07:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByTimeAgressionsAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], '08:00:00', '11:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['08:00 - 11:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByTimeAgressionsAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], '12:00:00', '15:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['12:00 - 15:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByTimeAgressionsAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], '16:00:00', '19:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['16:00 - 19:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByTimeAgressionsAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'], '20:00:00', '23:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['20:00 - 23:59'][] = $LifeQuery->count;
    });

    foreach ($timeAggression as $key => $time)
    {
      $data['data'][] = array(
        'label' => $key,
        'value' => array_sum($time),
      );

      $excel['details']['time'][] = $key;
      $excel['details']['homicides'][] = array_sum($time);
    }

    $data['labels']['categoryAxis'] = 'Rango de horario';
    $data['labels']['valueAxis'] = 'Número de homicidios';
    $data['year'] = $years;
    $data['labels']['categoryAxisRotation'] = 270;
    $data['gender'] = $this->homicidesRatesGenderFilter();
    $data['municipality'] = $this->homicidesRatesMunicipalityFilter();
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesByIllegitimateAggressionByYear(Array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'year' => 'Años',
      'aggressions' => 'Agresiones Ilegítimas'
    );

    $this->LifeQuery->homicidesByIllegitimateAgressionsByYear($input['filter']['department'], $input['filter']['municipality'])->each(function($LifeQuery) use(&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $LifeQuery->year,
        'value' => $LifeQuery->count,
      );

      $excel['details']['year'][] = $LifeQuery->year;
      $excel['details']['aggressions'][] =  $LifeQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Agresiones Ilegítimas';
    $data['department'] = $this->illegitimateAggressionsDepartmentFilter();
    $data['municipality'] = $this->illegitimateAggressionsMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesByIllegitimateAgressionsOnCopsByYear(Array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'year' => 'Años',
      'aggressions' => 'Agresiones Ilegítimas'
    );

    $this->LifeQuery->homicidesByIllegitimateAgressionsOnCopsByYearAndDepartmentAndMunicipality($input['filter']['department'], $input['filter']['municipality'])->each(function($LifeQuery) use(&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $LifeQuery->year,
        'value' => $LifeQuery->count,
      );

      $excel['details']['year'][] = $LifeQuery->year;
      $excel['details']['aggressions'][] =  $LifeQuery->count;
    });

    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Agresiones Ilegítimas';
    $data['department'] = $this->aggressionDeathDepartmentFilter();
    $data['municipality'] = $this->aggresionDeathMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getPoliceInvestigatedByCrimes(Array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'year' => 'Años',
      'investigations' => 'Policías investigados'
    );

    $this->LifeQuery->policeInvestigatedByCrimesOnYearsByDepartmentAndMunicipality($input['filter']['department'], $input['filter']['municipality'])->each(function($LifeQuery) use(&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $LifeQuery->year,
        'value' => $LifeQuery->count,
      );

      $excel['details']['year'][] = $LifeQuery->year;
      $excel['details']['investigations'][] =  $LifeQuery->count;
    });

    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Policías investigados';
    $data['department'] = $this->investigatedOfficersDepartmentFilter();
    $data['municipality'] = $this->investigatedOfficersMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesByIllegitimateAgressionsByWeekDayAndYear(Array $input)
  {
    $years = $this->illegitimateAggressionYearFilter();
    $data = $excel = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'weeks' => 'Días de semana',
      'aggressions' => 'Agresiones Ilegítimas'
    );

    $this->LifeQuery->homicidesByIllegitimateAgressionsByWeekDayAndYear(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'])->each(function($LifeQuery) use (&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $LifeQuery->day,
        'value' => $LifeQuery->count,
      );

      $excel['details']['weeks'][] = $LifeQuery->day;
      $excel['details']['aggressions'][] =  $LifeQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Días de semana';
    $data['labels']['valueAxis'] = 'Agresiones Ilegítimas';
    $data['year'] = $years;
    $data['labels']['categoryAxisRotation'] = 270;
    $data['department'] = $this->illegitimateAggressionsDepartmentFilter();
    $data['municipality'] = $this->illegitimateAggressionsMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesIncidentByIllegitimateAggressionByTime(Array $input)
  {
    $years = $this->illegitimateAggressionYearFilter();
    $data = $excel = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'time' => 'Rango de horario',
      'homicides' => 'Agresiones ilegítimas'
    );

    $timeAggression = array();
    $this->LifeQuery->homicidesIncidentByIllegitimateAggressionByTimeAndYearAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], '00:00:00', '03:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['00:00 - 03:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByIllegitimateAggressionByTimeAndYearAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], '04:00:00', '07:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['04:00 - 07:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByIllegitimateAggressionByTimeAndYearAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], '08:00:00', '11:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['08:00 - 11:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByIllegitimateAggressionByTimeAndYearAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], '12:00:00', '15:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['12:00 - 15:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByIllegitimateAggressionByTimeAndYearAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], '16:00:00', '19:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['16:00 - 19:59'][] = $LifeQuery->count;
    });
    $this->LifeQuery->homicidesIncidentByIllegitimateAggressionByTimeAndYearAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], '20:00:00', '23:59:00')->each(function($LifeQuery) use (&$timeAggression)
    {
      $timeAggression['20:00 - 23:59'][] = $LifeQuery->count;
    });

    foreach ($timeAggression as $key => $time)
    {
      $data['data'][] = array(
        'label' => $key,
        'value' => array_sum($time),
      );

      $excel['details']['time'][] = $key;
      $excel['details']['homicides'][] = array_sum($time);
    }
    $data['labels']['categoryAxis'] = 'Rango de horario';
    $data['labels']['valueAxis'] = 'Agresiones ilegítimas';
    $data['year'] = $years;
    $data['labels']['categoryAxisRotation'] = 270;
    $data['department'] = $this->illegitimateAggressionsDepartmentFilter();
    $data['municipality'] = $this->illegitimateAggressionsMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHomicidesByIllegitimateAgressionsByGang(Array $input)
  {
    $years = $this->illegitimateAggressionYearFilter();
    $data = $excel = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'groups' => 'Grupos delincuenciales',
      'homicides' => 'Agresiones ilegítimas'
    );

    $this->LifeQuery->homicidesByIllegitimateAgressionsByGangAndYearAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'])->each(function($LifeQuery) use(&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $LifeQuery->gang,
        'value' => $LifeQuery->count,
      );

      $excel['details']['groups'][] = $LifeQuery->gang;
      $excel['details']['homicides'][] = $LifeQuery->count;
    });

    $data['labels']['categoryAxis'] = 'Grupos delincuenciales';
    $data['labels']['valueAxis'] = 'Agresiones ilegítimas';
    $data['labels']['categoryAxisRotation'] = 270;
    $data['year'] = $years;
    $data['department'] = $this->illegitimateAggressionsDepartmentFilter();
    $data['municipality'] = $this->illegitimateAggressionsMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

    /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getPoliceInvestigatedByHomicideAndFeminicide(Array $input)
  {
    $data = $temp = $excel = array();

    $excel['header'] = array(
      'year' => 'Años',
      'man' => 'Homicidios',
      'woman' => 'Feminicidios',
    );

    $this->LifeQuery->policeInvestigatedByHomicideAndFeminicide($input['filter']['department'], $input['filter']['municipality'])->each(function($LifeQuery) use(&$temp)
    {
      $temp[$LifeQuery->year]['year'] = $LifeQuery->year;
      $temp[$LifeQuery->year][$LifeQuery->victim_sex]['count'] = $LifeQuery->count;
    });

    foreach ($temp as $key => $victims)
    {
      $data['data'][] = array(
        'year' => $victims['year'],
        'column_a' => $victims['HOMBRE']['count'],
        'column_b' => (array_key_exists('MUJER', $victims)) ? $victims['MUJER']['count'] : '0',
      );

      $excel['details']['year'][] = $victims['year'];
      $excel['details']['man'][] = $victims['HOMBRE']['count'];
      $excel['details']['woman'][] = (array_key_exists('MUJER', $victims)) ? $victims['MUJER']['count'] : '0';
    }

    $data['municipality'] = $this->investigatedOfficersMunicipalityFilter();
    $data['department'] = $this->investigatedOfficersDepartmentFilter();

    $data['labels']['valueAxis'] = 'Delitos';
    $data['labels']['column_a'] = 'Homicidios';
    $data['labels']['column_b'] = 'Feminicidios';

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getHomicidesIncidentByMonth(Array $input)
  {
    $data = $excel = array();

    $years = $this->homicidesRatesYearFilter();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'month' => 'Meses',
      'homicides' => 'Homicidios',
    );


    $this->LifeQuery->homicidesIncidentByMonthAndYearAndGenderAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['gender'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'])->each(function($LifeQuery) use(&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $LifeQuery->month,
        'value' => $LifeQuery->value,
      );
      
      $excel['details']['month'][] =  $LifeQuery->month;
      $excel['details']['homicides'][] = $LifeQuery->value;
    });
    $data['labels']['categoryAxisRotation'] = 270;
    $data['labels']['categoryAxis'] = 'Meses';
    $data['labels']['valueAxis'] = 'Incidencia de homicidios';
    $data['year'] = $years;
    $data['gender'] = $this->homicidesRatesGenderFilter();
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['municipality'] = $this->homicidesRatesMunicipalityFilter();
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }


  public function getHomicidesIncidentOnWomenByMonth(Array $input)
  {
    $data = $excel = array();

    $years = $this->homicidesRatesYearFilter();

    $excel['header'] = array(
      'month' => 'Meses',
      'homicides' => 'Feminicidios',
    );

    $this->LifeQuery->homicidesIncidentOnWomenByMonthAndYearAndDepartmentAndMunicipalityAndWeaponType(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'], $input['filter']['weapon_type'])->each(function($LifeQuery) use(&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $LifeQuery->month,
        'value' => $LifeQuery->value,
      );

      $excel['details']['month'][] = $LifeQuery->month;
      $excel['details']['homicides'][] = $LifeQuery->value;
    });

    $data['labels']['categoryAxisRotation'] = 270;
    $data['labels']['categoryAxis'] = 'Meses';
    $data['labels']['valueAxis'] = 'Incidencia de feminicidios';
    $data['year'] = $years;
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['municipality'] = $this->homicidesRatesMunicipalityFilter();
    $data['weapon_type'] = $this->homicidesRatesWeaponTypeFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getProportionalRatioOnIllegitimateAggressions(Array $input)
  {


    $data = $people = $cops = $excel = array();

    $people = $this->LifeQuery->homicidesByIllegitimateAggressionsOnNaturalPeopleByDepartmentAndMunicipality($input['filter']['department'],
    $input['filter']['municipality'])->toArray();
    $cops = $this->LifeQuery->homicidesByIllegitimateAggressionsOnCopsByDepartmentAndMunicipality($input['filter']['department'],
    $input['filter']['municipality'])->toArray();

    $excel['header'] = array(
      'year' => 'Años',
      'homicides' => 'Relación proporcional',
    );

    foreach ($people as $key => $deceased)
    {
      $data['data'][] = array(
        'label' => $deceased->year,
        'value' => round($deceased->sum / $cops[$key]->sum, 2),
      );

      $excel['details']['year'][] = $deceased->year;
      $excel['details']['homicides'][] = round($deceased->sum / $cops[$key]->sum, 2);
    }
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Relación proporcional';
    $data['department'] = $this->homicidesRatesDepartmentFilter();
    $data['municipality'] = $this->homicidesRatesMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function homicidesByIllegitimateAggressionsByDeceased(Array $input)
  {
    $years = $this->illegitimateAggressionYearFilter();
    $data = $excel = array();

    $minYear = min($years);
    $maxYear = max($years);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $input['filter']['yearFrom'] = array($minYear);
      $input['filter']['yearTo'] = array($maxYear);
    }

    $excel['header'] = array(
      'homicides' => 'Personas fallecidas',
      'events' => 'Número de eventos',
    );

    $this->LifeQuery->homicidesByIllegitimateAggressionsByDeceasedAndYearAndDepartmentAndMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'],
    $input['filter']['municipality'])->each(function($LifeQuery) use(&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $LifeQuery->deceased,
        'value' => $LifeQuery->eventos,
        'valueAxis' => 'Número de eventos'
      );

      $excel['details']['homicides'][] = $LifeQuery->deceased;
      $excel['details']['events'][] = $LifeQuery->eventos;
    });

  $data['labels']['categoryAxis'] = 'Personas fallecidas';
  $data['labels']['valueAxis'] = 'Número de eventos';
  $data['year'] = $years;
  $data['department'] = $this->illegitimateAggressionsDepartmentFilter();
  $data['municipality'] = $this->illegitimateAggressionsMunicipalityFilter();

  return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getOfficersInvestigatedByInvestigationType(Array $input)
  {

    $years = $this->investigatedOfficersYearFilter();

    $data = $excel = array();

    $excel['header'] = array(
      'crime' => 'Principales delitos',
      'homicides' => 'Número de casos',
    );

    $this->LifeQuery->investigatedOfficersByYearByDepartmentAndByMunicipality(null, $input['filter']['yearFrom'], $input['filter']['yearTo'], $input['filter']['department'], $input['filter']['municipality'])->each(function ($LifeQuery) use(&$data, &$excel)
    {
      $reducedStr = $this->substrwords($LifeQuery->crime, 30, '.');
      
      $data['data'][] = array(
        'label'=> $reducedStr,
        'value' => $LifeQuery->count
      );

      $excel['details']['crime'][] = $LifeQuery->crime;
      $excel['details']['homicides'][] = $LifeQuery->count;
    });

    if (empty($data['data'])) 
    {
      $data['data'] = array();
    }

    $data['labels']['categoryAxis'] = 'Principales delitos';
    $data['labels']['valueAxis'] = 'Número de casos';
    $data['labels']['categoryAxisRotation'] = 270;
    $data['year'] = $years;
    $data['department'] = $this->investigatedOfficersDepartmentFilter();
    $data['municipality'] = $this->investigatedOfficersMunicipalityFilter();

    return json_encode(array('data' => $data, 'excel' => $excel));
  }
}
