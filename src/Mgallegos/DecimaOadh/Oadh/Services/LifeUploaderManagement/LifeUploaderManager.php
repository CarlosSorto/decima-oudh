<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\LifeUploaderManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;

use Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface;

use App\Kwaai\Security\Repositories\Journal\JournalInterface;

use Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface;

use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeHomicidesRatesGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeHomicidesRatesTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeOfficersInvestigatedGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeOfficersInvestigatedTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeAggressionsWeaponGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeAggressionsWeaponTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeAggressionsDeathGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentLifeAggressionsDeathTempGridRepository;

// use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentPrefixTableNameGridRepository;

// use Mgallegos\DecimaOadh\Oadh\Repositories\Life\EloquentPrefixTableNameTempGridRepository;

use Carbon\Carbon;

use Maatwebsite\Excel\Excel;

use Illuminate\Config\Repository;

use Illuminate\Translation\Translator;

use Illuminate\Database\DatabaseManager;

class LifeUploaderManager extends AbstractLaravelValidator implements LifeUploaderManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * File Management Interface
  *
  * @var Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface
  *
  */
  protected $FileManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $UploaderManager;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;


  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
  * Maatwebsite\Excel\Excel
  *
  * @var Excel
  *
  */
  protected $Excel;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    FileManagementInterface $FileManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    UploaderManagementInterface $UploaderManager,

    EloquentLifeHomicidesRatesGridRepository $EloquentLifeHomicidesRatesGridRepository,
    EloquentLifeHomicidesRatesTempGridRepository $EloquentLifeHomicidesRatesTempGridRepository,

    EloquentLifeOfficersInvestigatedGridRepository $EloquentLifeOfficersInvestigatedGridRepository,
    EloquentLifeOfficersInvestigatedTempGridRepository $EloquentLifeOfficersInvestigatedTempGridRepository,

    EloquentLifeAggressionsWeaponGridRepository $EloquentLifeAggressionsWeaponGridRepository,
    EloquentLifeAggressionsWeaponTempGridRepository $EloquentLifeAggressionsWeaponTempGridRepository,

    EloquentLifeAggressionsDeathGridRepository $EloquentLifeAggressionsDeathGridRepository,
    EloquentLifeAggressionsDeathTempGridRepository $EloquentLifeAggressionsDeathTempGridRepository,

    //EloquentPrefixTableNameGridRepository $EloquentPrefixTableNameGridRepository,
    //EloquentPrefixTableNameTempGridRepository $EloquentPrefixTableNameTempGridRepository,

    Carbon $Carbon,
    Excel $Excel,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->FileManager = $FileManager;

    $this->Journal = $Journal;

    $this->GridEncoder = $GridEncoder;

    $this->UploaderManager = $UploaderManager;

    $this->EloquentLifeHomicidesRatesGridRepository = $EloquentLifeHomicidesRatesGridRepository;
    $this->EloquentLifeHomicidesRatesTempGridRepository = $EloquentLifeHomicidesRatesTempGridRepository;

    $this->EloquentLifeOfficersInvestigatedGridRepository = $EloquentLifeOfficersInvestigatedGridRepository;
    $this->EloquentLifeOfficersInvestigatedTempGridRepository = $EloquentLifeOfficersInvestigatedTempGridRepository;

    $this->EloquentLifeAggressionsWeaponGridRepository = $EloquentLifeAggressionsWeaponGridRepository;
    $this->EloquentLifeAggressionsWeaponTempGridRepository = $EloquentLifeAggressionsWeaponTempGridRepository;

    $this->EloquentLifeAggressionsDeathGridRepository = $EloquentLifeAggressionsDeathGridRepository;
    $this->EloquentLifeAggressionsDeathTempGridRepository = $EloquentLifeAggressionsDeathTempGridRepository;

    //$this->EloquentCrimesVictimsGridRepository = $EloquentCrimesVictimsGridRepository;
    //$this->EloquentCrimesVictimsTempGridRepository = $EloquentCrimesVictimsTempGridRepository;

    $this->Carbon = $Carbon;

    $this->Excel = $Excel;

    $this->DB = $DB;

		$this->Lang = $Lang;

		$this->Config = $Config;
	}

  //homicidesRates
  public function getHomicidesRatesGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentLifeHomicidesRatesGridRepository, $post);
  }

  public function getHomicidesRatesTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentLifeHomicidesRatesTempGridRepository, $post);
  }

	public function homicidesRatesProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format', 'time_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['time_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );


    $homicidesRates = 'OADH_LIFE_Homicides_Rates';

    return $this->UploaderManager->processSpreadsheet(
      $params,
      $input,
      array('record', 'year', 'month', 'day', 'time_aggression', 'age', 'sex', 'occupation', 'municipality', 'department', 'weapon_type'),
      $homicidesRates,
      array('time_aggression'),
      array('record', 'year', 'day', 'age'),
      array('time_aggression')
    );
  }

  public function homicidesRatesCopyToProduction($input)
  {
    $response = array();
    $columns = ['record', 'year', 'month', 'day', 'time_aggression', 'age', 'sex', 'occupation', 'municipality', 'department', 'weapon_type', 'file_id', 'organization_id'];
    $homicidesRates = 'OADH_LIFE_Homicides_Rates';

    return $this->UploaderManager->copyToProduction($input['file_id'], $homicidesRates, $columns);
  }

  public function homicidesRatesDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_LIFE_Homicides_Rates');
  }

  public function homicidesRatesDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_LIFE_Homicides_Rates');
  }

  //officersInvestigated
  public function getOfficersInvestigatedGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentLifeOfficersInvestigatedGridRepository, $post);
  }

  public function getOfficersInvestigatedTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentLifeOfficersInvestigatedTempGridRepository, $post);
  }

	public function officersInvestigatedProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $tableName = 'OADH_LIFE_Officers_Investigated';

    return $this->UploaderManager->processSpreadsheet(
      $params,
      $input,
      array('record','investigation_type','year', 'crime','category','position','officer_sex','location_place','location_department','location_municipality','date','act_place','act_department','act_municipality','weapon_type','victim_quantity','victim_sex','criminal_justice_process'),
      $tableName,
      array('record','category','position','officer_sex','location_place','location_department','location_municipality','date','act_place','act_department','act_municipality','weapon_type','victim_quantity','victim_sex','criminal_justice_process'),
      array('year','victim_quantity')
    );
  }

  public function officersInvestigatedCopyToProduction($input)
  {
    $response = array();
    $columns = ['record', 'investigation_type', 'year', 'crime','category','position','officer_sex','location_place','location_department','location_municipality','date','act_place','act_department','act_municipality','weapon_type','victim_quantity','victim_sex','criminal_justice_process', 'file_id', 'organization_id'];
    $tableName = 'OADH_LIFE_Officers_Investigated';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function officersInvestigatedDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_LIFE_Officers_Investigated');
  }

  public function officersInvestigatedDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_LIFE_Officers_Investigated');
  }

  //Aggression Weapon
  public function getAggressionsWeaponGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentLifeAggressionsWeaponGridRepository, $post);
  }

  public function getAggressionsWeaponTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentLifeAggressionsWeaponTempGridRepository, $post);
  }

	public function aggressionsWeaponProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format', 'time_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['time_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $tableName = 'OADH_LIFE_Aggression_Weapon';

    return $this->UploaderManager->processSpreadsheet(
        $params,
        $input,
        array('department', 'municipality', 'day', 'day_number', 'month', 'year', 'date', 'time', 'gang', 'division', 'aggressions', 'deceased'),
        $tableName,
        array('division', 'time'),
        array('day_number', 'month', 'year', 'aggressions', 'deceased'),
        array('time')
      );
  }

  public function aggressionsWeaponCopyToProduction($input)
  {
    $response = array();
    $columns = ['department', 'municipality', 'day', 'day_number', 'month', 'year', 'date', 'time', 'gang', 'division', 'aggressions', 'deceased', 'file_id', 'organization_id'];
    $tableName = 'OADH_LIFE_Aggression_Weapon';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function aggressionsWeaponDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_LIFE_Aggression_Weapon');
  }

  public function aggressionsWeaponDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_LIFE_Aggression_Weapon');
  }

  //aggressions death
  public function getAggressionsDeathGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentLifeAggressionsDeathGridRepository, $post);
  }

  public function getAggressionsDeathTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentLifeAggressionsDeathTempGridRepository, $post);
  }

	public function aggressionsDeathProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format', 'time_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['time_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $tableName = 'OADH_LIFE_Aggression_Death';

    return $this->UploaderManager->processSpreadsheet(
      $params,
      $input,
      array('department', 'municipality', 'day', 'day_number', 'month', 'year', 'date', 'time', 'gang', 'division', 'deceased_policemen', 'aggressions'),
      $tableName,
      array('division', 'time'),
      array('day_number', 'month', 'year', 'year', 'deceased_policemen', 'aggressions'),
      array('time')
    );
  }

  public function aggressionsDeathCopyToProduction($input)
  {
    $response = array();
    $columns = ['department', 'municipality', 'day', 'day_number', 'month', 'year', 'date', 'time', 'gang', 'division', 'deceased_policemen', 'aggressions', 'file_id', 'organization_id'];
    $tableName = 'OADH_LIFE_Aggression_Death';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function aggressionsDeathDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_LIFE_Aggression_Death');
  }

  public function aggressionsDeathDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_LIFE_Aggression_Death');
  }

/*
  //tableName
  public function getTableNameGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameGridRepository, $post);
  }

  public function getTableNameTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameTempGridRepository, $post);
  }

	public function tableNameProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['column_1', 'column_2', 'column_3', 'column_4'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function tableNameCopyToProduction($input)
  {
    $response = array();
    $columns = ['column_1', 'column_2', 'column_3', 'column_4', 'file_id', 'organization_id'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function tableNameDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_Prefix_Table_Name');
  }

  public function tableNameDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_Prefix_Table_Name');
  }
*/
}
