<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\ActivityManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Activity\EloquentActivityGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\ActivityImage\EloquentActivityImageGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Activity\ActivityInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\ActivityImage\ActivityImageInterface;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Translation\Translator;
use Illuminate\Config\Repository;
use Illuminate\Cache\CacheManager;

class ActivityManager extends AbstractLaravelValidator implements ActivityManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent Activity Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Activity\EloquentActivityGridRepository
	 *
	 */
	protected $EloquentActivityGridRepository;

  /**
	 * Eloquent Activity Detail Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ActivityImage\EloquentActivityImageGridRepository
	 *
	 */
	protected $EloquentActivityImageGridRepository;

  /**
	 *  Activity Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Activity\ActivityInterface
	 *
	 */
	protected $Activity;

  /**
	 *  Activity Detail Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ActivityImage\ActivityImageInterface
	 *
	 */
	protected $ActivityImage;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    EloquentActivityGridRepository $EloquentActivityGridRepository,
    EloquentActivityImageGridRepository $EloquentActivityImageGridRepository,
    ActivityInterface $Activity,
    ActivityImageInterface $ActivityImage,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->EloquentActivityGridRepository = $EloquentActivityGridRepository;
    $this->EloquentActivityImageGridRepository = $EloquentActivityImageGridRepository;
    $this->Activity = $Activity;
    $this->ActivityImage = $ActivityImage;
    $this->Carbon = $Carbon;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
    $this->Cache = $Cache;
	}

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataMaster(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentActivityGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataDetail(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentActivityImageGridRepository, $post);
  }

  /**
   * Get search modal table rows
   *
   * @return array
   */
  public function getSearchModalTableRows($id = null, $organizationId = null, $databaseConnectionName = null, $returnJson = true)
  {
    $rows = array();

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    $this->Activity->searchModalTableRows($id, $organizationId, $databaseConnectionName)->each(function($Activity) use (&$rows)
    {
      $rows[$Activity->id] = (array)$Activity;
    });

    if($returnJson)
    {
      return json_encode($rows);
    }

    return $rows;
  }

  /**
   * Get ...
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getActivity($id, $databaseConnectionName = null)
  {
    $Activity = $this->Activity->byId($id, $databaseConnectionName);

    if(empty($Activity))
    {
      return false;
    }

    return $Activity;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getActivitys()
  {
    $Activitys = array();

    $this->Activity->byOrganization($this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($Activity) use (&$Activitys)
    {
      array_push($Activitys, array('label'=> $Activity->name , 'value'=>$Activity->id));
    });

    return $Activitys;
  }

  public function getActivitiesWithGalleries($lang = null)
  {
    $data = array();

    // $this->Activity->byOrganization(1)->each(function($Activity) use (&$data)
    $this->Activity->byLang($lang)->each(function($Activity) use (&$data)
    {
      $data['activities'][$Activity->id] = array(
        'id' => $Activity->id,
        'title' => $Activity->title,
        'description' => $Activity->description,
        'image_url' => $Activity->image_url
      );

    });

    $this->ActivityImage->byOrganization(1)->each(function($Gallery) use (&$data)
    {
      $data['galleries'][$Gallery->activity_id][$Gallery->id] = array(
        'description' =>  $Gallery->description,
        'image_url' => $Gallery->image_url
      );
    });

    return $data;

  }

  /**
	 * Create a new ...
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function createMaster(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    unset(
      $input['_token'],
      $input['lang_label'],
      $input['tags_tokenfield']
    );

    $input = eloquent_array_filter_for_insert($input);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');

		$input = array_add($input, 'organization_id', $organizationId);

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
		{
      $Activity = $this->Activity->create($input, $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $Activity->id, 'journalized_type' => $this->Activity->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::activity-management.addedMasterJournal', array('name' => $Activity->title)), $Journal));

      $this->Cache->forget('ActivitiesSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessSaveMessage'),
        'id' => $Activity->id,
        'smtRows' => $this->getSearchModalTableRows(null,$organizationId, $databaseConnectionName, false)
      )
    );

    // return json_encode(
    //   array(
    //     'success' => $this->Lang->get('form.defaultSuccessSaveMessage'),
    //     'id' => $Activity->id,
    //     'number' => $Activity->number,
    //     'smtRows' => $this->getSearchModalTableRows($organizationId, $databaseConnectionName, false)
    //   )
    // );
  }

  /**
	 * Create a new ...
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function createDetail(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    unset(
      $input['_token']
    );

    $input = eloquent_array_filter_for_insert($input);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

		$input = array_add($input, 'organization_id', $organizationId);
    // $input = array_add($input, 'created_by', $loggedUserId);

    // if(!empty($input['date']))
    // {
    //   $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    // }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
		{
      $ActivityImage = $this->ActivityImage->create($input, $databaseConnectionName);
      $Activity = $this->Activity->byId($ActivityImage->activity_id, $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $ActivityImage->activity_id, 'journalized_type' => $this->Activity->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::activity-management.addedDetailJournal'), $Journal));

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessUpdateMessage')
      )
    );
  }

  /**
   * Update an existing ...
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function updateMaster(array $input, $Activity = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $newValues['lang'] = $input['lang_label'];

    unset(
      $input['_token'],
      $input['lang_label'],
      $input['tags_tokenfield']
    );

    $input = eloquent_array_filter_for_update($input);

    if(!empty($input['date']))
    {
      $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      if(empty($Activity))
      {
        $Activity = $this->Activity->byId($input['id'], $databaseConnectionName);
      }

      $unchangedValues = $Activity->toArray();

      $this->Activity->update($input, $Activity);

      $diff = 0;

      foreach ($input as $key => $value)
      {
        if($unchangedValues[$key] != $value)
        {
          $diff++;

          if($diff == 1)
          {
            $Journal = $this->Journal->create(array('journalized_id' => $Activity->id, 'journalized_type' => $this->Activity->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
          }

          if($key == 'status')//Para autocomple de estados
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
          }
          else if ($key == 'field1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::activity-management.field1'), 'field_lang_key' => 'decima-oadh::activity-management.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'name')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'chekbox0' || $key == 'chekbox1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::activity-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::activity-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
          }
          else if($key == 'date')
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $oldValue = '';
            }

            if(!empty($value))
            {
              $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $newValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
          }
          else if($key == 'table_name_id')//field required
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::activity-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::activity-management.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
          }
          else if($key == 'table_name_id')//field not required
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
            }
            else
            {
              $oldValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::activity-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::activity-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
          }
          else if($key == 'lang')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::publication-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::publication-management.' . camel_case($key), 'old_value' => $this->Lang->get('decima-oadh::publication-management.' . $unchangedValues[$key]), 'new_value' => $newValues[$key]), $Journal);
          }
          else
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::publication-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::publication-management.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
        }
      }

      $this->Cache->forget('ActivitiesSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessUpdateMessage'),
        'smtRows' => $this->getSearchModalTableRows($organizationId, $databaseConnectionName, false)
      )
    );
  }

  /**
   * Update an existing ...
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function updateDetail(array $input, $ActivityImage = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    // if(!empty($input['table_name_label']))
    // {
    //   $newValues['table_name_id'] = $input['table_name_label'];
    // }

    unset(
      $input['_token']
    );

    $input = eloquent_array_filter_for_update($input);

    // if(!empty($input['date']))
    // {
    //   $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    // }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      if(empty($ActivityImage))
      {
        $ActivityImage = $this->ActivityImage->byId($input['id'], $databaseConnectionName);
      }

      $unchangedValues = $ActivityImage->toArray();

      $this->ActivityImage->update($input, $ActivityImage);

      $diff = 0;

      foreach ($input as $key => $value)
      {
        if($unchangedValues[$key] != $value)
        {
          $diff++;

          if($diff == 1)
          {
            $Journal = $this->Journal->create(array('journalized_id' => $ActivityImage->activity_id, 'journalized_type' => $this->Activity->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
          }

          if($key == 'status')//Para autocomple de estados
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
          }

          else if ($key == 'name')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'chekbox0' || $key == 'chekbox1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::activity-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::activity-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
          }
          else if($key == 'image_url')//field required
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::publication-management.imageUrl1140'), 'field_lang_key' => 'decima-oadh::publication-management.imageUrl1140', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if($key == 'description')//field required
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::publication-management.description'), 'field_lang_key' => 'decima-oadh::publication-management.description', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::activity-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::activity-management.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
        }
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
  }

  /**
   * Delete an existing ... (soft delete)
   *
   * @param array $input
	 * 	An array as follows: array(id => $id);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */
  public function deleteMaster(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      $Activity = $this->Activity->byId($input['id'], $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $input['id'], 'journalized_type' => $this->Activity->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::activity-management.deletedMasterJournal', array('name' => $Activity->name)), $Journal));

      $this->ActivityImage->massDelete($input['id'], $databaseConnectionName);
      $this->Activity->delete(array($input['id']), $databaseConnectionName);

      $this->Cache->forget('ActivitiesSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessDeleteMessage'),
        'smtRows' => $this->getSearchModalTableRows($organizationId, $databaseConnectionName, false)
      )
    );
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $input
	 * 	An array as follows: array($id0, $id1,…);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */
   public function deleteDetails(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
   {
     $count = 0;

     if(empty($organizationId))
     {
       $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
     }

     if(empty($loggedUserId))
     {
       $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
     }

     $this->beginTransaction($openTransaction, $databaseConnectionName);

     try
     {
       foreach ($input['id'] as $key => $id)
       {
         $count++;

         $ActivityImage = $this->ActivityImage->byId($id, $databaseConnectionName);

         if(empty($Activity))
         {
           $Activity = $this->Activity->byId($ActivityImage->activity_id, $databaseConnectionName);
         }

         $Journal = $this->Journal->create(array('journalized_id' => $ActivityImage->activity_id, 'journalized_type' => $this->Activity->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
         $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::activity-management.deletedDetailJournal', array('detailName' => $ActivityImage->image_url, 'masterName' => $Activity->title))), $Journal);

         $this->ActivityImage->delete(array($id), $databaseConnectionName);
       }

       $this->commit($openTransaction);
     }
     catch (\Exception $e)
     {
       $this->rollBack($openTransaction);

       throw $e;
     }
     catch (\Throwable $e)
     {
       $this->rollBack($openTransaction);

       throw $e;
     }

     if($count == 1)
     {
       return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
     }
     else
     {
       return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
     }
   }
}
