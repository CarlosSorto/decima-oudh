<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\RightTo\RightToInterface;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;
use Illuminate\Config\Repository;
use Illuminate\Translation\Translator;
use Illuminate\Database\DatabaseManager;

class UploaderManager extends AbstractLaravelValidator implements UploaderManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * File Management Interface
  *
  * @var Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface
  *
  */
  protected $FileManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $RightTo;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
  * Maatwebsite\Excel\Excel
  *
  * @var Excel
  *
  */
  protected $Excel;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    FileManagementInterface $FileManager,
    JournalInterface $Journal,
    RightToInterface $RightTo,
    Carbon $Carbon,
    Excel $Excel,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->FileManager = $FileManager;

    $this->Journal = $Journal;

    $this->RightTo = $RightTo;

    $this->Carbon = $Carbon;

    $this->Excel = $Excel;

    $this->DB = $DB;

		$this->Lang = $Lang;

    $this->Config = $Config;

    $this->departments = array(
      'ahuachapán',
      'ahuachapan',
      'santa ana',
      'sonsonate',
      'chalatenango',
      'la libertad',
      'san salvador',
      'cabañas',
      'la paz',
      'morazán',
      'morazan',
      'cuscatlán',
      'cuscatlan',
      'san vicente',
      'usulután',
      'usulutan',
      'san miguel',
      'la unión',
      'la unión',
      'ciudad de guatemala',
      'honduras',
      'n/s',
      'no determinado',
      'no disponible'
    );

    $this->municipalities = array(
      'ahuachapán',
      'ahuachapan',
      'concepción de ataco',
      'concepcion de ataco',
      'turín',
      'turin',
      'atiquizaya',
      'san lorenzo',
      'el refugio',
      'san pedro puxtla',
      'tacuba',
      'apaneca',
      'guaymango',
      'jujutla',
      'san francisco menéndez',
      'san francisco menendez',
      'sensuntepeque',
      'tejutepeque',
      'ilobasco',
      'dolores',
      'san isidro',
      'jutiapa',
      'victoria',
      'guacotecti',
      'cinquera',
      'santa rita',
      'san rafael',
      'chalatenango',
      'citalá',
      'citala',
      'la laguna',
      'las vueltas',
      'san miguel de mercedes',
      'la palma',
      'san luís del carmen',
      'san luis del carmen',
      'nueva trinidad',
      'concepción quezaltepeque',
      'concepcion quezaltepeque',
      'tejutla',
      'cancasque',
      'san isidro labrador',
      'agua caliente',
      'potonico',
      'la reina',
      'ojos de agua',
      'san francisco lempa',
      'san antonio de la cruz',
      'nueva concepción',
      'san ignacio',
      'el carrizal',
      'azacualpa',
      'comalapa',
      'las flores',
      'san josé las flores',
      'san jose las flores',
      'san francisco morazán',
      'san francisco morazan',
      'san fernando',
      'el paraiso',
      'el paraíso',
      'san antonio los ranchos',
      'nombre de jesús',
      'nombre de jesus',
      'arcatao',
      'dulce nombre de maria',
      'dulce nombre de maría',
      'tenancingo',
      // 'el rosario cuscatlán',
      // 'el rosario cuscatlan',
      'san josé guayabal',
      'san jose guayabal',
      'san rafael cedros',
      'san bartolomé perulapía',
      'san bartolome perulapia',
      'san pedro perulapán',
      'san pedro perulapan',
      'santa cruz analquito',
      // 'el carmen cuscatlán',
      // 'el carmen cuscatlan',
      'cojutepeque',
      'candelaría',
      'candelaria',
      'oratorio de concepción',
      'oratorio de concepcion',
      'san ramon',
      'san ramón',
      'monte san juán',
      'monte san juan',
      'santa cruz michapa',
      'suchitoto',
      'san cristóbal',
      'san cristobal',
      'tepecoyo',
      'antiguo cuscatlán',
      'antiguo cuscatlan',
      'talnique',
      'ciudad arce',
      'san matías',
      'san matias',
      'sacacoyo',
      'nuevo cuscatlán',
      'nuevo cuscatlan',
      'santa tecla',
      'huizucar',
      'huizúcar',
      'zaragoza',
      'chiltiupán',
      'chiltiupan',
      'quezaltepeque',
      'colón',
      'colon',
      'san juan opico',
      'jayaque',
      'comasagua',
      'la libertad',
      'puerto de la libertad',
      'jicalápa',
      'jicalapa',
      'san pablo tacachico',
      'tamanique',
      'san josé villanueva',
      'san jose villanueva',
      'teotepeque',
      'jerusalen',
      'jerusalén',
      'mercedes la ceiba',
      'san emigdio',
      'san juan nonualco',
      'san antonio masahuat',
      'san miguel tepezontes',
      'san pedro nonualco',
      'zacatecoluca',
      'san juan tepezontes',
      'san rafael obrajuelo',
      // 'el rosario de la paz',
      'paraíso de osorio',
      'paraiso de osorio',
      'san luís talpa',
      'san luis talpa',
      'san francisco chinameca',
      'cuyultitán',
      'cuyultitan',
      'tapalhuaca',
      'santiago nonualco',
      'olocuilta',
      'santa maria ostuma',
      'santa maría ostuma',
      'santa maría',
      'santa maria',
      'san pedro masahuat',
      'san juan talpa',
      'san luís la herradura',
      'san luis la herradura',
      'lislique',
      'la unión',
      'la union',
      'intipucá',
      'intipuca',
      'san jose',
      'san josé',
      'san jose la fuente',
      'san josé la fuente',
      'meanguera del golfo',
      'santa rosa de lima',
      'pasaquina',
      'nueva esparta',
      'conchagua',
      'el carmen',
      'concepción de oriente',
      'concepcion de oriente',
      'anamorós',
      'anamoros',
      'yayantique',
      'polorós',
      'poloros',
      'bolívar',
      'bolivar',
      'yucuaiquín',
      'yucuaiquin',
      'san alejo',
      'el sauce',
      'chilanga',
      'cacaopera',
      'torola',
      'san isidro',
      'gualococti',
      'corinto',
      'guatajiagua',
      'perquín',
      'perquin',
      'jocoro',
      'lolotiquillo',
      'sociedad',
      'el rosario',
      'jocoaitique',
      'meanguera',
      'yamabal',
      'joateca',
      'san francisco gotera',
      'sensembra',
      'arambala',
      'el divisadero',
      'san fernando',
      'delicias de concepción',
      'delicias de concepcion',
      'osicala',
      'yoloaiquín',
      'yoloaiquin',
      'san simón',
      'san simon',
      'san carlos',
      'san rafael oriente',
      'quelepa',
      'chinameca',
      'chirilagua',
      'lolotique',
      'chapeltique',
      'moncagua',
      'nueva guadalupe',
      'san luís de la reina',
      'san luis de la reina',
      'ciudad barrios',
      'nuevo edén de san juan',
      'nuevo eden de san juan',
      'san antonio',
      'carolina',
      'el tránsito',
      'el transito',
      'san gerardo',
      'comacarán',
      'comacaran',
      'sesori',
      'uluazapa',
      'san miguel',
      'san jorge',
      'santiago texacuangos',
      'el paísnal',
      'el paisnal',
      'san martín',
      'san martin',
      // 'delgado',
      'ciudad delgado',
      'aguilares',
      'san marcos',
      'rosario de mora',
      'nejapa',
      'ayutuxtepeque',
      'tonacatepeque',
      'panchimalco',
      'santo tomas',
      'santo tomás',
      'soyapango',
      'san salvador',
      'apopa',
      'cuscatancingo',
      'guazapa',
      'ilopango',
      'mejicanos',
      'san ildefonso',
      'verapaz',
      'santa clara',
      'santo domingo',
      'san esteban catarina',
      'tecoluca',
      'san lorenzo',
      'san sebastián',
      'san sebastian',
      'tepetitán',
      'tepetitan',
      'nuevo tepetitán',
      'nuevo tepetitan',
      'san vicente',
      'apastepeque',
      'guadalupe',
      'san cayetano istepeque',
      'santa rosa guachipilín',
      'santa rosa guachipilin',
      'santiago de la frontera',
      'coatepeque',
      'candelaria de la frontera',
      'el congo',
      'el porvenir',
      'san sebastián salitrillo',
      'san antonio pajonal',
      'masahuat',
      'chalchuapa',
      'texistepeque',
      'metapán',
      'metapan',
      'santa ana',
      'armenia',
      'nahulingo',
      'nahuilingo',
      'san julián',
      'san julian',
      'cuisnahuat',
      'santa isabel ishuatán',
      'santa isabel ishuatan',
      'sonsonate',
      'santo domingo de guzmán',
      'santo domingo de guzman',
      'juayúa',
      'juayua',
      'santa catarina masahuat',
      'san antonio del monte',
      'san antonio del mosco',
      'nahuizalco',
      'izalco',
      'caluco',
      'acajutla',
      'sonzacate',
      'salcoatitán',
      'salcoatitan',
      'san buenaventura',
      'estanzuelas',
      'jucuarán',
      'jucuaráa',
      'santa elena',
      'concepción batres',
      'concepcion batres',
      'berlín',
      'berlin',
      'san francisco javier',
      'jiquilisco',
      'puerto el triunfo',
      'san dionisio',
      'california',
      'el triunfo',
      'jucuapa',
      'mercedes umaña',
      'ozatlán',
      'ozatlan',
      'santiago de maria',
      'santiago de maría',
      'santa maria',
      'tecapán',
      'tecapan',
      'ereguayquin',
      'ereguayquín',
      'usulután',
      'usulutan',
      'alegría',
      'alegria',
      'san agustín',
      'san agustin',
      'nueva granada',
      'no determinado',
      'no disponible'
    );

    $this->days = array(
      'lunes',
      'martes',
      'miércoles',
      'miercoles',
      'jueves',
      'viernes',
      'sábado',
      'sabado',
      'domingo',
      'no disponible'
    );
	}

  public function startsWith ($string, $startString)
  {
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
  }

  /**
	 * Read and process a spreadsheet
	 *
	 * @param array $params
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1);
	 * @param array $columns
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1);
   *
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function processSpreadsheet(array $params, array $input, array $columns, $tableName, $notRequiredColumns = array(), $integerColumnns = array(), $timeColumns = array(), $dateColumnns = array(), $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    ini_set('memory_limit', '2048M');
    ini_set('max_execution_time', 300);

    $tempFile = false;
    $insertSize = 2;
    $uploadSize = 40000;

    $filePath = $this->FileManager->getLocalFilePath($params['name'], $params['system_route'], $tempFile,  true);
    $idFile = $input['id'];
    $params['header_rows_number'] = (int)remove_thousands_separator($params['header_rows_number']);
    $params['last_row_number'] = (int)remove_thousands_separator($params['last_row_number']);

    unset($input['id']);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $tempIntegerColumnns = $integerColumnns;
    $requiredColumns = $data = $pivot = $carbonData = $integerColumnns = array();

    foreach ($columns as $num => $column)
    {
      if(!in_array($column, $notRequiredColumns))
      {
        $requiredColumns[$column] = $this->Lang->get('decima-oadh::back-end-column.' . camel_case($column));
      }
    }

    foreach ($tempIntegerColumnns as $num => $column)
    {
      $integerColumnns[$column] = $this->Lang->get('decima-oadh::back-end-column.' . camel_case($column));
    }

    foreach ($input as $column => $letter)
    {
      if(!empty($letter))
      {
        $pivot[$column]['position'] = letterToNumber($letter);
        $pivot[$column]['name'] = $column;
      }
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    $this->RightTo->massDelete($tableName . '_Temp', $params['id']);

    try
    {
      if($params['last_row_number'] > $uploadSize)
      {
        return  json_encode(array('info' => $this->Lang->get('decima-oadh::back-end-general.numberOfLinesExceeded', array('maxRows' => $uploadSize))));
      }

      $this->Excel->selectSheetsByIndex(0)->load($filePath, function($Reader) use (&$carbonData, &$infoMessage, &$count, &$letter, &$value, $input, $params, $requiredColumns, $notRequiredColumns, $integerColumnns, $timeColumns, $dateColumnns, $pivot, $tempFile, $tableName, $openTransaction, $databaseConnectionName, $organizationId, $loggedUserId, &$data, &$columns, &$insertSize, &$uploadSize)
      {
        $rows = $Reader->formatDates(false)->toArray();
        $count = 0;
        $arraySize = count($rows);
        $uploaderCounter = 1;

        foreach ($rows as $key => $row)
        {
          $count++;

          if($count > $params['header_rows_number'] && $count <= $params['last_row_number'])
          {
            foreach ($requiredColumns as $column => $lang)
            {
              if(!isset($row[$pivot[$column]['position']]) || is_null($row[$pivot[$column]['position']]) || trim($row[$pivot[$column]['position']]) === '')
              {
                if(!empty($infoMessage))
                {
                  $infoMessage .= '<br> ';
                }

                $infoMessage .= '- ' . $this->Lang->get('decima-oadh::back-end-column.columnValidation', array('row' => $count,'letter' => $input[$column], 'column' => $lang));
              }
            }

            foreach ($integerColumnns as $column => $lang)
            {
              if(isset($row[$pivot[$column]['position']]) && $row[$pivot[$column]['position']] != '0' && ((int)$row[$pivot[$column]['position']]) == 0)
              {
                if(!empty($infoMessage))
                {
                  $infoMessage .= '<br> ';
                }

                $infoMessage .= '- ' . $this->Lang->get('decima-oadh::back-end-column.integerColumnValidation', array('row' => $count, 'letter' => $input[$column], 'column' => $lang));
              }
            }

            foreach ($columns as $columnKey => $col)
            {
              if($col == 'department')
              {
                $value = trim($row[$pivot[$col]['position']]);

                if (!in_array(mb_strtolower($value), $this->departments))
                {
                  if(!empty($infoMessage))
                  {
                    $infoMessage .= '<br> ';
                  }

                  $infoMessage .= '- ' . $this->Lang->get('decima-oadh::back-end-column.departmentColumnValidation', array('row' => $count, 'letter' => $input[$col], 'value' => $value));
                }
              }
              else if($col == 'municipality')
              {
                $value = trim($row[$pivot[$col]['position']]);

                if (!in_array(mb_strtolower($value), $this->municipalities))
                {
                  if(!empty($infoMessage))
                  {
                    $infoMessage .= '<br> ';
                  }

                  $infoMessage .= '- ' . $this->Lang->get('decima-oadh::back-end-column.municipalityColumnValidation', array('row' => $count, 'letter' => $input[$col], 'value' => $value));
                }
              }
            }

            if(!empty($infoMessage))
            {
              continue;
            }

            foreach ($columns as $columnKey => $col)
            {
              $letter = $input[$col];
              $value = $row[$pivot[$col]['position']];

              if(!empty($row[$pivot[$col]['position']]) && ($this->startsWith($col, 'date') || in_array($col, $dateColumnns)))
              {
                $temp[$col] = $this->Carbon->createFromFormat($params['date_format'], $row[$pivot[$col]['position']])->format('Y-m-d');
              }
              else if(empty($row[$pivot[$col]['position']]) && in_array($col, $dateColumnns))
              {
                $temp[$col] = null;
              }
              else if(($row[$pivot[$col]['position']] == '0' || !empty($row[$pivot[$col]['position']])) && in_array($col, $timeColumns))
              {
                if(strlen($row[$pivot[$col]['position']]) < 4)
                {
                  $row[$pivot[$col]['position']] = str_pad($row[$pivot[$col]['position']], 4, '0', STR_PAD_LEFT);
                }

                $temp[$col] = $this->Carbon->createFromFormat($params['time_format'], $row[$pivot[$col]['position']])->format('H:i');
              }
              else
              {
                if(!empty($row[$pivot[$col]['position']]))
                {
                  $temp[$col] = trim($row[$pivot[$col]['position']]);
                }
                else
                {
                  $temp[$col] = $row[$pivot[$col]['position']];
                }

              }
            }

            $temp['status'] = 'T';
            $temp['file_id'] = $params['id'];
            $temp['organization_id'] = $organizationId;

            array_push($data, $temp);

            if(empty($infoMessage) && ($uploaderCounter == $insertSize || $count == $params['last_row_number']))
            {
              $this->RightTo->massCreate($tableName . '_Temp', $data);

              unset($data);

              $data = array();

              $uploaderCounter = 1;
            }

            if($count == $params['last_row_number'])
            {
              break;
            }

            $uploaderCounter ++;
          }
        }
      });

      if($tempFile)
      {
        $this->FileManager->deleteLocalTempFile($params['name']);
      }

      if(!empty($infoMessage))
      {
        $this->rollBack($openTransaction);

        return json_encode(array('info' => $infoMessage));
      }

      $this->commit($openTransaction);
    }
    catch (\InvalidArgumentException $e)
    {
      $this->rollBack($openTransaction);

      if($tempFile)
      {
        $this->FileManager->deleteLocalTempFile($params['name']);
      }

      return json_encode(array('info' => 'Error: ' . $e->getMessage() . ', ' . 'Fila:' . $count . ', ' . 'Columna:' . $letter . ', ' . 'Valor:' . $value));
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      if($tempFile)
      {
        $this->FileManager->deleteLocalTempFile($params['name']);
      }

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      if($tempFile)
      {
        $this->FileManager->deleteLocalTempFile($params['name']);
      }

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('decima-oadh::back-end-general.processedSuccessfully')));
  }

  /**
	 * Copy data from temp table to production table
	 *
	 * @param integer $fileId
	 * @param string $tableName
	 * @param array $columnNames
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
  public function copyToProduction($fileId, $tableName, $columns, $databaseConnectionName = null)
  {
    //$columns, hacer un explode del array para crear el string: (year, sex, court, file_id, organization_id)
    $stringColumns = '';

    foreach ($columns as $key => $value)
    {
      if($stringColumns == '')
      {
        $stringColumns = $value;
      }
      else
      {
        $stringColumns = $stringColumns . ','  . $value;
      }
    }

    // $data = $this->DB->connection($databaseConnectionName)->statement('INSERT INTO public."OADH_FREE_Habeas_Corpus_Request" (year, sex, court, file_id, organization_id)
    // select year, sex, court, file_id, organization_id FROM public."OADH_FREE_Habeas_Corpus_Request_Temp" WHERE file_id = ' . $fileId );
    $sql = 'INSERT INTO public.' . '"' . $tableName . '" ' . ' (' . $stringColumns . ') ' . ' SELECT ' .  $stringColumns . ' FROM public.' . '"' . $tableName . '_Temp" WHERE file_id =' . $fileId;

    $request = $this->RightTo->countRequest($tableName, $fileId);

    if($request > 0)
    {
      $response = json_encode(array('info' => $this->Lang->get('decima-oadh::back-end-general.alreadyCopied')));
    }
    else
    {
      $this->RightTo->executeStatement($sql);
      $this->RightTo->massUpdate($tableName . '_Temp', $fileId,  array('status' => 'P') );
      // $this->DB->connection()->table('OADH_FREE_Habeas_Corpus_Request_Temp')->where('file_id', '=', $fileId)->update();
      $response = json_encode(array('success' => $this->Lang->get('decima-oadh::back-end-general.copiedSuccessfully')));
    }

    return $response;
  }

  /**
	 * Delete data from production
	 *
   * @param integer $fileId
	 * @param string $tableName
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
  public function deleteFromProduction($fileId, $tableName, $databaseConnectionName = null)
  {
    $request = $this->RightTo->countRequest($tableName, $fileId);

    if($request == 0)
    {
      $response = json_encode(array('info' => $this->Lang->get('decima-oadh::back-end-general.fileNeverCopied')));
    }
    else
    {
      $this->RightTo->massDelete($tableName, $fileId);
      $this->RightTo->massUpdate($tableName . '_Temp', $fileId,  array('status' => 'T') );

      $response = json_encode(array('success' => $this->Lang->get('decima-oadh::back-end-general.deletedSuccessfully')));
    }

    return $response;
  }

  /**
	 * Delete file from disk
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            )
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
  public function deleteFile($fileId, $tableName, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $this->beginTransaction($openTransaction, $databaseConnectionName);
    $response = "";

    try
    {
      $request = $this->RightTo->countRequest($tableName, $fileId);

      if($request > 0)
      {
        $response = json_encode(array('info' => $this->Lang->get('decima-oadh::back-end-general.errorDeletingFile')));
      }
      else
      {
        $this->RightTo->massDelete($tableName . '_Temp', $fileId);
        // $this->DB->connection($databaseConnectionName)->table('OADH_FREE_Habeas_Corpus_Request_Temp')->where('file_id', '=', $fileId)->delete();

        $this->FileManager->delete(
          array(
            'id' => $fileId
          ),
          $openTransaction,
          $databaseConnectionName,
          $organizationId,
          $loggedUserId
        );

        $response = json_encode(array('success' => $this->Lang->get('decima-oadh::back-end-general.fileDeletedSuccessfully')));
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return $response;
  }
}
