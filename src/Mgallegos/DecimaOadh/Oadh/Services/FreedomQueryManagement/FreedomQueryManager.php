<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\FreedomQueryManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;

use App\Kwaai\Security\Repositories\Journal\JournalInterface;

use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\FreedomQueryInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\FreeVictimsLibertyDeprivation\FreeVictimsLibertyDeprivationInterface;

use Carbon\Carbon;

use Illuminate\Database\DatabaseManager;

use Illuminate\Translation\Translator;

use Illuminate\Config\Repository;

use Illuminate\Cache\CacheManager;

class FreedomQueryManager extends AbstractLaravelValidator implements FreedomQueryManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
   *  FreedomQuery Interface
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\FreedomQueryInterface
   *
   */
  protected $FreedomQuery;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    FreedomQueryInterface $FreedomQuery,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->Journal = $Journal;

    $this->GridEncoder = $GridEncoder;

    $this->FreedomQuery = $FreedomQuery;

    $this->Carbon = $Carbon;

    $this->DB = $DB;

		$this->Lang = $Lang;

		$this->Config = $Config;
	}

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHabeasCorpus(array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->habeasCorpusCourtFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['court'][] = array('label' => $FreedomQuery->court, 'value' => $FreedomQuery->court);
    });

    $this->FreedomQuery->habeasCorpusSexFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['gender'][] = array('label' => $FreedomQuery->sex, 'value' => $FreedomQuery->sex);
    });

    $this->FreedomQuery->habeasCorpusByCourtBySexByStatus($input['filter']['court'], $input['filter']['gender'])->each(function($FreedomQuery) use (&$data, &$excel)
    {
      $data['data'][] = array('label'=> $FreedomQuery->year , 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getPeopleDetained (array $input) {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->peopleDetainedStatusFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['state'][] = array('label' => $FreedomQuery->state, 'value' => $FreedomQuery->state);
    });

    $this->FreedomQuery->peopleDetainedDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->peopleDetainedByStateByDepartmentByStatus($input['filter']['state'], $input['filter']['department'])->each(function($FreedomQuery) use (&$data, &$excel)
    {
      $data['data'][] = array('label'=> $FreedomQuery->year , 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getCriminalCasesTrials (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Fase',
      'C' =>  'Frecuencia',
    );

    $this->FreedomQuery->criminalCasesTrialsCourtFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['court'][] = array('label' => $FreedomQuery->court, 'value' => $FreedomQuery->court);
    });

    $this->FreedomQuery->criminalCasesTrailsByPhaseByCourtByStatus($input['filter']['court'])->each(function($FreedomQuery) use (&$data, &$excel)
    {
      // $data['data'][] = array('year' => $FreedomQuery->year, 'count' => $FreedomQuery->count);
      $data['data'][$FreedomQuery->year]['year'] = $FreedomQuery->year;
      $data['data'][$FreedomQuery->year][$FreedomQuery->phase]['label'] = $FreedomQuery->phase;
      $data['data'][$FreedomQuery->year][$FreedomQuery->phase]['count'] = $FreedomQuery->count;

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->phase;
      $excel['details']['C'][] = $FreedomQuery->count;
    });

    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getMassTrials(array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->massTrialsSexFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['gender'][] = array('label' => $FreedomQuery->sex, 'value' => $FreedomQuery->sex);
    });

    $this->FreedomQuery->massTrialsCourtFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['court'][] = array('label' => $FreedomQuery->court, 'value' => $FreedomQuery->court);
    });

    $this->FreedomQuery->massTrialsBySexByCourtByStatus($input['filter']['gender'], $input['filter']['court'])->each(function($FreedomQuery) use (&$data, &$excel)
    {
      $data['data'][] = array('label'=> $FreedomQuery->year , 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getProceduralFraud(array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->proceduralFraudGenderFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['gender'][] = array('label' => $FreedomQuery->gender, 'value' => $FreedomQuery->gender);
    });

    $this->FreedomQuery->proceduralFraudCrimeFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['crime'][] = array('label' => $FreedomQuery->crime, 'value' => $FreedomQuery->crime);
    });

    $this->FreedomQuery->proceduralFraudMunicipalityFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['municipality'][] = array('label' => $FreedomQuery->municipality, 'value' => $FreedomQuery->municipality);
    });

    $this->FreedomQuery->proceduralFraudDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->proceduralFraudByGenderByCrimeByDepartmentByMunicipalityByStatus($input['filter']['gender'], $input['filter']['department'], $input['filter']['crime'], $input['filter']['municipality'])->each(function($FreedomQuery) use (&$data, &$excel)
    {
      $data['data'][] = array('label'=> $FreedomQuery->year , 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getCaptureOrders(array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->captureOrdersDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->captureOrdersMunicipalityFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['municipality'][] = array('label' => $FreedomQuery->municipality, 'value' => $FreedomQuery->municipality);
    });

    $this->FreedomQuery->captureOrdersByDepartmentByMunicipalityByStatus($input['filter']['department'], $input['filter']['municipality'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $FreedomQuery->year, 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getDisappearancesVictims(array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->disappearancesVictimsDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->disappearancesVictimsMunicipalityFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['municipality'][] = array('label' => $FreedomQuery->municipality, 'value' => $FreedomQuery->municipality);
    });

    $this->FreedomQuery->disappearancesVictimsByDepartmentByMunicipalityByStatus($input['filter']['department'], $input['filter']['municipality'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $FreedomQuery->year, 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getAccusedLibertyDeprivation(array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->accusedLibertyDeprivationDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->accusedLibertyDeprivationMunicipalityFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['municipality'][] = array('label' => $FreedomQuery->municipality, 'value' => $FreedomQuery->municipality);
    });

    $this->FreedomQuery->accusedLibertyDeprivationProfessionFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['profession'][] = array('label' => $FreedomQuery->profession, 'value' => $FreedomQuery->profession);
    });

    $this->FreedomQuery->accusedLibertyDeprivationGenderFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['gender'][] = array('label' => $FreedomQuery->gender, 'value' => $FreedomQuery->gender);
    });

    $this->FreedomQuery->accusedLibertyDeprivationByDepartmentByMunicipalityByProfessionByGenderByStatus($input['filter']['department'], $input['filter']['municipality'], $input['filter']['profession'], $input['filter']['gender'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $FreedomQuery->year, 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getVictimsLibertyDeprivation(array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->victimsLibertyDeprivationDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->victimsLibertyDeprivationMunicipalityFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['municipality'][] = array('label' => $FreedomQuery->municipality, 'value' => $FreedomQuery->municipality);
    });

    $this->FreedomQuery->victimsLibertyDeprivationByDepartmentByMunicipalityByStatus($input['filter']['department'], $input['filter']['municipality'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $FreedomQuery->year, 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getForcedDisappearances (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->forcedDisappearancesGenderFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['gender'][] = array('label' => $FreedomQuery->gender, 'value' => $FreedomQuery->gender);
    });

    $this->FreedomQuery->forcedDisappearancesDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->forcedDisappearancesMunicipalityFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['municipality'][] = array('label' => $FreedomQuery->municipality, 'value' => $FreedomQuery->municipality);
    });

    $this->FreedomQuery->forcedDisappearancesProfessionFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['profession'][] = array('label' => $FreedomQuery->profession, 'value' => $FreedomQuery->profession);
    });

    $this->FreedomQuery->forcedDisappearancesCrimeFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['crime'][] = array('label' => $FreedomQuery->crime, 'value' => $FreedomQuery->crime);
    });

    $this->FreedomQuery->forcedDisappearancesByDepartmentByMunicipalityByProfessionByGenderByCrimeByStatus($input['filter']['department'], $input['filter']['municipality'], $input['filter']['profession'], $input['filter']['gender'], $input['filter']['crime'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $FreedomQuery->year, 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getJailsOccupation (array $input) 
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Hombres',
      'C' =>  'Mujeres',
    );

    $this->FreedomQuery->jailsOccupationDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->jailsOccupationStationFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['station'][] = array('label' => $FreedomQuery->station, 'value' => $FreedomQuery->station);
    });

    $this->FreedomQuery->jailsOccupationUnitFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['unit'][] = array('label' => $FreedomQuery->unit, 'value' => $FreedomQuery->unit);
    });

    $this->FreedomQuery->jailsOccupationByDepartmentByStationByUnitByStatus($input['filter']['department'], $input['filter']['station'], $input['filter']['unit'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('year' => $FreedomQuery->year, 'column_a' => $FreedomQuery->women_count, 'column_b' => $FreedomQuery->man_count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->man_count;
      $excel['details']['C'][] = $FreedomQuery->women_count;
    });

    $data['isGender'] = true;
    $data['labels']['valueAxis'] = 'Población';
    $data['labels']['column_a'] = 'Mujeres';
    $data['labels']['column_b'] = 'Hombres';
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getDeprivedPersons (array $input) 
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->deprivedPersonsRightFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['right'][] = array('label' => $FreedomQuery->right, 'value' => $FreedomQuery->right);
    });

    $this->FreedomQuery->deprivedPersonsViolatedFactFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['violatedFact'][] = array('label' => $FreedomQuery->violatedFact, 'value' => $FreedomQuery->violatedFact);
    });

    $this->FreedomQuery->deprivedPersonsGenderFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['gender'][] = array('label' => $FreedomQuery->gender, 'value' => $FreedomQuery->gender);
    });

    $this->FreedomQuery->deprivedPersonsDepartmentFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['department'][] = array('label' => $FreedomQuery->department, 'value' => $FreedomQuery->department);
    });

    $this->FreedomQuery->deprivedPersonsMunicipalityFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['municipality'][] = array('label' => $FreedomQuery->municipality, 'value' => $FreedomQuery->municipality);
    });

    $this->FreedomQuery->deprivedPersonsByRightByViolatedFactByGenderByDepartmentByMunicipalityStatus($input['filter']['right'], $input['filter']['violatedFact'], $input['filter']['gender'], $input['filter']['department'], $input['filter']['municipality'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $FreedomQuery->year, 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });

    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getAcuteDiseases (array $input) 
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->acuteDiseasesDiseaseFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['disease'][] = array('label' => $FreedomQuery->disease, 'value' => $FreedomQuery->disease);
    });

    $this->FreedomQuery->acuteDiseasesByDiseaseByStatus($input['filter']['disease'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $FreedomQuery->year, 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getChronicDiseases (array $input) 
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' =>  'Años',
      'B' =>  'Frecuencia',
    );

    $this->FreedomQuery->chronicDiseasesChronicDiseaseFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['chronicDisease'][] = array('label' => $FreedomQuery->chronicdisease, 'value' => $FreedomQuery->chronicdisease);
    });

    $this->FreedomQuery->chronicDiseasesGenderFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['gender'][] = array('label' => $FreedomQuery->gender, 'value' => $FreedomQuery->gender);
    });

    $this->FreedomQuery->chronicDiseasesJailFilter()->each(function ($FreedomQuery) use (&$data) {
      $data['jail'][] = array('label' => $FreedomQuery->jail, 'value' => $FreedomQuery->jail);
    });

    $this->FreedomQuery->chronicDiseasesByChronicDiseaseByGenderByJailByStatus($input['filter']['chronicDisease'], $input['filter']['gender'], $input['filter']['jail'])->each(function ($FreedomQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $FreedomQuery->year, 'value' => $FreedomQuery->count);

      $excel['details']['A'][] = $FreedomQuery->year;
      $excel['details']['B'][] = $FreedomQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }
}
