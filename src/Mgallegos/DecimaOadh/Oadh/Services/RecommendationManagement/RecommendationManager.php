<?php
/**
 * @file
 * Recommendations Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\RecommendationManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation\EloquentRecommendationGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationInstitution\EloquentRecommendationInstitutionGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight\EloquentRecommendationRightGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport\EloquentRecommendationImportGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation\RecommendationInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationInstitution\RecommendationInstitutionInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight\RecommendationRightInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport\RecommendationImportInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Institution\InstitutionInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Right\RightInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Setting\SettingInterface;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Translation\Translator;
use Illuminate\Config\Repository;
use Illuminate\Cache\CacheManager;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class RecommendationManager extends AbstractLaravelValidator implements RecommendationManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Uploader Manager
  *
  * @var Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface
  *
  */
  protected $UploaderManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent Recommendation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation\EloquentRecommendationGridRepository
	 *
	 */
	protected $EloquentRecommendationGridRepository;

  /**
	 * Eloquent Recommendation Detail Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationInstitution\EloquentRecommendationInstitutionGridRepository
	 *
	 */
	protected $EloquentRecommendationInstitutionGridRepository;

  /**
	 * Eloquent Recommendation Detail Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight\EloquentRecommendationRightGridRepository
	 *
	 */
	protected $EloquentRecommendationRightGridRepository;

  /**
	 * Eloquent Recommendation Detail Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport\EloquentRecommendationImportGridRepository
	 *
	 */
	protected $EloquentRecommendationImportGridRepository;

  /**
	 *  Recommendation Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Recommendation\RecommendationInterface
	 *
	 */
	protected $Recommendation;

  /**
	 *  Recommendation Detail Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationRight\RecommendationRightInterface
	 *
	 */
	protected $RecommendationRight;

  /**
	 *  Recommendation Detail Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationImport\RecommendationImportInterface
	 *
	 */
	protected $RecommendationImport;

  /**
	 *  Institution Detail Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Institution\InstitutionInterface
	 *
	 */
	protected $Institution;

  /**
	 *  Recommendation Detail Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Right\RightInterface
	 *
	 */
	protected $Right;

  /**
	 *  Recommendation Detail Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\RecommendationInstitution\RecommendationInstitutionInterface
	 *
	 */
	protected $RecommendationInstitution;

  /**
	 *  Module Table Name Interface
	 *
	 * @var Vendor\DecimaModule\Module\Repositories\Setting\SettingInterface
	 *
	 */
	protected $Setting;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    UploaderManagementInterface $UploaderManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    EloquentRecommendationGridRepository $EloquentRecommendationGridRepository,
    EloquentRecommendationInstitutionGridRepository $EloquentRecommendationInstitutionGridRepository,
    EloquentRecommendationRightGridRepository $EloquentRecommendationRightGridRepository,
    EloquentRecommendationImportGridRepository $EloquentRecommendationImportGridRepository,
    RecommendationInterface $Recommendation,
    RecommendationInstitutionInterface $RecommendationInstitution,
    RecommendationRightInterface $RecommendationRight,
    RecommendationImportInterface $RecommendationImport,
    InstitutionInterface $Institution,
    RightInterface $Right,
    SettingInterface $Setting,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->UploaderManager = $UploaderManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->EloquentRecommendationGridRepository = $EloquentRecommendationGridRepository;
    $this->EloquentRecommendationInstitutionGridRepository = $EloquentRecommendationInstitutionGridRepository;
    $this->EloquentRecommendationRightGridRepository = $EloquentRecommendationRightGridRepository;
    $this->EloquentRecommendationImportGridRepository = $EloquentRecommendationImportGridRepository;
    $this->Recommendation = $Recommendation;
    $this->RecommendationInstitution = $RecommendationInstitution;
    $this->RecommendationRight = $RecommendationRight;
    $this->RecommendationImport = $RecommendationImport;
    $this->Institution = $Institution;
    $this->Right = $Right;
    $this->Setting = $Setting;
    $this->Carbon = $Carbon;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
    $this->Cache = $Cache;
	}

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataMaster(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentRecommendationGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataInstitution(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentRecommendationInstitutionGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataRight(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentRecommendationRightGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataImport(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentRecommendationImportGridRepository, $post);
  }

  /**
   * Get search modal table rows
   *
   * @return array
   */
  public function getSearchModalTableRows($id = null, $input, $pager = false, $organizationId = null, $databaseConnectionName = null, $returnJson = true)
  {
    $rows = array();
    $limit = $offset = $count = 0;
    $filter = '';

    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(!empty($input['filter']))
    {
      $filter = $input['filter'];
    }

    if($pager)
    {
      $count = $this->Recommendation->searchModalTableRows($id, $organizationId, true, $limit, $offset, $filter, $databaseConnectionName);

      encode_requested_data(
        $input,
        $count,
        $limit,
        $offset
      );
    }

    $this->Recommendation->searchModalTableRows($id, $organizationId, false, $limit, $offset, $filter, $databaseConnectionName)->each(function($Recommendation) use (&$rows)
    {
      $rows['key' . $Recommendation->id] = (array)$Recommendation;
    });

    $rows = array(
      'from' => $offset,
      'to' => $limit,
      'page' => !empty($input['page']) ? (int)$input['page'] : 1,
      'records' => $count,
      'rows' => $rows
    );

    if($returnJson)
    {
      return json_encode($rows);
    }

    return $rows;
  }

  /**
   * get Advanced Search Modal Table Rows with in-built eloquent paginate
   *
   * @param integer $id
   * @param array $input
   * @param string $databaseConnectionName
   * @param integer $organizationId
   * @return Collection
   */
  public function getAdvancedSearchModalTableRows($id = null, $input = null, $databaseConnectionName = null, $organizationId = null)
  {
    $filters = '';

    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(!empty($input['filters']))
    {
      $filters = $input['filters'];

      foreach($filters as $key => $filter)
      {
        if($filter['field'] == 'rec.date')
        {
          if(!empty($filter['data_from']))
          {
            $filters[$key]['data_from'] = $this->Carbon->createFromDate($filter['data_from'])->startOfYear()->format('Y-m-d');
          }

          if(!empty($filter['data_to']))
          {
            $filters[$key]['data_to'] = $this->Carbon->createFromDate($filter['data_to'])->endOfYear()->format('Y-m-d');
          }
          
        }
      }
    }

    $Recommendation = $this->Recommendation->searchAdvancedModalTableRows($id, $filters, true, $databaseConnectionName);
    
    $Recommendation->map(function($rec) use($databaseConnectionName)
    {
      $institutions = $this->Recommendation->getInstitutionsByRecommendationId($rec->id, $databaseConnectionName);

      if(!empty($institutions))
      {
        $rec->institutions = $institutions;
      }
      return $rec;
    });

    return $Recommendation;
  }

  /**
   * Creates a .xlsx file containing data from visible recommendation cards
   *
   * @param Array $input
   * @param string $databaseConnectionName
   * @param integer $organizationId
   * @return \Symfony\Component\HttpFoundation\StreamedResponse $streamedResponse
   */
  public function downloadAllExcel($input = null, $databaseConnectionName = null)
  {
    $filters = '';
    $input = json_decode($input['oadh-rdt-export-data'], true);

    if(!empty($input['filters']))
    {
      $filters = $input['filters'];

      foreach($filters as $key => $filter)
      {
        if($filter['field'] == 'rec.date')
        {
          if(!empty($filter['data_from']))
          {
            $filters[$key]['data_from'] = $this->Carbon->createFromDate($filter['data_from'])->startOfYear()->format('Y-m-d');
          }

          if(!empty($filter['data_to']))
          {
            $filters[$key]['data_to'] = $this->Carbon->createFromDate($filter['data_to'])->endOfYear()->format('Y-m-d');
          }
          
        }
      }
    }

    $data = $this->Recommendation->searchAdvancedModalTableRows(null, $filters, false, $databaseConnectionName);

    $Recommendation = collect($data);

    $Recommendation->map(function($rec) use($databaseConnectionName)
    {
      $institutions = $this->Recommendation->getInstitutionsByRecommendationId($rec->id, $databaseConnectionName);
      $rights = $this->Recommendation->getRightsByRecommendationId($rec->id, $databaseConnectionName);

      if(!empty($institutions))
      {
        $instArr = $rightArr = array();
        foreach($institutions as $inst)
        {
          $instArr[] = $inst->name . ' ('. $inst->abbreviation .')';
        }

        foreach($rights as $right)
        {
          $rightArr[] = $right->name;
        }
        $rec->institutions = implode(', ', $instArr);
        $rec->rights = implode(', ', $rightArr);
      }

      return $rec;
    });

    $streamedResponse = new StreamedResponse();

    $streamedResponse->setCallback(function () use($Recommendation, $input)
		{
      $spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
      $lastHeaderCellRow = 'K';

      $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
      $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
			$sheet->getPageSetup()->setFitToWidth(1);
			$sheet->getPageSetup()->setFitToHeight(0);

			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $styles = array(
        'header' =>  array(
          'fill' => array(
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => array(
              'argb' => 'FF2A4074'
            ) 
          ),
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => true,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE
            )
          ),
          'borders' => array(
            'outline' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE
              )
            )
          )
        ),
        'details' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_JUSTIFY,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => false,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        ),
        'details_date' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => false,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        )
      );

      // $headerTitle = array(
      //   'Observatorio Universitario de Derechos Humanos de la UCA (OUDH)',
      //   'Recomendaciones al Estado Salvadoreños de los Sistemas Internacionales de Derechos Humanos',
      //   'Resultados de búsqueda. Generados el ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      // );

      $headerTitle = array(
        $this->Config->get('system-lang.' . $input['lang'] . '.oudh'),
        $this->Config->get('system-lang.' . $input['lang'] . '.recomendacionesv'),
        $this->Config->get('system-lang.' . $input['lang'] . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      // $tableHeaders = array(
      //   'A4' => 'Nombre del sistema',
      //   'B4' => 'Recomendación',
      //   'C4' => 'Código',
      //   'D4' => 'Fecha de recomendación',
      //   'E4' => 'Mecanismo',
      //   'F4' => 'Derechos relacionados',
      //   'G4' => 'Instituciones responsables',
      //   'H4' => 'Fuente de la que surge la recomendación',
      //   'I4' => 'Base legal de las competencias institucionales.',
      //   'J4' => 'Observaciones',
      //   'K4' => 'Actualización',
      // );

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $input['lang'] . '.sistema'),
        'B4' => $this->Config->get('system-lang.' . $input['lang'] . '.recomendacion'),
        'C4' => $this->Config->get('system-lang.' . $input['lang'] . '.codigoreferencia'),
        'D4' => $this->Config->get('system-lang.' . $input['lang'] . '.fecharecomendacion'),
        'E4' => $this->Config->get('system-lang.' . $input['lang'] . '.mecanismo'),
        'F4' => $this->Config->get('system-lang.' . $input['lang'] . '.derechosrelacionados'),
        'G4' => $this->Config->get('system-lang.' . $input['lang'] . '.institucionesresponsables'),
        'H4' => $this->Config->get('system-lang.' . $input['lang'] . '.fuenterecomendacion'),
        'I4' => $this->Config->get('system-lang.' . $input['lang'] . '.baselegal'),
        'J4' => $this->Config->get('system-lang.' . $input['lang'] . '.observaciones'),
        'K4' => $this->Config->get('system-lang.' . $input['lang'] . '.actualizacion'),
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:$lastHeaderCellRow$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:K$row")->applyFromArray($styles['header']);
      }

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }

      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(30);
      }
      
      foreach (range('C', 'D') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(15);
      }

      foreach (range('E', 'F') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(30);
      }

      foreach (range('G', 'I') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }

      foreach (range('J', 'K') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(15);
      }

      //details

      $startRowNumber = 4;
      $lastRowNumber = $startRowNumber;

      foreach ($Recommendation as $key => $row)
      {
        $lastRowNumber++;
        $sheet->setCellValue("A$lastRowNumber", $row->system);
        $sheet->setCellValue("B$lastRowNumber", $row->recommendation);
        $sheet->setCellValue("C$lastRowNumber", $row->code);
        $sheet->setCellValue("D$lastRowNumber", $this->Carbon->createFromFormat('Y-m-d', $row->date)->format($this->Lang->get('form.phpShortDateFormat')));
        $sheet->getStyle("D$lastRowNumber")->applyFromArray($styles['details_date']);
        $sheet->setCellValue("E$lastRowNumber", $row->mechanism);
        $sheet->setCellValue("F$lastRowNumber", $row->rights);
        $sheet->setCellValue("G$lastRowNumber", $row->institutions);
        $sheet->setCellValue("H$lastRowNumber", $row->source);
        $sheet->setCellValue("I$lastRowNumber", $row->legal_base);
        $sheet->setCellValue("J$lastRowNumber", $row->observations);
        $sheet->setCellValue("K$lastRowNumber", (!empty($row->last_modified_date)) ? $this->Carbon->createFromFormat('Y-m-d', $row->last_modified_date)->format($this->Lang->get('form.phpShortDateFormat')) : '');
        $sheet->getStyle("K$lastRowNumber")->applyFromArray($styles['details_date']);
      }

      $startRowNumber++;

      $sheet->getStyle("A$startRowNumber:C$lastRowNumber")->applyFromArray($styles['details']);
      $sheet->getStyle("E$startRowNumber:J$lastRowNumber")->applyFromArray($styles['details']);
      $sheet->getStyle("E$startRowNumber:J$lastRowNumber")->applyFromArray($styles['details']);
      $sheet->getStyle("A$startRowNumber:K$lastRowNumber")->getAlignment()->setWrapText(true);

			$writer =  new Xlsx($spreadsheet);
			$writer->save('php://output');
    });

    $streamedResponse->setStatusCode(Response::HTTP_OK);
		$streamedResponse->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$streamedResponse->headers->set('Content-Disposition', 'attachment; filename="' . $this->Config->get('system-lang.' . $input['lang'] . '.recomendaciones') . '_' . $this->Carbon->now()->format('d_m_Y') .'.xlsx"');

		return $streamedResponse->send();
  }


  public function downloadSinglePdf($input, $databaseConnectionName = null, $organizationId = null)
  {
    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    $id = json_decode($input['oadh-rdt-export-data-pdf'], true);

    $right = $institution = array();
    
    $Recommendation = $this->Recommendation->byId($id, $databaseConnectionName)->toArray();

    $this->Recommendation->getInstitutionsByRecommendationId($id, $databaseConnectionName)->each(function($Institution) use(&$institution)
    {
      if(!empty($Institution->name))
      {
        $institution[] = $Institution->name;
      }
    });

    $this->Recommendation->getRightsByRecommendationId($id, $databaseConnectionName)->each(function($Right) use(&$right)
    {
      if(!empty($Right->name))
      {
        $right[] = $Right->name;
      }
    });

    if(!empty($right))
    {
      $Recommendation['rights'] = $right;
    }
    else
    {
      $Recommendation['rights'] = [];
    }

    if(!empty($institution))
    {
      $Recommendation['institutions'] = $institution;
    }
    else
    {
      $Recommendation['institutions'] = [];
    }

    $downloadInput = array(
      'datetime' => $this->Carbon->now()->setTimeZone('America/El_Salvador')->format('Y-m-d H:m:i'),
      'recommendation_id' => $id,
      'organization_id' => 1,
    );

    $this->Setting->createDownload($downloadInput);

    $pdf = App::make('dompdf.wrapper');
    $Recommendation['lang'] = $input['lang'];

    return $pdf->loadView('decima-oadh::front-end/recommendation-card-pdf', $Recommendation)
      ->setPaper('letter')
      ->download($Recommendation['code'] . '.pdf');
  }

  /**
   * Get Recommendation
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getRecommendation($id, $databaseConnectionName = null)
  {
    $Recommendation = $this->Recommendation->byId($id, $databaseConnectionName)->toArray();

    $this->Recommendation->getInstitutionsByRecommendationId($id, $databaseConnectionName)->each(function($Institution) use(&$Recommendation)
    {
      $Recommendation['institutions'][] = (array)$Institution;
    });

    $this->Recommendation->getRightsByRecommendationId($id, $databaseConnectionName)->each(function($Right) use(&$Recommendation)
    {
      $Recommendation['rights'][] = (array)$Right;
    });

    if(empty($Recommendation))
    {
      return false;
    }

    return $Recommendation;
  }

  /**
   * Get Recommendation
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getRecommendations($databaseConnectionName = null)
  {
    $Recommendations = array();

    $this->Recommendation->byOrganization($this->AuthenticationManager->getCurrentUserOrganizationId(), $databaseConnectionName)->each(function($Recommendation) use (&$Recommendations)
    {
      array_push($Recommendations, array('label'=> $Recommendation->name , 'value'=>$Recommendation->id));
    });

    return $Recommendations;
  }
  
/**
 * Get distinct systems
 *
 * @param string $databaseConnectionName
 * @return Collection
 */
  public function getSystems($databaseConnectionName = null)
  {
    $data = array();
    $this->Recommendation->getSystems($databaseConnectionName)->each(function($Recommendation) use(&$data, $databaseConnectionName)
    {
      $data[$Recommendation->system]['system'] = $Recommendation->system;

      $this->Recommendation->getMechanismBySystem($Recommendation->system, $databaseConnectionName)->each(function($Mechanism) use(&$data, $Recommendation)
      {
        $data[$Recommendation->system]['mechanism'][] = $Mechanism->mechanism; 
      });
    });

    // $data['Sistema Interamericano de Derechos Humanos'] = array(
    //   'system' =>  'Sistema Interamericano de Derechos Humanos',
    //   'mechanism' => array(
    //     'mechanism 1',
    //     'mechanism 2',
    //     'mechanism 3',
    //   )
    // );

    // $data['Sistema 2'] = array(
    //   'system' =>  'Sistema 2',
    //   'mechanism' => array(
    //     'mechanism 4',
    //     'mechanism 5',
    //     'mechanism 6',
    //   )
    // );

    return $data;
  }

/**
 * Get distinct mechanisms
 *
 * @param string $databaseConnectionName
 * @return Collection
 */
  public function getMechanisms($databaseConnectionName = null)
  {
    return $this->Recommendation->getMechanisms($databaseConnectionName);
  }

  /**
   * Get institutions
   *
   * @param string $databaseConnectionName
   * @return Collection
   */
  public function getInstitutions($databaseConnectionName = null)
  {
    return $this->Recommendation->getInstitutions($databaseConnectionName);
  }

  /**
   * Get rights
   *
   * @param string $databaseConnectionName
   * @return Collection
   */
  public function getRights($databaseConnectionName = null)
  {
    return $this->Recommendation->getRights($databaseConnectionName);
  }

  public function recommendationProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['system', 'recommendation', 'code', 'date', 'mechanism', 'related_rights', 'responsible_institutions', 'source', 'legal_base', 'observations', 'last_modified_date'];
    $recommendation = 'OADH_Recommendation';

    return $this->UploaderManager->processSpreadsheet(
      $params, 
      $input, 
      $columns, 
      $recommendation, 
      array('observations', 'last_modified_date'), //$notRequiredColumns = array(), 
      array(), //$integerColumnns = array(), 
      array(), //$timeColumns = array(), 
      array('date', 'last_modified_date'), //$dateColumnns = array(), 
      true, //$openTransaction = true, 
      $databaseConnectionName, 
      $organizationId, 
      $loggedUserId
    );
  }

  public function recommendationCopyToProduction($input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null)
  {
    $columns = ['system', 'recommendation', 'code', 'date', 'mechanism', 'related_rights', 'responsible_institutions', 'source', 'legal_base', 'observations', 'last_modified_date', 'file_id', 'organization_id'];
    $recommendation = 'OADH_Recommendation';
    $infoMessage = '';
    $institutions = $rights = array();

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      $this->Institution->byOrganization($organizationId, $databaseConnectionName)->each(function($Institution) use (&$institutions)
      {
        // $institutions[$Institution->abbreviation] = array('id' => $Institution->id, 'name' =>$Institution->name);
        $institutions[$Institution->abbreviation] = $Institution->id;
      });
      
      $this->Right->byOrganization($organizationId, $databaseConnectionName)->each(function($Right) use (&$rights)
      {
        $rights[$Right->name] = $Right->id;
      });

      $this->RecommendationImport->byFileId($input['file_id'], $databaseConnectionName)->each(function($Recommendation) use (&$infoMessage, &$institutions, &$rights)
      {
        foreach (explode(',', $Recommendation->responsible_institutions) as $key => $institution)
        {  
          if(!isset($institutions[trim($institution)]))
          {
            if(!empty($infoMessage))
            {
              $infoMessage .= '<br> ';
            }

            $infoMessage .= '- ' . $this->Lang->get('decima-oadh::back-end-column.institutionColumnValidation', array('name' => $institution));
          }
        }

        foreach (explode(',', $Recommendation->related_rights) as $key => $right)
        {
          if(!isset($rights[trim($right)]))
          {
            if(!empty($infoMessage))
            {
              $infoMessage .= '<br> ';
            }

            $infoMessage .= '- ' . $this->Lang->get('decima-oadh::back-end-column.rightColumnValidation', array('name' => $right));
          }
        }
      });

      if(!empty($infoMessage))
      {
        return json_encode(
          array('info' => $infoMessage)
        );
      }

      $response = json_decode( 
        $this->UploaderManager->copyToProduction($input['file_id'], $recommendation, $columns),
        true
      );

      if(!empty($response['info']))
      {
        return json_encode(
          array('info' => $response['info'])
        );
      }

      $this->Recommendation->byFileId($input['file_id'], $databaseConnectionName)->each(function($Recommendation) use (&$infoMessage, &$institutions, &$rights, $input, $databaseConnectionName, $organizationId)
      {
        foreach (explode(',', $Recommendation->responsible_institutions) as $key => $institution)
        { 
          $this->RecommendationInstitution->create(
            array(
              'recommendation_id' => $Recommendation->id,
              'institution_id' => $institutions[trim($institution)],
              'file_id' => $input['file_id'],
              'organization_id' => $organizationId
            ), 
            $databaseConnectionName
          );
        }

        foreach (explode(',', $Recommendation->related_rights) as $key => $right)
        {
          
          $this->RecommendationRight->create(
            array(
              'recommendation_id' => $Recommendation->id,
              'right_id' => $rights[trim($right)],
              'file_id' => $input['file_id'],
              'organization_id' => $organizationId
            ), 
            $databaseConnectionName
          );
        }
      });

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('decima-oadh::back-end-general.copiedSuccessfully')));
  }

  public function recommendationDeleteFromProduction($input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null)
  {
    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      $this->Recommendation->massDelete('OADH_Recommendation_Institution', $input['file_id']);
      $this->Recommendation->massDelete('OADH_Recommendation_Right', $input['file_id']);

      $response = json_decode( 
        $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_Recommendation'),
        true
      );

      if(!empty($response['info']))
      {
        return json_encode(
          array('info' => $response['info'])
        );
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('decima-oadh::back-end-general.deletedSuccessfully')));
  }

  public function recommendationDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_Recommendation');
  }

  /**
	 * Create a new Recommendation
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function createMaster(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    unset(
      $input['_token']
    );

    if(!empty($input['token']))
    {
      $token = !empty($input['token']) ? $input['token'] : '';
      $loggedUser = $this->AuthenticationManager->getApiLoggedUser($token, false);

      if(empty($loggedUser))
      {
        $this->Log->warning('[SECURITY EVENT] Action - Invalid token', array(
          'error' => 'Invalid token', 
          'errorCode' => '001',
        ));

        return response()->json(['error' => 'Invalid token', 'errorCode' => '001']);
      }

      unset( $input['token'] );

      $databaseConnectionName = $loggedUser['database_connection_name'];
      $organizationId = $loggedUser['organization_id'];
      $loggedUserId = $loggedUser['id'];
    }

    $input = eloquent_array_filter_for_insert($input);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

		$input = array_add($input, 'organization_id', $organizationId);
    // $input = array_add($input, 'created_by', $loggedUserId);
    // $input = array_add($input, 'number', $this->Recommendation->getMaxNumber($organizationId, $databaseConnectionName) + 1);

    if(!empty($input['date']))
    {
      $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    }
    
    if(!empty($input['last_modified_date']))
    {
      $input['last_modified_date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['last_modified_date'])->format('Y-m-d');
    }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
		{
      $Recommendation = $this->Recommendation->create($input, $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $Recommendation->id, 'journalized_type' => $this->Recommendation->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail(
        $Journal->id, 
        array(
          'note' => $this->Lang->get('decima-oadh::recommendation-management.addedMasterJournal', array(
            'name' => substr($Recommendation->recommendation, 0, 50)
          )
        )), 
        $Journal
      );

      // $this->Cache->forget('RecommendationsSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessSaveMessage'),
        'id' => $Recommendation->id,
        // 'smtRow' => $this->getSearchModalTableRows($Recommendation->id, $organizationId, $databaseConnectionName, false)
      )
    );
  }

  /**
	 * Create a new recommendation
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function createInsDetail(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    unset(
      $input['_token'],
      $input['id'],
      $input['institution_label']
    );

    if(!empty($input['token']))
    {
      $token = !empty($input['token']) ? $input['token'] : '';
      $loggedUser = $this->AuthenticationManager->getApiLoggedUser($token, false);

      if(empty($loggedUser))
      {
        $this->Log->warning('[SECURITY EVENT] Action - Invalid token', array(
          'error' => 'Invalid token', 
          'errorCode' => '001',
        ));

        return response()->json(['error' => 'Invalid token', 'errorCode' => '001']);
      }

      unset( $input['token'] );

      $databaseConnectionName = $loggedUser['database_connection_name'];
      $organizationId = $loggedUser['organization_id'];
      $loggedUserId = $loggedUser['id'];
    }

    $input = eloquent_array_filter_for_insert($input);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

		$input = array_add($input, 'organization_id', $organizationId);
    // $input = array_add($input, 'created_by', $loggedUserId);

    // if(!empty($input['date']))
    // {
    //   $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    // }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
		{
      $RecommendationInstitution = $this->RecommendationInstitution->create($input, $databaseConnectionName);
      $Recommendation = $this->Recommendation->byId($RecommendationInstitution->recommendation_id, $databaseConnectionName);
      $Instituion = $this->Institution->byId($RecommendationInstitution->institution_id, $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $RecommendationInstitution->recommendation_id, 'journalized_type' => $this->Recommendation->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail(
        $Journal->id, 
        array(
          'note' => $this->Lang->get('decima-oadh::recommendation-management.addedInsDetailJournal', array(
            'detailName' => $Instituion->name . '(' . $Instituion->abbreviation . ')', 
            'masterName' => substr($Recommendation->recommendation, 0, 50)
          ))
        ), 
        $Journal);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessUpdateMessage')
      )
    );
  }

  /**
	 * Create a new recommendation
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function createRigDetail(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    unset(
      $input['_token'],
      $input['id'],
      $input['right_label']
    );

    if(!empty($input['token']))
    {
      $token = !empty($input['token']) ? $input['token'] : '';
      $loggedUser = $this->AuthenticationManager->getApiLoggedUser($token, false);

      if(empty($loggedUser))
      {
        $this->Log->warning('[SECURITY EVENT] Action - Invalid token', array(
          'error' => 'Invalid token', 
          'errorCode' => '001',
        ));

        return response()->json(['error' => 'Invalid token', 'errorCode' => '001']);
      }

      unset( $input['token'] );

      $databaseConnectionName = $loggedUser['database_connection_name'];
      $organizationId = $loggedUser['organization_id'];
      $loggedUserId = $loggedUser['id'];
    }

    $input = eloquent_array_filter_for_insert($input);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

		$input = array_add($input, 'organization_id', $organizationId);
    // $input = array_add($input, 'created_by', $loggedUserId);

    // if(!empty($input['date']))
    // {
    //   $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    // }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
		{
      $RecommendationRight = $this->RecommendationRight->create($input, $databaseConnectionName);
      $Recommendation = $this->Recommendation->byId($RecommendationRight->recommendation_id, $databaseConnectionName);
      $Right = $this->Right->byId($RecommendationRight->right_id, $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $RecommendationRight->recommendation_id, 'journalized_type' => $this->Recommendation->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail(
        $Journal->id, 
        array(
          'note' => $this->Lang->get('decima-oadh::recommendation-management.addedRigDetailJournal', array(
            'detailName' => $Right->name, 
            'masterName' => substr($Recommendation->recommendation, 0, 50)
          ))
        ), 
        $Journal);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessUpdateMessage')
      )
    );
  }

  /**
   * Update an existing Recommendation
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function updateMaster(array $input, $Recommendation = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    // if(!empty($input['table_name_label']))
    // {
    //   $newValues['table_name_id'] = $input['table_name_label'];
    // }

    unset(
      $input['_token']
    );

    if(!empty($input['token']))
    {
      $token = !empty($input['token']) ? $input['token'] : '';
      $loggedUser = $this->AuthenticationManager->getApiLoggedUser($token, false);

      if(empty($loggedUser))
      {
        $this->Log->warning('[SECURITY EVENT] Action - Invalid token', array(
          'error' => 'Invalid token', 
          'errorCode' => '001',
        ));

        return response()->json(['error' => 'Invalid token', 'errorCode' => '001']);
      }

      unset( $input['token'] );

      $databaseConnectionName = $loggedUser['database_connection_name'];
      $organizationId = $loggedUser['organization_id'];
      $loggedUserId = $loggedUser['id'];
    }

    $input = eloquent_array_filter_for_update($input);

    if(!empty($input['date']))
    {
      $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    }
    
    if($input['last_modified_date'] == " " || empty($input['last_modified_date']))
    {
      unset($input['last_modified_date']);
    }
    
    if(!empty($input['last_modified_date']))
    {
      $input['last_modified_date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['last_modified_date'])->format('Y-m-d');
    }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      if(empty($Recommendation))
      {
        $Recommendation = $this->Recommendation->byId($input['id'], $databaseConnectionName);
      }

      $unchangedValues = $Recommendation->toArray();

      $this->Recommendation->update($input, $Recommendation);

      $diff = 0;

      foreach ($input as $key => $value)
      {
        if($unchangedValues[$key] != $value)
        {
          $diff++;

          if($diff == 1)
          {
            $Journal = $this->Journal->create(array('journalized_id' => $Recommendation->id, 'journalized_type' => $this->Recommendation->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
          }

          if($key == 'status')//Para autocomple de estados
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
          }
          else if ($key == 'field1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.field1'), 'field_lang_key' => 'decima-oadh::recommendation-management.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'name')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'chekbox0' || $key == 'chekbox1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::recommendation-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
          }
          else if($key == 'date' || $key == 'last_modified_date')
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $oldValue = '';
            }

            if(!empty($value))
            {
              $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $newValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::back-end-column.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::back-end-column.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
          }
          else if($key == 'table_name_id')//field required
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::recommendation-management.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
          }
          else if($key == 'table_name_id')//field not required
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
            }
            else
            {
              $oldValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::recommendation-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
          }
          else
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::back-end-column.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::back-end-column.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
        }
      }

      // $this->Cache->forget('RecommendationsSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessUpdateMessage'),
        // 'smtRow' => $this->getSearchModalTableRows($Recommendation->id, $organizationId, $databaseConnectionName, false)
      )
    );
  }

  // /**
  //  * Update an existing Recommendation
  //  *
  //  * @param array $input
  //  * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
  //  *
  //  * @return JSON encoded string
  //  *  A string as follows:
  //  *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
  //  */
  // public function updateDetail(array $input, $RecommendationInstitution = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  // {
  //   // if(!empty($input['table_name_label']))
  //   // {
  //   //   $newValues['table_name_id'] = $input['table_name_label'];
  //   // }

  //   unset(
  //     $input['_token']
  //   );

  //   if(!empty($input['token']))
  //   {
  //     $token = !empty($input['token']) ? $input['token'] : '';
  //     $loggedUser = $this->AuthenticationManager->getApiLoggedUser($token, false);

  //     if(empty($loggedUser))
  //     {
  //       $this->Log->warning('[SECURITY EVENT] Action - Invalid token', array(
  //         'error' => 'Invalid token', 
  //         'errorCode' => '001',
  //       ));

  //       return response()->json(['error' => 'Invalid token', 'errorCode' => '001']);
  //     }

  //     unset( $input['token'] );

  //     $databaseConnectionName = $loggedUser['database_connection_name'];
  //     $organizationId = $loggedUser['organization_id'];
  //     $loggedUserId = $loggedUser['id'];
  //   }

  //   $input = eloquent_array_filter_for_update($input);

  //   // if(!empty($input['date']))
  //   // {
  //   //   $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
  //   // }

  //   // if(!empty($input['amount']))
  //   // {
  //   //   $input['amount'] = remove_thousands_separator($input['amount']);
  //   // }

  //   if(empty($organizationId))
  //   {
  //     $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  //   }

  //   if(empty($loggedUserId))
  //   {
  //     $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  //   }

  //   $this->beginTransaction($openTransaction, $databaseConnectionName);

  //   try
  //   {
  //     if(empty($RecommendationInstitution))
  //     {
  //       $RecommendationInstitution = $this->RecommendationInstitution->byId($input['id'], $databaseConnectionName);
  //     }

  //     $unchangedValues = $RecommendationInstitution->toArray();

  //     $this->RecommendationInstitution->update($input, $RecommendationInstitution);

  //     $diff = 0;

  //     foreach ($input as $key => $value)
  //     {
  //       if($unchangedValues[$key] != $value)
  //       {
  //         $diff++;

  //         if($diff == 1)
  //         {
  //           $Journal = $this->Journal->create(array('journalized_id' => $RecommendationInstitution->recommendation_id, 'journalized_type' => $this->Recommendation->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
  //         }

  //         if($key == 'status')//Para autocomple de estados
  //         {
  //           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
  //         }
  //         else if ($key == 'field1')
  //         {
  //           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.field1'), 'field_lang_key' => 'decima-oadh::recommendation-management.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
  //         }
  //         else if ($key == 'name')
  //         {
  //           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
  //         }
  //         else if ($key == 'chekbox0' || $key == 'chekbox1')
  //         {
  //           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::recommendation-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
  //         }
  //         else if($key == 'date')
  //         {
  //           if(!empty($unchangedValues[$key]))
  //           {
  //             $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
  //           }
  //           else
  //           {
  //             $oldValue = '';
  //           }

  //           if(!empty($value))
  //           {
  //             $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
  //           }
  //           else
  //           {
  //             $newValue = '';
  //           }

  //           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::recommendation-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
  //         }
  //         else if($key == 'table_name_id')//field required
  //         {
  //           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::recommendation-management.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
  //         }
  //         else if($key == 'table_name_id')//field not required
  //         {
  //           if(!empty($unchangedValues[$key]))
  //           {
  //             $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
  //           }
  //           else
  //           {
  //             $oldValue = '';
  //           }

  //           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::recommendation-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
  //         }
  //         else
  //         {
  //           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::recommendation-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::recommendation-management.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
  //         }
  //       }
  //     }

  //     $this->commit($openTransaction);
  //   }
  //   catch (\Exception $e)
  //   {
  //     $this->rollBack($openTransaction);

  //     throw $e;
  //   }
  //   catch (\Throwable $e)
  //   {
  //     $this->rollBack($openTransaction);

  //     throw $e;
  //   }

  //   return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
  // }


  /**
   * Delete an existing Recommendation
   *
   * @param array $input
	 * 	An array as follows: array(id => $id);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */
  public function deleteMaster(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    if(!empty($input['token']))
    {
      $token = !empty($input['token']) ? $input['token'] : '';
      $loggedUser = $this->AuthenticationManager->getApiLoggedUser($token, false);

      if(empty($loggedUser))
      {
        $this->Log->warning('[SECURITY EVENT] Action - Invalid token', array(
          'error' => 'Invalid token', 
          'errorCode' => '001',
        ));

        return response()->json(['error' => 'Invalid token', 'errorCode' => '001']);
      }

      unset( $input['token'] );

      $databaseConnectionName = $loggedUser['database_connection_name'];
      $organizationId = $loggedUser['organization_id'];
      $loggedUserId = $loggedUser['id'];
    }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      $Recommendation = $this->Recommendation->byId($input['id'], $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $input['id'], 'journalized_type' => $this->Recommendation->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail(
        $Journal->id, 
        array(
          'note' => $this->Lang->get('decima-oadh::recommendation-management.deletedMasterJournal', array(
            'name' => substr($Recommendation->recommendation, 0, 50)
          )
        )), 
        $Journal
      );

      $this->RecommendationRight->massDeleteByColumn('recommendation_id', $Recommendation->id, $databaseConnectionName);
      $this->RecommendationInstitution->massDeleteByColumn('recommendation_id', $Recommendation->id, $databaseConnectionName);
      $this->Recommendation->delete(array($input['id']), $databaseConnectionName);

      // $this->Cache->forget('RecommendationsSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessDeleteMessage'),
        // 'smtRowId' => $input['id']
      )
    );
  }

  /**
   * Delete existing Recommendation
   *
   * @param array $input
   * 	An array as follows: array($id0, $id1,…);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
  */
  public function deleteInsDetails(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $count = 0;

    if(!empty($input['token']))
    {
      $token = !empty($input['token']) ? $input['token'] : '';
      $loggedUser = $this->AuthenticationManager->getApiLoggedUser($token, false);

      if(empty($loggedUser))
      {
        $this->Log->warning('[SECURITY EVENT] Action - Invalid token', array(
          'error' => 'Invalid token', 
          'errorCode' => '001',
        ));

        return response()->json(['error' => 'Invalid token', 'errorCode' => '001']);
      }

      unset( $input['token'] );

      $databaseConnectionName = $loggedUser['database_connection_name'];
      $organizationId = $loggedUser['organization_id'];
      $loggedUserId = $loggedUser['id'];
    }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      foreach ($input['id'] as $key => $id)
      {
        $count++;

        $RecommendationInstitution = $this->RecommendationInstitution->byId($id, $databaseConnectionName);
        $Institution = $this->Institution->byId($RecommendationInstitution->institution_id, $databaseConnectionName);

        if(empty($Recommendation))
        {
          $Recommendation = $this->Recommendation->byId($RecommendationInstitution->recommendation_id, $databaseConnectionName);
        }

        $Journal = $this->Journal->create(array('journalized_id' => $RecommendationInstitution->recommendation_id, 'journalized_type' => $this->Recommendation->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
        $this->Journal->attachDetail(
          $Journal->id, 
          array(
            'note' => $this->Lang->get('decima-oadh::recommendation-management.deletedInsDetailJournal', array(
              'detailName' => $Institution->name . ' (' . $Institution->abbreviation . ')', 
              'masterName' => substr($Recommendation->recommendation, 0, 50)
            )
          )), 
          $Journal
        );

        $this->RecommendationInstitution->delete(array($id), $databaseConnectionName);
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    if($count == 1)
    {
      return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
    }
    else
    {
      return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
    }
  }

  /**
   * Delete existing Recommendation
   *
   * @param array $input
   * 	An array as follows: array($id0, $id1,…);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
  */
  public function deleteRigDetails(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $count = 0;

    if(!empty($input['token']))
    {
      $token = !empty($input['token']) ? $input['token'] : '';
      $loggedUser = $this->AuthenticationManager->getApiLoggedUser($token, false);

      if(empty($loggedUser))
      {
        $this->Log->warning('[SECURITY EVENT] Action - Invalid token', array(
          'error' => 'Invalid token', 
          'errorCode' => '001',
        ));

        return response()->json(['error' => 'Invalid token', 'errorCode' => '001']);
      }

      unset( $input['token'] );

      $databaseConnectionName = $loggedUser['database_connection_name'];
      $organizationId = $loggedUser['organization_id'];
      $loggedUserId = $loggedUser['id'];
    }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      foreach ($input['id'] as $key => $id)
      {
        $count++;

        $RecommendationRight = $this->RecommendationRight->byId($id, $databaseConnectionName);
        $Right = $this->Right->byId($RecommendationRight->right_id, $databaseConnectionName);

        if(empty($Recommendation))
        {
          $Recommendation = $this->Recommendation->byId($RecommendationRight->recommendation_id, $databaseConnectionName);
        }

        $Journal = $this->Journal->create(array('journalized_id' => $RecommendationRight->recommendation_id, 'journalized_type' => $this->Recommendation->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
        $this->Journal->attachDetail(
          $Journal->id, 
          array(
            'note' => $this->Lang->get('decima-oadh::recommendation-management.deletedInsDetailJournal', array(
              'detailName' => $Right->name, 
              'masterName' => substr($Recommendation->recommendation, 0, 50)
            )
          )), 
          $Journal
        );

        $this->RecommendationRight->delete(array($id), $databaseConnectionName);
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    if($count == 1)
    {
      return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
    }
    else
    {
      return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
    }
  }
}
