<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\IntegrityUploaderManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;

use Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface;

use App\Kwaai\Security\Repositories\Journal\JournalInterface;

use Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface;

use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteCrimesVictimsGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteCrimesVictimsTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteCrimesAccusedGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteCrimesAccusedTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteWorkingOfficialsGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteWorkingOfficialsTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteGenderTraffickingGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteGenderTraffickingTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeTraffickingGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeTraffickingTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeTraffickingGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeTraffickingTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteMovementFreedomGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteMovementFreedomTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeWomenGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeWomenTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeWomenGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeWomenTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteStateWomenGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteStateWomenTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeSexualGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteTypeSexualTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteGenderSexualGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteGenderSexualTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeSexualGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeSexualTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteStateInjuriesGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteStateInjuriesTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeInjuriesGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgeInjuriesTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteScheduleInjuriesGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteScheduleInjuriesTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgentInvestigationGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteAgentInvestigationTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteIntegrityViolationGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\EloquentInteIntegrityViolationTempGridRepository;

use Carbon\Carbon;

use Maatwebsite\Excel\Excel;

use Illuminate\Config\Repository;

use Illuminate\Translation\Translator;

use Illuminate\Database\DatabaseManager;

class IntegrityUploaderManager extends AbstractLaravelValidator implements IntegrityUploaderManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * File Management Interface
  *
  * @var Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface
  *
  */
  protected $FileManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $UploaderManager;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent CrimesVictims Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\CrimesVictims\EloquentCrimesVictimsGridRepository
	 *
	 */
	protected $EloquentCrimesVictimsGridRepository;

  /**
	 * Eloquent CrimesVictims Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\CrimesVictims\EloquentCrimesVictimsGridRepository
	 *
	 */
	protected $EloquentCrimesVictimsTempGridRepository;

  /**
	 * Eloquent CrimesAccused Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\CrimesAccused\EloquentCrimesAccusedGridRepository
	 *
	 */
	protected $EloquentCrimesAccusedGridRepository;

  /**
	 * Eloquent CrimesAccused Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\CrimesAccused\EloquentCrimesAccusedGridRepository
	 *
	 */
	protected $EloquentCrimesAccusedTempGridRepository;

  /**
	 * Eloquent WorkingOfficials Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\WorkingOfficials\EloquentWorkingOfficialsGridRepository
	 *
	 */
	protected $EloquentWorkingOfficialsGridRepository;

  /**
	 * Eloquent WorkingOfficials Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\WorkingOfficials\EloquentWorkingOfficialsGridRepository
	 *
	 */
	protected $EloquentWorkingOfficialsTempGridRepository;

  /**
	 * Eloquent GenderTrafficking Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\GenderTrafficking\EloquentGenderTraffickingGridRepository
	 *
	 */
	protected $EloquentGenderTraffickingGridRepository;

  /**
	 * Eloquent GenderTrafficking Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\GenderTrafficking\EloquentGenderTraffickingGridRepository
	 *
	 */
	protected $EloquentGenderTraffickingTempGridRepository;

  /**
	 * Eloquent TypeTrafficking Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\TypeTrafficking\EloquentTypeTraffickingGridRepository
	 *
	 */
	protected $EloquentTypeTraffickingGridRepository;

  /**
	 * Eloquent TypeTrafficking Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\TypeTrafficking\EloquentTypeTraffickingGridRepository
	 *
	 */
	protected $EloquentTypeTraffickingTempGridRepository;

  /**
	 * Eloquent AgeTrafficking Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgeTrafficking\EloquentAgeTraffickingGridRepository
	 *
	 */
	protected $EloquentAgeTraffickingGridRepository;

  /**
	 * Eloquent AgeTrafficking Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgeTrafficking\EloquentAgeTraffickingGridRepository
	 *
	 */
	protected $EloquentAgeTraffickingTempGridRepository;

  /**
	 * Eloquent MovementFreedom Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\MovementFreedom\EloquentMovementFreedomGridRepository
	 *
	 */
	protected $EloquentMovementFreedomGridRepository;

  /**
	 * Eloquent MovementFreedom Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\MovementFreedom\EloquentMovementFreedomGridRepository
	 *
	 */
	protected $EloquentMovementFreedomTempGridRepository;

  /**
	 * Eloquent AgeWomen Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgeWomen\EloquentAgeWomenGridRepository
	 *
	 */
	protected $EloquentAgeWomenGridRepository;

  /**
	 * Eloquent AgeWomen Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgeWomen\EloquentAgeWomenGridRepository
	 *
	 */
	protected $EloquentAgeWomenTempGridRepository;

  /**
	 * Eloquent TypeWomen Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\TypeWomen\EloquentTypeWomenGridRepository
	 *
	 */
	protected $EloquentTypeWomenGridRepository;

  /**
	 * Eloquent TypeWomen Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\TypeWomen\EloquentTypeWomenGridRepository
	 *
	 */
	protected $EloquentTypeWomenTempGridRepository;

  /**
	 * Eloquent StateWomen Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\StateWomen\EloquentStateWomenGridRepository
	 *
	 */
	protected $EloquentStateWomenGridRepository;

  /**
	 * Eloquent StateWomen Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\StateWomen\EloquentStateWomenGridRepository
	 *
	 */
	protected $EloquentStateWomenTempGridRepository;

  /**
	 * Eloquent TypeSexual Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\TypeSexual\EloquentTypeSexualGridRepository
	 *
	 */
	protected $EloquentTypeSexualGridRepository;

  /**
	 * Eloquent TypeSexual Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\TypeSexual\EloquentTypeSexualGridRepository
	 *
	 */
	protected $EloquentTypeSexualTempGridRepository;

  /**
	 * Eloquent GenderSexual Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\GenderSexual\EloquentGenderSexualGridRepository
	 *
	 */
	protected $EloquentGenderSexualGridRepository;

  /**
	 * Eloquent GenderSexual Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\GenderSexual\EloquentGenderSexualGridRepository
	 *
	 */
	protected $EloquentGenderSexualTempGridRepository;

  /**
	 * Eloquent AgeSexual Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgeSexual\EloquentAgeSexualGridRepository
	 *
	 */
	protected $EloquentAgeSexualGridRepository;

  /**
	 * Eloquent AgeSexual Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgeSexual\EloquentAgeSexualGridRepository
	 *
	 */
	protected $EloquentAgeSexualTempGridRepository;

  /**
	 * Eloquent StateInjuries Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\StateInjuries\EloquentStateInjuriesGridRepository
	 *
	 */
	protected $EloquentStateInjuriesGridRepository;

  /**
	 * Eloquent StateInjuries Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\StateInjuries\EloquentStateInjuriesGridRepository
	 *
	 */
	protected $EloquentStateInjuriesTempGridRepository;

  /**
	 * Eloquent AgeInjuries Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgeInjuries\EloquentAgeInjuriesGridRepository
	 *
	 */
	protected $EloquentAgeInjuriesGridRepository;

  /**
	 * Eloquent AgeInjuries Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgeInjuries\EloquentAgeInjuriesGridRepository
	 *
	 */
	protected $EloquentAgeInjuriesTempGridRepository;

  /**
	 * Eloquent ScheduleInjuries Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ScheduleInjuries\EloquentScheduleInjuriesGridRepository
	 *
	 */
	protected $EloquentScheduleInjuriesGridRepository;

  /**
	 * Eloquent ScheduleInjuries Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ScheduleInjuries\EloquentScheduleInjuriesGridRepository
	 *
	 */
	protected $EloquentScheduleInjuriesTempGridRepository;

  /**
	 * Eloquent AgentInvestigation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgentInvestigation\EloquentAgentInvestigationGridRepository
	 *
	 */
	protected $EloquentAgentInvestigationGridRepository;

  /**
	 * Eloquent AgentInvestigation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AgentInvestigation\EloquentAgentInvestigationGridRepository
	 *
	 */
	protected $EloquentAgentInvestigationTempGridRepository;

  /**
	 * Eloquent IntegrityViolation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\IntegrityViolation\EloquentIntegrityViolationGridRepository
	 *
	 */
	protected $EloquentIntegrityViolationGridRepository;

  /**
	 * Eloquent IntegrityViolation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\IntegrityViolation\EloquentIntegrityViolationGridRepository
	 *
	 */
	protected $EloquentIntegrityViolationTempGridRepository;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
  * Maatwebsite\Excel\Excel
  *
  * @var Excel
  *
  */
  protected $Excel;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    FileManagementInterface $FileManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    UploaderManagementInterface $UploaderManager,

    EloquentInteCrimesVictimsGridRepository $EloquentCrimesVictimsGridRepository,
    EloquentInteCrimesVictimsTempGridRepository $EloquentCrimesVictimsTempGridRepository,

    EloquentInteCrimesAccusedGridRepository $EloquentCrimesAccusedGridRepository,
    EloquentInteCrimesAccusedTempGridRepository $EloquentCrimesAccusedTempGridRepository,

    EloquentInteWorkingOfficialsGridRepository $EloquentWorkingOfficialsGridRepository,
    EloquentInteWorkingOfficialsTempGridRepository $EloquentWorkingOfficialsTempGridRepository,

    EloquentInteGenderTraffickingGridRepository $EloquentGenderTraffickingGridRepository,
    EloquentInteGenderTraffickingTempGridRepository $EloquentGenderTraffickingTempGridRepository,

    EloquentInteTypeTraffickingGridRepository $EloquentTypeTraffickingGridRepository,
    EloquentInteTypeTraffickingTempGridRepository $EloquentTypeTraffickingTempGridRepository,

    EloquentInteAgeTraffickingGridRepository $EloquentAgeTraffickingGridRepository,
    EloquentInteAgeTraffickingTempGridRepository $EloquentAgeTraffickingTempGridRepository,

    EloquentInteMovementFreedomGridRepository $EloquentMovementFreedomGridRepository,
    EloquentInteMovementFreedomTempGridRepository $EloquentMovementFreedomTempGridRepository,

    EloquentInteAgeWomenGridRepository $EloquentAgeWomenGridRepository,
    EloquentInteAgeWomenTempGridRepository $EloquentAgeWomenTempGridRepository,

    EloquentInteTypeWomenGridRepository $EloquentTypeWomenGridRepository,
    EloquentInteTypeWomenTempGridRepository $EloquentTypeWomenTempGridRepository,

    EloquentInteStateWomenGridRepository $EloquentStateWomenGridRepository,
    EloquentInteStateWomenTempGridRepository $EloquentStateWomenTempGridRepository,

    EloquentInteTypeSexualGridRepository $EloquentTypeSexualGridRepository,
    EloquentInteTypeSexualTempGridRepository $EloquentTypeSexualTempGridRepository,

    EloquentInteGenderSexualGridRepository $EloquentGenderSexualGridRepository,
    EloquentInteGenderSexualTempGridRepository $EloquentGenderSexualTempGridRepository,

    EloquentInteAgeSexualGridRepository $EloquentAgeSexualGridRepository,
    EloquentInteAgeSexualTempGridRepository $EloquentAgeSexualTempGridRepository,

    EloquentInteStateInjuriesGridRepository $EloquentStateInjuriesGridRepository,
    EloquentInteStateInjuriesTempGridRepository $EloquentStateInjuriesTempGridRepository,

    EloquentInteAgeInjuriesGridRepository $EloquentAgeInjuriesGridRepository,
    EloquentInteAgeInjuriesTempGridRepository $EloquentAgeInjuriesTempGridRepository,

    EloquentInteScheduleInjuriesGridRepository $EloquentScheduleInjuriesGridRepository,
    EloquentInteScheduleInjuriesTempGridRepository $EloquentScheduleInjuriesTempGridRepository,

    EloquentInteAgentInvestigationGridRepository $EloquentAgentInvestigationGridRepository,
    EloquentInteAgentInvestigationTempGridRepository $EloquentAgentInvestigationTempGridRepository,

    EloquentInteIntegrityViolationGridRepository $EloquentIntegrityViolationGridRepository,
    EloquentInteIntegrityViolationTempGridRepository $EloquentIntegrityViolationTempGridRepository,

    Carbon $Carbon,
    Excel $Excel,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->FileManager = $FileManager;

    $this->Journal = $Journal;

    $this->GridEncoder = $GridEncoder;

    $this->UploaderManager = $UploaderManager;

    $this->EloquentCrimesVictimsGridRepository = $EloquentCrimesVictimsGridRepository;
    $this->EloquentCrimesVictimsTempGridRepository = $EloquentCrimesVictimsTempGridRepository;

    $this->EloquentCrimesAccusedGridRepository = $EloquentCrimesAccusedGridRepository;
    $this->EloquentCrimesAccusedTempGridRepository = $EloquentCrimesAccusedTempGridRepository;

    $this->EloquentWorkingOfficialsGridRepository = $EloquentWorkingOfficialsGridRepository;
    $this->EloquentWorkingOfficialsTempGridRepository = $EloquentWorkingOfficialsTempGridRepository;

    $this->EloquentGenderTraffickingGridRepository = $EloquentGenderTraffickingGridRepository;
    $this->EloquentGenderTraffickingTempGridRepository = $EloquentGenderTraffickingTempGridRepository;

    $this->EloquentTypeTraffickingGridRepository = $EloquentTypeTraffickingGridRepository;
    $this->EloquentTypeTraffickingTempGridRepository = $EloquentTypeTraffickingTempGridRepository;

    $this->EloquentAgeTraffickingGridRepository = $EloquentAgeTraffickingGridRepository;
    $this->EloquentAgeTraffickingTempGridRepository = $EloquentAgeTraffickingTempGridRepository;

    $this->EloquentMovementFreedomGridRepository = $EloquentMovementFreedomGridRepository;
    $this->EloquentMovementFreedomTempGridRepository = $EloquentMovementFreedomTempGridRepository;

    $this->EloquentAgeWomenGridRepository = $EloquentAgeWomenGridRepository;
    $this->EloquentAgeWomenTempGridRepository = $EloquentAgeWomenTempGridRepository;

    $this->EloquentTypeWomenGridRepository = $EloquentTypeWomenGridRepository;
    $this->EloquentTypeWomenTempGridRepository = $EloquentTypeWomenTempGridRepository;

    $this->EloquentStateWomenGridRepository = $EloquentStateWomenGridRepository;
    $this->EloquentStateWomenTempGridRepository = $EloquentStateWomenTempGridRepository;

    $this->EloquentTypeSexualGridRepository = $EloquentTypeSexualGridRepository;
    $this->EloquentTypeSexualTempGridRepository = $EloquentTypeSexualTempGridRepository;

    $this->EloquentGenderSexualGridRepository = $EloquentGenderSexualGridRepository;
    $this->EloquentGenderSexualTempGridRepository = $EloquentGenderSexualTempGridRepository;

    $this->EloquentAgeSexualGridRepository = $EloquentAgeSexualGridRepository;
    $this->EloquentAgeSexualTempGridRepository = $EloquentAgeSexualTempGridRepository;

    $this->EloquentStateInjuriesGridRepository = $EloquentStateInjuriesGridRepository;
    $this->EloquentStateInjuriesTempGridRepository = $EloquentStateInjuriesTempGridRepository;

    $this->EloquentAgeInjuriesGridRepository = $EloquentAgeInjuriesGridRepository;
    $this->EloquentAgeInjuriesTempGridRepository = $EloquentAgeInjuriesTempGridRepository;

    $this->EloquentScheduleInjuriesGridRepository = $EloquentScheduleInjuriesGridRepository;
    $this->EloquentScheduleInjuriesTempGridRepository = $EloquentScheduleInjuriesTempGridRepository;

    $this->EloquentAgentInvestigationGridRepository = $EloquentAgentInvestigationGridRepository;
    $this->EloquentAgentInvestigationTempGridRepository = $EloquentAgentInvestigationTempGridRepository;

    $this->EloquentIntegrityViolationGridRepository = $EloquentIntegrityViolationGridRepository;
    $this->EloquentIntegrityViolationTempGridRepository = $EloquentIntegrityViolationTempGridRepository;

    $this->Carbon = $Carbon;

    $this->Excel = $Excel;

    $this->DB = $DB;

		$this->Lang = $Lang;

		$this->Config = $Config;
	}


  //Habeas Corpus Request
  public function getcrimesVictimsGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentCrimesVictimsGridRepository, $post);
  }

  public function getcrimesVictimsTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentCrimesVictimsTempGridRepository, $post);
  }

	public function crimesVictimsProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime', 'sex', 'age_range'];
    $tableName = 'OADH_INT_Crimes_Victims';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function crimesVictimsCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'sex', 'age_range', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Crimes_Victims';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function crimesVictimsDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Crimes_Victims');
  }

  public function crimesVictimsDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Crimes_Victims');
  }

  //Crimes Accused
  public function getcrimesAccusedGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentCrimesAccusedGridRepository, $post);
  }

  public function getcrimesAccusedTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentCrimesAccusedTempGridRepository, $post);
  }

	public function crimesAccusedProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime', 'category', 'department', 'municipality'];
    $tableName = 'OADH_INT_Crimes_Accused';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function crimesAccusedCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'category', 'department', 'municipality', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Crimes_Accused';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function crimesAccusedDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Crimes_Accused');
  }

  public function crimesAccusedDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Crimes_Accused');
  }

  //Working Officials
  public function getworkingOfficialsGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentWorkingOfficialsGridRepository, $post);
  }

  public function getworkingOfficialsTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentWorkingOfficialsTempGridRepository, $post);
  }

	public function workingOfficialsProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime', 'sex', 'category', 'department'];
    $tableName = 'OADH_INT_Working_Officials';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function workingOfficialsCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'sex', 'category', 'department', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Working_Officials';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function workingOfficialsDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Working_Officials');
  }

  public function workingOfficialsDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Working_Officials');
  }

  //Gender traffcking
  public function getgenderTraffickingGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentGenderTraffickingGridRepository, $post);
  }

  public function getgenderTraffickingTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentGenderTraffickingTempGridRepository, $post);
  }

	public function genderTraffickingProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'sex'];
    $tableName = 'OADH_INT_Gender_Trafficking';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function genderTraffickingCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'sex', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Gender_Trafficking';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function genderTraffickingDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Gender_Trafficking');
  }

  public function genderTraffickingDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Gender_Trafficking');
  }

  //Type traffcking
  public function gettypeTraffickingGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTypeTraffickingGridRepository, $post);
  }

  public function gettypeTraffickingTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTypeTraffickingTempGridRepository, $post);
  }

	public function typeTraffickingProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime'];
    $tableName = 'OADH_INT_Type_Trafficking';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function typeTraffickingCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Type_Trafficking';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function typeTraffickingDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Type_Trafficking');
  }

  public function typeTraffickingDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Type_Trafficking');
  }

  //Age traffcking
  public function getageTraffickingGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgeTraffickingGridRepository, $post);
  }

  public function getageTraffickingTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgeTraffickingTempGridRepository, $post);
  }

	public function ageTraffickingProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'age_range'];
    $tableName = 'OADH_INT_Age_Trafficking';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function ageTraffickingCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'age_range', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Age_Trafficking';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function ageTraffickingDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Age_Trafficking');
  }

  public function ageTraffickingDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Age_Trafficking');
  }

  //Movement Freedom
  public function getmovementFreedomGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentMovementFreedomGridRepository, $post);
  }

  public function getmovementFreedomTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentMovementFreedomTempGridRepository, $post);
  }

	public function movementFreedomProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime', 'department', 'municipality', 'age_range', 'sex'];
    $tableName = 'OADH_INT_Movement_Freedom';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function movementFreedomCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'department', 'municipality', 'age_range', 'sex', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Movement_Freedom';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function movementFreedomDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Movement_Freedom');
  }

  public function movementFreedomDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Movement_Freedom');
  }

  //Age Women
  public function getageWomenGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgeWomenGridRepository, $post);
  }

  public function getageWomenTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgeWomenTempGridRepository, $post);
  }

	public function ageWomenProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'age_range'];
    $tableName = 'OADH_INT_Age_Women';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function ageWomenCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'age_range', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Age_Women';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function ageWomenDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Age_Women');
  }

  public function ageWomenDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Age_Women');
  }

  //Type Women
  public function gettypeWomenGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTypeWomenGridRepository, $post);
  }

  public function gettypeWomenTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTypeWomenTempGridRepository, $post);
  }

	public function typeWomenProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime'];
    $tableName = 'OADH_INT_Type_Women';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function typeWomenCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Type_Women';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function typeWomenDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Type_Women');
  }

  public function typeWomenDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Type_Women');
  }

  //State Women
  public function getstateWomenGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentStateWomenGridRepository, $post);
  }

  public function getstateWomenTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentStateWomenTempGridRepository, $post);
  }

	public function stateWomenProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'department', 'municipality'];
    $tableName = 'OADH_INT_State_Women';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function stateWomenCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'department', 'municipality', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_State_Women';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function stateWomenDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_State_Women');
  }

  public function stateWomenDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_State_Women');
  }

  //Type Sexual
  public function gettypeSexualGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTypeSexualGridRepository, $post);
  }

  public function gettypeSexualTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTypeSexualTempGridRepository, $post);
  }

	public function typeSexualProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime', 'department', 'municipality'];
    $tableName = 'OADH_INT_Type_Sexual';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function typeSexualCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'department', 'municipality', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Type_Sexual';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function typeSexualDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Type_Sexual');
  }

  public function typeSexualDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Type_Sexual');
  }

  //Gender Sexual
  public function getgenderSexualGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentGenderSexualGridRepository, $post);
  }

  public function getgenderSexualTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentGenderSexualTempGridRepository, $post);
  }

	public function genderSexualProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime', 'sex'];
    $tableName = 'OADH_INT_Gender_Sexual';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function genderSexualCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'sex', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Gender_Sexual';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function genderSexualDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Gender_Sexual');
  }

  public function genderSexualDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Gender_Sexual');
  }

  //Age Sexual
  public function getageSexualGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgeSexualGridRepository, $post);
  }

  public function getageSexualTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgeSexualTempGridRepository, $post);
  }

	public function ageSexualProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'crime', 'age_range'];
    $tableName = 'OADH_INT_Age_Sexual';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function ageSexualCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'crime', 'age_range', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Age_Sexual';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function ageSexualDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Age_Sexual');
  }

  public function ageSexualDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Age_Sexual');
  }

  //State injuries
  public function getstateInjuriesGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentStateInjuriesGridRepository, $post);
  }

  public function getstateInjuriesTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentStateInjuriesTempGridRepository, $post);
  }

	public function stateInjuriesProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    // $columns = ['year', 'department', 'municipality', 'total'];
    $columns = ['year', 'department', 'municipality'];
    $tableName = 'OADH_INT_State_Injuries';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function stateInjuriesCopyToProduction($input)
  {
    $response = array();
    // $columns = ['year', 'department', 'municipality', 'total', 'file_id', 'organization_id'];
    $columns = ['year', 'department', 'municipality', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_State_Injuries';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function stateInjuriesDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_State_Injuries');
  }

  public function stateInjuriesDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_State_Injuries');
  }

  //Age injuries
  public function getageInjuriesGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgeInjuriesGridRepository, $post);
  }

  public function getageInjuriesTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgeInjuriesTempGridRepository, $post);
  }

  public function ageInjuriesProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    // $columns = ['year', 'sex', 'age_range', 'total'];
    $columns = ['year', 'sex', 'crime', 'age_range'];
    $tableName = 'OADH_INT_Age_Injuries';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array('sex'), array('year'));
  }

  public function ageInjuriesCopyToProduction($input)
  {
    $response = array();
    // $columns = ['year', 'sex', 'age_range', 'total', 'file_id', 'organization_id'];
    $columns = ['year', 'sex', 'crime', 'age_range', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Age_Injuries';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function ageInjuriesDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Age_Injuries');
  }

  public function ageInjuriesDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Age_Injuries');
  }

  //Schedule Injuries
  public function getscheduleInjuriesGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentScheduleInjuriesGridRepository, $post);
  }

  public function getscheduleInjuriesTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentScheduleInjuriesTempGridRepository, $post);
  }

  public function scheduleInjuriesProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    // $columns = ['year', 'aggressor_type', 'time', 'total'];
    $columns = ['year', 'weapon_type'];
    $tableName = 'OADH_INT_Schedule_Injuries';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function scheduleInjuriesCopyToProduction($input)
  {
    $response = array();
    // $columns = ['year', 'aggressor_type', 'time', 'total', 'file_id', 'organization_id'];
    $columns = ['year', 'weapon_type', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Schedule_Injuries';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function scheduleInjuriesDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Schedule_Injuries');
  }

  public function scheduleInjuriesDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Schedule_Injuries');
  }

  //Agent Investigation
  public function getagentInvestigationGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgentInvestigationGridRepository, $post);
  }

  public function getagentInvestigationTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAgentInvestigationTempGridRepository, $post);
  }

  public function agentInvestigationProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['code', 'year', 'crime', 'category', 'sex', 'place', 'department', 'municipality', 'state'];
    $tableName = 'OADH_INT_Agent_Investigation';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function agentInvestigationCopyToProduction($input)
  {
    $response = array();
    $columns = ['code', 'year', 'crime', 'category', 'sex', 'place', 'department', 'municipality', 'state', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Agent_Investigation';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function agentInvestigationDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Agent_Investigation');
  }

  public function agentInvestigationDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Agent_Investigation');
  }

  //Integrity Violation
  public function getintegrityViolationGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentIntegrityViolationGridRepository, $post);
  }

  public function getintegrityViolationTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentIntegrityViolationTempGridRepository, $post);
  }

  public function integrityViolationProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    // $columns = ['date', 'sex', 'age', 'department', 'municipality', 'right_to', 'violated_fact', 'institutions', 'dependencies'];
    $columns = ['year', 'sex', 'age', 'department', 'municipality', 'right_to', 'violated_fact', 'institutions', 'dependencies'];
    $tableName = 'OADH_INT_Integrity_Violation';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('age', 'year'));
  }

  public function integrityViolationCopyToProduction($input)
  {
    $response = array();
    // $columns = ['date', 'sex', 'age', 'department', 'municipality', 'right_to', 'violated_fact', 'institutions', 'dependencies', 'file_id', 'organization_id'];
    $columns = ['year', 'sex', 'age', 'department', 'municipality', 'right_to', 'violated_fact', 'institutions', 'dependencies', 'file_id', 'organization_id'];
    $tableName = 'OADH_INT_Integrity_Violation';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function integrityViolationDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_INT_Integrity_Violation');
  }

  public function integrityViolationDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_INT_Integrity_Violation');
  }

  //tableName
  public function getTableNameGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameGridRepository, $post);
  }

  public function getTableNameTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameTempGridRepository, $post);
  }

	public function tableNameProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['column_1', 'column_2', 'column_3', 'column_4'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function tableNameCopyToProduction($input)
  {
    $response = array();
    $columns = ['column_1', 'column_2', 'column_3', 'column_4', 'file_id', 'organization_id'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function tableNameDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_Prefix_Table_Name');
  }

  public function tableNameDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_Prefix_Table_Name');
  }
}
