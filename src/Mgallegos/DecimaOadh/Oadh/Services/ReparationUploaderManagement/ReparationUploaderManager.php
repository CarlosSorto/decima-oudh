<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\ReparationUploaderManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationAllegationsGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationAllegationsTempGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationBudgetGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationBudgetTempGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationFisdlBeneficiariesGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationFisdlBeneficiariesTempGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationAmnestyGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Reparation\EloquentReparationAmnestyTempGridRepository;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;
use Illuminate\Config\Repository;
use Illuminate\Translation\Translator;
use Illuminate\Database\DatabaseManager;

class ReparationUploaderManager extends AbstractLaravelValidator implements ReparationUploaderManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * File Management Interface
  *
  * @var Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface
  *
  */
  protected $FileManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $UploaderManager;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent Allegations Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Allegations\EloquentReparationAllegationsGridRepository
	 *
	 */
	protected $EloquentReparationAllegationsGridRepository;

  /**
	 * Eloquent Allegations Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Allegations\EloquentReparationAllegationsGridRepository
	 *
	 */
	protected $EloquentReparationAllegationsTempGridRepository;

  /**
	 * Eloquent Budget Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Budget\EloquentReparationBudgetGridRepository
	 *
	 */
	protected $EloquentReparationBudgetGridRepository;

  /**
	 * Eloquent Budget Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Budget\EloquentReparationConstitutionalProccessGridRepository
	 *
	 */
	protected $EloquentReparationBudgetTempGridRepository;

  /**
	 * Eloquent FisdlBeneficiaries Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\FisdlBeneficiaries\EloquentReparationFisdlBeneficiariesGridRepository
	 *
	 */
	protected $EloquentReparationFisdlBeneficiariesGridRepository;

  /**
	 * Eloquent FisdlBeneficiaries Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\FisdlBeneficiaries\EloquentReparationFisdlBeneficiariesGridRepository
	 *
	 */
	protected $EloquentReparationFisdlBeneficiariesTempGridRepository;

  /**
	 * Eloquent Amnesty Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Amnesty\EloquentReparationAmnestyGridRepository
	 *
	 */
	protected $EloquentReparationAmnestyGridRepository;

  /**
	 * Eloquent Amnesty Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Amnesty\EloquentReparationConstitutionalProccessGridRepository
	 *
	 */
	protected $EloquentReparationAmnestyTempGridRepository;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
  * Maatwebsite\Excel\Excel
  *
  * @var Excel
  *
  */
  protected $Excel;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    FileManagementInterface $FileManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    UploaderManagementInterface $UploaderManager,
    EloquentReparationAllegationsGridRepository $EloquentReparationAllegationsGridRepository,
    EloquentReparationAllegationsTempGridRepository $EloquentReparationAllegationsTempGridRepository,
    EloquentReparationBudgetGridRepository $EloquentReparationBudgetGridRepository,
    EloquentReparationBudgetTempGridRepository $EloquentReparationBudgetTempGridRepository,
    EloquentReparationFisdlBeneficiariesGridRepository $EloquentReparationFisdlBeneficiariesGridRepository,
    EloquentReparationFisdlBeneficiariesTempGridRepository $EloquentReparationFisdlBeneficiariesTempGridRepository,
    EloquentReparationAmnestyGridRepository $EloquentReparationAmnestyGridRepository,
    EloquentReparationAmnestyTempGridRepository $EloquentReparationAmnestyTempGridRepository,
    Carbon $Carbon,
    Excel $Excel,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->FileManager = $FileManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->UploaderManager = $UploaderManager;
    $this->EloquentReparationAllegationsGridRepository = $EloquentReparationAllegationsGridRepository;
    $this->EloquentReparationAllegationsTempGridRepository = $EloquentReparationAllegationsTempGridRepository;
    $this->EloquentReparationBudgetGridRepository = $EloquentReparationBudgetGridRepository;
    $this->EloquentReparationBudgetTempGridRepository = $EloquentReparationBudgetTempGridRepository;
    $this->EloquentReparationFisdlBeneficiariesGridRepository = $EloquentReparationFisdlBeneficiariesGridRepository;
    $this->EloquentReparationFisdlBeneficiariesTempGridRepository = $EloquentReparationFisdlBeneficiariesTempGridRepository;
    $this->EloquentReparationAmnestyGridRepository = $EloquentReparationAmnestyGridRepository;
    $this->EloquentReparationAmnestyTempGridRepository = $EloquentReparationAmnestyTempGridRepository;
    $this->Carbon = $Carbon;
    $this->Excel = $Excel;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
	}


  //Allegations
  public function getAllegationsRequestGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentReparationAllegationsGridRepository, $post);
  }

  public function getAllegationsRequestTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentReparationAllegationsTempGridRepository, $post);
  }

	public function allegationsProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'institution', 'department', 'allegations'];
    $tableName = 'OADH_CR_Allegations';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array('victim_gender', 'result'), array('year', 'allegations'));
  }

  public function allegationsCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'institution', 'department', 'allegations', 'file_id', 'organization_id'];
    $tableName = 'OADH_CR_Allegations';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function allegationsDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_CR_Allegations');
  }

  public function allegationsDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_CR_Allegations');
  }

  //Budget
  public function getBudgetRequestGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentReparationBudgetGridRepository, $post);
  }

  public function getBudgetRequestTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentReparationBudgetTempGridRepository, $post);
  }

	public function budgetProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'institution', 'budget_unit', 'budget_amount'];
    $tableName = 'OADH_CR_Budget';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year', 'budget_amount'));
  }

  public function budgetCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'institution', 'budget_unit', 'budget_amount', 'file_id', 'organization_id'];
    $tableName = 'OADH_CR_Budget';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function budgetDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_CR_Budget');
  }

  public function budgetDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_CR_Budget');
  }
  
  //FisdlBeneficiaries
  public function getFisdlBeneficiariesRequestGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentReparationFisdlBeneficiariesGridRepository, $post);
  }

  public function getFisdlBeneficiariesRequestTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentReparationFisdlBeneficiariesTempGridRepository, $post);
  }

	public function fisdlBeneficiariesProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'department', 'men_total', 'women_total'];
    $tableName = 'OADH_CR_Fisdl_Beneficiaries';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year', 'men_total', 'women_total'));
  }

  public function fisdlBeneficiariesCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'department', 'men_total', 'women_total', 'file_id', 'organization_id'];
    $tableName = 'OADH_CR_Fisdl_Beneficiaries';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function fisdlBeneficiariesDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_CR_Fisdl_Beneficiaries');
  }

  public function fisdlBeneficiariesDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_CR_Fisdl_Beneficiaries');
  }

  //Amnesty
  public function getAmnestyRequestGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentReparationAmnestyGridRepository, $post);
  }

  public function getAmnestyRequestTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentReparationAmnestyTempGridRepository, $post);
  }

	public function amnestyProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['date', 'summary', 'link'];
    $tableName = 'OADH_CR_Amnesty';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array('link'), array(), array(), array('date'));
  }

  public function amnestyCopyToProduction($input)
  {
    $response = array();
    $columns = ['date', 'summary', 'link', 'file_id', 'organization_id'];
    $tableName = 'OADH_CR_Amnesty';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function amnestyDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_CR_Amnesty');
  }

  public function amnestyDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_CR_Amnesty');
  }

  //tableName
  public function getTableNameGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameGridRepository, $post);
  }

  public function getTableNameTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameTempGridRepository, $post);
  }

	public function tableNameProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['column_1', 'column_2', 'column_3', 'column_4'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function tableNameCopyToProduction($input)
  {
    $response = array();
    $columns = ['column_1', 'column_2', 'column_3', 'column_4', 'file_id', 'organization_id'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function tableNameDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_Prefix_Table_Name');
  }

  public function tableNameDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_Prefix_Table_Name');
  }
}
