<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\MultimediaManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Multimedia\EloquentMultimediaGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Multimedia\MultimediaInterface;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Translation\Translator;
use Illuminate\Config\Repository;
use Illuminate\Cache\CacheManager;

class MultimediaManager extends AbstractLaravelValidator implements MultimediaManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent Grid Repository
	 *
	 * @var Vendor\DecimaModule\Module\Repositories\Multimedia\EloquentMultimediaGridRepository
	 *
	 */
	protected $EloquentMultimediaGridRepository;

  /**
	 *  Module Table Name Interface
	 *
	 * @var Vendor\DecimaModule\Module\Repositories\Multimedia\MultimediaInterface
	 *
	 */
	protected $Multimedia;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    EloquentMultimediaGridRepository $EloquentMultimediaGridRepository,
    MultimediaInterface $Multimedia,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->EloquentMultimediaGridRepository = $EloquentMultimediaGridRepository;
    $this->Multimedia = $Multimedia;
    $this->Carbon = $Carbon;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
    $this->Cache = $Cache;
	}

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentMultimediaGridRepository, $post);
  }

  /**
   * Get search modal table rows
   *
   * @return array
   */
  public function getSearchModalTableRows($organizationId = null, $databaseConnectionName = null, $returnJson = true)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    $rows = $this->Multimedia->searchModalTableRows($organizationId, $databaseConnectionName)->toArray();

    if($returnJson)
    {
      return json_encode($rows);
    }

    return $rows;
  }

  /**
   * Get ...
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getMultimedia($id, $databaseConnectionName = null)
  {
    $Multimedia = $this->Multimedia->byId($id, $databaseConnectionName);

    if(empty($Multimedia))
    {
      return false;
    }

    return $Multimedia;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getMultimedias($organizationId = null, $databaseConnectionName = null, $returnJson = false)
  {
    $multimedias = array();

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(!$this->Cache->has('multimedias' . $organizationId))
    {
      $this->Multimedia->byOrganization($organizationId)->each(function($Multimedia) use (&$multimedias)
      {
        array_push($multimedias, array('label'=> $Multimedia->name , 'value'=>$Multimedia->id));
      });

      $this->Cache->put('multimedias' . $organizationId, json_encode($multimedias), 360);
    }
    else
    {
      $multimedias = json_decode($this->Cache->get('multimedias' . $organizationId), true);
    }

    if($returnJson)
    {
      return json_encode($multimedias);
    }

    return $multimedias;
  }

  public function multimedias($filter = '', $lang = null)
  {
    $data = array();

    // $this->Multimedia->byOrganization(1)->each(function($Multimedia) use(&$data)
    $this->Multimedia->byFilterAndByLang($filter, $lang)->each(function($Multimedia) use(&$data)
    {
      if ($Multimedia->is_highlighted == 1)
      {
        $data['highlighted'][] = array(
          'id' => $Multimedia->id,
          'title' => $Multimedia->title,
          'description' => $Multimedia->description,
          'image_url' => $Multimedia->image_url
        );
      }

      $data['galleries'][] = array(
        'id' => $Multimedia->id,
        'title' => $Multimedia->title,
        'description' => $Multimedia->description,
        'image_url' => $Multimedia->image_url
      );
      
    });

    return $data;
  }

  /**
	 * Create a new ...
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function create(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    unset(
      $input['_token'],
      $input['lang_label'],
      $input['tags_tokenfield']
    );

    $input = eloquent_array_filter_for_insert($input);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

		$input = array_add($input, 'organization_id', $organizationId);
    // $input = array_add($input, 'created_by', $loggedUserId);
    $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    // $input['amount'] = remove_thousands_separator($input['amount']);

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
		{
      // if(!empty($input['is_highlighted']))
      // {
      //   $this->Multimedia->updateMassIsHighlighted($organizationId, $databaseConnectionName);
      // }

      $Multimedia = $this->Multimedia->create($input, $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $Multimedia->id, 'journalized_type' => $this->Multimedia->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::multimedia-management.addedJournal', array('name' => $Multimedia->title)), $Journal));

      $this->Cache->forget('multimediasSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessSaveMessage'),
        'smtRows' => $this->getSearchModalTableRows($organizationId, $databaseConnectionName, false)
      )
    );
  }

  /**
   * Update an existing ...
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function update(array $input, $Multimedia = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $newValues['lang'] = $input['lang_label'];

    unset(
      $input['_token'],
      $input['lang_label'],
      $input['tags_tokenfield']
    );

    $input = eloquent_array_filter_for_update($input);

    if(!empty($input['date']))
    {
      $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      // if(!empty($input['is_highlighted']))
      // {
      //   $this->Multimedia->updateMassIsHighlighted($organizationId, $databaseConnectionName);
      // }

      if(empty($Multimedia))
      {
        $Multimedia = $this->Multimedia->byId($input['id'], $databaseConnectionName);
      }

      $unchangedValues = $Multimedia->toArray();

      $this->Multimedia->update($input, $Multimedia);

      $diff = 0;

      foreach ($input as $key => $value)
      {
        if($unchangedValues[$key] != $value)
        {
          $diff++;

          if($diff == 1)
          {
            $Journal = $this->Journal->create(array('journalized_id' => $Multimedia->id, 'journalized_type' => $this->Multimedia->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
          }

          if($key == 'status')//Para autocomple de estados
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
          }
          else if ($key == 'title')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::publication-management.title'), 'field_lang_key' => 'decima-oadh::publication-management.title', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'description')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::publication-management.description'), 'field_lang_key' => 'decima-oadh::publication-management.description', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'tags')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::publication-management.tags'), 'field_lang_key' => 'decima-oadh::publication-management.tags', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'is_highlighted')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::multimedia-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::multimedia-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
          }
          else if($key == 'date')
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $oldValue = '';
            }

            if(!empty($value))
            {
              $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $newValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
          }
          else if($key == 'table_name_id')//field required
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::multimedia-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::multimedia-management.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
          }
          else if($key == 'table_name_id')//field not required
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
            }
            else
            {
              $oldValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::multimedia-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::multimedia-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
          }
          else if($key == 'lang')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::publication-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::publication-management.' . camel_case($key), 'old_value' => $this->Lang->get('decima-oadh::publication-management.' . $unchangedValues[$key]), 'new_value' => $newValues[$key]), $Journal);
          }
          else
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::multimedia-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::multimedia-management.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
        }
      }

      $this->Cache->forget('multimediasSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessUpdateMessage'),
        'smtRows' => $this->getSearchModalTableRows($organizationId, $databaseConnectionName, false)
      )
    );
  }

  /**
   * Delete an existing ... (soft delete)
   *
   * @param array $input
	 * 	An array as follows: array(id => $id);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */
  public function delete(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      $Multimedia = $this->Multimedia->byId($input['id'], $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $input['id'], 'journalized_type' => $this->Multimedia->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::multimedia-management.deletedJournal', array('name' => $Multimedia->name)), $Journal));

      $this->Multimedia->delete(array($input['id']), $databaseConnectionName);

      $this->Cache->forget('multimediasSmt' . $organizationId);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(
      array(
        'success' => $this->Lang->get('form.defaultSuccessDeleteMessage'),
        'smtRows' => $this->getSearchModalTableRows($organizationId, $databaseConnectionName, false)
      )
    );
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $input
	 * 	An array as follows: array($id0, $id1,…);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */
   public function delete1(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
   {
     $count = 0;

     if(empty($organizationId))
     {
       $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
     }

     if(empty($loggedUserId))
     {
       $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
     }

     $this->beginTransaction($openTransaction, $databaseConnectionName);

     try
     {
       foreach ($input['id'] as $key => $id)
       {
         $count++;

         $Multimedia = $this->Multimedia->byId($id, $databaseConnectionName);

         $Journal = $this->Journal->create(array('journalized_id' => $id, 'journalized_type' => $this->Multimedia->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
         $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::multimedia-management.deletedJournal', array('name' => $Multimedia->name))), $Journal);

         $this->Multimedia->delete(array($id), $databaseConnectionName);
       }

       $this->Cache->forget('multimediasSmt' . $organizationId);

       $this->commit($openTransaction);
     }
     catch (\Exception $e)
     {
       $this->rollBack($openTransaction);

       throw $e;
     }
     catch (\Throwable $e)
     {
       $this->rollBack($openTransaction);

       throw $e;
     }


     if($count == 1)
     {
       return json_encode(
         array(
           'success' => $this->Lang->get('form.defaultSuccessDeleteMessage'),
           'smtRows' => $this->getSearchModalTableRows($organizationId, $databaseConnectionName, false)
         )
       );
     }
     else
     {
       return json_encode(
         array(
           'success' => $this->Lang->get('form.defaultSuccessDeleteMessage1'),
           'smtRows' => $this->getSearchModalTableRows($organizationId, $databaseConnectionName, false)
         )
       );
     }
   }
}
