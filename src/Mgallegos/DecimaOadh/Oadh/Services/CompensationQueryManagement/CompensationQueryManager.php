<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\CompensationQueryManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Compensation\CompensationQueryInterface;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Translation\Translator;
use Illuminate\Config\Repository;
use Illuminate\Cache\CacheManager;

class CompensationQueryManager extends AbstractLaravelValidator implements CompensationQueryManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 *  Module Table Name Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\CompensationQuery\CompensationQueryInterface
	 *
	 */
	protected $CompensationQuery;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

  protected $mapIds;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    CompensationQueryInterface $CompensationQuery,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->CompensationQuery = $CompensationQuery;
    $this->Carbon = $Carbon;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
    $this->Cache = $Cache;

    $this->mapIds = array(
      'sonsonate' => 'SV-SO',
      'santa ana' => 'SV-SA',
      'ahuachapan' => 'SV-Ah',
      'ahuachapán' => 'SV-Ah',
      'chalatenango' => 'SV-CH',
      'la libertad' => 'SV-LI',
      'san salvador' => 'SV-SS',
      'la paz' => 'SV-PA',
      'cuscatlan' => 'SV-CU',
      'cuscatlán' => 'SV-CU',
      'cabañas' => 'SV-CA',
      'san miguel' => 'SV-SM',
      'usulutan' => 'SV-US',
      'usulután' => 'SV-US',
      'morazan' => 'SV-MO',
      'morazán' => 'SV-MO',
      'san vicente' => 'SV-SV',
      'la union' => 'SV-UN',
      'la unión' => 'SV-UN',
    );
	}

  /**
   * Get CompensationQuery
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getCompensationQuery($id, $databaseConnectionName = null)
  {
    $CompensationQuery = $this->CompensationQuery->byId($id, $databaseConnectionName);

    if(empty($CompensationQuery))
    {
      return false;
    }

    return $CompensationQuery;
  }

  /**
   * Get CompensationQuery
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getCompensationQuerys($organizationId = null, $databaseConnectionName = null, $returnJson = false)
  {
    $moduleTableNames = array();

    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    // if(!$this->Cache->has('moduleTableNames' . $organizationId))
    // {
    //   $this->CompensationQuery->byOrganization($organizationId)->each(function($CompensationQuery) use (&$moduleTableNames)
    //   {
    //     array_push($moduleTableNames, array('label'=> $CompensationQuery->name , 'value'=>$CompensationQuery->id));
    //   });
    //
    //   $this->Cache->put('moduleTableNames' . $organizationId, json_encode($moduleTableNames), 360);
    // }
    // else
    // {
    //   $moduleTableNames = json_decode($this->Cache->get('moduleTableNames' . $organizationId), true);
    // }

    $this->CompensationQuery->byOrganization($organizationId, $databaseConnectionName)->each(function($CompensationQuery) use (&$moduleTableNames)
    {
      array_push(
        $moduleTableNames,
        array(
          'label'=> $CompensationQuery->name ,
          'value'=>$CompensationQuery->id
        )
      );
    });

    if($returnJson)
    {
      return json_encode($moduleTableNames);
    }

    return $moduleTableNames;
  }

  public function getMgdtVsFisdl(array $input)
  {
    $data = $dataTemp = $excel = array();

    $excel['header'] = array(
      'A' => 'Años',
      'B' => 'Grupo',
      'C' => 'Presupuesto',
    );

    $this->CompensationQuery->getCompensationYearFilter()->each(function($CompensationQuery) use(&$data)
    {
      $data['year'][] = $CompensationQuery->year;
    });

    $this->CompensationQuery->getInstitutionYearlyBudgetUnit('03. Desarrollo territorial')->each(Function($CompensationQuery) use(&$dataTemp)
    {
      $dataTemp[$CompensationQuery->year]['year'] = $CompensationQuery->year;
      $dataTemp[$CompensationQuery->year]['Veteranos y Ex combatientes'][] = $CompensationQuery->count;
    });

    $this->CompensationQuery->getInstitutionYearlyBudgetUnit('05. Compensación a Personas en Condición de Vulnerabilidad a Consecuencia del Conflicto Armado Interno (01. Veteranos de Guerra)')->each(Function($CompensationQuery) use(&$dataTemp)
    {
      $dataTemp[$CompensationQuery->year]['year'] = $CompensationQuery->year;
      $dataTemp[$CompensationQuery->year]['Veteranos y Ex combatientes'][] = $CompensationQuery->count;
    });

    $this->CompensationQuery->getInstitutionYearlyBudgetUnit('05. Compensación a Personas en Condición de Vulnerabilidad a Consecuencia del Conflicto Armado Interno (02. Indemnización a Víctimas de Graves Violaciones)')->each(Function($CompensationQuery) use(&$dataTemp)
    {
      $dataTemp[$CompensationQuery->year]['year'] = $CompensationQuery->year;
      $dataTemp[$CompensationQuery->year]['Víctimas de Graves Violaciones'][] = $CompensationQuery->count;
    });

    foreach ($data['year'] as $key => $value) 
    {
      $dataTemp[$value]['Veteranos y Ex combatientes']['count'] = array_sum($dataTemp[$value]['Veteranos y Ex combatientes']);
      $dataTemp[$value]['Víctimas de Graves Violaciones']['count'] = array_sum($dataTemp[$value]['Víctimas de Graves Violaciones']);
    }

    foreach ($dataTemp as $key => $row)
    {
      $data['data'][] = array(
        'year' => $row['year'],
        'column_a' => (array_key_exists('Veteranos y Ex combatientes', $row)) ? $row['Veteranos y Ex combatientes']['count'] : '0',
        'column_b' => (array_key_exists('Víctimas de Graves Violaciones', $row)) ? $row['Víctimas de Graves Violaciones']['count'] : '0',
      );

      $excel['details']['A'][] = $row['year'];
      $excel['details']['A'][] = $row['year'];
      $excel['details']['B'][] = 'Veteranos y Ex combatientes';
      $excel['details']['B'][] = 'Víctimas de Graves Violaciones';
      $excel['details']['C'][] = (array_key_exists('Veteranos y Ex combatientes', $row)) ? $row['Veteranos y Ex combatientes']['count'] : '0';
      $excel['details']['C'][] = (array_key_exists('Víctimas de Graves Violaciones', $row)) ? $row['Víctimas de Graves Violaciones']['count'] : '0';
    }

    $data['labels']['categoryAxis'] = 'Año';
    $data['labels']['valueAxis'] = 'Presupuesto';
    $data['labels']['column_a'] = 'Veteranos y Ex combatientes';
    $data['labels']['column_b'] = 'Víctimas de Graves Violaciones';

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getFisdlBeneficiaries(array $input)
  {
    $data = $year = $excel = array();

    $excel['header'] = array(
      'A' => 'Departamento',
      'B' => 'Beneficiarios',
    );

    $this->CompensationQuery->getFisdlYearFilter()->each(function ($CompensationQuery) use(&$data)
    {
      $data['year'][] = $CompensationQuery->year;
    });

    $minYear = min($data['year']);
    $maxYear = max($data['year']);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $year['yearFrom'] = array($minYear);
      $year['yearTo'] = array($maxYear);
    }
    else
    {
      $year['yearFrom'] = $input['filter']['yearFrom'];
      $year['yearTo'] = $input['filter']['yearTo'];
    }

    unset($input['filter']['yearFrom'], $input['filter']['yearTo']);

    $this->CompensationQuery->getFisdlBeneficiariesByYear($year['yearFrom'], $year['yearTo'])->each(Function($CompensationQuery) use(&$data, &$excel)
    {
      $departmentName = rtrim(ltrim($CompensationQuery->department));

      $data['data'][] = array(
        'id' => isset($this->mapIds[mb_strtolower($departmentName)]) ?  $this->mapIds[mb_strtolower($departmentName)] : '',
        'value' => (int) $CompensationQuery->count,
        'department' => $departmentName,
        'legend' => ' Beneficiarios'
      );

      $excel['details']['A'][] = mb_strtoupper($departmentName);
      $excel['details']['B'][] = (int) $CompensationQuery->count;
    });

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getAllegationsByInstitution(array $input, $institution)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' => 'Departamento',
      'B' => 'Denuncias',
    );

    $this->CompensationQuery->getAllegationsYearFilter()->each(function ($CompensationQuery) use(&$data)
    {
      $data['year'][] = $CompensationQuery->year;
    });

    if(empty($input['filter']['year']))
    {
      $input['filter']['year'] = array($data['year'][count($data['year']) - 1]);
    }

    $this->CompensationQuery->getAllegationsByInstitution($input['filter'], $institution)->each(Function($CompensationQuery) use(&$data, &$excel)
    {
      $departmentName = rtrim(ltrim($CompensationQuery->department));

      $data['data'][] = array(
        'id' => isset($this->mapIds[mb_strtolower($departmentName)]) ?  $this->mapIds[mb_strtolower($departmentName)] : '',
        'value' => (int) $CompensationQuery->count,
        'department' => $departmentName,
        'legend' => ' Denuncias'
      );

      $excel['details']['A'][] = mb_strtoupper($departmentName);
      $excel['details']['B'][] = (int) $CompensationQuery->count;
    });

    if(empty($excel['details']))
    {
      $excel['details']['A'][] = 'No hay datos';
      $excel['details']['B'][] = 'No hay datos';
    }

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getVeteransExCombatantsByInstitution(array $input, $institution)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' => 'Unidad presupuestaria',
      'B' => 'Presupuesto',
    );

    $this->CompensationQuery->getCompensationYearFilterByInstitution($institution)->each(function ($CompensationQuery) use(&$data)
    {
      $data['year'][] = $CompensationQuery->year;
    });

    if(empty($input['filter']['year']))
    {
      $input['filter']['year'] = array($data['year'][count($data['year']) - 1]);
    }

    $this->CompensationQuery->getVeteransExCombatantsBudgetByInstitution($input['filter'], $institution)->each(function ($CompensationQuery) use (&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $CompensationQuery->budget_unit,
        'value' => $CompensationQuery->budget_amount
      );

      $excel['details']['A'][] = $CompensationQuery->budget_unit;
      $excel['details']['B'][] = $CompensationQuery->budget_amount;
    });
    
    $data['labels']['categoryAxis'] = 'Unidad presupuestaria';
    $data['labels']['valueAxis'] = 'Presupuesto';

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getVeteransVsVictims(array $input)
  {
    $data = $dataTemp = $excel = array();

    $excel['header'] = array(
      'A' => 'Unidad presupuestaria',
      'B' => 'Presupuesto',
    );

    $this->CompensationQuery->getCompensationYearFilter()->each(function ($CompensationQuery) use(&$data)
    {
      $data['year'][] = $CompensationQuery->year;
    });

    if(empty($input['filter']['year']))
    {
      $input['filter']['year'] = array($data['year'][count($data['year']) - 1]);
    }

    $this->CompensationQuery->getVeteransExCombatantsByUnitBudgetSum($input['filter'], '03. Desarrollo territorial (03. Atención a veteranos y Excombatientes)')->each(function ($CompensationQuery) use (&$dataTemp)
    {
      $dataTemp['Veteranos y excombatientes'][] = $CompensationQuery->budget_amount;
    });

    $this->CompensationQuery->getVeteransExCombatantsByUnitBudgetSum($input['filter'], '03. Desarrollo territorial (04. Financiamiento para la Estabilización y Fomento Económico para la Atención de Veteranos y Excombatientes)')->each(function ($CompensationQuery) use (&$dataTemp)
    {
      $dataTemp['Veteranos y excombatientes'][] = $CompensationQuery->budget_amount;
    });

    $this->CompensationQuery->getVeteransExCombatantsByUnitBudgetSum($input['filter'], '05. Compensación a Personas en Condición de Vulnerabilidad a Consecuencia del Conflicto Armado Interno (01. Veteranos de Guerra)')->each(function ($CompensationQuery) use (&$dataTemp)
    {
      $dataTemp['Veteranos y excombatientes'][] =$CompensationQuery->budget_amount;
    });

    $this->CompensationQuery->getVeteransExCombatantsByUnitBudgetSum($input['filter'], '05. Compensación a Personas en Condición de Vulnerabilidad a Consecuencia del Conflicto Armado Interno (02. Indemnización a Víctimas de Graves Violaciones)')->each(function ($CompensationQuery) use (&$dataTemp)
    {
      $dataTemp['Víctimas de graves violaciones'][] = $CompensationQuery->budget_amount;
    });

    $dataTemp['Veteranos y excombatientes'] = array_sum($dataTemp['Veteranos y excombatientes']);
    $dataTemp['Víctimas de graves violaciones'] = array_sum($dataTemp['Víctimas de graves violaciones']);

    foreach ($dataTemp as $key => $value) 
    {
      $data['data'][] = array(
        'label' => $key,
        'value' => $value
      );

      $excel['details']['A'][] = $key;
      $excel['details']['B'][] = $value;
    }

    $data['labels']['categoryAxis'] = 'Unidad presupuestaria';
    $data['labels']['valueAxis'] = 'Presupuesto';

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getNationalSearchComission(array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'A' => 'Unidad presupuestaria',
      'B' => 'Presupuesto',
    );

    $this->CompensationQuery->getCompensationYearFilter()->each(function ($CompensationQuery) use(&$data)
    {
      $data['year'][] = $CompensationQuery->year;
    });

    if(empty($input['filter']['year']))
    {
      $input['filter']['year'] = array($data['year'][count($data['year']) - 1]);
    }

    $this->CompensationQuery->getVeteransExCombatantsBudgetByInstitution($input['filter'], 'RREE (CNB)')->each(function ($CompensationQuery) use (&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $CompensationQuery->budget_unit,
        'value' => $CompensationQuery->budget_amount,

      );
      $excel['details']['A'][] = $CompensationQuery->budget_unit;
      $excel['details']['B'][] = $CompensationQuery->budget_amount;
    });

    $this->CompensationQuery->getVeteransExCombatantsBudgetByInstitution($input['filter'], 'RREE (CONABÚSQUEDA)')->each(function ($CompensationQuery) use (&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $CompensationQuery->budget_unit,
        'value' => $CompensationQuery->budget_amount,

      );
      $excel['details']['A'][] = $CompensationQuery->budget_unit;
      $excel['details']['B'][] = $CompensationQuery->budget_amount;
    });

    $data['labels']['categoryAxis'] = 'Unidad presupuestaria';
    $data['labels']['valueAxis'] = 'Presupuesto';

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getTimeline()
  {
    $data = array();
    $size = array();
    $months = $this->getMonthNames();
    //query de los años
    $this->CompensationQuery->getExtractedYearFromDate()->each(function($CompensationQuery) use(&$data, &$size, $months)
    {
      $stringMonth = $months[$CompensationQuery->month - 1];
      $firstDate = $this->Carbon->create($CompensationQuery->year, $CompensationQuery->month)->startOfMonth()->format('Y-m-d');
      $lastDate = $this->Carbon->create($CompensationQuery->year, $CompensationQuery->month)->endOfMonth()->format('Y-m-d');
      

      $this->CompensationQuery->getAmnestyDataByStartAndEndOfDate($firstDate, $lastDate)->each(function($NewsQuery) use($CompensationQuery, $stringMonth, &$data)
      {
        $data[$CompensationQuery->year][$CompensationQuery->month][] = array(
          'month' => $stringMonth,
          'date' => $NewsQuery->date, 
          'link' => $NewsQuery->link, 
          'summary' => $NewsQuery->summary
        );

        
      });

      $size[$CompensationQuery->year][] = count($data[$CompensationQuery->year][$CompensationQuery->month]);

    });

    foreach ($size as $key => $value) 
    {
      $size[$key] = array_sum($value);
    }
    

    return json_encode(array('success' => 'success', 'data' => $data, 'sizes' => $size));
  }

  public function getMonthNames()
  {
    return array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
  }

}
