<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\FreedomUploaderManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;

use Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface;

use App\Kwaai\Security\Repositories\Journal\JournalInterface;

use Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface;

use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeHabeasCorpusRequestGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeHabeasCorpusRequestTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreePeopleDetainedGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreePeopleDetainedTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeCriminalCasesTrialsGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeCriminalCasesTrialsTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeMassTrialsGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeMassTrialsTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeProceduralFraudGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeProceduralFraudTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeCaptureOrdersGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeCaptureOrdersTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeDisappearancesVictimsGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeDisappearancesVictimsTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeAccusedLibertyDeprivationGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeAccusedLibertyDeprivationTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeVictimsLibertyDeprivationGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeVictimsLibertyDeprivationTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeForcedDisappearancesGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeForcedDisappearancesTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeJailsOccupationGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeJailsOccupationTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeDeprivedPersonsGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeDeprivedPersonsTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeAcuteDiseasesGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeAcuteDiseasesTempGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeChronicDiseasesGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\Freedom\EloquentFreeChronicDiseasesTempGridRepository;

use Carbon\Carbon;

use Maatwebsite\Excel\Excel;

use Illuminate\Config\Repository;

use Illuminate\Translation\Translator;

use Illuminate\Database\DatabaseManager;

class FreedomUploaderManager extends AbstractLaravelValidator implements FreedomUploaderManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * File Management Interface
  *
  * @var Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface
  *
  */
  protected $FileManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $UploaderManager;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent HabeasCorpusRequest Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\HabeasCorpusRequest\EloquentHabeasCorpusRequestGridRepository
	 *
	 */
	protected $EloquentHabeasCorpusRequestGridRepository;

  /**
	 * Eloquent HabeasCorpusRequest Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\HabeasCorpusRequest\EloquentHabeasCorpusRequestGridRepository
	 *
	 */
	protected $EloquentHabeasCorpusRequestTempGridRepository;

  /**
	 * Eloquent PeopleDetained Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\PeopleDetained\EloquentPeopleDetainedGridRepository
	 *
	 */
	protected $EloquentPeopleDetainedGridRepository;

  /**
	 * Eloquent PeopleDetained Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\PeopleDetained\EloquentPeopleDetainedGridRepository
	 *
	 */
	protected $EloquentPeopleDetainedTempGridRepository;


  /**
	 * Eloquent CriminalCasesTrials Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\CriminalCasesTrials\EloquentCriminalCasesTrialsGridRepository
	 *
	 */
	protected $EloquentCriminalCasesTrialsGridRepository;

  /**
	 * Eloquent CriminalCasesTrials Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\CriminalCasesTrials\EloquentCriminalCasesTrialsGridRepository
	 *
	 */
	protected $EloquentCriminalCasesTrialsTempGridRepository;

  /**
	 * Eloquent MassTrials Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\MassTrials\EloquentMassTrialsGridRepository
	 *
	 */
	protected $EloquentMassTrialsGridRepository;

  /**
	 * Eloquent MassTrials Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\MassTrials\EloquentMassTrialsGridRepository
	 *
	 */
	protected $EloquentMassTrialsTempGridRepository;

  /**
	 * Eloquent ProceduralFraud Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ProceduralFraud\EloquentProceduralFraudGridRepository
	 *
	 */
	protected $EloquentProceduralFraudGridRepository;

  /**
	 * Eloquent ProceduralFraud Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ProceduralFraud\EloquentProceduralFraudGridRepository
	 *
	 */
	protected $EloquentProceduralFraudTempGridRepository;

  /**
	 * Eloquent Capture Orders Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ProceduralFraud\EloquentCaptureOrdersGridRepository
	 *
	 */
	protected $EloquentCaptureOrdersGridRepository;

  /**
	 * Eloquent CaptureOrders Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\CaptureOrders\EloquentCaptureOrdersGridRepository
	 *
	 */
	protected $EloquentCaptureOrdersTempGridRepository;

  /**
	 * Eloquent DisappearancesVictims Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\DisappearancesVictims\EloquentDisappearancesVictimsGridRepository
	 *
	 */
	protected $EloquentDisappearancesVictimsGridRepository;

  /**
	 * Eloquent DisappearancesVictims Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\DisappearancesVictims\EloquentDisappearancesVictimsGridRepository
	 *
	 */
	protected $EloquentDisappearancesVictimsTempGridRepository;

  /**
	 * Eloquent AccusedLibertyDeprivation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AccusedLibertyDeprivation\EloquentAccusedLibertyDeprivationGridRepository
	 *
	 */
	protected $EloquentAccusedLibertyDeprivationGridRepository;

  /**
	 * Eloquent AccusedLibertyDeprivation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AccusedLibertyDeprivation\EloquentAccusedLibertyDeprivationGridRepository
	 *
	 */
	protected $EloquentAccusedLibertyDeprivationTempGridRepository;

  /**
	 * Eloquent VictimsLibertyDeprivation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\VictimsLibertyDeprivation\EloquentVictimsLibertyDeprivationGridRepository
	 *
	 */
	protected $EloquentVictimsLibertyDeprivationGridRepository;

  /**
	 * Eloquent VictimsLibertyDeprivation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\VictimsLibertyDeprivation\EloquentVictimsLibertyDeprivationGridRepository
	 *
	 */
	protected $EloquentVictimsLibertyDeprivationTempGridRepository;

  /**
	 * Eloquent ForcedDisappearances Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ForcedDisappearances\EloquentForcedDisappearancesGridRepository
	 *
	 */
	protected $EloquentForcedDisappearancesGridRepository;

  /**
	 * Eloquent ForcedDisappearances Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ForcedDisappearances\EloquentForcedDisappearancesGridRepository
	 *
	 */
	protected $EloquentForcedDisappearancesTempGridRepository;

  /**
	 * Eloquent JailsOccupation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\JailsOccupation\EloquentJailsOccupationGridRepository
	 *
	 */
	protected $EloquentJailsOccupationGridRepository;

  /**
	 * Eloquent JailsOccupation Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\JailsOccupation\EloquentJailsOccupationGridRepository
	 *
	 */
	protected $EloquentJailsOccupationTempGridRepository;

  /**
	 * Eloquent DeprivedPersons Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\DeprivedPersons\EloquentDeprivedPersonsGridRepository
	 *
	 */
	protected $EloquentDeprivedPersonsGridRepository;

  /**
	 * Eloquent DeprivedPersons Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\DeprivedPersons\EloquentDeprivedPersonsGridRepository
	 *
	 */
	protected $EloquentDeprivedPersonsTempGridRepository;

  /**
	 * Eloquent AcuteDeseases Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AcuteDeseases\EloquentAcuteDeseasesGridRepository
	 *
	 */
	protected $EloquentAcuteDiseasesGridRepository;

  /**
	 * Eloquent AcuteDeseases Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\AcuteDeseases\EloquentAcuteDeseasesGridRepository
	 *
	 */
	protected $EloquentAcuteDiseasesTempGridRepository;

  /**
	 * Eloquent ChronicDiseases Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ChronicDiseases\EloquentChronicDiseasesGridRepository
	 *
	 */
	protected $EloquentChronicDiseasesGridRepository;

  /**
	 * Eloquent ChronicDiseases Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ChronicDiseases\EloquentChronicDiseasesGridRepository
	 *
	 */
	protected $EloquentChronicDiseasesTempGridRepository;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
  * Maatwebsite\Excel\Excel
  *
  * @var Excel
  *
  */
  protected $Excel;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    FileManagementInterface $FileManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    UploaderManagementInterface $UploaderManager,
    EloquentFreeHabeasCorpusRequestGridRepository $EloquentHabeasCorpusRequestGridRepository,
    EloquentFreeHabeasCorpusRequestTempGridRepository $EloquentHabeasCorpusRequestTempGridRepository,
    EloquentFreePeopleDetainedGridRepository $EloquentPeopleDetainedGridRepository,
    EloquentFreePeopleDetainedTempGridRepository $EloquentPeopleDetainedTempGridRepository,
    EloquentFreeCriminalCasesTrialsGridRepository $EloquentCriminalCasesTrialsGridRepository,
    EloquentFreeCriminalCasesTrialsTempGridRepository $EloquentCriminalCasesTrialsTempGridRepository,
    EloquentFreeMassTrialsGridRepository $EloquentMassTrialsGridRepository,
    EloquentFreeMassTrialsTempGridRepository $EloquentMassTrialsTempGridRepository,
    EloquentFreeProceduralFraudGridRepository $EloquentProceduralFraudGridRepository,
    EloquentFreeProceduralFraudTempGridRepository $EloquentProceduralFraudTempGridRepository,
    EloquentFreeCaptureOrdersGridRepository $EloquentCaptureOrdersGridRepository,
    EloquentFreeCaptureOrdersTempGridRepository $EloquentCaptureOrdersTempGridRepository,
    EloquentFreeDisappearancesVictimsGridRepository $EloquentDisappearancesVictimsGridRepository,
    EloquentFreeDisappearancesVictimsTempGridRepository $EloquentDisappearancesVictimsTempGridRepository,
    EloquentFreeAccusedLibertyDeprivationGridRepository $EloquentAccusedLibertyDeprivationGridRepository,
    EloquentFreeAccusedLibertyDeprivationTempGridRepository $EloquentAccusedLibertyDeprivationTempGridRepository,
    EloquentFreeVictimsLibertyDeprivationGridRepository $EloquentVictimsLibertyDeprivationGridRepository,
    EloquentFreeVictimsLibertyDeprivationTempGridRepository $EloquentVictimsLibertyDeprivationTempGridRepository,
    EloquentFreeForcedDisappearancesGridRepository $EloquentForcedDisappearancesGridRepository,
    EloquentFreeForcedDisappearancesTempGridRepository $EloquentForcedDisappearancesTempGridRepository,
    EloquentFreeJailsOccupationGridRepository $EloquentJailsOccupationGridRepository,
    EloquentFreeJailsOccupationTempGridRepository $EloquentJailsOccupationTempGridRepository,
    EloquentFreeDeprivedPersonsGridRepository $EloquentDeprivedPersonsGridRepository,
    EloquentFreeDeprivedPersonsTempGridRepository $EloquentDeprivedPersonsTempGridRepository,
    EloquentFreeAcuteDiseasesGridRepository $EloquentAcuteDiseasesGridRepository,
    EloquentFreeAcuteDiseasesTempGridRepository $EloquentAcuteDiseasesTempGridRepository,
    EloquentFreeChronicDiseasesGridRepository $EloquentChronicDiseasesGridRepository,
    EloquentFreeChronicDiseasesTempGridRepository $EloquentChronicDiseasesTempGridRepository,

    Carbon $Carbon,
    Excel $Excel,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->FileManager = $FileManager;

    $this->Journal = $Journal;

    $this->GridEncoder = $GridEncoder;

    $this->UploaderManager = $UploaderManager;

    $this->EloquentHabeasCorpusRequestGridRepository = $EloquentHabeasCorpusRequestGridRepository;

    $this->EloquentHabeasCorpusRequestTempGridRepository = $EloquentHabeasCorpusRequestTempGridRepository;

    $this->EloquentPeopleDetainedGridRepository = $EloquentPeopleDetainedGridRepository;

    $this->EloquentPeopleDetainedTempGridRepository = $EloquentPeopleDetainedTempGridRepository;

    $this->EloquentCriminalCasesTrialsGridRepository = $EloquentCriminalCasesTrialsGridRepository;

    $this->EloquentCriminalCasesTrialsTempGridRepository = $EloquentCriminalCasesTrialsTempGridRepository;

    $this->EloquentMassTrialsGridRepository = $EloquentMassTrialsGridRepository;

    $this->EloquentMassTrialsTempGridRepository = $EloquentMassTrialsTempGridRepository;

    $this->EloquentProceduralFraudGridRepository = $EloquentProceduralFraudGridRepository;

    $this->EloquentProceduralFraudTempGridRepository = $EloquentProceduralFraudTempGridRepository;

    $this->EloquentCaptureOrdersGridRepository = $EloquentCaptureOrdersGridRepository;

    $this->EloquentCaptureOrdersTempGridRepository = $EloquentCaptureOrdersTempGridRepository;

    $this->EloquentDisappearancesVictimsGridRepository = $EloquentDisappearancesVictimsGridRepository;

    $this->EloquentDisappearancesVictimsTempGridRepository = $EloquentDisappearancesVictimsTempGridRepository;

    $this->EloquentAccusedLibertyDeprivationGridRepository = $EloquentAccusedLibertyDeprivationGridRepository;

    $this->EloquentAccusedLibertyDeprivationTempGridRepository = $EloquentAccusedLibertyDeprivationTempGridRepository;

    $this->EloquentVictimsLibertyDeprivationGridRepository = $EloquentVictimsLibertyDeprivationGridRepository;

    $this->EloquentVictimsLibertyDeprivationTempGridRepository = $EloquentVictimsLibertyDeprivationTempGridRepository;

    $this->EloquentForcedDisappearancesGridRepository = $EloquentForcedDisappearancesGridRepository;

    $this->EloquentForcedDisappearancesTempGridRepository = $EloquentForcedDisappearancesTempGridRepository;

    $this->EloquentJailsOccupationGridRepository = $EloquentJailsOccupationGridRepository;

    $this->EloquentJailsOccupationTempGridRepository = $EloquentJailsOccupationTempGridRepository;

    $this->EloquentDeprivedPersonsGridRepository = $EloquentDeprivedPersonsGridRepository;

    $this->EloquentDeprivedPersonsTempGridRepository = $EloquentDeprivedPersonsTempGridRepository;

    $this->EloquentAcuteDiseasesGridRepository = $EloquentAcuteDiseasesGridRepository;

    $this->EloquentAcuteDiseasesTempGridRepository = $EloquentAcuteDiseasesTempGridRepository;

    $this->EloquentChronicDiseasesGridRepository = $EloquentChronicDiseasesGridRepository;

    $this->EloquentChronicDiseasesTempGridRepository = $EloquentChronicDiseasesTempGridRepository;

    $this->Carbon = $Carbon;

    $this->Excel = $Excel;

    $this->DB = $DB;

		$this->Lang = $Lang;

		$this->Config = $Config;
	}


  //Habeas Corpus Request
  public function gethabeasCorpusRequestGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentHabeasCorpusRequestGridRepository, $post);
  }

  public function gethabeasCorpusRequestTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentHabeasCorpusRequestTempGridRepository, $post);
  }

	public function habeasCorpusRequestProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'court', 'sex'];
    $tableName = 'OADH_FREE_Habeas_Corpus_Request';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function habeasCorpusRequestCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'court', 'sex', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Habeas_Corpus_Request';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function habeasCorpusRequestDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Habeas_Corpus_Request');
  }

  public function habeasCorpusRequestDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Habeas_Corpus_Request');
  }


  //People Detained
  public function getpeopleDetainedGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentPeopleDetainedGridRepository, $post);
  }

  public function getpeopleDetainedTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentPeopleDetainedTempGridRepository, $post);
  }

	public function peopleDetainedProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['state', 'year', 'departament'];
    $tableName = 'OADH_FREE_People_Detained';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function peopleDetainedCopyToProduction($input)
  {
    $response = array();
    $columns = ['state', 'year', 'departament', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_People_Detained';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function peopleDetainedDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_People_Detained');
  }

  public function peopleDetainedDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_People_Detained');
  }

  //Criminal Cases Trials
  public function getcriminalCasesTrialsGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentCriminalCasesTrialsGridRepository, $post);
  }

  public function getcriminalCasesTrialsTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentCriminalCasesTrialsTempGridRepository, $post);
  }

	public function criminalCasesTrialsProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'phase', 'court'];
    $tableName = 'OADH_FREE_Criminal_Cases_Trials';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function criminalCasesTrialsCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'phase', 'court', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Criminal_Cases_Trials';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function criminalCasesTrialsDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Criminal_Cases_Trials');
  }

  public function criminalCasesTrialsDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Criminal_Cases_Trials');
  }

  //Mass Trials
  public function getmassTrialsGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentMassTrialsGridRepository, $post);
  }

  public function getmassTrialsTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentMassTrialsTempGridRepository, $post);
  }

	public function massTrialsProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'sex', 'court'];
    $tableName = 'OADH_FREE_Mass_Trials';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function massTrialsCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'sex', 'court', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Mass_Trials';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function massTrialsDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Mass_Trials');
  }

  public function massTrialsDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Mass_Trials');
  }

  //Procedural Fraud
  public function getproceduralFraudGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentProceduralFraudGridRepository, $post);
  }

  public function getproceduralFraudTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentProceduralFraudTempGridRepository, $post);
  }

	public function proceduralFraudProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['gender', 'crime', 'year', 'departament', 'municipality'];
    $tableName = 'OADH_FREE_Procedural_Fraud';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function proceduralFraudCopyToProduction($input)
  {
    $response = array();
    $columns = ['gender', 'crime', 'year', 'departament', 'municipality', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Procedural_Fraud';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function proceduralFraudDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Procedural_Fraud');
  }

  public function proceduralFraudDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Procedural_Fraud');
  }

  //Procedural Fraud
  public function getcaptureOrdersGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentCaptureOrdersGridRepository, $post);
  }

  public function getcaptureOrdersTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentCaptureOrdersTempGridRepository, $post);
  }

	public function captureOrdersProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['departament', 'municipality', 'year'];
    $tableName = 'OADH_FREE_Capture_Orders';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function captureOrdersCopyToProduction($input)
  {
    $response = array();
    $columns = ['departament', 'municipality', 'year', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Capture_Orders';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function captureOrdersDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Capture_Orders');
  }

  public function captureOrdersDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Capture_Orders');
  }

  //Procedural Fraud
  public function getDisappearancesVictimsGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentDisappearancesVictimsGridRepository, $post);
  }

  public function getDisappearancesVictimsTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentDisappearancesVictimsTempGridRepository, $post);
  }

	public function disappearancesVictimsProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['departament', 'municipality', 'year'];
    $tableName = 'OADH_FREE_Disappearances_Victims';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function disappearancesVictimsCopyToProduction($input)
  {
    $response = array();
    $columns = ['departament', 'municipality', 'year', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Disappearances_Victims';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function disappearancesVictimsDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Disappearances_Victims');
  }

  public function disappearancesVictimsDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Disappearances_Victims');
  }

  //Accused Lilberty Deprivation
  public function getaccusedLibertyDeprivationGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAccusedLibertyDeprivationGridRepository, $post);
  }

  public function getaccusedLibertyDeprivationTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAccusedLibertyDeprivationTempGridRepository, $post);
  }

	public function accusedLibertyDeprivationProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'departament', 'municipality', 'profession', 'gender'];
    $tableName = 'OADH_FREE_Accused_Liberty_Deprivation';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function accusedLibertyDeprivationCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'departament', 'municipality', 'profession', 'gender', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Accused_Liberty_Deprivation';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function accusedLibertyDeprivationDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Accused_Liberty_Deprivation');
  }

  public function accusedLibertyDeprivationDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Accused_Liberty_Deprivation');
  }

  //Victim Liberty Deprivation
  public function getvictimLibertyDeprivationGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentVictimsLibertyDeprivationGridRepository, $post);
  }

  public function getvictimLibertyDeprivationTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentVictimsLibertyDeprivationTempGridRepository, $post);
  }

	public function victimLibertyDeprivationProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['departament', 'municipality', 'year'];
    $tableName = 'OADH_FREE_Victims_Liberty_Deprivation';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function victimLibertyDeprivationCopyToProduction($input)
  {
    $response = array();
    $columns = ['departament', 'municipality', 'year', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Victims_Liberty_Deprivation';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function victimLibertyDeprivationDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Victims_Liberty_Deprivation');
  }

  public function victimLibertyDeprivationDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Victims_Liberty_Deprivation');
  }

  //Forced Disappearances
  public function getforcedDisappearancesGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentForcedDisappearancesGridRepository, $post);
  }

  public function getforcedDisappearancesTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentForcedDisappearancesTempGridRepository, $post);
  }

	public function forcedDisappearancesProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'gender', 'departament', 'municipality', 'profession', 'crime'];
    $tableName = 'OADH_FREE_Forced_Disappearances';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function forcedDisappearancesCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'gender', 'departament', 'municipality', 'profession', 'crime', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Forced_Disappearances';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function forcedDisappearancesDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Forced_Disappearances');
  }

  public function forcedDisappearancesDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Forced_Disappearances');
  }

  //Jails Occupation
  public function getjailsOccupationGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentJailsOccupationGridRepository, $post);
  }

  public function getjailsOccupationTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentJailsOccupationTempGridRepository, $post);
  }

	public function jailsOccupationProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['departament', 'station', 'unit', 'year', 'capacity', 'man_population', 'women_population'];
    $tableName = 'OADH_FREE_Jails_Occupation';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function jailsOccupationCopyToProduction($input)
  {
    $response = array();
    $columns = ['departament', 'station', 'unit', 'year', 'capacity', 'man_population', 'women_population', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Jails_Occupation';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function jailsOccupationDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Jails_Occupation');
  }

  public function jailsOccupationDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Jails_Occupation');
  }

  //Deprived Persons
  public function getdeprivedPersonsGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentDeprivedPersonsGridRepository, $post);
  }

  public function getdeprivedPersonsTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentDeprivedPersonsTempGridRepository, $post);
  }

	public function deprivedPersonsProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'right_to', 'violated_fact', 'sex', 'age', 'departament', 'municipality'];
    $tableName = 'OADH_FREE_Deprived_Persons';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('age', 'year'));
  }

  public function deprivedPersonsCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'right_to', 'violated_fact', 'sex', 'age', 'departament', 'municipality', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Deprived_Persons';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function deprivedPersonsDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Deprived_Persons');
  }

  public function deprivedPersonsDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Deprived_Persons');
  }

  //Acute Disease
  public function getacuteDiseaseGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAcuteDiseasesGridRepository, $post);
  }

  public function getacuteDiseaseTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentAcuteDiseasesTempGridRepository, $post);
  }

	public function acuteDiseaseProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'disease', 'quantity'];
    $tableName = 'OADH_FREE_Acute_Diseases';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function acuteDiseaseCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'disease', 'quantity', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Acute_Diseases';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function acuteDiseaseDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Acute_Diseases');
  }

  public function acuteDiseaseDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Acute_Diseases');
  }

  //Chronic Diseases
  public function getchronicDiseasesGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentChronicDiseasesGridRepository, $post);
  }

  public function getchronicDiseasesTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentChronicDiseasesTempGridRepository, $post);
  }

	public function chronicDiseasesProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['chronic_disease', 'sex', 'age', 'jail', 'year'];
    $tableName = 'OADH_FREE_Chronic_Diseases';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('age', 'year'));
  }

  public function chronicDiseasesCopyToProduction($input)
  {
    $response = array();
    $columns = ['chronic_disease', 'sex', 'age', 'jail', 'year', 'file_id', 'organization_id'];
    $tableName = 'OADH_FREE_Chronic_Diseases';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function chronicDiseasesDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_FREE_Chronic_Diseases');
  }

  public function chronicDiseasesDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_FREE_Chronic_Diseases');
  }

  //tableName
  public function getTableNameGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameGridRepository, $post);
  }

  public function getTableNameTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameTempGridRepository, $post);
  }

	public function tableNameProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['column_1', 'column_2', 'column_3', 'column_4'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function tableNameCopyToProduction($input)
  {
    $response = array();
    $columns = ['column_1', 'column_2', 'column_3', 'column_4', 'file_id', 'organization_id'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function tableNameDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_Prefix_Table_Name');
  }

  public function tableNameDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_Prefix_Table_Name');
  }
}
