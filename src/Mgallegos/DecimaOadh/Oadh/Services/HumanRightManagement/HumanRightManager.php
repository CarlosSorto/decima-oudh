<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\HumanRightManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\EloquentHumanRightGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\HumanRightInterface;
use Carbon\Carbon;
use Illuminate\Config\Repository;
use Illuminate\Translation\Translator;
use Illuminate\Database\DatabaseManager;

class HumanRightManager extends AbstractLaravelValidator implements HumanRightManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent HumanRight Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\EloquentHumanRightGridRepository
	 *
	 */
	protected $EloquentHumanRightGridRepository;

  /**
	 *  HumanRight Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\HumanRightInterface
	 *
	 */
	protected $HumanRight;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    EloquentHumanRightGridRepository $EloquentHumanRightGridRepository,
    HumanRightInterface $HumanRight,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->Journal = $Journal;

    $this->GridEncoder = $GridEncoder;

    $this->EloquentHumanRightGridRepository = $EloquentHumanRightGridRepository;

    $this->HumanRight = $HumanRight;

    $this->Carbon = $Carbon;

    $this->DB = $DB;

		$this->Lang = $Lang;

		$this->Config = $Config;
	}

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentHumanRightGridRepository, $post);
  }


  /**
   * Get ...
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getHumanRight($id, $databaseConnectionName = null)
  {
    $HumanRight = $this->HumanRight->byId($id, $databaseConnectionName);

    if(empty($HumanRight))
    {
      return false;
    }

    return $HumanRight;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getHumanRights()
  {
    //Acumulador de todo
    $Data = array();

    $this->HumanRight->byOrganizationAndParent(null, $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($Tracing) use (&$Data) {
      $HumanRights = array();

      $this->HumanRight->byOrganizationAndParent($Tracing->id, $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($Right) use (&$HumanRights, &$Tracing) {

        $Topic = array();
        $ViolatedFact = array();

        $this->HumanRight->byOrganizationAndParent($Right->id, $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($TopicOrViolated) use (&$Topic, &$ViolatedFact) {

          if($TopicOrViolated->type == "H")
          {
            array_push($ViolatedFact, array(
              'label'=> $TopicOrViolated->name ,
              'value'=>$TopicOrViolated->id,
              )
            );
          }
          else
          {
            array_push($Topic, array(
              'label'=> $TopicOrViolated->name,
              'value'=>$TopicOrViolated->id,
              )
            );
          }
        });

        array_push($HumanRights, array(
          'label'=> $Right->name ,
          'value'=>$Right->id,
          "topics" => $Topic,
          "violated_fact" => $ViolatedFact
          )
        );
      });

      array_push($Data, array(
        'label'=> $Tracing->name ,
        'value'=>$Tracing->id,
        "rights" => $HumanRights
        )
      );
    });

    return $Data;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getTracings()
  {
    //Acumulador de todo
    $Data = array();

    $this->HumanRight->byOrganizationAndParentAndType(null, null, $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($Tracing) use (&$Data) {
      array_push($Data, array(
        'label'=> $Tracing->name,
        'value'=>$Tracing->id
        )
      );
    });

    return $Data;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getOnlyHumanRight()
  {
    //Acumulador de todo
    $Data = array();

    $this->HumanRight->byOrganizationAndType(null, $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($HumanRight) use (&$Data) {
      array_push($Data, array(
        'label'=> $HumanRight->name,
        'value'=>$HumanRight->id
        )
      );
    });

    return $Data;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getFrontendOnlyHumanRight()
  {
    //Acumulador de todo
    $Data = array();

    $this->HumanRight->byType(null)->each(function($HumanRight) use (&$Data) {
      array_push($Data, array(
        'label'=> $HumanRight->name,
        'value'=>$HumanRight->id
        )
      );
    });

    return $Data;
  }

  public function getHumanRightsWithTopicAndViolatedFact()
  {
    $data = array();

    $this->HumanRight->byType(null)->each(function($HumanRight) use (&$data) 
    {
      $data[$HumanRight->name] = array(
        'label' => $HumanRight->name,
        'value' => $HumanRight->id,
      );

      $this->HumanRight->byTypeAndParentId("T", array($HumanRight->id))->each(function($Topic) use (&$data, $HumanRight) 
      {
        $data[$HumanRight->name]['topics'][] = array(
          'label' => $Topic->name,
          'value' => $Topic->id,
        );
      });

      $this->HumanRight->byTypeAndParentId("H", array($HumanRight->id))->each(function($Topic) use (&$data, $HumanRight) 
      {
        $data[$HumanRight->name]['violated_fact'][] = array(
          'label' => $Topic->name,
          'value' => $Topic->id,
        );
      });

    });

    return $data;
  }

  public function getWebVisibleHumanRightsWithTopicAndViolatedFact($lang)
  {
    $data = array();

    $this->HumanRight->webVisibleByType(null)->each(function($HumanRight) use (&$data, $lang) 
    {
      $data[$HumanRight->name] = array(
        'label' => $HumanRight->name,
        'value' => $HumanRight->id,
        'es_hr_name' => $HumanRight->es_name,
        'en_hr_name' => $HumanRight->en_name,
        'es_hr_description' => $HumanRight->es_description,
        'en_hr_description' => $HumanRight->en_description,
      );

      $this->HumanRight->webVisibleByTypesAndParentId(array("T", "H"), array($HumanRight->id), $lang)->each(function($Topic) use (&$data, $HumanRight) 
      // $this->HumanRight->webVisibleByTypeAndParentId("T", array($HumanRight->id), $lang)->each(function($Topic) use (&$data, $HumanRight) 
      {
        $data[$HumanRight->name]['topics'][] = array(
          'label' => $Topic->name,
          'value' => $Topic->id,
          'type' => $Topic->type,
          'es_name' => $Topic->es_name,
          'en_name' => $Topic->en_name,
          'es_hr_name' => $HumanRight->es_name,
          'en_hr_name' => $HumanRight->en_name,
          'es_hr_description' => $HumanRight->es_description,
          'en_hr_description' => $HumanRight->en_description,
        );
      });

      // $this->HumanRight->webVisibleByTypeAndParentId("H", array($HumanRight->id), $lang)->each(function($Topic) use (&$data, $HumanRight) 
      // {
      //   $data[$HumanRight->name]['violated_fact'][] = array(
      //     'label' => $Topic->name,
      //     'value' => $Topic->id,
      //     'es_name' => $Topic->es_name,
      //     'en_name' => $Topic->en_name,
      //     'es_hr_name' => $HumanRight->es_name,
      //     'en_hr_name' => $HumanRight->en_name,
      //     'es_hr_description' => $HumanRight->es_description,
      //     'en_hr_description' => $HumanRight->en_description,
      //   );
      // });

    });

    // dd($data);

    return $data;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getTopics()
  {
    //Acumulador de todo
    $Data = array();

    $this->HumanRight->byOrganizationAndType("T", $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($HumanRight) use (&$Data) {
      array_push($Data, array(
        'label'=> $HumanRight->name,
        'value'=>$HumanRight->id
        )
      );
    });

    return $Data;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getFrontendTopics()
  {
    //Acumulador de todo
    $Data = array();

    $this->HumanRight->byType("T")->each(function($HumanRight) use (&$Data) {
      array_push($Data, array(
        'label'=> $HumanRight->name,
        'value'=>$HumanRight->id
        )
      );
    });

    return $Data;
  }

/**
 * Get topics by parentId
 *
 * @param integer $id
 * @return array $data
 */
  public function getFrontendTopicByParentId($input)
  {
    $data = array();

    if(array_key_exists('filters', $input))
    {
      foreach ($input['filters'] as $key => $filter) 
      {
        if($filter['field'] == 'right')
        {
          $this->HumanRight->byTypeAndParentId('T', $filter['data'])->each(function($HumanRight) use (&$data)
          {
            $data[] = array(
              'label'=> $HumanRight->name,
              'value'=>$HumanRight->id
            );
          });

          break;
        }
      }
    }
    else
    {
      $this->HumanRight->byTypeAndParentId('T', $input['id'])->each(function($HumanRight) use (&$data)
      {
        $data[] = array(
          'label'=> $HumanRight->name,
          'value'=>$HumanRight->id
        );
      });
    }

    return $data;
  }

/**
 * Get topics by parentId
 *
 * @param integer $id
 * @return array $data
 */
  public function getFrontendViolatedFactByParentId($input)
  {
    $data = array();

    if(array_key_exists('filters', $input))
    {
      foreach ($input['filters'] as $key => $filter) 
      {
        if($filter['field'] == 'topic_id')
        {
          $this->HumanRight->byTypeAndParentId('H', $filter['data'])->each(function($HumanRight) use (&$data)
          {
            $data[] = array(
              'label'=> $HumanRight->name,
              'value'=>$HumanRight->id
            );
          });

          break;
        }
      }
    }
    else
    {
      $this->HumanRight->byTypeAndParentId('H', $input['id'])->each(function($HumanRight) use (&$data)
      {
        $data[] = array(
          'label'=> $HumanRight->name,
          'value'=>$HumanRight->id
        );
      });
    }

    return $data;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getViolatedFact()
  {
    //Acumulador de todo
    $Data = array();

    $this->HumanRight->byOrganizationAndType("H", $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($HumanRight) use (&$Data) {
      array_push($Data, array(
        'label'=> $HumanRight->name,
        'value'=>$HumanRight->id
        )
      );
    });

    return $Data;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getFrontendViolatedFact()
  {
    //Acumulador de todo
    $Data = array();

    $this->HumanRight->byType("H")->each(function($HumanRight) use (&$Data) {
      array_push($Data, array(
        'label'=> $HumanRight->name,
        'value'=>$HumanRight->id
        )
      );
    });

    return $Data;
  }

  /**
	 * Create a new ...
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function create(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{

    if($input['right_id'])
    {
      $input["parent_id"] = $input['right_id'];
    }
    else
    {
      $input["parent_id"] = $input['tracing_id'];
    }

    unset(
      $input['_token'],
      $input['tracing_label'],
      $input['right_label'],
      $input["tracing_id"],
      $input["right_id"]
    );

    $input = eloquent_array_filter_for_insert($input);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

		$input = array_add($input, 'organization_id', $organizationId);

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
		{
      $HumanRight = $this->HumanRight->create($input, $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $HumanRight->id, 'journalized_type' => $this->HumanRight->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::human-right-management.addedJournal', array('name' => $HumanRight->name)), $Journal));

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage'), 'id' => $HumanRight->id, 'humanRights' => $this->getHumanRights()));
    // return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage'), 'id' => $HumanRight->id, 'number' => $HumanRight->number));
  }
  /**
   * Update an existing ...
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function update(array $input, $HumanRight = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    unset(
      $input['_token'],
      $input['tracing_label'],
      $input['right_label'],
      $input["tracing_id"],
      $input["right_id"]
    );

    $input = eloquent_array_filter_for_update($input);


    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      if(empty($HumanRight))
      {
        $HumanRight = $this->HumanRight->byId($input['id'], $databaseConnectionName);
      }

      $unchangedValues = $HumanRight->toArray();

      $this->HumanRight->update($input, $HumanRight);

      $diff = 0;

      foreach ($input as $key => $value)
      {
        if($unchangedValues[$key] != $value)
        {
          $diff++;

          if($diff == 1)
          {
            $Journal = $this->Journal->create(array('journalized_id' => $HumanRight->id, 'journalized_type' => $this->HumanRight->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
          }

          if($key == 'status')//Para autocomple de estados
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
          }
          else if ($key == 'is_web_visible')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
          }
          else if ($key == 'field1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.field1'), 'field_lang_key' => 'decima-oadh::human-right-management.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          // else if ($key == 'name')
          // {
          //   $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          // }
          else if ($key == 'chekbox0' || $key == 'chekbox1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
          }
          else if($key == 'date')
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $oldValue = '';
            }

            if(!empty($value))
            {
              $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $newValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
          }
          else if($key == 'table_name_id')//field required
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
          }
          else if($key == 'table_name_id')//field not required
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
            }
            else
            {
              $oldValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
          }
          else
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
        }
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage'), 'humanRights' => $this->getHumanRights()));
  }

  /**
   * Update an existing ...
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function updateDetail(array $input, $HumanRightDetail = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    // if(!empty($input['table_name_label']))
    // {
    //   $newValues['table_name_id'] = $input['table_name_label'];
    // }

    unset(
      $input['_token']
    );

    $input = eloquent_array_filter_for_update($input);

    // if(!empty($input['date']))
    // {
    //   $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    // }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      if(empty($HumanRightDetail))
      {
        $HumanRightDetail = $this->HumanRightDetail->byId($input['id'], $databaseConnectionName);
      }

      $unchangedValues = $HumanRightDetail->toArray();

      $this->HumanRightDetail->update($input, $HumanRightDetail);

      $diff = 0;

      foreach ($input as $key => $value)
      {
        if($unchangedValues[$key] != $value)
        {
          $diff++;

          if($diff == 1)
          {
            $Journal = $this->Journal->create(array('journalized_id' => $HumanRightDetail->master_id, 'journalized_type' => $this->HumanRight->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
          }

          if($key == 'status')//Para autocomple de estados
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
          }
          else if ($key == 'field1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.field1'), 'field_lang_key' => 'decima-oadh::human-right-management.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'name')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'chekbox0' || $key == 'chekbox1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
          }
          else if($key == 'date')
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $oldValue = '';
            }

            if(!empty($value))
            {
              $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $newValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
          }
          else if($key == 'table_name_id')//field required
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
          }
          else if($key == 'table_name_id')//field not required
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
            }
            else
            {
              $oldValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
          }
          else
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::human-right-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::human-right-management.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
        }
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
  }

  /**
   * Authorize an existing ...
   *
   * @param array $input
	 * 	An array as follows: array(id => $id);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */

  /**
   * Delete an existing ... (soft delete)
   *
   * @param array $input
	 * 	An array as follows: array(id => $id);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */
  public function delete(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      $HumanRight = $this->HumanRight->byId($input['id'], $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $input['id'], 'journalized_type' => $this->HumanRight->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::human-right-management.deletedJournal', array('name' => $HumanRight->name)), $Journal));

      $this->HumanRight->delete(array($input['id']), $databaseConnectionName);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
  }

  /**
   * Delete existing ... (soft delete)
   *
   * @param array $input
	 * 	An array as follows: array($id0, $id1,…);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */
   public function deleteDetails(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
   {
     $count = 0;

     if(empty($organizationId))
     {
       $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
     }

     if(empty($loggedUserId))
     {
       $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
     }

     $this->beginTransaction($openTransaction, $databaseConnectionName);

     try
     {
       foreach ($input['id'] as $key => $id)
       {
         $count++;

         $HumanRightDetail = $this->HumanRightDetail->byId($id, $databaseConnectionName);

         if(empty($HumanRight))
         {
           $HumanRight = $this->HumanRight->byId($HumanRightDetail->master_id, $databaseConnectionName);
         }

         $Journal = $this->Journal->create(array('journalized_id' => $HumanRightDetail->master_id, 'journalized_type' => $this->HumanRight->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
         $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::human-right-management.deletedDetailJournal', array('detailName' => $HumanRightDetail->name, 'masterName' => $HumanRight->name))), $Journal);

         $this->HumanRightDetail->delete(array($id), $databaseConnectionName);
       }

       $this->commit($openTransaction);
     }
     catch (\Exception $e)
     {
       $this->rollBack($openTransaction);

       throw $e;
     }
     catch (\Throwable $e)
     {
       $this->rollBack($openTransaction);

       throw $e;
     }

     if($count == 1)
     {
       return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
     }
     else
     {
       return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
     }
   }
}
