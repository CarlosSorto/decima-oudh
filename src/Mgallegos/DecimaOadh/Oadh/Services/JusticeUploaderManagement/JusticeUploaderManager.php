<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\JusticeUploaderManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeCrimeGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeCrimeTempGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeConstitutionalProccessGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Justice\EloquentJusticeConstitutionalProccessTempGridRepository;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;
use Illuminate\Config\Repository;
use Illuminate\Translation\Translator;
use Illuminate\Database\DatabaseManager;

class JusticeUploaderManager extends AbstractLaravelValidator implements JusticeUploaderManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * File Management Interface
  *
  * @var Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface
  *
  */
  protected $FileManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $UploaderManager;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent crime Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\crime\EloquentJusticeCrimeGridRepository
	 *
	 */
	protected $EloquentJusticeCrimeGridRepository;

  /**
	 * Eloquent crime Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\crime\EloquentJusticeCrimeGridRepository
	 *
	 */
	protected $EloquentJusticeCrimeTempGridRepository;

  /**
	 * Eloquent ConstitutionalProccess Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ConstitutionalProccess\EloquentJusticeConstitutionalProccessGridRepository
	 *
	 */
	protected $EloquentJusticeConstitutionalProccessGridRepository;

  /**
	 * Eloquent ConstitutionalProccess Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ConstitutionalProccess\EloquentJusticeConstitutionalProccessGridRepository
	 *
	 */
	protected $EloquentJusticeConstitutionalProccessTempGridRepository;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
  * Maatwebsite\Excel\Excel
  *
  * @var Excel
  *
  */
  protected $Excel;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    FileManagementInterface $FileManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    UploaderManagementInterface $UploaderManager,
    EloquentJusticeCrimeGridRepository $EloquentJusticeCrimeGridRepository,
    EloquentJusticeCrimeTempGridRepository $EloquentJusticeCrimeTempGridRepository,
    EloquentJusticeConstitutionalProccessGridRepository $EloquentJusticeConstitutionalProccessGridRepository,
    EloquentJusticeConstitutionalProccessTempGridRepository $EloquentJusticeConstitutionalProccessTempGridRepository,
    Carbon $Carbon,
    Excel $Excel,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->FileManager = $FileManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->UploaderManager = $UploaderManager;
    $this->EloquentJusticeCrimeGridRepository = $EloquentJusticeCrimeGridRepository;
    $this->EloquentJusticeCrimeTempGridRepository = $EloquentJusticeCrimeTempGridRepository;
    $this->EloquentJusticeConstitutionalProccessGridRepository = $EloquentJusticeConstitutionalProccessGridRepository;
    $this->EloquentJusticeConstitutionalProccessTempGridRepository = $EloquentJusticeConstitutionalProccessTempGridRepository;
    $this->Carbon = $Carbon;
    $this->Excel = $Excel;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
	}


  //Habeas Corpus Request
  public function getCrimeRequestGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentJusticeCrimeGridRepository, $post);
  }

  public function getCrimeRequestTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentJusticeCrimeTempGridRepository, $post);
  }

	public function crimeProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'month', 'department', 'municipality', 'stage', 'crime_type', 'crime', 'victim_gender', 'victim_age_range', 'result', 'value'];
    $tableName = 'OADH_JA_Crime';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array('victim_gender', 'result'), array('year', 'month', 'value'));
  }

  public function crimeCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'month', 'department', 'municipality', 'stage', 'crime_type', 'crime', 'victim_gender', 'victim_age_range', 'result', 'value', 'file_id', 'organization_id'];
    $tableName = 'OADH_JA_Crime';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function crimeDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_JA_Crime');
  }

  public function crimeDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_JA_Crime');
  }

  //ConstitutionalProccess
  public function getConstitutionalProccessRequestGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentJusticeConstitutionalProccessGridRepository, $post);
  }

  public function getConstitutionalProccessRequestTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentJusticeConstitutionalProccessTempGridRepository, $post);
  }

	public function constitutionalProccessProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'month', 'protection_committals', 'protection_resolved', 'habeas_corpus_committals', 'habeas_corpus_resolved', 'unconstitutional_committals', 'unconstitutional_resolved'];
    $tableName = 'OADH_JA_Constitutional_Proccess';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year', 'month', 'protection_committals', 'protection_resolved', 'habeas_corpus_committals', 'habeas_corpus_resolved', 'unconstitutional_committals', 'unconstitutional_resolved'));
  }

  public function constitutionalProccessCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'month', 'protection_committals', 'protection_resolved', 'habeas_corpus_committals', 'habeas_corpus_resolved', 'unconstitutional_committals', 'unconstitutional_resolved', 'file_id', 'organization_id'];
    $tableName = 'OADH_JA_Constitutional_Proccess';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function constitutionalProccessDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_JA_Constitutional_Proccess');
  }

  public function constitutionalProccessDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_JA_Constitutional_Proccess');
  }


  //tableName
  public function getTableNameGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameGridRepository, $post);
  }

  public function getTableNameTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameTempGridRepository, $post);
  }

	public function tableNameProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['column_1', 'column_2', 'column_3', 'column_4'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function tableNameCopyToProduction($input)
  {
    $response = array();
    $columns = ['column_1', 'column_2', 'column_3', 'column_4', 'file_id', 'organization_id'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function tableNameDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_Prefix_Table_Name');
  }

  public function tableNameDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_Prefix_Table_Name');
  }
}
