<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\JusticeQueryManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Justice\JusticeQueryInterface;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Translation\Translator;
use Illuminate\Config\Repository;
use Illuminate\Cache\CacheManager;

class JusticeQueryManager extends AbstractLaravelValidator implements JusticeQueryManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 *  Module Table Name Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\JusticeQuery\JusticeQueryInterface
	 *
	 */
	protected $JusticeQuery;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

  protected $mapIds;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    JusticeQueryInterface $JusticeQuery,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->JusticeQuery = $JusticeQuery;
    $this->Carbon = $Carbon;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
    $this->Cache = $Cache;

    $this->mapIds = array(
      'sonsonate' => 'SV-SO',
      'santa ana' => 'SV-SA',
      'ahuachapan' => 'SV-Ah',
      'ahuachapán' => 'SV-Ah',
      'chalatenango' => 'SV-CH',
      'la libertad' => 'SV-LI',
      'san salvador' => 'SV-SS',
      'la paz' => 'SV-PA',
      'cuscatlan' => 'SV-CU',
      'cuscatlán' => 'SV-CU',
      'cabañas' => 'SV-CA',
      'san miguel' => 'SV-SM',
      'usulutan' => 'SV-US',
      'usulután' => 'SV-US',
      'morazan' => 'SV-MO',
      'morazán' => 'SV-MO',
      'san vicente' => 'SV-SV',
      'la union' => 'SV-UN',
      'la unión' => 'SV-UN',
    );
	}

  /**
   * Get JusticeQuery
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getJusticeQuery($id, $databaseConnectionName = null)
  {
    $JusticeQuery = $this->JusticeQuery->byId($id, $databaseConnectionName);

    if(empty($JusticeQuery))
    {
      return false;
    }

    return $JusticeQuery;
  }

  /**
   * Get JusticeQuery
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getJusticeQuerys($organizationId = null, $databaseConnectionName = null, $returnJson = false)
  {
    $moduleTableNames = array();

    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    // if(!$this->Cache->has('moduleTableNames' . $organizationId))
    // {
    //   $this->JusticeQuery->byOrganization($organizationId)->each(function($JusticeQuery) use (&$moduleTableNames)
    //   {
    //     array_push($moduleTableNames, array('label'=> $JusticeQuery->name , 'value'=>$JusticeQuery->id));
    //   });
    //
    //   $this->Cache->put('moduleTableNames' . $organizationId, json_encode($moduleTableNames), 360);
    // }
    // else
    // {
    //   $moduleTableNames = json_decode($this->Cache->get('moduleTableNames' . $organizationId), true);
    // }

    $this->JusticeQuery->byOrganization($organizationId, $databaseConnectionName)->each(function($JusticeQuery) use (&$moduleTableNames)
    {
      array_push(
        $moduleTableNames,
        array(
          'label'=> $JusticeQuery->name ,
          'value'=>$JusticeQuery->id
        )
      );
    });

    if($returnJson)
    {
      return json_encode($moduleTableNames);
    }

    return $moduleTableNames;
  }

  public function getVictimsByCrimeType(array $input, $crimeType)
  {
    $data = $dataTemp = $year = $excel =  array();

    $excel['header'] = array(
      'A' => 'Meses',
      'B' => 'Víctimas',
      'C' => 'Casos judicializados',
      'D' => 'Sentencias',
    );

    $this->JusticeQuery->getJusticeYearFilterByCrimeType($crimeType)->each(function ($JusticeQuery) use(&$data)
    {
      $data['year'][] = $JusticeQuery->year;
    });

    $this->JusticeQuery->getJusticeDepartmentFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['department'][] = array('label' => $JusticeQuery->department, 'value' => $JusticeQuery->department);
    });

    if($crimeType != 'Violencia Intrafamiliar')
    {
      $this->JusticeQuery->getJusticeCrimeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
      {
        $data['crime'][] = array('label' => $JusticeQuery->crime, 'value' => $JusticeQuery->crime);
      });
    }

    $this->JusticeQuery->getJusticeMunicipalityFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['municipality'][] = array('label' => $JusticeQuery->municipality, 'value' => $JusticeQuery->municipality);
    });

    $this->JusticeQuery->getJusticeAgeRangeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['ageRange'][] = array('label' => $JusticeQuery->victim_age_range, 'value' => $JusticeQuery->victim_age_range);
    });

    $minYear = min($data['year']);
    $maxYear = max($data['year']);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $year['yearFrom'] = array($minYear);
      $year['yearTo'] = array($maxYear);
    }
    else
    {
      $year['yearFrom'] = $input['filter']['yearFrom'];
      $year['yearTo'] = $input['filter']['yearTo'];
    }

    unset($input['filter']['yearFrom'], $input['filter']['yearTo']);

    $this->JusticeQuery->getCrimesByYearCrimeTypeAndStage(null, $year['yearFrom'], $year['yearTo'], $input['filter'], $crimeType, 'Ingresadas')->each(Function($JusticeQuery) use(&$dataTemp)
    {
      $dataTemp['victims'][$JusticeQuery->month - 1] = $JusticeQuery->count;
    });

    $this->JusticeQuery->getCrimesByYearCrimeTypeAndStage(null, $year['yearFrom'], $year['yearTo'], $input['filter'], $crimeType, 'Judicializadas')->each(Function($JusticeQuery) use(&$dataTemp)
    {
      $dataTemp['processed'][$JusticeQuery->month - 1] = $JusticeQuery->count;
    });

    $this->JusticeQuery->getCrimesByYearCrimeTypeAndStage(null, $year['yearFrom'], $year['yearTo'], $input['filter'], $crimeType, 'Sentencias')->each(Function($JusticeQuery) use(&$dataTemp)
    {
      $dataTemp['judgments'][$JusticeQuery->month - 1] =  $JusticeQuery->count;
    });

    
    $dataTemp['months'] = $this->getMonthNames();

    foreach ($dataTemp['months'] as $key => $row) 
    {
      $data['data'][] = array(
        'month' => $row,
        'victims' => isset($dataTemp['victims'][$key]) ? (int) $dataTemp['victims'][$key] : 0,
        'processed' => isset($dataTemp['processed'][$key]) ? (int) $dataTemp['processed'][$key] : 0,
        'judgments' => isset($dataTemp['judgments'][$key]) ? (int) $dataTemp['judgments'][$key] : 0,
      );

      $excel['details']['A'][] = $row;
      $excel['details']['B'][] = isset($dataTemp['victims'][$key]) ? (int) $dataTemp['victims'][$key] : 0;
      $excel['details']['C'][] = isset($dataTemp['processed'][$key]) ? (int) $dataTemp['processed'][$key] : 0;
      $excel['details']['D'][] = isset($dataTemp['judgments'][$key]) ? (int) $dataTemp['judgments'][$key] : 0;
    }

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getDomesticViolenceOnWomenByCrimeTypeAndStage(array $input, $crimeType, $stage)
  {
    $data = $dataTemp = $year = $domesticViolenceBymunicipality = $excel = array();

    $monthNames = $this->getMonthNames();

    $excel['header'] = array(
      'A' => 'Departamento',
      'B' => 'Municipio',
      'C' => 'Casos registrados',
    );

    $this->JusticeQuery->getJusticeYearFilterByCrimeType($crimeType)->each(function ($JusticeQuery) use(&$data)
    {
      $data['year'][] = $JusticeQuery->year;
    });

    $this->JusticeQuery->getJusticeMonthFilterByCrimeType($crimeType)->each(function ($JusticeQuery, $index) use(&$data, $monthNames)
    {
      $data['month'][] = array('label' => $monthNames[$index], 'value' => $JusticeQuery->month);
    });
    

    $this->JusticeQuery->getJusticeCrimeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['crime'][] = array('label' => $JusticeQuery->crime, 'value' => $JusticeQuery->crime);
    });

    $this->JusticeQuery->getJusticeAgeRangeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['ageRange'][] = array('label' => $JusticeQuery->victim_age_range, 'value' => $JusticeQuery->victim_age_range);
    });

    $minYear = min($data['year']);
    $maxYear = max($data['year']);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $year['yearFrom'] = array($minYear);
      $year['yearTo'] = array($maxYear);
    }
    else
    {
      $year['yearFrom'] = $input['filter']['yearFrom'];
      $year['yearTo'] = $input['filter']['yearTo'];
    }

    unset($input['filter']['yearFrom'], $input['filter']['yearTo']);

    $headers = [
      'grid_col_1' => ['label' => 'Municipio', 'width' => '70%'],
      'grid_col_2' => ['label' => 'Casos registrados', 'width' => '30%', 'align' => 'center'],
    ];

    $this->JusticeQuery->getCrimesByDepartmentYearCrimeTypeAndStage(null, $year['yearFrom'], $year['yearTo'], $input['filter'], $crimeType, $stage)->each(function ($JusticeQuery) use (&$data, $headers)
    {
      $departmentName = rtrim(ltrim($JusticeQuery->department));

      $data['data'][] = array(
        'id' => isset($this->mapIds[mb_strtolower($departmentName)]) ?  $this->mapIds[mb_strtolower($departmentName)] : '',
        'value' => (int) $JusticeQuery->count,
        'department' => $departmentName,
        'headers' => $headers,
        'legend' => ' Casos registrados'
      );
    });

    $this->JusticeQuery->getCrimesByMunicipalityYearCrimeTypeAndStage(null, $year['yearFrom'], $year['yearTo'], $input['filter'], $crimeType, $stage)->each(function ($JusticeQuery) use (&$domesticViolenceBymunicipality, &$excel)
    {
      $departmentName = rtrim(ltrim($JusticeQuery->department));
      $municipalityName = rtrim(ltrim($JusticeQuery->municipality));
      $domesticViolenceBymunicipality[mb_strtolower($departmentName)][] = array(
        'grid_col_1' => $municipalityName,
        'grid_col_2' => $JusticeQuery->count
      );

      $excel['details']['A'][] = mb_strtoupper($departmentName);
      $excel['details']['B'][] = mb_strtoupper($municipalityName);
      $excel['details']['C'][] = $JusticeQuery->count;
    });

    foreach ($data['data'] as $key => &$department)
    {
      if(!empty($domesticViolenceBymunicipality[mb_strtolower($department['department'])]))
      {
        $department['rows'] = $domesticViolenceBymunicipality[mb_strtolower($department['department'])];
      }
      else
      {
        $department['rows'] = array();
      }

    }

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getDomesticViolenceOnWomenByMonthCrimeTypeResultAndStage(array $input, $crimeType, $stage)
  {
    $data = $dataTemp = $year = $excel = array();

    $month = $this->getMonthNames();

    $excel['header'] = array(
      'A' => 'Meses',
      'B' => 'Sentencias Absolutorias',
      'C' => 'Sentencias Condenatorias',
    );

    $this->JusticeQuery->getJusticeYearFilterByCrimeType($crimeType)->each(function ($JusticeQuery) use(&$data)
    {
      $data['year'][] = $JusticeQuery->year;
    });

    $this->JusticeQuery->getJusticeDepartmentFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['department'][] = array('label' => $JusticeQuery->department, 'value' => $JusticeQuery->department);
    });

    $this->JusticeQuery->getJusticeCrimeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['crime'][] = array('label' => $JusticeQuery->crime, 'value' => $JusticeQuery->crime);
    });

    $this->JusticeQuery->getJusticeMunicipalityFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['municipality'][] = array('label' => $JusticeQuery->municipality, 'value' => $JusticeQuery->municipality);
    });

    $this->JusticeQuery->getJusticeAgeRangeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['ageRange'][] = array('label' => $JusticeQuery->victim_age_range, 'value' => $JusticeQuery->victim_age_range);
    });

    $minYear = min($data['year']);
    $maxYear = max($data['year']);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $year['yearFrom'] = array($minYear);
      $year['yearTo'] = array($maxYear);
    }
    else
    {
      $year['yearFrom'] = $input['filter']['yearFrom'];
      $year['yearTo'] = $input['filter']['yearTo'];
    }

    unset($input['filter']['yearFrom'], $input['filter']['yearTo']);


    $this->JusticeQuery->getCrimesByMonthResultYearCrimeTypeAndStage(null, $year['yearFrom'], $year['yearTo'], $input['filter'], $crimeType, $stage)->each(function ($JusticeQuery) use (&$dataTemp, $month)
    {
      $dataTemp[$month[$JusticeQuery->month - 1]]['month'] = $month[$JusticeQuery->month - 1];
      $dataTemp[$month[$JusticeQuery->month - 1]][$JusticeQuery->result]['count'] = $JusticeQuery->count;
    });

    foreach ($dataTemp as $key => $victims)
    {
      $data['data'][] = array(
        'year' => $victims['month'],
        'column_a' => (array_key_exists('Sentencias Absolutorias', $victims)) ? $victims['Sentencias Absolutorias']['count'] : '0',
        'column_b' =>  (array_key_exists('Sentencias Condenatorias', $victims)) ? $victims['Sentencias Condenatorias']['count'] : 0,
      );

      $excel['details']['A'][] = $victims['month'];
      $excel['details']['B'][] = (array_key_exists('Sentencias Absolutorias', $victims)) ? $victims['Sentencias Absolutorias']['count'] : '0';
      $excel['details']['C'][] = (array_key_exists('Sentencias Condenatorias', $victims)) ? $victims['Sentencias Condenatorias']['count'] : 0;
    }

    $data['labels']['categoryAxisRotation'] = 270;
    $data['labels']['categoryAxis'] = 'Meses';
    $data['labels']['valueAxis'] = 'Casos registrados';
    $data['labels']['column_a'] = 'Sentencias Absolutorias';
    $data['labels']['column_b'] = 'Sentencias Condenatorias';

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getTotalAnualDomesticViolenceByCrimeTypeAndYear(array $input, $crimeType)
  {
    $data = $dataTemp = $year = $excel = array();

    $excel['header'] = array(
      'A' => 'Casos',
      'B' => 'Totales',
    );

    $monthNames = $this->getMonthNames();

    $this->JusticeQuery->getJusticeYearFilterByCrimeType($crimeType)->each(function ($JusticeQuery) use(&$data)
    {
      $data['year'][] = $JusticeQuery->year;
    });

    $this->JusticeQuery->getJusticeMonthFilterByCrimeType($crimeType)->each(function ($JusticeQuery, $index) use(&$data, $monthNames)
    {
      $data['month'][] = array('label' => $monthNames[$index], 'value' => $JusticeQuery->month);
    });

    $this->JusticeQuery->getJusticeDepartmentFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['department'][] = array('label' => $JusticeQuery->department, 'value' => $JusticeQuery->department);
    });

    $this->JusticeQuery->getJusticeCrimeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['crime'][] = array('label' => $JusticeQuery->crime, 'value' => $JusticeQuery->crime);
    });

    $this->JusticeQuery->getJusticeMunicipalityFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['municipality'][] = array('label' => $JusticeQuery->municipality, 'value' => $JusticeQuery->municipality);
    });

    $this->JusticeQuery->getJusticeAgeRangeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['ageRange'][] = array('label' => $JusticeQuery->victim_age_range, 'value' => $JusticeQuery->victim_age_range);
    });

    $minYear = min($data['year']);
    $maxYear = max($data['year']);

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $year['yearFrom'] = array($minYear);
      $year['yearTo'] = array($maxYear);
    }
    else
    {
      $year['yearFrom'] = $input['filter']['yearFrom'];
      $year['yearTo'] = $input['filter']['yearTo'];
    }

    unset($input['filter']['yearFrom'], $input['filter']['yearTo']);


    $this->JusticeQuery->getStageValueSumByYearAndCrimeType(null, $year['yearFrom'], $year['yearTo'], $input['filter'], $crimeType)->each(function ($JusticeQuery) use (&$data, &$excel)
    {
      $data['data'][] = array(
        'label' => $JusticeQuery->stage,
        'value' => $JusticeQuery->count
      );

      $excel['details']['A'][] = $JusticeQuery->stage;
      $excel['details']['B'][] = $JusticeQuery->count;
    });

    $data['labels']['categoryAxisRotation'] = 270;
    $data['labels']['categoryAxis'] = 'Casos';
    $data['labels']['valueAxis'] = 'Totales';

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getProcessedResolvedProtectionCasesByFields(array $input, $committal, $resolved, array $fieldNames = array())
  {
    $data = $dataTemp = $year = $excel = array();

    $excel['header'] = array(
      'A' => 'Meses',
      'B' => $fieldNames[0],
      'C' => $fieldNames[1],
    );

    $dataTemp['months'] = $this->getMonthNames();

    $this->JusticeQuery->getProcessedResolvedProtectionCasesByYear()->each(function ($JusticeQuery) use(&$data)
    {
      $data['year'][] = $JusticeQuery->year;
    });

    $this->JusticeQuery->getProcessedResolvedProtectionCasesByFields($input['filter'], $committal)->each(function ($JusticeQuery) use (&$dataTemp)
    {
      $dataTemp['committals'][$JusticeQuery->month - 1] = $JusticeQuery->count;
    });

    $this->JusticeQuery->getProcessedResolvedProtectionCasesByFields($input['filter'], $resolved)->each(function ($JusticeQuery) use (&$dataTemp)
    {
      $dataTemp['resolved'][$JusticeQuery->month - 1] = $JusticeQuery->count;
    });

    $letterCounter = 'A';
    foreach ($dataTemp['months'] as $key => $row) 
    {
      $data['data'][] = array(
        'month' => $row,
        'committals' => isset($dataTemp['committals'][$key]) ? (int) $dataTemp['committals'][$key] : 0,
        'resolved' => isset($dataTemp['resolved'][$key]) ? (int) $dataTemp['resolved'][$key] : 0,
      );

      $excel['details']['A'][] = $row;
      $excel['details']['B'][] = isset($dataTemp['committals'][$key]) ? (int) $dataTemp['committals'][$key] : 0;
      $excel['details']['C'][] = isset($dataTemp['resolved'][$key]) ? (int) $dataTemp['resolved'][$key] : 0;
    }

    $data['fields'] = array('committals', 'resolved');
    $data['names'] = $fieldNames;

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getAnualComparisonClusteredByStageFilteredByCrimeType(array $input, $crimeType)
  {
    $data = $dataTemp = $year = $yearChart = $excel = array();

    $excel['header'] = array(
      'A' => 'Años',
      'B' => 'Casos',
      'C' => 'Totales',
    );

    $monthNames = $this->getMonthNames();

    $this->JusticeQuery->getJusticeYearFilterByCrimeType($crimeType)->each(function ($JusticeQuery) use(&$data)
    {
      $data['year'][] = $JusticeQuery->year;
    });

    $this->JusticeQuery->getJusticeMonthFilterByCrimeType($crimeType)->each(function ($JusticeQuery, $index) use(&$data, $monthNames)
    {
      $data['month'][] = array('label' => $monthNames[$index], 'value' => $JusticeQuery->month);
    });

    $this->JusticeQuery->getJusticeDepartmentFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['department'][] = array('label' => $JusticeQuery->department, 'value' => $JusticeQuery->department);
    });

    $this->JusticeQuery->getJusticeCrimeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['crime'][] = array('label' => $JusticeQuery->crime, 'value' => $JusticeQuery->crime);
    });

    $this->JusticeQuery->getJusticeMunicipalityFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['municipality'][] = array('label' => $JusticeQuery->municipality, 'value' => $JusticeQuery->municipality);
    });

    $this->JusticeQuery->getJusticeAgeRangeFilterByCrimeType($crimeType)->each(Function($JusticeQuery) use(&$data)
    {
      $data['ageRange'][] = array('label' => $JusticeQuery->victim_age_range, 'value' => $JusticeQuery->victim_age_range);
    });

    if(count($data['year']) > 1)
    {
      $minYear = max($data['year']) - 1;
      $maxYear = max($data['year']);
    }
    else
    {
      $minYear = min($data['year']);
      $maxYear = max($data['year']);
    }

    if (empty($input['filter']['yearFrom']) && empty($input['filter']['yearTo'])) 
    {
      $year['yearFrom'] = array($minYear);
      $year['yearTo'] = array($maxYear);
    }
    else
    {
      $year['yearFrom'] = $input['filter']['yearFrom'];
      $year['yearTo'] = $input['filter']['yearTo'];
    }

    unset($input['filter']['yearFrom'], $input['filter']['yearTo']);

    $this->JusticeQuery->getAnualComparisonClusteredByStageFilteredByCrimeType($year['yearFrom'], $year['yearTo'], $input['filter'], $crimeType)->each(function ($JusticeQuery) use (&$dataTemp, &$yearChart, &$excel)
    {
      $stage = $JusticeQuery->stage == 'Ingresadas' ? 'Víctimas' : $JusticeQuery->stage;

      $dataTemp[$stage]['category'] =  $stage;

      $dataTemp[$stage][$JusticeQuery->year] = $JusticeQuery->count;

      $yearChart[] = $JusticeQuery->year;

      $excel['details']['A'][] = $JusticeQuery->year;
      $excel['details']['B'][] = $stage;
      $excel['details']['C'][] = $JusticeQuery->count;
    });

    $yearChart = array_unique($yearChart);

    foreach($yearChart as $key => $value)
    {
      $data['year_chart'][] = $value;
    }

    foreach($dataTemp as $key => $row)
    {
      $data['data'][] = (array) $row;
    }

    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  public function getMonthNames()
  {
    return array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
  }

}
