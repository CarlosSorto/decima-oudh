<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\NewsManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\News\EloquentNewsGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\News\EloquentNewsNewsGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\News\NewsInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\News\NewsNewsInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Context\ContextInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Setting\SettingInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Context\EloquentContextGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights\HumanRightsInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights\EloquentHumanRightsGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\People\PeopleInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\People\EloquentVictimGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\People\EloquentVictimizerGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Source\SourceInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Source\EloquentSourceGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Producedure\ProducedureInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Producedure\EloquentProducedureGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\DigitalMediaInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\HumanRight\HumanRightInterface;
use Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
use Carbon\Carbon;
use Illuminate\Config\Repository;
use Illuminate\Translation\Translator;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\App;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Routing\ResponseFactory;

class NewsManager extends AbstractLaravelValidator implements NewsManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent News Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\News\EloquentNewsGridRepository
	 *
	 */
	protected $EloquentNewsGridRepository;

  /**
	 * Eloquent News News Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\News\EloquentNewsNewsGridRepository
	 *
	 */
	protected $EloquentNewsNewsGridRepository;

  /**
	 *  News Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\News\NewsInterface
	 *
	 */
	protected $News;

  /**
	 *  News Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\News\NewsNewsInterface
	 *
	 */
	protected $NewsNews;

  /**
	 *  Context Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Context\ContextInterface
	 *
	 */
	protected $Context;

  /**
	 *  Module Table Name Interface
	 *
	 * @var Vendor\DecimaModule\Module\Repositories\Setting\SettingInterface
	 *
	 */
	protected $Setting;

  /**
	 * Eloquent Context Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ContextDetail\EloquentContextDetailGridRepository
	 *
	 */
	protected $EloquentContextGridRepository;

  /**
	 *  HumanRights Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\HumanRights\HumanRightsInterface
	 *
	 */
	protected $HumanRights;

  /**
	 * Eloquent HumanRights Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\HumanRightsDetail\EloquentHumanRightsDetailGridRepository
	 *
	 */
	protected $EloquentHumanRightsGridRepository;

  /**
	 *  People Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\People\PeopleInterface
	 *
	 */
	protected $People;

  /**
	 * Eloquent People Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\PeopleDetail\EloquentPeopleDetailGridRepository
	 *
	 */
	protected $EloquentVictimGridRepository;

  /**
	 * Eloquent People Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\PeopleDetail\EloquentPeopleDetailGridRepository
	 *
	 */
	protected $EloquentVictimizerGridRepository;

  /**
	 *  Source Interface
	 *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Source\SourceInterface
	 *
	 */
	protected $Source;

  /**
	 * Eloquent Source Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\SourceDetail\EloquentSourceDetailGridRepository
	 *
	 */
	protected $EloquentSourceGridRepository;

  /**
	 *  Producedure Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\Producedure\ProducedureInterface
	 *
	 */
	protected $Producedure;

  /**
	 *  DigitalMedia Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\DigitalMediaInterface
	 *
	 */
	protected $DigitalMedia;

  /**
	 *  DigitalMedia Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\DigitalMediaInterface
	 *
	 */
	protected $HumanRight;

  /**
	 * Eloquent Producedure Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\ProducedureDetail\EloquentProducedureDetailGridRepository
	 *
	 */
	protected $EloquentProducedureGridRepository;

  /**
	* App Manager Service
	*
	* @var Mgallegos\DecimaOadh\Oadh\Services\SettingManagement\SettingManagementInterface;
	*
	*/
	protected $SettingManager;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Contracts\Routing\ResponseFactory
   *
   */
  protected $Response;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    EloquentNewsGridRepository $EloquentNewsGridRepository,
    EloquentNewsNewsGridRepository $EloquentNewsNewsGridRepository,
    NewsInterface $News,
    NewsNewsInterface $NewsNews,
    EloquentContextGridRepository $EloquentContextGridRepository,
    ContextInterface $Context,
    SettingInterface $Setting,
    EloquentHumanRightsGridRepository $EloquentHumanRightsGridRepository,
    HumanRightsInterface $HumanRights,
    EloquentVictimGridRepository $EloquentVictimGridRepository,
    EloquentVictimizerGridRepository $EloquentVictimizerGridRepository,
    PeopleInterface $People,
    EloquentSourceGridRepository $EloquentSourceGridRepository,
    SourceInterface $Source,
    EloquentProducedureGridRepository $EloquentProducedureGridRepository,
    ProducedureInterface $Producedure,
    DigitalMediaInterface $DigitalMedia,
    HumanRightInterface $HumanRight,
    SettingManagementInterface $SettingManager,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    ResponseFactory $Response
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->EloquentNewsGridRepository = $EloquentNewsGridRepository;
    $this->EloquentNewsNewsGridRepository = $EloquentNewsNewsGridRepository;
    $this->News = $News;
    $this->NewsNews = $NewsNews;
    $this->EloquentContextGridRepository = $EloquentContextGridRepository;
    $this->Context = $Context;
    $this->Setting = $Setting;
    $this->EloquentHumanRightsGridRepository = $EloquentHumanRightsGridRepository;
    $this->HumanRights = $HumanRights;
    $this->EloquentVictimGridRepository = $EloquentVictimGridRepository;
    $this->EloquentVictimizerGridRepository = $EloquentVictimizerGridRepository;
    $this->People = $People;
    $this->EloquentSourceGridRepository = $EloquentSourceGridRepository;
    $this->Source = $Source;
    $this->EloquentProducedureGridRepository = $EloquentProducedureGridRepository;
    $this->Producedure = $Producedure;
    $this->DigitalMedia = $DigitalMedia;
    $this->HumanRight = $HumanRight;
    $this->SettingManager = $SettingManager;
    $this->Carbon = $Carbon;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
		$this->Response = $Response;
	}

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataMaster(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentNewsGridRepository, $post);
  }

  /**
   * Get ...
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getNews($id, $databaseConnectionName = null)
  {
    $News = $this->News->byId($id, $databaseConnectionName);

    if(empty($News))
    {
      return false;
    }

    return $News;
  }

  /**
   * Get ...
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getAdvancedNews($id, $lang, $databaseConnectionName = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }

    $topics = $procedures = $affected = $weapon = $context = $source = $victim = $victimizer = $rights = $violatedFact = $digitalMedia = $files = Array();

    $News = $this->News->byId($id, $databaseConnectionName)->toArray();

    if(empty($News))
    {
      return false;
    }

    $years = $this->Config->get('system-lang.' . $lang . '.anios');
    $man = $this->Config->get('system-lang.' . $lang . '.hombre');
    $woman = $this->Config->get('system-lang.' . $lang . '.mujer');
    $notSpecified = $this->Config->get('system-lang.' . $lang . '.noespecificado');
    $media = $this->DigitalMedia->byId($News['digital_media_id']);
    $digitalMedia = $media->name . ($media->abbreviation ? " ($media->abbreviation)" : '');

    $this->HumanRights->getTracingRightAndViolatedFactByNewsId($id, $databaseConnectionName)->each(function($Rights) use(&$rights, &$violatedFact)
    {

      $rights[] = $Rights->name;
      $violatedFact[] = $Rights->violated_fact;
    });

    $this->HumanRights->getTopicByNewsId($id, $databaseConnectionName)->each(function($Topics) use(&$topics)
    {
      $topics[] = !empty($Topics) ? $Topics->name : [];
    });

    $this->HumanRights->getAffectedPeopleByNewsId($id, $databaseConnectionName)->each(function($Affected) use(&$affected)
    {
      if(!empty($Affected))
      {
        $affected[] = $Affected->population_affected;
      }
    });

    $this->Producedure->getProducedureByNewsId($id, $databaseConnectionName)->each(function($Producedures) use(&$procedures, &$weapon)
    {
      if(!empty($Producedures->weapon))
      {
        $weapon[] = $Producedures->weapon;
      }

      if(!empty($Producedures->hypothesis_fact))
      {
        $procedures[] = $Producedures->hypothesis_fact;
      }
    });

    $this->News->getContextByNewsId($id, $databaseConnectionName)->each(function($Context) use(&$context)
    {
      if(!empty($Context))
      {
        $contextName =  !empty($Context->municipality) ? " ($Context->municipality)" : '';
        $context['context'][] = $Context->department . $contextName;
      }
    });

    $this->News->getVictimByNewsId($id, 'V', $databaseConnectionName)->each(function($Victim) use(&$victim, $years, $man, $woman, $notSpecified)
    {
      $age = $gender = $profesion =  '';
      if(!empty($Victim->age))
      {
        $age = "$Victim->age $years";
      }
      if(!empty($Victim->profesion))
      {
        $profesion = $Victim->profesion;
      }

      if($Victim->gender == 'H')
      {
        $gender = $man;
      }
      else if($Victim->gender == 'M')
      {
        $gender = $woman;
      }
      else
      {
        $gender = $notSpecified;
      }

      $victim['victim'][] = !empty($gender) ? $gender . 
      ((!empty($age) || !empty($profesion))  ? 
      ' (' . ($age ?: $age) . (!$profesion ? '' :  (!empty($age) ? ', ' . $profesion : $profesion)) . ')' : '') : [];
    });

    $this->News->getVictimByNewsId($id, 'R', $databaseConnectionName)->each(function($Victimizer) use(&$victimizer, $years, $man, $woman, $notSpecified)
    {
      $age = $gender = $profesion =  '';
      if(!empty($Victimizer->age))
      {
        $age = "$Victimizer->age $years";
      }
      if(!empty($Victimizer->profesion))
      {
        $profesion = $Victimizer->profesion;
      }

      if($Victimizer->gender == 'H')
      {
        $gender = $man;
      }
      else if($Victimizer->gender == 'M')
      {
        $gender = $woman;
      }
      else
      {
        $gender = $notSpecified;
      }

      $victimizer['victimizer'][] = !empty($gender) ? $gender . 
      ((!empty($age) || !empty($profesion))  ? 
      ' (' . ($age ?: $age) . (!$profesion ? '' : (!empty($age) ? ', ' . $profesion : $profesion)) . ')' : '') : [];
    });

    $this->News->getSourceByNewsId($id, $databaseConnectionName)->each(function($Source) use(&$source)
    {
      $instance = $responsable = '';

      if(!empty($Source->instance))
      {
        $instance = $Source->instance;
      }

      if(!empty($Source->responsable))
      {
        $responsable = $Source->responsable;
      }

      $source[] = !empty($Source) ? $Source->source . 
      ((!empty($instance) || !empty($responsable))  ? 
      ' (' . ($instance ?: $instance) . (!$responsable ? '' :  (!empty($instance) ? ', ' . $responsable : $responsable)) . ')' : '') : [];
    });

    $this->News->getNewsFiles($id, $databaseConnectionName)->each(function($Files) use(&$files)
    {
      if(!empty($Files))
      {
        $icon = '';

        switch ($Files->system_type) {
          case 'image':
            $icon = 'far fa-file-image';
            break;
          case 'pdf':
            $icon = 'far fa-file-pdf';
            break;
          case 'text':
            $icon = 'far fa-file-file-alt';
            break;
          case 'spreadsheet':
            $icon = 'far fa-file-excel';
            break;
          case 'document':
            $icon = 'far fa-file-word';
            break;
          case 'presentation':
            $icon = 'far fa-file-powerpoint';
            break;
          
          default:
            $icon = 'far fa-file';
            break;
        }
        $files[] = Array(
          'file_id' => $Files->id,
          'name' => $Files->name,
          'system_route' => $Files->system_route,
          'system_type' => $Files->system_type,
          'icon' => $icon,
        );
      }
    });

    $News['digital_media'] = !empty($digitalMedia) ? $digitalMedia : 'N/A';
    $News['rights'] = !empty($rights) ? $rights : ['N/A'];
    $News['violated_fact'] = !empty($violatedFact) ? implode(', ', $violatedFact) : 'N/A';
    $News['topics'] = !empty($topics) ? implode(', ', $topics) : 'N/A';
    $News['procedures'] = !empty($procedures) ? implode(', ', $procedures) : 'N/A';
    $News['affected'] = !empty($affected) ? implode(', ', $affected) : 'N/A';
    $News['weapon'] = !empty($weapon) ? implode(', ', $weapon) : 'N/A';
    $News['context'] = !empty($context) ? implode(', ', $context['context']) : 'N/A';
    $News['victim'] = !empty($victim) ? implode(', ', $victim['victim']) : 'N/A';
    $News['victimizer'] = !empty($victimizer) ? implode(', ', $victimizer['victimizer']) : 'N/A';
    $News['source'] = !empty($source) ? implode(', ', $source) : 'N/A';
    $News['files'] = !empty($files) ? $files : [];
    $News['keyValue'] = $this->SettingManager->getKeyValues($lang)['N001'];

    return $News;
  }

  /**
   * Get News
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getNewsWithInformation()
  {
    $Data = array();

    $this->News->byOrganization($this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($News) use (&$Data) {

      $digitalMedia = $this->DigitalMedia->byId($News->digital_media_id)->name;

      array_push($Data, array(
        'label'=> $News->code . ' - ' . $News->title . ' (' . $digitalMedia . ')',
        'value'=>$News->id
        )
      );
    });

    return $Data;
  }

  /**
   * Get News
   *
   * @return JSON
   *
   */
  public function getNewsInformation($json = false)
  {
    $Data = array();

    $this->News->withInformation($this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($News) use (&$Data) {

      array_push(
        $Data,
        array(
          'id'=>$News->id,
          'code'=>$News->code,
          'time'=>$News->time,
          'date'=>$this->Carbon->createFromFormat('Y-m-d', $News->date)->format($this->Lang->get('form.phpShortDateFormat')),
          'title'=>$News->title,
          'page'=>$News->page,
          'journalistic_genre'=>$News->journalistic_genre,
          'summary'=>$News->summary,
          'new_link'=>$News->new_link,
          'note_locations'=>$News->note_locations,
          'digital_media_label'=>$News->digital_media_label,
          'digital_media_id'=>$News->digital_media_id,
          'section_label'=>$News->section_label,
          'section_id'=>$News->section_id,
          'twitter_link'=>$News->twitter_link,
          'facebook_link'=>$News->facebook_link
        )
      );
    });

    if($json)
    {
    return json_encode($Data);
    }
    else
    {
      return $Data;
    }
  }

  /**
   * Get search modal table rows
   *
   * @return array
   */
  public function getSearchModalTableRows($id = null, $input, $pager = false, $organizationId = null, $databaseConnectionName = null, $returnJson = true)
  {
    $rows = array();
    $limit = $offset = $count = 0;
    $filter = '';

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(!empty($input['filter']))
    {
      $filter = $input['filter'];
    }

    if($pager)
    {
      $count = $this->News->searchModalTableRows($id, $organizationId, true, $limit, $offset, $filter, $databaseConnectionName);

      encode_requested_data(
        $input,
        $count,
        $limit,
        $offset
      );
    }

    $this->News->searchModalTableRows($id, $organizationId, false, $limit, $offset, $filter, $databaseConnectionName)->each(function($News) use (&$rows)
    {
      $rows['key' . $News->oadh_id] = (array)$News;
    });

    $rows = array(
      'from' => $offset,
      'to' => $limit,
      'page' => !empty($input['page']) ? (int)$input['page'] : 1,
      'records' => $count,
      'rows' => $rows
    );
    
    if($returnJson)
    {
      return json_encode($rows);
    }

    return $rows;
  }

  /**
   * Get search modal table rows
   *
   * @return array
   */
  public function getAdvancedSearchModalTableRows($id = null, $input = null, $databaseConnectionName = null, $organizationId = null)
  {
    $filters = '';

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }

    if(!empty($input['filters']))
    {
      $filters = $input['filters'];
    }

    $News = $this->News->searchAdvancedModalTableRows($id, $filters, true, $databaseConnectionName);

    
    if($News->count() == 0)
    {
      return false;
    }

    $News->map(function($new) use($databaseConnectionName)
    {
      $topics = $this->HumanRights->getTopicByNewsId($new->oadh_id, $databaseConnectionName);

      if(!empty($topics))
      {
        $new->topics = $topics;
      }
      return $new;
    });

    return $News;
  }

  /**
   * Get search modal table rows
   *
   * @return array
   */
  public function getPressStatisticsRows($input = null, $databaseConnectionName = null, $organizationId = null)
  {
    $data = array(
      'pressNews' => array(),
      'pressSources' => array(),
      'pressVictims01' => array(),
      'pressVictims03' => array(),
      'pressPerpetrators01' => array(),
      'pressPerpetrators03' => array(),
      'pressPopulation' => array(),
      'pressDepartments' => array(),
      'pressWeapons' => array(),
    );
    $columnName = '';

    switch ($input['type'])
    {
      case 'R':
        //human right
        $columnName = 'right_id';
        break;
      case 'T':
        //topic
        $columnName = 'topic_id';
        break;
      case 'H':
        //violated fact
        $columnName = 'violated_fact_id';
        break;
      default:
        # code...
        break;
    }

    if($input['lang'] == 'es')
      {
        $genders = array(
          'M' => 'Mujer', 
          'H' => 'Hombre', 
          'N' => 'No especificado'
        );
      }
      else
      {
        $genders = array(
          'M' => 'Woman', 
          'H' => 'Man', 
          'N' => 'Unspecified'
        );
      }
    
    $this->News->getNewsByHumanRightId($columnName, $input['id'], $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data)
    {
      $data['pressNews'][] = array(
        'grid_col_1' => $Item->grid_col_1, 
        'grid_col_2' => $Item->grid_col_2
      );
    });

    $this->News->getSourcesByHumanRightId($columnName, $input['id'], $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data)
    {
      $data['pressSources'][] = array(
        'grid_col_1' => $Item->grid_col_1, 
        'grid_col_2' => $Item->grid_col_2
      );
    });

    //V - victim, R - victimizer
    $this->News->getPeopleAgeByTypeAndByHumanRightId($columnName, $input['id'], 'V', $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data)
    {
      $data['pressVictims01'][] = array(
        'grid_col_1' => $Item->grid_col_1, 
        'grid_col_2' => $Item->grid_col_2
      );
    });

    //V - victim, R - victimizer
    $this->News->getPeopleGenderByTypeAndByHumanRightId($columnName, $input['id'], 'V', $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data, $genders)
    {
      $data['pressVictims03'][] = array(
        'grid_col_1' => $genders[$Item->grid_col_1], 
        'grid_col_2' => $Item->grid_col_2
      );
    });

    //V - victim, R - victimizer
    $this->News->getPeopleAgeByTypeAndByHumanRightId($columnName, $input['id'], 'R', $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data)
    {
      $data['pressPerpetrators01'][] = array(
        'grid_col_1' => $Item->grid_col_1, 
        'grid_col_2' => $Item->grid_col_2
      );
    });

    //V - victim, R - victimizer
    $this->News->getPeopleGenderByTypeAndByHumanRightId($columnName, $input['id'], 'R', $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data, $genders)
    {
      $data['pressPerpetrators03'][] = array(
        'grid_col_1' => $genders[$Item->grid_col_1], 
        'grid_col_2' => $Item->grid_col_2
      );
    });

    $this->News->getPopulationAffectedByHumanRightId($columnName, $input['id'], $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data)
    {
      $data['pressPopulation'][] = array(
        'grid_col_1' => $Item->grid_col_1, 
        'grid_col_2' => $Item->grid_col_2
      );
    });

    $this->News->getDepartmentsByHumanRightId($columnName, $input['id'], $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data)
    {
      $data['pressDepartments'][] = array(
        'grid_col_1' => $Item->grid_col_1, 
        'grid_col_2' => $Item->grid_col_2
      );
    });

    $this->News->getWeaponsByHumanRightId($columnName, $input['id'], $input['dateFrom'], $input['dateTo'], $databaseConnectionName)->each(function($Item) use(&$data)
    {
      $data['pressWeapons'][] = array(
        'grid_col_1' => $Item->grid_col_1, 
        'grid_col_2' => $Item->grid_col_2
      );
    });


    return json_encode($data);
  }



/**
 * Create a new ...
 *
 * @param array $input
 * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
 *                            );
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
 */
public function createMaster(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  unset(
    $input['_token'],
    $input['id'],
    $input['digital_media_label'],
    $input['section_label']
  );

  $input = eloquent_array_filter_for_insert($input);

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

  //Generando codigo
  $abbreviation = $this->DigitalMedia->byId($input['digital_media_id'])->abbreviation;
  $lastNew = $this->News->getMaxId($organizationId);
  $input = array_add($input, 'code', trim($abbreviation) . str_replace('/', '', $input['date']) . str_pad(($lastNew + 1), 5, '0',STR_PAD_LEFT));

  //todo validar y agregar unique a la  tabla
  if ($this->News->countByCode($input['code']) > 0)
  {
    return json_encode(array('info' => $this->Lang->get('decima-oadh::media-monitoring.codeAlreadyExists', array('code' => $input['barcode']))));
  }

  $input = array_add($input, 'organization_id', $organizationId);

  if(!empty($input['date']))
  {
    $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
  }

  if(!empty($input['page']))
  {
    $input['page'] = remove_thousands_separator($input['page']);
  }
  else
  {
    $input['page'] = null;
  }

  $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
	{
    $News = $this->News->create($input, $databaseConnectionName);

    $Journal = $this->Journal->create(array('journalized_id' => $News->id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
    $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.addedJournal', array('name' => $News->title)), $Journal));

    // $NewsInformation = $this->getNewsInformation();
    // $NewsRelated = $this->getNewsWithInformation();

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(
    array(
      'success' => $this->Lang->get('form.defaultSuccessSaveMessage'),
      'id' => $News->id, 
      "code" => $input['code'], 
      // 'news'=>$NewsInformation, 
      // 'news_related'=>$NewsRelated,
      // 'smtRows' => $this->getSearchModalTableRows($News->id, $organizationId, $databaseConnectionName, false)
    )
  );
}

/**
 * Update an existing ...
 *
 * @param array $input
 * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
 */
public function updateMaster(array $input, $News = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  if(!empty($input['section_label']))
  {
    $newValues['section_id'] = $input['section_label'];
  }

  if(!empty($input['digital_media_label']))
  {
    $newValues['digital_media_id'] = $input['digital_media_label'];
  }

  unset(
    $input['_token'],
    $input['digital_media_label'],
    $input['section_label']
  );

  $input = eloquent_array_filter_for_update($input);

  if(!empty($input['date']))
  {
    $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
  }

  if(!empty($input['page']))
  {
    $input['page'] = remove_thousands_separator($input['page']);
  }
  else
  {
    $input['page'] = null;
  }

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

  $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
  {
    if(empty($News))
    {
      $News = $this->News->byId($input['id'], $databaseConnectionName);
    }

    $unchangedValues = $News->toArray();

    $this->News->update($input, $News);

    $diff = 0;

    foreach ($input as $key => $value)
    {
      if($unchangedValues[$key] != $value)
      {
        $diff++;

        if($diff == 1)
        {
          $Journal = $this->Journal->create(array('journalized_id' => $News->id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
        }

        else if($key == 'date')
        {
          if(!empty($unchangedValues[$key]))
          {
            $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
          }
          else
          {
            $oldValue = '';
          }

          if(!empty($value))
          {
            $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
          }
          else
          {
            $newValue = '';
          }

          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
        }
        else if($key == 'section_id')//field required
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.section'), 'field_lang_key' => 'decima-oadh::media-monitoring.section', 'old_value' => $this->DigitalMedia->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
        }
        else if($key == 'digital_media_id')//field required
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.digitalMedia'), 'field_lang_key' => 'decima-oadh::media-monitoring.digitalMedia', 'old_value' => $this->DigitalMedia->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
        }
        else
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
      }
    }

    // $NewsInformation = $this->getNewsInformation();
    // $NewsRelated = $this->getNewsWithInformation();

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(
    array(
      'success' => $this->Lang->get('form.defaultSuccessUpdateMessage'),
      // 'news'=>$NewsInformation,
      // 'news_related'=>$NewsRelated,
      // 'smtRows' => $this->getSearchModalTableRows($News->id, $organizationId, $databaseConnectionName, false)
    )
  );
}

/**
 * Delete an existing ... (soft delete)
 *
 * @param array $input
 * 	An array as follows: array(id => $id);
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
 */
public function deleteMaster(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

  $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
  {
    $News = $this->News->byId($input['id'], $databaseConnectionName);

    $Journal = $this->Journal->create(array('journalized_id' => $input['id'], 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
    $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.deleteJournal', array('name' => $News->title)), $Journal));

    // $this->HumanRights->massDelete($input['id'], $databaseConnectionName);
    // $this->Context->massDelete($input['id'], $databaseConnectionName);
    // $this->Source->massDelete($input['id'], $databaseConnectionName);
    // $this->People->massDelete($input['id'], $databaseConnectionName);
    // $this->Producedure->massDelete($input['id'], $databaseConnectionName);
    // $this->NewsNews->massDelete($input['id'], $databaseConnectionName);
    $this->News->delete(array($input['id']), $databaseConnectionName);

    // $NewsInformation = $this->getNewsInformation();
    // $NewsRelated = $this->getNewsWithInformation();


    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(
    array(
      'success' => $this->Lang->get('form.defaultSuccessDeleteMessage'),
      // 'news'=>$NewsInformation, 
      // 'news_related'=>$NewsRelated,
      // 'smtRowId' => 'key'.$input['id']
    )
  );
}
/*
  Ends master
*/

/*
  Start Human rights
*/

/**
* Echo grid data in a jqGrid compatible format
*
* @param array $post
*	All jqGrid posted data
*
* @return void
*/
public function getGridDataHumanRights(array $post)
{
 $this->GridEncoder->encodeRequestedData($this->EloquentHumanRightsGridRepository, $post);
}

/**
 * Create a new ...
 *
 * @param array $input
* 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
*                            );
*
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
 */
public function createHumanRights(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  $right = $input['right_label'];
 unset(
   $input['_token'],
   $input['tracing_type_label'],
   $input['right_label'],
   $input['topic_label'],
   $input['violated_fact_label']
 );

 $input = eloquent_array_filter_for_insert($input);

 if($input['qualification'] != -1  && $input['qualification'] != 1 && $input['qualification'] != 0)
 {
    return json_encode(array('info' => $this->Lang->get('decima-oadh::media-monitoring.qualificationIsNotValid')));
 }

 if(empty($organizationId))
 {
   $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
 }

 if(empty($loggedUserId))
 {
   $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
 }

 $input = array_add($input, 'organization_id', $organizationId);

 $this->beginTransaction($openTransaction, $databaseConnectionName);

 try
	{
   $HumanRights = $this->HumanRights->create($input, $databaseConnectionName);
   $News = $this->News->byId($HumanRights->news_id, $databaseConnectionName);

   $Journal = $this->Journal->create(array('journalized_id' => $HumanRights->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
   $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.addedRightJournal', array('name' => $right)), $Journal));

   $this->commit($openTransaction);
 }
 catch (\Exception $e)
 {
   $this->rollBack($openTransaction);

   throw $e;
 }
 catch (\Throwable $e)
 {
   $this->rollBack($openTransaction);

   throw $e;
 }

 return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage')));
}

/**
* Update an existing ...
*
* @param array $input
* 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
*
* @return JSON encoded string
*  A string as follows:
*	In case of success: {"success" : form.defaultSuccessUpdateMessage}
*/
public function updateHumanRights(array $input, $HumanRights = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  if(!empty($input['tracing_type_label']))
  {
    $newValues['tracing_type_id'] = $input['tracing_type_label'];
  }
  if(!empty($input['right_label']))
  {
    $newValues['right_id'] = $input['right_label'];
  }
  if(!empty($input['topic_label']))
  {
    $newValues['topic_id'] = $input['topic_label'];
  }
  if(!empty($input['violated_fact_label']))
  {
    $newValues['violated_fact_id'] = $input['violated_fact_label'];
  }

 unset(
   $input['_token'],
   $input['tracing_type_label'],
   $input['right_label'],
   $input['topic_label'],
   $input['violated_fact_label']
 );

 if($input['qualification'] != '-1'  && $input['qualification'] != '1' && $input['qualification'] != '0')
 {
    return json_encode(array('info' => $this->Lang->get('decima-oadh::media-monitoring.qualificationIsNotValid')));
 }

 $input = eloquent_array_filter_for_update($input);

 if(empty($organizationId))
 {
   $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
 }

 if(empty($loggedUserId))
 {
   $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
 }

 $this->beginTransaction($openTransaction, $databaseConnectionName);

 try
 {
   if(empty($HumanRights))
   {
     $HumanRights = $this->HumanRights->byId($input['id'], $databaseConnectionName);
   }

   $unchangedValues = $HumanRights->toArray();

   $this->HumanRights->update($input, $HumanRights);

   $diff = 0;

   foreach ($input as $key => $value)
   {
     if($unchangedValues[$key] != $value)
     {
       $diff++;

       if($diff == 1)
       {
         $Journal = $this->Journal->create(array('journalized_id' => $HumanRights->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
       }
       else if($key == 'date')
       {
         if(!empty($unchangedValues[$key]))
         {
           $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
         }
         else
         {
           $oldValue = '';
         }

         if(!empty($value))
         {
           $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
         }
         else
         {
           $newValue = '';
         }

         $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
       }
       else if($key == 'tracing_type_id')//field required
       {
         $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.tracingType'), 'field_lang_key' => 'decima-oadh::media-monitoring.tracingType', 'old_value' => $this->HumanRight->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
       }
       else if($key == 'right_id')//field required
       {
         $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.right'), 'field_lang_key' => 'decima-oadh::media-monitoring.right', 'old_value' => $this->HumanRight->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
       }
       else if($key == 'topic_id')//field required
       {
         $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.topic'), 'field_lang_key' => 'decima-oadh::media-monitoring.topic', 'old_value' => $this->HumanRight->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
       }
       else if($key == 'violated_fact_id')//field required
       {
         $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.violatedFact'), 'field_lang_key' => 'decima-oadh::media-monitoring.violatedFact', 'old_value' => $this->HumanRight->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
       }
       else
       {
         $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
       }


       //Pa despues
       // if($key == 'status')//Para autocomple de estados
       // {
       //   $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
       // }
       // else if ($key == 'chekbox0' || $key == 'chekbox1')
       // {
       //   $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
       // }


     }
   }

   $this->commit($openTransaction);
 }
 catch (\Exception $e)
 {
   $this->rollBack($openTransaction);

   throw $e;
 }
 catch (\Throwable $e)
 {
   $this->rollBack($openTransaction);

   throw $e;
 }

 return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
}

/**
* Delete existing ... (soft delete)
*
* @param array $input
 * 	An array as follows: array($id0, $id1,…);
*
* @return JSON encoded string
*  A string as follows:
*	In case of success: {"success" : form.defaultSuccessDeleteMessage}
*/
public function deleteHumanRights(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  $count = 0;

  $News = array();

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

  $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
  {
    foreach ($input['id'] as $key => $id)
    {
      $count++;

      $HumanRights = $this->HumanRights->byId($id, $databaseConnectionName);
      $HumanRight= $this->HumanRight->byId($HumanRights->right_id, $databaseConnectionName);

      if(empty($News))
      {
        $News = $this->News->byId($HumanRights['news_id'], $databaseConnectionName);
      }

      $Journal = $this->Journal->create(array('journalized_id' => $HumanRights->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.deletedDetailJournal', array('detailName' => $HumanRight->name, 'masterName' => $News->title))), $Journal);

      $this->HumanRights->delete(array($id), $databaseConnectionName);
    }

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  if($count == 1)
  {
    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
  }
  else
  {
    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
  }
}

/**
 * Echo grid data in a jqGrid compatible format
 *
 * @param array $post
 *	All jqGrid posted data
 *
 * @return void
 */
public function getGridDataContext(array $post)
{
  $this->GridEncoder->encodeRequestedData($this->EloquentContextGridRepository, $post);
}

/**
  * Create a new ...
  *
  * @param array $input
 * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
 *                            );
 *
  * @return JSON encoded string
  *  A string as follows:
  *	In case of success: {"success" : form.defaultSuccessSaveMessage}
  */
 public function createContext(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
 {
  unset(
    $input['_token']
  );

  $input = eloquent_array_filter_for_insert($input);

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

   $input = array_add($input, 'organization_id', $organizationId);

   $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
   {
    $Context = $this->Context->create($input, $databaseConnectionName);
    $News = $this->News->byId($Context->news_id, $databaseConnectionName);

    $name = !empty($Context->department)? $Context->department : $Context->neighborhood;

    $Journal = $this->Journal->create(array('journalized_id' => $Context->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
    $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.addedContextJournal', array('name' => $name)), $Journal));

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage')));
}

/**
 * Update an existing ...
 *
 * @param array $input
 * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
 */
public function updateContext(array $input, $Context = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  unset(
    $input['_token']
  );

  $input = eloquent_array_filter_for_update($input);

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

  $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
  {
    if(empty($Context))
    {
      $Context = $this->Context->byId($input['id'], $databaseConnectionName);
    }

    $unchangedValues = $Context->toArray();

    $this->Context->update($input, $Context);

    $diff = 0;

    foreach ($input as $key => $value)
    {
      if($unchangedValues[$key] != $value)
      {
        $diff++;

        if($diff == 1)
        {
          $Journal = $this->Journal->create(array('journalized_id' => $Context->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
        }

        if($key == 'status')//Para autocomple de estados
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
        }
        else if ($key == 'field1')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.field1'), 'field_lang_key' => 'decima-oadh::media-monitoring.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
        else if ($key == 'name')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
        else if ($key == 'chekbox0' || $key == 'chekbox1')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
        }
        else if($key == 'table_name_id')//field required
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
        }
        else
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
      }
    }

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
}

/**
 * Delete existing ... (soft delete)
 *
 * @param array $input
  * 	An array as follows: array($id0, $id1,…);
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
 */
 public function deleteContext(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
 {
   $count = 0;

   if(empty($organizationId))
   {
     $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
   }

   if(empty($loggedUserId))
   {
     $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
   }

   $this->beginTransaction($openTransaction, $databaseConnectionName);
   // return $input;

   try
   {
     foreach ($input['id'] as $key => $id)
     {
       $count++;

       $Context = $this->Context->byId($id, $databaseConnectionName);

       $name = !empty($Context->department)? $Context->department : $Context->neighborhood;

       if(empty($News))
       {
         $News = $this->News->byId($Context['news_id'], $databaseConnectionName);
       }

       $Journal = $this->Journal->create(array('journalized_id' => $Context->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
       $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.deletedContextJournal', array('detailName' => $name))), $Journal);

       $this->Context->delete(array($id), $databaseConnectionName);
     }

     $this->commit($openTransaction);
   }
   catch (\Exception $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }
   catch (\Throwable $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }

   if($count == 1)
   {
     return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
   }
   else
   {
     return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
   }
 }



 /**
  * Echo grid data in a jqGrid compatible format
  *
  * @param array $post
  *	All jqGrid posted data
  *
  * @return void
  */
 public function getGridDataSource(array $post)
 {
   $this->GridEncoder->encodeRequestedData($this->EloquentSourceGridRepository, $post);
 }

 /**
   * Create a new ...
   *
   * @param array $input
  * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
  *                            );
  *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessSaveMessage}
   */
  public function createSource(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
   unset(
     $input['_token']
   );

   $input = eloquent_array_filter_for_insert($input);

   if(empty($organizationId))
   {
     $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
   }

   if(empty($loggedUserId))
   {
     $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
   }

   $input = array_add($input, 'organization_id', $organizationId);
   $this->beginTransaction($openTransaction, $databaseConnectionName);

   try
    {
     $Source = $this->Source->create($input, $databaseConnectionName);
     $News = $this->News->byId($Source->news_id, $databaseConnectionName);

     $name = !empty($Source->source)? $Source->source : $Source->responsable;
     $name = !empty($name)? $name : $Source->instance;

     $Journal = $this->Journal->create(array('journalized_id' => $Source->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
     $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.addedSourceJournal', array('name' => $name)), $Journal));

     $this->commit($openTransaction);
   }
   catch (\Exception $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }
   catch (\Throwable $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }

   return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage')));
 }

 /**
  * Update an existing ...
  *
  * @param array $input
  * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
  *
  * @return JSON encoded string
  *  A string as follows:
  *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
  */
 public function updateSource(array $input, $Source = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
 {
   unset(
     $input['_token']
   );

   $input = eloquent_array_filter_for_update($input);

   if(empty($organizationId))
   {
     $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
   }

   if(empty($loggedUserId))
   {
     $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
   }

   $this->beginTransaction($openTransaction, $databaseConnectionName);

   try
   {
     if(empty($Source))
     {
       $Source = $this->Source->byId($input['id'], $databaseConnectionName);
     }

     $unchangedValues = $Source->toArray();

     $this->Source->update($input, $Source);

     $diff = 0;

     foreach ($input as $key => $value)
     {
       if($unchangedValues[$key] != $value)
       {
         $diff++;

         if($diff == 1)
         {
           $Journal = $this->Journal->create(array('journalized_id' => $Source->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
         }

         if($key == 'status')//Para autocomple de estados
         {
           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
         }
         else if ($key == 'field1')
         {
           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.field1'), 'field_lang_key' => 'decima-oadh::media-monitoring.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
         }
         else if ($key == 'name')
         {
           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
         }
         else if ($key == 'chekbox0' || $key == 'chekbox1')
         {
           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
         }
         else if($key == 'date')
         {
           if(!empty($unchangedValues[$key]))
           {
             $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
           }
           else
           {
             $oldValue = '';
           }

           if(!empty($value))
           {
             $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
           }
           else
           {
             $newValue = '';
           }

           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
         }
         else if($key == 'table_name_id')//field required
         {
           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
         }
         else if($key == 'table_name_id')//field not required
         {
           if(!empty($unchangedValues[$key]))
           {
             $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
           }
           else
           {
             $oldValue = '';
           }

           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
         }
         else
         {
           $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
         }
       }
     }

     $this->commit($openTransaction);
   }
   catch (\Exception $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }
   catch (\Throwable $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }

   return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
 }

 /**
  * Delete existing ... (soft delete)
  *
  * @param array $input
   * 	An array as follows: array($id0, $id1,…);
  *
  * @return JSON encoded string
  *  A string as follows:
  *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
  */
  public function deleteSource(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $count = 0;

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      foreach ($input['id'] as $key => $id)
      {
        $count++;

        $Source = $this->Source->byId($id, $databaseConnectionName);

        if(empty($News))
        {
          $News = $this->News->byId($Source->news_id, $databaseConnectionName);
        }

        $name = !empty($Source->source)? $Source->source : $Source->responsable;
        $name = !empty($name)? $name : $Source->instance;

        $Journal = $this->Journal->create(array('journalized_id' => $Source->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
        $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.deletedSourceJournal', array('detailName' => $name))), $Journal);

        $this->Source->delete(array($id), $databaseConnectionName);
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    if($count == 1)
    {
      return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
    }
    else
    {
      return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
    }
  }



  /**
 * Echo grid data in a jqGrid compatible format
 *
 * @param array $post
 *	All jqGrid posted data
 *
 * @return void
 */
public function getGridDataVictim(array $post)
{
  $this->GridEncoder->encodeRequestedData($this->EloquentVictimGridRepository, $post);
}

  /**
 * Echo grid data in a jqGrid compatible format
 *
 * @param array $post
 *	All jqGrid posted data
 *
 * @return void
 */
public function getGridDataVictimizer(array $post)
{
  $this->GridEncoder->encodeRequestedData($this->EloquentVictimizerGridRepository, $post);
}

/**
  * Create a new ...
  *
  * @param array $input
 * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
 *                            );
 *
  * @return JSON encoded string
  *  A string as follows:
  *	In case of success: {"success" : form.defaultSuccessSaveMessage}
  */
 public function createPeople(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
 {
  unset(
    $input['_token'],
    $input['relation'],
    $input['second_relation'],
    $input['subtype_p'],
    $input['subtype_s']
  );

  $input = eloquent_array_filter_for_insert($input);

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

   $input = array_add($input, 'organization_id', $organizationId);

   $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
   {
    $People = $this->People->create($input, $databaseConnectionName);
    $News = $this->News->byId($People->news_id, $databaseConnectionName);

    $Journal = $this->Journal->create(array('journalized_id' => $People->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));

    if($People->type == 'V')
    {
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.addedVictimJournal', array('name' => $People->first_name)), $Journal));
    }
    else
    {
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.addedVictimizerJournal', array('name' => $People->first_name)), $Journal));
    }

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage')));
}

/**
 * Update an existing ...
 *
 * @param array $input
 * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
 */
public function updatePeople(array $input, $People = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  unset(
    $input['_token'],
    $input['relation'],
    $input['second_relation'],
    $input['subtype_p'],
    $input['subtype_s']
  );

  $tempInput = $input;
  $input = eloquent_array_filter_for_update($input);

  // if(isset($tempInput['victimizer_relation']) && $tempInput['victimizer_relation'] == '0')
  // {
  //   $input['victimizer_relation'] = 0;
  // }

  // if(isset($tempInput['main_victim_relation']) && $tempInput['main_victim_relation'] == '0')
  // {
  //   $input['main_victim_relation'] = 0;
  // }

  // if(isset($tempInput['victim_relation']) && $tempInput['victim_relation'] == '0')
  // {
  //   $input['victim_relation'] = 0;
  // }

  // var_dump($input);die();

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

  $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
  {
    if(empty($People))
    {
      $People = $this->People->byId($input['id'], $databaseConnectionName);
    }

    $unchangedValues = $People->toArray();

    $this->People->update($input, $People);

    if(isset($tempInput['victimizer_relation']) && $tempInput['victimizer_relation'] == '0')
    {
      $this->People->customUpdateById(array('victimizer_relation' => 0), $input['id']);
    }

    if(isset($tempInput['main_victim_relation']) && $tempInput['main_victim_relation'] == '0')
    {
      $this->People->customUpdateById(array('main_victim_relation' => 0), $input['id']);
    }

    if(isset($tempInput['victim_relation']) && $tempInput['victim_relation'] == '0')
    {
      $this->People->customUpdateById(array('victim_relation' => 0), $input['id']);
    }

    $diff = 0;

    foreach ($input as $key => $value)
    {
      if($unchangedValues[$key] != $value)
      {
        $diff++;

        if($diff == 1)
        {
          $Journal = $this->Journal->create(array('journalized_id' => $People->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
        }

        if($key == 'status')//Para autocomple de estados
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
        }
        else if ($key == 'field1')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.field1'), 'field_lang_key' => 'decima-oadh::media-monitoring.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
        else if ($key == 'name')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
        else if ($key == 'chekbox0' || $key == 'chekbox1')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
        }
        else if($key == 'date')
        {
          if(!empty($unchangedValues[$key]))
          {
            $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
          }
          else
          {
            $oldValue = '';
          }

          if(!empty($value))
          {
            $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
          }
          else
          {
            $newValue = '';
          }

          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
        }
        else if($key == 'table_name_id')//field required
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
        }
        else if($key == 'table_name_id')//field not required
        {
          if(!empty($unchangedValues[$key]))
          {
            $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
          }
          else
          {
            $oldValue = '';
          }

          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
        }
        else
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
      }
    }

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
}

/**
 * Delete existing ... (soft delete)
 *
 * @param array $input
  * 	An array as follows: array($id0, $id1,…);
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
 */
 public function deletePeople(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
 {
   $count = 0;

   if(empty($organizationId))
   {
     $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
   }

   if(empty($loggedUserId))
   {
     $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
   }

   $this->beginTransaction($openTransaction, $databaseConnectionName);

   try
   {
     foreach ($input['id'] as $key => $id)
     {
       $count++;

       $People = $this->People->byId($id, $databaseConnectionName);

       if(empty($News))
       {
         $News = $this->News->byId($People->news_id, $databaseConnectionName);
       }

       $Journal = $this->Journal->create(array('journalized_id' => $People->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));

       if($People->type == 'V')
       {
         $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.deletedVictimJournal', array('detailName' => $People->first_name)), $Journal));
       }
       else
       {
         $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.deletedVictimizerJournal', array('detailName' => $People->first_name)), $Journal));
       }

       $this->People->delete(array($id), $databaseConnectionName);
     }

     $this->commit($openTransaction);
   }
   catch (\Exception $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }
   catch (\Throwable $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }

   if($count == 1)
   {
     return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
   }
   else
   {
     return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
   }
 }

  /**
 * Echo grid data in a jqGrid compatible format
 *
 * @param array $post
 *	All jqGrid posted data
 *
 * @return void
 */
public function getGridDataProducedure(array $post)
{
  $this->GridEncoder->encodeRequestedData($this->EloquentProducedureGridRepository, $post);
}

  /**
 * Echo grid data in a jqGrid compatible format
 *
 * @param array $post
 *	All jqGrid posted data
 *
 * @return void
 */
public function getGridDataNewsNews(array $post)
{
  $this->GridEncoder->encodeRequestedData($this->EloquentNewsNewsGridRepository, $post);
}

/**
  * Create a new ...
  *
  * @param array $input
 * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
 *                            );
 *
  * @return JSON encoded string
  *  A string as follows:
  *	In case of success: {"success" : form.defaultSuccessSaveMessage}
  */
 public function createProducedure(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
 {
  unset(
    $input['_token']
  );

  $input = eloquent_array_filter_for_insert($input);

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

   $input = array_add($input, 'organization_id', $organizationId);

   $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
   {
    $Producedure = $this->Producedure->create($input, $databaseConnectionName);
    $News = $this->News->byId($Producedure->news_id, $databaseConnectionName);

    $name = !empty($Producedure->hypothesis_fact)? $Producedure->hypothesis_fact : $Producedure->weapon;
    $name = !empty($name)? $name : $Producedure->place;

    $Journal = $this->Journal->create(array('journalized_id' => $Producedure->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
    $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.addedProducedureJournal', array('name' => $name)), $Journal));

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage')));
}

/**
 * Update an existing ...
 *
 * @param array $input
 * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
 */
public function updateProducedure(array $input, $Producedure = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
{
  unset(
    $input['_token']
  );

  $input = eloquent_array_filter_for_update($input);

  if(empty($organizationId))
  {
    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
  }

  if(empty($loggedUserId))
  {
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
  }

  $this->beginTransaction($openTransaction, $databaseConnectionName);

  try
  {
    if(empty($Producedure))
    {
      $Producedure = $this->Producedure->byId($input['id'], $databaseConnectionName);
    }

    $unchangedValues = $Producedure->toArray();

    $this->Producedure->update($input, $Producedure);

    $diff = 0;

    foreach ($input as $key => $value)
    {
      if($unchangedValues[$key] != $value)
      {
        $diff++;

        if($diff == 1)
        {
          $Journal = $this->Journal->create(array('journalized_id' => $Producedure->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
        }

        if($key == 'status')//Para autocomple de estados
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
        }
        else if ($key == 'field1')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.field1'), 'field_lang_key' => 'decima-oadh::media-monitoring.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
        else if ($key == 'name')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
        else if ($key == 'chekbox0' || $key == 'chekbox1')
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
        }
        else if($key == 'date')
        {
          if(!empty($unchangedValues[$key]))
          {
            $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
          }
          else
          {
            $oldValue = '';
          }

          if(!empty($value))
          {
            $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
          }
          else
          {
            $newValue = '';
          }

          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
        }
        else if($key == 'table_name_id')//field required
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
        }
        else if($key == 'table_name_id')//field not required
        {
          if(!empty($unchangedValues[$key]))
          {
            $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
          }
          else
          {
            $oldValue = '';
          }

          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
        }
        else
        {
          $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::media-monitoring.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
        }
      }
    }

    $this->commit($openTransaction);
  }
  catch (\Exception $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }
  catch (\Throwable $e)
  {
    $this->rollBack($openTransaction);

    throw $e;
  }

  return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
}

/**
 * Delete existing ... (soft delete)
 *
 * @param array $input
  * 	An array as follows: array($id0, $id1,…);
 *
 * @return JSON encoded string
 *  A string as follows:
 *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
 */
 public function deleteProducedure(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
 {
   $count = 0;

   if(empty($organizationId))
   {
     $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
   }

   if(empty($loggedUserId))
   {
     $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
   }

   $this->beginTransaction($openTransaction, $databaseConnectionName);

   try
   {
     foreach ($input['id'] as $key => $id)
     {
       $count++;

       $Producedure = $this->Producedure->byId($id, $databaseConnectionName);

       if(empty($News))
       {
         $News = $this->News->byId($Producedure->news_id, $databaseConnectionName);
       }

       $name = !empty($Producedure->hypothesis_fact)? $Producedure->hypothesis_fact : $Producedure->weapon;
       $name = !empty($name)? $name : $Producedure->place;

       $Journal = $this->Journal->create(array('journalized_id' => $Producedure->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
       $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.deletedProducedureJournal', array('detailName' => $name))), $Journal);

       $this->Producedure->delete(array($id), $databaseConnectionName);
     }

     $this->commit($openTransaction);
   }
   catch (\Exception $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }
   catch (\Throwable $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }

   if($count == 1)
   {
     return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
   }
   else
   {
     return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
   }
 }

 /**
   * Create a new News News
   *
   * @param array $input
  * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
  *                            );
  *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessSaveMessage}
   */
  public function createNewsNews(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
   unset(
     $input['_token'],
     $input['related_news_label']
   );

   $input = eloquent_array_filter_for_insert($input);

   if(empty($organizationId))
   {
     $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
   }

   if(empty($loggedUserId))
   {
     $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
   }

    $input = array_add($input, 'organization_id', $organizationId);

    $this->beginTransaction($openTransaction, $databaseConnectionName);

   try
    {
     $NewsNews = $this->NewsNews->create($input, $databaseConnectionName);
     $News = $this->News->byId($NewsNews->news_id, $databaseConnectionName);
     $RelatedNews = $this->News->byId($NewsNews->related_news_id, $databaseConnectionName);

     $Journal = $this->Journal->create(array('journalized_id' => $NewsNews->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
     $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.addedNewsNewsJournal', array('name' => $RelatedNews->title)), $Journal));

     $this->commit($openTransaction);
   }
   catch (\Exception $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }
   catch (\Throwable $e)
   {
     $this->rollBack($openTransaction);

     throw $e;
   }

   return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage')));
 }


 /**
  * Delete existing ... (soft delete)
  *
  * @param array $input
   * 	An array as follows: array($id0, $id1,…);
  *
  * @return JSON encoded string
  *  A string as follows:
  *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
  */
  public function deleteNewsNews(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    $count = 0;

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      foreach ($input['id'] as $key => $id)
      {
        $count++;

        $NewsNews = $this->NewsNews->byId($id, $databaseConnectionName);
        $RelatedNews = $this->News->byId($NewsNews->related_news_id, $databaseConnectionName);

        if(empty($News))
        {
          $News = $this->News->byId($NewsNews->news_id, $databaseConnectionName);
        }

        $Journal = $this->Journal->create(array('journalized_id' => $NewsNews->news_id, 'journalized_type' => $this->News->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
        $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::media-monitoring.deletedNewsNewsJournal', array('detailName' => $RelatedNews->title))), $Journal);

        $this->NewsNews->delete(array($id), $databaseConnectionName);
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    if($count == 1)
    {
      return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
    }
    else
    {
      return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage1')));
    }
  }

  /**
   * Creates a .xlsx file containing data from visible news filtered cards
   *
   * @param Array $input
   * @param string $databaseConnectionName
   * @param integer $organizationId
   * @return \Symfony\Component\HttpFoundation\StreamedResponse $streamedResponse
   */
  public function downloadAllExcel($input = null, $databaseConnectionName = null)
  {
    $filters = '';
    $lang = $input['lang'];
    $input = json_decode($input['oadh-news-export-data'], true);

    if(!empty($input['filters']))
    {
      $filters = $input['filters'];
    }

    $data = $this->News->searchAdvancedModalTableRows(null, $filters, false, $databaseConnectionName);

    $News = collect($data);

    $News->map(function($news) use($lang, $databaseConnectionName)
    {
      $contextsArr = $sourcesArr = $rightsArr = $factArr = $affectedPeopleArr = $victimsArr = $victimizersArr = $produceduresArr = $weaponArr = $topicsArr = array();
      $media = $this->DigitalMedia->byId($news->oadh_digital_media_id);
      $contexts = $this->News->getContextByNewsId($news->oadh_id, $databaseConnectionName);
      $sources = $this->News->getSourceByNewsId($news->oadh_id, $databaseConnectionName);
      $rights = $this->HumanRights->getTracingRightAndViolatedFactByNewsId($news->oadh_id, $databaseConnectionName);
      $victims = $this->News->getVictimByNewsId($news->oadh_id, 'V', $databaseConnectionName);
      $victimizers = $this->News->getVictimByNewsId($news->oadh_id, 'R', $databaseConnectionName);
      $producedures = $this->Producedure->getProducedureByNewsId($news->oadh_id, $databaseConnectionName);
      $topics = $this->HumanRights->getTopicByNewsId($news->oadh_id, $databaseConnectionName);
      $affectedPeoples = $this->HumanRights->getAffectedPeopleByNewsId($news->oadh_id, $databaseConnectionName);

      $news->media = $media->name . ($media->abbreviation ? " ($media->abbreviation)" : '');

      if(!empty($contexts))
      {
        foreach($contexts as $context)
        {
          $mun = '';
          if(!empty($context->municipality))
          {
            $mun = " ($context->municipality)";
          }

          $contextsArr[] = $context->department . $mun;
        }
      }

      if(!empty($sources))
      {
        foreach($sources as $source)
        {
          $instance = $responsable = '';

          if(!empty($source->instance))
          {
            $instance = $source->instance;
          }
    
          if(!empty($source->responsable))
          {
            $responsable = $source->responsable;
          }
          $sourcesArr[] = !empty($source) ? $source->source . 
          ((!empty($instance) || !empty($responsable))  ? 
          ' (' . ($instance ?: $instance) . (!$responsable ? '' :  (!empty($instance) ? ', ' . $responsable : $responsable)) . ')' : '') : [];
        }
      }

      if(!empty($rights))
      {
        foreach($rights as $right)
        {
          if(!empty($right->name))
          {
            $rightsArr[] = $right->name;
          }

          if(!empty($right->violated_fact))
          {
            $factArr[] = $right->violated_fact;
          }
        }
      }

      if(!empty($affectedPeoples))
      {
        foreach($affectedPeoples as $affectedPeople)
        {
          if(!empty($affectedPeople->population_affected))
          {
            $affectedPeopleArr[] = $affectedPeople->population_affected;
          }
        }
      }

      if(!empty($victims))
      {
        foreach($victims as $victim)
        {
          $age = $gender = $profesion =  '';
          if(!empty($victim->age))
          {
            $age = "$victim->age " . $this->Config->get('system-lang.' . $lang . '.anios');
          }
          if(!empty($victim->profesion))
          {
            $profesion = $victim->profesion;
          }

          if($victim->gender == 'H')
          {
            $gender = $this->Config->get('system-lang.' . $lang . '.hombre');
          }
          else if($victim->gender == 'M')
          {
            $gender = $this->Config->get('system-lang.' . $lang . '.mujer');
          }
          else
          {
            $gender = $this->Config->get('system-lang.' . $lang . '.noespecificado');
          }

          $victimsArr[] = !empty($gender) ? $gender . 
          ((!empty($age) || !empty($profesion))  ? 
          ' (' . ($age ?: $age) . (!$profesion ? '' : (!empty($age) ? ', ' . $profesion : $profesion)) . ')' : '') : [];
        }
      }

      if(!empty($victimizers))
      {
        foreach($victimizers as $victimizer)
        {
          $age = $gender = $profesion =  '';
          if(!empty($victimizer->age))
          {
            $age = "$victimizer->age " . $this->Config->get('system-lang.' . $lang . '.anios');
          }
          if(!empty($victimizer->profesion))
          {
            $profesion = $victimizer->profesion;
          }

          if($victimizer->gender == 'H')
          {
            $gender = $this->Config->get('system-lang.' . $lang . '.hombre');
          }
          else if($victimizer->gender == 'M')
          {
            $gender = $this->Config->get('system-lang.' . $lang . '.mujer');
          }
          else
          {
            $gender = $this->Config->get('system-lang.' . $lang . '.noespecificado');
          }

          $victimizersArr[] = !empty($gender) ? $gender . 
          ((!empty($age) || !empty($profesion))  ? 
          ' (' . ($age ?: $age) . (!$profesion ? '' : (!empty($age) ? ', ' . $profesion : $profesion)) . ')' : '') : [];
        }
      }

      if(!empty($producedures))
      {
        foreach($producedures as $producedure)
        {
          if(!empty($producedure->hypothesis_fact))
          {
            $produceduresArr[] = $producedure->hypothesis_fact;
          }

          if(!empty($producedure->weapon))
          {
            $weaponArr[] = $producedure->weapon;
          }
        }
      }

      if(!empty($topics))
      {
        foreach($topics as $topic)
        {
          $topicsArr[] = $topic->name;
        }
      }

      $news->context = implode(', ', $contextsArr);
      $news->source = implode(', ', $sourcesArr);
      $news->rights = implode(', ', $rightsArr);
      $news->violated_facts = implode(', ', $factArr);
      $news->affected_people = implode(', ', $affectedPeopleArr);
      $news->victims = implode(', ', $victimsArr);
      $news->victimizers = implode(', ', $victimizersArr);
      $news->producedures = implode(', ', $produceduresArr);
      $news->weapons = implode(', ', $weaponArr);
      $news->topics = implode(', ', $topicsArr);

      return $news;
    });

    $keyValue = $this->SettingManager->getKeyValues($lang)['N001'];

    $streamedResponse = new StreamedResponse();

    $streamedResponse->setCallback(function () use($News, $keyValue, $lang)
		{
      $spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
      $lastHeaderCellRow = 'J';

      $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
      $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
			$sheet->getPageSetup()->setFitToWidth(1);
			$sheet->getPageSetup()->setFitToHeight(0);

			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $styles = array(
        'header' =>  array(
          'fill' => array(
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => array(
              'argb' => 'FF2A4074'
            ) 
          ),
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => true,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE
            )
          ),
          'borders' => array(
            'outline' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE
              )
            )
          )
        ),
        'details' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_JUSTIFY,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => false,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        ),
        'details_date' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => false,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        )
      );

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $keyValue,
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      $tableHeaders = array(
        'A4' => 'Fecha',
        'B4' => 'Medio de comunicación',
        'C4' => 'Titular',
        'D4' => 'Resumen',
        'E4' => 'Lugar de los hechos',
        'F4' => 'Fuentes',
        'G4' => 'Derechos humanos',
        'H4' => 'Temas',
        'I4' => 'Hechos Violatorios',
        'J4' => 'Población vulnerada',
        'K4' => 'Hipotesis de los hechos',
        'L4' => 'Tipo de arma',
        'M4' => 'Víctimas',
        'N4' => 'Víctimarios',
        'O4' => 'Enlace externo',
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:$lastHeaderCellRow$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:O$row")->applyFromArray($styles['header']);
      }

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(15);
      }

      foreach (range('C', 'D') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }

      foreach (range('E', 'G') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(30);
      }

      foreach (range('H', 'L') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }

      foreach (range('M', 'O') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(40);
      }

      //details

      $startRowNumber = 4;
      $lastRowNumber = $startRowNumber;

      foreach ($News as $key => $row)
      {
        $lastRowNumber++;
        $sheet->setCellValue("A$lastRowNumber", $this->Carbon->createFromFormat('Y-m-d', $row->oadh_date)->format($this->Lang->get('form.phpShortDateFormat')));
        $sheet->getStyle("A$lastRowNumber")->applyFromArray($styles['details_date']);
        $sheet->setCellValue("B$lastRowNumber", $row->media);
        $sheet->setCellValue("C$lastRowNumber", $row->oadh_title);
        $sheet->setCellValue("D$lastRowNumber", $row->oadh_summary);
        $sheet->setCellValue("E$lastRowNumber", $row->context);
        $sheet->setCellValue("F$lastRowNumber", $row->source);
        $sheet->setCellValue("G$lastRowNumber", $row->rights);
        $sheet->setCellValue("H$lastRowNumber", $row->topics);
        $sheet->setCellValue("I$lastRowNumber", $row->violated_facts);
        $sheet->setCellValue("J$lastRowNumber", $row->affected_people);
        $sheet->setCellValue("K$lastRowNumber", $row->producedures);
        $sheet->setCellValue("L$lastRowNumber", $row->weapons);
        $sheet->setCellValue("M$lastRowNumber", $row->victims);
        $sheet->setCellValue("N$lastRowNumber", $row->victimizers);
        $sheet->setCellValue("O$lastRowNumber", $row->oadh_new_link);
      }

      $startRowNumber++;

      $sheet->getStyle("B$startRowNumber:O$lastRowNumber")->applyFromArray($styles['details']);
      $sheet->getStyle("A$startRowNumber:O$lastRowNumber")->getAlignment()->setWrapText(true);

      $writer =  new Xlsx($spreadsheet);
			$writer->save('php://output');
    });

    $streamedResponse->setStatusCode(Response::HTTP_OK);
    $streamedResponse->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    $streamedResponse->headers->set('Content-Disposition', 'attachment; filename="medios_de_prensa_'. $this->Carbon->now()->format('d_m_Y') .'.xlsx"');

		return $streamedResponse->send();
  }

  /**
   * Creates a .xlsx file containing data from visible news filtered cards
   *
   * @param Array $input
   * @param string $databaseConnectionName
   * @param integer $organizationId
   * @return \Symfony\Component\HttpFoundation\StreamedResponse $streamedResponse
   */
  public function exportPressStatistic($input = null, $databaseConnectionName = null)
  {
    $id = $input['id'];
    // $pressKey = $input['key'];
    $lang = $input['lang'];
    $data = json_decode($input['data'], true);

    // array:9 [▼
    //   "news" => array:4 [▼
    //     0 => array:2 [▼
    //       "grid_col_1" => "Diario Co-latino"
    //       "grid_col_2" => 293
    //     ]
    //     1 => array:2 [▶]
    //     2 => array:2 [▶]
    //     3 => array:2 [▶]
    //   ]
    //   "sources" => array:45 [▶]
    //   "victims01" => array:96 [▶]
    //   "victims03" => array:3 [▶]
    //   "perpetrators01" => array:60 [▶]
    //   "perpetrators03" => array:3 [▶]
    //   "population" => array:17 [▶]
    //   "departments" => array:15 [▶]
    //   "weapons" => array:5 [▶]
    // ]

    $streamedResponse = new StreamedResponse();

    $streamedResponse->setCallback(function () use($id, $lang, $data)
		{
      $spreadsheet = new Spreadsheet();
      $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');

      $styles = array(
        'header' =>  array(
          'fill' => array(
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => array(
              'argb' => 'FF2A4074'
            ) 
          ),
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => true,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE
            )
          ),
          'borders' => array(
            'outline' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE
              )
            )
          )
        ),
        'detailsLeft' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => false,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        ),
        'detailsRight' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => false,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        ),
        'totalsLeft' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => true,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        ),
        'totalsRight' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => true,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        ),
      );

      //--------------------------Sheet Noticias----------------------------------------------------
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($this->Config->get('system-lang.' . $lang . '.noticias'));
      
			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $this->Config->get('system-lang.' . $lang . '.noticiascantidad'),//label
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:B$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
      }

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $lang . '.periodico'),
        'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
      );

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }
      
      $startRowNumber = 4;
      $total = 0;

      foreach ($data['news'] as $key => $row)
      {
        $startRowNumber++;
        $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
        $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
        $total += $row['grid_col_2'];
      }

      $startRowNumber++;

      $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsLeft']);
      $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
      $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
      $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
      
      $sheet->setCellValue("A$startRowNumber", 'Total:');  
      $sheet->setCellValue("B$startRowNumber", $total);

      //--------------------------Sheet Fuentes----------------------------------------------------
			$spreadsheet->createSheet();
			$spreadsheet->setActiveSheetIndex(1);
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($this->Config->get('system-lang.' . $lang . '.fuentes'));
      
			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $this->Config->get('system-lang.' . $lang . '.fuentes'),//label
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:B$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
      }

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $lang . '.fuente'),
        'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
      );

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }
      
      $startRowNumber = 4;
      $total = 0;

      foreach ($data['sources'] as $key => $row)
      {
        $startRowNumber++;
        $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
        $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
        $total += $row['grid_col_2'];
      }

      $startRowNumber++;

      $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsLeft']);
      $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
      $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
      $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
      
      $sheet->setCellValue("A$startRowNumber", 'Total:');  
      $sheet->setCellValue("B$startRowNumber", $total);

      //--------------------------Sheet Víctimas 01----------------------------------------------------
			$spreadsheet->createSheet();
			$spreadsheet->setActiveSheetIndex(2);
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($this->Config->get('system-lang.' . $lang . '.victimas') . ' (' . $this->Config->get('system-lang.' . $lang . '.edad') . ')' );
      
			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $this->Config->get('system-lang.' . $lang . '.victimas'),//label
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:B$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
      }

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $lang . '.edad'),
        'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
      );

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }
      
      $startRowNumber = 4;
      $total = 0;

      foreach ($data['victims01'] as $key => $row)
      {
        $startRowNumber++;
        $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
        $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
        $total += $row['grid_col_2'];
      }

      $startRowNumber++;

      $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
      $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
      $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
      
      $sheet->setCellValue("A$startRowNumber", 'Total:');  
      $sheet->setCellValue("B$startRowNumber", $total);

      //--------------------------Sheet Víctimas 02----------------------------------------------------
			$spreadsheet->createSheet();
			$spreadsheet->setActiveSheetIndex(3);
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($this->Config->get('system-lang.' . $lang . '.victimas') . ' (' . $this->Config->get('system-lang.' . $lang . '.sex') . ')' );
      
			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $this->Config->get('system-lang.' . $lang . '.victimas'),//label
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:B$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
      }

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $lang . '.generovictima'),
        'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
      );

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }
      
      $startRowNumber = 4;
      $total = 0;

      foreach ($data['victims03'] as $key => $row)
      {
        $startRowNumber++;
        $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
        $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
        $total += $row['grid_col_2'];
      }

      $startRowNumber++;

      $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsLeft']);
      $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
      $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
      $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
      
      $sheet->setCellValue("A$startRowNumber", 'Total:');  
      $sheet->setCellValue("B$startRowNumber", $total);

      //--------------------------Sheet Víctimario 01----------------------------------------------------
			$spreadsheet->createSheet();
			$spreadsheet->setActiveSheetIndex(4);
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($this->Config->get('system-lang.' . $lang . '.victimariosShort') . ' (' . $this->Config->get('system-lang.' . $lang . '.edad') . ')' );
      
			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $this->Config->get('system-lang.' . $lang . '.victimariosShort'),//label
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:B$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
      }

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $lang . '.edad'),
        'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
      );

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }
      
      $startRowNumber = 4;
      $total = 0;

      foreach ($data['perpetrators01'] as $key => $row)
      {
        $startRowNumber++;
        $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
        $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
        $total += $row['grid_col_2'];
      }

      $startRowNumber++;

      $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
      $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
      $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
      
      $sheet->setCellValue("A$startRowNumber", 'Total:');  
      $sheet->setCellValue("B$startRowNumber", $total);

      //--------------------------Sheet Víctimario 02----------------------------------------------------
			$spreadsheet->createSheet();
			$spreadsheet->setActiveSheetIndex(5);
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($this->Config->get('system-lang.' . $lang . '.victimariosShort') . ' (' . $this->Config->get('system-lang.' . $lang . '.sex') . ')' );
      
			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $this->Config->get('system-lang.' . $lang . '.victimariosShort'),//label
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:B$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
      }

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $lang . '.generovictimario'),
        'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
      );

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }
      
      $startRowNumber = 4;
      $total = 0;

      foreach ($data['perpetrators03'] as $key => $row)
      {
        $startRowNumber++;
        $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
        $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
        $total += $row['grid_col_2'];
      }

      $startRowNumber++;

      $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsLeft']);
      $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
      $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
      $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
      
      $sheet->setCellValue("A$startRowNumber", 'Total:');  
      $sheet->setCellValue("B$startRowNumber", $total);

      //--------------------------Sheet Poblacion----------------------------------------------------
			$spreadsheet->createSheet();
			$spreadsheet->setActiveSheetIndex(6);
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($this->Config->get('system-lang.' . $lang . '.poblacion'));
      
			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $this->Config->get('system-lang.' . $lang . '.poblacion'),//label
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:B$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
      }

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $lang . '.poblacion'),
        'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
      );

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }
      
      $startRowNumber = 4;
      $total = 0;

      foreach ($data['population'] as $key => $row)
      {
        $startRowNumber++;
        $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
        $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
        $total += $row['grid_col_2'];
      }

      $startRowNumber++;

      $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsLeft']);
      $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
      $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
      $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
      
      $sheet->setCellValue("A$startRowNumber", 'Total:');  
      $sheet->setCellValue("B$startRowNumber", $total);

      //--------------------------Sheet Departments----------------------------------------------------
			$spreadsheet->createSheet();
			$spreadsheet->setActiveSheetIndex(7);
			$sheet = $spreadsheet->getActiveSheet();
			$sheet->setTitle($this->Config->get('system-lang.' . $lang . '.departamentos'));
      
			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      $headerTitle = array(
        $this->Config->get('system-lang.' . $lang . '.oudh'),
        $this->Config->get('system-lang.' . $lang . '.departamentos'),//label
        $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
      );

      //header
      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("A$row:B$row");
        $sheet->setCellValue("A$row", $headerTitle[$key]);
        $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
      }

      $tableHeaders = array(
        'A4' => $this->Config->get('system-lang.' . $lang . '.departamento'),
        'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
      );

      //tableHeaders
      foreach($tableHeaders as $cell => $title)
      {
        $sheet->setCellValue("$cell", $title);
        $sheet->getStyle("$cell")->applyFromArray($styles['header']);
        $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
      }
      
      //autoWidth columns
      foreach (range('A', 'B') as $key => $column) {
        $sheet->getColumnDimension($column)->setWidth(50);
      }
      
      $startRowNumber = 4;
      $total = 0;

      foreach ($data['departments'] as $key => $row)
      {
        $startRowNumber++;
        $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
        $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
        $total += $row['grid_col_2'];
      }

      $startRowNumber++;

      $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsLeft']);
      $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
      $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
      $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
      $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
      
      $sheet->setCellValue("A$startRowNumber", 'Total:');  
      $sheet->setCellValue("B$startRowNumber", $total);

      //--------------------------Sheet Weapons----------------------------------------------------

      if($id == '2')
      {
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(8);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($this->Config->get('system-lang.' . $lang . '.tipoarmas'));
        
        $sheet->getPageMargins()->setTop(0.5);
        $sheet->getPageMargins()->setRight(0.30);
        $sheet->getPageMargins()->setLeft(0.25);
        $sheet->getPageMargins()->setBottom(0.30);

        $headerTitle = array(
          $this->Config->get('system-lang.' . $lang . '.oudh'),
          $this->Config->get('system-lang.' . $lang . '.tipoarmas'),//label
          $this->Config->get('system-lang.' . $lang . '.archivogenerado') . ' ' . $this->Carbon->now()->format($this->Lang->get('form.phpShortDateFormat'))
        );

        //header
        foreach (range(1, 3) as $key => $row) 
        {
          $sheet->mergeCells("A$row:B$row");
          $sheet->setCellValue("A$row", $headerTitle[$key]);
          $sheet->getStyle("A$row:B$row")->applyFromArray($styles['header']);
        }

        $tableHeaders = array(
          'A4' => $this->Config->get('system-lang.' . $lang . '.tipoarma'),
          'B4' => $this->Config->get('system-lang.' . $lang . '.cantidad'),
        );

        //tableHeaders
        foreach($tableHeaders as $cell => $title)
        {
          $sheet->setCellValue("$cell", $title);
          $sheet->getStyle("$cell")->applyFromArray($styles['header']);
          $sheet->getStyle("$cell")->getAlignment()->setWrapText(true);
        }
        
        //autoWidth columns
        foreach (range('A', 'B') as $key => $column) {
          $sheet->getColumnDimension($column)->setWidth(50);
        }
        
        $startRowNumber = 4;
        $total = 0;

        foreach ($data['weapons'] as $key => $row)
        {
          $startRowNumber++;
          $sheet->setCellValue("A$startRowNumber", $row['grid_col_1']);
          $sheet->setCellValue("B$startRowNumber", $row['grid_col_2']);
          $total += $row['grid_col_2'];
        }

        $startRowNumber++;

        $sheet->getStyle("A5:A$startRowNumber")->applyFromArray($styles['detailsLeft']);
        $sheet->getStyle("B5:B$startRowNumber")->applyFromArray($styles['detailsRight']);
        $sheet->getStyle("A5:B$startRowNumber")->getAlignment()->setWrapText(true);
        $sheet->getStyle("A$startRowNumber:A$startRowNumber")->applyFromArray($styles['totalsLeft']);
        $sheet->getStyle("B$startRowNumber:B$startRowNumber")->applyFromArray($styles['totalsRight']);
        
        $sheet->setCellValue("A$startRowNumber", 'Total:');  
        $sheet->setCellValue("B$startRowNumber", $total);
      }

      // switch ($pressKey) {
      //   case 'N':
      //     $spreadsheet->setActiveSheetIndex(0);
      //     break;
      //   case 'S':
      //     $spreadsheet->setActiveSheetIndex(1);
      //     break;
      //   case 'V':
      //     $spreadsheet->setActiveSheetIndex(2);
      //     break;
      //   case 'V':
      //     $spreadsheet->setActiveSheetIndex(3);
      //     break;
      //   default:
      //   $spreadsheet->setActiveSheetIndex(4);
      //     break;
      // }
      $spreadsheet->setActiveSheetIndex(0);

      $writer =  new Xlsx($spreadsheet);
			$writer->save('php://output');
    });

    $streamedResponse->setStatusCode(Response::HTTP_OK);
    $streamedResponse->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    $streamedResponse->headers->set('Content-Disposition', 'attachment; filename="' . $this->Config->get('system-lang.' . $lang . '.prensa') . '_'. $this->Carbon->now()->format('d_m_Y') .'.xlsx"');

		return $streamedResponse->send();
  }

  
  /**
   * download single record pdf 
   *
   * @param array $input
   * @param string $databaseConnectionName
   * @param integer $organizationId
   * @return Barryvdh\DomPDF\PDF
   */
  public function downloadSinglePdf($input, $databaseConnectionName = null, $organizationId = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }

    $topics = $procedures = $affected = $weapon = $context = $source = $victim = $victimizer = $rights = $violatedFact = $digitalMedia = $files = Array();

    $id = json_decode($input['oadh-news-export-data-pdf'], true);
    $News = $this->News->byId($id, $databaseConnectionName)->toArray();
    $lang = $input['lang'];
    $years = $this->Config->get('system-lang.' . $lang . '.anios');
    $man = $this->Config->get('system-lang.' . $lang . '.hombre');
    $woman = $this->Config->get('system-lang.' . $lang . '.mujer');
    $notSpecified = $this->Config->get('system-lang.' . $lang . '.noespecificado');

    if(empty($News))
    {
      return false;
    }

    $media = $this->DigitalMedia->byId($News['digital_media_id']);
    
    $digitalMedia = !empty($media) ? $media->name . " ($media->abbreviation)" : "";

    $this->HumanRights->getTracingRightAndViolatedFactByNewsId($id, $databaseConnectionName)->each(function($Rights) use(&$rights, &$violatedFact)
    {

      $rights[] = $Rights->name;
      $violatedFact[] = $Rights->violated_fact;
    });

    $this->HumanRights->getTopicByNewsId($id, $databaseConnectionName)->each(function($Topics) use(&$topics)
    {
      $topics[] = !empty($Topics) ? $Topics->name : [];
    });

    $this->HumanRights->getAffectedPeopleByNewsId($id, $databaseConnectionName)->each(function($Affected) use(&$affected)
    {
      if(!empty($Affected))
      {
        $affected[] = $Affected->population_affected;
      }
    });

    $this->Producedure->getProducedureByNewsId($id, $databaseConnectionName)->each(function($Producedures) use(&$procedures, &$weapon)
    {
      if(!empty($Producedures->weapon))
      {
        $weapon[] = $Producedures->weapon;
      }

      if(!empty($Producedures->hypothesis_fact))
      {
        $procedures[] = $Producedures->hypothesis_fact;
      }
    });

    $this->News->getContextByNewsId($id, $databaseConnectionName)->each(function($Context) use(&$context)
    {
      if(!empty($Context))
      {
        $contextName =  !empty($Context->municipality) ? " ($Context->municipality)" : '';
        $context['context'][] = $Context->department . $contextName;
      }
    });

    $this->News->getVictimByNewsId($id, 'V', $databaseConnectionName)->each(function($Victim) use(&$victim, $years, $man, $woman, $notSpecified)
    {
      $age = $gender = $profesion =  '';
      if(!empty($Victim->age))
      {
        $age = "$Victim->age $years";
      }
      if(!empty($Victim->profesion))
      {
        $profesion = $Victim->profesion;
      }

      if($Victim->gender == 'H')
      {
        $gender = $man;
      }
      else if($Victim->gender == 'M')
      {
        $gender = $woman;
      }
      else
      {
        $gender = $notSpecified;
      }

      $victim['victim'][] = !empty($gender) ? $gender . 
      ((!empty($age) || !empty($profesion))  ? 
      ' (' . ($age ?: $age) . (!$profesion ? '' : (!empty($age) ? ', ' . $profesion : $profesion)) . ')' : '') : [];
    });

    $this->News->getVictimByNewsId($id, 'R', $databaseConnectionName)->each(function($Victimizer) use(&$victimizer, $years, $man, $woman, $notSpecified)
    {
      $age = $gender = $profesion =  '';
      if(!empty($Victimizer->age))
      {
        $age = "$Victimizer->age $years";
      }
      if(!empty($Victimizer->profesion))
      {
        $profesion = $Victimizer->profesion;
      }

      if($Victimizer->gender == 'H')
      {
        $gender = $man;
      }
      else if($Victimizer->gender == 'M')
      {
        $gender = $woman;
      }
      else
      {
        $gender = $notSpecified;
      }

      $victimizer['victimizer'][] = !empty($gender) ? $gender . 
      ((!empty($age) || !empty($profesion))  ? 
      ' (' . ($age ?: $age) . (!$profesion ? '' : (!empty($age) ? ', ' . $profesion : $profesion)) . ')' : '') : [];
    });

    $this->News->getSourceByNewsId($id, $databaseConnectionName)->each(function($Source) use(&$source)
    {
      $instance = $responsable = '';

      if(!empty($Source->instance))
      {
        $instance = $Source->instance;
      }

      if(!empty($Source->responsable))
      {
        $responsable = $Source->responsable;
      }
      $source[] = !empty($Source) ? $Source->source . 
      ((!empty($instance) || !empty($responsable))  ? 
      ' (' . ($instance ?: $instance) . (!$responsable ? '' :  (!empty($instance) ? ', ' . $responsable : $responsable)) . ')' : '') : [];
    });

    $News['digital_media'] = !empty($digitalMedia) ? $digitalMedia : 'N/A';
    $News['rights'] = !empty($rights) ? $rights : [];
    $News['violated_fact'] = !empty($violatedFact) ? implode(', ', $violatedFact) : 'N/A';
    $News['topics'] = !empty($topics) ? implode(', ', $topics) : 'N/A';
    $News['procedures'] = !empty($procedures) ? implode(', ', $procedures) : 'N/A';
    $News['affected'] = !empty($affected) ? implode(', ', $affected) : 'N/A';
    $News['weapon'] = !empty($weapon) ? implode(', ', $weapon) : 'N/A';
    $News['context'] = !empty($context) ? implode(', ', $context['context']) : 'N/A';
    $News['victim'] = !empty($victim) ? implode(', ', $victim['victim']) : 'N/A';
    $News['victimizer'] = !empty($victimizer) ? implode(', ', $victimizer['victimizer']) : 'N/A';
    $News['source'] = !empty($source) ? implode(', ', $source) : 'N/A';
    $News['files'] = !empty($files) ? $files : [];
    $News['keyValue'] = $this->SettingManager->getKeyValues($lang)['N001'];

    $downloadInput = array(
      'datetime' => $this->Carbon->now()->setTimeZone('America/El_Salvador')->format('Y-m-d H:m:i'),
      'news_id' => $id,
      'organization_id' => 1,
    );

    $this->Setting->createDownload($downloadInput);

    $pdf = App::make('dompdf.wrapper');
    $News['lang'] = $input['lang'];

    return $pdf->loadView('decima-oadh::front-end/news-card-pdf', $News)
      ->setPaper('letter')
      ->download($News['code'] . ".pdf");
  }

  public function downloadFile($input, $databaseConnectionName = null, $organizationId = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(!empty($organizationId) && empty($databaseConnectionName))
    {
      $databaseConnectionName = $this->AuthenticationManager->getCurrentUserOrganizationConnection((int)$organizationId);
    }
    
    $input = json_decode($input['oadh-news-download-file'], true);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->Config->get('filesystems.disks.owncloud.baseUri') . $input['route']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_USERPWD, $this->Config->get('filesystems.disks.owncloud.userName') . ':' . $this->Config->get('filesystems.disks.owncloud.password'));
    $out = curl_exec($ch);
    curl_close($ch);

    if($input['type'] == 'image')
    {
      return $this->Response->make($out)->header('Content-Type', 'image');
    }
    else if($input['type'] == 'pdf')
    {
      return $this->Response->make($out)->header('Content-Type', 'application/pdf');
    }
    else
    {
      return $this->Response->make($out)
        ->header('Pragma', 'public')
        ->header('Expires', '0')
        ->header('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
        ->header('Content-Type', 'application/force-download')
        ->header('Content-Type', 'application/octet-stream')
        ->header('Content-Type', 'application/download')
        ->header('Content-Disposition', 'attachment; filename=' . $input['name'])
        ->header('Content-Transfer-Encoding', 'binary');
    }
  }
}
