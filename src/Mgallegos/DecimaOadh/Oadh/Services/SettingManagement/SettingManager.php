<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\SettingManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Setting\SettingInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentSettingGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Download\EloquentDownloadGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Download\EloquentNewsDownloadGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Download\EloquentRecommendationDownloadGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Subscriber\EloquentSubscriberGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Access\EloquentAccessGridRepository;
use App\Kwaai\Security\Repositories\User\UserInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Carbon\Carbon;
use Illuminate\Config\Repository;
use Illuminate\Translation\Translator;
use Illuminate\Database\DatabaseManager;
use Illuminate\Cache\CacheManager;
use Illuminate\Validation\Factory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class SettingManager extends AbstractLaravelValidator implements SettingManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 *  Module Table Name Interface
	 *
	 * @var Vendor\DecimaModule\Module\Repositories\Setting\SettingInterface
	 *
	 */
	protected $Setting;

  /**
   * User
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentSettingGridRepository
   *
   */
  protected $EloquentSettingGridRepository;

  /**
   * User
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentSettingGridRepository
   *
   */
  protected $EloquentDownloadGridRepository;

  /**
   * User
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentNewsDownloadGridRepository
   *
   */
  protected $EloquentNewsDownloadGridRepository;

  /**
   * User
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentRecommendationDownloadGridRepository
   *
   */
  protected $EloquentRecommendationDownloadGridRepository;

  /**
   * User
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentSettingGridRepository
   *
   */
  protected $EloquentSubscriberGridRepository;

  /**
   * User
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Setting\EloquentSettingGridRepository
   *
   */
  protected $EloquentAccessGridRepository;

  /**
   * User
   *
   * @var App\Kwaai\Security\Repositories\User\UserInterface
   *
   */
  protected $User;

  /**
   * User
   *
   * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
   *
   */
  protected $GridEncoder;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Validation rules
   *
   * @var Array
   */
  protected $rules;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    SettingInterface $Setting,
    EloquentSettingGridRepository $EloquentSettingGridRepository,
    EloquentDownloadGridRepository $EloquentDownloadGridRepository,
    EloquentNewsDownloadGridRepository $EloquentNewsDownloadGridRepository,
    EloquentRecommendationDownloadGridRepository $EloquentRecommendationDownloadGridRepository,
    EloquentSubscriberGridRepository $EloquentSubscriberGridRepository,
    EloquentAccessGridRepository $EloquentAccessGridRepository,
    UserInterface $User,
    Factory $Validator,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->Setting = $Setting;
    $this->EloquentSettingGridRepository = $EloquentSettingGridRepository;
    $this->EloquentDownloadGridRepository = $EloquentDownloadGridRepository;
    $this->EloquentNewsDownloadGridRepository = $EloquentNewsDownloadGridRepository;
    $this->EloquentRecommendationDownloadGridRepository = $EloquentRecommendationDownloadGridRepository;
    $this->EloquentSubscriberGridRepository = $EloquentSubscriberGridRepository;
    $this->EloquentAccessGridRepository = $EloquentAccessGridRepository;
    $this->User = $User;
    $this->Carbon = $Carbon;
    $this->DB = $DB;
		$this->Lang = $Lang;
    $this->Config = $Config;
    $this->Validator = $Validator;
    $this->Cache = $Cache;
	}

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentSettingGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataMaster(array $post)
  {
    // dd('asdasdasda');
    $this->GridEncoder->encodeRequestedData($this->EloquentSubscriberGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataDetail(array $post)
  {
    // dd('asdasdasda');
    $this->GridEncoder->encodeRequestedData($this->EloquentDownloadGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataDetail2(array $post)
  {
    // dd('asdasdasda');
    $this->GridEncoder->encodeRequestedData($this->EloquentAccessGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataDetail3(array $post)
  {
    // dd('asdasdasda');
    $this->GridEncoder->encodeRequestedData($this->EloquentNewsDownloadGridRepository, $post);
  }

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridDataDetail4(array $post)
  {
    // dd('asdasdasda');
    $this->GridEncoder->encodeRequestedData($this->EloquentRecommendationDownloadGridRepository, $post);
  }

  /**
  * Get Setting Journals
  *
  * @return array
  */
  public function getSettingJournals()
  {
    return $this->JournalManager->getJournalsByApp(array('appId' => 'initial-oadh-setup', 'page' => 1, 'journalizedId' => $this->Setting->byOrganization($this->AuthenticationManager->getCurrentUserOrganization('id'))->first()->id, 'filter' => null, 'userId' => null, 'onlyActions' => false), true);
  }

  /**
  * Get current setting configuration
  *
  * @param integer $organizationId
  *
  * @return array
  */
  public function getCurrentSettingConfiguration($organizationId = null, $databaseConnectionName = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    $Setting = $this->Setting->byOrganization($organizationId, $databaseConnectionName);

    if(empty($Setting))
    {
      return array();
    }

    return $Setting->first()->toArray();
  }

  /**
  * Get current setting configuration
  *
  * @param integer $organizationId
  *
  * @return array
  */
  public function getCurrentSettingFrontendConfiguration($organizationId = null, $databaseConnectionName = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    $Setting = $this->Setting->getSettings($databaseConnectionName);

    if(empty($Setting))
    {
      return array();
    }

    return $Setting->first()->toArray();
  }

  /**
  * Get key values
  *
  * @param integer $organizationId
  *
  * @return array
  */
  public function getKeyValues($lang, $databaseConnectionName = null)
  {
    $keyValues = array();
    $valueColumn = $lang . '_value';

    $this->Setting->keyValues($databaseConnectionName)->each(function($KeyValue) use (&$keyValues, $valueColumn)
    {
      $keyValues[$KeyValue->key] =  $KeyValue->$valueColumn;
    });

    return $keyValues;
  }

  /**
  * Get current setting configuration
  *
  * @return array
  */
  public function isUpdateSetup()
  {
    $Setting = $this->Setting->byOrganization($this->AuthenticationManager->getCurrentUserOrganization('id'))->first();

    if(empty($Setting->is_configured))
    {
      return false;
    }
    else
    {
      return true;
    }
  }

  /**
   * Get current population affected
   *
   * @return array
   */
   public function getPopulationAffected($currentSettingConfiguration = null)
   {
     $populationAffected = array();

     if(empty($currentSettingConfiguration))
     {
       $currentSettingConfiguration = $this->getCurrentSettingConfiguration();
     }

     if(!empty($currentSettingConfiguration['population_affected']))
     {
       $population = explode(',', $currentSettingConfiguration['population_affected']);

       foreach ($population as $key => $value)
       {
         array_push($populationAffected, array('label'=> $value, 'value' => $value));
       }
     }

     return $populationAffected;
   }

  /**
   * Get current population affected
   *
   * @return array
   */
   public function getFrontendPopulationAffected($currentSettingConfiguration = null)
   {
     $populationAffected = array();

     if(empty($currentSettingConfiguration))
     {
       $currentSettingConfiguration = $this->getCurrentSettingFrontendConfiguration();
     }

     if(!empty($currentSettingConfiguration['population_affected']))
     {
       $population = explode(',', $currentSettingConfiguration['population_affected']);

       foreach ($population as $key => $value)
       {
         array_push($populationAffected, array('label'=> $value, 'value' => $value));
       }
     }

     return $populationAffected;
   }

   /**
    * Get current note locations
    *
    * @return array
    */
    public function getNoteLocations($currentSettingConfiguration = null)
    {
     $noteLocations = array();

     if(empty($currentSettingConfiguration))
     {
       $currentSettingConfiguration = $this->getCurrentSettingConfiguration();
     }

     if(!empty($currentSettingConfiguration['note_locations']))
     {
       $note = explode(',', $currentSettingConfiguration['note_locations']);

       foreach ($note as $key => $value)
       {
         array_push($noteLocations, array('label'=> $value, 'value' => $value));
       }
     }

     return $noteLocations;
    }

    /**
     * Get current population affected
     *
     * @return array
     */
     public function getJournalisticGenre($currentSettingConfiguration = null)
     {
       $JournalisticGenre = array();

       if(empty($currentSettingConfiguration))
       {
         $currentSettingConfiguration = $this->getCurrentSettingConfiguration();
       }

       if(!empty($currentSettingConfiguration['journalistic_genre']))
       {
         $journalictic = explode(',', $currentSettingConfiguration['journalistic_genre']);

         foreach ($journalictic as $key => $value)
         {
           array_push($JournalisticGenre, array('label'=> $value, 'value' => $value));
         }
       }

       return $JournalisticGenre;
     }

  /**
   * Get current sources
   *
   * @return array
   */
   public function getSources($currentSettingConfiguration = null)
   {
     $Sources = array();

     if(empty($currentSettingConfiguration))
     {
       $currentSettingConfiguration = $this->getCurrentSettingConfiguration();
     }

     if(!empty($currentSettingConfiguration['sources']))
     {
       $sources = explode(',', $currentSettingConfiguration['sources']);

       foreach ($sources as $key => $value)
       {
         array_push($Sources, array('label'=> $value, 'value' => $value));
       }
     }

     return $Sources;
   }

  /**
   * Get current sources
   *
   * @return array
   */
   public function getFrontendSources($currentSettingConfiguration = null)
   {
     $Sources = array();

     if(empty($currentSettingConfiguration))
     {
       $currentSettingConfiguration = $this->getCurrentSettingFrontendConfiguration();
     }

     if(!empty($currentSettingConfiguration['sources']))
     {
       $sources = explode(',', $currentSettingConfiguration['sources']);

       foreach ($sources as $key => $value)
       {
         array_push($Sources, array('label'=> $value, 'value' => $value));
       }
     }

     return $Sources;
   }

  /**
   * Get current sexual_diversities
   *
   * @return array
   */
  public function getSexualDiversities($currentSettingConfiguration = null)
  {
   $SexualDiversities = array();

   if(empty($currentSettingConfiguration))
   {
     $currentSettingConfiguration = $this->getCurrentSettingConfiguration();
   }

   if(!empty($currentSettingConfiguration['sexual_diversities']))
   {
     $sexualDiversities = explode(',', $currentSettingConfiguration['sexual_diversities']);

     foreach ($sexualDiversities as $key => $value)
     {
       array_push($SexualDiversities, array('label'=> $value, 'value' => $value));
     }
   }

   return $SexualDiversities;
  }

  /**
   * Get current guns_types
   *
   * @return array
   */
  public function getGunsTypes($currentSettingConfiguration = null)
  {
   $GunsTypes = array();

   if(empty($currentSettingConfiguration))
   {
     $currentSettingConfiguration = $this->getCurrentSettingConfiguration();
   }

   if(!empty($currentSettingConfiguration['guns_types']))
   {
     $gunsTypes = explode(',', $currentSettingConfiguration['guns_types']);

     foreach ($gunsTypes as $key => $value)
     {
       array_push($GunsTypes, array('label'=> $value, 'value' => $value));
     }
   }

   return $GunsTypes;
  }

  /**
   * Get current guns_types
   *
   * @return array
   */
  public function getFrontendGunsTypes($currentSettingConfiguration = null)
  {
   $GunsTypes = array();

   if(empty($currentSettingConfiguration))
   {
     $currentSettingConfiguration = $this->getCurrentSettingFrontendConfiguration();
   }

   if(!empty($currentSettingConfiguration['guns_types']))
   {
     $gunsTypes = explode(',', $currentSettingConfiguration['guns_types']);

     foreach ($gunsTypes as $key => $value)
     {
       array_push($GunsTypes, array('label'=> $value, 'value' => $value));
     }
   }

   return $GunsTypes;
  }

  /**
   * Update an existing Initial sales setup
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function update(array $input)
  {

    $settingFieldLabel['population_affected'] = $input['population_affected'];
    $settingFieldLabel['note_locations'] = $input['note_locations'];
    $settingFieldLabel['journalistic_genre'] = $input['journalistic_genre'];
    $settingFieldLabel['sources'] = $input['sources'];
    $settingFieldLabel['sexual_diversities'] = $input['sexual_diversities'];
    $settingFieldLabel['guns_types'] = $input['guns_types'];

    unset($input['_token'],$input['population_affected_tokenfield'],$input['note_locations_tokenfield'],$input['journalistic_genre_tokenfield'], $input['sources_tokenfield'], $input['guns_types_tokenfield'], $input['sexual_diversities_tokenfield']);

    $input = eloquent_array_filter_for_update($input);

    $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    $loggedUserId = $this->AuthenticationManager->getLoggedUserId();

    $this->DB->transaction(function() use (&$input, $organizationId, $loggedUserId, $settingFieldLabel)
    {
      $Setting = $this->Setting->byOrganization($organizationId, null)->first();

      $unchangedSettingValues = $Setting->toArray();

      $SettingJournals = $this->Journal->create(array('journalized_id' => $Setting->id, 'journalized_type' => $this->Setting->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));

      if(empty($Setting->is_configured))
      {
        $this->Journal->attachDetail($SettingJournals->id, array('note' => $this->Lang->get('decima-oadh::initial-oadh-setup.settingAddedJournal')), $SettingJournals);

        $input['id'] = $Setting->id;
        $input['is_configured'] = true;
        $this->Setting->update($input);
      }
      else
      {
        $input['id'] = $Setting->id;
        $unchangedSettingValues = $Setting->toArray();
        $this->Setting->update($input);
        $diff = 0;

        foreach ($input as $key => $value)
        {
          if($unchangedSettingValues[$key] != $value)
          {
            $diff++;

            if($diff == 1)
            {
              $Journal = $this->Journal->create(array('journalized_id' => $Setting->id, 'journalized_type' => $this->Setting->getTable(), 'user_id' => $this->AuthenticationManager->getLoggedUserId(), 'organization_id' => $this->AuthenticationManager->getCurrentUserOrganizationId()));
            }

            if($key == 'population_affected')
            {
              $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.populationAffected'), 'field_lang_key' => 'decima-oadh::media-monitoring.populationAffected', 'old_value' => $this->Lang->get($unchangedSettingValues[$key]), 'new_value' => $this->Lang->get($value)), $Journal);
            }
            else if($key == 'note_locations')
            {
              $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.noteLocations'), 'field_lang_key' => 'decima-oadh::media-monitoring.noteLocations', 'old_value' => $this->Lang->get($unchangedSettingValues[$key]), 'new_value' => $this->Lang->get($value)), $Journal);
            }
            else if($key == 'journalistic_genre')
            {
              $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::media-monitoring.journalisticGenre'), 'field_lang_key' => 'decima-oadh::media-monitoring.journalisticGenre', 'old_value' => $this->Lang->get($unchangedSettingValues[$key]), 'new_value' => $this->Lang->get($value)), $Journal);
            }
            else if($key == 'sources' || $key == 'guns_types' || $key == 'sexual_diversities')
            {
              $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::initial-oadh-setup.'.camel_case($key)), 'field_lang_key' => 'decima-oadh::initial-oadh-setup.'.camel_case($key), 'old_value' => $this->Lang->get($unchangedSettingValues[$key]), 'new_value' => $this->Lang->get($value)), $Journal);
            }
          }
        }
      }
    });

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
  }

  /**
   * Update key value
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function updateKeyValue(array $input)
  {
    $this->Setting->updateKeyValue($input['key'], $input['value'], $input['lang']);

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
  }

  /**
   * Update key value
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function createSubscription(array $input)
  {

    $this->rules = array(
      'email' => 'required|email'
    );

    $data = array(
      'email' => $input['email']
    );

    $subscriptionInput = array(
      'email' => $input['email'],
      'organization_id' => 1,
      'datetime' => $this->Carbon->now()->setTimeZone('America/El_Salvador')->format('Y-m-d H:m:i'),
    );

    if ($this->with($data)->fails())
    {
      return json_encode(array('success' => false));
    }

    $this->Setting->createSubscription($subscriptionInput);

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
  }

  /**
   * Update key value
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function createDownloads(array $input)
  {

    $downloadInput = array(
      'datetime' => $this->Carbon->now()->setTimeZone('America/El_Salvador')->format('Y-m-d H:m:i'),
      'publication_id' => $input['publication_id'],
      'organization_id' => 1,
    );

    $this->Setting->createDownload($downloadInput);

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
  }

  /**
   * Update key value
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function createAccess(array $input)
  {

    $scapedName = ltrim(rtrim(preg_replace('/\s+/', ' ', $input['name'])));

    $accessInput = array(
      'datetime' => $this->Carbon->now()->setTimeZone('America/El_Salvador')->format('Y-m-d H:m:i'),
      'right_to' => $input['right_to'],
      'name' => $scapedName,
      'organization_id' => 1,
    );

    $this->Setting->createAccess($accessInput);

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage')));
  }

/**
 * Downloads chart data to an Xlsx file
 *
 * @param array $input
 * @return Spreadsheet $data
 */
  public function downloadXlsxData(array $input)
  {

    $headerTitle = json_decode($input['oadh-xlsx-data-titles'], true);
    $input = json_decode($input['oadh-xlsx-data'], true);
    $headers = $input['header'];
    $details = $input['details'];

    $streamedResponse = new StreamedResponse();

    $streamedResponse->setCallback(function () use($headers, $details, $headerTitle)
		{
      $columnLetter = $lastLetter = 'A';

      $spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();
      $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
      $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
			$sheet->getPageSetup()->setFitToWidth(1);
			$sheet->getPageSetup()->setFitToHeight(0);

			$sheet->getPageMargins()->setTop(0.5);
			$sheet->getPageMargins()->setRight(0.30);
			$sheet->getPageMargins()->setLeft(0.25);
			$sheet->getPageMargins()->setBottom(0.30);

      foreach(range(2, count($details)) as $key => $row)
      {
        $lastLetter++;
      }

      $styles = array(
        'header' =>  array(
          'fill' => array(
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => array(
              'argb' => 'FF2A4074'
            ) 
          ),
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => true,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE
            )
          ),
          'borders' => array(
            'outline' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE
              )
            )
          )
        ),
        'details' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_JUSTIFY,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => false,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        ),
        'details_date' => array(
          'alignment' => array(
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
          ),
          'font' => array(
            'bold' => false,
            'size' => 10,
            'color' => array(
              'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
            )
          ),
          'borders' => array(
            'allBorders' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              'color' => array(
                'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK
              )
            )
          )
        )
      );

      foreach (range(1, 3) as $key => $row) 
      {
        $sheet->mergeCells("$columnLetter$row:$lastLetter$row");
        $sheet->setCellValue("$columnLetter$row", $headerTitle[$key]);
        $sheet->getStyle("$columnLetter$row:$lastLetter$row")->applyFromArray($styles['header']);
      }

      foreach (range($columnLetter, $lastLetter) as $key => $column) 
      {
        $sheet->getColumnDimension($column)->setWidth(30);
      }

      $headerCounterCell = 'A';
      $headerCounterRow = 4;

      foreach($headers as $key => $row)
      {
        $sheet->setCellValue("$headerCounterCell$headerCounterRow", $row);

        $headerCounterCell++;
      }

      $sheet->getStyle("$columnLetter$headerCounterRow:$lastLetter$headerCounterRow")->applyFromArray($styles['header']);
      $sheet->getStyle("$columnLetter$headerCounterRow:$lastLetter$headerCounterRow")->getAlignment()->setWrapText(true);


      $detailCounterCell = 'A';

      foreach($details as $key => $row)
      {
        $detailCounterRows = 5;
        foreach($details[$key] as $key2 => $detailRows)
        {
          $sheet->setCellValue("$detailCounterCell$detailCounterRows", $detailRows);
          $sheet->getStyle("$detailCounterCell$detailCounterRows")->applyFromArray($styles['details']);

          $detailCounterRows++;
        }
        $detailCounterCell++;
      }

      // $sheet->getStyle("$columnLetter$startDetail:$lastLetter$detailCounterRow")->applyFromArray($styles['header']);

      $writer =  new Xlsx($spreadsheet);
			$writer->save('php://output');
    });

    $title = strlen($headerTitle[2]) > 20 ? substr($headerTitle[2], 0, 20) : $headerTitle[2];


    $streamedResponse->setStatusCode(Response::HTTP_OK);
		$streamedResponse->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		$streamedResponse->headers->set('Content-Disposition', 'attachment; filename="'. $title . '_' . $this->Carbon->now()->format('d_m_Y') .'.xlsx"');

		return $streamedResponse->send();
  }


  
  public function getDepartmentAndMunicipality()
  {
    $data = array();

    array_push($data,
      array(
        "label" => "Todos los departamentos",
        "value" => "Todos los departamentos",
        "child" => array(),
    ));

    array_push($data,
      array(
        "label" => "Ahuachapán",
        "value" => "Ahuachapán",
        "child" => array(
          array(
            "label" => "Ahuachapán",
            "value" => "Ahuachapán"
          ),
          array(
            "label" => "Apaneca",
            "value" => "Apaneca"
          ),
          array(
            "label" => "Atiquizaya",
            "value" => "Atiquizaya"
          ),
          array(
            "label" => "Concepción de Ataco",
            "value" => "Concepción de Ataco"
          ),
          array(
            "label" => "El Refugio",
            "value" => "El Refugio"
          ),
          array(
            "label" => "Guaymango",
            "value" => "Guaymango"
          ),
          array(
            "label" => "Jujutla",
            "value" => "Jujutla"
          ),
          array(
            "label" => "San Francisco Menéndez",
            "value" => "San Francisco Menéndez"
          ),
          array(
            "label" => "San Lorenzo",
            "value" => "San Lorenzo"
          ),
          array(
            "label" => "San Pedro Puxtla",
            "value" => "San Pedro Puxtla"
          ),
          array(
            "label" => "Tacuba",
            "value" => "Tacuba"
          ),
          array(
            "label" => "Turín",
            "value" => "Turín"
          )
        )
      )
    );

    array_push($data,
      array(
        "label" => "Santa Ana",
        "value" => "Santa Ana",
        "child" => array(
          array(
            "label" => "Candelaria de la Frontera",
            "value" => "Candelaria de la Frontera"
          ),
          array(
            "label" => "Chalchuapa",
            "value" => "Chalchuapa"
          ),
          array(
            "label" => "Coatepeque",
            "value" => "Coatepeque"
          ),
          array(
            "label" => "El Congo",
            "value" => "El Congo"
          ),
          array(
            "label" => "El Porvenir",
            "value" => "El Porvenir"
          ),
          array(
            "label" => "Masahuat",
            "value" => "Masahuat"
          ),
          array(
            "label" => "Metapán",
            "value" => "Metapán"
          ),
          array(
            "label" => "San Antonio Pajonal",
            "value" => "San Antonio Pajonal"
          ),
          array(
            "label" => "San Sebastián Salitrillo",
            "value" => "San Sebastián Salitrillo"
          ),
          array(
            "label" => "Santa Ana",
            "value" => "Santa Ana"
          ),
          array(
            "label" => "Santa Rosa Guachipilín",
            "value" => "Santa Rosa Guachipilín"
          ),
          array(
            "label" => "Santiago de la Frontera",
            "value" => "Santiago de la Frontera"
          ),
          array(
            "label" => "Texistepeque",
            "value" => "Texistepeque"
          )
        )
      )
    );


    array_push($data,
      array(
        "label" => "Sonsonate",
        "value" => "Sonsonate",
        "child" => array(
          array(
            "label" => "Acajutla",
            "value" => "Acajutla"
          ),
          array(
            "label" => "Armenia",
            "value" => "Armenia"
          ),
          array(
            "label" => "Caluco",
            "value" => "Caluco"
          ),
          array(
            "label" => "Cuisnahuat",
            "value" => "Cuisnahuat"
          ),
          array(
            "label" => "Izalco",
            "value" => "Izalco"
          ),
          array(
            "label" => "Juayúa",
            "value" => "Juayúa"
          ),
          array(
            "label" => "Nahuizalco",
            "value" => "Nahuizalco"
          ),
          array(
            "label" => "Nahulingo",
            "value" => "Nahulingo"
          ),
          array(
            "label" => "Salcoatitán",
            "value" => "Salcoatitán"
          ),
          array(
            "label" => "San Antonio del Monte",
            "value" => "San Antonio del Monte"
          ),
          array(
            "label" => "San Julián",
            "value" => "San Julián"
          ),
          array(
            "label" => "Santa Catarina Masahuat",
            "value" => "Santa Catarina Masahuat"
          ),
          array(
            "label" => "Santa Isabel Ishuatán",
            "value" => "Santa Isabel Ishuatán"
          ),
          array(
            "label" => "Santo Domingo Guzmán",
            "value" => "Santo Domingo Guzmán"
          ),
          array(
            "label" => "Sonsonate",
            "value" => "Sonsonate"
          ),
          array(
            "label" => "Sonzacate",
            "value" => "Sonzacate"
          )
        )
      )
    );

    array_push($data,
      array(
        "label" => "Chalatenango",
        "value" => "Chalatenango",
        "child" => array(
          array(
            "label" => "Agua Caliente",
            "value" => "Agua Caliente"
          ),
          array(
            "label" => "Azacualpa",
            "value" => "Azacualpa"
          ),
          array(
            "label" => "Chalatenango",
            "value" => "Chalatenango"
          ),
          array(
            "label" => "Comalapa",
            "value" => "Comalapa"
          ),
          array(
            "label" => "Citalá",
            "value" => "Citalá"
          ),
          array(
            "label" => "Concepción Quezaltepeque",
            "value" => "Concepción Quezaltepeque"
          ),
          array(
            "label" => "Dulce Nombre de María",
            "value" => "Dulce Nombre de María"
          ),
          array(
            "label" => "El Carrizal",
            "value" => "El Carrizal"
          ),
          array(
            "label" => "El Paraíso",
            "value" => "El Paraíso"
          ),
          array(
            "label" => "La Laguna",
            "value" => "La Laguna"
          ),
          array(
            "label" => "La Palma",
            "value" => "La Palma"
          ),
          array(
            "label" => "La Reina",
            "value" => "La Reina"
          ),
          array(
            "label" => "Las Vueltas",
            "value" => "Las Vueltas"
          ),
          array(
            "label" => "Nueva Concepción",
            "value" => "Nueva Concepción"
          ),
          array(
            "label" => "Nueva Trinidad",
            "value" => "Nueva Trinidad"
          ),
          array(
            "label" => "Nombre de Jesús",
            "value" => "Nombre de Jesús"
          ),
          array(
            "label" => "Ojos de Agua",
            "value" => "Ojos de Agua"
          ),
          array(
            "label" => "Potonico",
            "value" => "Potonico"
          ),
          array(
            "label" => "San Antonio de la Cruz",
            "value" => "San Antonio de la Cruz"
          ),
          array(
            "label" => "San Antonio Los Ranchos",
            "value" => "San Antonio Los Ranchos"
          ),
          array(
            "label" => "San Fernando",
            "value" => "San Fernando"
          ),
          array(
            "label" => "San Francisco Lempa",
            "value" => "San Francisco Lempa"
          ),
          array(
            "label" => "San Francisco Morazán",
            "value" => "San Francisco Morazán"
          ),
          array(
            "label" => "San Ignacio",
            "value" => "San Ignacio"
          ),
          array(
            "label" => "San Isidro Labrador",
            "value" => "San Isidro Labrador"
          ),
          array(
            "label" => "San José Cancasque",
            "value" => "San José Cancasque"
          ),
          array(
            "label" => "San José Las Flores",
            "value" => "San José Las Flores"
          ),
          array(
            "label" => "San Luis del Carmen",
            "value" => "San Luis del Carmen"
          ),
          array(
            "label" => "San Miguel de Mercedes",
            "value" => "San Miguel de Mercedes"
          ),
          array(
            "label" => "San Rafael",
            "value" => "San Rafael"
          ),
          array(
            "label" => "Santa Rita",
            "value" => "Santa Rita"
          ),
          array(
            "label" => "Tejutla",
            "value" => "Tejutla"
          )
        )
      )
    );

    array_push($data,
      array(
        "label" => "Cuscatlán",
        "value" => "Cuscatlán",
        "child" => array(
          array(
            "label" => "Candelaria",
            "value" => "Candelaria"
          ),
          array(
            "label" => "Cojutepeque",
            "value" => "Cojutepeque"
          ),
          array(
            "label" => "El Carmen",
            "value" => "El Carmen"
          ),
          array(
            "label" => "El Rosario",
            "value" => "El Rosario"
          ),
          array(
            "label" => "Monte San Juan",
            "value" => "Monte San Juan"
          ),
          array(
            "label" => "Oratorio de Concepción",
            "value" => "Oratorio de Concepción"
          ),
          array(
            "label" => "San Bartolomé Perulapía",
            "value" => "San Bartolomé Perulapía"
          ),
          array(
            "label" => "San Cristóbal",
            "value" => "San Cristóbal"
          ),
          array(
            "label" => "San José Guayabal",
            "value" => "San José Guayabal"
          ),
          array(
            "label" => "San Pedro Perulapán",
            "value" => "San Pedro Perulapán"
          ),
          array(
            "label" => "San Rafael Cedros",
            "value" => "San Rafael Cedros"
          ),
          array(
            "label" => "San Ramón",
            "value" => "San Ramón"
          ),
          array(
            "label" => "Santa Cruz Analquito",
            "value" => "Santa Cruz Analquito"
          ),
          array(
            "label" => "Santa Cruz Michapa",
            "value" => "Santa Cruz Michapa"
          ),
          array(
            "label" => "Suchitoto",
            "value" => "Suchitoto"
          ),
          array(
            "label" => "Tenancingo",
            "value" => "Tenancingo"
          )
        )
      )
    );
    array_push($data,
      array(
        "label" => "San Salvador",
        "value" => "San Salvador",
        "child" => array(
          array(
            "label" => "Aguilares",
            "value" => "Aguilares"
          ),
          array(
            "label" => "Apopa",
            "value" => "Apopa"
          ),
          array(
            "label" => "Ayutuxtepeque",
            "value" => "Ayutuxtepeque"
          ),
          array(
            "label" => "Cuscatancingo",
            "value" => "Cuscatancingo"
          ),
          array(
            "label" => "Ciudad Delgado",
            "value" => "Ciudad Delgado"
          ),
          array(
            "label" => "El Paisnal",
            "value" => "El Paisnal"
          ),
          array(
            "label" => "Guazapa",
            "value" => "Guazapa"
          ),
          array(
            "label" => "Ilopango",
            "value" => "Ilopango"
          ),
          array(
            "label" => "Mejicanos",
            "value" => "Mejicanos"
          ),
          array(
            "label" => "Nejapa",
            "value" => "Nejapa"
          ),
          array(
            "label" => "Panchimalco",
            "value" => "Panchimalco"
          ),
          array(
            "label" => "Rosario de Mora",
            "value" => "Rosario de Mora"
          ),
          array(
            "label" => "San Marcos",
            "value" => "San Marcos"
          ),
          array(
            "label" => "San Martín",
            "value" => "San Martín"
          ),
          array(
            "label" => "San Salvador",
            "value" => "San Salvador"
          ),
          array(
            "label" => "Santiago Texacuangos",
            "value" => "Santiago Texacuangos"
          ),
          array(
            "label" => "Santo Tomás",
            "value" => "Santo Tomás"
          ),
          array(
            "label" => "Soyapango",
            "value" => "Soyapango"
          ),
          array(
            "label" => "Tonacatepeque",
            "value" => "Tonacatepeque"
          )
        )
      )
    );
    array_push($data,
      array(
        "label" => "La Libertad",
        "value" => "La Libertad",
        "child" => array(
          array(
            "label" => "Antiguo Cuscatlán",
            "value" => "Antiguo Cuscatlán"
          ),
          array(
            "label" => "Chiltiupán",
            "value" => "Chiltiupán"
          ),
          array(
            "label" => "Ciudad Arce",
            "value" => "Ciudad Arce"
          ),
          array(
            "label" => "Colón",
            "value" => "Colón"
          ),
          array(
            "label" => "Comasagua",
            "value" => "Comasagua"
          ),
          array(
            "label" => "Huizúcar",
            "value" => "Huizúcar"
          ),
          array(
            "label" => "Jayaque",
            "value" => "Jayaque"
          ),
          array(
            "label" => "Jicalapa",
            "value" => "Jicalapa"
          ),
          array(
            "label" => "La Libertad",
            "value" => "La Libertad"
          ),
          array(
            "label" => "Nueva San Salvador (Santa Tecla)",
            "value" => "Nueva San Salvador (Santa Tecla)"
          ),
          array(
            "label" => "Nuevo Cuscatlán",
            "value" => "Nuevo Cuscatlán"
          ),
          array(
            "label" => "San Juan Opico",
            "value" => "San Juan Opico"
          ),
          array(
            "label" => "Quezaltepeque",
            "value" => "Quezaltepeque"
          ),
          array(
            "label" => "Sacacoyo",
            "value" => "Sacacoyo"
          ),
          array(
            "label" => "San José Villanueva",
            "value" => "San José Villanueva"
          ),
          array(
            "label" => "San Matías",
            "value" => "San Matías"
          ),
          array(
            "label" => "San Pablo Tacachico",
            "value" => "San Pablo Tacachico"
          ),
          array(
            "label" => "Talnique",
            "value" => "Talnique"
          ),
          array(
            "label" => "Tamanique",
            "value" => "Tamanique"
          ),
          array(
            "label" => "Teotepeque",
            "value" => "Teotepeque"
          ),
          array(
            "label" => "Tepecoyo",
            "value" => "Tepecoyo"
          ),
          array(
            "label" => "Zaragoza",
            "value" => "Zaragoza"
          )
        )
      )
    );

    array_push($data,
      array(
        "label" => "San Vicente",
        "value" => "San Vicente",
        "child" => array(
          array(
            "label" => "Apastepeque",
            "value" => "Apastepeque"
          ),
          array(
            "label" => "Guadalupe",
            "value" => "Guadalupe"
          ),
          array(
            "label" => "San Cayetano Istepeque",
            "value" => "San Cayetano Istepeque"
          ),
          array(
            "label" => "San Esteban Catarina",
            "value" => "San Esteban Catarina"
          ),
          array(
            "label" => "San Ildefonso",
            "value" => "San Ildefonso"
          ),
          array(
            "label" => "San Lorenzo",
            "value" => "San Lorenzo"
          ),
          array(
            "label" => "San Sebastián",
            "value" => "San Sebastián"
          ),
          array(
            "label" => "San Vicente",
            "value" => "San Vicente"
          ),
          array(
            "label" => "Santa Clara",
            "value" => "Santa Clara"
          ),
          array(
            "label" => "Santo Domingo",
            "value" => "Santo Domingo"
          ),
          array(
            "label" => "Tecoluca",
            "value" => "Tecoluca"
          ),
          array(
            "label" => "Tepetitán",
            "value" => "Tepetitán"
          ),
          array(
            "label" => "Verapaz",
            "value" => "Verapaz"
          )
        )
      )
    );

    array_push($data,
      array(
        "label" => "Cabañas",
        "value" => "Cabañas",
        "child" => array(
          array(
            "label" => "Cinquera",
            "value" => "Cinquera"
          ),
          array(
            "label" => "Dolores",
            "value" => "Dolores"
          ),
          array(
            "label" => "Guacotecti",
            "value" => "Guacotecti"
          ),
          array(
            "label" => "Ilobasco",
            "value" => "Ilobasco"
          ),
          array(
            "label" => "Jutiapa",
            "value" => "Jutiapa"
          ),
          array(
            "label" => "San Isidro",
            "value" => "San Isidro"
          ),
          array(
            "label" => "Sensuntepeque",
            "value" => "Sensuntepeque"
          ),
          array(
            "label" => "Tejutepeque",
            "value" => "Tejutepeque"
          ),
          array(
            "label" => "Victoria",
            "value" => "Victoria"
          )
        )
      )
    );
    array_push($data,
      array(
        "label" => "La Paz",
        "value" => "La Paz",
        "child" => array(
          array(
            "label" => "Cuyultitán",
            "value" => "Cuyultitán"
          ),
          array(
            "label" => "El Rosario",
            "value" => "El Rosario"
          ),
          array(
            "label" => "Jerusalén",
            "value" => "Jerusalén"
          ),
          array(
            "label" => "Mercedes La Ceiba",
            "value" => "Mercedes La Ceiba"
          ),
          array(
            "label" => "Olocuilta",
            "value" => "Olocuilta"
          ),
          array(
            "label" => "Paraíso de Osorio",
            "value" => "Paraíso de Osorio"
          ),
          array(
            "label" => "San Antonio Masahuat",
            "value" => "San Antonio Masahuat"
          ),
          array(
            "label" => "San Emigdio",
            "value" => "San Emigdio"
          ),
          array(
            "label" => "San Francisco Chinameca",
            "value" => "San Francisco Chinameca"
          ),
          array(
            "label" => "San Juan Nonualco",
            "value" => "San Juan Nonualco"
          ),
          array(
            "label" => "San Juan Talpa",
            "value" => "San Juan Talpa"
          ),
          array(
            "label" => "San Juan Tepezontes",
            "value" => "San Juan Tepezontes"
          ),
          array(
            "label" => "San Luis Talpa",
            "value" => "San Luis Talpa"
          ),
          array(
            "label" => "San Luis La Herradura",
            "value" => "San Luis La Herradura"
          ),
          array(
            "label" => "San Miguel Tepezontes",
            "value" => "San Miguel Tepezontes"
          ),
          array(
            "label" => "San Pedro Masahuat",
            "value" => "San Pedro Masahuat"
          ),
          array(
            "label" => "San Pedro Nonualco",
            "value" => "San Pedro Nonualco"
          ),
          array(
            "label" => "San Rafael Obrajuelo",
            "value" => "San Rafael Obrajuelo"
          ),
          array(
            "label" => "Santa María Ostuma",
            "value" => "Santa María Ostuma"
          ),
          array(
            "label" => "Santiago Nonualco",
            "value" => "Santiago Nonualco"
          ),
          array(
            "label" => "Tapalhuaca",
            "value" => "Tapalhuaca"
          ),
          array(
            "label" => "Zacatecoluca",
            "value" => "Zacatecoluca"
          )
        )
      )
    );
    array_push($data,
      array(
        "label" => "Usulután",
        "value" => "Usulután",
        "child" => array(
          array(
            "label" => "Alegría",
            "value" => "Alegría"
          ),
          array(
            "label" => "Berlín",
            "value" => "Berlín"
          ),
          array(
            "label" => "California",
            "value" => "California"
          ),
          array(
            "label" => "Concepción Batres",
            "value" => "Concepción Batres"
          ),
          array(
            "label" => "El Triunfo",
            "value" => "El Triunfo"
          ),
          array(
            "label" => "Ereguayquín",
            "value" => "Ereguayquín"
          ),
          array(
            "label" => "Estanzuelas",
            "value" => "Estanzuelas"
          ),
          array(
            "label" => "Jiquilisco",
            "value" => "Jiquilisco"
          ),
          array(
            "label" => "Jucuapa",
            "value" => "Jucuapa"
          ),
          array(
            "label" => "Jucuarán",
            "value" => "Jucuarán"
          ),
          array(
            "label" => "Mercedes Umaña",
            "value" => "Mercedes Umaña"
          ),
          array(
            "label" => "Nueva Granada",
            "value" => "Nueva Granada"
          ),
          array(
            "label" => "Ozatlán",
            "value" => "Ozatlán"
          ),
          array(
            "label" => "Puerto El Triunfo",
            "value" => "Puerto El Triunfo"
          ),
          array(
            "label" => "San Agustín",
            "value" => "San Agustín"
          ),
          array(
            "label" => "San Buenaventura",
            "value" => "San Buenaventura"
          ),
          array(
            "label" => "San Dionisio",
            "value" => "San Dionisio"
          ),
          array(
            "label" => "San Francisco Javier",
            "value" => "San Francisco Javier"
          ),
          array(
            "label" => "Santa Elena",
            "value" => "Santa Elena"
          ),
          array(
            "label" => "Santa María",
            "value" => "Santa María"
          ),
          array(
            "label" => "Santiago de María",
            "value" => "Santiago de María"
          ),
          array(
            "label" => "Tecapán",
            "value" => "Tecapán"
          ),
          array(
            "label" => "Usulután",
            "value" => "Usulután"
          )
        )
      )
    );
    array_push($data,
      array(
        "label" => "San Miguel",
        "value" => "San Miguel",
        "child" => array(
          array(
            "label" => "Carolina",
            "value" => "Carolina"
          ),
          array(
            "label" => "Chapeltique",
            "value" => "Chapeltique"
          ),
          array(
            "label" => "Chinameca",
            "value" => "Chinameca"
          ),
          array(
            "label" => "Chirilagua",
            "value" => "Chirilagua"
          ),
          array(
            "label" => "Ciudad Barrios",
            "value" => "Ciudad Barrios"
          ),
          array(
            "label" => "Comacarán",
            "value" => "Comacarán"
          ),
          array(
            "label" => "El Tránsito",
            "value" => "El Tránsito"
          ),
          array(
            "label" => "Lolotique",
            "value" => "Lolotique"
          ),
          array(
            "label" => "Moncagua",
            "value" => "Moncagua"
          ),
          array(
            "label" => "Nueva Guadalupe",
            "value" => "Nueva Guadalupe"
          ),
          array(
            "label" => "Nuevo Edén de San Juan",
            "value" => "Nuevo Edén de San Juan"
          ),
          array(
            "label" => "Quelepa",
            "value" => "Quelepa"
          ),
          array(
            "label" => "San Antonio del Mosco",
            "value" => "San Antonio del Mosco"
          ),
          array(
            "label" => "San Gerardo",
            "value" => "San Gerardo"
          ),
          array(
            "label" => "San Jorge",
            "value" => "San Jorge"
          ),
          array(
            "label" => "San Luis de la Reina",
            "value" => "San Luis de la Reina"
          ),
          array(
            "label" => "San Miguel",
            "value" => "San Miguel"
          ),
          array(
            "label" => "San Rafael Oriente",
            "value" => "San Rafael Oriente"
          ),
          array(
            "label" => "Sesori",
            "value" => "Sesori"
          ),
          array(
            "label" => "Uluazapa",
            "value" => "Uluazapa"
          )
        )
      )
    );
    array_push($data,
      array(
        "label" => "La Unión",
        "value" => "La Unión",
        "child" => array(
          array(
            "label" => "Anamorós",
            "value" => "Anamorós"
          ),
          array(
            "label" => "Bolivar",
            "value" => "Bolivar"
          ),
          array(
            "label" => "Concepción de Oriente",
            "value" => "Concepción de Oriente"
          ),
          array(
            "label" => "Conchagua",
            "value" => "Conchagua"
          ),
          array(
            "label" => "El Carmen",
            "value" => "El Carmen"
          ),
          array(
            "label" => "El Sauce",
            "value" => "El Sauce"
          ),
          array(
            "label" => "Intipucá",
            "value" => "Intipucá"
          ),
          array(
            "label" => "La Unión",
            "value" => "La Unión"
          ),
          array(
            "label" => "Lislique",
            "value" => "Lislique"
          ),
          array(
            "label" => "Meanguera del Golfo",
            "value" => "Meanguera del Golfo"
          ),
          array(
            "label" => "Nueva Esparta",
            "value" => "Nueva Esparta"
          ),
          array(
            "label" => "Pasaquina",
            "value" => "Pasaquina"
          ),
          array(
            "label" => "Polorós",
            "value" => "Polorós"
          ),
          array(
            "label" => "San Alejo",
            "value" => "San Alejo"
          ),
          array(
            "label" => "San José",
            "value" => "San José"
          ),
          array(
            "label" => "Santa Rosa de Lima",
            "value" => "Santa Rosa de Lima"
          ),
          array(
            "label" => "Yayantique",
            "value" => "Yayantique"
          ),
          array(
            "label" => "Yucuaiquín",
            "value" => "Yucuaiquín"
          )
        )
      )
    );

    array_push($data,
      array(
        "label" => "Morazán",
        "value" => "Morazán",
        "child" => array(
          array(
            "label" => "Arambala",
            "value" => "Arambala"
          ),
          array(
            "label" => "Cacaopera",
            "value" => "Cacaopera"
          ),
          array(
            "label" => "Chilanga",
            "value" => "Chilanga"
          ),
          array(
            "label" => "Corinto",
            "value" => "Corinto"
          ),
          array(
            "label" => "Delicias de Concepción",
            "value" => "Delicias de Concepción"
          ),
          array(
            "label" => "El Divisadero",
            "value" => "El Divisadero"
          ),
          array(
            "label" => "El Rosario",
            "value" => "El Rosario"
          ),
          array(
            "label" => "Gualococti",
            "value" => "Gualococti"
          ),
          array(
            "label" => "Guatajiagua",
            "value" => "Guatajiagua"
          ),
          array(
            "label" => "Joateca",
            "value" => "Joateca"
          ),
          array(
            "label" => "Jocoaitique",
            "value" => "Jocoaitique"
          ),
          array(
            "label" => "Jocoro",
            "value" => "Jocoro"
          ),
          array(
            "label" => "Lolotiquillo",
            "value" => "Lolotiquillo"
          ),
          array(
            "label" => "Meanguera",
            "value" => "Meanguera"
          ),
          array(
            "label" => "Osicala",
            "value" => "Osicala"
          ),
          array(
            "label" => "Perquín",
            "value" => "Perquín"
          ),
          array(
            "label" => "San Carlos",
            "value" => "San Carlos"
          ),
          array(
            "label" => "San Fernando",
            "value" => "San Fernando"
          ),
          array(
            "label" => "San Francisco Gotera",
            "value" => "San Francisco Gotera"
          ),
          array(
            "label" => "San Isidro",
            "value" => "San Isidro"
          ),
          array(
            "label" => "San Simón",
            "value" => "San Simón"
          ),
          array(
            "label" => "Sensembra",
            "value" => "Sensembra"
          ),
          array(
            "label" => "Sociedad",
            "value" => "Sociedad"
          ),
          array(
            "label" => "Torola",
            "value" => "Torola"
          ),
          array(
            "label" => "Yamabal",
            "value" => "Yamabal"
          ),
          array(
            "label" => "Yoloaiquín",
            "value" => "Yoloaiquín"
          )
        )
      )
    );



    return $data;
  }


  public function getMunicipality()
  {
    $data = array();

    array_push($data,
          array(
            "label" => "Ahuachapán",
            "value" => "Ahuachapán"
          ),
          array(
            "label" => "Apaneca",
            "value" => "Apaneca"
          ),
          array(
            "label" => "Atiquizaya",
            "value" => "Atiquizaya"
          ),
          array(
            "label" => "Concepción de Ataco",
            "value" => "Concepción de Ataco"
          ),
          array(
            "label" => "El Refugio",
            "value" => "El Refugio"
          ),
          array(
            "label" => "Guaymango",
            "value" => "Guaymango"
          ),
          array(
            "label" => "Jujutla",
            "value" => "Jujutla"
          ),
          array(
            "label" => "San Francisco Menéndez",
            "value" => "San Francisco Menéndez"
          ),
          array(
            "label" => "San Lorenzo",
            "value" => "San Lorenzo"
          ),
          array(
            "label" => "San Pedro Puxtla",
            "value" => "San Pedro Puxtla"
          ),
          array(
            "label" => "Tacuba",
            "value" => "Tacuba"
          ),
          array(
            "label" => "Turín",
            "value" => "Turín"
          ),
          array(
            "label" => "Candelaria de la Frontera",
            "value" => "Candelaria de la Frontera"
          ),
          array(
            "label" => "Chalchuapa",
            "value" => "Chalchuapa"
          ),
          array(
            "label" => "Coatepeque",
            "value" => "Coatepeque"
          ),
          array(
            "label" => "El Congo",
            "value" => "El Congo"
          ),
          array(
            "label" => "El Porvenir",
            "value" => "El Porvenir"
          ),
          array(
            "label" => "Masahuat",
            "value" => "Masahuat"
          ),
          array(
            "label" => "Metapán",
            "value" => "Metapán"
          ),
          array(
            "label" => "San Antonio Pajonal",
            "value" => "San Antonio Pajonal"
          ),
          array(
            "label" => "San Sebastián Salitrillo",
            "value" => "San Sebastián Salitrillo"
          ),
          array(
            "label" => "Santa Ana",
            "value" => "Santa Ana"
          ),
          array(
            "label" => "Santa Rosa Guachipilín",
            "value" => "Santa Rosa Guachipilín"
          ),
          array(
            "label" => "Santiago de la Frontera",
            "value" => "Santiago de la Frontera"
          ),
          array(
            "label" => "Texistepeque",
            "value" => "Texistepeque"
          ),
          array(
            "label" => "Acajutla",
            "value" => "Acajutla"
          ),
          array(
            "label" => "Armenia",
            "value" => "Armenia"
          ),
          array(
            "label" => "Caluco",
            "value" => "Caluco"
          ),
          array(
            "label" => "Cuisnahuat",
            "value" => "Cuisnahuat"
          ),
          array(
            "label" => "Izalco",
            "value" => "Izalco"
          ),
          array(
            "label" => "Juayúa",
            "value" => "Juayúa"
          ),
          array(
            "label" => "Nahuizalco",
            "value" => "Nahuizalco"
          ),
          array(
            "label" => "Nahulingo",
            "value" => "Nahulingo"
          ),
          array(
            "label" => "Salcoatitán",
            "value" => "Salcoatitán"
          ),
          array(
            "label" => "San Antonio del Monte",
            "value" => "San Antonio del Monte"
          ),
          array(
            "label" => "San Julián",
            "value" => "San Julián"
          ),
          array(
            "label" => "Santa Catarina Masahuat",
            "value" => "Santa Catarina Masahuat"
          ),
          array(
            "label" => "Santa Isabel Ishuatán",
            "value" => "Santa Isabel Ishuatán"
          ),
          array(
            "label" => "Santo Domingo Guzmán",
            "value" => "Santo Domingo Guzmán"
          ),
          array(
            "label" => "Sonsonate",
            "value" => "Sonsonate"
          ),
          array(
            "label" => "Sonzacate",
            "value" => "Sonzacate"
          ),
          array(
            "label" => "Agua Caliente",
            "value" => "Agua Caliente"
          ),
          array(
            "label" => "Azacualpa",
            "value" => "Azacualpa"
          ),
          array(
            "label" => "Chalatenango",
            "value" => "Chalatenango"
          ),
          array(
            "label" => "Comalapa",
            "value" => "Comalapa"
          ),
          array(
            "label" => "Citalá",
            "value" => "Citalá"
          ),
          array(
            "label" => "Concepción Quezaltepeque",
            "value" => "Concepción Quezaltepeque"
          ),
          array(
            "label" => "Dulce Nombre de María",
            "value" => "Dulce Nombre de María"
          ),
          array(
            "label" => "El Carrizal",
            "value" => "El Carrizal"
          ),
          array(
            "label" => "El Paraíso",
            "value" => "El Paraíso"
          ),
          array(
            "label" => "La Laguna",
            "value" => "La Laguna"
          ),
          array(
            "label" => "La Palma",
            "value" => "La Palma"
          ),
          array(
            "label" => "La Reina",
            "value" => "La Reina"
          ),
          array(
            "label" => "Las Vueltas",
            "value" => "Las Vueltas"
          ),
          array(
            "label" => "Nueva Concepción",
            "value" => "Nueva Concepción"
          ),
          array(
            "label" => "Nueva Trinidad",
            "value" => "Nueva Trinidad"
          ),
          array(
            "label" => "Nombre de Jesús",
            "value" => "Nombre de Jesús"
          ),
          array(
            "label" => "Ojos de Agua",
            "value" => "Ojos de Agua"
          ),
          array(
            "label" => "Potonico",
            "value" => "Potonico"
          ),
          array(
            "label" => "San Antonio de la Cruz",
            "value" => "San Antonio de la Cruz"
          ),
          array(
            "label" => "San Antonio Los Ranchos",
            "value" => "San Antonio Los Ranchos"
          ),
          array(
            "label" => "San Fernando",
            "value" => "San Fernando"
          ),
          array(
            "label" => "San Francisco Lempa",
            "value" => "San Francisco Lempa"
          ),
          array(
            "label" => "San Francisco Morazán",
            "value" => "San Francisco Morazán"
          ),
          array(
            "label" => "San Ignacio",
            "value" => "San Ignacio"
          ),
          array(
            "label" => "San Isidro Labrador",
            "value" => "San Isidro Labrador"
          ),
          array(
            "label" => "San José Cancasque",
            "value" => "San José Cancasque"
          ),
          array(
            "label" => "San José Las Flores",
            "value" => "San José Las Flores"
          ),
          array(
            "label" => "San Luis del Carmen",
            "value" => "San Luis del Carmen"
          ),
          array(
            "label" => "San Miguel de Mercedes",
            "value" => "San Miguel de Mercedes"
          ),
          array(
            "label" => "San Rafael",
            "value" => "San Rafael"
          ),
          array(
            "label" => "Santa Rita",
            "value" => "Santa Rita"
          ),
          array(
            "label" => "Tejutla",
            "value" => "Tejutla"
          ),
          array(
            "label" => "Candelaria",
            "value" => "Candelaria"
          ),
          array(
            "label" => "Cojutepeque",
            "value" => "Cojutepeque"
          ),
          array(
            "label" => "El Carmen",
            "value" => "El Carmen"
          ),
          array(
            "label" => "El Rosario",
            "value" => "El Rosario"
          ),
          array(
            "label" => "Monte San Juan",
            "value" => "Monte San Juan"
          ),
          array(
            "label" => "Oratorio de Concepción",
            "value" => "Oratorio de Concepción"
          ),
          array(
            "label" => "San Bartolomé Perulapía",
            "value" => "San Bartolomé Perulapía"
          ),
          array(
            "label" => "San Cristóbal",
            "value" => "San Cristóbal"
          ),
          array(
            "label" => "San José Guayabal",
            "value" => "San José Guayabal"
          ),
          array(
            "label" => "San Pedro Perulapán",
            "value" => "San Pedro Perulapán"
          ),
          array(
            "label" => "San Rafael Cedros",
            "value" => "San Rafael Cedros"
          ),
          array(
            "label" => "San Ramón",
            "value" => "San Ramón"
          ),
          array(
            "label" => "Santa Cruz Analquito",
            "value" => "Santa Cruz Analquito"
          ),
          array(
            "label" => "Santa Cruz Michapa",
            "value" => "Santa Cruz Michapa"
          ),
          array(
            "label" => "Suchitoto",
            "value" => "Suchitoto"
          ),
          array(
            "label" => "Tenancingo",
            "value" => "Tenancingo"
          ),
          array(
            "label" => "Aguilares",
            "value" => "Aguilares"
          ),
          array(
            "label" => "Apopa",
            "value" => "Apopa"
          ),
          array(
            "label" => "Ayutuxtepeque",
            "value" => "Ayutuxtepeque"
          ),
          array(
            "label" => "Cuscatancingo",
            "value" => "Cuscatancingo"
          ),
          array(
            "label" => "Ciudad Delgado",
            "value" => "Ciudad Delgado"
          ),
          array(
            "label" => "El Paisnal",
            "value" => "El Paisnal"
          ),
          array(
            "label" => "Guazapa",
            "value" => "Guazapa"
          ),
          array(
            "label" => "Ilopango",
            "value" => "Ilopango"
          ),
          array(
            "label" => "Mejicanos",
            "value" => "Mejicanos"
          ),
          array(
            "label" => "Nejapa",
            "value" => "Nejapa"
          ),
          array(
            "label" => "Panchimalco",
            "value" => "Panchimalco"
          ),
          array(
            "label" => "Rosario de Mora",
            "value" => "Rosario de Mora"
          ),
          array(
            "label" => "San Marcos",
            "value" => "San Marcos"
          ),
          array(
            "label" => "San Martín",
            "value" => "San Martín"
          ),
          array(
            "label" => "San Salvador",
            "value" => "San Salvador"
          ),
          array(
            "label" => "Santiago Texacuangos",
            "value" => "Santiago Texacuangos"
          ),
          array(
            "label" => "Santo Tomás",
            "value" => "Santo Tomás"
          ),
          array(
            "label" => "Soyapango",
            "value" => "Soyapango"
          ),
          array(
            "label" => "Tonacatepeque",
            "value" => "Tonacatepeque"
          ),
          array(
            "label" => "Antiguo Cuscatlán",
            "value" => "Antiguo Cuscatlán"
          ),
          array(
            "label" => "Chiltiupán",
            "value" => "Chiltiupán"
          ),
          array(
            "label" => "Ciudad Arce",
            "value" => "Ciudad Arce"
          ),
          array(
            "label" => "Colón",
            "value" => "Colón"
          ),
          array(
            "label" => "Comasagua",
            "value" => "Comasagua"
          ),
          array(
            "label" => "Huizúcar",
            "value" => "Huizúcar"
          ),
          array(
            "label" => "Jayaque",
            "value" => "Jayaque"
          ),
          array(
            "label" => "Jicalapa",
            "value" => "Jicalapa"
          ),
          array(
            "label" => "La Libertad",
            "value" => "La Libertad"
          ),
          array(
            "label" => "Nueva San Salvador (Santa Tecla)",
            "value" => "Nueva San Salvador (Santa Tecla)"
          ),
          array(
            "label" => "Nuevo Cuscatlán",
            "value" => "Nuevo Cuscatlán"
          ),
          array(
            "label" => "San Juan Opico",
            "value" => "San Juan Opico"
          ),
          array(
            "label" => "Quezaltepeque",
            "value" => "Quezaltepeque"
          ),
          array(
            "label" => "Sacacoyo",
            "value" => "Sacacoyo"
          ),
          array(
            "label" => "San José Villanueva",
            "value" => "San José Villanueva"
          ),
          array(
            "label" => "San Matías",
            "value" => "San Matías"
          ),
          array(
            "label" => "San Pablo Tacachico",
            "value" => "San Pablo Tacachico"
          ),
          array(
            "label" => "Talnique",
            "value" => "Talnique"
          ),
          array(
            "label" => "Tamanique",
            "value" => "Tamanique"
          ),
          array(
            "label" => "Teotepeque",
            "value" => "Teotepeque"
          ),
          array(
            "label" => "Tepecoyo",
            "value" => "Tepecoyo"
          ),
          array(
            "label" => "Zaragoza",
            "value" => "Zaragoza"
          ),
          array(
            "label" => "Apastepeque",
            "value" => "Apastepeque"
          ),
          array(
            "label" => "Guadalupe",
            "value" => "Guadalupe"
          ),
          array(
            "label" => "San Cayetano Istepeque",
            "value" => "San Cayetano Istepeque"
          ),
          array(
            "label" => "San Esteban Catarina",
            "value" => "San Esteban Catarina"
          ),
          array(
            "label" => "San Ildefonso",
            "value" => "San Ildefonso"
          ),
          array(
            "label" => "San Lorenzo",
            "value" => "San Lorenzo"
          ),
          array(
            "label" => "San Sebastián",
            "value" => "San Sebastián"
          ),
          array(
            "label" => "San Vicente",
            "value" => "San Vicente"
          ),
          array(
            "label" => "Santa Clara",
            "value" => "Santa Clara"
          ),
          array(
            "label" => "Santo Domingo",
            "value" => "Santo Domingo"
          ),
          array(
            "label" => "Tecoluca",
            "value" => "Tecoluca"
          ),
          array(
            "label" => "Tepetitán",
            "value" => "Tepetitán"
          ),
          array(
            "label" => "Verapaz",
            "value" => "Verapaz"
          ),
          array(
            "label" => "Cinquera",
            "value" => "Cinquera"
          ),
          array(
            "label" => "Dolores",
            "value" => "Dolores"
          ),
          array(
            "label" => "Guacotecti",
            "value" => "Guacotecti"
          ),
          array(
            "label" => "Ilobasco",
            "value" => "Ilobasco"
          ),
          array(
            "label" => "Jutiapa",
            "value" => "Jutiapa"
          ),
          array(
            "label" => "San Isidro",
            "value" => "San Isidro"
          ),
          array(
            "label" => "Sensuntepeque",
            "value" => "Sensuntepeque"
          ),
          array(
            "label" => "Tejutepeque",
            "value" => "Tejutepeque"
          ),
          array(
            "label" => "Victoria",
            "value" => "Victoria"
          ),
          array(
            "label" => "Cuyultitán",
            "value" => "Cuyultitán"
          ),
          array(
            "label" => "El Rosario",
            "value" => "El Rosario"
          ),
          array(
            "label" => "Jerusalén",
            "value" => "Jerusalén"
          ),
          array(
            "label" => "Mercedes La Ceiba",
            "value" => "Mercedes La Ceiba"
          ),
          array(
            "label" => "Olocuilta",
            "value" => "Olocuilta"
          ),
          array(
            "label" => "Paraíso de Osorio",
            "value" => "Paraíso de Osorio"
          ),
          array(
            "label" => "San Antonio Masahuat",
            "value" => "San Antonio Masahuat"
          ),
          array(
            "label" => "San Emigdio",
            "value" => "San Emigdio"
          ),
          array(
            "label" => "San Francisco Chinameca",
            "value" => "San Francisco Chinameca"
          ),
          array(
            "label" => "San Juan Nonualco",
            "value" => "San Juan Nonualco"
          ),
          array(
            "label" => "San Juan Talpa",
            "value" => "San Juan Talpa"
          ),
          array(
            "label" => "San Juan Tepezontes",
            "value" => "San Juan Tepezontes"
          ),
          array(
            "label" => "San Luis Talpa",
            "value" => "San Luis Talpa"
          ),
          array(
            "label" => "San Luis La Herradura",
            "value" => "San Luis La Herradura"
          ),
          array(
            "label" => "San Miguel Tepezontes",
            "value" => "San Miguel Tepezontes"
          ),
          array(
            "label" => "San Pedro Masahuat",
            "value" => "San Pedro Masahuat"
          ),
          array(
            "label" => "San Pedro Nonualco",
            "value" => "San Pedro Nonualco"
          ),
          array(
            "label" => "San Rafael Obrajuelo",
            "value" => "San Rafael Obrajuelo"
          ),
          array(
            "label" => "Santa María Ostuma",
            "value" => "Santa María Ostuma"
          ),
          array(
            "label" => "Santiago Nonualco",
            "value" => "Santiago Nonualco"
          ),
          array(
            "label" => "Tapalhuaca",
            "value" => "Tapalhuaca"
          ),
          array(
            "label" => "Zacatecoluca",
            "value" => "Zacatecoluca"
          ),
          array(
            "label" => "Alegría",
            "value" => "Alegría"
          ),
          array(
            "label" => "Berlín",
            "value" => "Berlín"
          ),
          array(
            "label" => "California",
            "value" => "California"
          ),
          array(
            "label" => "Concepción Batres",
            "value" => "Concepción Batres"
          ),
          array(
            "label" => "El Triunfo",
            "value" => "El Triunfo"
          ),
          array(
            "label" => "Ereguayquín",
            "value" => "Ereguayquín"
          ),
          array(
            "label" => "Estanzuelas",
            "value" => "Estanzuelas"
          ),
          array(
            "label" => "Jiquilisco",
            "value" => "Jiquilisco"
          ),
          array(
            "label" => "Jucuapa",
            "value" => "Jucuapa"
          ),
          array(
            "label" => "Jucuarán",
            "value" => "Jucuarán"
          ),
          array(
            "label" => "Mercedes Umaña",
            "value" => "Mercedes Umaña"
          ),
          array(
            "label" => "Nueva Granada",
            "value" => "Nueva Granada"
          ),
          array(
            "label" => "Ozatlán",
            "value" => "Ozatlán"
          ),
          array(
            "label" => "Puerto El Triunfo",
            "value" => "Puerto El Triunfo"
          ),
          array(
            "label" => "San Agustín",
            "value" => "San Agustín"
          ),
          array(
            "label" => "San Buenaventura",
            "value" => "San Buenaventura"
          ),
          array(
            "label" => "San Dionisio",
            "value" => "San Dionisio"
          ),
          array(
            "label" => "San Francisco Javier",
            "value" => "San Francisco Javier"
          ),
          array(
            "label" => "Santa Elena",
            "value" => "Santa Elena"
          ),
          array(
            "label" => "Santa María",
            "value" => "Santa María"
          ),
          array(
            "label" => "Santiago de María",
            "value" => "Santiago de María"
          ),
          array(
            "label" => "Tecapán",
            "value" => "Tecapán"
          ),
          array(
            "label" => "Usulután",
            "value" => "Usulután"
          ),
          array(
            "label" => "Carolina",
            "value" => "Carolina"
          ),
          array(
            "label" => "Chapeltique",
            "value" => "Chapeltique"
          ),
          array(
            "label" => "Chinameca",
            "value" => "Chinameca"
          ),
          array(
            "label" => "Chirilagua",
            "value" => "Chirilagua"
          ),
          array(
            "label" => "Ciudad Barrios",
            "value" => "Ciudad Barrios"
          ),
          array(
            "label" => "Comacarán",
            "value" => "Comacarán"
          ),
          array(
            "label" => "El Tránsito",
            "value" => "El Tránsito"
          ),
          array(
            "label" => "Lolotique",
            "value" => "Lolotique"
          ),
          array(
            "label" => "Moncagua",
            "value" => "Moncagua"
          ),
          array(
            "label" => "Nueva Guadalupe",
            "value" => "Nueva Guadalupe"
          ),
          array(
            "label" => "Nuevo Edén de San Juan",
            "value" => "Nuevo Edén de San Juan"
          ),
          array(
            "label" => "Quelepa",
            "value" => "Quelepa"
          ),
          array(
            "label" => "San Antonio del Mosco",
            "value" => "San Antonio del Mosco"
          ),
          array(
            "label" => "San Gerardo",
            "value" => "San Gerardo"
          ),
          array(
            "label" => "San Jorge",
            "value" => "San Jorge"
          ),
          array(
            "label" => "San Luis de la Reina",
            "value" => "San Luis de la Reina"
          ),
          array(
            "label" => "San Miguel",
            "value" => "San Miguel"
          ),
          array(
            "label" => "San Rafael Oriente",
            "value" => "San Rafael Oriente"
          ),
          array(
            "label" => "Sesori",
            "value" => "Sesori"
          ),
          array(
            "label" => "Uluazapa",
            "value" => "Uluazapa"
          ),
          array(
            "label" => "Anamorós",
            "value" => "Anamorós"
          ),
          array(
            "label" => "Bolivar",
            "value" => "Bolivar"
          ),
          array(
            "label" => "Concepción de Oriente",
            "value" => "Concepción de Oriente"
          ),
          array(
            "label" => "Conchagua",
            "value" => "Conchagua"
          ),
          array(
            "label" => "El Carmen",
            "value" => "El Carmen"
          ),
          array(
            "label" => "El Sauce",
            "value" => "El Sauce"
          ),
          array(
            "label" => "Intipucá",
            "value" => "Intipucá"
          ),
          array(
            "label" => "La Unión",
            "value" => "La Unión"
          ),
          array(
            "label" => "Lislique",
            "value" => "Lislique"
          ),
          array(
            "label" => "Meanguera del Golfo",
            "value" => "Meanguera del Golfo"
          ),
          array(
            "label" => "Nueva Esparta",
            "value" => "Nueva Esparta"
          ),
          array(
            "label" => "Pasaquina",
            "value" => "Pasaquina"
          ),
          array(
            "label" => "Polorós",
            "value" => "Polorós"
          ),
          array(
            "label" => "San Alejo",
            "value" => "San Alejo"
          ),
          array(
            "label" => "San José",
            "value" => "San José"
          ),
          array(
            "label" => "Santa Rosa de Lima",
            "value" => "Santa Rosa de Lima"
          ),
          array(
            "label" => "Yayantique",
            "value" => "Yayantique"
          ),
          array(
            "label" => "Yucuaiquín",
            "value" => "Yucuaiquín"
          ),
          array(
            "label" => "Arambala",
            "value" => "Arambala"
          ),
          array(
            "label" => "Cacaopera",
            "value" => "Cacaopera"
          ),
          array(
            "label" => "Chilanga",
            "value" => "Chilanga"
          ),
          array(
            "label" => "Corinto",
            "value" => "Corinto"
          ),
          array(
            "label" => "Delicias de Concepción",
            "value" => "Delicias de Concepción"
          ),
          array(
            "label" => "El Divisadero",
            "value" => "El Divisadero"
          ),
          array(
            "label" => "El Rosario",
            "value" => "El Rosario"
          ),
          array(
            "label" => "Gualococti",
            "value" => "Gualococti"
          ),
          array(
            "label" => "Guatajiagua",
            "value" => "Guatajiagua"
          ),
          array(
            "label" => "Joateca",
            "value" => "Joateca"
          ),
          array(
            "label" => "Jocoaitique",
            "value" => "Jocoaitique"
          ),
          array(
            "label" => "Jocoro",
            "value" => "Jocoro"
          ),
          array(
            "label" => "Lolotiquillo",
            "value" => "Lolotiquillo"
          ),
          array(
            "label" => "Meanguera",
            "value" => "Meanguera"
          ),
          array(
            "label" => "Osicala",
            "value" => "Osicala"
          ),
          array(
            "label" => "Perquín",
            "value" => "Perquín"
          ),
          array(
            "label" => "San Carlos",
            "value" => "San Carlos"
          ),
          array(
            "label" => "San Fernando",
            "value" => "San Fernando"
          ),
          array(
            "label" => "San Francisco Gotera",
            "value" => "San Francisco Gotera"
          ),
          array(
            "label" => "San Isidro",
            "value" => "San Isidro"
          ),
          array(
            "label" => "San Simón",
            "value" => "San Simón"
          ),
          array(
            "label" => "Sensembra",
            "value" => "Sensembra"
          ),
          array(
            "label" => "Sociedad",
            "value" => "Sociedad"
          ),
          array(
            "label" => "Torola",
            "value" => "Torola"
          ),
          array(
            "label" => "Yamabal",
            "value" => "Yamabal"
          ),
          array(
            "label" => "Yoloaiquín",
            "value" => "Yoloaiquín"
          )

      );

    return $data;
  }
}
