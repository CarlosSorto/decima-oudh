<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\DigitalMediaManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;

use App\Kwaai\Security\Repositories\Journal\JournalInterface;

use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\EloquentDigitalMediaGridRepository;

use Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\DigitalMediaInterface;

use Carbon\Carbon;

use Illuminate\Config\Repository;

use Illuminate\Translation\Translator;

use Illuminate\Database\DatabaseManager;

class DigitalMediaManager extends AbstractLaravelValidator implements DigitalMediaManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
	 * Eloquent DigitalMedia Grid Repository
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\EloquentDigitalMediaGridRepository
	 *
	 */
	protected $EloquentDigitalMediaGridRepository;

  /**
	 *  DigitalMedia Interface
	 *
	 * @var Mgallegos\DecimaOadh\Oadh\Repositories\DigitalMedia\DigitalMediaInterface
	 *
	 */
	protected $DigitalMedia;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    EloquentDigitalMediaGridRepository $EloquentDigitalMediaGridRepository,
    DigitalMediaInterface $DigitalMedia,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->Journal = $Journal;

    $this->GridEncoder = $GridEncoder;

    $this->EloquentDigitalMediaGridRepository = $EloquentDigitalMediaGridRepository;

    $this->DigitalMedia = $DigitalMedia;

    $this->Carbon = $Carbon;

    $this->DB = $DB;

		$this->Lang = $Lang;

		$this->Config = $Config;
	}

  /**
   * Echo grid data in a jqGrid compatible format
   *
   * @param array $post
   *	All jqGrid posted data
   *
   * @return void
   */
  public function getGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentDigitalMediaGridRepository, $post);
  }

  /**
   * Get ...
   *
   * @return mixed Illuminate\Database\Eloquent\Model if not empty, false if empty
   */
  public function getDigitalMedia($id, $databaseConnectionName = null)
  {
    $DigitalMedia = $this->DigitalMedia->byId($id, $databaseConnectionName);

    if(empty($DigitalMedia))
    {
      return false;
    }

    return $DigitalMedia;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getDigitalMedias()
  {
    $DigitalMedias = array();

    $this->DigitalMedia->byOrganizationAndParent(null, $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($DigitalMedia) use (&$DigitalMedias)
    {
      $Sections = array();
      $this->DigitalMedia->byOrganizationAndParent($DigitalMedia->id, $this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($Section) use (&$Sections)
      {
        array_push($Sections, array('label'=> $Section->name , 'value'=>$Section->id));
      });

      array_push($DigitalMedias, array('label'=> $DigitalMedia->name , 'value'=>$DigitalMedia->id, "sections" => $Sections));
    });

    return $DigitalMedias;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getFrontendDigitalMedias($databaseConnectionName = null)
  {
    $DigitalMedias = array();

    $this->DigitalMedia->getAllWithParent(null, $databaseConnectionName)->each(function($DigitalMedia) use (&$DigitalMedias)
    {

      array_push($DigitalMedias, array('label'=> $DigitalMedia->name , 'value'=>$DigitalMedia->id));
    });

    return $DigitalMedias;
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getSections()
  {
    $Sections = array();

    $this->DigitalMedia->byOrganization($this->AuthenticationManager->getCurrentUserOrganizationId())->each(function($DigitalMedia) use (&$Sections)
    {
      if($DigitalMedia->type == 'S')
      {
        array_push($Sections, array('label'=> $DigitalMedia->name , 'value'=> $DigitalMedia->id));
      }
    });

    return $Sections;
  }

  /**
	 * Create a new ...
	 *
	 * @param array $input
   * 	An array as follows: array('field0'=>$field0, 'field1'=>$field1
   *                            );
   *
	 * @return JSON encoded string
	 *  A string as follows:
	 *	In case of success: {"success" : form.defaultSuccessSaveMessage}
	 */
	public function create(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    unset(
      $input['_token'],
      $input['parent_label']
    );

    if(empty($input['parent_id']))
    {
      $input['type'] = 'M';
    }
    else
    {
      $input['type'] = 'S';
    }


    $input = eloquent_array_filter_for_insert($input);

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

		$input = array_add($input, 'organization_id', $organizationId);


    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
		{
      $DigitalMedia = $this->DigitalMedia->create($input, $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $DigitalMedia->id, 'journalized_type' => $this->DigitalMedia->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::digital-media-management.addedJournal', array('name' => $DigitalMedia->name)), $Journal));

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessSaveMessage'), 'id' => $DigitalMedia->id, 'digitalMedias' => $this->getDigitalMedias() ));
  }


  /**
   * Update an existing ...
   *
   * @param array $input
   * 	An array as follows: array('id' => $id, 'field0'=>$field0, 'field1'=>$field1
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessUpdateMessage}
   */
  public function update(array $input, $DigitalMedia = null, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    // if(!empty($input['table_name_label']))
    // {
    //   $newValues['table_name_id'] = $input['table_name_label'];
    // }

    unset(
      $input['_token'],
      $input['parent_label']
    );

    $input = eloquent_array_filter_for_update($input);

    // if(!empty($input['date']))
    // {
    //   $input['date'] = $this->Carbon->createFromFormat($this->Lang->get('form.phpShortDateFormat'), $input['date'])->format('Y-m-d');
    // }

    // if(!empty($input['amount']))
    // {
    //   $input['amount'] = remove_thousands_separator($input['amount']);
    // }

    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      if(empty($DigitalMedia))
      {
        $DigitalMedia = $this->DigitalMedia->byId($input['id'], $databaseConnectionName);
      }

      $unchangedValues = $DigitalMedia->toArray();

      $this->DigitalMedia->update($input, $DigitalMedia);

      $diff = 0;

      foreach ($input as $key => $value)
      {
        if($unchangedValues[$key] != $value)
        {
          $diff++;

          if($diff == 1)
          {
            $Journal = $this->Journal->create(array('journalized_id' => $DigitalMedia->id, 'journalized_type' => $this->DigitalMedia->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
          }

          if($key == 'status')//Para autocomple de estados
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.status'), 'field_lang_key' => 'form.status', 'old_value' => $this->Lang->get('form.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('form.' . $value)), $Journal);
          }
          else if ($key == 'field1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::digital-media-management.field1'), 'field_lang_key' => 'decima-oadh::digital-media-management.field1', 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'name')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('form.' . camel_case($key)), 'field_lang_key' => 'form.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
          else if ($key == 'chekbox0' || $key == 'chekbox1')
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::digital-media-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::digital-media-management.' . camel_case($key), 'old_value' => $this->Lang->get('journal.' . $unchangedValues[$key]), 'new_value' => $this->Lang->get('journal.' . $value)), $Journal);
          }
          else if($key == 'date')
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->Carbon->createFromFormat('Y-m-d', $unchangedValues[$key], 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $oldValue = '';
            }

            if(!empty($value))
            {
              $newValue = $this->Carbon->createFromFormat('Y-m-d', $value, 'UTC')->format($this->Lang->get('form.phpShortDateFormat'));
            }
            else
            {
              $newValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::digital-media-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::digital-media-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValue), $Journal);
          }
          else if($key == 'table_name_id')//field required
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::digital-media-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::digital-media-management.' . camel_case($key), 'old_value' => $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name, 'new_value' => $newValues[$key]), $Journal);
          }
          else if($key == 'table_name_id')//field not required
          {
            if(!empty($unchangedValues[$key]))
            {
              $oldValue = $this->TableName->byId($unchangedValues[$key], $databaseConnectionName)->name;
            }
            else
            {
              $oldValue = '';
            }

            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::digital-media-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::digital-media-management.' . camel_case($key), 'old_value' => $oldValue, 'new_value' => $newValues[$key]), $Journal);
          }
          else
          {
            $this->Journal->attachDetail($Journal->id, array('field' => $this->Lang->get('decima-oadh::digital-media-management.' . camel_case($key)), 'field_lang_key' => 'decima-oadh::digital-media-management.' . camel_case($key), 'old_value' => $unchangedValues[$key], 'new_value' => $value), $Journal);
          }
        }
      }

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessUpdateMessage'), 'digitalMedias' => $this->getDigitalMedias()));
  }

  /**
   * Delete an existing ... (soft delete)
   *
   * @param array $input
	 * 	An array as follows: array(id => $id);
   *
   * @return JSON encoded string
   *  A string as follows:
   *	In case of success: {"success" : form.defaultSuccessDeleteMessage}
   */
  public function delete(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
  {
    if(empty($organizationId))
    {
      $organizationId = $this->AuthenticationManager->getCurrentUserOrganizationId();
    }

    if(empty($loggedUserId))
    {
      $loggedUserId = $this->AuthenticationManager->getLoggedUserId();
    }

    $this->beginTransaction($openTransaction, $databaseConnectionName);

    try
    {
      $DigitalMedia = $this->DigitalMedia->byId($input['id'], $databaseConnectionName);

      $Journal = $this->Journal->create(array('journalized_id' => $input['id'], 'journalized_type' => $this->DigitalMedia->getTable(), 'user_id' => $loggedUserId, 'organization_id' => $organizationId));
      $this->Journal->attachDetail($Journal->id, array('note' => $this->Lang->get('decima-oadh::digital-media-management.deletedJournal', array('name' => $DigitalMedia->name)), $Journal));

      $this->DigitalMedia->delete(array($input['id']), $databaseConnectionName);

      $this->commit($openTransaction);
    }
    catch (\Exception $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }
    catch (\Throwable $e)
    {
      $this->rollBack($openTransaction);

      throw $e;
    }

    return json_encode(array('success' => $this->Lang->get('form.defaultSuccessDeleteMessage')));
  }

}
