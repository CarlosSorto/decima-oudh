<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\PopulationUploaderManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;
use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;
use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;
use Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface;
use App\Kwaai\Security\Repositories\Journal\JournalInterface;
use Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;
use Mgallegos\DecimaOadh\Oadh\Repositories\Population\EloquentPopTerritoryPopulationGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Population\EloquentPopTerritoryPopulationTempGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Population\EloquentPopAgePopulationGridRepository;
use Mgallegos\DecimaOadh\Oadh\Repositories\Population\EloquentPopAgePopulationTempGridRepository;
use Carbon\Carbon;
use Maatwebsite\Excel\Excel;
use Illuminate\Config\Repository;
use Illuminate\Translation\Translator;
use Illuminate\Database\DatabaseManager;

class PopulationUploaderManager extends AbstractLaravelValidator implements PopulationUploaderManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * File Management Interface
  *
  * @var Mgallegos\DecimaFile\File\Services\FileManagement\FileManagementInterface
  *
  */
  protected $FileManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
  * Uploader Manager
  *
  * @var Mgallegos\DecimaOadh\Oadh\Services\UploaderManagement\UploaderManagementInterface
  *
  */
  protected $UploaderManager;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
  * Maatwebsite\Excel\Excel
  *
  * @var Excel
  *
  */
  protected $Excel;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    FileManagementInterface $FileManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    UploaderManagementInterface $UploaderManager,
    EloquentPopTerritoryPopulationGridRepository $EloquentPopTerritoryPopulationGridRepository,
    EloquentPopTerritoryPopulationTempGridRepository $EloquentPopTerritoryPopulationTempGridRepository,
    EloquentPopAgePopulationGridRepository $EloquentPopAgePopulationGridRepository,
    EloquentPopAgePopulationTempGridRepository $EloquentPopAgePopulationTempGridRepository,
    Carbon $Carbon,
    Excel $Excel,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;
    $this->JournalManager = $JournalManager;
    $this->FileManager = $FileManager;
    $this->Journal = $Journal;
    $this->GridEncoder = $GridEncoder;
    $this->UploaderManager = $UploaderManager;
    $this->EloquentPopTerritoryPopulationGridRepository = $EloquentPopTerritoryPopulationGridRepository;
    $this->EloquentPopTerritoryPopulationTempGridRepository = $EloquentPopTerritoryPopulationTempGridRepository;
    $this->EloquentPopAgePopulationGridRepository = $EloquentPopAgePopulationGridRepository;
    $this->EloquentPopAgePopulationTempGridRepository = $EloquentPopAgePopulationTempGridRepository;
    $this->Carbon = $Carbon;
    $this->Excel = $Excel;
    $this->DB = $DB;
		$this->Lang = $Lang;
		$this->Config = $Config;
	}

  //territoryPopulation
  public function getTerritoryPopulationGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentPopTerritoryPopulationGridRepository, $post);
  }

  public function getTerritoryPopulationTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentPopTerritoryPopulationTempGridRepository, $post);
  }

	public function territoryPopulationProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'department', 'municipality', 'sex', 'population'];
    $tableName = 'OADH_POP_Territory_Population';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year', 'population'));
  }

  public function territoryPopulationCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'department', 'municipality', 'sex', 'population', 'file_id', 'organization_id'];
    $tableName = 'OADH_POP_Territory_Population';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function territoryPopulationDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_POP_Territory_Population');
  }

  public function territoryPopulationDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_POP_Territory_Population');
  }

  //agePopulation
  public function getAgePopulationGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentPopAgePopulationGridRepository, $post);
  }

  public function getAgePopulationTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentPopAgePopulationTempGridRepository, $post);
  }

	public function agePopulationProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['year', 'age', 'population'];
    $tableName = 'OADH_POP_Age_Population';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year', 'age', 'population'));
  }

  public function agePopulationCopyToProduction($input)
  {
    $response = array();
    $columns = ['year', 'age', 'population', 'file_id', 'organization_id'];
    $tableName = 'OADH_POP_Age_Population';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function agePopulationDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_POP_Age_Population');
  }

  public function agePopulationDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_POP_Age_Population');
  }

  //tableName
  public function getTableNameGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameGridRepository, $post);
  }

  public function getTableNameTempGridData(array $post)
  {
    $this->GridEncoder->encodeRequestedData($this->EloquentTableNameTempGridRepository, $post);
  }

	public function tableNameProcessSpreadsheet(array $input, $openTransaction = true, $databaseConnectionName = null, $organizationId = null, $loggedUserId = null)
	{
    $params = array_only($input, array('id', 'name', 'system_route', 'header_rows_number', 'last_row_number', 'decimal_number', 'date_format'));

    unset(
      $input['_token'],
      $input['name'],
      $input['system_route'],
      $input['header_rows_number'],
      $input['last_row_number'],
      $input['date_format'],
      $input['status'],
      $input['columns'],
      $input['view']
    );

    $columns = ['column_1', 'column_2', 'column_3', 'column_4'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->processSpreadsheet($params, $input, $columns, $tableName, array(), array('year'));
  }

  public function tableNameCopyToProduction($input)
  {
    $response = array();
    $columns = ['column_1', 'column_2', 'column_3', 'column_4', 'file_id', 'organization_id'];
    $tableName = 'OADH_Prefix_Table_Name';

    return $this->UploaderManager->copyToProduction($input['file_id'], $tableName, $columns);
  }

  public function tableNameDeleteFromProduction($input)
  {
    return $this->UploaderManager->deleteFromProduction($input['file_id'], 'OADH_Prefix_Table_Name');
  }

  public function tableNameDeleteFile($input)
  {
    return $this->UploaderManager->deleteFile($input['file_id'], 'OADH_Prefix_Table_Name');
  }
}
