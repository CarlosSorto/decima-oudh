<?php
/**
 * @file
 * Module App Management Interface Implementation.
 *
 * All ModuleName code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

namespace Mgallegos\DecimaOadh\Oadh\Services\IntegrityQueryManagement;

use App\Kwaai\System\Services\Validation\AbstractLaravelValidator;

use App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface;

use App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface;

use App\Kwaai\Security\Repositories\Journal\JournalInterface;

use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\IntegrityQueryInterface;

use Mgallegos\DecimaOadh\Oadh\Repositories\FreeVictimsLibertyDeprivation\FreeVictimsLibertyDeprivationInterface;

use Carbon\Carbon;

use Illuminate\Database\DatabaseManager;

use Illuminate\Translation\Translator;

use Illuminate\Config\Repository;

use Illuminate\Cache\CacheManager;

class IntegrityQueryManager extends AbstractLaravelValidator implements IntegrityQueryManagementInterface {

  /**
   * Authentication Management Interface
   *
   * @var App\Kwaai\Security\Services\AuthenticationManagement\AuthenticationManagementInterface
   *
   */
  protected $AuthenticationManager;

  /**
  * Journal Management Interface (Security)
  *
  * @var App\Kwaai\Security\Services\JournalManagement\JournalManagementInterface
  *
  */
  protected $JournalManager;

  /**
  * Journal (Security)
  *
  * @var App\Kwaai\Security\Repositories\Journal\JournalInterface
  *
  */
  protected $Journal;

  /**
	 * Grid Encoder
	 *
	 * @var Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface
	 *
	 */
	protected $GridEncoder;

  /**
   *  IntegrityQuery Interface
   *
   * @var Mgallegos\DecimaOadh\Oadh\Repositories\Integrity\IntegrityQueryInterface
   *
   */
  protected $IntegrityQuery;

  /**
   * Carbon instance
   *
   * @var Carbon\Carbon
   *
   */
  protected $Carbon;

  /**
   * Laravel Database Manager
   *
   * @var Illuminate\Database\DatabaseManager
   *
   */
  protected $DB;

  /**
   * Laravel Translator instance
   *
   * @var Illuminate\Translation\Translator
   *
   */
  protected $Lang;

  /**
   * Laravel Repository instance
   *
   * @var Illuminate\Config\Repository
   *
   */
  protected $Config;

  /**
  * Laravel Cache instance
  *
  * @var \Illuminate\Cache\CacheManager
  *
  */
  protected $Cache;

	public function __construct(
    AuthenticationManagementInterface $AuthenticationManager,
    JournalManagementInterface $JournalManager,
    JournalInterface $Journal,
    RequestedDataInterface $GridEncoder,
    IntegrityQueryInterface $IntegrityQuery,
    Carbon $Carbon,
    DatabaseManager $DB,
    Translator $Lang,
    Repository $Config,
    CacheManager $Cache
  )
	{
    $this->AuthenticationManager = $AuthenticationManager;

    $this->JournalManager = $JournalManager;

    $this->Journal = $Journal;

    $this->GridEncoder = $GridEncoder;

    $this->IntegrityQuery = $IntegrityQuery;

    $this->Carbon = $Carbon;

    $this->DB = $DB;

		$this->Lang = $Lang;

		$this->Config = $Config;
  }
  
  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function  getCrimesVictims (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->crimesVictimsCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);  
    });

    $this->IntegrityQuery->crimesVictimsSexFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['gender'][] = array('label' => $IntegrityQuery->sex, 'value' => $IntegrityQuery->sex);
    });

    $this->IntegrityQuery->crimesVictimsAgeRangeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['ageRange'][] = array('label' => $IntegrityQuery->age_range, 'value' => $IntegrityQuery->age_range);
    });

    $this->IntegrityQuery->crimesVictimsByCrimeBySexByAgeRangeByStatus($input['filter']['crime'], $input['filter']['gender'], $input['filter']['ageRange'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year , 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });

    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getCrimesAccused (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->crimesAccusedCategoryFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['category'][] = array('label' => $IntegrityQuery->category, 'value' => $IntegrityQuery->category);  
    });

    $this->IntegrityQuery->crimesAccusedCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);  
    });    

    $this->IntegrityQuery->crimesAccusedDepartmentFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['department'][] = array('label' => $IntegrityQuery->department, 'value' => $IntegrityQuery->department);
    });

    $this->IntegrityQuery->crimesAccusedMunicipalityFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['municipality'][] = array('label' => $IntegrityQuery->municipality, 'value' => $IntegrityQuery->municipality);
    });

    $this->IntegrityQuery->crimesAccusedByCategoryByDepartmentByMunicipalityByStatus($input['filter']['category'], $input['filter']['crime'], $input['filter']['department'], $input['filter']['municipality'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year , 'value' => $IntegrityQuery->count);
      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getWorkingOfficials (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->workingOfficialsCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);  
    });

    $this->IntegrityQuery->workingOfficialsCategoryFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['category'][] = array('label' => $IntegrityQuery->category, 'value' => $IntegrityQuery->category);
    });

    $this->IntegrityQuery->workingOfficialsSexFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['gender'][] = array('label' => $IntegrityQuery->sex, 'value' => $IntegrityQuery->sex);
    });

    $this->IntegrityQuery->workingOfficialsDepartmentFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['department'][] = array('label' => $IntegrityQuery->department, 'value' => $IntegrityQuery->department);
    });

    $this->IntegrityQuery->workingOfficialsByCrimeByCategoryBySexByDepartmentByStatus($input['filter']['crime'], $input['filter']['category'], $input['filter']['gender'], $input['filter']['department'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year , 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getGenderTrafficking (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->genderTraffickingSexFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['gender'][] = array('label' => $IntegrityQuery->sex, 'value' => $IntegrityQuery->sex);
    });

    $this->IntegrityQuery->genderTraffickingBySexByStatus($input['filter']['gender'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year , 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getTypeTrafficking (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->typeTraffickingCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);
    });

    $this->IntegrityQuery->typeTraffickingByCrimeByStatus($input['filter']['crime'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year , 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getAgeTrafficking (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->ageTraffickingAgeRangeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['ageRange'][] = array('label' => $IntegrityQuery->age_range, 'value' => $IntegrityQuery->age_range);
    });

    $this->IntegrityQuery->ageTraffickingByAgeRangeByStatus($input['filter']['ageRange'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year , 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getMovementFreedom (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->movementFreedomCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);
    });

    $this->IntegrityQuery->movementFreedomDepartmentFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['department'][] = array('label' => $IntegrityQuery->department, 'value' => $IntegrityQuery->department);
    });

    $this->IntegrityQuery->movementFreedomMunicipalityFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['municipality'][] = array('label' => $IntegrityQuery->municipality, 'value' => $IntegrityQuery->municipality);
    });

    $this->IntegrityQuery->movementFreedomAgeRangeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['ageRange'][] = array('label' => $IntegrityQuery->age_range, 'value' => $IntegrityQuery->age_range);
    });

    $this->IntegrityQuery->movementFreedomSexFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['gender'][] = array('label' => $IntegrityQuery->sex, 'value' => $IntegrityQuery->sex);
    });

    // dd($data['gender']);

    $this->IntegrityQuery->movementFreedomByCrimeByDepartmentByMunicipalityByAgeRangeBySexByStatus($input['filter']['crime'], $input['filter']['department'], $input['filter']['municipality'], $input['filter']['ageRange'], $input['filter']['gender'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year , 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getAgeWomen (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->ageWomanAgeRangeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['ageRange'][] = array('label' => $IntegrityQuery->age_range, 'value' => $IntegrityQuery->age_range);
    });

    $this->IntegrityQuery->ageWomanByAgeRangeByStatus($input['filter']['ageRange'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year , 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getTypeWomen (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->typeWomanCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);
    });

    $this->IntegrityQuery->typeWomanByCrimeByStatus($input['filter']['crime'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getStateWomen (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->stateWomanDepartmentFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['department'][] = array('label' => $IntegrityQuery->department, 'value' => $IntegrityQuery->department);
    });

    $this->IntegrityQuery->stateWomanMunicipalityFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['municipality'][] = array('label' => $IntegrityQuery->municipality, 'value' => $IntegrityQuery->municipality);
    });

    $this->IntegrityQuery->stateWomanByDepartmentByMunicipalityByStatus($input['filter']['department'], $input['filter']['municipality'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getTypeSexual (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->typeSexualCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);
    });

    $this->IntegrityQuery->typeSexualDepartmentFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['department'][] = array('label' => $IntegrityQuery->department, 'value' => $IntegrityQuery->department);
    });

    $this->IntegrityQuery->typeSexualMunicipalityFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['municipality'][] = array('label' => $IntegrityQuery->municipality, 'value' => $IntegrityQuery->municipality);
    });

    $this->IntegrityQuery->typeSexualByCrimeByDepartmentByMunicipalityByStatus($input['filter']['crime'], $input['filter']['department'], $input['filter']['municipality'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getGenderSexual (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->genderSexualCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
       $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);
    });

    $this->IntegrityQuery->genderSexualSexFilter()->each(function ($IntegrityQuery) use (&$data) {
       $data['gender'][] = array('label' => $IntegrityQuery->sex, 'value' => $IntegrityQuery->sex);
    });

    $this->IntegrityQuery->genderSexualByCrimeBySexByStatus($input['filter']['crime'], $input['filter']['gender'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getAgeSexual (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );


    $this->IntegrityQuery->ageSexualCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
       $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);
    });

    $this->IntegrityQuery->ageSexualAgeRangeFilter()->each(function ($IntegrityQuery) use (&$data) {
       $data['ageRange'][] = array('label' => $IntegrityQuery->age_range, 'value' => $IntegrityQuery->age_range);
    });

    $this->IntegrityQuery->ageSexualByCrimeByAgeRangeByStatus($input['filter']['crime'], $input['filter']['ageRange'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getStateInjuries (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->stateInjuriesDepartmentFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['department'][] = array('label' => $IntegrityQuery->department, 'value' => $IntegrityQuery->department);
    });

    $this->IntegrityQuery->stateInjuriesMunicipalityFilter()->each(function ($IntegrityQuery) use (&$data) {
       $data['municipality'][] = array('label' => $IntegrityQuery->municipality, 'value' => $IntegrityQuery->municipality);
    });

    $this->IntegrityQuery->stateInjuriesByDepartmentByMunicipalityByStatus($input['filter']['department'], $input['filter']['municipality'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getAgeInjuries (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->ageInjuriesAgeRangeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['ageRange'][] = array('label' => $IntegrityQuery->age_range, 'value' => $IntegrityQuery->age_range);
    });

    $this->IntegrityQuery->ageInjuriesCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);
    });

    $this->IntegrityQuery->ageInjuriesBySexByAgeRangeByStatus($input['filter']['ageRange'], $input['filter']['crime'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }


  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getScheduleInjuries (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->scheduleInjuriesWeaponTypeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['weaponType'][] = array('label' => $IntegrityQuery->weapon_type, 'value' => $IntegrityQuery->weapon_type);
    });

    $this->IntegrityQuery->scheduleInjuriesByAggressorTypeByTimeByStatus($input['filter']['weaponType'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
   */
  public function getAgentInvestigation (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->agentInvestigationCrimeFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['crime'][] = array('label' => $IntegrityQuery->crime, 'value' => $IntegrityQuery->crime);
    });

    $this->IntegrityQuery->agentInvestigationCategoryFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['category'][] = array('label' => $IntegrityQuery->category, 'value' => $IntegrityQuery->category);
    });

    $this->IntegrityQuery->agentInvestigationSexFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['gender'][] = array('label' => $IntegrityQuery->sex, 'value' => $IntegrityQuery->sex);
    });

    $this->IntegrityQuery->agentInvestigationPlaceFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['place'][] = array('label' => $IntegrityQuery->place, 'value' => $IntegrityQuery->place);
    });

    $this->IntegrityQuery->agentInvestigationDepartmentFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['department'][] = array('label' => $IntegrityQuery->department, 'value' => $IntegrityQuery->department);
    });

    $this->IntegrityQuery->agentInvestigationMunicipalityFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['municipality'][] = array('label' => $IntegrityQuery->municipality, 'value' => $IntegrityQuery->municipality);
    });

    $this->IntegrityQuery->agentInvestigationStateFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['state'][] = array('label' => $IntegrityQuery->state, 'value' => $IntegrityQuery->state);
    });

    $this->IntegrityQuery->agentInvestigationByCrimeByCategoryBySexByPlaceByDepartmentByMunicipalityByStateByStatus($input['filter']['crime'], $input['filter']['category'], $input['filter']['gender'], $input['filter']['place'], $input['filter']['department'], $input['filter']['municipality'], $input['filter']['state'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }

  /**
   * Get ...
   *
   * @return array
   *  An array of arrays as follows: array( array('label'=>$name0, 'value'=>$id0), array('label'=>$name1, 'value'=>$id1),…)
  */
  public function getIntegrityViolation (array $input)
  {
    $data = $excel = array();

    $excel['header'] = array(
      'columnA' => 'Años',
      'columnB' => 'Frecuencia',
    );

    $this->IntegrityQuery->integrityViolationSexFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['gender'][] = array('label' => $IntegrityQuery->sex, 'value' => $IntegrityQuery->sex);
    });

    $this->IntegrityQuery->integrityViolationDepartmentFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['department'][] = array('label' => $IntegrityQuery->department, 'value' => $IntegrityQuery->department);
    });

    $this->IntegrityQuery->integrityViolationMunicipalityFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['municipality'][] = array('label' => $IntegrityQuery->municipality, 'value' => $IntegrityQuery->municipality);
    });

    $this->IntegrityQuery->integrityViolationViolatedFactFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['violatedFact'][] = array('label' => $IntegrityQuery->violated_fact, 'value' => $IntegrityQuery->violated_fact);
    });

    $this->IntegrityQuery->integrityViolationDependenciesFilter()->each(function ($IntegrityQuery) use (&$data) {
      $data['dependencies'][] = array('label' => $IntegrityQuery->dependencies, 'value' => $IntegrityQuery->dependencies);
    });

    $this->IntegrityQuery->integrityViolationBySexByAgeByDepartmentByMunicipalityByRightToByViolatedFactByInstitutionsByDependenciesByStatus($input['filter']['gender'], $input['filter']['age'], $input['filter']['department'], $input['filter']['municipality'], $input['filter']['violatedFact'], $input['filter']['dependencies'])->each(function ($IntegrityQuery) use (&$data, &$excel) {
      $data['data'][] = array('label' => $IntegrityQuery->year, 'value' => $IntegrityQuery->count);

      $excel['details']['columnA'][] = $IntegrityQuery->year;
      $excel['details']['columnB'][] = $IntegrityQuery->count;
    });
    $data['labels']['categoryAxis'] = 'Años';
    $data['labels']['valueAxis'] = 'Frecuencia';
    return json_encode(array('data' => $data, 'excel' => $excel));
  }
}
