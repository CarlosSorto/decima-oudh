<?php
/**
 * @file
 * Language lines.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'gridTitle' => 'Publicaciones',
  'new' => 'Nueva publicación',
  'edit' => 'Editar publicación',
  'delete' => 'Eliminar publicación',
  'save' => 'Guardar publicación',
  'formNewTitle' => 'Creación de publicación',
  'formEditTitle' => 'Edición de publicación existente',
  'deleteMessageConfirmation' => '¿Está seguro que desea eliminar la publicación ":name"?',
  'addedJournal' => 'Se agregó la publicación ":name"',
  'deletedJournal' => 'Se eliminó la publicación ":name"',
  'type' => 'Tipos disponibles',
  'B' => 'Boletín',
  'V' => 'Memorias',
  'F' => 'Informe anual',
  'I' => 'Investigación',
  'R' => 'Resumen',
  'title' => 'Título',
  'description' => 'Descripción',
  'isHighlightedGrid' => 'Destacada',
  'isHighlighted' => 'Mostrar como publicación destacada',
  'author' => 'Autor(es)',
  'tags' => 'Tags',
  'imageUrl' => 'Imagen (760x350)',
  'imageUrl1140' => 'Imagen (1140x440)',
  'downloadUrl' => 'URL PDF completo',
  'summaryUrl' => 'URL PDF resumen',
  'infographicUrl' => 'URL PDF infográfico',
  'place' => 'Lugar',
  'lang' => 'Idioma',
  'en' => 'Inglés',
	'es' => 'Español',
  'langGridText' => ':;en:Inglés;es:Español',
  // 'typeGridText' => ':;B:Boletín;V:Investigación;F:Informe anual;I:Investigaciones',
  'typeGridText' => ':;B:Boletín;V:Memorias;F:Informe anual;I:Investigación;R:Resumen',
);
