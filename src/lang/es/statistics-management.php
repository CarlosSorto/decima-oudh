<?php
/**
 * @file
 * Language lines.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'gridMasterTitle' => 'Subscriptores',
  'gridDetailTitle' => 'Descargas de publicaciones',
  'gridDetailTitle3' => 'Descargas de noticias',
  'gridDetailTitle4' => 'Descargas de recomendaciones',
  'gridDetailTitle2' => 'Acceso a gráficas y mapas',
  'right' => 'Nombre del derecho',
  'name' => 'Título',
  'access' => 'Número de accesos',
  'new' => 'Nuevo ...',
  'edit' => 'Editar ...',
  'delete' => 'Eliminar ...',
  'save' => 'Guardar ...',
  'formNewTitle' => 'Creación de ...',
  'formEditTitle' => 'Edición de ... Existente',
  'deleteMessageConfirmation' => '¿Está seguro que desea eliminar el ... ":name"?',
  'addedJournal' => 'Se agregó el ... ":name"',
  'deletedJournal' => 'Se eliminó el ... ":name"',
  'downloads' => 'Número de descargas',
  'subscriberCount' => 'Número de subscriptores',
);
