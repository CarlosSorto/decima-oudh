<?php
/**
 * @file
 * Menus (database) language lines.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'H' => 'Hombre',
  'M' => 'Mujer',
  'N' => 'No aplica',
  'humanRight' => 'Diario/ medio digital',
  'mainHumanRight' => 'Diario/ medio digital perteneciente',
  'formNewTitleTracing' => 'Nuevo Eje',
  'formNewTitleHumanRight' => 'Nuevo Derecho Humano',
  'formNewTitleTopic' => 'Nuevo Tema',
  'formNewTitleViolatedFact' => 'Nuevo Hecho violatorio',
  'formEditTitleTracing' => 'Editar Eje',
  'formEditTitleHumanRight' => 'Editar Derecho Humano',
  'formEditTitleTopic' => 'Editar Tema',
  'formEditTitleViolatedFact' => 'Editar Hecho violatorio',
  'gridTitle' => 'Derechos humanos',
  'new' => 'Nuevo registro',
  'edit' => 'Editar registro',
  'delete' => 'Eliminar registro',
  'save' => 'Guardar cambios',
  'typeGridText' => ':;T:Tema;H:Hecho violatorio',
  'tracing' => 'Eje',
  'humanRight' => 'Derecho Humano',
  'isWebVisible' => 'Visible en web',
  'name' => 'Nombre sistema',
  'esName' => 'Nombre web español',
  'enName' => 'Nombre web inglés',
  'esDescription' => 'Descripción web español',
  'enDescription' => 'Descripción web inglés',
  'position' => 'Posición',
  // 'descriptionHelper' => ' (visible solo para derechos humanos)',
);
