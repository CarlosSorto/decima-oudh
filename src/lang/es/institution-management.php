<?php
/**
 * @file
 * Language lines.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'gridTitle' => 'Instituciones',
  'new' => 'Nueva institución',
  'edit' => 'Editar institución',
  'delete' => 'Eliminar institución',
  'save' => 'Guardar institución',
  'formNewTitle' => 'Creación de institución',
  'formEditTitle' => 'Edición de institución existente',
  'deleteMessageConfirmation' => '¿Está seguro que desea eliminar la institución ":name"?',
  'addedJournal' => 'Se agregó la institución ":name"',
  'deletedJournal' => 'Se eliminó la institución ":name"'
);
