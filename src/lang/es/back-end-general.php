<?php
/**
 * @file
 * Menus (database) language lines.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'tempData' => 'Datos temporales',
  'prodData' => 'Datos en producción',
  'copyToProd' => '¿Desea copiar los datos a producción?',
  'alreadyCopied' => 'Los datos ya fueron copiados a producción',
  'copiedSuccessfully' => 'Los registros de copiaron exitosamente',
  'deletedSuccessfully' => 'Los datos en producción se eliminaron exitosamente',
  'fileDeletedSuccessfully' => 'El archivo se eliminó exitosamente',
  'errorDeletingFile' => 'El archivo no puede ser eliminado debido a que ya ha sido copiado a la base de datos de producción',
  'fileNeverCopied' => 'El archivo no ha sido copiado a la base de datos de producción',
  'court' => 'Juzgado',
  'process' => 'Procesar hoja de cálculo',
  'general' => 'Datos Generales',
  'processLegend' => 'Variables por columnas de la hoja de calculo',
  'headerRowsNumber' => 'Filas de encabezados',
  'lastRowsNumber' => 'Ultima fila con datos',
  'dateFormat' => 'Formato de fecha',
  'timeFormat' => 'Formato de hora',
  'copyToProductionConfirmationMessage' => '¿Está seguro que desea copiar los registros a producción? Una vez se hayan copiado, se visualizarán a través de la gráfica del sitio web.',
  'copyToProd' => 'Copiar a producción',
  'deleteFromProd' => 'Eliminar de producción',
  'deleteFile' => 'Eliminar archivo',
  'deleteFromProdMessageConfirmation' => '¿Desea eliminar de producción?, una vez eliminados los datos no podrán ser recuperados',
  'deleteFileMessageConfirmation' => '¿Desea eliminar este archivo?, una vez eliminado no podrá ser recuperado',
  'generateMessageConfirmation' => '¿Desea mover los datos a producción?',
  'processedSuccessfully' => 'La hoja de cálculo se procesó exitosamente',
  'numberOfLinesExceeded' => 'El número maximo de filas es de :maxRows',
);
