<?php
/**
 * @file
 * Language lines.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'gridTitle' => 'Derechos',
  'new' => 'Nuevo derecho',
  'edit' => 'Editar derecho',
  'delete' => 'Eliminar derecho',
  'save' => 'Guardar derecho',
  'formNewTitle' => 'Creación de derecho',
  'formEditTitle' => 'Edición de derecho existente',
  'deleteMessageConfirmation' => '¿Está seguro que desea eliminar el derecho ":name"?',
  'addedJournal' => 'Se agregó el derecho ":name"',
  'deletedJournal' => 'Se eliminó el derecho ":name"'
);
