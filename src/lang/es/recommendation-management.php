<?php
/**
 * @file
 * Menus (database) language lines.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'gridMasterTitle' => 'Recomendaciones',
  'gridInsDetailTitle' => 'Instituciones relacionadas',
  'gridRigDetailTitle' => 'Derechos relacionadas',
  'newMaster' => 'Nueva recomendación',
  'editMaster' => 'Editar recomendación',
  'deleteMaster' => 'Eliminar recomendación',
  // 'voidMaster' => 'Anular ....',
  // 'authorizeMaster' => 'Autorizar ....',
  'saveMaster' => 'Guardar recomendación',
  'editDetail' => 'Editar recomendación',
  'deleteDetail' => 'Eliminar recomendación',
  'saveDetail' => 'Guardar recomendación',
  'formNewTitle' => 'Creación de recomendación',
  'formEditTitle' => 'Edición de recomendación existente',
  'formInsDetailTitle' => 'Asociar instituciones',
  'formRigDetailTitle' => 'Asociar derechos',
  // 'deleteMessageConfirmation' => '¿Está seguro que desea eliminar el ... ":name"?',
  //'voidMessageConfirmation' => '¿Está seguro que desea anular el ... ":name"?',
  'addedMasterJournal' => 'Se agregó la recomendación ":name"',
  'deletedMasterJournal' => 'Se eliminó la recomendación ":name"',
  // 'authorizedMasterJournal' => 'Se autorizó el ... ":name"',
  // 'voidMasterJournal' => 'Se anuló el ... ":name"',
  'addedInsDetailJournal' => 'Se agregó la institución ":detailName" a la recomendación ":masterName"',
  'deletedInsDetailJournal' => 'Se eliminó la institución ":detailName" a la recomendación ":masterName"',
  'addedRigDetailJournal' => 'Se agregó el derecho ":detailName" a la recomendación ":masterName"',
  'deletedRigDetailJournal' => 'Se eliminó la derecho ":detailName" a la recomendación ":masterName"',
);
