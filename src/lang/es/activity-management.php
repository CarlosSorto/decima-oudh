<?php
/**
 * @file
 * Language lines.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'gridMasterTitle' => 'Actividades',
  'newMaster' => 'Nueva actividad',
  'editMaster' => 'Editar actividad',
  'deleteMaster' => 'Eliminar actividad',
  'deleteDetail' => 'Eliminar imagen de la galeria',
  'saveMaster' => 'Guardar actividad',
  'saveDetail' => 'Guardar imagen en la galeria',
  'addedDetailJournal' => 'Se agrego una imagen a la galeria de la actividad.',
  'formNewTitle' => 'Creación de actividad',
  'formDetailTitle' => 'Galeria de fotos',
  'formEditTitle' => 'Edición de actividad existente',
  'deleteMessageConfirmation' => '¿Está seguro que desea eliminar la actividad ":name"?',
  'addedMasterJournal' => 'Se agregó la actividad ":name"',
  'deletedJournal' => 'Se eliminó la actividad ":name"',
  'deletedDetailJournal' => 'Se eliminó la imagen :detailName de la actividad :masterName',
);
