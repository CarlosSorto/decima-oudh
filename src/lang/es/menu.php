<?php
/**
 * @file
 * Menus (database) language lines.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'module' => 'OUDH',
 	'setup' => 'Medios de comunicación',
  'appManagement' => 'Gestión de ...',
 	'transactions' => 'Transacciones (OADH)',
  'appManagement' => 'Gestión de ...',
  'mediaMonitoring' => 'Monitoreo de medios'
);
