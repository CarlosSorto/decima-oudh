<?php
/**
 * @file
 * Language lines.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'gridTitle' => 'Videos',
  'new' => 'Nuevo video',
  'edit' => 'Editar video',
  'delete' => 'Eliminar video',
  'save' => 'Guardar video',
  'formNewTitle' => 'Creación de video',
  'formEditTitle' => 'Edición de video existente',
  'deleteMessageConfirmation' => '¿Está seguro que desea eliminar el video ":name"?',
  'addedJournal' => 'Se agregó el video ":name"',
  'deletedJournal' => 'Se eliminó el video ":name"',
  'imageUrl' => 'URL YouTube',
  'isHighlighted' => 'Video destacado',
  'isHighlightedForm' => 'Video destacado (se mostrará en la página de información de prensa)',
);
