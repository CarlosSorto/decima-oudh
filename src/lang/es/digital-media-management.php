<?php
/**
 * @file
 * Menus (database) language lines.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(

  'digitalMedia' => 'Diario/ medio digital',

  'mainDigitalMedia' => 'Diario/ medio digital perteneciente',

  'formNewTitleDigitalMedia' => 'Nuevo Medio digital',

  'formEditTitleDigitalMedia' => 'Editar Medio digital',

  'formNewTitleSection' => 'Nueva sección',

  'formEditTitleSection' => 'Editar sección',

  'gridTitle' => 'Medio Digitales',

  'new' => 'Nuevo registro',

  'edit' => 'Editar registro',

  'delete' => 'Eliminar registro',

  'save' => 'Guardar cambios',

  'newMedia' => 'Nuevo medio digital',

  'newSection' => 'Nueva sección',

  'abbreviation' => 'Abreviación',

  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
);
