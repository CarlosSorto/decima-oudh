<?php
/**
 * @file
 * Language lines.
 *
 * All DecimaModule code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(

  'gridTitle' => 'Suscriptores',

  'email' => 'Correo Electrónico',

  'addedJournal' => 'Se agregó el ... ":name"',

  'deletedJournal' => 'Se eliminó el ... ":name"'
);
