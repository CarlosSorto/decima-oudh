<?php
/**
 * @file
 * Menus (database) language lines.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(
  'court' => 'Juzgado',
  'year' => 'Año',
  'sex' => 'Sexo',
  'men' => 'Hombres',
  'women' => 'Mujeres',
  'sexGridText' => ':;H:Hombre;M:Mujer',
  'statusGridText' => ':;T:Temporal;P:Producción',
  'gender' => 'Género',
  'status' => 'Estado',
  'department' => 'Departamento',
  'situation' => 'Situación',
  'phase' => 'Fase',
  'crime' => 'Delito',
  'municipality' => 'Municipio',
  'profession' => 'Profesión',
  'station' => 'Estación',
  'unit' => 'Unidad',
  'capacity' => 'Capacidad',
  'manPopulation' => 'Población hombres',
  'womenPopulation' => 'Población mujeres',
  'age' => 'Edad',
  'disease' => 'Enfermedad',
  'quantity' => 'Cantidad',
  'jail' => 'Penal',
  'chronicDisease' => 'Enfermedad crónica',
  'ageRange' => 'Rango de edad',
  'category' => 'Categoría',
  'total' => 'Total',
  'aggressorType' => 'Tipo de victimario',
  'time' => 'Hora',
  'code' => 'Expediente',
  'record' => 'Expediente',
  'place' => 'Lugar',
  'state' => 'Situación',
  'institution' => 'Institución',
  'institutions' => 'Instituciones',
  'dependencies' => 'Dependencias',
  'date' => 'Fecha',
  'rightTo' => 'Derecho',
  'violatedFact' => 'Hecho violatorio',
  'record' => 'Número de registro',
  'day' => 'Día',
  'timeAggression' => 'Hora de agresión',
  'occupation' => 'Ocupación',
  'weaponType' => 'Tipo de Arma',
  'month' => 'Mes',
  'investigationType' => 'Tipo de investigación',
  'position' => 'Cargo policial',
  'locationPlace' => 'Lugar de destacamiento',
  'actPlace' => 'Lugar del hecho',
  'victimQuantity' => 'Cantidad de víctimas',
  'criminalJusticeProcess' => 'Estado del proceso penal',
  'officerSex' => 'Sexo del oficial',
  'victimSex' => 'Sexo de la víctima',
  'dayNumber' => 'Dia (número)',
  'gang' => 'Pandilla o delincuente común',
  'division' => 'Division a la que pertenece',
  'aggressions' => 'Agresiones ilegítimas',
  'deceased' => 'Fallecidos',
  'deceasedPolicemen' => 'Policias fallecidos',
  'population' => 'Población',
  'system' => 'Sistema',
  'recommendation' => 'Recomendación',
  'code' => 'Código',
  'mechanism' => 'Mecanismo',
  'relatedRights' => 'Derechos',
  // 'responsibleInstitutions' => 'Insti',
  'source' => 'Fuente',
  'legalBase' => 'Base legal',
  'crimeType' => 'Tipo delito',
  'result' => 'Resultado',
  'value' => 'Valor',
  'stage' => 'Fase',
  'allegations' => 'Denuncias',
  'observations' => 'Observaciones',
  'protectionCommittals' => 'Amparos ingresos',
  'protectionResolved' => 'Amparos resueltos',
  'habeasCorpusCommittals' => 'Hábeas Corpus ingresos',
  'habeasCorpusResolved' => 'Hábeas Corpus resueltos',
  'unconstitutionalCommittals' => 'Inconstitucionalidades ingresos',
  'unconstitutionalResolved' => 'Inconstitucionalidades resueltos',
  'budgetUnit' => 'Unidad presupuestaria',
  'budgetAmount' => 'Presupuesto',
  'lastModifiedDate' => 'Actualización',
  'summary' => 'Resumen',
  'link' => 'Link',
  'columnValidation' => 'La fila :row en la columna :letter (:column) no puede estar vacía.',
  'integerColumnValidation' => 'La fila :row en la columna :letter (:column) debe ser un número entero.',
  'departmentColumnValidation' => 'La fila :row en la columna :letter con valor :value no es un departamento válido.',
  'municipalityColumnValidation' => 'La fila :row en la columna :letter con valor :value no es un municipio válido.',
  'rightColumnValidation' => 'El derecho :name no se encuentra definido en el catálogo.',
  'institutionColumnValidation' => 'La institución :name no se encuentra definida en el catálogo.',
);
