<?php
/**
 * @file
 * Menus (database) language lines.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(

  'initialOadhSetup' => 'Configuracion inicial',

  'settingAddedJournal' => 'Se realizó exitosamente la configuración inicial del OADH',

  'sources' => 'Fuentes',

  'sexualDiversities' => 'Diversidad sexual',

  'gunsTypes' => 'Tipos de armas',

  'update' => 'Actualizar configuración',

);
