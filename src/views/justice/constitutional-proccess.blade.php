@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-justice-cp-new-action', null, array('id' => 'oadh-justice-cp-new-action')) !!}
{!! Form::hidden('oadh-justice-cp-edit-action', null, array('id' => 'oadh-justice-cp-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-justice-cp-remove-action', null, array('id' => 'oadh-justice-cp-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-justice-cp-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-justice-cp-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-justice-cp-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhJusticeCpOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#oadh-justice-cp-grid').getSelectedRowId('file_id'), rowData = $('#oadh-justice-cp-grid').getRowData($('#oadh-justice-cp-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-justice-cp-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-justice-cp-', 'firstPage', id);

				$('#oadh-justice-cp-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-justice-cp-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-justice-cp-btn-group-2').enableButtonGroup();

		}

		function oadhJusticeCpOnLoadCompleteEvent()
		{
			$('#oadh-justice-cp-temp-data').jqGrid('clearGridData');
			$('#oadh-justice-cp-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-justice-cp-btn-tooltip').tooltip();

			$('#oadh-justice-cp-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-justice-cp-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-justice-cp-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-justice-cp-grid-section').collapse('show');

				$('#oadh-justice-cp-journals-section').collapse('show');

				$('#oadh-justice-cp-filters').show();
			});

			$('#oadh-justice-cp-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-justice-cp-').focus();
			});

			$('#oadh-justice-cp-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-justice-cp-btn-refresh').click();
	    });

			$('#oadh-justice-cp-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-justice-cp-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-justice-cp-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-justice-cp-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-justice-cp-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-justice-cp-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-justice-cp-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-justice-cp-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-justice-cp-btn-toolbar').disabledButtonGroup();
				$('#oadh-justice-cp-btn-group-1').enableButtonGroup();
				$('#oadh-justice-cp-grid').trigger('reloadGrid');
				cleanJournals('oadh-justice-cp-');
			});

			// $('#oadh-justice-cp-detail-btn-refresh').click(function()
			// {
			// 	$('.decima-erp-tooltip').tooltip('hide');

			// 	$('#oadh-justice-cp-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-justice-cp-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			// });

			// $('#oadh-justice-cp-detail-btn-export-xls').click(function()
			// {
			// 	$('#oadh-justice-cp-back-detail-gridXlsButton').click();
			// });

			// $('#oadh-justice-cp-detail-btn-export-csv').click(function()
			// {
			// 	$('#oadh-justice-cp-back-detail-gridCsvButton').click();
			// });

			// $('#oadh-justice-cp-btn-export-xls').click(function()
			// {
			// 	if($('#oadh-justice-cp-journals-section').attr('data-target-id') == '')
			// 	{
			// 		$('#oadh-justice-cp-gridXlsButton').click();
			// 	}
			// });

			// $('#oadh-justice-cp-btn-export-csv').click(function()
			// {
			// 	if($('#oadh-justice-cp-journals-section').attr('data-target-id') == '')
			// 	{
			// 		$('#oadh-justice-cp-gridCsvButton').click();
			// 	}
			// });

			$('#oadh-justice-cp-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-justice-cp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-justice-cp-grid').isRowSelected())
					{
						$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-justice-cp-modal-delete-file').modal('show');
			});

			$('#oadh-justice-cp-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-justice-cp-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/justice/justice-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-justice-cp-btn-toolbar', false);
						$('#oadh-justice-cp-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-justice-cp-modal-delete-prod').click();
							$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-justice-cp-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-justice-cp-grid').trigger('reloadGrid');
						$('#oadh-justice-cp-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-justice-cp-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-justice-cp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-justice-cp-grid').isRowSelected())
					{
						$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-justice-cp-modal-delete-prod').modal('show');
			});

			$('#oadh-justice-cp-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-justice-cp-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/justice/justice-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-justice-cp-btn-toolbar', false);
						$('#oadh-justice-cp-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-justice-cp-modal-delete-prod').click();
							$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-justice-cp-prod-data').trigger('reloadGrid');
							$('#oadh-justice-cp-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-justice-cp-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-justice-cp-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-justice-cp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-justice-cp-grid').isRowSelected())
					{
						$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-justice-cp-modal-generate').modal('show');
			});

			$('#oadh-justice-cp-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-justice-cp-grid').getRowData($('#oadh-justice-cp-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/justice/justice-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-justice-cp-btn-toolbar', false);
						$('#oadh-justice-cp-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-justice-cp-btn-refresh').click();
							$("#oadh-justice-cp-btn-group-2").disabledButtonGroup();
							$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-justice-cp-btn-refresh').click();
							$("#oadh-justice-cp-btn-group-2").disabledButtonGroup();
							$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-justice-cp-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-justice-cp-btn-process').click(function()
			{

				if(!$('#oadh-justice-cp-grid').isRowSelected())
				{
					$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-justice-cp-grid').getRowData($('#oadh-justice-cp-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-justice-cp-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-justice-cp-id').val(rowData.file_id);
				$('#oadh-justice-cp-name').val(rowData.file_name);
				$('#oadh-justice-cp-system-route').val(rowData.file_system_route);

				$('#oadh-justice-cp-mp-modal').modal('show');
			});

			$('#oadh-justice-cp-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-justice-cp-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-justice-cp-mp-form').formToObject('oadh-justice-cp-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/justice/justice-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-justice-cp-btn-toolbar', false);
						$('#oadh-justice-cp-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-justice-cp-btn-refresh').click();
							$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-justice-cp-temp-data, #oadh-justice-cp-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-justice-cp-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-justice-cp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-justice-cp-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-justice-cp-edit-action').isEmpty())
			{
				showButtonHelper('oadh-justice-cp-btn-close', 'oadh-justice-cp-btn-group-2', $('#oadh-justice-cp-edit-action').attr('data-content'));
			}

			$('#oadh-justice-cp-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-justice-cp-btn-close', 'oadh-justice-cp-btn-group-2', $('#oadh-justice-cp-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-justice-cp-delete-action').isEmpty())
			{
				showButtonHelper('oadh-justice-cp-btn-close', 'oadh-justice-cp-btn-group-2', $('#oadh-justice-cp-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-justice-cp-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-justice-cp-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-justice-cp-btn-upload', 'class' => 'btn btn-default oadh-justice-cp-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-justice-cp-btn-refresh', 'class' => 'btn btn-default oadh-justice-cp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-justice-cp-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-justice-cp-btn-process', 'class' => 'btn btn-default oadh-justice-cp-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-justice-cp-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-justice-cp-btn-delete-prod', 'class' => 'btn btn-default oadh-justice-cp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-justice-cp-btn-delete-file', 'class' => 'btn btn-default oadh-justice-cp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-justice-cp-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-justice-cp-grid'>
				{!!
				GridRender::setGridId("oadh-justice-cp-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhJusticeCpOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhJusticeCpOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-justice-cp-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-justice-cp-temp-data-tab" aria-controls="oadh-justice-cp-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-justice-cp-processed-data-tab" aria-controls="oadh-justice-cp-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-justice-cp-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-justice-cp-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-justice-cp-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/justice/justice-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_justice_cp_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_justice_cp_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_justice_cp_year', 'align' => 'center', 'width' => 40, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.month'), 'index' => 'month', 'name' => 'oadh_justice_cp_month', 'align' => 'center', 'width' => 40, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.protectionCommittals'), 'index' => 'protection_committals', 'name' => 'oadh_justice_cp_protection_committals', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.protectionResolved'), 'index' => 'protection_resolved', 'name' => 'oadh_justice_cp_protection_resolved', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.habeasCorpusCommittals'), 'index' => 'habeas_corpus_committals', 'name' => 'oadh_justice_cp_habeas_corpus_committals', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.habeasCorpusResolved'), 'index' => 'habeas_corpus_resolved', 'name' => 'oadh_justice_cp_habeas_corpus_resolved', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.unconstitutionalCommittals'), 'index' => 'unconstitutional_committals', 'name' => 'oadh_justice_cp_unconstitutional_committals', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.unconstitutionalResolved'), 'index' => 'unconstitutional_resolved', 'name' => 'oadh_justice_cp_unconstitutional_resolved', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_justice_cp_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => 50))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-justice-cp-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-justice-cp-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-justice-cp-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/justice/justice-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_justice_cp_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_justice_cp_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_justice_cp_year', 'align' => 'center', 'width' => 40, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.month'), 'index' => 'month', 'name' => 'oadh_justice_cp_month', 'align' => 'center', 'width' => 40, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.protectionCommittals'), 'index' => 'protection_committals', 'name' => 'oadh_justice_cp_protection_committals', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.protectionResolved'), 'index' => 'protection_resolved', 'name' => 'oadh_justice_cp_protection_resolved', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.habeasCorpusCommittals'), 'index' => 'habeas_corpus_committals', 'name' => 'oadh_justice_cp_habeas_corpus_committals', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.habeasCorpusResolved'), 'index' => 'habeas_corpus_resolved', 'name' => 'oadh_justice_cp_habeas_corpus_resolved', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.unconstitutionalCommittals'), 'index' => 'unconstitutional_committals', 'name' => 'oadh_justice_cp_unconstitutional_committals', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.unconstitutionalResolved'), 'index' => 'unconstitutional_resolved', 'name' => 'oadh_justice_cp_unconstitutional_resolved', 'align' => 'center', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-justice-cp-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-justice-cp-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-justice-cp-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-justice-cp-header-rows-number', 1 , array('id' => 'oadh-justice-cp-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-justice-cp-id', null, array('id' => 'oadh-justice-cp-id')) !!}
											{!! Form::hidden('oadh-justice-cp-name', null, array('id' => 'oadh-justice-cp-name')) !!}
											{!! Form::hidden('oadh-justice-cp-system-route', null, array('id' => 'oadh-justice-cp-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-justice-cp-last-row-number', null, array('id' => 'oadh-justice-cp-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-justice-cp-mp-date-format', null, array('id' => 'oadh-justice-cp-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;margin-right:55px">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-justice-cp-year', 'A' , array('id' => 'oadh-justice-cp-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-month', Lang::get('decima-oadh::back-end-column.month'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-justice-cp-month', 'B' , array('id' => 'oadh-justice-cp-month', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;margin-right:55px">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-protection-committals', Lang::get('decima-oadh::back-end-column.protectionCommittals'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-justice-cp-protection-committals', 'C' , array('id' => 'oadh-justice-cp-protection-committals', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-protection-resolved', Lang::get('decima-oadh::back-end-column.protectionResolved'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-justice-cp-protection-resolved', 'D' , array('id' => 'oadh-justice-cp-protection-resolved', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;margin-right:55px">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-habeas-corpus-committals', Lang::get('decima-oadh::back-end-column.habeasCorpusCommittals'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-justice-cp-habeas-corpus-committals', 'E' , array('id' => 'oadh-justice-cp-habeas-corpus-committals', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-habeas-corpus-resolved', Lang::get('decima-oadh::back-end-column.habeasCorpusResolved'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-justice-cp-habeas-corpus-resolved', 'F' , array('id' => 'oadh-justice-cp-habeas-corpus-resolved', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;margin-right:55px">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-unconstitutional-committals', Lang::get('decima-oadh::back-end-column.unconstitutionalCommittals'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-justice-cp-unconstitutional-committals', 'G' , array('id' => 'oadh-justice-cp-unconstitutional-committals', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-justice-cp-unconstitutional-resolved', Lang::get('decima-oadh::back-end-column.unconstitutionalResolved'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-justice-cp-unconstitutional-resolved', 'H' , array('id' => 'oadh-justice-cp-unconstitutional-resolved', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-justice-cp-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-justice-cp-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-justice-cp-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-justice-cp-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-justice-cp-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-justice-cp-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-justice-cp-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-justice-cp-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-justice-cp-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-justice-cp-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-justice-cp-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
