@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-s-new-action', null, array('id' => 'oadh-s-new-action')) !!}
{!! Form::hidden('oadh-s-edit-action', null, array('id' => 'oadh-s-edit-action', 'data-content' =>
Lang::get('decima-oadh::subscriber-management.editHelpText'))) !!}
{!! Form::hidden('oadh-s-remove-action', null, array('id' => 'oadh-s-remove-action', 'data-content' =>
Lang::get('decima-oadh::subscriber-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-s-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-s-btn-delete-helper', 'class' => 'hidden')) !!}
<style></style>

<script type='text/javascript'>
	//For grids with multiselect enabled
	function oadhSOnSelectRowEvent(id)
	{
		var selRowIds = $('#oadh-s-grid').jqGrid('getGridParam', 'selarrrow'), id;

		if(selRowIds.length == 0)
		{
			$('#oadh-s-btn-group-2').disabledButtonGroup();
			cleanJournals('oadh-s-');
			// cleanFiles('oadh-s-')
		}
		else if(selRowIds.length == 1)
		{
			$('#oadh-s-btn-group-2').enableButtonGroup();

			id = $('#oadh-s-grid').getSelectedRowId('oadh_s_id');

			if($('#oadh-s-journals').attr('data-journalized-id') != id)
			{
				cleanJournals('oadh-s-');
				// getElementFiles('oadh-s-', id);
				getAppJournals('oadh-s-','firstPage', id);
			}

		}
		else if(selRowIds.length > 1)
		{
			$('#oadh-s-btn-group-2').disabledButtonGroup();
			$('#oadh-s-btn-delete').removeAttr('disabled');
			cleanJournals('oadh-s-');
			// cleanFiles('oadh-s-')
		}
	}

	/*
	//For grids with multiselect disabled
	function oadhSOnSelectRowEvent()
	{
		var id = $('#oadh-s-grid').getSelectedRowId('oadh_s_id');

		if($('#oadh-s-grid-section').attr('data-id') != id)
		{
			$('#oadh-s-grid-section').attr('data-id', id);
			getAppJournals('oadh-s-', 'firstPage', id);
			// getElementFiles('oadh-s-', id);
		}

		$('#oadh-s-btn-group-2').enableButtonGroup();
	}
	*/

	$(document).ready(function()
	{
		$('.oadh-s-btn-tooltip').tooltip();

		$('#oadh-s-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-s-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-s-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-s-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-s-').focus();
		});

		$('#oadh-s-').focusout(function()
		{
			$('#oadh-s-btn-save').focus();
		});

		$('#oadh-s-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-s-grid-section').collapse('show');
		});

		$('#oadh-s-btn-new').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-s-btn-toolbar').disabledButtonGroup();

			$('#oadh-s-btn-group-3').enableButtonGroup();

			$('#oadh-s-form-new-title').removeClass('hidden');

			$('#oadh-s-journals-section').attr('data-target-id', '#oadh-s-form-section');

			$('#oadh-s-grid-section').collapse('hide');

			$('.decima-erp-tooltip').tooltip('hide');
		});

		$('#oadh-s-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-s-btn-toolbar').disabledButtonGroup();

			$('#oadh-s-btn-group-1').enableButtonGroup();

			if($('#oadh-s-journals-section').attr('data-target-id') == '' || $('#oadh-s-journals-section').attr('data-target-id') == '#oadh-s-form-section')
			{
				$('#oadh-s-grid').trigger('reloadGrid');

				$('#oadh-s-grid-section').attr('data-id', '');

				cleanJournals('oadh-s-');
				// cleanFiles('oadh-s-');
			}
			else
			{

			}
		});

		$('#oadh-s-btn-export-xls').click(function()
		{
			if($('#oadh-s-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-s-gridXlsButton').click();
			}
			else
			{

			}
		});

		$('#oadh-s-btn-export-csv').click(function()
		{
			if($('#oadh-s-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-s-gridCsvButton').click();
			}
			else
			{

			}
		});

		$('#oadh-s-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-s-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-s-grid').isRowSelected())
				{
					$('#oadh-s-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				$('#oadh-s-btn-toolbar').disabledButtonGroup();

				$('#oadh-s-btn-group-3').enableButtonGroup();

				$('#oadh-s-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-s-grid').getRowData($('#oadh-s-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				$('#oadh-s-form-edit-title').removeClass('hidden');

				$('#oadh-s-journals-section').attr('data-target-id', '#oadh-s-form-section');

				$('#oadh-s-grid-section').collapse('hide');
			}
			else
			{

			}
		});

		$('#oadh-s-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-s-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-s-grid').isRowSelected())
				{
					$('#oadh-s-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-s-grid').getRowData($('#oadh-s-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-s-delete-message').html($('#oadh-s-delete-message').attr('data-default-label').replace(':name', rowData.oadh_s_name));
			}
			else
			{

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-s-modal-delete').modal('show');
		});

		$('#oadh-s-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-s-journals-section').attr('data-target-id') == '')
			{
				url = $('#oadh-s-form').attr('action') + '/delete';
				//For grids with multiselect enabled
				id = $('#oadh-s-grid').getSelectedRowsIdCell('oadh_s_id');
				//For grids with multiselect disabled
				// id = $('#oadh-s-grid').getSelectedRowId('oadh_s_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify({'_token':$('#app-token').val(), 'id':id}),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-s-btn-toolbar', false);
					$('#oadh-s-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-s-btn-refresh').click();
						$('#oadh-s-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}
					else if(json.info)
					{
						$('#oadh-s-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 12000);
					}

					$('#oadh-s-modal-delete').modal('hide');
					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-s-btn-save').click(function()
		{
			var url = $('#oadh-s-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-s-journals-section').attr('data-target-id') == '#oadh-s-form-section')
			{
				if(!$('#oadh-s-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-s-id').isEmpty())
				{
					url = url + '/create';
				}
				else
				{
					url = url + '/update';
					action = 'edit';
				}

				data = $('#oadh-s-form').formToObject('oadh-s-');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-s-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-s-journals-section').attr('data-target-id') == '#oadh-s-form-section')
						{
							$('#oadh-s-btn-close').click();
							$('#oadh-s-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
						}
						else
						{
							// $('#oadh-s-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-s-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-s-journals-section').attr('data-target-id') == '#oadh-s-form-section')
						{
							$('#oadh-s-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-s-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-s-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			// oadh-s-form-section
			if($('#oadh-s-journals-section').attr('data-target-id') == '#oadh-s-form-section')
			{
				$('#oadh-s-form-new-title').addClass('hidden');
				$('#oadh-s-form-edit-title').addClass('hidden');
				$('#oadh-s-btn-refresh').click();
				$('#oadh-s-form').jqMgVal('clearForm');
				$('#oadh-s-form-section').collapse('hide');
			}
			else
			{

			}

			$('#oadh-s-btn-group-1').enableButtonGroup();
			$('#oadh-s-btn-group-3').disabledButtonGroup();
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-s-journals-section').attr('data-target-id', '')
		});

		$('#oadh-s-btn-edit-helper').click(function()
	  {
			showButtonHelper('oadh-s-btn-close', 'oadh-s-btn-group-2', $('#oadh-s-edit-action').attr('data-content'));
	  });

		$('#oadh-s-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-s-btn-close', 'oadh-s-btn-group-2', $('#oadh-s-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-s-new-action').isEmpty())
		{
			$('#oadh-s-btn-new').click();
		}

		if(!$('#oadh-s-edit-action').isEmpty())
		{
			showButtonHelper('oadh-s-btn-close', 'oadh-s-btn-group-2', $('#oadh-s-edit-action').attr('data-content'));
		}

		if(!$('#oadh-s-delete-action').isEmpty())
		{
			showButtonHelper('oadh-s-btn-close', 'oadh-s-btn-group-2', $('#oadh-s-delete-action').attr('data-content'));
		}
	});
</script>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-s-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-s-btn-group-1" class="btn-group btn-group-app-toolbar">
				{{-- {!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new'), array('id' => 'oadh-s-btn-new', 'class' => 'btn btn-default oadh-s-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-oadh::subscriber-management.new'))) !!} --}}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' =>
				'oadh-s-btn-refresh', 'class' => 'btn btn-default oadh-s-btn-tooltip decima-erp-tooltip', 'data-container' =>
				'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span
						class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body',
					'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
						<li><a id='oadh-s-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i>
								{{ Lang::get('decima-accounting::journal-management.standardSpreadsheet') . ' (' . Lang::get('form.spreadsheet') . ')' }}</a>
						</li>
						<!-- <li><a id='oadh-s-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li> -->
					</ul>
				</div>
			</div>
			<div id="oadh-s-btn-group-2" class="btn-group btn-group-app-toolbar">
				{{-- {!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-s-btn-edit', 'class' => 'btn btn-default oadh-s-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::subscriber-management.edit'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-s-btn-delete', 'class' => 'btn btn-default oadh-s-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::subscriber-management.delete'))) !!} --}}
				{{-- Form::button('<i class="fa fa-upload"></i> ', array('id' => 'oadh-s-btn-upload', 'class' => 'btn btn-default oadh-s-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-folder' => Lang::get('decima-oadh::subscriber-management.folder'), 'data-original-title' => Lang::get('toolbar.uploadLongText'))) --}}
				{{-- Form::button('<i class="fa fa-files-o"></i> ', array('id' => 'oadh-s-btn-show-files', 'class' => 'btn btn-default oadh-s-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showFilesLongText'))) --}}
				{{-- Form::button('<i class="fa fa-history"></i> ', array('id' => 'oadh-s-btn-show-history', 'class' => 'btn btn-default oadh-s-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showHistoryLongText'))) --}}
			</div>
			<div id="oadh-s-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-s-btn-save',
				'class' => 'btn btn-default oadh-s-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' =>
				'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::subscriber-management.save'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-s-btn-close',
				'class' => 'btn btn-default oadh-s-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' =>
				'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-s-grid-section' class='app-grid collapse in' data-id='' data-app-grid-id='oadh-s-grid'>
			{!!
			GridRender::setGridId('oadh-s-grid')
			->enablefilterToolbar(false, false)
			->hideXlsExporter()
			->hideCsvExporter()
			->setGridOption('url',URL::to('ucaoadh/cms/subscribers/grid-data'))
			->setGridOption('caption', Lang::get('decima-oadh::subscriber-management.gridTitle'))
			->setGridOption('filename', Lang::get('decima-oadh::subscriber-management.gridTitle'))
			->setGridOption('postData',array('_token' => Session::token()))
			->setGridEvent('onSelectRow', 'oadhSOnSelectRowEvent')
			->addColumn(array('index' => 's.id', 'name' => 'oadh_s_id', 'hidden' => true))
			->addColumn(array('label' => Lang::get('decima-oadh::subscriber-management.email'), 'index' => 's.email' ,'name' => 'oadh_s_email'))
			->addColumn(array('label' => Lang::get('form.dateTime'), 'index' => 's.datetime', 'name' => 'oadh_s_datetime', 'width' => 100, 'align' => 'center', 'formatter' => 'date', 'formatoptions' => array('srcformat' => 'Y-m-d H:i:s', 'newformat' => Lang::get('form.phpDateTimeFormat'))))
			//->addColumn(array('label' => Lang::get('form.status'), 'index' => 'status', 'name' => 'oadh_s_status','formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.statusGridText')), 'align' => 'center','hidden' => false, 'stype' => 'select', 'width' => 80))
			// ->addColumn(array('label' => Lang::get('form.status'), 'index' => 'status', 'name' => 'oadh_s_status','formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::subscriber-management.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select'))
			// ->addColumn(array('label' => Lang::get('form.active'), 'index' => 'is_active' ,'name' => 'oadh_s_is_active','formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.booleanText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70))
			// ->addColumn(array('label' => Lang::get('decima-oadh::subscriber-management.money'), 'index' => 'money', 'name' => 'money', 'formatter' => 'currency', 'align'=>'right', 'width' => 100, 'hidden' => false, 'formatoptions' => array('prefix' => OrganizationManager::getOrganizationCurrencySymbol() . ' ')))
			->renderGrid();
			!!}
		</div>
	</div>
</div>
<div id='oadh-s-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-s-form', 'url' => URL::to('module/category/app'), 'role' => 'form', 'onsubmit'
			=> 'return false;')) !!}
			<legend id="oadh-s-form-new-title" class="hidden">
				{{ Lang::get('decima-oadh::subscriber-management.formNewTitle') }}</legend>
			<legend id="oadh-s-form-edit-title" class="hidden">
				{{ Lang::get('decima-oadh::subscriber-management.formEditTitle') }}</legend>
			<div class="row">
				<div class="col-md-6 form-division-line">
					<div class="form-group mg-hm">
						{!! Form::label('oadh-s-name', Lang::get('form.name'), array('class' => 'control-label')) !!}
						{!! Form::text('oadh-s-name', null , array('id' => 'oadh-s-name', 'class' => 'form-control',
						'data-mg-required' => '')) !!}
						{!! Form::hidden('oadh-s-id', null, array('id' => 'oadh-s-id')) !!}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group mg-hm">
						{!! Form::label('oadh-s-phone-number', Lang::get('decima-oadh::subscriber-management.phoneNumber'),
						array('class' => 'control-label')) !!}
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-phone"></i>
							</span>
							{!! Form::text('oadh-s-phone-number', null , array('id' => 'oadh-s-phone-number', 'class' =>
							'form-control')) !!}
						</div>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div id='oadh-s-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals('oadh-s-', $appInfo['id']) !!}
</div>
<div id='oadh-s-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm oadh-s-btn-delete">
		<div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-s-delete-message">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
				<!-- <p id="oadh-s-delete-message" data-default-label="{{ Lang::get('decima-oadh::subscriber-management.deleteMessageConfirmation') }}"></p> -->
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-s-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
		</div>
	</div>
</div>
@parent
@stop