@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-reparation-as-new-action', null, array('id' => 'oadh-reparation-as-new-action')) !!}
{!! Form::hidden('oadh-reparation-as-edit-action', null, array('id' => 'oadh-reparation-as-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-reparation-as-remove-action', null, array('id' => 'oadh-reparation-as-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-reparation-as-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-reparation-as-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-reparation-as-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhDerechoPrefijoOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#oadh-reparation-as-grid').getSelectedRowId('file_id'), rowData = $('#oadh-reparation-as-grid').getRowData($('#oadh-reparation-as-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-reparation-as-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-reparation-as-', 'firstPage', id);

				$('#oadh-reparation-as-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-reparation-as-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-reparation-as-btn-group-2').enableButtonGroup();

		}

		function oadhDerechoPrefijoOnLoadCompleteEvent()
		{
			$('#oadh-reparation-as-temp-data').jqGrid('clearGridData');
			$('#oadh-reparation-as-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-reparation-as-btn-tooltip').tooltip();

			$('#oadh-reparation-as-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-reparation-as-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-reparation-as-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-reparation-as-grid-section').collapse('show');

				$('#oadh-reparation-as-journals-section').collapse('show');

				$('#oadh-reparation-as-filters').show();
			});

			$('#oadh-reparation-as-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-reparation-as-').focus();
			});

			$('#oadh-reparation-as-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-reparation-as-btn-refresh').click();
	    });

			$('#oadh-reparation-as-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-reparation-as-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-reparation-as-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-reparation-as-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-reparation-as-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-reparation-as-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-reparation-as-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-reparation-as-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-reparation-as-btn-toolbar').disabledButtonGroup();
				$('#oadh-reparation-as-btn-group-1').enableButtonGroup();
				$('#oadh-reparation-as-grid').trigger('reloadGrid');
				cleanJournals('oadh-reparation-as-');
			});

			// $('#oadh-reparation-as-detail-btn-refresh').click(function()
			// {
			// 	$('.decima-erp-tooltip').tooltip('hide');

			// 	$('#oadh-reparation-as-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-reparation-as-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			// });

			// $('#oadh-reparation-as-detail-btn-export-xls').click(function()
			// {
			// 	$('#oadh-reparation-as-back-detail-gridXlsButton').click();
			// });

			// $('#oadh-reparation-as-detail-btn-export-csv').click(function()
			// {
			// 	$('#oadh-reparation-as-back-detail-gridCsvButton').click();
			// });

			// $('#oadh-reparation-as-btn-export-xls').click(function()
			// {
			// 	if($('#oadh-reparation-as-journals-section').attr('data-target-id') == '')
			// 	{
			// 		$('#oadh-reparation-as-gridXlsButton').click();
			// 	}
			// });

			// $('#oadh-reparation-as-btn-export-csv').click(function()
			// {
			// 	if($('#oadh-reparation-as-journals-section').attr('data-target-id') == '')
			// 	{
			// 		$('#oadh-reparation-as-gridCsvButton').click();
			// 	}
			// });

			$('#oadh-reparation-as-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-reparation-as-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-reparation-as-grid').isRowSelected())
					{
						$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-reparation-as-modal-delete-file').modal('show');
			});

			$('#oadh-reparation-as-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-reparation-as-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/reparation/reparation-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-reparation-as-btn-toolbar', false);
						$('#oadh-reparation-as-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-reparation-as-modal-delete-prod').click();
							$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-reparation-as-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-reparation-as-grid').trigger('reloadGrid');
						$('#oadh-reparation-as-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-reparation-as-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-reparation-as-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-reparation-as-grid').isRowSelected())
					{
						$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-reparation-as-modal-delete-prod').modal('show');
			});

			$('#oadh-reparation-as-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-reparation-as-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/reparation/reparation-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-reparation-as-btn-toolbar', false);
						$('#oadh-reparation-as-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-reparation-as-modal-delete-prod').click();
							$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-reparation-as-prod-data').trigger('reloadGrid');
							$('#oadh-reparation-as-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-reparation-as-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-reparation-as-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-reparation-as-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-reparation-as-grid').isRowSelected())
					{
						$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-reparation-as-modal-generate').modal('show');
			});

			$('#oadh-reparation-as-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-reparation-as-grid').getRowData($('#oadh-reparation-as-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/reparation/reparation-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-reparation-as-btn-toolbar', false);
						$('#oadh-reparation-as-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-reparation-as-btn-refresh').click();
							$("#oadh-reparation-as-btn-group-2").disabledButtonGroup();
							$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-reparation-as-btn-refresh').click();
							$("#oadh-reparation-as-btn-group-2").disabledButtonGroup();
							$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-reparation-as-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-reparation-as-btn-process').click(function()
			{

				if(!$('#oadh-reparation-as-grid').isRowSelected())
				{
					$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-reparation-as-grid').getRowData($('#oadh-reparation-as-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-reparation-as-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-reparation-as-id').val(rowData.file_id);
				$('#oadh-reparation-as-name').val(rowData.file_name);
				$('#oadh-reparation-as-system-route').val(rowData.file_system_route);

				$('#oadh-reparation-as-mp-modal').modal('show');
			});

			$('#oadh-reparation-as-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-reparation-as-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-reparation-as-mp-form').formToObject('oadh-reparation-as-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/reparation/reparation-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-reparation-as-btn-toolbar', false);
						$('#oadh-reparation-as-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-reparation-as-btn-refresh').click();
							$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-reparation-as-temp-data, #oadh-reparation-as-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-reparation-as-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-reparation-as-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-reparation-as-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-reparation-as-edit-action').isEmpty())
			{
				showButtonHelper('oadh-reparation-as-btn-close', 'oadh-reparation-as-btn-group-2', $('#oadh-reparation-as-edit-action').attr('data-content'));
			}

			$('#oadh-reparation-as-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-reparation-as-btn-close', 'oadh-reparation-as-btn-group-2', $('#oadh-reparation-as-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-reparation-as-delete-action').isEmpty())
			{
				showButtonHelper('oadh-reparation-as-btn-close', 'oadh-reparation-as-btn-group-2', $('#oadh-reparation-as-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-reparation-as-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-reparation-as-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-reparation-as-btn-upload', 'class' => 'btn btn-default oadh-reparation-as-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-reparation-as-btn-refresh', 'class' => 'btn btn-default oadh-reparation-as-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-reparation-as-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-reparation-as-btn-process', 'class' => 'btn btn-default oadh-reparation-as-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-reparation-as-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-reparation-as-btn-delete-prod', 'class' => 'btn btn-default oadh-reparation-as-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-reparation-as-btn-delete-file', 'class' => 'btn btn-default oadh-reparation-as-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-reparation-as-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-reparation-as-grid'>
				{!!
				GridRender::setGridId("oadh-reparation-as-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhDerechoPrefijoOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhDerechoPrefijoOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-reparation-as-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-reparation-as-temp-data-tab" aria-controls="oadh-reparation-as-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-reparation-as-processed-data-tab" aria-controls="oadh-reparation-as-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-reparation-as-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-reparation-as-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-reparation-as-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/reparation/reparation-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_reparation_as_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_reparation_as_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_reparation_as_year', 'align' => 'center', 'width' => 40, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.institution'), 'index' => 'institution', 'name' => 'oadh_reparation_as_institution', 'align' => 'center', 'width' => 80, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'department', 'name' => 'oadh_reparation_as_department', 'align' => 'center', 'width' => 80, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.allegations'), 'index' => 'allegations', 'name' => 'oadh_reparation_as_allegations', 'align' => 'center', 'width' => 80, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_reparation_as_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => 50))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-reparation-as-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-reparation-as-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-reparation-as-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/reparation/reparation-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_reparation_as_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_reparation_as_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_reparation_as_year', 'align' => 'center', 'width' => 40, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.institution'), 'index' => 'institution', 'name' => 'oadh_reparation_as_institution', 'align' => 'center', 'width' => 80, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'department', 'name' => 'oadh_reparation_as_department', 'align' => 'center', 'width' => 80, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.allegations'), 'index' => 'allegations', 'name' => 'oadh_reparation_as_allegations', 'align' => 'center', 'width' => 80, 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-reparation-as-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-reparation-as-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-reparation-as-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-as-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-reparation-as-header-rows-number', 1 , array('id' => 'oadh-reparation-as-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-reparation-as-id', null, array('id' => 'oadh-reparation-as-id')) !!}
											{!! Form::hidden('oadh-reparation-as-name', null, array('id' => 'oadh-reparation-as-name')) !!}
											{!! Form::hidden('oadh-reparation-as-system-route', null, array('id' => 'oadh-reparation-as-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-as-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-reparation-as-last-row-number', null, array('id' => 'oadh-reparation-as-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-as-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-reparation-as-mp-date-format', null, array('id' => 'oadh-reparation-as-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-as-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-reparation-as-year', 'A' , array('id' => 'oadh-reparation-as-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-as-institution', Lang::get('decima-oadh::back-end-column.institution'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-reparation-as-institution', 'B' , array('id' => 'oadh-reparation-as-institution', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-as-department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-reparation-as-department', 'C' , array('id' => 'oadh-reparation-as-department', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-as-allegations', Lang::get('decima-oadh::back-end-column.allegations'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-reparation-as-allegations', 'D' , array('id' => 'oadh-reparation-as-allegations', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-reparation-as-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-reparation-as-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-reparation-as-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-reparation-as-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-reparation-as-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-reparation-as-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-reparation-as-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-reparation-as-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-reparation-as-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-reparation-as-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-reparation-as-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
