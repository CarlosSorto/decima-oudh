@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-reparation-aln-new-action', null, array('id' => 'oadh-reparation-aln-new-action')) !!}
{!! Form::hidden('oadh-reparation-aln-edit-action', null, array('id' => 'oadh-reparation-aln-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-reparation-aln-remove-action', null, array('id' => 'oadh-reparation-aln-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-reparation-aln-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-reparation-aln-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-reparation-aln-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhReparationAlnOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#oadh-reparation-aln-grid').getSelectedRowId('file_id'), rowData = $('#oadh-reparation-aln-grid').getRowData($('#oadh-reparation-aln-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-reparation-aln-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-reparation-aln-', 'firstPage', id);

				$('#oadh-reparation-aln-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-reparation-aln-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-reparation-aln-btn-group-2').enableButtonGroup();

		}

		function oadhReparationAlnOnLoadCompleteEvent()
		{
			$('#oadh-reparation-aln-temp-data').jqGrid('clearGridData');
			$('#oadh-reparation-aln-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-reparation-aln-btn-tooltip').tooltip();

			$('#oadh-reparation-aln-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-reparation-aln-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-reparation-aln-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-reparation-aln-grid-section').collapse('show');

				$('#oadh-reparation-aln-journals-section').collapse('show');

				$('#oadh-reparation-aln-filters').show();
			});

			$('#oadh-reparation-aln-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-reparation-aln-').focus();
			});

			$('#oadh-reparation-aln-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-reparation-aln-btn-refresh').click();
	    });

			$('#oadh-reparation-aln-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-reparation-aln-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-reparation-aln-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-reparation-aln-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-reparation-aln-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-reparation-aln-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-reparation-aln-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-reparation-aln-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-reparation-aln-btn-toolbar').disabledButtonGroup();
				$('#oadh-reparation-aln-btn-group-1').enableButtonGroup();
				$('#oadh-reparation-aln-grid').trigger('reloadGrid');
				cleanJournals('oadh-reparation-aln-');
			});

			// $('#oadh-reparation-aln-detail-btn-refresh').click(function()
			// {
			// 	$('.decima-erp-tooltip').tooltip('hide');

			// 	$('#oadh-reparation-aln-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-reparation-aln-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			// });

			// $('#oadh-reparation-aln-detail-btn-export-xls').click(function()
			// {
			// 	$('#oadh-reparation-aln-back-detail-gridXlsButton').click();
			// });

			// $('#oadh-reparation-aln-detail-btn-export-csv').click(function()
			// {
			// 	$('#oadh-reparation-aln-back-detail-gridCsvButton').click();
			// });

			// $('#oadh-reparation-aln-btn-export-xls').click(function()
			// {
			// 	if($('#oadh-reparation-aln-journals-section').attr('data-target-id') == '')
			// 	{
			// 		$('#oadh-reparation-aln-gridXlsButton').click();
			// 	}
			// });

			// $('#oadh-reparation-aln-btn-export-csv').click(function()
			// {
			// 	if($('#oadh-reparation-aln-journals-section').attr('data-target-id') == '')
			// 	{
			// 		$('#oadh-reparation-aln-gridCsvButton').click();
			// 	}
			// });

			$('#oadh-reparation-aln-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-reparation-aln-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-reparation-aln-grid').isRowSelected())
					{
						$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-reparation-aln-modal-delete-file').modal('show');
			});

			$('#oadh-reparation-aln-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-reparation-aln-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/reparation/reparation-uploader/amnesty-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-reparation-aln-btn-toolbar', false);
						$('#oadh-reparation-aln-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-reparation-aln-modal-delete-prod').click();
							$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-reparation-aln-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-reparation-aln-grid').trigger('reloadGrid');
						$('#oadh-reparation-aln-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-reparation-aln-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-reparation-aln-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-reparation-aln-grid').isRowSelected())
					{
						$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-reparation-aln-modal-delete-prod').modal('show');
			});

			$('#oadh-reparation-aln-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-reparation-aln-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/reparation/reparation-uploader/amnesty-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-reparation-aln-btn-toolbar', false);
						$('#oadh-reparation-aln-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-reparation-aln-modal-delete-prod').click();
							$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-reparation-aln-prod-data').trigger('reloadGrid');
							$('#oadh-reparation-aln-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-reparation-aln-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-reparation-aln-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-reparation-aln-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-reparation-aln-grid').isRowSelected())
					{
						$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-reparation-aln-modal-generate').modal('show');
			});

			$('#oadh-reparation-aln-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-reparation-aln-grid').getRowData($('#oadh-reparation-aln-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/reparation/reparation-uploader/amnesty-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-reparation-aln-btn-toolbar', false);
						$('#oadh-reparation-aln-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-reparation-aln-btn-refresh').click();
							$("#oadh-reparation-aln-btn-group-2").disabledButtonGroup();
							$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-reparation-aln-btn-refresh').click();
							$("#oadh-reparation-aln-btn-group-2").disabledButtonGroup();
							$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-reparation-aln-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-reparation-aln-btn-process').click(function()
			{

				if(!$('#oadh-reparation-aln-grid').isRowSelected())
				{
					$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-reparation-aln-grid').getRowData($('#oadh-reparation-aln-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-reparation-aln-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-reparation-aln-id').val(rowData.file_id);
				$('#oadh-reparation-aln-name').val(rowData.file_name);
				$('#oadh-reparation-aln-system-route').val(rowData.file_system_route);

				$('#oadh-reparation-aln-mp-modal').modal('show');
			});

			$('#oadh-reparation-aln-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-reparation-aln-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-reparation-aln-mp-form').formToObject('oadh-reparation-aln-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/reparation/reparation-uploader/amnesty-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-reparation-aln-btn-toolbar', false);
						$('#oadh-reparation-aln-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-reparation-aln-btn-refresh').click();
							$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-reparation-aln-temp-data, #oadh-reparation-aln-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-reparation-aln-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-reparation-aln-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-reparation-aln-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-reparation-aln-edit-action').isEmpty())
			{
				showButtonHelper('oadh-reparation-aln-btn-close', 'oadh-reparation-aln-btn-group-2', $('#oadh-reparation-aln-edit-action').attr('data-content'));
			}

			$('#oadh-reparation-aln-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-reparation-aln-btn-close', 'oadh-reparation-aln-btn-group-2', $('#oadh-reparation-aln-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-reparation-aln-delete-action').isEmpty())
			{
				showButtonHelper('oadh-reparation-aln-btn-close', 'oadh-reparation-aln-btn-group-2', $('#oadh-reparation-aln-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-reparation-aln-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-reparation-aln-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-reparation-aln-btn-upload', 'class' => 'btn btn-default oadh-reparation-aln-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-reparation-aln-btn-refresh', 'class' => 'btn btn-default oadh-reparation-aln-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-reparation-aln-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-reparation-aln-btn-process', 'class' => 'btn btn-default oadh-reparation-aln-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-reparation-aln-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-reparation-aln-btn-delete-prod', 'class' => 'btn btn-default oadh-reparation-aln-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-reparation-aln-btn-delete-file', 'class' => 'btn btn-default oadh-reparation-aln-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-reparation-aln-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-reparation-aln-grid'>
				{!!
				GridRender::setGridId("oadh-reparation-aln-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhReparationAlnOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhReparationAlnOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-reparation-aln-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-reparation-aln-temp-data-tab" aria-controls="oadh-reparation-aln-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-reparation-aln-processed-data-tab" aria-controls="oadh-reparation-aln-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-reparation-aln-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-reparation-aln-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-reparation-aln-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/reparation/reparation-uploader/amnesty-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_reparation_aln_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_reparation_aln_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'year', 'name' => 'oadh_reparation_aln_date', 'align' => 'center', 'width' => 50, 'formatter' => 'date', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.summary'), 'index' => 'court', 'name' => 'oadh_reparation_aln_summary', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.link'), 'index' => 'court', 'name' => 'oadh_reparation_aln_link', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_reparation_aln_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => 50))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-reparation-aln-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-reparation-aln-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-reparation-aln-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/reparation/reparation-uploader/amnesty-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_reparation_aln_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_reparation_aln_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'year', 'name' => 'oadh_reparation_aln_date', 'align' => 'center', 'width' => 50, 'formatter' => 'date', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.summary'), 'index' => 'court', 'name' => 'oadh_reparation_aln_summary', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.link'), 'index' => 'court', 'name' => 'oadh_reparation_aln_link', 'align' => 'center', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-reparation-aln-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-reparation-aln-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-reparation-aln-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-aln-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-reparation-aln-header-rows-number', 1 , array('id' => 'oadh-reparation-aln-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-reparation-aln-id', null, array('id' => 'oadh-reparation-aln-id')) !!}
											{!! Form::hidden('oadh-reparation-aln-name', null, array('id' => 'oadh-reparation-aln-name')) !!}
											{!! Form::hidden('oadh-reparation-aln-system-route', null, array('id' => 'oadh-reparation-aln-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-aln-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-reparation-aln-last-row-number', null, array('id' => 'oadh-reparation-aln-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-aln-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-reparation-aln-date-format', 'd/m/Y', array('id' => 'oadh-reparation-aln-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-aln-date', Lang::get('decima-oadh::back-end-column.date'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-reparation-aln-date', 'A' , array('id' => 'oadh-reparation-aln-date', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-aln-summary', Lang::get('decima-oadh::back-end-column.summary'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-reparation-aln-summary', 'B' , array('id' => 'oadh-reparation-aln-summary', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-reparation-aln-link', Lang::get('decima-oadh::back-end-column.link'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-reparation-aln-link', 'C' , array('id' => 'oadh-reparation-aln-link', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-reparation-aln-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-reparation-aln-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-reparation-aln-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-reparation-aln-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-reparation-aln-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-reparation-aln-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-reparation-aln-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-reparation-aln-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-reparation-aln-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-reparation-aln-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-reparation-aln-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
