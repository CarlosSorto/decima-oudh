@extends('decima-oadh::front-end/base')
@section('container')
<style>
	.oadh-bottom-border {
		border-bottom: 1px solid;
		border-color: rgb(255, 152, 21) !important;
	}

	form .oadh-outline-none:focus {
		outline: none;
		box-shadow: none;
	}

	input[type="text"].oadh-outline-none:focus {
		border-color: rgb(0, 22, 70);
	}

	form .btn:hover {
		background-color: rgba(0, 22, 70, 0.9) !important;
	}

	.oadh-dropdown-toggle {
		white-space: nowrap;
	}

	.oadh-dropdown-toggle:active {
		color: rgb(255, 152, 21) !important;
	}

	.oadh-dropdown-toggle::after {
		display: inline-block;
		margin-left: .255em;
		vertical-align: .255em;
		content: "";
		border-top: .3em solid;
		border-right: .3em solid transparent;
		border-bottom: 0;
		border-left: .3em solid transparent;
	}

	.oadh-input-group-text {
		background-color: rgb(0, 22, 70) !important;
		border: 0 !important;
		color: white !important;
	}

	.oadh-input-group-text-border-right {
		border-right: 0.125rem solid white !important;
	}

	.oadh-border-card {
		border-color: rgb(0, 22, 70);
	}

	a.oadh-text-primary-color:hover {
		color: rgba(0, 22, 70, 0.7) !important;
	}

	.pagination>.page-item>a.page-link {
		color: rgba(0, 22, 70, 0.7) !important;
	}

	.badge-primary {
		background-color: rgb(0, 22, 70);
	}

	.table-striped>tbody>tr:nth-of-type(odd) {
		background-color: rgba(255, 152, 21, 0.5);
	}

	.table-striped>tbody>tr:nth-of-type(even) {
		background-color: rgba(255, 152, 21, 0.8);
	}

	.thead-primary {
		background-color: rgb(0, 22, 70);
		color: white;
	}

	.bootstrap-select>button {
		color: #495057 !important;
		background-color: #fff !important;
		border: 1px solid #ced4da !important;
		border-radius: .25rem !important;
	}

	.bootstrap-select>button:hover {
		color: #495057 !important;
		background-color: #fff !important;
	}

	.actions-btn:hover {
		color: white !important;
	}

	.pagination>.page-item.active>a.page-link {
		color: white !important;
		background-color: rgb(0, 22, 70) !important;
	}
</style>
@section('javascript')
	<script type="text/javascript">
		$(document).ready(function()
		{
      $('body').on('click', '.oadh-rdt-single-download', function()
      {
        let recId = $(this).attr('data-id')
        if (empty(recId)) 
        {
          return;
        }

        $('#oadh-rdt-export-data-pdf').val((!empty(recId)) ? JSON.stringify(recId) : JSON.stringify(''));

        $('#oadh-rdt-export-form-pdf').submit();
      })
		})
	</script>
@endsection

@php
$insts = $rec['institutions'];
$rights = $rec['rights'];
@endphp

<div class="container my-4">
	<div class="row">
		<div class="col-md-12 my-4">
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<thead class="thead-primary">
						<th scope="col" colspan="6" class="text-center">{{$rec['system']}}</th>
					</thead>
					<tbody>
						<tr>
							<th scope="row">{{ Config::get('system-lang.' . $lang . '.recomendacion') }}</th>
							<td colspan="5" class="text-justify">{{$rec['recommendation']}}</td>
						</tr>
						<tr>
							<th scope="row">{!! Config::get('system-lang.' . $lang . '.codigoreferencia') !!}</th>
							<td>{{$rec['code']}}</td>
							<th>{{ Config::get('system-lang.' . $lang . '.fecharecomendacion') }}</th>
							<td>
								{{\Carbon\Carbon::createFromFormat('Y-m-d', $rec['date'])->format(Lang::get('form.phpShortDateFormat'))}}
							</td>
							<th>{{ Config::get('system-lang.' . $lang . '.mecanismo') }}</th>
							<td>{{$rec['mechanism']}}</td>
						</tr>
						<tr>
							<th scope="row">{{ Config::get('system-lang.' . $lang . '.derechosrelacionados') }}</th>
							<td colspan="2">
								<ul class="list-group" style="list-style-type: none;">
									@foreach ($rights as $right)
									<li class="list-item">
										<span class="badge badge-pill badge-primary">{{$right['name']}}</span>
									</li>
									@endforeach
								</ul>
								
							</td>
							<th scope="row">{{ Config::get('system-lang.' . $lang . '.institucionesresponsables') }}</th>
							<td colspan="2">
								<ul class="list-group" style="list-style-type: none;">
									@foreach ($insts as $inst)
										@if (isset($inst['name']))
										<li class="list-item">
											<span class="badge badge-pill badge-primary">{{$inst['name']}} ({{$inst['abbreviation']}})</span>
										</li>
										@endif
									@endforeach
								</ul>
							</td>
						</tr>
						<tr>
							<th scope="row">{{ Config::get('system-lang.' . $lang . '.fuenterecomendacion') }}</th>
							<td colspan="5" class="text-justify">{{$rec['source']}}</td>
						</tr>
						<tr>
							<th>{{ Config::get('system-lang.' . $lang . '.baselegal') }}.</th>
							<td colspan="5" class="text-justify">{{$rec['legal_base']}}</td>
						</tr>
						<tr>
							<th scope="row">{{ Config::get('system-lang.' . $lang . '.observaciones') }}</th>
							<td colspan="5" class="text-justify">{{$rec['observations']}}</td>
						</tr>
						<tr>
							<th scope="row">{{ Config::get('system-lang.' . $lang . '.actualizacion') }}</th>
							<td colspan="5" class="text-justify">
								{{ !empty($rec['last_modified_date']) ? \Carbon\Carbon::createFromFormat('Y-m-d', $rec['last_modified_date'])->format(Lang::get('form.phpShortDateFormat')) : ''}}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-12">
			<button class="btn oadh-bg-secondary-color oadh-text-tertiary-color float-right ml-3 oadh-rdt-single-download"
				data-id="{{$rec['id']}}">{{ Config::get('system-lang.' . $lang . '.descargar') }}</button>
			<a href="{{url('cms/recomendaciones')}}"
				class="btn oadh-bg-primary-color oadh-text-tertiary-color float-right">{{ Config::get('system-lang.' . $lang . '.regresar') }}</a>
		</div>
	</div>
</div>
<form action="{{url('/cms/recomendaciones/download-single')}}" method="POST" id="oadh-rdt-export-form-pdf" class="d-none">
	<input type="hidden" name="oadh-rdt-export-data-pdf" id="oadh-rdt-export-data-pdf">
	<input type="hidden" name="lang" id="oadh-rdt-export-data-lang" value="{{ $lang }}">
</form>
@parent
@stop