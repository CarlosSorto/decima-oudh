@extends('decima-oadh::front-end/base')
@section('container')

<div class="container my-4">
  <div class="row">
    <div class="col-lg-3 col-md-5">
      <div class="oadh-lg-py-3 mb-4">
        {{-- <button id="bulletins" data-tab-id="bulletins-tab" class="btn btn-entries btn-bulletins w-100 text-right"><span class="d-block d-lg-block d-md-none d-sm-none oadh-font pr-2">{{ Config::get('system-lang.' . $lang . '.boletines') }}</span></button> --}}
        <button id="bulletins" data-tab-id="bulletins-tab" class="btn btn-entries btn-bulletins w-100 text-right"><span class="d-block oadh-font pr-2">{{ Config::get('system-lang.' . $lang . '.boletines') }}</span></button>
      </div>
      <div class="oadh-lg-py-3 mb-4">
        <button id="investigations" data-tab-id="investigations-tab" class="btn btn-entries btn-memories w-100 text-right"><span class="d-block oadh-font pr-2">{{ Config::get('system-lang.' . $lang . '.memorias') }}</span></button>
      </div>
      <div class="oadh-lg-py-3 mb-4">
        <button id="reports" data-tab-id="reports-tab" class="btn btn-entries btn-reports w-100 text-right"><span class="d-block oadh-font pr-2">{{ Config::get('system-lang.' . $lang . '.informes') }}</span></button>
      </div>
      <div class="oadh-lg-py-3 mb-4">
        <button id="memories" data-tab-id="memories-tab" class="btn btn-entries btn-investigations w-100 text-right"><span class="d-block oadh-font pr-2">{{ Config::get('system-lang.' . $lang . '.investigaciones') }}</span></button>
      </div>
      <div class="oadh-lg-py-3 mb-4">
        <button id="summaries" data-tab-id="summaries-tab" class="btn btn-entries btn-summaries w-100 text-right"><span class="d-block oadh-font pr-2">{{ Config::get('system-lang.' . $lang . '.resumenes') }}</span></button>
      </div>
    </div>
    <div class="col-lg-9 col-md-7">
      <div class="article bulletins-tab">
        @php
          $entryType = 'bulletins';
        @endphp
        @include('decima-oadh::front-end/entries-highlight')
      </div>
      <div class="article investigations-tab">    
        @php
          $entryType = 'investigations';
        @endphp
        @include('decima-oadh::front-end/entries-highlight')
      </div>
      <div class="article reports-tab">    
        @php
          $entryType = 'reports';
        @endphp
        @include('decima-oadh::front-end/entries-highlight')
      </div>
      <div class="article memories-tab">    
        @php
          $entryType = 'memories';
        @endphp
        @include('decima-oadh::front-end/entries-highlight')
      </div>
      <div class="article summaries-tab">    
        @php
          $entryType = 'summaries';
        @endphp
        @include('decima-oadh::front-end/entries-highlight')
      </div>    
    </div>
  </div>
  <div id="bulletins-tab" class="article">
    @php
      $entryType = 'bulletins';
    @endphp
    @include('decima-oadh::front-end/entries-card')
  </div>
  <div id="investigations-tab" class="article">
    @php
      $entryType = 'investigations';
    @endphp
    @include('decima-oadh::front-end/entries-card')    
  </div>
  <div id="reports-tab" class="article">
    @php
      $entryType = 'reports';
    @endphp
    @include('decima-oadh::front-end/entries-card')
  </div>
  <div id="memories-tab" class="article">
    @php
      $entryType = 'memories';
    @endphp
    @include('decima-oadh::front-end/entries-card')
  </div>
  <div id="summaries-tab" class="article">
    @php
      $entryType = 'summaries';
    @endphp
    @include('decima-oadh::front-end/entries-card')
  </div>
</div>
@parent
@stop
