<script>
  $.fn.selectpicker.Constructor.BootstrapVersion = '4';

  var genderLabelsMultiSelect =
  {
    h: "{!! Config::get('system-lang.' . $lang . '.hombre') !!}",
    m: "{!! Config::get('system-lang.' . $lang . '.mujer') !!}",
    n: "{!! Config::get('system-lang.' . $lang . '.noespecificado') !!}",
    c: "{!! Config::get('system-lang.' . $lang . '.colectiva') !!}"
  }
  var mapData = {};
  var filterId = '';
  var API = '';
  var tempChart, tempChartTimeline, oadhTimeLineData, oadhTimeLineYearSize;
  var pressId, pressHeaders01, pressHeaders02, pressHeaders03, pressTitle, pressRows01, pressRows02, pressRows03, pressNews, pressSources, pressVictims01, pressVictims02, pressVictims03, pressPerpetrators01, pressPerpetrators02, pressPerpetrators03, pressPopulation, pressDepartments, pressWeapons;
  var pressTotal01, pressTotal02, pressTotal03, pressNewsTotal, pressSourcesTotal, pressVictims01Total, pressVictims02Total, pressVictims03Total, pressPerpetrators01Total, pressPerpetrators02Total, pressPerpetrators03Total, pressPopulationTotal, pressDepartmentsTotal, pressWeaponsTotal;
  var oadhPressDataType, oadhPressDataId, oadhPressDataKey;

  const swalWithBootstrapButtons = Swal.mixin(
  {
    customClass:
    {
      confirmButton: 'btn oadh-bg-secondary-color oadh-text-tertiary-color',
      cancelButton: 'btn oadh-bg-primary-color oadh-text-tertiary-color',
      // confirmButton: 'btn oadh-bg-secondary-color oadh-text-tertiary-color w-50 ',
      // cancelButton: 'btn oadh-bg-primary-color oadh-text-tertiary-color w-50',
      iconColor: '#FF9815'
    },
    buttonsStyling: false,
  });

  am4core.options.commercialLicense = true

  function getGenderLabels (data)
  {
    var str = '';

    for (var i = 0; i < data.length; i ++)
    {
      str += data[i].label[0]
    }

    return str
  }

  function oadhPressSearch(showModal)
  {
    $.ajax(
    {
      type: 'POST',
      dataType : 'json',
      url: $('#app-url').val() + '/cms/medios-de-prensa/press-statistics-rows',
      data: JSON.stringify({
        '_token': $('#app-token').val(), 
        // 'type': $(this).attr('data-type'),
        'type': oadhPressDataType,
        // 'id': $(this).attr('data-id'),
        'id': oadhPressDataId,
        'dateFrom': $('#oadh-press-news-daterange-from').val(),
        'dateTo': $('#oadh-press-news-daterange-to').val(),
        'lang': '{{ $lang }}',
      }),
      error: function (jqXHR, statusText, errorThrown)
      {
        // console.log("Error: " + errorThrown);
        // console.log("Status Text:" + statusText);
        // console.log(jqXHR);
      },
      beforeSend: function()
      {
        $('#app-loader').css({ display: "inherit"});
      },
      success: function(json)
      {
        pressNews = json.pressNews;
        pressSources = json.pressSources;
        pressVictims01 = json.pressVictims01;
        // pressVictims02 = json.pressVictims02;
        pressVictims03 = json.pressVictims03;
        pressPerpetrators01 = json.pressPerpetrators01;
        // pressPerpetrators02 = json.pressPerpetrators02;
        pressPerpetrators03 = json.pressPerpetrators03;
        pressPopulation = json.pressPopulation;
        pressDepartments = json.pressDepartments;
        pressWeapons = json.pressWeapons;
        pressNewsTotal = pressSourcesTotal = pressVictims01Total = pressVictims02Total = pressVictims03Total = pressPerpetrators01Total = 0;
        pressPerpetrators02Total = pressPerpetrators03Total = pressPopulationTotal = pressDepartmentsTotal = pressWeaponsTotal = 0;
        
        $.each(pressNews, function( key, row)
        {
          pressNewsTotal += row['grid_col_2'];
        });
        $.each(pressSources, function( key, row)
        {
          pressSourcesTotal += row['grid_col_2'];
        });
        $.each(pressVictims01, function( key, row)
        {
          pressVictims01Total += row['grid_col_2'];
        });
        $.each(pressVictims03, function( key, row)
        {
          pressVictims03Total += row['grid_col_2'];
        });
        $.each(pressPerpetrators01, function( key, row)
        {
          pressPerpetrators01Total += row['grid_col_2'];
        });
        $.each(pressPerpetrators03, function( key, row)
        {
          pressPerpetrators03Total += row['grid_col_2'];
        });
        $.each(pressPopulation, function( key, row)
        {
          pressPopulationTotal += row['grid_col_2'];
        });
        $.each(pressDepartments, function( key, row)
        {
          pressDepartmentsTotal += row['grid_col_2'];
        });
        $.each(pressWeapons, function( key, row)
        {
          pressWeaponsTotal += row['grid_col_2'];
        });

        $('#oadh-press-data').val(JSON.stringify({
          'news': pressNews,
          'sources': pressSources,
          'victims01': pressVictims01,
          'victims03': pressVictims03,
          'perpetrators01': pressPerpetrators01,
          'perpetrators03': pressPerpetrators03,
          'population': pressPopulation,
          'departments': pressDepartments,
          'weapons': pressWeapons
        }));

        $('#app-loader').css({ display: "none"});

        $('#oadh-hr-name').effect('highlight');
        $('#oadh-hr-description').effect('highlight');
        $('.oadh-press-details-link').effect('highlight');

        if(showModal)
        {
          oadhPressShowModal();
        }
      }
    });
  }

  function oadhPressShowModal()
  {
    // pressNews = pressSources = pressVictims01 = pressVictims02 = pressVictims03 = pressPerpetrators01 = pressPerpetrators02 = pressPerpetrators03 = pressPopulation = pressDepartments = pressWeapons = [
    //   { 'grid_col_1': 'LPG', 'grid_col_2': '500'}
    // ];

    $('.oadh-press-victim-note').hide();

    switch (oadhPressDataKey) {
      case 'N'://noticias
        pressId = '#press02-modal-body-01';
        pressTitle = '{{ Config::get('system-lang.' . $lang . '.noticiascantidad') }}';
        pressHeaders01 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.periodico') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        pressRows01 = pressNews;
        pressTotal01 = pressNewsTotal;
        break;
      case 'S'://fuentes
        pressId = '#press02-modal-body-01';
        pressTitle = '{{ Config::get('system-lang.' . $lang . '.fuentes') }}';
        pressHeaders01 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.fuente') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        pressRows01 = pressSources;
        pressTotal01 = pressSourcesTotal;
        break;
      case 'V':
        pressId = '#press02-modal-body-02';
        pressTitle = '{{ Config::get('system-lang.' . $lang . '.victimas') }}';
        pressHeaders01 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.edad') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        // pressHeaders02 = {
        //   'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.sex') }}', 'width' : '70%'},
        //   'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        // };
        pressHeaders03 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.generovictima') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        pressRows01 = pressVictims01;
        // pressRows02 = pressVictims02;
        pressRows03 = pressVictims03;
        pressTotal01 = pressVictims01Total;
        pressTotal03 = pressVictims03Total;
        
        $('.oadh-press-victim-note').show();
        break;
      case 'C':
        pressId = '#press02-modal-body-02';
        pressTitle = '{{ Config::get('system-lang.' . $lang . '.victimarios') }}';
        pressHeaders01 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.edad') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        // pressHeaders02 = {
        //   'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.sex') }}', 'width' : '70%'},
        //   'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        // };
        pressHeaders03 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.generovictimario') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        pressRows01 = pressPerpetrators01;
        // pressRows02 = pressPerpetrators02;
        pressRows03 = pressPerpetrators03;
        pressTotal01 = pressPerpetrators01Total;
        pressTotal03 = pressPerpetrators03Total;
        $('.oadh-press-victim-note').show();
        break;
      case 'P':
        pressId = '#press02-modal-body-01';
        pressTitle = '{{ Config::get('system-lang.' . $lang . '.poblacion') }}';
        pressHeaders01 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.poblacion') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        pressRows01 = pressPopulation;
        pressTotal01 = pressPopulationTotal;
        break;
      case 'D':
        pressId = '#press02-modal-body-01';
        pressTitle = '{{ Config::get('system-lang.' . $lang . '.departamentos') }}';
        pressHeaders01 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.departamento') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        pressRows01 = pressDepartments;
        pressTotal01 = pressDepartmentsTotal;
        break;
      case 'W':
        pressId = '#press02-modal-body-01';
        pressTitle = '{{ Config::get('system-lang.' . $lang . '.tipoarmas') }}';
        pressHeaders01 = {
          'grid_col_1' : {'label' : '{{ Config::get('system-lang.' . $lang . '.tipoarma') }}', 'width' : '70%'},
          'grid_col_2' : {'label' : '{{ Config::get('system-lang.' . $lang . '.cantidad') }}', 'width' : '30%', 'align' : 'center'},
        };
        pressRows01 = pressWeapons;
        pressTotal01 = pressWeaponsTotal;
        break;
      default:
        break;
    }

    switch (pressId)
    {
      case '#press02-modal-body-01':
        $(pressId).createTable('', '', '', pressRows01, pressHeaders01, 'table table-striped table-sm');            

        setTimeout(function () {
          $('#press02-modal-body-01').find('tbody').append('<tr><td style="text-align:right;font-weight: bold;">Total:</td><td style="text-align: center;font-weight: bold;">' + pressTotal01 +'</td></tr>');
        }, 200);
        break;
      case '#press02-modal-body-02':
        $(pressId + '-01').createTable('', '', '', pressRows01, pressHeaders01, 'table table-striped table-sm');            
        // $(pressId + '-02').createTable('', '', '', pressRows02, pressHeaders02, 'table table-striped table-sm');            
        $(pressId + '-03').createTable('', '', '', pressRows03, pressHeaders03, 'table table-striped table-sm');            

        setTimeout(function () {
          $('#press02-modal-body-02-01').find('tbody').append('<tr><td style="text-align:right;font-weight: bold;">Total:</td><td style="text-align: center;font-weight: bold;">' + pressTotal01 +'</td></tr>');
          $('#press02-modal-body-02-03').find('tbody').append('<tr><td style="text-align:right;font-weight: bold;">Total:</td><td style="text-align: center;font-weight: bold;">' + pressTotal03 +'</td></tr>');
        }, 200);
        break;
      default:
        break;
    }

    $('#press02-modal-title').html(pressTitle);
    
    $('#oadh-press-label').val(pressTitle);

    $(pressId).show();

    $('#press02-modal').modal('show');
  }

  function oudhSubscribe(email)
  {
    swalWithBootstrapButtons.fire(
    {
    title: "{!! Config::get('system-lang.' . $lang . '.preguntasuscribirse') !!}",
    text:	"{!! Config::get('system-lang.' . $lang . '.recibirinformacion') !!}",
    icon: 'info',
    showCancelButton: true,
    confirmButtonText: "{!! Config::get('system-lang.' . $lang . '.aceptar') !!}",
    cancelButtonText: "{!! Config::get('system-lang.' . $lang . '.regresar') !!}",
    }).then((result) =>
    {
      if (result.value)
      {
        $.ajax(
        {
          type: 'POST',
          dataType : 'json',
          url: $('#app-url').val() + '/cms/setting/create-subscription',
          data: JSON.stringify({ '_token': $('#app-token').val(), email: email}),
          error: function (jqXHR, statusText, errorThrown)
          {
            // console.log("Error: " + errorThrown)
            // console.log("Status Text:" + statusText)
            // console.log(jqXHR)
          },
          success: function(json)
          {
            if (empty(json.success))
            {
              swalWithBootstrapButtons.fire(
              {
              title: "{!! Config::get('system-lang.' . $lang . '.datosincorrectos') !!}",
              text:  email + " {!! Config::get('system-lang.' . $lang . '.correoinvalido') !!}",
              icon: 'warning',
              allowOutsideClick: false,
              timer:5000
              });
            }
            else
            {
              swalWithBootstrapButtons.fire(
                "{!! Config::get('system-lang.' . $lang . '.suscripcionexito') !!}",
                "{!! Config::get('system-lang.' . $lang . '.sucorreoes') !!}: " + email,
                'success'
              );
              $('.input-email, .input-email2').val('');
            }
          }
        });
      }
      else if (result.dismiss === Swal.DismissReason.cancel)
      {
        swalWithBootstrapButtons.fire(
          "{!! Config::get('system-lang.' . $lang . '.suscripcioncancelada') !!}",
          '',
          'info'
        );
      }
    });
  }

  function normalizeGender (label, genderParams)
  {
    var id = label[0]

    label = genderLabelsMultiSelect.n

    if (id == 'M' && genderParams.length == 1)
    {
      label = genderLabelsMultiSelect.h
    }

    if (genderParams.includes('C'))
    {
      if (id == 'C')
      {
        label = genderLabelsMultiSelect.c
      }
    }

    if (genderParams.includes('H') && genderParams.includes('M') && (id == 'H' || id == 'M'))
    {
      if (id == 'H')
      {
        label = genderLabelsMultiSelect.h
      }
      else
      {
        label = genderLabelsMultiSelect.m
      }
    }

    if (genderParams.includes('M') && genderParams.includes('F') && (id == 'M' || id == 'F'))
    {
      if (id == 'M')
      {
        label = genderLabelsMultiSelect.h
      } else
      {
        label = genderLabelsMultiSelect.m
      }

    }
    return label
  }

  // Create series
  function createAxisAndSeries(chart, field, name)
  {
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

      if(chart.yAxes.indexOf(valueAxis) != 0)
      {
        valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
      }

    var series = chart.series.push(new am4charts.LineSeries());
      series.dataFields.valueY = field;
      series.dataFields.categoryX = "month";
      series.name = name;
      series.tooltipText = "{name}: [bold]{valueY}[/]";
      series.tensionX = 0.8;
      series.showOnInit = true; 

    var interfaceColors = new am4core.InterfaceColorSet();

    var bullet = series.bullets.push(new am4charts.Bullet());
        bullet.width = 10;
        bullet.height = 10;
        bullet.horizontalCenter = "middle";
        bullet.verticalCenter = "middle";
        
        var rectangle = bullet.createChild(am4core.Rectangle);
        rectangle.stroke = interfaceColors.getFor("background");
        rectangle.strokeWidth = 2;
        rectangle.width = 10;
        rectangle.height = 10;

    
  }

  // function line(link, data, tabNumber, fields = ['victims', 'processed', 'judgments'], names = ['Víctimas', 'Casos judicializados', 'Sentencias'])
  function line(link, data, tabNumber, fields = ['victims', 'processed', 'judgments'], names = ["{!! Config::get('system-lang.' . $lang . '.victimas') !!}", "{!! Config::get('system-lang.' . $lang . '.casos') !!}", "{!! Config::get('system-lang.' . $lang . '.sentencias') !!}"])
  {
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end


    // Create chart instance
    let chart = am4core.create("chartdiv-" + tabNumber, am4charts.XYChart);
    tempChart = chart;

    // Create chart ends

    // Increase contrast by taking evey second color
    chart.colors.list = [
    am4core.color('#FF9815'),
    am4core.color('#163D75'),
    am4core.color('#81b222'),
    ];

    // Add data
    chart.data = data;

  // Create axes
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "month";
    categoryAxis.title.text = "Meses";
    categoryAxis.renderer.minGridDistance = 20;

    categoryAxis.renderer.labels.template.rotation = -90;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.verticalCenter = "middle";

    if(fields.length === names.length)
    {
      fields.forEach((field, index) => {
        createAxisAndSeries(chart, field, names[index]);
      });
    }

    // Add legend
    chart.legend = new am4charts.Legend();

    chart.cursor = new am4charts.XYCursor();

    chart.exporting.menu = new am4core.ExportMenu();
    chart.exporting.menu.items = [
      {
        label: '<i class="fas fa-arrow-down mt-2"></i>',
        menu: [
          {
            label: 'Imagen',
            menu: [
              { "type": "png", "label": "PNG" },
              { "type": "jpg", "label": "JPG" },
              { "type": "gif", "label": "GIF" },
              { "type": "svg", "label": "SVG" },
              { "type": "pdf", "label": "PDF" }
            ]
          },
          {
            label: 'Datos',
            menu: [
              { "type": "json", "label": "JSON" },
              { "type": "csv", "label": "CSV" },
              { "type": "xlsx", "label": "XLSX" }
            ]
          },
          {
            "label": "Imprimir",
            "type": "print"
          }
        ]
      }
    ];
  }

  function chart2(data, tabNumber)
  {
    var charData = [];
    am4core.useTheme(am4themes_animated);

    count = 0;

    $.each(data, function( index, year)
    {

      charData.push({
        'year' : year['year'].toString(),
        'fase1' : !(typeof year['Fase Inicial'] === 'undefined') ? !empty(year['Fase Inicial']['count']) ? year['Fase Inicial']['count'] : 0 : 0,
        'fase2' : !(typeof year['Fase Instructora'] === 'undefined') ? !empty(year['Fase Instructora']['count'])?year['Fase Instructora']['count'] : 0 : 0,
        'fase3' : !(typeof year['Fase de Sentencia'] === 'undefined') ? !empty(year['Fase de Sentencia']['count'])?year['Fase de Sentencia']['count'] : 0 : 0,
        'color' : (count % 2 == 0)?'#163D75':'#FF9815'
      });

      count++;
    });

    var chart = am4core.create("chartdiv-" + tabNumber, am4charts.XYChart);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
    chart.data = charData;
    tempChart = chart;

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis())
    categoryAxis.title.text = "{!! Config::get('system-lang.' . $lang . '.anios') !!}"
    categoryAxis.renderer.grid.template.location = 0
    categoryAxis.dataFields.category = "year"
    categoryAxis.renderer.minGridDistance = 10

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis())
    valueAxis.title.text = "{!! Config::get('system-lang.' . $lang . '.frecuencia') !!}"
    valueAxis.min = 0
    valueAxis.strictMinMax = true
    valueAxis.renderer.minGridDistance = 50

    var series = chart.series.push(new am4charts.ColumnSeries())
    series.name = "{!! Config::get('system-lang.' . $lang . '.inicial') !!}";
    // series.name = "Inicial";
    series.dataFields.categoryX = "year"
    series.dataFields.valueY = "fase1"
    series.columns.template.tooltipText = "{name}: [bold]{valueY.value}[/]";
    series.columns.template.tooltipY = 0
    series.columns.template.strokeOpacity = 0

    var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
    categoryLabel.label.text = "{name}";
    categoryLabel.label.dy = 10;
    categoryLabel.label.fill = am4core.color("#fff");

    var series2 = chart.series.push(new am4charts.ColumnSeries())
    series2.name = "{!! Config::get('system-lang.' . $lang . '.instructora') !!}";
    // series2.name = "Instructora";
    series2.dataFields.categoryX = "year"
    series2.dataFields.valueY = "fase2"
    series2.columns.template.tooltipText = "{name}: [bold]{valueY.value}[/]";
    series2.columns.template.tooltipY = 0
    series2.columns.template.strokeOpacity = 0

    var categoryLabel2 = series2.bullets.push(new am4charts.LabelBullet());
    categoryLabel2.label.text = "{name}";
    categoryLabel2.label.dy = 10;
    categoryLabel2.label.fill = am4core.color("#fff");

    var series3 = chart.series.push(new am4charts.ColumnSeries())
    series3.name = "{!! Config::get('system-lang.' . $lang . '.sentencia') !!}";
    // series3.name = "Sentencia";
    series3.dataFields.categoryX = "year"
    series3.dataFields.valueY = "fase3"
    series3.columns.template.tooltipText = "{name}: [bold]{valueY.value}[/]";
    series3.columns.template.tooltipY = 0
    series3.columns.template.strokeOpacity = 0

    var categoryLabel3 = series3.bullets.push(new am4charts.LabelBullet());
    categoryLabel3.label.text = "{name}";
    categoryLabel3.label.dy = 10;
    categoryLabel3.label.fill = am4core.color("#fff");

    series.columns.template.adapter.add("fill", function(fill, target)
    {
      return target.dataItem.dataContext.color;
    });

    series2.columns.template.adapter.add("fill", function(fill, target)
    {
      return target.dataItem.dataContext.color;
    });

    series3.columns.template.adapter.add("fill", function(fill, target)
    {
      return target.dataItem.dataContext.color;
    });
  }

  function chartTwoColumnByYear(data, labels, tabNumber, isGender)
  {
    var charData = [];
    am4core.useTheme(am4themes_animated);
    if (isGender)
    {
      am4core.useTheme(am4themes_oadhGendersTheme);
    }
    else
    {
    am4core.useTheme(am4themes_TwoBarsTheme);
    }

    $.each(data, function(index, row)
    {
      charData.push({
        'year' : row['year'].toString(),
        'column_a' : !empty(row['column_a']) ? row['column_a'] : 0,
        'column_b' : !empty(row['column_b']) ? row['column_b'] : 0,
      });
    });

    var chart = am4core.create("chartdiv-" + tabNumber, am4charts.XYChart);
    chart.hiddenState.properties.opacity = 0;
    chart.data = charData;
    tempChart = chart;

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.title.text =  !empty(labels['categoryAxis'])?labels['categoryAxis']:"{!! Config::get('system-lang.' . $lang . '.anios') !!}";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataFields.category = "year";
    categoryAxis.renderer.minGridDistance = 10;

    if (!empty(labels['categoryAxisRotation']))
    {
    categoryAxis.renderer.labels.template.rotation = labels['categoryAxisRotation'];
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.verticalCenter = "middle";
    }

    categoryAxis.renderer.cellStartLocation = 0;
    categoryAxis.renderer.cellEndLocation = 0.9;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = !empty(labels['valueAxis'])?labels['valueAxis']:"{!! Config::get('system-lang.' . $lang . '.frecuencia') !!}";
    valueAxis.min = 0
    valueAxis.strictMinMax = true
    valueAxis.renderer.minGridDistance = 50

    var series = chart.series.push(new am4charts.ColumnSeries())
    series.name = labels['column_a'];
    series.dataFields.categoryX = "year"
    series.dataFields.valueY = "column_a"
    series.columns.template.tooltipText = "{name}: [bold]{valueY.value}[/]";
    series.columns.template.tooltipY = 0
    series.columns.template.strokeOpacity = 0
    series.columns.template.width = am4core.percent(100);

    var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
    categoryLabel.label.text = "{name}";
    categoryLabel.label.dy = 10;
    categoryLabel.label.fill = am4core.color("#fff");


    var series2 = chart.series.push(new am4charts.ColumnSeries())
    series2.name = labels['column_b'];
    series2.dataFields.categoryX = "year";
    series2.dataFields.valueY = "column_b";
    // series2.columns.template.tooltipText = "{valueY.value} "
    series2.columns.template.tooltipText = "{name}: [bold]{valueY.value}[/]";
    series2.columns.template.tooltipY = 0;
    series2.columns.template.strokeOpacity = 0;
    series2.columns.template.width = am4core.percent(100);

    var categoryLabel2 = series2.bullets.push(new am4charts.LabelBullet());
    categoryLabel2.label.text = "{name}";
    categoryLabel2.label.dy = 10;
    categoryLabel2.label.fill = am4core.color("#fff");

    chart.exporting.menu = new am4core.ExportMenu();
    chart.exporting.menu.items = [
      {
        label: '<i class="fas fa-arrow-down mt-2"></i>',
        menu: [
          {
            label: 'Imagen',
            menu: [
              { "type": "png", "label": "PNG" },
              { "type": "jpg", "label": "JPG" },
              { "type": "gif", "label": "GIF" },
              { "type": "svg", "label": "SVG" },
              { "type": "pdf", "label": "PDF" }
            ]
          },
          {
            label: 'Datos',
            menu: [
              { "type": "json", "label": "JSON" },
              { "type": "csv", "label": "CSV" },
              { "type": "xlsx", "label": "XLSX" }
            ]
          },
          {
            "label": "Imprimir",
            "type": "print"
          }
        ]
      }
    ];
  }

  function clearArrayFilterValues (values)
  {
    for (var key in values)
    {
      values[key] = values[key].split("_")[1];
    }

    return values;
  }

  function buildFiltersAux(filter, divId, data)
  {
    var formGroup, formHorizontalGroup, inputFromContainer, inputToContainer, filterLabel, filterDropdown, filterDropdownOption, genderParams, filterData, input, inputFrom, inputTo;

    id = divId + filter.filter;
    filterData = data[filter['filter']];
    formGroup = document.createElement('div');
    inputContainer = document.createElement('div');
    filterLabel = document.createElement('label');

    formGroup.className = 'form-group row';
    inputContainer.className = 'col-sm-9';
    filterLabel.htmlFor = id;
    filterLabel.className = 'col-sm-3 col-form-label';
    filterLabel.appendChild(document.createTextNode(filter.label));

    if (filter.filter == 'gender')
    {
      genderParams = getGenderLabels(filterData);
    }

    if (filter.type == 'slider')
    {
      input = document.createElement('input');
      input.id = divId + filter.filter;
      input.setAttribute('type', 'text');

      if (!empty(filter.range) && filterData.length > 1)
      {
        input.setAttribute('data-slider-value', '[' + Math.min.apply(null, filterData) + ','+ Math.max.apply(null, filterData) + ']');
      }
      else
      {
        input.setAttribute('data-slider-min', Math.min.apply(null, filterData));
        input.setAttribute('data-slider-max', Math.max.apply(null, filterData));
      }

      formGroup.appendChild(filterLabel);
      inputContainer.appendChild(input);
      formGroup.appendChild(inputContainer);

      $('#' + divId).append(formGroup);

      if (empty(filter.range)) 
      {
        $(input).bootstrapSlider(
        {
          ticks: filterData,
          ticks_labels: filterData,
          lock_to_ticks: true,
          value: (filter.range == false) ? Math.max.apply(null, filterData) : '',
        });
      }
      else
      {
        if(!empty(filter.limit) && filterData.length > 3)
        {
          
          let max = Math.max.apply(null, filterData);
          input.className = 'oadh-slider-limit';

          $(input).bootstrapSlider(
          {
            ticks: filterData,
            ticks_labels: filterData,
            lock_to_ticks: true,
            value: [max - 2, max]
          });
        }
        else if(!empty(data.year_chart) && !empty(filter.limit) && data.year_chart.length > 1)
        {
          $(input).bootstrapSlider(
          {
            ticks: filterData,
            ticks_labels: filterData,
            lock_to_ticks: true,
            value: data.year_chart,
          });
        }
        else
        {
          $(input).bootstrapSlider(
          {
            ticks: filterData,
            ticks_labels: filterData,
            lock_to_ticks: true,
          });
        }
        
      }
    }

    if (filter.type == 'select')
    {
      filterDropdown = document.createElement('select');
      filterDropdown.id = divId + filter.filter;

      filterDropdown.setAttribute('multiple', 'multiple')

      filterDropdown.style.display = 'none'

      filterData.forEach(function (item, index)
      {
        option = document.createElement('option')
        option.value = filter.filter[0] + '_' + item.value

        if (filter.filter == "gender")
        {
          item.label = normalizeGender(item.label, genderParams)
        }

        option.appendChild(document.createTextNode(item.label))
        filterDropdown.appendChild(option)
      });

      formGroup.appendChild(filterLabel);
      inputContainer.appendChild(filterDropdown);
      formGroup.appendChild(inputContainer);

      $('#' + divId).append(formGroup);

      $(filterDropdown).bsMultiSelect(
      {
        selectedPanelDefMinHeight: 'calc(2.25rem + 2px)',  // default size
        selectedPanelLgMinHeight: 'calc(2.875rem + 2px)',  // LG size
        selectedPanelSmMinHeight: 'calc(1.8125rem + 2px)', // SM size
        selectedPanelDisabledBackgroundColor: '#e9ecef',   // disabled background
        selectedPanelFocusBorderColor: '#80bdff',          // focus border
        selectedPanelFocusBoxShadow: '0 0 0 0.2rem rgba(0, 123, 255, 0.25)',  // foxus shadow
        selectedPanelFocusValidBoxShadow: '0 0 0 0.2rem rgba(40, 167, 69, 0.25)',  // valid foxus shadow
        selectedPanelFocusInvalidBoxShadow: '0 0 0 0.2rem rgba(220, 53, 69, 0.25)',  // invalid foxus shadow
        inputColor: '#495057', // color of keyboard entered text
        selectedItemContentDisabledOpacity: '.65', // btn disabled opacity used
      });

    }

    if (filter.type == 'ageRange')
    {
      formHorizontalGroup = document.createElement('div');
      formHorizontalGroup.className = 'form-group row';

      inputFromContainer = document.createElement('div');
      inputFromContainer.className = 'col-sm-3 oadh-mb-sm-2';
      inputToContainer = document.createElement('div');
      inputToContainer.className = 'col-sm-3';

      inputFrom = document.createElement('input');
      inputFrom.setAttribute('type', 'text');
      inputFrom.setAttribute('data-age-type', 'from');
      inputFrom.className = 'form-control';
      inputFrom.setAttribute('placeholder', "{!! Config::get('system-lang.' . $lang . '.del') !!}");
      inputFrom.id = divId + filter.filter + 'From';

      inputTo = document.createElement('input');
      inputTo.setAttribute('type', 'text');
      inputTo.setAttribute('data-age-type', 'to');
      inputTo.className = 'form-control';
      inputTo.setAttribute('placeholder', "{!! Config::get('system-lang.' . $lang . '.al') !!}");
      inputTo.id = divId + filter.filter + 'To';

      formHorizontalGroup.appendChild(filterLabel);

      inputFromContainer.appendChild(inputFrom);
      inputToContainer.appendChild(inputTo);

      formHorizontalGroup.appendChild(inputFromContainer);
      formHorizontalGroup.appendChild(inputToContainer);

      $('#' + divId).append(formHorizontalGroup);
    }
  }

  function buildFilters(link, data)
  {
    if($('#' + $(link).attr('data-div-filter-id')).children().length != 0)
    {
      return;
    }

    $('#oadh-chart-title').text($(link).text());

    $('#filter-description').html($(link).attr('data-description'));
    $('#filter-description').attr('data-key', $(link).attr('data-description-key'));

    $('#' + $(link).attr('data-div-reference-id')).html($(link).attr('data-reference'));
    $('#' + $(link).attr('data-div-reference-id')).attr('data-key', $(link).attr('data-reference-key'));

    $('.dataReference').html($(this).attr('data-reference'));

    if(!empty($(link).attr('data-filters')))
    {
      $.each(JSON.parse($(link).attr('data-filters')), function(index, filter)
      {
        buildFiltersAux(filter, $(link).attr('data-div-filter-id'), data);
      });
    }
    
  }

  function getFilterData(link, divId)
  {
    var filters = {}, value, age = [];

    if($('#' + $(link).attr('data-div-filter-id')).children().length != 0)
    {
      $.each(JSON.parse($(link).attr('data-filters')), function(index, filter)
      {
        if (filter.type == 'select')
        {
          filters[filter['filter']] = clearArrayFilterValues($('#' + $(link).attr('data-div-filter-id') + filter['filter']).val());
        }
        else if (filter.type == 'slider')
        {
          value = $('#' + $(link).attr('data-div-filter-id') + filter['filter']).bootstrapSlider('getValue');

          if (!empty(filter.range))
          {
            if(!Number.isInteger(value))
            {
              filters[filter['filter'] + 'From'] = value[0];
              filters[filter['filter'] + 'To'] = value[1];
            }
            else
            {
              filters[filter['filter'] + 'From'] = value;
              filters[filter['filter'] + 'To'] = value;
            }
          }
          else
          {
            filters[filter['filter']] = [value];
          }
        }
        else if (filter.type == 'ageRange')
        {
          if(!isNaN(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'From').val())) && parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'From').val()) >= 0)
          {
            age.push(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'From').val()));
          }

          if(!isNaN(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'To').val())) && parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'To').val()) <= 100)
          {
            age.push(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'To').val()));
          }

          if (isNaN(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'From').val())) && !isNaN(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'To').val())))
          {
            $('#' + $(link).attr('data-div-filter-id')+filter['filter']+'From').val(0);
            age.push(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'From').val()));
          }

          if (isNaN(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'To').val())) && !isNaN(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'From').val())))
          {
            $('#' + $(link).attr('data-div-filter-id')+filter['filter']+'To').val(100);
            age.push(parseInt($('#' + $(link).attr('data-div-filter-id')+filter['filter']+'To').val()));
          }

          if (age[0] > age[1])
          {
            age.reverse();
          }

          filters[filter['filter']] = age;
        }
      });
    }
    else
    {
      if(!empty($(link).attr('data-filters')))
      {
        $.each(JSON.parse($(link).attr('data-filters')), function(index, filter)
        {
          if (filter.type == 'slider')
          {
            if (!empty(filter.range))
            {
              filters[filter['filter'] + 'From'] = '';
              filters[filter['filter'] + 'To'] = '';
            }
            else
            {
              filters[filter['filter']] = '';
            }
          }
          else
          {
            filters[filter['filter']] = '';
          }
        });
      }
    }

    return filters;
  }

  function createAmChartMaps(link, dataObject, animation = false)
  {
    var values = [];
    var maxVal = 0;
    var minVal = 0;

    if(!empty(dataObject))
    {
      dataObject.forEach(element => {
      values.push(element.value);
      });
    }
    

    minVal = Math.min.apply(null, values);
    maxVal = Math.max.apply(null, values);

    // Themes begin
    am4core.unuseTheme(am4themes_animated);
    am4core.unuseTheme(am4themes_TwoBarsTheme);
    am4core.unuseTheme(am4themes_oadhGendersTheme);
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create map instance
    var chart = am4core.create($(link).attr('data-div-id'), am4maps.MapChart);
    tempChart = chart;
    // var chart = am4core.create('mapdiv-1', am4maps.MapChart);

    // Set map definition
    chart.geodata = am4geodata_elSalvadorLow;

    // Set projection
    chart.projection = new am4maps.projections.Projection();

    chart.maxZoomLevel = 1;
    chart.seriesContainer.draggable = false;

    // Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    //Set min/max fill color for each area
    polygonSeries.heatRules.push(
      {
        property: "fill",
        target: polygonSeries.mapPolygons.template,
        min: chart.colors.getIndex(1).brighten(1),
        max: chart.colors.getIndex(1).brighten(-0.3)
      });

    // Make map load polygon data (state shapes and names) from GeoJSON
    polygonSeries.useGeodata = true;

    // Set heatmap values for each state
    polygonSeries.data = dataObject

    // Set up heat legend
    if (!empty(dataObject)) 
    {
      let heatLegend = chart.createChild(am4maps.HeatLegend);
      heatLegend.series = polygonSeries;
      heatLegend.align = "left";
      heatLegend.valign = "bottom";
      heatLegend.width = am4core.percent(15);
      heatLegend.marginLeft = am4core.percent(6);
      heatLegend.minValue = minVal;
      heatLegend.maxValue = maxVal;

      // Set up custom heat map legend labels using axis ranges
      var minRange = heatLegend.valueAxis.axisRanges.create();
      minRange.value = heatLegend.minValue;
      minRange.label.text = heatLegend.minValue;
      var maxRange = heatLegend.valueAxis.axisRanges.create();
      maxRange.value = heatLegend.maxValue;
      maxRange.label.text = heatLegend.maxValue;

      // Blank out internal heat legend value axis labels
      heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText)
      {
        return "";
      });

      // Configure series tooltip
      var polygonTemplate = polygonSeries.mapPolygons.template;
      polygonTemplate.tooltipText = "{name}: {value}{legend}";
      polygonTemplate.nonScalingStroke = true;
      polygonTemplate.strokeWidth = 0.5;

      polygonTemplate.events.on("hit", function(ev)
      {
        var data = ev.target.dataItem.dataContext;
        $('#' + $(link).attr('data-grid-id')).createTable('', '', '', data.rows, data.headers, 'table table-striped table-sm');
      });

      var hs = polygonTemplate.states.create("hover");
      hs.properties.fill = am4core.color("#3c5bdc");
    }


    if(animation)
    {
      minRange.label.fill = '#ffffff';
      maxRange.label.fill = '#ffffff';
      chart.defaultState.transitionEasing = am4core.ease.circleIn;
    }
    else
    {
      chart.exporting.menu = new am4core.ExportMenu();
      chart.exporting.menu.items = [
        {
          label: '<i class="fas fa-arrow-down mt-2"></i>',
          menu: [
            {
              label: 'Imagen',
              menu: [
                { "type": "png", "label": "PNG" },
                { "type": "jpg", "label": "JPG" },
                { "type": "gif", "label": "GIF" },
                { "type": "svg", "label": "SVG" },
                { "type": "pdf", "label": "PDF" }
              ]
            },
            {
              label: 'Datos',
              menu: [
                { "type": "json", "label": "JSON" },
                { "type": "csv", "label": "CSV" },
                { "type": "xlsx", "label": "XLSX" }
              ]
            },
            {
              "label": "Imprimir",
              "type": "print"
            }
          ]
        }
      ];
    }
  }

  /*
  * TEMAS
  */

  function am4themes_oadhTheme(target)
  {
    if (target instanceof am4core.ColorSet)
    {
      target.list = [
        am4core.color("#163D75")
      ];
    }
  }

  function am4themes_TwoBarsTheme(target)
  {
    if (target instanceof am4core.ColorSet) {
      target.list = [
        am4core.color("#163D75"),
        am4core.color("#FF9815")
        ];
    }
  }

  function am4themes_oadhGendersTheme(target)
  {
    if (target instanceof am4core.ColorSet) {
      target.list = [
        am4core.color("#3c3c6d"),
        am4core.color("#81b222")
        ];
    }
  }

  function createAmChartPie(link, dataObject, genderColorset)
  {

    var chart = am4core.create($(link).attr('data-div-id'), am4charts.PieChart3D);
    tempChart = chart;

    am4core.useTheme(am4themes_animated);

    dataObject.forEach(element =>
    {
      switch (element.category)
      {
        case 'M':
          // element.category = 'Masculino';
          element.category = "{!! Config::get('system-lang.' . $lang . '.hombre') !!}";
          break;
        case 'F':
          // element.category = 'Femenino'
          element.category = "{!! Config::get('system-lang.' . $lang . '.mujer') !!}"
          break;
        case 'I':
          // element.category = 'Indeterminado'
          element.category = "{!! Config::get('system-lang.' . $lang . '.nodeterminado') !!}"
          break;

        default:
          break;
      }
    });

    chart.data = dataObject;

    var pieSeries = chart.series.push(new am4charts.PieSeries3D());
    pieSeries.dataFields.value = "value";
    pieSeries.dataFields.category = "category";

    pieSeries.labels.template.disabled = true;
    pieSeries.ticks.template.disabled = true;
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeWidth = 0.2;
    pieSeries.slices.template.strokeOpacity = 1;

    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;

    if (genderColorset) {
      pieSeries.colors.list = [
        am4core.color('#3c3c6d'),
        am4core.color('#81b222'),
        am4core.color('#597a91'),
      ];
    }
    else {
      pieSeries.colors.list = [
        am4core.color('rgb(255, 155, 33)'),
        am4core.color('rgb(89, 122, 145)'),
        am4core.color('rgb(255, 207, 81)'),
        am4core.color('rgb(81, 116, 144)'),
        am4core.color('rgb(227, 169,43)'),
        am4core.color('rgb(94, 115, 136)'),
        am4core.color('rgb(214,168,119)'),
        am4core.color('rgb(76, 91, 110)'),
        am4core.color('rgb(255, 155, 57)'),
        am4core.color('rgb(72,103, 112)'),
        am4core.color('rgb(186, 141, 28)'),
        am4core.color('rgb(179, 87, 0)'),
        am4core.color('rgb(11, 180, 188)'),
        am4core.color('rgb(223, 103,6)'),
        am4core.color('rgb(135, 175, 165)'),
        am4core.color('rgb(188, 98,38)'),
        am4core.color('rgb(141, 169, 193)'),
        am4core.color('rgb(209, 131, 78)'),
        am4core.color('rgb(209, 131, 78)')
      ];
    }

    chart.exporting.menu = new am4core.ExportMenu();
    chart.exporting.menu.items = [
      {
        label: '<i class="fas fa-arrow-down mt-2"></i>',
        menu: [
          {
            label: 'Imagen',
            menu: [
              { "type": "png", "label": "PNG" },
              { "type": "jpg", "label": "JPG" },
              { "type": "gif", "label": "GIF" },
              { "type": "svg", "label": "SVG" },
              { "type": "pdf", "label": "PDF" }
            ]
          },
          {
            label: 'Datos',
            menu: [
              { "type": "json", "label": "JSON" },
              { "type": "csv", "label": "CSV" },
              { "type": "xlsx", "label": "XLSX" }
            ]
          },
          {
            "label": "Imprimir",
            "type": "print"
          }
        ]
      }
    ];
  }

  function chartClusteredColumn(link, dataObject, yearObject)
  {
    am4core.useTheme(am4themes_animated);
    var chart = am4core.create($(link).attr('data-div-id'), am4charts.XYChart);
    tempChart = chart;

    chart.data = dataObject;

    var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
    xAxis.dataFields.category = 'category'
    xAxis.renderer.cellStartLocation = 0.1
    xAxis.renderer.cellEndLocation = 0.9
    xAxis.renderer.grid.template.location = 0;

    var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
    yAxis.min = 0;

    chart.colors.list = [
    am4core.color('#FF9815'),
    am4core.color('#163D75'),
    ];

    yearObject.forEach(function(value, index)
    {
      createSeries(chart, value);
    })

    chart.legend = new am4charts.Legend()

    chart.exporting.menu = new am4core.ExportMenu();
    chart.exporting.menu.items = [
      {
        label: '<i class="fas fa-arrow-down mt-2"></i>',
        menu: [
          {
            label: 'Imagen',
            menu: [
              { "type": "png", "label": "PNG" },
              { "type": "jpg", "label": "JPG" },
              { "type": "gif", "label": "GIF" },
              { "type": "svg", "label": "SVG" },
              { "type": "pdf", "label": "PDF" }
            ]
          },
          {
            label: 'Datos',
            menu: [
              { "type": "json", "label": "JSON" },
              { "type": "csv", "label": "CSV" },
              { "type": "xlsx", "label": "XLSX" }
            ]
          },
          {
            "label": "Imprimir",
            "type": "print"
          }
        ]
      }
    ];
  }


  function createSeries(chart, name) 
  {
    var series = chart.series.push(new am4charts.ColumnSeries())
    series.dataFields.valueY = name
    series.dataFields.categoryX = 'category'
    series.name = name
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";

    var bullet = series.bullets.push(new am4charts.LabelBullet())
    bullet.interactionsEnabled = false
    bullet.dy = 30;
    bullet.label.text = '{valueY}'
    bullet.label.fill = am4core.color('#ffffff')

    return series;
  }

  function chartOneColumn(link, dataObject, labels, horizontal = false)
  {
    // console.log(dataObject);
    $year = [];
    am4core.useTheme(am4themes_animated);

    $.each(dataObject, (index, data) =>
    {
      data['color'] = (index % 2 == 0) ? '#163D75':'#FF9815';
      if (!empty(data['year']))
      {
        data['year'] = data['year'].toString();
      }

      if(Number.isInteger(data['label']))
      {
        data['label'] = data['label'].toString();
      }
    });

    var chart = am4core.create($(link).attr('data-div-id'), am4charts.XYChart);
    tempChart = chart;
    chart.data = dataObject;

    if(horizontal)
    {
      var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "label";
      categoryAxis.title.text = !empty(labels['categoryAxis']) ? labels['categoryAxis'] : "{!! Config::get('system-lang.' . $lang . '.anios') !!}";

      if (!empty(labels['categoryAxisRotation']))
      {
      categoryAxis.renderer.labels.template.rotation = labels['categoryAxisRotation'];
      categoryAxis.renderer.labels.template.horizontalCenter = "right";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      }
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 30;
      categoryAxis.renderer.grid.template.disabled = true;

      var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
      valueAxis.title.text = !empty(labels['valueAxis']) ? labels['valueAxis'] : "{!! Config::get('system-lang.' . $lang . '.frecuencia') !!}";
      valueAxis.min = 0;
      valueAxis.strictMinMax = true

      // Create series
      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueX = "value";
      series.dataFields.categoryY = "label";
      series.columns.template.tooltipText = "{categoryY}: [bold]{valueX}[/]";
      series.columns.template.fillOpacity = 1;
    }
    else
    {
      var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "label";
      categoryAxis.title.text = !empty(labels['categoryAxis']) ? labels['categoryAxis'] : "{!! Config::get('system-lang.' . $lang . '.anios') !!}";

      if (!empty(labels['categoryAxisRotation']))
      {
      categoryAxis.renderer.labels.template.rotation = labels['categoryAxisRotation'];
      categoryAxis.renderer.labels.template.horizontalCenter = "right";
      categoryAxis.renderer.labels.template.verticalCenter = "middle";
      }
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.minGridDistance = 30;
      categoryAxis.renderer.grid.template.disabled = true;

      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.title.text = !empty(labels['valueAxis']) ? labels['valueAxis'] : "{!! Config::get('system-lang.' . $lang . '.frecuencia') !!}";
      valueAxis.min = 0;
      valueAxis.strictMinMax = true

      // Create series
      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = "value";
      series.dataFields.categoryX = "label";
      series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
      series.columns.template.fillOpacity = 1;
    }

    var label = categoryAxis.renderer.labels.template;
    label.wrap = true;
    label.maxWidth = 200;

    var columnTemplate = series.columns.template;
    columnTemplate.stroke = '#222222';
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 0.5;

    series.columns.template.adapter.add("fill", function(fill, target)
    {
      return target.dataItem.dataContext.color;
    });

    chart.exporting.menu = new am4core.ExportMenu();
    chart.exporting.menu.items = [
      {
        label: '<i class="fas fa-arrow-down mt-2"></i>',
        menu: [
          {
            label: 'Imagen',
            menu: [
              { "type": "png", "label": "PNG" },
              { "type": "jpg", "label": "JPG" },
              { "type": "gif", "label": "GIF" },
              { "type": "svg", "label": "SVG" },
              { "type": "pdf", "label": "PDF" }
            ]
          },
          {
            label: 'Datos',
            menu: [
              { "type": "json", "label": "JSON" },
              { "type": "csv", "label": "CSV" },
              { "type": "xlsx", "label": "XLSX" }
            ]
          },
          {
            "label": "Imprimir",
            "type": "print"
          }
        ]
      }
    ];
  }

  // function createBubbleChart(chartId, year)
  // {
  //   const labels = [
  //     {month: '0', number: 0},
  //     {month: 'Enero', number: 1},
  //     {month: 'Febrero', number: 2},
  //     {month: 'Marzo', number: 3},
  //     {month: 'Abril', number: 4},
  //     {month: 'Mayo', number: 5},
  //     {month: 'Junio', number: 6},
  //     {month: 'Julio', number: 7},
  //     {month: 'Agosto', number: 8},
  //     {month: 'Septiembre', number: 9},
  //     {month: 'Octubre', number: 10},
  //     {month: 'Noviembre', number: 11},
  //     {month: 'Diciembre', number: 12},
  //   ];

  //   const dataset = [
  //     {x: 0, y: 0, r: 0},
  //     {title: 'Los familiares de siete personas víctimas de desapariciones forzadas, durante el conflicto armado salvadoreño, denuncia a la FAES y dos organizaciones que integraron el FLMN.', date: '31/31/2020', x: 1, y: 15, r: 5},
  //     {title: 'Los familiares de siete personas víctimas de desapariciones forzadas, durante el conflicto armado salvadoreño, denuncia a la FAES y dos organizaciones que integraron el FLMN.', date: '31/31/2020', x: 1, y: 25, r: 5},
  //     {title: 'Los familiares de siete personas víctimas de desapariciones forzadas, durante el conflicto armado salvadoreño, denuncia a la FAES y dos organizaciones que integraron el FLMN.', date: '31/31/2020', x: 2, y: 20, r: 5},
  //     {title: 'Los familiares de siete personas víctimas de desapariciones forzadas, durante el conflicto armado salvadoreño, denuncia a la FAES y dos organizaciones que integraron el FLMN.', date: '31/31/2020', x: 3, y: 11, r: 5},
  //     {title: 'Los familiares de siete personas víctimas de desapariciones forzadas, durante el conflicto armado salvadoreño, denuncia a la FAES y dos organizaciones que integraron el FLMN.', date: '31/31/2020', x: 4, y: 12, r: 5},
  //     {title: 'Los familiares de siete personas víctimas de desapariciones forzadas, durante el conflicto armado salvadoreño, denuncia a la FAES y dos organizaciones que integraron el FLMN.', date: '31/31/2020', x: 5, y: 30, r: 5},
  //     {title: 'Los familiares de siete personas víctimas de desapariciones forzadas, durante el conflicto armado salvadoreño, denuncia a la FAES y dos organizaciones que integraron el FLMN.', date: '31/31/2020', x: 7, y: 14, r: 5},
  //   ]

  //   const data = {
  //     datasets: [{
  //       label: 'Noticia',
  //       backgroundColor: 'rgba(0, 22, 77, 0.8)',
  //       borderColor: 'rgb(0, 22, 77)',
  //       data: dataset,
  //     }]
  //   };

  //   const config = {
  //     type: 'bubble',
  //     data,
  //     options: {
  //       responsive: true,
  //       plugins: {
  //         legend: {
  //           position: 'top',
  //         },
  //         title: {
  //           display: false,
  //         },
  //         tooltip: {
  //           callbacks: {
  //             label: function(context)
  //             {
  //               var label = context.dataset.label || '';

  //               if (label) {
  //                   label += ': ';
  //               }

  //               label += context.raw.title;

  //               return label
  //             },
  //             title: function(context)
  //             {
  //               let [ctx] = context;

  //               return `{{ Config::get('system-lang.' . $lang . '.fecha') }}: ${ctx.raw.date}`;
  //             }
  //           } 
  //         }
  //       },    
  //   scales: {
  //     x: {
  //       ticks: {
  //           callback: function(value, index, values) {
              
  //             let [month] = labels.filter(month => month.number == value)
              
  //             return month.month;
  //           }
  //         }
  //       }
  //     }
  //     }
  //   };
    
  //   var element = document.getElementById(chartId);


  //   var myChart = new Chart(
  //     element,
  //     config
  //   );

  //   tempChartTimeline = myChart;

  //   window.scrollTo(element.offsetLeft, element.offsetTop);
  // }

  function createTimeLineStageOne(link)
  {

    $('#btn-filter, #btn-download-xlsx').addClass('d-none');
    $('#oadh-filter-title').text('Línea de tiempo');
    
      // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    //data for stageOne Year timeline
    let yearObject = [];

    Object.keys(oadhTimeLineData).forEach((value, index) => {
      yearObject = [...yearObject, {
        category: "",
        year: value,
        size: oadhTimeLineYearSize[value],
        text: "{!! Config::get('system-lang.' . $lang . '.noticiasanuales') !!}"
      }]
    })


    var chart = am4core.create($(link).attr('data-div-id'), am4plugins_timeline.CurveChart);
    chart.curveContainer.padding(0, 100, 0, 120);
    chart.maskBullets = false;
    tempChart = chart;

    var colorSet = new am4core.ColorSet();

    chart.data = yearObject;
    chart.dateFormatter.inputDateFormat = "yyyy";

    chart.fontSize = 11;
    chart.tooltipContainer.fontSize = 11;

    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.grid.template.disabled = true;

    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.points = [{ x: -400, y: 0 }, { x: 0, y: 50 }, { x: 400, y: 0 }]
    dateAxis.renderer.polyspline.tensionX = 0.8;
    dateAxis.renderer.grid.template.disabled = true;
    dateAxis.renderer.line.strokeDasharray = "1,4";
    dateAxis.baseInterval = {period:"day", count:1}; // otherwise initial animation will be not smooth
    switch ($('#' + $(link).attr('data-div-id')).attr('data-date-type')) 
    {
      case 'month':
      dateAxis.dateFormatter.dateFormat = "MMMM";
        break;
      case 'day':
      dateAxis.dateFormatter.dateFormat = "dd";
        break;
    
      default:
      dateAxis.dateFormatter.dateFormat = "yy";
        break;
    }

    dateAxis.renderer.labels.template.disabled = true;


    var series = chart.series.push(new am4plugins_timeline.CurveLineSeries());
    series.strokeOpacity = 0;
    
    
    series.dataFields.dateX = "year";
    series.dataFields.categoryY = "category";
    series.dataFields.value = "size";
    series.baseAxis = categoryAxis;

    var interfaceColors = new am4core.InterfaceColorSet();

    series.tooltip.pointerOrientation = "down";

    var distance = 100;
    var angle = 60;

    var bullet = series.bullets.push(new am4charts.Bullet());

    // var line = bullet.createChild(am4core.Line);
    // line.adapter.add("stroke", function(fill, target) {
    //   if (target.dataItem) {
    //     return chart.colors.getIndex(target.dataItem.index)
    //   }
    // });

    // line.x1 = 0;
    // line.y1 = 0;
    // line.y2 = 0;
    // line.x2 = distance - 10;
    // line.strokeDasharray = "1,3";

    var circle = bullet.createChild(am4core.Circle);
    circle.radius = 30;
    circle.fillOpacity = 1;
    circle.strokeOpacity = 0;

    var circleHoverState = circle.states.create("hover");
    circleHoverState.properties.scale = 1.3;

    series.heatRules.push({ target: circle, min: 20, max: 50, property: "radius" });
    circle.adapter.add("fill", function(fill, target) {
      if (target.dataItem) {
        return chart.colors.getIndex(target.dataItem.index)
      }
    });
    circle.tooltipText = "{text}: {value}";
    circle.adapter.add("tooltipY", function(tooltipY, target){
      return -target.pixelRadius - 4;
    });

    var yearLabel = bullet.createChild(am4core.Label);
    yearLabel.text = "{year}";
    yearLabel.strokeOpacity = 0;
    yearLabel.fill = am4core.color("#fff");
    yearLabel.horizontalCenter = "middle";
    yearLabel.verticalCenter = "middle";
    yearLabel.interactionsEnabled = false;

    // var label = bullet.createChild(am4core.Label);
    // label.propertyFields.text = "text";
    // label.strokeOpacity = 0;
    // label.horizontalCenter = "right";
    // label.verticalCenter = "middle";

    // label.adapter.add("opacity", function(opacity, target) {
    //   if(target.dataItem){
    //     var index = target.dataItem.index;
    //     var line = target.parent.children.getIndex(0);

    //     if (index % 2 == 0) {
    //       target.y = -distance * am4core.math.sin(-angle);
    //       target.x = -distance * am4core.math.cos(-angle);
    //       line.rotation = -angle - 180;
    //       target.rotation = -angle;
    //     }
    //     else {
    //       target.y = -distance * am4core.math.sin(angle);
    //       target.x = -distance * am4core.math.cos(angle);
    //       line.rotation = angle - 180;
    //       target.rotation = angle;
    //     }
    //   }
    //   return 1;
    // });

    var outerCircle = bullet.createChild(am4core.Circle);
    outerCircle.radius = 30;
    outerCircle.fillOpacity = 0;
    outerCircle.strokeOpacity = 0;
    outerCircle.strokeDasharray = "1,3";

    var hoverState = outerCircle.states.create("hover");
    hoverState.properties.strokeOpacity = 0.8;
    hoverState.properties.scale = 1.5;

    outerCircle.events.on("over", function(event){
      var circle = event.target.parent.children.getIndex(1);
      circle.isHover = true;
      event.target.stroke = circle.fill;
      event.target.radius = circle.pixelRadius;
      event.target.animate({property: "rotation", from: 0, to: 360}, 4000, am4core.ease.sinInOut);
    });

    outerCircle.events.on("out", function(event){
      var circle = event.target.parent.children.getIndex(1);
      circle.isHover = false;
    });

    chart.scrollbarX = new am4core.Scrollbar();
    chart.scrollbarX.opacity = 0.5;
    chart.scrollbarX.width = am4core.percent(50);
    chart.scrollbarX.align = "center";

    let child = $('#' + $(link).attr('data-div-id')).attr('data-child-div');

    circle.events.on("hit", function(event)
    {
      if(!empty(child))
      {
        let {year} = event.target.dataItem.dataContext;
        createTimeLineStageTwo($('#' + child), year);
        var element = document.getElementById(child);

        window.scrollTo(element.offsetLeft, element.offsetTop);
      }
    });
  }

  function createTimeLineStageTwo(link, year)
  {
    let dataFromYear = oadhTimeLineData[year];
    let monthsObject = [];

    Object.keys(dataFromYear).forEach(function(value, index) {
      monthsObject = [...monthsObject, {
        category: "",
        year: new Date(year, (value - 1)),
        size: dataFromYear[value].length,
        text: "{!! Config::get('system-lang.' . $lang . '.noticiasmensuales') !!}"
      }]
    });

    $(link).removeClass('d-none');
    var chart = am4core.create($(link).attr('data-div-id'), am4plugins_timeline.CurveChart);
    chart.curveContainer.padding(0, 100, 0, 120);
    chart.maskBullets = false;
    tempChart = chart;

    var colorSet = new am4core.ColorSet();

    chart.data = monthsObject;
    chart.dateFormatter.inputDateFormat = "yyyy";
    chart.language.locale = am4lang_es_ES;

    chart.fontSize = 11;
    chart.tooltipContainer.fontSize = 11;

    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.grid.template.disabled = true;

    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.points = [{ x: -400, y: 0 }, { x: 0, y: 50 }, { x: 400, y: 0 }]
    dateAxis.renderer.polyspline.tensionX = 0.8;
    dateAxis.renderer.grid.template.disabled = true;
    dateAxis.renderer.line.strokeDasharray = "1,4";
    dateAxis.baseInterval = {period:"day", count:1}; // otherwise initial animation will be not smooth
    dateAxis.dateFormatter.dateFormat = "MMMM";

    dateAxis.renderer.labels.template.disabled = true;


    var series = chart.series.push(new am4plugins_timeline.CurveLineSeries());
    series.strokeOpacity = 0;
    
    
    series.dataFields.dateX = "year";
    series.dataFields.categoryY = "category";
    series.dataFields.value = "size";
    series.baseAxis = categoryAxis;

    var interfaceColors = new am4core.InterfaceColorSet();

    series.tooltip.pointerOrientation = "down";

    var distance = 100;
    var angle = 60;

    var bullet = series.bullets.push(new am4charts.Bullet());

    var circle = bullet.createChild(am4core.Circle);
    circle.radius = 30;
    circle.fillOpacity = 1;
    circle.strokeOpacity = 0;

    var circleHoverState = circle.states.create("hover");
    circleHoverState.properties.scale = 1.3;

    series.heatRules.push({ target: circle, min: 20, max: 50, property: "radius" });
    circle.adapter.add("fill", function(fill, target) {
      if (target.dataItem) {
        return chart.colors.getIndex(target.dataItem.index)
      }
    });
    circle.tooltipText = "{text}: {value}";
    circle.adapter.add("tooltipY", function(tooltipY, target){
      return -target.pixelRadius - 4;
    });

    var yearLabel = bullet.createChild(am4core.Label);
    yearLabel.text = "{year}";
    yearLabel.strokeOpacity = 0;
    yearLabel.fill = am4core.color("#fff");
    yearLabel.horizontalCenter = "middle";
    yearLabel.verticalCenter = "middle";
    yearLabel.interactionsEnabled = false;

    var outerCircle = bullet.createChild(am4core.Circle);
    outerCircle.radius = 30;
    outerCircle.fillOpacity = 0;
    outerCircle.strokeOpacity = 0;
    outerCircle.strokeDasharray = "1,3";

    var hoverState = outerCircle.states.create("hover");
    hoverState.properties.strokeOpacity = 0.8;
    hoverState.properties.scale = 1.5;

    outerCircle.events.on("over", function(event){
      var circle = event.target.parent.children.getIndex(1);
      circle.isHover = true;
      event.target.stroke = circle.fill;
      event.target.radius = circle.pixelRadius;
      event.target.animate({property: "rotation", from: 0, to: 360}, 4000, am4core.ease.sinInOut);
    });

    outerCircle.events.on("out", function(event){
      var circle = event.target.parent.children.getIndex(1);
      circle.isHover = false;
    });

    chart.scrollbarX = new am4core.Scrollbar();
    chart.scrollbarX.opacity = 0.5;
    chart.scrollbarX.width = am4core.percent(50);
    chart.scrollbarX.align = "center";

    let child = $('#' + $(link).attr('data-div-id')).attr('data-child-div');

    circle.events.on("hit", function(event)
    {
      if(!empty(child))
      {
        let month = event.target.dataItem.dataContext.year.getUTCMonth() + 1;
        createTimeLineStageThree($('#' + child), year, month);
        var element = document.getElementById(child);
        window.scrollTo(element.offsetLeft, element.offsetTop);
      }
    });
  }

  function createTimeLineStageThree(link, year, month)
  {
    let newsData = oadhTimeLineData[year][month];
    let newsObject = [];
    let hours = 0;
    let minutes = 0;
    let tempDate = 0;
    Object.values(newsData).forEach(function(value, index) {
      let parts = value.date.split('-');
      if(tempDate == 0)
      {
        tempDate = value.date;
      }

      if(tempDate == value.date)
      {
        hours+= 4;
      }
      else
      {
        hours = 0;
      }

      if(hours == 24)
      {
        hours = 0;

        if(minutes == 60)
        {
          minutes = 0;
        }
        else
        {
          minutes+= 15;
        }
      }

      newsObject = [...newsObject, {
        category: "",
        year: new Date(parts[0], (month - 1), parts[2], hours, minutes),
        realDate: new Date(parts[0], (month - 1), parts[2]),
        size: 15,
        text: "{!! Config::get('system-lang.' . $lang . '.resumen') !!}",
        link: value.link,
        summary: value.summary
      }]
      
      tempDate = value.date;
    });


    $(link).removeClass('d-none');
    var chart = am4core.create($(link).attr('data-div-id'), am4plugins_timeline.CurveChart);
    chart.curveContainer.padding(0, 100, 0, 120);
    chart.maskBullets = false;
    tempChart = chart;

    var colorSet = new am4core.ColorSet();

    chart.data = newsObject;
    chart.dateFormatter.inputDateFormat = "yyyy";
    chart.language.locale = am4lang_es_ES;

    chart.fontSize = 11;
    chart.tooltipContainer.fontSize = 11;

    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.grid.template.disabled = true;

    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.points = [{ x: -400, y: 0 }, { x: 0, y: 50 }, { x: 400, y: 0 }]
    dateAxis.renderer.polyspline.tensionX = 0.8;
    dateAxis.renderer.grid.template.disabled = true;
    dateAxis.renderer.line.strokeDasharray = "1,4";
    dateAxis.baseInterval = {period:"day", count:1}; // otherwise initial animation will be not smooth
    dateAxis.dateFormatter.dateFormat = "MMMM dd";

    dateAxis.renderer.labels.template.disabled = true;

    var series = chart.series.push(new am4plugins_timeline.CurveLineSeries());
    series.strokeOpacity = 0;
    
    
    series.dataFields.dateX = "year";
    series.dataFields.categoryY = "category";
    series.dataFields.value = "size";
    series.baseAxis = categoryAxis;

    var interfaceColors = new am4core.InterfaceColorSet();

    series.tooltip.pointerOrientation = "down";
    series.tooltip.label.wrap = true;
    series.tooltip.label.width = 300;


    var distance = 100;
    var angle = 60;

    var bullet = series.bullets.push(new am4charts.Bullet());

    var circle = bullet.createChild(am4core.Circle);
    circle.radius = 30;
    circle.fillOpacity = 1;
    circle.strokeOpacity = 0;

    var circleHoverState = circle.states.create("hover");
    circleHoverState.properties.scale = 1.3;

    series.heatRules.push({ target: circle, min: 20, max: 50, property: "radius" });
    circle.adapter.add("fill", function(fill, target) {
      if (target.dataItem) {
        return chart.colors.getIndex(target.dataItem.index)
      }
    });
    circle.tooltipText = "{text}: {realDate}, {summary}";
    circle.adapter.add("tooltipY", function(tooltipY, target){
      return -target.pixelRadius - 4;
    });

    var yearLabel = bullet.createChild(am4core.Label);
    yearLabel.text = "{realDate}";
    yearLabel.strokeOpacity = 0;
    yearLabel.fill = am4core.color("#fff");
    yearLabel.horizontalCenter = "middle";
    yearLabel.verticalCenter = "middle";
    yearLabel.interactionsEnabled = false;

    var outerCircle = bullet.createChild(am4core.Circle);
    outerCircle.radius = 30;
    outerCircle.fillOpacity = 0;
    outerCircle.strokeOpacity = 0;
    outerCircle.strokeDasharray = "1,3";

    var hoverState = outerCircle.states.create("hover");
    hoverState.properties.strokeOpacity = 0.8;
    hoverState.properties.scale = 1.5;

    outerCircle.events.on("over", function(event){
      var circle = event.target.parent.children.getIndex(1);
      circle.isHover = true;
      event.target.stroke = circle.fill;
      event.target.radius = circle.pixelRadius;
      event.target.animate({property: "rotation", from: 0, to: 360}, 4000, am4core.ease.sinInOut);
    });

    outerCircle.events.on("out", function(event){
      var circle = event.target.parent.children.getIndex(1);
      circle.isHover = false;
    });

    chart.scrollbarX = new am4core.Scrollbar();
    chart.scrollbarX.opacity = 0.5;
    chart.scrollbarX.width = am4core.percent(50);
    chart.scrollbarX.align = "center";

    circle.events.on("hit", function(event)
    {
      let data = event.target.dataItem.dataContext;
      window.open(data.link, '_blank');
    });
  }

  $(document).ready(function ()
  {

    @include('decima-oadh::front-end/base-document-ready-js')

    @if(Auth::check())
      $('.carousel').carousel({
        interval: false,
        keyboard: false
      });

      tinymce.init({
        selector: '.editable-element',
        toolbar: 'customSaveButton | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | table tabledelete | fontsizeselect | bullist numlist outdent indent',
        menubar: false,
        inline: true,
        setup: function (editor)
        {
          editor.ui.registry.addButton('customSaveButton',
          {
            icon: 'save',
            text: 'Guardar',
            onAction: function (_)
            {
              $.ajax (
              {
                type: 'POST',
                dataType : 'json',
                url: $('#app-url').val() + '/cms/setting/update-key-value',
                data: JSON.stringify({ '_token': $('#app-token').val(), lang: '{{ $lang }}', key: $(editor.targetElm).attr('data-key'), value: $(editor.targetElm).html()}),
                error: function (jqXHR, statusText, errorThrown)
                {
                  // console.log("Error: " + errorThrown)
                  // console.log("Status Text:" + statusText)
                  // console.log(jqXHR)
                }
              });

              editor.hide();
              editor.show();
            }
          });
        }
      });
    @endif

    if (window.location.pathname == '/cms/inicio')
    {
      @if(!empty($mapData))
        var mapData = {!! $mapData !!};
      @endif

      if(!empty(mapData))
      {
        createAmChartMaps(
          $('<a/>', {
            'data-div-id': 'homemap',
            'data-grid-id': 'homemapnogrid'
          }),
          mapData.data.data,
          true
        );
      }

      window.localStorage.clear();
    }

    if (window.location.pathname == '/cms/publicaciones')
    {
      $('.article').hide();

      $('#bulletins').addClass('active-tabs');

      $('#bulletins-tab').show();
      $('.bulletins-tab').show();
      $('.ezview').EZView();

      $('.btn-entries').on('click', function()
      {
        $('.btn-entries').removeClass('active-tabs');
        $(this).addClass('active-tabs');

        $('.article').hide();

        $('#' + $(this).attr('data-tab-id') + ', .' + $(this).attr('data-tab-id')).fadeIn(1000);
      });

      $('.btn-download').on('click', function()
      {
        if (!empty($(this).attr('data-publication-id')))
        {
          var publicationId = $(this).attr('data-publication-id');
          $.ajax(
          {
            type: 'POST',
            dataType: 'json',
            url: $('#app-url').val() + '/cms/publicaciones/create-download',
            data: JSON.stringify({'_token': $('#app-token').val(), publication_id: publicationId}),
            error: function (jqXHR, statusText, errorThrown)
            {
              // console.log("Error: " + errorThrown)
              // console.log("Status Text:" + statusText)
              // console.log(jqXHR)
            }
          });
        }
      });
    }

    if (window.location.pathname == '/cms/acerca-monitoreo-medios')
    {
      $('.oadh-li-video-tutorial').click(function()
      {
        $('.oadh-video-tab').hide();

        $('.oadh-li-video-tutorial').removeClass('nav-link-press01-active');
        
        $(this).addClass('nav-link-press01-active');

        $('.' + $(this).attr('data-tab-class')).fadeIn(1000);
      });
    }

    if (window.location.pathname == '/cms/monitoreo-de-medios')
    {
      $('.oadh-press-menu-link').click(function()
      {
        $('.oadh-press-menu-link').removeClass('active')
        
        $(this).addClass('active')
        
        $('#oadh-hr-name').html($(this).attr('data-hr-name'));
        $('#oadh-hr-description').html($(this).attr('data-hr-description'));
  
        window.scrollTo(0, 0);
        
        $('#oadh-press-description-cards').hide();
        
        $('#oadh-press-detail-cards').show();
        $('.oadh-press-details-link').show();

        if($(this).attr('data-type') == 'R')
        {
          $('.oadh-press-menu-hr').removeClass('oadh-press-human-right');
          $('.oadh-press-menu-hr').addClass('oadh-press-human-right');
          $(this).parent().removeClass('oadh-press-human-right');

          if($('#oadh-press-arrow-' + $(this).attr('data-id')).hasClass('fa-arrow-circle-down'))
          {
            $('.human-right-nav').hide();
          
            $('.human-right-arrow').removeClass('fa-arrow-circle-up');
            $('.human-right-arrow').removeClass('fa-arrow-circle-down');
            
            $('.human-right-arrow').addClass('fa-arrow-circle-down');

            $('#oadh-press-arrow-' + $(this).attr('data-id')).removeClass('fa-arrow-circle-down');
            
            $('#oadh-press-arrow-' + $(this).attr('data-id')).addClass('fa-arrow-circle-up');
            
            
            $('#oadh-press-human-right-nav-' + $(this).attr('data-id')).fadeIn(1000);
          }
          else
          {
            $('.human-right-nav').hide();
            $('#oadh-press-arrow-' + $(this).attr('data-id')).removeClass('fa-arrow-circle-up');
            $('#oadh-press-arrow-' + $(this).attr('data-id')).addClass('fa-arrow-circle-down');
          }

          if($(this).attr('data-id') == '2')
          {
            $('.oadh-press-life').show();
          }
          else
          {
            $('.oadh-press-life').hide();
          }

          $('#oadh-press-id').val($(this).attr('data-id'));
        }
        else
        {
          $('.oadh-press-menu-hr').removeClass('oadh-press-human-right');
          $('.oadh-press-menu-hr').addClass('oadh-press-human-right');
        }

        oadhPressDataType = $(this).attr('data-type');
        oadhPressDataId = $(this).attr('data-id');

        oadhPressSearch(false);
      });
      
      $('.oadh-press-details-link').click(function()
      {
        $('.oadh-press-modal-body').hide();

        oadhPressDataKey = $(this).attr('data-key');

        $('#oadh-press-key').val(oadhPressDataKey);

        oadhPressShowModal();
      });

      $('#oadh-press-download-button').click(function()
      {
        $('#oadh-press-download-form').submit();
      });
    }

    if (window.location.pathname == '/cms/videos')
    {
      $('#oadh-multi-search-text').onEnter(function()
      {
        if(empty($('#oadh-multi-search-text').val())) return;
        $('#oadh-multi-btn-search').click();
      })

      $('#oadh-multi-btn-search').click(function()
      {
        $.ajax(
        {
          type: 'POST',
          // dataType : 'json',
          url: $('#app-url').val() + '/cms/videos-card',
          data: JSON.stringify({
            '_token': $('#app-token').val(), 
            'filter': $('#oadh-multi-search-text').val(),
            'lang': '{{ $lang }}',
          }),
          error: function (jqXHR, statusText, errorThrown)
          {
            // console.log("Error: " + errorThrown);
            // console.log("Status Text:" + statusText);
            // console.log(jqXHR);
          },
          beforeSend: function()
          {
            $('#app-loader').css({ display: "inherit"});
          },
          success: function(html)
          {
            console.log(html);
            $('#oadh-multi-gallery').html(html); 
            $('#oadh-multi-gallery').highlight($('#oadh-multi-search-text').val());
            $('#app-loader').css({ display: "none"});
          }
        });
      });
    }

    if (window.location.pathname == '/cms/inicio')
    {
      // Set themes
      // am4core.useTheme(am4themes_animated)
      // am4core.useTheme(am4themes_amchartsdark)
      //
      // colorSet = new am4core.ColorSet()
      // setTimeout(init, 500)
      $('.btn-email').on('click', function()
      {
        if (!$('.input-email').isEmpty())
        {
          oudhSubscribe($('.input-email').val());
        }
        else
        {
          swalWithBootstrapButtons.fire(
          {
            title: "{!! Config::get('system-lang.' . $lang . '.datosincorrectos') !!}",
            text: "{!! Config::get('system-lang.' . $lang . '.correovacio') !!}",
            icon: 'warning',
            allowOutsideClick: false,
            timer:5000,
          });
        }
      });
    }

    $('.btn-email2').on('click', function()
    {
      if (!$('.input-email2').isEmpty())
      {
        oudhSubscribe($('.input-email2').val());
      }
      else
      {
        swalWithBootstrapButtons.fire(
        {
          title: "{!! Config::get('system-lang.' . $lang . '.datosincorrectos') !!}",
          text: "{!! Config::get('system-lang.' . $lang . '.correovacio') !!}",
          icon: 'warning',
          allowOutsideClick: false,
          timer:5000,
        });
      }
    });

    if (
      window.location.pathname == '/cms/derechos/vida' || 
      window.location.pathname == '/cms/derechos/libertad' || 
      window.location.pathname == '/cms/derechos/integridad' ||
      window.location.pathname == '/cms/derechos/acceso-a-la-justicia' ||
      window.location.pathname == '/cms/derechos/reparacion-integral-de-victimas'
    )
    {
      $('.card-filter').hide();

      $('.rights-description').show();

      $('.oadh-chart').hide();

      $('#btn-filter').click(function()
      {
        $('#nav-option-list .active').attr('data-apply-filter', '1');
        $('#nav-option-list .active').click();
      });

      $('#nav-option-list a').click(function()
      {
        if (!empty($(this).attr('data-toggle')))
        {
          return;
        }

        var context, url, link, tabNum, filters;

        if ($('#app-loader').is(':visible'))
        {
          return;
        }

        $('.rights-description').hide();

        $('.card-filter').show();

        $('.oadh-chart').hide();

        $('#' + $(this).attr('data-div-id')).parent().show();

        $("#nav-option-list a").removeClass('active');

        $(this).addClass('active');

        $('.mapdiv-filter').hide();

        $('#' + $(this).attr('data-div-filter-id')).show();

        window.scrollTo(0, 0);

        if($('#' + $(this).attr('data-div-filter-id')).children().length != 0 && empty($(this).attr('data-apply-filter')))
        {
          return;
        }

        // $('#' + $(this).attr('data-div-filter-id')).empty();

        context = $(this).attr('data-context');

        var rightTo = $('.main-title').text();

        var name = $('#' + $(this).attr('id')).text();

        $.ajax(
        {
          type: 'POST',
          dataType: 'json',
          url: $('#app-url').val() + '/cms/derechos/create-access',
          data: JSON.stringify({'_token': $('#app-token').val(), 'right_to': rightTo, 'name': name}),
        });

        if (window.location.pathname == '/cms/derechos/libertad')
        {
          url = $('#app-url').val() + '/cms/freedom/' + context
        }
        else if (window.location.pathname == '/cms/derechos/integridad')
        {
          url = $('#app-url').val() + '/cms/integrity/' + context
        }
        else if (window.location.pathname == '/cms/derechos/vida')
        {
          url = $('#app-url').val() + '/cms/life/' + context;
        }
        else if (window.location.pathname == '/cms/derechos/acceso-a-la-justicia')
        {
          url = $('#app-url').val() + '/cms/justice-access/' + context;
        }
        else if (window.location.pathname == '/cms/derechos/reparacion-integral-de-victimas')
        {
          url = $('#app-url').val() + '/cms/compensation-right/' + context;
        }

        link = this;
        tabNum = $(this).attr('data-tab-number');

        $.ajax(
        {
          type: 'POST',
          dataType : 'json',
          url: url,
          data: JSON.stringify({'_token': $('#app-token').val(), 'filter': getFilterData(link)}),
          error: function (jqXHR, statusText, errorThrown)
          {
            // console.log("Error: " + errorThrown);
            // console.log("Status Text:" + statusText);
            // console.log(jqXHR);
          },
          beforeSend: function()
          {
            $('#app-loader').css({ display: "inherit"});
          },
          success: function(json)
          {

            buildFilters(link, json.data);

            if (empty(json.data)) 
            {
              swalWithBootstrapButtons.fire(
              {
                title: "{!! Config::get('system-lang.' . $lang . '.sinregistrostitulo') !!}",
                text:	"{!! Config::get('system-lang.' . $lang . '.sinregistrosdesc') !!}",
                icon: 'info',
              });
            }

            if(!empty(tempChart))
            {
              tempChart.dispose();
            }

            $('#btn-filter, #btn-download-xlsx').removeClass('d-none');
            $('#oadh-filter-title').text("{!! Config::get('system-lang.' . $lang . '.disponibles') !!}");

            switch ($(link).attr('data-chart-type')) 
            {
              case 'map':
                createAmChartMaps(link, json.data.data);
                break;
              case 'pie':
                createAmChartPie(link, json.data.data, json.genderColor);
                break;
              case 'bar':
                chartOneColumn(link, json.data.data, json.data.labels);
                break;
              case 'dbar': // else if(context == 'jails-occupation')
                chartTwoColumnByYear(json.data.data, json.data.labels, tabNum, json.data.isGender);
                break;
              case 'chart2': // if(context == 'criminal-cases-trials')
                chart2(json.data.data, tabNum);
                break;
              case 'line': // if(context == 'criminal-cases-trials')
                line(link, json.data.data, tabNum, json.data.fields, json.data.names);
                break;
              case 'hbar': // if(context == 'criminal-cases-trials')
                chartOneColumn(link, json.data.data, json.data.labels, tabNum, true);
                break;
              case 'cbar': // if(context == 'criminal-cases-trials')
                chartClusteredColumn(link, json.data.data, json.data.year_chart);
                break;
              case 'timeline': // if(context == 'criminal-cases-trials')

                oadhTimeLineData = json.data;
                oadhTimeLineYearSize = json.sizes;
                createTimeLineStageOne(link);
                break;
            }

            if (empty($(link).attr('data-apply-filter')))
            {
              if (empty(filterId))
              {
                filterId = '#' + $(link).attr('data-div-filter-id');
              }
              else
              {
                $(filterId + ' div.form-group.row').remove();
                filterId = '#' + $(link).attr('data-div-filter-id');
              }
            }

            $(link).attr('data-apply-filter', '0');

            $('#app-loader').css({ display: "none"});

            let title = '';
            let parent = $(link).parent().parent().parent();
            // let title = $(link).text().trim();

            if(parent.hasClass('collapse'))
            {
              title = `${parent.prev().text()} ${$(link).text().trim()}`;
            }
            else
            {
              title = $(link).text().trim();
            }

            // let titles = ['Observatorio Universitario de Derechos Humanos de la UCA (OUDH)', $('.main-title').text(), title];
            let titles = ['OUDH', $('.main-title').text(), title];
            
            if(!empty(json.excel))
            {
              $('#oadh-xlsx-data').attr('data-details', JSON.stringify(json.excel));
              $('#oadh-xlsx-data-titles').attr('data-details-titles', JSON.stringify(titles));
            }
          }
        });
      });

      $('#nav-mobile-list a').on('click', function()
      {
        $('#' + $(this).attr('data-link-id')).click();
        API.close();
      });

      $("#left-rights-on-mobile-menu").mmenu(
      {
        //options
        wrappers: ['bootstrap4'],
        counters: true,
        keyboardNavigation:
        {
          enable: "default",
          enhance: true
        },
        navbars:
        [
          {
            position: "top",
            content: [
              "searchfield"
            ]
          }
        ],
        searchfield:
        {
          panel: true,
          showSubPanels: false
        },
        extensions:
        [
          "pagedim-black",
          "theme-white"
        ],
        setSelected:
        {
          hover: true,
          parent: true
        },
      },
      {
        //configuration
        searchfield:
        {
          clear: true
        },
        offCanvas:
        {
          pageSelector: "#page-wrapper"
        }
      });

      API = $('#left-rights-on-mobile-menu').data('mmenu');

      API.bind('open:finish', function()
      {
        $("input[placeholder='Search']").focus();
      });

      $('#btn-mmenu').click(function(event)
      {
          API.open();
          event.preventDefault();
          event.stopPropagation();
      });
    }

    $('.navbar-nav .oadh-nav-item .oadh-nav-link').each(function()
    {
      if ($(this).attr('href') == window.location.pathname)
      {
          // $(this).addClass('oadh-text-secondary-color');
          $(this).attr('style', 'color: rgb(255, 152, 21) !important;');
      }
      else if (
        window.location.pathname == '/cms/derechos/libertad' || 
        window.location.pathname == '/cms/derechos/vida' || 
        window.location.pathname == '/cms/derechos/integridad' ||
        window.location.pathname == '/cms/derechos/acceso-a-la-justicia' ||
        window.location.pathname == '/cms/derechos/reparacion-integral-de-victimas')
      {
        $('#rights').attr('style', 'color: rgb(255, 152, 21) !important;');
      }
    });

    $('body').on('slideStop', '.oadh-slider-limit', function(e)
    {
      let valueMin = Math.min.apply(null, e.value);
      let valueMax = Math.max.apply(null, e.value);
      let valueDif = valueMax - valueMin;

      if(Math.abs(valueDif) >= 2)
      {
        $(this).bootstrapSlider('setValue', [valueMax - 2, valueMax]);
      }
    });

    $('#btn-download-xlsx').click(function()
    {
      let data = $('#oadh-xlsx-data').attr('data-details');
      let titles = $('#oadh-xlsx-data-titles').attr('data-details-titles');

      if(empty(JSON.parse(data)))
      {
        swalWithBootstrapButtons.fire(
          {
            title: '{{ Config::get('system-lang.' . $lang . '.nodatosdescargar') }}',
            icon: 'info',
            timerProgressBar: true,
            timer: 2000,
            confirmButtonText: 'Regresar'
          }
        )

        return false;
      }

      $('#oadh-xlsx-data').val(data);
      $('#oadh-xlsx-data-titles').val(titles);
      $('#oadh-xlsx-download-form').submit();
    });
  });
</script>
