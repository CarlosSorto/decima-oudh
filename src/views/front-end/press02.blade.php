@extends('decima-oadh::front-end/base')

@section('javascript')
    <script type="text/javascript">
      
    </script>
@endsection


@section('container')

<div class="container my-4">
  <div class="row">
    <div class="col-md-4">
      <div class="sidebar-sticky">
        @foreach ($humanRights as $humanRight)
          <h6 class="nav sidebar-heading d-flex justify-content-between align-items-center mb-2 oadh-press-menu-hr oadh-press-human-right oadh-text-primary-color">
            <a class="oadh-press-menu-link nav-link-press-font-size nav-link-right px-2 oadh-fake-link oadh-text-primary-color" data-type='R' data-id='{{ $humanRight['value'] }}' data-hr-name='{{ $humanRight[$lang . '_hr_name'] }}' data-hr-description='{{ $humanRight[$lang . '_hr_description'] }}'>
              {{ $humanRight[$lang . '_hr_name'] }} <i id="oadh-press-arrow-{{ $humanRight['value'] }}" class="fas fa-arrow-circle-down human-right-arrow"></i>
              {{-- fa-arrow-circle-down --}}
              {{-- fa-arrow-circle-up --}}
            </a>
          </h6>
          <ul id="oadh-press-human-right-nav-{{ $humanRight['value'] }}" class="nav flex-column mb-2 human-right-nav press-option-list" style="display: none;">
          @if (!empty($humanRight['topics']))
            @foreach ($humanRight['topics'] as $item)
              <li class="nav-item">
                <a class="oadh-press-menu-link nav-link-press nav-link-press-font-size oadh-text-primary-color oadh-fake-link" data-type='{{ $item['type'] }}' data-id='{{ $item['value'] }}' data-hr-name='{{ $item[$lang . '_hr_name'] }}' data-hr-description='{{ $item[$lang . '_hr_description'] }}'>
                  <i class="fas fa-chart-line"></i> {{ $item[$lang . '_name'] }}
                </a>
              </li>
            @endforeach
          @endif
          {{-- @if (!empty($humanRight['violated_fact']))
            @foreach ($humanRight['violated_fact'] as $item)
              <li class="nav-item">
                <a class="oadh-press-menu-link nav-link-press nav-link-press-font-size oadh-text-primary-color oadh-fake-link" data-type='v'  data-id='{{ $item['value'] }}' data-hr-name='{{ $item[$lang . '_hr_name'] }}' data-hr-description='{{ $item[$lang . '_hr_description'] }}'>
                  <i class="fas fa-chart-line"></i> {{ $item[$lang . '_name'] }}
                </a>
              </li>
            @endforeach
          @endif --}}
          </ul>
        @endforeach
        
        {{-- <h6 class="nav sidebar-heading d-flex justify-content-between align-items-center px-3 mb-1 oadh-text-primary-color">
          <a class="nav-link-right oadh-fake-link oadh-text-primary-color">
            Derecho Humano 1
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link-press oadh-text-primary-color oadh-fake-link">
              <i class="fas fa-chart-line"></i> Tema 1.1
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link-press oadh-text-primary-color" href="#">
              <i class="fas fa-chart-line"></i> Tema 1.2
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link-press oadh-text-primary-color" href="#">
              <i class="fas fa-chart-line"></i> Tema 1.3
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link-press oadh-text-primary-color" href="#">
              <i class="fas fa-chart-line"></i> Tema 1.4
            </a>
          </li>
        </ul> --}}
      </div>


    </div>
    <div class="col-md-8">
      <h3 id="oadh-hr-name" class="oadh-text-primary-color text-center editable-element" data-key="N014">{!! $keyValues['N014'] !!}</h3>
      <p id="oadh-hr-description" class="text-justify editable-element" data-key="N015">
        {!! $keyValues['N015'] !!}
      </p>

      <div id="oadh-press-description-cards" class="row">
        
        <div class="col-md-12">
            <div class="card mb-4 oadh-bg-secondary-color">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-lg-10 col-md-8 col-7">
                          <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.noticiascantidad') }}</h5>
                          {{-- <h3 class="my-2 py-1 font-weight-bold oadh-text-white-color">9,184</h3> --}}
                          <p class="text-justify editable-element oadh-text-white-color" data-key="N016">
                            {!! $keyValues['N016'] !!}
                          </p>
                          <a class="oadh-fake-link oadh-press-details-link oadh-text-primary-color" data-key="N" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-5">
                          {{-- <img src="{{ URL::asset('oadh/logos/logo-uca-azul-v2.png') }}" alt="" class="img-fluid"> --}}
                          <img src="{{ URL::asset('oadh/prensa/noticias-v3.png') }}" alt="" class="img-fluid">
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div> <!-- end col -->
    
        <div class="col-md-12">
            <div class="card mb-4 oadh-bg-primary-color">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-lg-10 col-md-8 col-7">
                          <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.fuentes') }}</h5>
                          <p class="text-justify editable-element oadh-text-white-color" data-key="N017">
                            {!! $keyValues['N017'] !!}
                          </p>
                          <a class="oadh-fake-link oadh-press-details-link oadh-text-secondary-color" data-key="S" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-5">
                          <img src="{{ URL::asset('oadh/prensa/fuente-v2.png') }}" alt="" class="img-fluid">
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div> <!-- end col -->
    
        <div class="col-md-12">
            <div class="card mb-4 oadh-bg-secondary-color">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-lg-10 col-md-8 col-7">
                          <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.victimas') }}</h5>
                          <p class="text-justify editable-element oadh-text-white-color" data-key="N018">
                            {!! $keyValues['N018'] !!}
                          </p>
                          <a class="oadh-fake-link oadh-press-details-link oadh-text-primary-color" data-key="V" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-5">
                          <img src="{{ URL::asset('oadh/prensa/victima-v2.png') }}" alt="" class="img-fluid">
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div> <!-- end col -->

        <div class="col-md-12">
          <div class="card mb-4 oadh-bg-primary-color">
              <div class="card-body">
                  <div class="row align-items-center">
                      <div class="col-lg-10 col-md-8 col-7">
                        <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.presuntovictimario') }}</h5>
                        <p class="text-justify editable-element oadh-text-white-color" data-key="N019">
                          {!! $keyValues['N019'] !!}
                        </p>
                        <a class="oadh-fake-link oadh-press-details-link oadh-text-secondary-color" data-key="C" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                      </div>
                      <div class="col-lg-2 col-md-4 col-5">
                        <img src="{{ URL::asset('oadh/prensa/victimario-v2.png') }}" alt="" class="img-fluid">
                      </div>
                  </div> <!-- end row-->
              </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

        <div class="col-md-12">
          <div class="card mb-4 oadh-bg-secondary-color">
              <div class="card-body">
                  <div class="row align-items-center">
                      <div class="col-lg-10 col-md-8 col-7">
                        <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.poblacion') }}</h5>
                        <p class="text-justify editable-element oadh-text-white-color" data-key="N020">
                          {!! $keyValues['N020'] !!}
                        </p>
                        <a class="oadh-fake-link oadh-press-details-link oadh-text-primary-color" data-key="P" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                      </div>
                      <div class="col-lg-2 col-md-4 col-5">
                        <img src="{{ URL::asset('oadh/prensa/poblacion-vulnerada-v2.png') }}" alt="" class="img-fluid">
                      </div>
                  </div> <!-- end row-->
              </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

        <div class="col-md-12">
          <div class="card mb-4 oadh-bg-primary-color">
              <div class="card-body">
                  <div class="row align-items-center">
                      <div class="col-lg-10 col-md-8 col-7">
                        <h5 class="fw-normal mt-0 oadh-text-white-color" title="Deals">{{ Config::get('system-lang.' . $lang . '.departamentos') }}</h5>
                        <p class="text-justify editable-element oadh-text-white-color" data-key="N021">
                          {!! $keyValues['N021'] !!}
                        </p>
                        <a class="oadh-fake-link oadh-press-details-link oadh-text-secondary-color" data-key="D" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                      </div>
                      <div class="col-lg-2 col-md-4 col-5">
                        <img src="{{ URL::asset('oadh/prensa/departamentos-v2.png') }}" alt="" class="img-fluid">
                      </div>
                  </div> <!-- end row-->
              </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

        <div class="col-md-12 oadh-press-life">
          <div class="card mb-4 oadh-bg-secondary-color">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-lg-10 col-md-8 col-7">
                  <h5 class="fw-normal mt-0 oadh-text-white-color" title="Deals">{{ Config::get('system-lang.' . $lang . '.tipoarmas') }}</h5>
                  <p class="text-justify editable-element oadh-text-white-color" data-key="N022">
                    {!! $keyValues['N022'] !!}
                  </p>
                  <a class="oadh-fake-link oadh-press-details-link oadh-text-primary-color" data-key="W" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                </div>
                <div class="col-lg-2 col-md-4 col-5">
                  <img src="{{ URL::asset('oadh/prensa/tipo-de-arma-v2.png') }}" alt="" class="img-fluid">
                </div>
              </div> <!-- end row-->
            </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

      </div>

      <div id="oadh-press-detail-cards" class="row" style="display: none;">
        
        <div class="col-sm-6">
          <div class="card mb-4 oadh-bg-secondary-color">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="offset-sm-3 col-sm-6 offset-2 col-8">
                  <img src="{{ URL::asset('oadh/prensa/noticias-v3.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 text-center">
                  <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.noticiascantidad') }}</h5>
                  <a class="oadh-fake-link oadh-press-details-link oadh-text-primary-color" data-key="N" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                </div>
              </div> <!-- end row-->
            </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->
    
        <div class="col-sm-6">
          <div class="card mb-4 oadh-bg-primary-color">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="offset-sm-3 col-sm-6 offset-2 col-8">
                  <img src="{{ URL::asset('oadh/prensa/fuente-v2.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 text-center">
                  <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.fuentes') }}</h5>
                  <a class="oadh-fake-link oadh-press-details-link oadh-text-secondary-color" data-key="S" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                </div>
              </div> <!-- end row-->
            </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->
    
        <div class="col-md-6">
          <div class="card mb-4 oadh-bg-secondary-color">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="offset-sm-3 col-sm-6 offset-2 col-8">
                  <img src="{{ URL::asset('oadh/prensa/victima-v2.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 text-center">
                  <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.victimas') }}</h5>
                  <a class="oadh-fake-link oadh-press-details-link oadh-text-primary-color" data-key="V" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                </div>
              </div> <!-- end row-->
            </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

        <div class="col-md-6">
          <div class="card mb-4 oadh-bg-primary-color">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="offset-sm-3 col-sm-6 offset-2 col-8">
                  <img src="{{ URL::asset('oadh/prensa/victimario-v2.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 text-center">
                  <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.presuntovictimario') }}</h5>
                  <a class="oadh-fake-link oadh-press-details-link oadh-text-secondary-color" data-key="C" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                </div>
              </div> <!-- end row-->
            </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

        <div class="col-md-6">
          <div class="card mb-4 oadh-bg-secondary-color">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="offset-sm-3 col-sm-6 offset-2 col-8">
                  <img src="{{ URL::asset('oadh/prensa/poblacion-vulnerada-v2.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 text-center">
                  <h5 class="fw-normal mt-0 oadh-text-white-color">{{ Config::get('system-lang.' . $lang . '.poblacion') }}</h5>
                  <a class="oadh-fake-link oadh-press-details-link oadh-text-primary-color" data-key="P" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                </div>
              </div> <!-- end row-->
            </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

        <div class="col-md-6">
          <div class="card mb-4 oadh-bg-primary-color">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="offset-sm-3 col-sm-6 offset-2 col-8">
                  <img src="{{ URL::asset('oadh/prensa/departamentos-v2.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 text-center">
                  <h5 class="fw-normal mt-0 oadh-text-white-color" title="Deals">{{ Config::get('system-lang.' . $lang . '.departamentos') }}</h5>
                  <a class="oadh-fake-link oadh-press-details-link oadh-text-secondary-color" data-key="D" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                </div>
              </div> <!-- end row-->
            </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

        <div class="col-md-6 oadh-press-life">
          <div class="card mb-4 oadh-bg-secondary-color">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="offset-sm-3 col-sm-6 offset-2 col-8">
                  <img src="{{ URL::asset('oadh/prensa/tipo-de-arma-v2.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 text-center">
                  <h5 class="fw-normal mt-0 oadh-text-white-color" title="Deals">{{ Config::get('system-lang.' . $lang . '.tipoarmas') }}</h5>
                  <a class="oadh-fake-link oadh-press-details-link oadh-text-primary-color" data-key="W" style="display: none;">{{ Config::get('system-lang.' . $lang . '.verdetalles') }}</a>
                </div>
              </div> <!-- end row-->
            </div> <!-- end card-body -->
          </div> <!-- end card -->
        </div> <!-- end col -->

      </div>

    </div>
  
  </div>

</div>

<div id="press02-modal" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 id="press02-modal-title" class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
          {{-- <label for="validationCustom01">{{ Config::get('system-lang.' . $lang . '.rangofecha') }}</label> --}}
          <div class="input-group">
            <div class="input-group-prepend d-none d-sm-block">
              <p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.de') }}</p>
            </div>
            <input type="date" class="oadh-outline-none form-control" id="oadh-press-news-daterange-from" name="oadh-press-news-daterange-from">
            <div class="input-group-prepend input-group-append d-none d-sm-block">
              <p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.al') }}</p>
            </div>
            <input type="date" class="oadh-outline-none form-control" id="oadh-press-news-daterange-to" name="oadh-press-news-daterange-to">
            <div class="input-group-append">
              <button type="button" class="btn oadh-press-news-search-control oadh-bg-primary-color oadh-outline-none oadh-text-white-color" id="oadh-press-news-btn-search" type="button" onclick="oadhPressSearch(true);">{{ Config::get('system-lang.' . $lang . '.buscar') }}</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body oadh-press-victim-note" style="margin-bottom: 0;">
        <p>{{ Config::get('system-lang.' . $lang . '.notaVictima') }}</p>
      </div>
      <div id="press02-modal-body-01" class="modal-body oadh-press-modal-body">
        <div class="smt-body">
          <p>Modal body text goes here.</p>
        </div>
      </div>
      <div id="press02-modal-body-02" class="modal-body oadh-press-modal-body">
        <div class="row">
          <div id="press02-modal-body-02-01" class="col-12 col-md-6">
            <div class="smt-body">
              <p>Modal body text goes here.</p>
            </div>
          </div>
          {{-- <div id="press02-modal-body-02-02" class="col-12 col-md-4">
            <div class="smt-body">
              <p>Modal body text goes here.</p>
            </div>
          </div> --}}
          <div id="press02-modal-body-02-03" class="col-12 col-md-6">
            <div class="smt-body">
              <p>Modal body text goes here.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button id="oadh-press-download-button" type="button" class="btn btn-primary">{{ Config::get('system-lang.' . $lang . '.descargar') }}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ Config::get('system-lang.' . $lang . '.cerrar') }}</button>
      </div>
    </div>
  </div>
</div>

<form action="{{url('/cms/medios-de-prensa/download-press-statistic')}}" method="POST" id="oadh-press-download-form" class="d-none">
  <input type="hidden" name="id" id="oadh-press-id">
  <input type="hidden" name="key" id="oadh-press-key">
  <input type="hidden" name="label" id="oadh-press-label">
  <input type="hidden" name="data" id="oadh-press-data">
  <input type="hidden" name="lang" id="oadh-news-export-data-pdf-lang" value="{{ $lang }}">
</form>

@parent
@stop