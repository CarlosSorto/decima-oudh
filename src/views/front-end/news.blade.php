@extends('decima-oadh::front-end/base')
@section('container')

<style>
  .clickable {
    cursor: pointer;
  }

  .oadh-bottom-border {
    border-bottom: 1px solid;
    border-color: rgb(255, 152, 21) !important;
  }

  #oadh-news-search-group>.oadh-outline-none:focus {
    outline: none;
    box-shadow: none;
  }

  input[type="text"].oadh-outline-none:focus {
    border-color: rgb(0, 22, 70);
  }

  #oadh-news-search-group .oadh-news-search-control.oadh-bg-primary-color:hover {
    background-color: rgba(0, 22, 70, 0.8) !important;
  }

  #oadh-news-search-group .oadh-news-search-control.oadh-bg-secondary-color:hover {
    background-color: rgba(255, 152, 21, 0.8) !important;
  }

  .oadh-dropdown-toggle {
    white-space: nowrap;
  }

  .oadh-dropdown-toggle:active {
    color: rgb(255, 152, 21) !important;
  }

  .oadh-dropdown-toggle::after {
    display: inline-block;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid;
    border-right: .3em solid transparent;
    border-bottom: 0;
    border-left: .3em solid transparent;
  }

  .oadh-input-group-text {
    background-color: rgb(0, 22, 70) !important;
    color: white !important;
  }

  p.input-group-text {
    background-color: rgb(0, 22, 70) !important;
    color: white !important;
  }

  .oadh-input-group-text-border-right {
    border-right: 0.125rem solid white !important;
  }

  .oadh-border-card {
    border-color: rgb(0, 22, 70);
  }

  a.oadh-text-primary-color:hover {
    color: rgba(0, 22, 70, 0.7) !important;
  }

  .pagination>.page-item>a.page-link {
    color: rgba(0, 22, 70, 0.7) !important;
  }

  .badge-primary {
    background-color: rgb(0, 22, 70);
  }

  .bootstrap-select>button {
    color: #495057 !important;
    background-color: #fff !important;
    border: 1px solid #ced4da !important;
    border-radius: .25rem !important;
  }

  .bootstrap-select>button:hover {
    color: #495057 !important;
    background-color: #fff !important;
  }

  .actions-btn:hover {
    color: white !important;
    background-color: rgb(0, 22, 70) !important;
  }

  .pagination>.page-item.active>a.page-link {
    color: white !important;
    background-color: rgb(0, 22, 70) !important;
  }

  .bootstrap-select .dropdown-menu {
    width: 100%;
  }

  .bootstrap-select .dropdown-menu li a {
    white-space: normal;
  }

  .bootstrap-select .dropdown-menu li a:focus {
    color: white !important;
    background-color: rgb(0, 22, 70) !important;
  }

  .dropdown-item.active,
  .dropdown-item:active {
    color: #fff;
    text-decoration: none;
    background-color: rgb(0, 22, 70) !important;
  }

  .clickable a {
    text-decoration: none;
    color: rgb(0, 22, 70);
  }

  .clickable a:hover {
    text-decoration: none;
    color: rgba(0, 22, 70, 0.9);
  }
</style>

@section('javascript')
    <script type="text/javascript">
      var organizationDepartments = {!! json_encode($departments) !!};
      var organizationRights = {!! json_encode($rights) !!};
      function getNewsSearchFormData()
      {
        let filters = []

        if (!$('#oadh-news-search-text').isEmpty()) 
        {
          filters = [...filters, {'field': '', 'op': 'like', 'data': $('#oadh-news-search-text').val(), 'id': '#oadh-news-search-text'}]
        }

        if (!$('#oadh-news-media').isEmpty()) 
        {
          filters = [...filters, {'field': '', 'op': 'media_in', 'data': $('#oadh-news-media').val(), 'id': '#oadh-news-media'}]
        }

        if (!$('#oadh-news-date').isEmpty()) 
        {
          filters = [...filters, {'field': 'n.date', 'op': 'eq', 'data': $('#oadh-news-date').val(), 'id': '#oadh-news-date'}]
        }

        if (!$('#oadh-news-daterange-from').isEmpty() && !$('#oadh-news-daterange-to').isEmpty()) 
        {
          filters = [...filters, {'field': 'n.date', 'op': 'ge', 'data': $('#oadh-news-daterange-from').val(), 'id': '#oadh-news-daterange-from'}]
          filters = [...filters, {'field': 'n.date', 'op': 'le', 'data': $('#oadh-news-daterange-to').val(), 'id': '#oadh-news-daterange-to'}]
        }

        if (!$('#oadh-news-population-affected').isEmpty()) 
        {
          filters = [...filters, {'field': 'population_affected', 'op': 'population_affected_in', 'data': $('#oadh-news-population-affected').val(), 'id': '#oadh-news-population-affected'}]
        }

        if (!$('#oadh-news-department').isEmpty()) 
        {
          filters = [...filters, {'field': 'department', 'op': 'department_in', 'data': $('#oadh-news-department').val(), 'id': '#oadh-news-department'}]
        }

        if (!$('#oadh-news-municipality').isEmpty()) 
        {
          filters = [...filters, {'field': 'municipality', 'op': 'municipality_in', 'data': $('#oadh-news-municipality').val(), 'id': '#oadh-news-municipality'}]
        }

        if (!$('#oadh-news-right').isEmpty()) 
        {
          filters = [...filters, {'field': 'right', 'op': 'right_in', 'data': $('#oadh-news-right').val(), 'id': '#oadh-news-right'}]
        }

        if (!$('#oadh-news-source').isEmpty()) 
        {
          filters = [...filters, {'field': 'source', 'op': 'source_in', 'data': $('#oadh-news-source').val(), 'id': '#oadh-news-source'}]
        }

        if (!$('#oadh-news-topic').isEmpty()) 
        {
          filters = [...filters, {'field': 'topic_id', 'op': 'topic_in', 'data': $('#oadh-news-topic').val(), 'id': '#oadh-news-topic'}]
        }

        if (!$('#oadh-news-violated-fact').isEmpty()) 
        {
          filters = [...filters, {'field': 'violated_fact', 'op': 'violated_in', 'data': $('#oadh-news-violated-fact').val(), 'id': '#oadh-news-violated-fact'}]
        }

        if (!$('#oadh-news-victim-gender').isEmpty()) 
        {
          filters = [...filters, {'field': 'vic_gender', 'op': 'victim_gender_in', 'type': 'V', 'data': $('#oadh-news-victim-gender').val(), 'id': '#oadh-news-victim-gender'}]
        }

        if(!empty($('#oadh-news-victim-age-from').val()) || !empty($('#oadh-news-victim-age-to').val()))
        {
          filters = [...filters, {'field': 'vic_age', 'op': 'victim_age_in', 'type': 'V', 'data': null, 'data_from': $('#oadh-news-victim-age-from').val(), 'data_to': $('#oadh-news-victim-age-to').val(), 'id_from': '#oadh-news-victim-age-from', 'id_to': '#oadh-news-victim-age-to'}]
        }

        if (!$('#oadh-news-victimizer-gender').isEmpty()) 
        {
          filters = [...filters, {'field': 'victimizer_gender', 'op': 'victim_gender_in', 'type': 'R', 'data': $('#oadh-news-victimizer-gender').val(), 'id': '#oadh-news-victimizer-gender'}]
        }

        if (!empty($('#oadh-news-victimizer-age-from').val()) || !empty($('#oadh-news-victimizer-age-to').val()))
        {
          filters = [...filters, {'field': 'vic_age', 'op': 'victim_age_in', 'type': 'R', 'data': null, 'data_from': $('#oadh-news-victimizer-age-from').val(), 'data_to': $('#oadh-news-victimizer-age-to').val(), 'id_from': '#oadh-news-victimizer-age-from', 'id_to': '#oadh-news-victimizer-age-to'}]
        }

        if (!$('#oadh-news-weapon-type').isEmpty()) 
        {
          filters = [...filters, {'field': 'weapon', 'op': 'weapon_type_in', 'data': $('#oadh-news-weapon-type').val(), 'id': '#oadh-news-weapon-type'}]
        }

        if(empty(filters))
        {
          $('#oadh-news-download').attr('disabled', true);


          return JSON.stringify({ '_token': $('#app-token').val(), 'lang': '{{ $lang }}'});
        }
        
        $('#oadh-news-download').attr('disabled', false);

        return JSON.stringify({ '_token': $('#app-token').val(), 'lang': '{{ $lang }}', filters});
        // return data;
      }

      function getNewsFilteredData()
      {
        data = localStorage.getItem('dataNewsFilters')
        url =  localStorage.getItem('dataNewsUrl')

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: data,
            beforeSend: function()
            {
              $('#app-loader').css({ display: "inherit"});
              $('#main-panel-fieldset').attr('disabled', true);
            }, 
          })
        .done(function (data)
        {
          $('#app-loader').css({ display: "none"});
          $('#main-panel-fieldset').attr('disabled', false);
          $('#oadh-news-result-title').hasClass('d-none') && $('#oadh-news-result-title').removeClass('d-none')
          $('#oadh-news-news').html(data.html);
          $('#oadh-news-news').highlight($('#oadh-news-search-text').val());
        })
        .always(function()
        {
          let dataFilters = !empty(localStorage.getItem('dataNewsFilters')) ? JSON.parse(localStorage.getItem('dataNewsFilters')) : null;
          let {filters} = dataFilters;

          if(!$('#oadh-news-collapse-filter-form').hasClass('show'))
          {
            $('#oadh-news-btn-collapse-filter').click();
          }

          if (!empty(filters)) 
          {
            filters.forEach(element => {
              if(!empty(element.data))
              {
                $(element.id).val(element.data)
                if(element.id == '#oadh-news-department')
                {
                  $('#oadh-news-department').trigger('change');
                }

                if(element.id == '#oadh-news-right')
                {
                  $('#oadh-news-right').trigger('change');
                }
              }

              if(element.field == 'vic_age')
              {
                $(element.id_from).val(element.data_from);
                $(element.id_to).val(element.data_to);
              }
            });
          }

          $('.selectpicker').selectpicker('refresh');
        });
      }

      function loadNewsLastPage()
      {
        let data = !empty(localStorage.getItem('dataNewsFilters')) ? JSON.parse(localStorage.getItem('dataNewsFilters')) : null;
        let pagePos = !empty(localStorage.getItem('dataNewsPagePos')) ? JSON.parse(localStorage.getItem('dataNewsPagePos')) : null;
        if(!empty(data))
        {
          getNewsFilteredData(true)
        }
        else
        {
          $('#oadh-news-download').attr('disabled', true);
        }

        if(!empty(pagePos) && pagePos > 1)
        {
          getNewsFilteredData()
        }
      }

      $(document).ready(function()
      {
        (function($) {
          $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
              if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
              } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
              } else {
                this.value = "";
              }
            });
          };
        }(jQuery));

        $('body').on('click', '#oadh-news-btn-search', function() 
        {
          let data = getNewsSearchFormData();
          localStorage.setItem('dataNewsUrl', $('#oadh-news-search-form').attr('data-action'))
          localStorage.setItem('dataNewsFilters', data);
          localStorage.setItem('dataNewsPagePos', null);
          getNewsFilteredData()
        });

        $('.oadh-news-btn-change-date-datetime').click(function() 
        {
          $('.oadh-news-btn-change-date-datetime').each(function(index, element)
          {
            if($(`#${$(element).attr('data-visibility')}`).hasClass('d-none'))
            {
              $(`#${$(element).attr('data-visibility')}`).removeClass('d-none') 
            }
            else
            {
              $(`#${$(element).attr('data-visibility')}`).addClass('d-none') 
              $(`#${$(element).attr('data-visibility')}`).find('input').val('')
            }
          });
        });

        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });

        $('body').on('click', '#oadh-news-btn-clear-filter', function() 
        {
          $('.bs-deselect-all').click();
          if($('#oadh-news-collapse-filter-form').hasClass('show'))
          {
            $('#oadh-news-btn-collapse-filter').click();
          }

          $('#oadh-news-collapse-filter-form').find('input').val('');
          $('#oadh-news-victim-gender, #oadh-news-victim-age, #oadh-news-victimizer-gender, #oadh-news-victimizer-age').val("");
          $('#oadh-news-victim-gender, #oadh-news-victim-age, #oadh-news-victimizer-gender, #oadh-news-victimizer-age').selectpicker("refresh");
          $('#oadh-news-search-text').val('');

          localStorage.removeItem('dataNewsUrl');
          localStorage.removeItem('dataNewsFilters');
          localStorage.removeItem('dataNewsPagePos');
          $('#oadh-news-download').attr('disabled', true);
        });

        $('#oadh-news-victim-age-from, #oadh-news-victim-age-to').inputFilter(function(value)
        {
          return /^\d*$/.test(value);
        });

        // $('#oadh-news-btn-collapse-filter').click(function() {
        //   if($('#oadh-news-collapse-filter-form').hasClass('collapse show'))
        //   {
        //     $('.bs-deselect-all').click();
        //     $('#oadh-news-collapse-filter-form').find('input').val('');
        //     $('#oadh-news-victim-gender, #oadh-news-victim-age, #oadh-news-victimizer-gender, #oadh-news-victimizer-age').val("");
        //     $('#oadh-news-victim-gender, #oadh-news-victim-age, #oadh-news-victimizer-gender, #oadh-news-victimizer-age').selectpicker("refresh");
        //   }
        // })

        $('body').on('click', '.oadh-news-single-download', function()
        {
          let mediaId = $(this).attr('data-id')
          if (empty(mediaId)) 
          {
            return;
          }

          $('#oadh-news-export-data-pdf').val((!empty(mediaId)) ? JSON.stringify(mediaId) : JSON.stringify(''));

          $('#oadh-news-export-form-pdf').submit();
        });

        $('#oadh-news-download').click(function()
        {
          let filters = localStorage.getItem('dataNewsFilters');
          
          let data = getNewsSearchFormData();

          $('#oadh-news-export-data').val((!empty(data)) ? data : JSON.stringify([]));

          $('#oadh-news-export-form').submit();
        });

        $('body').on('click', '.pagination a', function(e)
        {
          e.preventDefault();

          
          var page = $(this).attr('href').slice($(this).attr('href').indexOf('?'));
          var pagePos = page.slice(page.indexOf('=') + 1);
          var url = `${$('#app-url').val()}/cms/medios-de-prensa/advanced-smt-rows${page}`;

          localStorage.setItem('dataNewsUrl', url);
          localStorage.setItem('dataNewsPagePos', pagePos);

          getNewsFilteredData();
        });

      $('#oadh-news-search-text').onEnter(function()
      {
        if(empty($('#oadh-news-search-text').val())) return;
        $('#oadh-news-btn-search').click();
      });

      $('#oadh-news-department').change(function()
      {
        let departments = $(this).val();
        let municipalities = tempMun = [];

        if(departments.includes('Todos los departamentos') && departments.length > 1)
        {
          let newDepartments = departments.filter(val => val != 'Todos los departamentos');

          $(this).val(newDepartments);

          $('#oadh-news-department').selectpicker('refresh');
        }

        tempMun = organizationDepartments.filter((el) => {
          return departments.some(val => {
            return val == el.label;
          });
        });

        $('#oadh-news-municipality').html('')

        tempMun.forEach(element => {
          element.child.forEach(municipality => {
            municipalities = [...municipalities, {label: municipality.label, value: municipality.value}]
          });
        });

        municipalities.forEach(municipality => {
          $('#oadh-news-municipality').append(`<option value=${municipality.value}>${municipality.label}</option>`)
        });

        $('#oadh-news-municipality').selectpicker('refresh');
      });

      $('#oadh-news-right').change(function()
      {
        let rightId = $(this).val();
        let topicsArr = factsArr = [];

        rightId.forEach(right => {
          let {topics, violated_fact} = Object.values(organizationRights).find(val => val.value == right);
          topicsArr = [...topicsArr, topics];
          factsArr = [...factsArr, violated_fact];
        });

        let mergedTopics = [].concat.apply([], topicsArr);
        let mergedFacts = [].concat.apply([], factsArr);

        $('#oadh-news-topic').html('');
        $('#oadh-news-violated-fact').html('');

        mergedTopics.forEach(topic => {
          $('#oadh-news-topic').append(`<option value=${topic.value}>${topic.label}</option>`)
        });

        mergedFacts.forEach(fact => {
          $('#oadh-news-violated-fact').append(`<option value=${fact.value}>${fact.label}</option>`)
        });

        $('#oadh-news-topic, #oadh-news-violated-fact').selectpicker('refresh');
      });

      $('.selectpicker').selectpicker({
        deselectAllText: 'Ninguno',
        selectAllText: 'Todos',
      });

        setTimeout(() => {
          loadNewsLastPage()
        }, 500);
      });
    </script>
@endsection

<div class="container my-4">
  <h3 class="oadh-text-primary-color text-center editable-element" data-key="N001">{!! $keyValues['N001'] !!}</h3>
  <p class="text-justify editable-element" data-key="N002">
    {!! $keyValues['N002'] !!}
  </p>
  <div id="oadh-news-search-form-section">
    @include('decima-oadh::front-end/news-search-form')
  </div>
  <div class="row">
    <div class="col-sm-12 mt-5">
      <h3 class="d-none" id="oadh-news-result-title">{{ Config::get('system-lang.' . $lang . '.archivosrel') }}</h3>
    </div>
  </div>
  <div id="oadh-news-news" class="row">
    @include('decima-oadh::front-end/news-cards')
  </div>
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn oadh-bg-secondary-color oadh-outline-none oadh-text-white-color float-right"
        id="oadh-news-download" type="button">{{ Config::get('system-lang.' . $lang . '.descargartodos') }}</button>
    </div>
  </div>
</div>
<form action="{{url('/cms/medios-de-prensa/download-all')}}" method="POST" id="oadh-news-export-form" class="d-none">
  <input type="hidden" name="oadh-news-export-data" id="oadh-news-export-data">
  <input type="hidden" name="lang" id="oadh-news-export-data-lang" value="{{ $lang }}">
</form>
<form action="{{url('/cms/medios-de-prensa/download-single')}}" method="POST" id="oadh-news-export-form-pdf"
  class="d-none">
  <input type="hidden" name="oadh-news-export-data-pdf" id="oadh-news-export-data-pdf">
  <input type="hidden" name="lang" id="oadh-news-export-data-pdf-lang" value="{{ $lang }}">
</form>
@parent
@stop