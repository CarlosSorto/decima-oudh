<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>{{ Config::get('system-lang.' . $lang . '.recomendacion') }}</title>
  <style type="text/css">
    @page {
      margin: 0cm 0cm;
    }

    * {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 14px;
    }

    .main-section {
      background-repeat: no-repeat;
      background-position: center;
      margin-right: 25px;
      margin-left: 25px;
      opacity: 0.1;
      background-image: url({{ public_path('/oadh/logos/logo-primario-v2.png') }}) ;
    }

    .header-section {
      background-repeat: no-repeat;
      background-position: center;
      margin-top: 10px;
      margin-right: 25px;
      margin-left: 25px;
      height: 100px;
      background-image: url({{ public_path('/oadh/pdf/logos_header.png') }}) ;
    }

    table,
    th {
      text-align: center;
    }

    table {
      border-collapse: collapse;
      background-color: transparent;
      border: 1px solid #000;
    }

    .table {
      width: 100%;
      max-width: 100%;
      line-height: 1.25;
      margin-bottom: 1rem;
    }

    table thead tr th {
      background-color: rgb(0, 22, 70);
      color: white;
      vertical-align: middle;
    }

    table tbody tr td {
      font-size: 12px;
    }

    th,
    td {
      border: 1px solid #000;
      padding: 0 5px;
    }

    .badge {
      padding: .25em .4em;
      font-size: 75%;
      font-weight: 700;
      line-height: 1.25;
    }

    .badge-pill {
      padding-right: .6em;
      padding-left: .6em;
      border-radius: 10rem;
    }

    .badge-primary {
      background-color: rgb(0, 22, 70);
      color: white;
    }

    footer {
      position: fixed;
      bottom: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2.6cm;

      /** Extra personal styles **/
    }

    .image {
      position: relative;
      width: 816px;
      object-fit: fill;
    }
  </style>
</head>

<body>
  <header class="header-section">
    
  </header>
  <section class="main-section">
    <table class="table">
      <thead>
        <tr>
          <th colspan="6" style="font-weight: bold">{{$system}}</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>{{ Config::get('system-lang.' . $lang . '.recomendacion') }}</th>
          <td colspan="5" align="justify">{{$recommendation}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: text-top;">{!! Config::get('system-lang.' . $lang . '.codigoreferencia') !!}</th>
          <td style="width: 10%; vertical-align: text-top;">{{$code}}</td>
          <th style="width: 10%; vertical-align: text-top;">{{ Config::get('system-lang.' . $lang . '.fecharecomendacion') }}</th>
          <td style="width: 10%; vertical-align: text-top;">
            {{\Carbon\Carbon::createFromFormat('Y-m-d', $date)->format(Lang::get('form.phpShortDateFormat'))}}</td>
          <th>{{ Config::get('system-lang.' . $lang . '.mecanismo') }}</th>
          <td align="left" style="vertical-align: text-top;">{{$mechanism}}</td>
        </tr>
        <tr>
          <th>{{ Config::get('system-lang.' . $lang . '.derechosrelacionados') }}</th>
          <td colspan="2" align="left">
            <ul style="padding: 0; margin-left: 10px;">
            @foreach ($rights as $right)
                <li>{{$right}}</li>
            @endforeach
            </ul>
          </td>
          <th>{{ Config::get('system-lang.' . $lang . '.institucionesresponsables') }}</th>
            <td colspan="2" align="left">
              <ul style="padding: 0; margin-left: 10px;">
                @foreach ($institutions as $institution)
                    <li>{{$institution}}</li>
                @endforeach
                </ul>
            </td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: text-top;">{{ Config::get('system-lang.' . $lang . '.fuenterecomendacion') }}</th>
          <td colspan="5" align="left" style="vertical-align: text-top;">{{$source}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: text-top;">{{ Config::get('system-lang.' . $lang . '.baselegal') }}</th>
          <td colspan="5" align="left" style="vertical-align: text-top;">{{$legal_base}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: text-top;">{{ Config::get('system-lang.' . $lang . '.observaciones') }}</th>
          <td colspan="5" align="left" style="vertical-align: text-top; height: 200px;">{{$observations}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: text-top;">{{ Config::get('system-lang.' . $lang . '.actualizacion') }}</th>
          <td colspan="5" align="left" style="vertical-align: text-top; height: 25px;">
            {{ !empty($last_modified_date) ? \Carbon\Carbon::createFromFormat('Y-m-d', $last_modified_date)->format(Lang::get('form.phpShortDateFormat')) : ''}}
          </td>
        </tr>
      </tbody>
    </table>
    <table class="table" style="width: 80%; margin: 50px auto; text-align: left; font-size: 16px;">
      <tr>
        <td style="line-height: 2">
          {{ Config::get('system-lang.' . $lang . '.descargadoel') }} {{\Carbon\Carbon::now()->format(Lang::get('form.phpShortDateFormat'))}} <br>
          {{ Config::get('system-lang.' . $lang . '.fuente') }}: <a href="{{URL::to('/cms/recomendaciones/fichas', $id)}}"
            style="text-decoration: none;">{{URL::to('/cms/recomendaciones/fichas', $id)}}</a>
        </td>
      </tr>
    </table>
  </section>
  <footer>
    {{-- <img src="{{ asset('assets/oadh/pie.jpg')}}" class="image" alt="pie"> --}}
    <img src="{{ public_path('oadh/pdf/pie.jpeg')}}" class="image" alt="pie">
  </footer>
</body>

</html>