@extends('decima-oadh::front-end/base')

@section('mobile-menu')
<div id="left-rights-on-mobile-menu">
	<ul class="my-3" id="nav-mobile-list" role="tabList">
		<li>
			<a id="op-m-1" href="#" data-link-id="op-1">{!! Config::get('system-lang.' . $lang . '.reparacion1') !!}</a>
		</li>
		<li>
			<a id="op-m-2" href="#" data-link-id="op-2">{!! Config::get('system-lang.' . $lang . '.reparacion2') !!}</a>
		</li>
		<li>
			<a id="op-m-3" href="#" data-link-id="op-3">{!! Config::get('system-lang.' . $lang . '.reparacion3') !!}</a>
		</li>
		<li >
			<a id="op-m-4" href="#" data-link-id="op-4">{!! Config::get('system-lang.' . $lang . '.reparacion4') !!}</a>
		</li>
		<li >
			<a id="op-m-5" href="#" data-link-id="op-5">{!! Config::get('system-lang.' . $lang . '.reparacion5') !!}</a>
		</li>
		<li >
			<a id="op-m-6" href="#" data-link-id="op-6">{!! Config::get('system-lang.' . $lang . '.reparacion6') !!}</a>
		</li>
		{{-- <li >
			<a id="op-m-7" href="#" data-link-id="op-7">{!! Config::get('system-lang.' . $lang . '.reparacion7') !!}</a>
		</li> --}}
		<li>
			<a id="op-m-8" href="#" data-link-id="op-8">{!! Config::get('system-lang.' . $lang . '.reparacion8') !!}</a>
		</li>
		<li>
			<a id="op-m-9" href="#" data-link-id="op-9">{!! Config::get('system-lang.' . $lang . '.reparacion9') !!}</a>
		</li>
	</ul>
</div>
@endsection

@section('container')
<div class="container">
  <div class="row my-4">
    <div id="sidebar" class="col-md-3 d-none d-lg-block d-md-none d-sm-none">
      <h4 class="menu-title main-title">{!! Config::get('system-lang.' . $lang . '.derechoreparacion') !!}</h4>
      <ul class="nav flex-column flex-nowrap" id="nav-option-list">
        <li class="nav-item">
          <a
          id="op-1"
          class="nav-link"
          href="#compensation-chart-1"
          role="tab"
          aria-controls="compensation-chart-1"
          aria-selected="true"
          data-tab-number="1"
          data-context="veterans-ex-combatants-mgdt"
          data-div-id="chartdiv-1"
          data-div-filter-id="mapdiv-filter-1"
          data-div-reference-id="mapdiv-reference-1"
          data-chart-type="hbar"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="C001"
          data-description="
          {{ $keyValues['C001'] }}
          "
          data-reference-key="C009"
          data-reference="{{ $keyValues['C009'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.reparacion1') !!}
          </a>
          <a
          id="op-2"
          class="nav-link"
          href="#compensation-chart-2"
          role="tab"
          aria-controls="compensation-chart-2"
          aria-selected="true"
          data-tab-number="2"
          data-context="veterans-ex-combatants-fisdl"
          data-div-id="chartdiv-2"
          data-div-filter-id="mapdiv-filter-2"
          data-div-reference-id="mapdiv-reference-2"
          data-chart-type="bar"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="C002"
          data-description="
          {{ $keyValues['C002'] }}
          "
          data-reference-key="C010"
          data-reference="{{ $keyValues['C010'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.reparacion2') !!}
          </a>
          <a
          id="op-3"
          class="nav-link"
          href="#compensation-chart-3"
          role="tab"
          aria-controls="compensation-chart-3"
          aria-selected="true"
          data-tab-number="3"
          data-context="veterans-vs-victims"
          data-div-id="chartdiv-3"
          data-div-filter-id="mapdiv-filter-3"
          data-div-reference-id="mapdiv-reference-3"
          data-chart-type="bar"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="C003"
          data-description="
          {{ $keyValues['C003'] }}
          "
          data-reference-key="C011"
          data-reference="{{ $keyValues['C011'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.reparacion3') !!}
          </a>
          <a
          id="op-4"
          class="nav-link"
          href="#compensation-chart-4"
          role="tab"
          aria-controls="compensation-chart-4"
          aria-selected="true"
          data-tab-number="4"
          data-context="mgdt-vs-fisdl"
          data-div-id="chartdiv-4"
          data-div-filter-id="mapdiv-filter-4"
          data-div-reference-id="mapdiv-reference-4"
          data-chart-type="dbar"
          data-description-key="C004"
          data-description="
          {{ $keyValues['C004'] }}
          "
          data-reference-key="C012"
          data-reference="{{ $keyValues['C012'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.reparacion4') !!}
          </a>
          <a
          id="op-5"
          class="nav-link"
          href="#compensation-chart-5"
          role="tab"
          aria-controls="compensation-chart-5"
          aria-selected="true"
          data-tab-number="5"
          data-context="fisdl-beneficiaries"
          data-div-id="mapdiv-5"
          data-div-filter-id="mapdiv-filter-5"
          data-div-reference-id="mapdiv-reference-5"
          data-chart-type="map"
          data-grid-id="grid-5"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true }
          ]'
          data-description-key="C005"
          data-description="
          {{ $keyValues['C005'] }}
          "
          data-reference-key="C013"
          data-reference="{{ $keyValues['C013'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.reparacion5') !!}
          </a>
          <a
          id="op-6"
          class="nav-link"
          href="#compensation-chart-6"
          role="tab"
          aria-controls="compensation-chart-6"
          aria-selected="true"
          data-tab-number="6"
          data-context="national-search-comission"
          data-div-id="chartdiv-6"
          data-div-filter-id="mapdiv-filter-6"
          data-div-reference-id="mapdiv-reference-6"
          data-chart-type="hbar"
          data-grid-id="grid-6"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="C006"
          data-description="
          {{ $keyValues['C006'] }}
          "
          data-reference-key="C014"
          data-reference="{{ $keyValues['C014'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.reparacion6') !!}
          </a>
          {{-- <a
          id="op-7"
          class="nav-link"
          href="#compensation-chart-7"
          role="tab"
          aria-controls="compensation-chart-7"
          aria-selected="true"
          data-tab-number="7"
          data-context="allegations-cnb"
          data-div-id="mapdiv-7"
          data-div-filter-id="mapdiv-filter-7"
          data-div-reference-id="mapdiv-reference-7"
          data-chart-type='map'
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="C007"
          data-description="
          {{ $keyValues['C007'] }}
          "
          data-reference-key="C015"
          data-reference="{{ $keyValues['C015'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.reparacion7') !!}
          </a> --}}
          <a
          id="op-8"
          class="nav-link"
          href="#compensation-chart-8"
          role="tab"
          aria-controls="compensation-chart-8"
          aria-selected="true"
          data-tab-number="8"
          data-context="allegations-conabusqueda"
          data-div-id="mapdiv-8"
          data-div-filter-id="mapdiv-filter-8"
          data-div-reference-id="mapdiv-reference-8"
          data-chart-type="map"
          data-grid-id="grid-8"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="C008"
          data-description="
          {{ $keyValues['C008'] }}
          "
          data-reference-key="C016"
          data-reference="{{ $keyValues['C016'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.reparacion8') !!}
          </a>
          <a
          id="op-8"
          class="nav-link"
          href="#compensation-chart-9"
          role="tab"
          aria-controls="compensation-chart-9"
          aria-selected="true"
          data-tab-number="9"
          data-context="compensation-timeline"
          data-div-id="chartdiv-9"
          data-timeline-chart-div-id="timeline-chart-9"
          data-div-filter-id="mapdiv-filter-9"
          data-div-reference-id="mapdiv-reference-9"
          data-chart-type="timeline"
          data-description-key="C020"
          data-description="
          {{ $keyValues['C020'] }}
          "
          data-reference-key="C021"
          data-reference="{{ $keyValues['C021'] }}">
            <i class="fas fa-project-diagram"></i> {!! Config::get('system-lang.' . $lang . '.reparacion9') !!}
          </a>
        </li>
      </ul>
    </div>
    <div id="mobile-menu" class="col-12 text-left d-sm-block d-md-block d-lg-none">
			<h4 class="menu-title main-title">{!! Config::get('system-lang.' . $lang . '.derechoreparacion') !!}</h4>
			<div class="text-center">
				<button type="button" id="btn-mmenu" class="btn btn-secondary oadh-bg-primary-color btn-lg w-100 my-3"><i class="fas fa-bars"></i> {!! Config::get('system-lang.' . $lang . '.veropciones') !!}</button>
			</div>
		</div>
    <div class="col-lg-9">
      <h4 class="oadh-text-primary-color text-center" id="oadh-chart-title"></h4>
			<div class="card card-filter">
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-sm-4">
							{!! Config::get('system-lang.' . $lang . '.disponibles') !!}
						</div>
						<div class="col-sm-8 text-right">
              <div class="btn-toolbar justify-content-end" role="toolbar" aria-label="accesibility buttons">
                <div class="btn-group" role="group" aria-label="filters">
                  <button type="button" id="btn-download-xlsx" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-arrow-down"></i> {{ Config::get('system-lang.' . $lang . '.descargar') }}
                  </button>
                </div>
                <div class="btn-group ml-2" role="group" aria-label="filters">
                  <button type="button" id="btn-filter" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-filter"></i> {!! Config::get('system-lang.' . $lang . '.aplicar') !!}
                  </button>
                </div>
              </div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="mapdiv-filter-1" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-2" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-3" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-4" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-5" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-6" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-7" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-8" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-9" style="display: none;" class="mapdiv-filter"></div>
					<p id="filter-description" data-key='' class="editable-element"></p>
				</div>
			</div>
			<div class="row align-items-center rights-description">
				<div class="col-lg-6">
					{{-- <img src="{{ URL::asset('oadh/inicios/reparacion-v1.jpg') }}" alt="logo" class="d-block w-100 img-fluid mx-auto"> --}}
          <div class="home-rights-container oadh-bg-primary-color">
            <div class="d-flex justify-content-center">
              <div class="rounded-circle home-rights-circle">
                <img src="{{ URL::asset('oadh/inicios/reparacion-v2.png') }}" alt="vida" class="img-fluid">
              </div>
            </div>
            <div class="mt-3">
              <h3 class="oadh-text-white-color text-center">
                {{ Config::get('system-lang.' . $lang . '.derechoreparacion') }}
              </h3>
            </div>
          </div>
				</div>
				<div class="col-lg-6">
					<h3 class="oadh-text-primary-color editable-element" data-key="C017">
            {{ $keyValues['C017'] }}
          </h3>
					<p class="text-justify editable-element" data-key="C018">
						{!! $keyValues['C018'] !!}
					</p>
					<p class="text-justify editable-element" data-key="C019">
						{!! $keyValues['C019'] !!}
					</p>
				</div>
			</div>
			<div class="my-2" data-spy="scroll" data-target="nav-option-list" data-offset="0">
        <div class="oadh-chart" id="tab-content-1" role="tabpanel" aria-labelledby="tc-1">
          <div id="chartdiv-1" class="chartdiv"></div>
          <p id="mapdiv-reference-1" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart">
          <div id="chartdiv-2" class="chartdiv"></div>
          <p id="mapdiv-reference-2" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart">
          <div id="chartdiv-3" class="chartdiv"></div>
          <p id="mapdiv-reference-3" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart">
          <div id="chartdiv-4" class="chartdiv"></div>
          <p id="mapdiv-reference-4" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart">
          <div id="mapdiv-5" class="chartdiv"></div>
          <p id="mapdiv-reference-5" class="reference-text editable-element"></p>
          {{-- <div id="grid-5">
            <div class="smt-body"></div>
          </div> --}}
        </div>
        <div class="oadh-chart">
          <div id="chartdiv-6" class="chartdiv"></div>
          <p id="mapdiv-reference-6" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-7" role="tabpanel" aria-labelledby="tc-7">
          <div id="mapdiv-7" class="chartdiv"></div>
          <p id="mapdiv-reference-7" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-8" role="tabpanel" aria-labelledby="tc-8">
          <div id="mapdiv-8" class="chartdiv"></div>
          <p id="mapdiv-reference-8" class="reference-text editable-element"></p>
          {{-- <div id="grid-8">
            <div class="smt-body"></div>
          </div> --}}
        </div>
        <div class="oadh-chart" id="tab-content-9" role="tabpanel" aria-labelledby="tc-9">
          <div id="chartdiv-9" class="chartdiv" data-date-type="year" data-child-url="" data-is-line="1" data-child-div="chartdiv-10"></div>
          <div id="chartdiv-10" class="chartdiv d-none"data-date-type="month" data-child-url="" data-is-line="1" data-div-id="chartdiv-10" data-child-div="chartdiv-11"></div>
          <div id="chartdiv-11" data-date-type="day" data-is-line="1" data-div-id="chartdiv-11" class="chartdiv d-none"></div>
          <p id="mapdiv-reference-9" class="reference-text editable-element"></p>
        </div>
			</div>
		</div>
  </div>
</div>
{!! Form::open(array('id' => 'oadh-xlsx-download-form', 'url' => URL::to('/cms/derechos/download-data-xlsx'), 'role'  =>  'form', 'class' => 'form-horizontal')) !!}
  {!! Form::hidden('oadh-xlsx-data', null, ['id' => 'oadh-xlsx-data', 'data-details' => '[]']) !!}
  {!! Form::hidden('oadh-xlsx-data-titles', null, ['id' => 'oadh-xlsx-data-titles', 'data-details-titles' => '[]']) !!}
{!! Form::close() !!}
@parent
@stop