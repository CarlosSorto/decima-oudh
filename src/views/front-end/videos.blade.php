@extends('decima-oadh::front-end/base')
@section('container')
<div class="container">
  <div class="row my-4">
    <div class="col-sm-12 video-wrapper oadh-bg-primary-color">
      <div class="my-3 mx-3 align-items-center">
        
        <div class="row mb-3 justify-content-center">
          <div class="col-sm-12 col-md-8">
            <div class="form-row justify-content-center">
              <div class="input-group">
                <input type="text" class="form-control oadh-outline-none" id="oadh-multi-search-text" name="oadh-multi-search-text"
                  required placeholder="{{ Config::get('system-lang.' . $lang . '.escribir') }}">
                <div class="input-group-append">
                  <button type="button" class="btn oadh-multi-search-control oadh-bg-secondary-color oadh-outline-none oadh-text-white-color"
                    id="oadh-multi-btn-search" type="button">{{ Config::get('system-lang.' . $lang . '.buscar') }}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        {{-- @if (!empty($videos['highlighted']))
          @foreach ($videos['highlighted'] as $highlight)
          <div class="col-sm-8 mt-3">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="{{$highlight['image_url'] }}"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
          <div class="col-sm-4">
            <p class="font-weight-bold title-entry-text videos-text-color text-left">{{ $highlight['title'] }}</p>
            <p class="text-justify videos-text-color">{{$highlight['description'] }}</p>
          </div>
          @endforeach
        @endif --}}
        <div id="oadh-multi-gallery" class="row">
          @include('decima-oadh::front-end/videos-card')
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@parent
@stop
