@if (!empty($news))
  @foreach ($news as $new)
  <div class="col-sm-6 my-3 d-flex align-items-stretch">
    <div class="card shadow-sm">
      <div class="card-body">
        <div class="card-title">
          <strong class="d-inline-block mb-2 oadh-text-primary-color">{{$new->oadh_digital_media_label}}</strong>
        </div>
        <h3 class="mb-0 clickable"><a href="{{url('cms/medios-de-prensa/fichas', $new->oadh_id)}}">{{$new->oadh_title}}</a></h3>
        <div class="mb-1 text-muted">{{Carbon\Carbon::createFromFormat('Y-m-d', $new->oadh_date)->format(Lang::get('form.phpShortDateFormat'))}}</div>
        <div class="card-text-mb-auto text-justify clickable"><a href="{{url('cms/medios-de-prensa/fichas', $new->oadh_id)}}">{{substr($new->oadh_summary, 0, 300)}}</a></div>
        <div class="row">
          <div class="col mb-2">
            @foreach ($new->topics as $topic)
            <span class="badge badge-pill badge-primary">{{$topic->name}}</span>
            @endforeach
          </div>
        </div>
        <a href="{{url('cms/medios-de-prensa/fichas', $new->oadh_id)}}"
        class="oadh-text-primary-color">{!! Config::get('system-lang.' . $lang . '.verficha') !!}</a>
        @if (!empty($new->oadh_new_link))
        <a href="{{$new->oadh_new_link}}" target="_blank" class="btn btn-sm oadh-bg-secondary-color oadh-outline-none oadh-text-white-color float-right ml-2">{!! Config::get('system-lang.' . $lang . '.enlace') !!}</a>
        @endif
        <button type="button" class="btn btn-sm oadh-bg-secondary-color oadh-outline-none oadh-text-white-color oadh-news-single-download float-right" data-id="{{$new->oadh_id}}" type="button">{{ Config::get('system-lang.' . $lang . '.descargar') }}</button>
      </div>
    </div>
  </div>
  @endforeach
  <div class="col-sm-12 justify-content-center">
    @include('decima-oadh::front-end/pagination', ['paginator' => $news])
  </div>
@else
  <div class="col-sm-6 my-3">
    <h4>No hay datos que mostrar</h4>
  </div>
@endif