<div class="row justify-content-center">
	<div class="col-sm-12 col-md-8" id="oadh-rdt-search-group">
		<input type="hidden" id="oadh-rdt-search-form" data-action="{{url('cms/recomendaciones/advanced-smt-rows')}}">
		<div class="form-row justify-content-center">
			<div class="input-group">
				<div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="Búsqueda avanzada">
					<button type="button" id="oadh-rdt-btn-collapse-filter"
						class="btn oadh-rdt-search-control oadh-bg-primary-color oadh-outline-none oadh-text-white-color oadh-dropdown-toggle"
						data-toggle="collapse" href="#oadh-rdt-collapse-filter-form" role="button" aria-expanded="false"
						aria-controls="oadh-rdt-collapse-filter-form">
						<span class="sr-only">Toggle collapse</span>
						{!! Config::get('system-lang.' . $lang . '.opciones') !!}
					</button>
				</div>
				<input type="text" class="form-control oadh-outline-none" id="oadh-rdt-search-text" name="oadh-rdt-search-text"
					required placeholder="{{ Config::get('system-lang.' . $lang . '.escribir') }}">
				<div class="input-group-append">
					<button type="button" class="btn oadh-rdt-search-control oadh-bg-primary-color oadh-outline-none oadh-text-white-color"
						id="oadh-rdt-btn-search" type="button">{{ Config::get('system-lang.' . $lang . '.buscar') }}</button>
				</div>
				<div class="input-group-append">
					<button type="button" class="btn oadh-rdt-search-control oadh-bg-secondary-color oadh-outline-none oadh-text-white-color"
						id="oadh-rdt-btn-clear-filter" type="button">{{ Config::get('system-lang.' . $lang . '.limpiar') }}</button>
				</div>
			</div>
			<div class="col-sm-12 p-0">
				<div class="collapse" id="oadh-rdt-collapse-filter-form">
					<div class="card card-body rounded-0 oadh-card">
						<div class="form-row">
							<div class="col-md-6 mb-3">
								<label for="oadh-rdt-system">{{ Config::get('system-lang.' . $lang . '.sistema') }}</label>
								<select id="oadh-rdt-system" name="oadh-rdt-system" class="form-control selectpicker" multiple
									data-size="2">
									@foreach ($systems as $system)
									<option value="{{$system['system']}}">{{$system['system']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-6 mb-3" id="oadh-rdt-date-time-group">
								<label for="validationCustom01">{{ Config::get('system-lang.' . $lang . '.rango') }}</label>
								<div class="input-group">
									<div class="input-group-prepend d-none d-sm-block">
										<p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.de') }}</p>
									</div>
									<input type="input" class="oadh-outline-none form-control" id="oadh-rdt-date-from" name="oadh-rdt-date-from" maxlength="4" placeholder="{!! Config::get('system-lang.' . $lang . '.digiteanio') !!}...">
									<div class="input-group-prepend input-group-append d-none d-sm-block">
										<p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.al') }}</p>
									</div>
									<input type="input" class="oadh-outline-none form-control" id="oadh-rdt-date-to" name="oadh-rdt-date-to" maxlength="4" placeholder="{!! Config::get('system-lang.' . $lang . '.digiteanio') !!}...">
								</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="oadh-rdt-">{{ Config::get('system-lang.' . $lang . '.mecanismo') }}</label>
								<select id="oadh-rdt-mechanism" name="oadh-rdt-mechanism" class="form-control selectpicker" multiple>
								</select>
							</div>
							<div class="col-md-6 mb-3">
								<label for="validationCustom03">{{ Config::get('system-lang.' . $lang . '.institucion') }}</label>
								<select id="oadh-rdt-institution" name="oadh-rdt-institution" class="form-control selectpicker"
									multiple data-size="2">
									@if (count($institutions) > 0)
									@foreach ($institutions as $institution)
									<option value="{{$institution->institution_id}}">{{$institution->institution}}
										({{$institution->abbreviation}})</option>
									@endforeach
									@endif
								</select>
							</div>
							<div class="col-md-6 mb-3">
								<label for="validationCustom05">{{ Config::get('system-lang.' . $lang . '.derecho') }}</label>
								<select id="oadh-rdt-right" name="oadh-rdt-right" class="form-control selectpicker" multiple data-size="2">
									@foreach ($rights as $right)
									<option value="{{$right->right_id}}">{{$right->right}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>