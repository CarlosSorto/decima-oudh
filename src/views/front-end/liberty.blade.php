@extends('decima-oadh::front-end/base')

@section('mobile-menu')
<div id="left-rights-on-mobile-menu">
	<ul class="my-3" id="nav-mobile-list" role="tabList">
		<li>
			<a id="op-m-1" href="#" data-link-id="op-1">{!! Config::get('system-lang.' . $lang . '.libertadop1') !!}</a>
		</li>
		<li>
			<a id="op-m-2" href="#" data-link-id="op-2">{!! Config::get('system-lang.' . $lang . '.libertadop2') !!}</a>
		</li>
		<li>
			<a id="op-m-3" href="#" data-link-id="op-3">{!! Config::get('system-lang.' . $lang . '.libertadop3') !!}</a>
		</li>
		<li >
			<a id="op-m-4" href="#" data-link-id="op-4">{!! Config::get('system-lang.' . $lang . '.libertadop4') !!}</a>
		</li>
		<li >
			<a id="op-m-5" href="#" data-link-id="op-5">{!! Config::get('system-lang.' . $lang . '.libertadop5') !!}</a>
		</li>
		<li >
			<a id="op-m-6" href="#" data-link-id="op-6">{!! Config::get('system-lang.' . $lang . '.libertadop6') !!}</a>
		</li>
		<li >
			<a id="op-m-7" href="#" data-link-id="op-7">{!! Config::get('system-lang.' . $lang . '.libertadop7') !!}</a>
		</li>
		<li>
			<a id="op-m-8" href="#" data-link-id="op-8">{!! Config::get('system-lang.' . $lang . '.libertadop8') !!}</a>
		</li>
		<li>
			<a id="op-m-9" href="#" data-link-id="op-9">{!! Config::get('system-lang.' . $lang . '.libertadop9') !!}</a>
		</li>
		<li >
			<a id="op-m-10" href="#" data-link-id="op-10">{!! Config::get('system-lang.' . $lang . '.libertadop10') !!}</a>
		</li>
		<li >
			<a id="op-m-11" href="#" data-link-id="op-11">{!! Config::get('system-lang.' . $lang . '.libertadop11') !!}</a>
		</li>
		<li>
			<a id="op-m-12" href="#" data-link-id="op-12">{!! Config::get('system-lang.' . $lang . '.libertadop12') !!}</a>
		</li>
		<li>
			<a id="op-m-13" href="#" data-link-id="op-13">{!! Config::get('system-lang.' . $lang . '.libertadop13') !!}</a>
		</li>
		<li>
			<a id="op-m-14" href="#" data-link-id="op-14">{!! Config::get('system-lang.' . $lang . '.libertadop14') !!}</a>
		</li>
	</ul>
</div>
@endsection

@section('container')
<div class="container">
	<div class="row my-4">
		<div id="sidebar" class="col-md-3 d-none d-lg-block d-md-none d-sm-none">
			<h4 class="menu-title">{!! Config::get('system-lang.' . $lang . '.derecholibertad') !!}</h4>
			<ul class="nav flex-column flex-nowrap" id="nav-option-list" role="tabList">
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-1"
						id="op-1"
						role="tab"
						aria-controls="tab-content-1"
						aria-selected="true"
						data-tab-number="1"
						data-div-id="mapdiv-1"
						data-div-filter-id="mapdiv-filter-1"
						data-div-reference-id="mapdiv-reference-1"
						data-chart-type='bar'
						data-context="habeas-corpus-request"
						data-filters='[
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "court", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.juzgado') !!}", "multiple": true }
						]'
						data-description-key="L001"
						data-reference-key="L015"
						data-description="{{ $keyValues['L001'] }}"
						data-reference="{{ $keyValues['L015'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop1') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-2"
						id="op-2"
						data-div-id="mapdiv-2"
						data-div-filter-id="mapdiv-filter-2"
						data-div-reference-id="mapdiv-reference-2"
						data-chart-type='bar'
						data-context="people-detained"

						role="tab"
						aria-controls="tab-content-2"
						aria-selected="false"
						data-tab-number="2"
						data-filters='[
							{ "filter": "state", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.estado') !!}", "multiple": true },
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true }
						]'
						data-description-key="L002"
						data-reference-key="L016"
						data-description="{{ $keyValues['L002'] }}"
						data-reference="{{ $keyValues['L016'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop2') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-3"
						id="op-3"
						role="tab"
						aria-controls="tab-content-3"
						aria-selected="false"
						data-tab-number="3"
						data-div-id="chartdiv-3"
						data-div-filter-id="mapdiv-filter-3"
						data-div-reference-id="mapdiv-reference-3"
						data-chart-type='chart2'
						data-context="criminal-cases-trials"
						data-filters='[
							{ "filter": "court", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.juzgado') !!}", "multiple": true }
						]'
						data-description-key="L003"
						data-reference-key="L017"
						data-description="{{ $keyValues['L003'] }}"
						data-reference="{{ $keyValues['L017'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop3') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-4"
						id="op-4"
						role="tab"
						aria-controls="tab-content-4"
						aria-selected="false"
						data-tab-number="4"
						data-div-id="mapdiv-4"
						data-div-filter-id="mapdiv-filter-4"
						data-div-reference-id="mapdiv-reference-4"
						data-chart-type='bar'
						data-context="mass-trials"
						data-filters='[
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "court", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.juzgado') !!}", "multiple": true }
						]'
						data-description-key="L004"
						data-reference-key="L018"
						data-description="{{ $keyValues['L004'] }}"
						data-reference="{{ $keyValues['L018'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop4') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-5"
						id="op-5"
						role="tab"
						aria-controls="tab-content-5"
						aria-selected="false"
						data-tab-number="5"
						data-div-id="mapdiv-5"
						data-div-filter-id="mapdiv-filter-5"
						data-div-reference-id="mapdiv-reference-5"
						data-chart-type='bar'
						data-context="procedural-fraud"
						data-filters='[
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
						]'
						data-description-key="L005"
						data-reference-key="L019"
						data-description="{{ $keyValues['L005'] }}"
						data-reference="{{ $keyValues['L019'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop5') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-6"
						id="op-6"
						role="tab"
						aria-controls="tab-content-6"
						aria-selected="false"
						data-tab-number="6"
						data-div-id="mapdiv-6"
						data-div-filter-id="mapdiv-filter-6"
						data-div-reference-id="mapdiv-reference-6"
						data-chart-type='bar'
						data-context="capture-orders"
						data-description-key="L014"
						data-reference-key="L020"
						data-description="{{ $keyValues['L014'] }}"
						data-filters='[
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
						]'
						data-reference="{{ $keyValues['L020'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop6') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-7"
						id="op-7"
						role="tab"
						aria-controls="tab-content-7"
						aria-selected="false"
						data-tab-number="7"
						data-div-id="mapdiv-7"
						data-div-filter-id="mapdiv-filter-7"
						data-div-reference-id="mapdiv-reference-7"
						data-chart-type='bar'
						data-context="disappearances-victims"
						data-filters='[
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
						]'
						data-description-key="L006"
						data-reference-key="L021"
						data-description="{{ $keyValues['L006'] }}"
						data-reference="{{ $keyValues['L021'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop7') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-8"
						id="op-8"
						role="tab"
						aria-controls="tab-content-8"
						data-tab-number="8"
						aria-selected="false"
						data-div-id="mapdiv-8"
						data-div-filter-id="mapdiv-filter-8"
						data-div-reference-id="mapdiv-reference-8"
						data-chart-type='bar'
						data-context="accused-liberty-deprivation"
						data-filters='[
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
							{ "filter": "profession", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.profesion') !!}", "multiple": true },
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true }
						]'
						data-description-key="L007"
						data-reference-key="L022"
						data-description="{{ $keyValues['L007'] }}"
						data-reference="{{ $keyValues['L022'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop8') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-9"
						id="op-9"
						role="tab"
						aria-controls="tab-content-9"
						aria-selected="false"
						data-tab-number="9"
						data-div-id="mapdiv-9"
						data-div-filter-id="mapdiv-filter-9"
						data-div-reference-id="mapdiv-reference-9"
						data-chart-type='bar'
						data-context="victims-liberty-deprivation"
						data-filters='[
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
						]'
						data-description-key="L008"
						data-reference-key="L023"
						data-description="{{ $keyValues['L008'] }}"
						data-reference="{{ $keyValues['L023'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop9') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-10"
						id="op-10"
						role="tab"
						aria-controls="tab-content-10"
						aria-selected="false"
						data-tab-number="10"
						data-div-id="mapdiv-10"
						data-div-filter-id="mapdiv-filter-10"
						data-div-reference-id="mapdiv-reference-10"
						data-chart-type='bar'
						data-context="forced-disappearances"
						data-filters='[
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
							{ "filter": "profession", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.profesion') !!}", "multiple": true },
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true }
						]'
						data-description-key="L009"
						data-reference-key="L024"
						data-description="{{ $keyValues['L009'] }}"
						data-reference="{{ $keyValues['L024'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop10') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-11"
						id="op-11"
						role="tab"
						aria-controls="tab-content-11"
						aria-selected="false"
						data-tab-number="11"
						data-div-id="chartdiv-11"
						data-div-filter-id="mapdiv-filter-11"
						data-div-reference-id="mapdiv-reference-11"
						data-chart-type='dbar'
						data-context="jails-occupation"
						data-filters='[
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "station", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.estacion') !!}", "multiple": true },
							{ "filter": "unit", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.unidad') !!}", "multiple": true }
						]'
						data-description-key="L010"
						data-reference-key="L025"
						data-description="{{ $keyValues['L010'] }}"
						data-reference="{{ $keyValues['L025'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop11') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-12"
						id="op-12"
						role="tab"
						aria-controls="tab-content-12"
						aria-selected="false"
						data-tab-number="12"
						data-div-id="mapdiv-12"
						data-div-filter-id="mapdiv-filter-12"
						data-div-reference-id="mapdiv-reference-12"
						data-chart-type='bar'
						data-context="deprived-persons"
						data-filters='[
							{ "filter": "right", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.derechohumano') !!}", "multiple": true },
							{ "filter": "violatedFact", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.hecho') }}", "multiple": true },
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
						]'
						data-description-key="L011"
						data-reference-key="L026"
						data-description="{{ $keyValues['L011'] }}"
						data-reference="{{ $keyValues['L026'] }}"><i
							class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop12') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-13"
						id="op-13"
						role="tab"
						aria-controls="tab-content-13"
						aria-selected="false"
						data-tab-number="13"
						data-div-id="mapdiv-13"
						data-div-filter-id="mapdiv-filter-13"
						data-div-reference-id="mapdiv-reference-13"
						data-chart-type='bar'
						data-context="acute-diseases"
						data-filters='[
							{ "filter": "disease", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.enfermedad') !!}", "multiple": true }
						]'
						data-description-key="L012"
						data-reference-key="L027"
						data-description="{{ $keyValues['L012'] }}"
						data-reference="{{ $keyValues['L027'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop13') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#mapdiv-14"
						id="op-14"
						role="tab"
						aria-controls="tab-content-14"
						aria-selected="false"
						data-tab-number="14"
							data-div-id="mapdiv-14"
							data-div-filter-id="mapdiv-filter-14"
							data-div-reference-id="mapdiv-reference-14"
						data-chart-type='bar'
							data-context="chronic-diseases"
						data-filters='[
							{ "filter": "chronicDisease", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.enfermedad') !!}", "multiple": true },
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "jail", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.penal') !!}", "multiple": true }
						]'
						data-description-key="L013"
						data-reference-key="L028"
						data-description="{{ $keyValues['L013'] }}"
						data-reference="{{ $keyValues['L028'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.libertadop14') !!}</a>
				</li>
			</ul>
		</div>
		<div id="mobile-menu" class="col-12 text-left d-sm-block d-md-block d-lg-none">
			<h4 class="menu-title main-title">{!! Config::get('system-lang.' . $lang . '.derecholibertad') !!}</h4>
			<div class="text-center">
				<button type="button" id="btn-mmenu" class="btn btn-secondary oadh-bg-primary-color btn-lg w-100 my-3"><i class="fas fa-bars"></i> {!! Config::get('system-lang.' . $lang . '.veropciones') !!}</button>
			</div>
		</div>
		<div class="col-lg-9">
			<h4 class="oadh-text-primary-color text-center" id="oadh-chart-title"></h4>
			<div class="card card-filter">
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-sm-4">
							{!! Config::get('system-lang.' . $lang . '.disponibles') !!}
						</div>
						<div class="col-sm-8 text-right">
              <div class="btn-toolbar justify-content-end" role="toolbar" aria-label="accesibility buttons">
                <div class="btn-group" role="group" aria-label="filters">
                  <button type="button" id="btn-download-xlsx" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-arrow-down"></i> {{ Config::get('system-lang.' . $lang . '.descargar') }}
                  </button>
                </div>
                <div class="btn-group ml-2" role="group" aria-label="filters">
                  <button type="button" id="btn-filter" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-filter"></i> {!! Config::get('system-lang.' . $lang . '.aplicar') !!}
                  </button>
                </div>
              </div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="mapdiv-filter-1" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-2" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-3" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-4" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-5" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-6" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-7" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-8" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-9" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-10" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-11" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-12" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-13" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-14" style="display: none;" class="mapdiv-filter"></div>
					<p id="filter-description" data-key='' class="editable-element"></p>
				</div>
			</div>
			<div class="row align-items-center rights-description">
				<div class="col-lg-6">
					{{-- <img src="{{ URL::asset('oadh/inicios/libertad-v3.png') }}" alt="logo" class="d-block w-100 img-fluid mx-auto"> --}}
					<div class="home-rights-container oadh-bg-primary-color">
						<div class="d-flex justify-content-center">
							<div class="rounded-circle home-rights-circle">
								<img src="{{ URL::asset('oadh/inicios/libertad-v4.png') }}" alt="vida" class="img-fluid">
							</div>
						</div>
						<div class="mt-3">
							<h3 class="oadh-text-white-color text-center">
								{{ Config::get('system-lang.' . $lang . '.derecholibertad') }}
							</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<h3 class="oadh-text-primary-color editable-element" data-key="L029">{{ $keyValues['L029'] }}</h3>
					<p class="text-justify editable-element" data-key="L030">
						{!! $keyValues['L030'] !!}
					</p>
					<p class="text-justify editable-element" data-key="L031">
						{!! $keyValues['L031'] !!}
					</p>
				</div>
			</div>
			<div class="my-2" data-spy="scroll" data-target="nav-option-list" data-offset="0">
				<div class="oadh-chart">
					<div id="mapdiv-1" class="chartdiv"></div>
					<p id="mapdiv-reference-1" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart">
					<div id="mapdiv-2" class="chartdiv"></div>
					<p id="mapdiv-reference-2" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart">
					<div id="chartdiv-3" class="chartdiv"></div>
					<p id="mapdiv-reference-3" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart">
					<div id="mapdiv-4" class="chartdiv"></div>
					<p id="mapdiv-reference-4" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart">
					<div id="mapdiv-5" class="chartdiv"></div>
					<p id="mapdiv-reference-5" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart">
					<div id="mapdiv-6" class="chartdiv"></div>
					<p id="mapdiv-reference-6" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart">
					<div id="mapdiv-7" class="chartdiv"></div>
					<p id="mapdiv-reference-7" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart" id="tab-content-8" role="tabpanel" aria-labelledby="tc-8">
					<div id="mapdiv-8" class="chartdiv"></div>
					<p id="mapdiv-reference-8" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart" id="tab-content-9" role="tabpanel" aria-labelledby="tc-9">
					<div id="mapdiv-9" class="chartdiv"></div>
					<p id="mapdiv-reference-9" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart" id="tab-content-10" role="tabpanel" aria-labelledby="tc-10">
					<div id="mapdiv-10" class="chartdiv"></div>
					<p id="mapdiv-reference-10" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart" id="tab-content-11" role="tabpanel" aria-labelledby="tc-11">
					<div id="chartdiv-11" class="chartdiv"></div>
					<p id="mapdiv-reference-11" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart" id="tab-content-12" role="tabpanel" aria-labelledby="tc-12">
					<div id="mapdiv-12" class="chartdiv"></div>
					<p id="mapdiv-reference-12" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart" id="tab-content-13" role="tabpanel" aria-labelledby="tc-13">
					<div id="mapdiv-13" class="chartdiv"></div>
					<p id="mapdiv-reference-13" class="reference-text editable-element"></p>
				</div>
				<div class="oadh-chart" id="tab-content-14" role="tabpanel" aria-labelledby="tc-14">
					<div id="mapdiv-14" class="chartdiv"></div>
					<p id="mapdiv-reference-14" class="reference-text editable-element"></p>
				</div>
			</div>
		</div>
	</div>
</div>
{!! Form::open(array('id' => 'oadh-xlsx-download-form', 'url' => URL::to('/cms/derechos/download-data-xlsx'), 'role'  =>  'form', 'class' => 'form-horizontal')) !!}
  {!! Form::hidden('oadh-xlsx-data', null, ['id' => 'oadh-xlsx-data', 'data-details' => '[]']) !!}
  {!! Form::hidden('oadh-xlsx-data-titles', null, ['id' => 'oadh-xlsx-data-titles', 'data-details-titles' => '[]']) !!}
{!! Form::close() !!}
@parent
@stop
