<style>
  #page-wrapper {
    padding-top: 56px;
  }

  .oadh-bg-primary-color {
    background: rgb(0, 22, 70) !important;
  }

  .oadh-text-primary-color {
    color: rgb(0, 22, 70) !important;
  }

  .oadh-bg-secondary-color {
    background-color: rgb(255, 152, 21) !important;
  }

  .oadh-active-nav-link,
  .oadh-text-secondary-color {
    color: rgb(255, 152, 21) !important;
  }

  .oadh-navbar-brand {
    font-weight: 500;
  }

  .oadh-fake-link {
	cursor: pointer;
  }

  /* custom Tinymce */
  .tox-toolbar {
    z-index: 1100 !important;
  }

  .highlight { background-color: yellow }

  /* .tox-collection__item-label h1,
  .tox-collection__item-label h2,
  .tox-collection__item-label h3,
  .tox-collection__item-label h4,
  .tox-collection__item-label h5,
  .tox-collection__item-label h6 {
    color: #000000 !important;
  } */

  /* .tox-collection__item-label  > * {} {
    color: #000000 !important;
  } */

  /* custom navbar classes */
  .oadh-text-white-color,
  .oadh-text-tertiary-color,
  .oadh-nav-text-color .nav-link,
  .oadh-nav-text-color .navbar-brand {
    color: #ffffff !important;
  }

  .nav-link-right {
    display: block;
    width: 100%;
    padding: 0.5rem 1rem 0.5rem 0;
  }

  .nav-link-press {
    display: block;
    padding: 0.5rem 1rem 0.5rem 1.5rem ;
  }

  .nav-link-press01 {
    display: block;
    padding: 0.5rem 1rem 0.5rem 0.5rem ;
    /* margin-bottom: 0.5rem; */
  }
  
  .nav-link-press-font-size {
    font-size: 10pt !important;
  }

  .dropdown-toggle:active,
  .dropdown-toggle,
  .nav-link:hover,
  .nav-link-right:hover,
  .nav-link-press:hover {
    color: rgb(255, 152, 21) !important;
  }

  .nav-link-press01:hover {
    color: rgb(0, 22, 70) !important;
    border-left: 2px solid rgb(0, 22, 70);
  }

  .nav-link-press01-active {
    background-color: rgb(0, 22, 70);
    border-radius: 0.25rem;
  }

  .nav-link-press01-active:hover {
    color: #ffffff !important;
  }

  .oadh-press-human-right {
    /* padding-left: 0.5rem !important; */
    -webkit-box-shadow: -2px 2px 2px 0px rgba(255, 152, 21);
    -moz-box-shadow: -2px 2px 2px 0px rgba(255, 152, 21);
    box-shadow: -2px 2px 2px 0px rgba(255, 152, 21);
  }

  .oadh-press-victim-note {
    padding-top: 0 !important;
    padding-bottom: 0 !important;
  }

  .login {
    border: 1px solid transparent;
    border-radius: 5px;
  }

  .login:hover {
    border: 1px solid transparent;
    border-radius: 5px;
    color: #fff !important;
    background-color: #FF9815;
  }

  /* General classes */
  .app-loader {
    z-index: 1051;
    position: absolute;
    width: 250px;
    margin: 25% 32%;
    color: #ffffff !important;
    background: #2c3e50 !important;
    border-color: #222222 !important;
  }

  /* Home page custom classes */
  .home-carouse-min-height {
    min-height: 400px;
  }

  .home-rights-container {
    border-radius: 4rem;
    min-height: 350px;
  }
  

  .home-rights-circle {
    background-color: #ffffff;
    width: 225px;
    height: 225px;
    margin-top: 20px; 
  }

  .home-rights-circle:hover {
    background-color: rgb(255, 152, 21) !important;
  }

  .homemap {
    width: 100%;
    height: 392px;
  }

  /* Pantallas Derechos*/
  .chartdiv {
    width: 100%;
    height: 600px;
  }

  #filter-description {
    font-size: 0.8rem;
  }

  .reference-text {
    font-size: 0.8rem;
    font-weight: bolder !important;
  }

  .brand-logo {
    max-width: 100%;
    height: 20px;
  }

  .slider.slider-horizontal {
    width: 90% !important;
  }

  .menu-title {
    font-size: 12pt;
    text-align: left;
    font-weight: 600;
    padding: 10px 10px;
    /* margin-bottom: 0px; */
    /* border-left: 2px solid #2c3e50; */
    -webkit-box-shadow: -2px 2px 2px 0px rgba(255, 152, 21);
    -moz-box-shadow: -2px 2px 2px 0px rgba(255, 152, 21);
    box-shadow: -2px 2px 2px 0px rgba(255, 152, 21);
    font-family: "Avant Garde", Avantgarde, "Century Gothic", CenturyGothic, "AppleGothic", sans-serif;
  }


  #nav-option-list .nav-link {
    font-size: 10pt;
    line-height: 20px;
    color: #222222;
  }

  .nav .active {
    color: white !important;
    background-color: rgb(255, 152, 21);
    border-radius: 0.25rem;
  }

  #nav-option-list .nav-link:hover, .press-option-list .nav-link-press:hover {
    border-left: 2px solid rgb(255, 152, 21);
  }

  /* ACTIVIDADES */
  .card-shadow {
    border-radius: 35px;
    -webkit-box-shadow: -4px 5px 8px 0px rgba(0, 0, 0, 0.4);
    -moz-box-shadow: -4px 5px 8px 0px rgba(0, 0, 0, 0.4);
    box-shadow: -4px 5px 8px 0px rgba(0, 0, 0, 0.4);
  }

  .gallery-carousel .carousel-control-prev {
    opacity: unset;
    top: 100%;
    left: 30%;
  }

  .gallery-carousel .carousel-control-next {
    opacity: unset;
    top: 100%;
    left: 50%;
  }

  .activity-carousel .carousel-control-prev {
    opacity: unset;
    top: 90%;
    left: 70%;
  }

  .activity-carousel .carousel-control-next {
    opacity: unset;
    top: 90%;
    left: 80%;
  }


  .activity-carousel .carousel-control-next-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23163D75' viewBox='0 0 8 8'%3e%3cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3e%3c/svg%3e");
  }

  .activity-carousel .carousel-control-prev-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23163D75' viewBox='0 0 8 8'%3e%3cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3e%3c/svg%3e");
  }

  .gallery-carousel .carousel-control-next-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23163D75' viewBox='0 0 8 8'%3e%3cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3e%3c/svg%3e");
  }

  .gallery-carousel .carousel-control-prev-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23163D75' viewBox='0 0 8 8'%3e%3cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3e%3c/svg%3e");
  }

  .carousel-control-next-icon,
  .carousel-control-prev-icon {
    width: 2rem;
    height: 2rem;
  }

  /* PUBLICACIONES */
  .btn-entries {
    height: 6rem;
    border-radius: 28px 28px 28px 28px !important;
    -moz-border-radius: 28px 28px 28px 28px !important;
    -webkit-border-radius: 28px 28px 28px 28px !important;
    border: 0px solid #000000;
    background-color: #ffffff;
    -webkit-box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39) !important;
    -moz-box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39) !important;
    box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39) !important;
    font-size: 1.2rem !important;
    font-weight: 500 !important;
    color: rgb(0, 22, 70) !important;
  }

  .active-tabs {
    -webkit-box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39) !important;
    -moz-box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39) !important;
    box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39);
    color: #ffffff !important;
    background-color: rgb(0, 22, 70) !important;
    border-color: transparent !important;
  }

  .btn-entries:focus,
  .btn-entries:active {
    outline: none;
    -webkit-box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39);
    -moz-box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39);
    box-shadow: -6px 5px 5px 0px rgba(0, 0, 0, 0.39);
  }

  .btn-bulletins {
    background-image: url({{ URL::asset('oadh/publicaciones/boletines-v1.png') }});
    background-size: 5rem;
    background-repeat: no-repeat;
    background-position: 9px;
  }

  .btn-memories {
    background-image: url({{ URL::asset('oadh/publicaciones/memorias-v1.png') }});
    background-size: 5rem;
    background-repeat: no-repeat;
    background-position: 9px;
  }

  .btn-reports {
    background-image: url({{ URL::asset('oadh/publicaciones/informes-v1.png') }});
    background-size: 5rem;
    background-repeat: no-repeat;
    background-position: 9px;
  }

  .btn-investigations {
    background-image: url({{ URL::asset('oadh/publicaciones/investigaciones-v1.png') }});
    background-size: 5rem;
    background-repeat: no-repeat;
    background-position: 9px;
  }

  .btn-summaries {
    background-image: url({{ URL::asset('oadh/publicaciones/resumenes-v2.png') }});
    background-size: 5rem;
    background-repeat: no-repeat;
    background-position: 9px;
  }

  .title-entry-text {
    font-size: 1.5rem;
    word-spacing: 0.1rem;
  }

  .entry-text {
    color: #c2c2c2;
  }

  /* Prensa */
  .press-section-padding {
    padding: 2rem !important;
  }

  /* VIDEOS */
  .title-entry-text {
    font-size: 1.5rem;
    word-spacing: 0.1rem;
  }

  .video-wrapper {
    border-radius: 25px 25px 25px 25px;
    -moz-border-radius: 25px 25px 25px 25px;
    -webkit-border-radius: 25px 25px 25px 25px;
    border: 0px solid #000000;
  }

  .videos-text-color {
    color: #fff;
  }

  .video-card {
    background-color: transparent !important;
    border: none !important;
  }
  
  .video-carousel .carousel-control-prev {
    opacity: unset;
    top: 98%;
    left: 20%;
  }

  .video-carousel .carousel-control-next {
    opacity: unset;
    top: 98%;
    left: 60%;
  }

  .video-carousel .carousel-control-next-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='white' viewBox='0 0 8 8'%3e%3cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3e%3c/svg%3e");
  }

  .video-carousel .carousel-control-prev-icon {
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='white' viewBox='0 0 8 8'%3e%3cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3e%3c/svg%3e");
  }

  /* FONT AWESOME ICONS */
  .icons i.fab {
    display: inline-block;
    border-radius: 60px;
    box-shadow: 0px 0px 2px transparent;
    background-color: rgb(255, 152, 21);
    padding: 0.3em 0.3em;
    color: rgb(0, 22, 70);
  }

  .about i.fa {
    display: inline-block;
    border-radius: 60px;
    box-shadow: 0px 0px 2px transparent;
    background-color: rgb(255, 152, 21);
    padding: 0.3em 0.3rem;
  }

  .footer-links .nav-item a:hover {
    color: rgb(255, 152, 21) !important;
    text-decoration: none;
  }

  /* SWEET ALERT 2 */
  .swal2-icon.swal2-info {
    border-color: rgb(0, 22, 70) !important;
    color: rgb(0, 22, 70) !important;
  }

  .swal2-icon.swal2-success .swal2-success-ring {
    border-color: rgb(255, 152, 21) !important;
  }

  .swal2-icon.swal2-success [class^="swal2-success-line"] {
    background-color: rgb(255, 152, 21) !important;
  }


  /* MEDIA QUERIES */
  @media only screen and (min-width: 992px) {
    /* .oadh-lg-padding-30 { 
      padding: 30px !important;
    } */

    .oadh-lg-py-5 {
      padding-top: 3rem !important;
      padding-bottom: 3rem !important;
    }

    /* .oadh-mr-5 {
      margin-right: 3rem !important;
    } */

    .oadh-pr-5 {
      padding-right: 3rem !important;
    }

    .oadh-font-size-small {
      padding-left: 5rem;
    }

    .oadh-position-absolute {
        position: absolute !important;
      }

  }

  @media only screen and (max-width: 768px) {
    .oadh-sm-d-none {
      display: none;
    }

    #homeCarousel .carousel-control-next-icon, #homeCarousel .carousel-control-prev-icon {
    width: 50px !important;
    height: 50px !important;
    }

    .oadh-lg-py-2 {
      padding-top: 2rem !important;
      padding-bottom: 2rem !important;
    }
  }

  @media only screen and (min-width: 768px) and (max-width: 991.98px)
  {
    .oadh-lg-py-2 {
      padding-top: 2rem !important;
      padding-bottom: 2rem !important;
    }
  }

  @media only screen and (min-width: 577px) and (max-width: 768px)
  {
    .oadh-font-size-small {
      font-size: 15px;
      /* padding-left: 5rem; */
    }
  }

  @media only screen and (max-width: 576px) {
    .oadh-font-size-small {
      font-size: 25px;
    }

    .oadh-mb-sm-2 {
      margin-bottom: 0.5rem !important;
    }

    #activitiesCarousel .carousel-control-next-icon,
    #activitiesCarousel .carousel-control-prev-icon, 
    .activity-gallery .carousel-control-next-icon, 
    .activity-gallery .carousel-control-prev-icon,
    .video-carousel .carousel-control-next-icon,  
    .video-carousel .carousel-control-prev-icon, {
    width: 30px !important;
    height: 30px !important;
    }

    #activitiesCarousel .carousel-control-prev {
      top: 100%;
      left: 0%;
    }

    #activitiesCarousel .carousel-control-next {
      top: 100%;
      right: 0%;
    }

  }

  @media only screen and (min-width: 972px) and (max-width: 1199px)
  {
    .oadh-max-height {
      max-height: 358.94px;
    }

    .homemap {
    width: 100%;
    height: 358.94px;
    }

  .oadh-pr-1 {
    padding-right: .25 !important;
    }
  }

  @media only screen and (min-width: 992px) and (max-width: 1199px)
  {
    .oadh-font {
      font-size: 1rem !important;
      padding-left: 1.5rem !important;
    }
  }

  @media only screen and (min-width: 1200px)
  {
    .oadh-max-height {
      max-height: 392px;
    }
  }

</style>
