@extends('decima-oadh::front-end/base')

@section('mobile-menu')
<div id="left-rights-on-mobile-menu">
	<ul class="my-3" id="nav-mobile-list" role="tabList">
		<li>
				<a id="op-m-1" href="#" data-link-id="op-1">{!! Config::get('system-lang.' . $lang . '.integridadop1') !!}</a>
		</li>
		<li>
				<a id="op-m-2" href="#" data-link-id="op-2">{!! Config::get('system-lang.' . $lang . '.integridadop2') !!}</a>
		</li>
		<li>
				<a id="op-m-3" href="#" data-link-id="op-3">{!! Config::get('system-lang.' . $lang . '.integridadop3') !!}</a>
		</li>
		<li>
			<span>{!! Config::get('system-lang.' . $lang . '.integridadop4abc') !!}</span>
			<ul>
				<li>
					<a id="op-m-4" href="#" data-link-id="op-4">{!! Config::get('system-lang.' . $lang . '.integridadop4') !!}</a>
				</li>
				<li>
					<a id="op-m-5" href="#" data-link-id="op-5">{!! Config::get('system-lang.' . $lang . '.integridadop5') !!}</a>
				</li>
				<li>
					<a id="op-m-6" href="#" data-link-id="op-6">{!! Config::get('system-lang.' . $lang . '.integridadop6') !!}</a>
				</li>
			</ul>
		</li>
		<li>
			<a id="op-m-7" href="#" data-link-id="op-7">{!! Config::get('system-lang.' . $lang . '.integridadop7') !!}</a>
		</li>
		<li >
			<span>{!! Config::get('system-lang.' . $lang . '.integridadop8abc') !!}</span>
			<ul>
					<li>
						<a id="op-m-8" href="#" data-link-id="op-8">{!! Config::get('system-lang.' . $lang . '.integridadop6') !!}</a>
					</li>
					<li>
						<a id="op-m-9" href="#" data-link-id="op-9">{!! Config::get('system-lang.' . $lang . '.integridadop5') !!}</a>
					</li>
					<li>
						<a id="op-m-10" href="#" data-link-id="op-10">{!! Config::get('system-lang.' . $lang . '.integridadop10') !!}</a>
					</li>
			</ul>
		</li>
		<li>
			<span>{!! Config::get('system-lang.' . $lang . '.integridadop11abc') !!}</span>
			<ul>
				<li>
					<a id="op-m-12" href="#" data-link-id="op-12">{!! Config::get('system-lang.' . $lang . '.integridadop4') !!}</a>
				</li>
				<li>
					<a id="op-m-13" href="#" data-link-id="op-13">{!! Config::get('system-lang.' . $lang . '.integridadop6') !!}</a>
				</li>
				<li>
					<a id="op-m-11" href="#" data-link-id="op-11">{!! Config::get('system-lang.' . $lang . '.integridadop5') !!}</a>
				</li>
			</ul>
		</li>
		<li>
			<span>{!! Config::get('system-lang.' . $lang . '.integridadop14abc') !!}</span>
			<ul>
				<li>
					<a id="op-m-14" href="#" data-link-id="op-14">{!! Config::get('system-lang.' . $lang . '.integridadop14') !!}</a>
				</li>
				<li>
					<a id="op-m-15" href="#" data-link-id="op-15">{!! Config::get('system-lang.' . $lang . '.integridadop15') !!}</a>
				</li>
				<li>
					<a id="op-m-16" href="#" data-link-id="op-16">{!! Config::get('system-lang.' . $lang . '.integridadop16') !!}</a>
				</li>
			</ul>
		</li>
		<li>
			<a id="op-m-17" href="#" data-link-id="op-17">{!! Config::get('system-lang.' . $lang . '.integridadop17') !!}</a>
		</li>
		<li>
			<a id="op-m-18" href="#" data-link-id="op-18">{!! Config::get('system-lang.' . $lang . '.integridadop18') !!}</a>
		</li>
	</ul>
</div>
@endsection

@section('container')
<div class="container">
	<div class="row my-4">
		<div id="sidebar" class="col-md-3 d-none d-lg-block d-md-none d-sm-none">
			<h4 class="menu-title main-title">{!! Config::get('system-lang.' . $lang . '.derechointegridad') !!}</h4>
			<ul class="nav flex-column flex-nowrap" id="nav-option-list" role="tabList">
				<li class="nav-item opt-list-item">
					<a
						class="nav-link"
						href="#tab-content-1"
						id="op-1"
						role="tab"
						aria-controls="tab-content-1"
						aria-selected="true"
						data-tab-number="1"
						data-div-id="mapdiv-1"
						data-div-filter-id="mapdiv-filter-1"
						data-div-reference-id="mapdiv-reference-1"
						data-context="crimes-victims"
						data-filters='[
							{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
						]'
						data-description-key="I001"
						data-reference-key="I019"
						data-description="{{ $keyValues['I001'] }}"
						data-chart-type = 'bar'
						data-reference="{{ $keyValues['I019'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop1') !!}
				<li class="nav-item opt-list-item">
					<a
						class="nav-link"
						href="#tab-content-2"
						id="op-2"
						role="tab"
						aria-controls="tab-content-2"
						aria-selected="true"
						data-tab-number="2"
						data-div-id="mapdiv-2"
						data-div-filter-id="mapdiv-filter-2"
						data-div-reference-id="mapdiv-reference-2"
						data-context="crimes-accused"
						data-filters='[
							{ "filter": "category", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.categoria') !!}", "multiple": true },
							{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
						]'
						data-description-key="I002"
						data-reference-key="I020"
						data-description="{{ $keyValues['I002'] }}"
						data-chart-type = 'bar'
						data-reference="{{ $keyValues['I020'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop2') !!}</a>
				</li>
				<li class="nav-item opt-list-item">
					<a
						class="nav-link"
						href="#tab-content-3"
						id="op-3"
						role="tab"
						aria-controls="tab-content-3"
						aria-selected="true"
						data-tab-number="3"
						data-div-id="mapdiv-3"
						data-div-filter-id="mapdiv-filter-3"
						data-div-reference-id="mapdiv-reference-3"
						data-context="working-officials"
						data-filters='[
							{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
							{ "filter": "category", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.categoria') !!}", "multiple": true },
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true }
						]'
						data-description-key="I003"
						data-reference-key="I021"
						data-description="{{ $keyValues['I003'] }}"
						data-chart-type = 'bar'
						data-reference="{{ $keyValues['I021'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop3') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link collapsed"
						role="collapse"
						id="tperson-list-btn-collapse"
						data-toggle="collapse"
						href="#tperson-list"
						aria-expanded="false"
						aria-controls="tperson-list"><i class="fas fa-arrows-alt-v"></i> {!! Config::get('system-lang.' . $lang . '.integridadop4abc') !!}</a>

					<div class="collapse" id="tperson-list" aria-expanded="false">
						<ul class="flex-column pl-2 nav" role="tabList">
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-4"
									id="op-4"
									role="tab"
									aria-controls="tab-content-4"
									aria-selected="false"
									data-tab-number="4"
									data-div-id="mapdiv-4"
									data-div-filter-id="mapdiv-filter-4"
									data-div-reference-id="mapdiv-reference-4"
									data-context="gender-trafficking"
												data-filters='[
										{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true }
										]'
									data-description-key="I004"
									data-reference-key="I022"
									data-description="{{ $keyValues['I004'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I022'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop4') !!}</a>
							</li>
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-5"
									id="op-5"
									role="tab"
									aria-controls="tab-content-5"
									aria-selected="false"
									data-tab-number="5"
									data-div-id="mapdiv-5"
									data-div-filter-id="mapdiv-filter-5"
									data-div-reference-id="mapdiv-reference-5"
									data-context="type-trafficking"
									data-filters='[
										{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true }
									]'
									data-description-key="I005"
									data-reference-key="I023"
									data-description="{{ $keyValues['I005'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I023'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop5') !!}</a>
							</li>
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-6"
									id="op-6"
									role="tab"
									aria-controls="tab-content-6"
									aria-selected="false"
									data-tab-number="6"
									data-div-id="mapdiv-6"
									data-div-filter-id="mapdiv-filter-6"
									data-div-reference-id="mapdiv-reference-6"
									data-context="age-trafficking"
									data-filters='[
										{ "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
									]'
									data-description-key="I006"
									data-reference-key="I024"
									data-description="{{ $keyValues['I006'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I024'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop6') !!}</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item opt-list-item">
					<a
						class="nav-link "
						href="#tab-content-7"
						id="op-7"
						role="tab"
						aria-controls="tab-content-7"
						aria-selected="false"
						data-tab-number="7"
						data-div-id="mapdiv-7"
						data-div-filter-id="mapdiv-filter-7"
						data-div-reference-id="mapdiv-reference-7"
						data-context="movement-freedom"
						data-filters='[
							{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
							{ "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true }
						]'
						data-description-key="I007"
						data-reference-key="I025"
						data-description="{{ $keyValues['I007'] }}"
						data-chart-type = 'bar'
						data-reference="{{ $keyValues['I025'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop7') !!}</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link collapsed"
						role="collapse"
						id="utem-list-btn-collapse"
						data-toggle="collapse"
						href="#utem-list"
						aria-expanded="false"
						aria-controls="utem-list"><i class="fas fa-arrows-alt-v"></i> {!! Config::get('system-lang.' . $lang . '.integridadop8abc') !!}</a>

					<div class="collapse" id="utem-list">
						<ul class="flex-column pl-2 nav" role="tabList">
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-8"
									id="op-8"
									role="tab"
									aria-controls="tab-content-8"
									aria-selected="false"
									data-tab-number="8"
									data-div-id="mapdiv-8"
									data-div-filter-id="mapdiv-filter-8"
									data-div-reference-id="mapdiv-reference-8"
									data-context="age-women"
									data-filters='[
										{ "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
									]'
									data-description-key="I008"
									data-reference-key="I026"
									data-description="{{ $keyValues['I008'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I026'] }}"><i class="fas fa-chart-bar"></i>{!! Config::get('system-lang.' . $lang . '.integridadop6') !!}</a>
							</li>
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-9"
									id="op-9"
									role="tab"
									aria-controls="tab-content-9"
									data-div-id="mapdiv-9"
									data-div-filter-id="mapdiv-filter-9"
									data-div-reference-id="mapdiv-reference-9"
									aria-selected="false"
									data-tab-number="9"
									data-context="type-women"
									data-filters='[
										{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true }
									]'
									data-description-key="I009"
									data-reference-key="I027"
									data-description="{{ $keyValues['I009'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I027'] }}"><i class="fas fa-chart-bar"></i>{!! Config::get('system-lang.' . $lang . '.integridadop5') !!}</a>
							</li>
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-10"
									id="op-10"
									role="tab"
									aria-controls="tab-content-10"
									aria-selected="false"
									data-tab-number="10"
									data-div-id="mapdiv-10"
									data-div-filter-id="mapdiv-filter-10"
									data-div-reference-id="mapdiv-reference-10"
									data-context="state-women"
									data-filters='[
										{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
										{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
									]'
									data-description-key="I010"
									data-reference-key="I028"
									data-description="{{ $keyValues['I010'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I028'] }}"><i class="fas fa-chart-bar"></i>{!! Config::get('system-lang.' . $lang . '.integridadop10') !!}</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item">
					<a
						class="nav-link collapsed"
						role="collapse"
						id="didacl-list-btn-collapse"
						data-toggle="collapse"
						href="#didacl-list"
						aria-expanded="false"
						aria-controls="didacl-list"><i class="fas fa-arrows-alt-v"></i> {!! Config::get('system-lang.' . $lang . '.integridadop11abc') !!}</a>

					<div class="collapse" id="didacl-list">
						<ul class="flex-column pl-2 nav" role="tabList">
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-11"
									id="op-11"
									role="tab"
									aria-controls="tab-content-11"
									aria-selected="false"
									data-tab-number="11"
									data-div-id="mapdiv-11"
									data-div-filter-id="mapdiv-filter-11"
									data-div-reference-id="mapdiv-reference-11"
									data-context="type-sexual"
												data-filters='[
										{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
										{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
										{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
									]'
									data-description-key="I011"
									data-reference-key="I029"
									data-description="{{ $keyValues['I011'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I029'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop5') !!}</a>
							</li>
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-12"
									id="op-12"
									role="tab"
									aria-controls="tab-content-12"
									aria-selected="false"
									data-tab-number="12"
									data-div-id="mapdiv-12"
									data-div-filter-id="mapdiv-filter-12"
									data-div-reference-id="mapdiv-reference-12"
									data-context="gender-sexual"
												data-filters='[
										{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
										{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true }
									]'
									data-description-key="I012"
									data-reference-key="I030"
									data-description="{{ $keyValues['I012'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I030'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop4') !!}</a>
							</li>
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-13"
									id="op-13"
									role="tab"
									aria-controls="tab-content-13"
									aria-selected="false"
									data-tab-number="13"
									data-div-id="mapdiv-13"
									data-div-filter-id="mapdiv-filter-13"
									data-div-reference-id="mapdiv-reference-13"
									data-context="age-sexual"
												data-filters='[
										{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
										{ "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
									]'
									data-description-key="I013"
									data-reference-key="I031"
									data-description="{{ $keyValues['I013'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I031'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop6') !!}</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link collapsed" role="collapse" id="ardl-list-btn-collapse" data-toggle="collapse"
						href="#ardl-list" aria-expanded="false" aria-controls="ardl-list"><i class="fas fa-arrows-alt-v"></i> {!! Config::get('system-lang.' . $lang . '.integridadop14abc') !!}</a>

					<div class="collapse" id="ardl-list">
						<ul class="flex-column pl-2 nav" role="tabList">
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-14"
									id="op-14"
									role="tab"
									aria-controls="tab-content-14"
									data-div-id="mapdiv-14"
									data-div-filter-id="mapdiv-filter-14"
									data-div-reference-id="mapdiv-reference-14"aria-selected="false" data-tab-number="14"
									data-context="state-injuries"
									data-description-key="I014"
									data-filters='[
										{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
										{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true }
									]'
									data-reference-key="I032"
									data-description="{{ $keyValues['I014'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I032'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop14') !!}</a>
							</li>
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-15"
									id="op-15"
									role="tab"
									aria-controls="tab-content-15"
									aria-selected="false"
									data-tab-number="15"
									data-div-id="mapdiv-15"
									data-div-filter-id="mapdiv-filter-15"
									data-div-reference-id="mapdiv-reference-15"
									data-context="age-injuries"
									data-filters='[
										{ "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
										{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.tipodelito') !!}", "multiple": true }
									]'
									data-description-key="I015"
									data-reference-key="I033"
									data-description="{{ $keyValues['I015'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I033'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop15') !!}</a>
							</li>
							<li class="nav-item pt-2">
								<a
									class="nav-link py-0"
									href="#tab-content-16"
									id="op-16"
									role="tab"
									aria-controls="tab-content-16"
									aria-selected="false"
									data-tab-number="16"
									data-div-id="mapdiv-16"
									data-div-filter-id="mapdiv-filter-16"
									data-div-reference-id="mapdiv-reference-16"
									data-context="schedule-injuries"
									data-filters='[
										{ "filter": "weaponType", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}", "multiple": true }
										]'
									data-description-key="I016"
									data-reference-key="I034"
									data-description="{{ $keyValues['I016'] }}"
									data-chart-type = 'bar'
									data-reference="{{ $keyValues['I034'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop16') !!}</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item opt-list-item">
					<a
						class="nav-link "
						href="#tab-content-17"
						id="op-17"
						role="tab"
						aria-controls="tab-content-17"
						aria-selected="false"
						data-tab-number="17"
						data-div-id="mapdiv-17"
						data-div-filter-id="mapdiv-filter-17"
						data-div-reference-id="mapdiv-reference-17"
						data-context="agent-investigation"
						data-filters='[
							{ "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
							{ "filter": "category", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.categoria') !!}", "multiple": true },
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "place", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.lugar') !!}", "multiple": true },
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
							{ "filter": "state", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.estado') !!}", "multiple": true }
						]'
						data-description-key="I017"
						data-reference-key="I035"
						data-description="{{ $keyValues['I017'] }}"
						data-chart-type = 'bar'
						data-reference="{{ $keyValues['I035'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop17') !!}</a>
				</li>
				<li class="nav-item opt-list-item">
					<a
						class="nav-link "
						href="#tab-content-18"
						id="op-18"
						role="tab"
						aria-controls="tab-content-18"
						aria-selected="false"
						data-tab-number="18"
						data-div-id="mapdiv-18"
						data-div-filter-id="mapdiv-filter-18"
						data-div-reference-id="mapdiv-reference-18"
						data-context="integrity-violation"
						data-filters='[
							{ "filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}", "multiple": true },
							{ "filter": "age", "type": "ageRange", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": false },
							{ "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
							{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
							{ "filter": "violatedFact", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.hecho') }}", "multiple": true },
							{ "filter": "dependencies", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.institucion') }}", "multiple": true }
						]'
						data-description-key="I018"
						data-reference-key="I036"
						data-description="{{ $keyValues['I018'] }}"
						data-chart-type = 'bar'
						data-reference="{{ $keyValues['I036'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.integridadop18') !!}</a>
				</li>
			</ul>
		</div>
		<div id="mobile-menu" class="col-12 text-left d-sm-block d-md-block d-lg-none">
			<h4 class="menu-title">{!! Config::get('system-lang.' . $lang . '.derechointegridad') !!}</h4>
			<div class="text-center">
				<button type="button" id="btn-mmenu" class="btn btn-secondary oadh-bg-primary-color btn-lg w-100 my-3"><i class="fas fa-bars"></i> {!! Config::get('system-lang.' . $lang . '.veropciones') !!}</button>
			</div>
		</div>
		<div class="col-lg-9">
			<h4 class="oadh-text-primary-color text-center" id="oadh-chart-title"></h4>
			<div class="card card-filter">
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-sm-4">
							{!! Config::get('system-lang.' . $lang . '.disponibles') !!}
						</div>
						<div class="col-sm-8 text-right">
              <div class="btn-toolbar justify-content-end" role="toolbar" aria-label="accesibility buttons">
                <div class="btn-group" role="group" aria-label="filters">
                  <button type="button" id="btn-download-xlsx" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-arrow-down"></i> {{ Config::get('system-lang.' . $lang . '.descargar') }}
                  </button>
                </div>
                <div class="btn-group ml-2" role="group" aria-label="filters">
                  <button type="button" id="btn-filter" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-filter"></i> {!! Config::get('system-lang.' . $lang . '.aplicar') !!}
                  </button>
                </div>
              </div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="mapdiv-filter-1" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-2" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-3" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-4" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-5" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-6" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-7" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-8" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-9" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-10" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-11" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-12" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-13" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-14" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-15" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-16" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-17" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-18" style="display: none;" class="mapdiv-filter"></div>
					<p id="filter-description" data-key='' class="editable-element"></p>
				</div>
			</div>
			<div class="row align-items-center rights-description">
				<div class="col-lg-6">
					{{-- <img src="{{ URL::asset('oadh/inicios/integridad-v3.png') }}" alt="logo" class="d-block w-100 img-fluid mx-auto"> --}}
					<div class="home-rights-container oadh-bg-primary-color">
						<div class="d-flex justify-content-center">
							<div class="rounded-circle home-rights-circle">
								<img src="{{ URL::asset('oadh/inicios/integridad-v4.png') }}" alt="vida" class="img-fluid">
							</div>
						</div>
						<div class="mt-3">
							<h3 class="oadh-text-white-color text-center">
								{{ Config::get('system-lang.' . $lang . '.derechointegridad') }}
							</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<h3 class="oadh-text-primary-color editable-element" data-key="I037">{{ $keyValues['I037'] }}</h3>
					<p class="text-justify editable-element" data-key="I038">
						{!! $keyValues['I038'] !!}
					</p>
					<p class="text-justify editable-element" data-key="I039">
						{!! $keyValues['I039'] !!}
					</p>
				</div>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-1" class="chartdiv"></div>
				<p id="mapdiv-reference-1" class="editable-element"></p>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-2" class="chartdiv"></div>
				<p id="mapdiv-reference-2" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-3" class="chartdiv"></div>
				<p id="mapdiv-reference-3" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-4" class="chartdiv"></div>
				<p id="mapdiv-reference-4" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-5" class="chartdiv"></div>
				<p id="mapdiv-reference-5" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-6" class="chartdiv"></div>
				<p id="mapdiv-reference-6" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-7" class="chartdiv"></div>
				<p id="mapdiv-reference-7" class="editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-8" role="tabpanel" aria-labelledby="tc-8">
				<div id="mapdiv-8" class="chartdiv"></div>
				<p id="mapdiv-reference-8" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-9" role="tabpanel" aria-labelledby="tc-9">
				<div id="mapdiv-9" class="chartdiv"></div>
				<p id="mapdiv-reference-9" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-10" role="tabpanel" aria-labelledby="tc-10">
				<div id="mapdiv-10" class="chartdiv"></div>
				<p id="mapdiv-reference-10" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-11" role="tabpanel" aria-labelledby="tc-11">
				<div id="mapdiv-11" class="chartdiv"></div>
				<p id="mapdiv-reference-11" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-12" role="tabpanel" aria-labelledby="tc-12">
				<div id="mapdiv-12" class="chartdiv"></div>
				<p id="mapdiv-reference-12" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-13" role="tabpanel" aria-labelledby="tc-13">
				<div id="mapdiv-13" class="chartdiv"></div>
				<p id="mapdiv-reference-13" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-14" role="tabpanel" aria-labelledby="tc-14">
				<div id="mapdiv-14" class="chartdiv"></div>
				<p id="mapdiv-reference-14" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-15" role="tabpanel" aria-labelledby="tc-15">
				<div id="mapdiv-15" class="chartdiv"></div>
				<p id="mapdiv-reference-15" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-16" role="tabpanel" aria-labelledby="tc-16">
				<div id="mapdiv-16" class="chartdiv"></div>
				<p id="mapdiv-reference-16" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-17" role="tabpanel" aria-labelledby="tc-17">
				<div id="mapdiv-17" class="chartdiv"></div>
				<p id="mapdiv-reference-17" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-18" role="tabpanel" aria-labelledby="tc-18">
				<div id="mapdiv-18" class="chartdiv"></div>
				<p id="mapdiv-reference-18" class="reference-text editable-element"></p>
			</div>
		</div>
	</div>
</div>
{!! Form::open(array('id' => 'oadh-xlsx-download-form', 'url' => URL::to('/cms/derechos/download-data-xlsx'), 'role'  =>  'form', 'class' => 'form-horizontal')) !!}
  {!! Form::hidden('oadh-xlsx-data', null, ['id' => 'oadh-xlsx-data', 'data-details' => '[]']) !!}
  {!! Form::hidden('oadh-xlsx-data-titles', null, ['id' => 'oadh-xlsx-data-titles', 'data-details-titles' => '[]']) !!}
{!! Form::close() !!}
@parent
@stop
