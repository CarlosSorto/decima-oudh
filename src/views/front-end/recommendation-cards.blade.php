@foreach ($recs as $rec)
<div class="col-sm-6 my-3 d-flex align-items-stretch">
  <div class="card shadow-sm">
    <div class="card-body">
      <div class="card-title">
        <strong class="d-inline-block mb-2 oadh-text-primary-color">{{$rec->system}}</strong>
      </div>
      <h3 class="mb-0 clickable"><a href="{{url('cms/recomendaciones/fichas', $rec->id)}}">{{$rec->mechanism}}</a></h3>
      <div class="mb-1 text-muted">{{Carbon\Carbon::createFromFormat('Y-m-d', $rec->date)->format(Lang::get('form.phpShortDateFormat'))}}</div>
      <div class="card-text-mb-auto text-justify clickable"><a href="{{url('cms/recomendaciones/fichas', $rec->id)}}">{{substr($rec->recommendation, 0, 300) . '...'}}</a></div>
      <div class="row">
        <div class="col mb-2">
          @foreach ($rec->institutions as $inst)
          <span class="badge badge-pill badge-primary">{{$inst->abbreviation}}</span>
          @endforeach
        </div>
      </div>
      <a href="{{ url('cms/recomendaciones/fichas', $rec->id) . '?lang=' . $lang }}"
      class="oadh-text-primary-color">{!! Config::get('system-lang.' . $lang . '.verrecomendacion') !!}</a>
      <button type="button" class="btn btn-sm oadh-bg-secondary-color oadh-outline-none oadh-text-white-color oadh-rdt-single-download float-right" data-id="{{$rec->id}}" type="button">{{ Config::get('system-lang.' . $lang . '.descargar') }}</button>
    </div>
  </div>
</div>
@endforeach
<div class="col-sm-12 justify-content-center">
  @include('decima-oadh::front-end/pagination', ['paginator' => $recs])
</div>