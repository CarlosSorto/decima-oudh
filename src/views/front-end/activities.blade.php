@extends('decima-oadh::front-end/base')
@section('container')
<script>
  $(document).ready(function(){
    $('.carousel').carousel({
      interval: false
    });

    $('#activitiesCarousel').on('slide.bs.carousel', function(event)
    {
      $('.activity-gallery').hide();

      $($(event.relatedTarget).attr('data-gallery-id')).fadeIn(250);
    });
  });
</script>

<!-- CONTAINER -->
<div class="container my-4">
  <!-- CARROUSEL PRINCIPAL -->
  @if (!empty($activity['activities']))
  <div class="row">
    <div class="col-sm-12">
      <div id="activitiesCarousel" class="carousel slide activity-carousel" data-ride="carousel">
        <div class="carousel-inner">
          @php
          $count = 0
          @endphp
          @foreach ($activity['activities'] as $key => $activities)
          <div class="carousel-item {{($count == 0) ? 'active' : ''}}" data-gallery-id="#gallery-{{$key}}">
            <div class="row">
              <div class="col-sm-8">
                  <img class="w-100 rounded img-fluid" src="{{$activities['image_url'] }}" alt="First slide">
              </div>
              <div class="col-sm-4">
                  <p class="font-weight-bold title-entry-text">{{$activities['title'] }}</p>
                  <p class="text-justify entry-text">{{$activities['description'] }}</p>
              </div>
            </div>
          </div>
          @php
          $count++
          @endphp
          @endforeach
        </div>
        <div class="carousel-controls">
          <a class="carousel-control-prev" href="#activitiesCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#activitiesCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
      </div>
      </div>
    </div>
    @endif
  </div>
  <!-- /CARROUSEL PRINCIPAL -->

  <!-- CARROUSEL DE GALERIAS -->
@if (!empty($activity['galleries']))
@php
  $count2 = 0
@endphp
  @foreach ($activity['galleries'] as $key => $galleries)
    <div id="gallery-{{$key}}" class="row mt-3 activity-gallery" {{($count2 != 0) ? 'style=display:none;' : ''}}>
      <div class="col-sm-12">
        <div class="carousel slide gallery-carousel carousel-fade" data-ride="carousel" id="multi-carousel-{{$key}}">
          <div class="carousel-inner">
            @php
              $count3 = 0
            @endphp
            @foreach ($galleries as $item)
              @if ($count3 == 0)
                <div class="carousel-item p-2 active">
                  <div class="row">
              @endif
              @if ($count3 > 0 && $count3 % 3 == 0)
                  </div>
                </div>
                <div class="carousel-item p-2">
                  <div class="row">
              @endif
              <div class="col-sm-4 oadh-lg-py-2">
                <div class="card card-shadow text-center w-100">
                  <div class="card-body">
                    <div class="m-auto">
                      <img class="d-block w-100 img-fluid rounded" src="{{$item['image_url'] }}" alt="imagen">
                    </div>
                    <p class="card-text mt-3 font-weight-bold">{{$item['description'] }}</p>
                  </div>
                </div>
              </div>
              @php
                $count3++
              @endphp
            @endforeach
            @if ($count3 > 0 && $count3 % 3 != 0)
                </div>
              </div>
            @else
                </div>
              </div>
            @endif
          </div>
          <a class="carousel-control-prev" href="#multi-carousel-{{$key}}" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#multi-carousel-{{$key}}" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
    @php
    $count2++
  @endphp
  @endforeach
@endif
  <!-- /CARROUSEL DE GALERIAS -->

</div>
<!-- CONTAINER -->


@parent
@stop
