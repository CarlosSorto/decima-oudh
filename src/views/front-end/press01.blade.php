@extends('decima-oadh::front-end/base')

@section('javascript')
    <script type="text/javascript">
      
    </script>
@endsection


@section('container')

<style>
  
</style>


<div class="container my-4">
  <div class="row">
    {{-- <div class="col-md-4">
      <img src="https://via.placeholder.com/400x1255" class="d-block w-100 img-fluid rounded mx-auto">
    </div> --}}
    {{-- <div class="col-md-8"> --}}
    <div class="col-md-12">
      
      <div class="oadh-bg-primary-color rounded">
        <div class="row">
          <div class="col-md-6 press-section-padding">
            <h3 class="oadh-text-secondary-color text-center editable-element" data-key="N003">{!! $keyValues['N003'] !!}</h3>
            <p class="oadh-text-white-color text-justify editable-element" data-key="N004">{!! $keyValues['N004'] !!}</p>
          </div>
          <div class="col-md-6 press-section-padding">
            <h3 class="oadh-text-secondary-color text-center editable-element" data-key="N005">{!! $keyValues['N005'] !!}</h3>
            <p class="oadh-text-white-color text-justify editable-element" data-key="N006">{!! $keyValues['N006'] !!}</p>
          </div>
          <div class="col-md-6 press-section-padding">
            <h3 class="oadh-text-secondary-color text-center editable-element" data-key="N007">{!! $keyValues['N007'] !!}</h3>
            <p class="oadh-text-white-color text-justify editable-element" data-key="N008">{!! $keyValues['N008'] !!}</p>
          </div>
          <div class="col-md-6 press-section-padding">
            <h3 class="oadh-text-secondary-color text-center editable-element" data-key="N009">{!! $keyValues['N009'] !!}</h3>
            <p class="oadh-text-white-color text-justify editable-element" data-key="N010">{!! $keyValues['N010'] !!}</p>
          </div>
        </div>
      </div>
      
      <div class="oadh-bg-secondary-color rounded mt-4">
        <div class="row">
          <div class="col-md-4 press-section-padding">
            <a class="oadh-li-video-tutorial oadh-fake-link oadh-press-menu-link nav-link-press01 nav-link-press-font-size oadh-text-white-color" data-tab-class="oadh-video-tab-tut-01">
              <i class="fab fa-youtube"></i> {{ Config::get('system-lang.' . $lang . '.tuto01name') }}
            </a>
            <a class="oadh-li-video-tutorial oadh-fake-link oadh-press-menu-link nav-link-press01 nav-link-press-font-size oadh-text-white-color" data-tab-class="oadh-video-tab-tut-02">
              <i class="fab fa-youtube"></i> {{ Config::get('system-lang.' . $lang . '.tuto02name') }}
            </a>
            <a class="oadh-li-video-tutorial oadh-fake-link oadh-press-menu-link nav-link-press01 nav-link-press-font-size oadh-text-white-color" data-tab-class="oadh-video-tab-tut-03">
              <i class="fab fa-youtube"></i> {{ Config::get('system-lang.' . $lang . '.tuto03name') }}
            </a>
            @if (!empty($videos['highlighted']))
              @foreach ($videos['highlighted'] as $gallery)
                <a class="oadh-li-video-tutorial oadh-fake-link oadh-press-menu-link nav-link-press01 nav-link-press-font-size oadh-text-white-color" data-tab-class="oadh-video-tab-{{ $gallery['id'] }}">
                  <i class="fab fa-youtube"></i> {{ $gallery['title'] }}
                </a>
              @endforeach
            @endif
          </div>
          <div class="col-md-8 press-section-padding">
            <h3 class="oadh-text-white-color text-center editable-element" data-key="N012">{!! $keyValues['N012'] !!}</h3>
            <p class="oadh-video-tab oadh-text-white-color text-justify editable-element" data-key="N013">{!! $keyValues['N013'] !!}</p>

            <p class="oadh-video-tab oadh-video-tab-tut-01 oadh-text-white-color text-justify" style="display: none;">{{ Config::get('system-lang.' . $lang . '.tuto01desc') }}</p>
            <div class="oadh-video-tab oadh-video-tab-tut-01 embed-responsive embed-responsive-16by9 mt-4" style="display: none;">
              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Dr6Ov4_DSv0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

            <p class="oadh-video-tab oadh-video-tab-tut-02 oadh-text-white-color text-justify" style="display: none;">{{ Config::get('system-lang.' . $lang . '.tuto02desc') }}</p>
            <div class="oadh-video-tab oadh-video-tab-tut-02 embed-responsive embed-responsive-16by9 mt-4" style="display: none;">
              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/4igf7JhGkqM" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

            <p class="oadh-video-tab oadh-video-tab-tut-03 oadh-text-white-color text-justify" style="display: none;">{{ Config::get('system-lang.' . $lang . '.tuto03desc') }}</p>
            <div class="oadh-video-tab oadh-video-tab-tut-03 embed-responsive embed-responsive-16by9 mt-4" style="display: none;">
              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/TJ_e-UWJk70" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

            @if (!empty($videos['highlighted']))
              @foreach ($videos['highlighted'] as $gallery)
                <p class="oadh-video-tab oadh-video-tab-{{ $gallery['id'] }} oadh-text-white-color text-justify" style="display: none;">{{ $gallery['description'] }}</p>
                <div class="oadh-video-tab oadh-video-tab-{{ $gallery['id'] }} embed-responsive embed-responsive-16by9 mt-4" style="display: none;">
                  <iframe class="embed-responsive-item" src="{{ $gallery['image_url'] }}" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              @endforeach
            @endif

          </div>
          
        </div>
      </div>

    </div>
  </div>
  
</div>
@parent
@stop