@extends('decima-oadh::front-end/base')

@section('mobile-menu')
<div id="left-rights-on-mobile-menu">
	<ul class="my-3" id="nav-mobile-list" role="tabList">
		<li>
			<a id="op-m-1" href="#" data-link-id="op-1">{!! Config::get('system-lang.' . $lang . '.justiciaop1') !!}</a>
		</li>
		<li>
			<a id="op-m-2" href="#" data-link-id="op-2">{!! Config::get('system-lang.' . $lang . '.justiciaop2') !!}</a>
		</li>
		<li>
			<a id="op-m-3" href="#" data-link-id="op-3">{!! Config::get('system-lang.' . $lang . '.justiciaop3') !!}</a>
		</li>
		<li >
			<a id="op-m-4" href="#" data-link-id="op-4">{!! Config::get('system-lang.' . $lang . '.justiciaop4') !!}</a>
		</li>
		<li >
			<a id="op-m-5" href="#" data-link-id="op-5">{!! Config::get('system-lang.' . $lang . '.justiciaop5') !!}</a>
		</li>
		<li >
			<a id="op-m-6" href="#" data-link-id="op-6">{!! Config::get('system-lang.' . $lang . '.justiciaop6') !!}</a>
		</li>
		<li >
			<a id="op-m-7" href="#" data-link-id="op-7">{!! Config::get('system-lang.' . $lang . '.justiciaop7') !!}</a>
		</li>
		<li>
			<a id="op-m-8" href="#" data-link-id="op-8">{!! Config::get('system-lang.' . $lang . '.justiciaop8') !!}</a>
		</li>
		<li>
			<a id="op-m-9" href="#" data-link-id="op-9">{!! Config::get('system-lang.' . $lang . '.justiciaop9') !!}</a>
		</li>
		<li >
			<a id="op-m-10" href="#" data-link-id="op-10">{!! Config::get('system-lang.' . $lang . '.justiciaop10') !!}</a>
		</li>
		<li >
			<a id="op-m-11" href="#" data-link-id="op-11">{!! Config::get('system-lang.' . $lang . '.justiciaop11') !!}</a>
		</li>
		<li>
			<a id="op-m-12" href="#" data-link-id="op-12">{!! Config::get('system-lang.' . $lang . '.justiciaop12') !!}</a>
		</li>
		<li>
			<a id="op-m-13" href="#" data-link-id="op-13">{!! Config::get('system-lang.' . $lang . '.justiciaop13') !!}</a>
		</li>
		<li>
			<a id="op-m-14" href="#" data-link-id="op-14">{!! Config::get('system-lang.' . $lang . '.justiciaop14') !!}</a>
		</li>
		<li>
			<a id="op-m-15" href="#" data-link-id="op-15">{!! Config::get('system-lang.' . $lang . '.justiciaop15') !!}</a>
		</li>
		<li>
			<a id="op-m-16" href="#" data-link-id="op-16">{!! Config::get('system-lang.' . $lang . '.justiciaop16') !!}</a>
		</li>
		<li>
			<a id="op-m-17" href="#" data-link-id="op-17">{!! Config::get('system-lang.' . $lang . '.justiciaop17') !!}</a>
		</li>
		<li>
			<a id="op-m-18" href="#" data-link-id="op-18">{!! Config::get('system-lang.' . $lang . '.justiciaop18') !!}</a>
		</li>
		<li>
			<a id="op-m-19" href="#" data-link-id="op-19">{!! Config::get('system-lang.' . $lang . '.justiciaop19') !!}</a>
		</li>
		<li>
			<a id="op-m-20" href="#" data-link-id="op-20">{!! Config::get('system-lang.' . $lang . '.justiciaop20') !!}</a>
		</li>
		<li>
			<a id="op-m-21" href="#" data-link-id="op-21">{!! Config::get('system-lang.' . $lang . '.justiciaop21') !!}</a>
		</li>
		<li>
			<a id="op-m-22" href="#" data-link-id="op-22">{!! Config::get('system-lang.' . $lang . '.justiciaop22') !!}</a>
		</li>
		<li>
			<a id="op-m-23" href="#" data-link-id="op-23">{!! Config::get('system-lang.' . $lang . '.justiciaop23') !!}</a>
		</li>
		<li>
			<a id="op-m-24" href="#" data-link-id="op-24">{!! Config::get('system-lang.' . $lang . '.justiciaop24') !!}</a>
		</li>
		<li>
			<a id="op-m-25" href="#" data-link-id="op-25">{!! Config::get('system-lang.' . $lang . '.justiciaop25') !!}</a>
		</li>
		<li>
			<a id="op-m-26" href="#" data-link-id="op-26">{!! Config::get('system-lang.' . $lang . '.justiciaop26') !!}</a>
		</li>
		<li>
			<a id="op-m-27" href="#" data-link-id="op-27">{!! Config::get('system-lang.' . $lang . '.justiciaop27') !!}</a>
		</li>
	</ul>
</div>
@endsection

@section('container')
<div class="container">
  <div class="row my-4">
    <div id="sidebar" class="col-md-3 d-none d-lg-block d-md-none d-sm-none">
      <h4 class="menu-title main-title">{!! Config::get('system-lang.' . $lang . '.derechojusticia') !!}</h4>
      <ul class="nav flex-column flex-nowrap" id="nav-option-list">
        <li class="nav-item">
          <a
          id="op-1"
          class="nav-link"
          href="#justice-chart-1"
          role="tab"
          aria-controls="justice-chart-1"
          aria-selected="true"
          data-tab-number="1"
          data-context="victims-of-domestic-violence"
          data-div-id="chartdiv-1"
          data-div-filter-id="mapdiv-filter-1"
          data-div-reference-id="mapdiv-reference-1"
          data-chart-type="line"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J001"
          data-description="
          {{ $keyValues['J001'] }}
          "
          data-reference-key="J021"
          data-reference="{{ $keyValues['J021'] }}">
            <i class="fas fa-chart-line"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop1') !!}
          </a>
          <a
          id="op-2"
          class="nav-link"
          href="#justice-chart-2"
          role="tab"
          aria-controls="justice-chart-2"
          aria-selected="true"
          data-tab-number="2"
          data-context="victims-in-leiv"
          data-div-id="chartdiv-2"
          data-div-filter-id="mapdiv-filter-2"
          data-div-reference-id="mapdiv-reference-2"
          data-chart-type="line"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J002"
          data-description="
          {{ $keyValues['J002'] }}
          "
          data-reference-key="J022"
          data-reference="{{ $keyValues['J022'] }}">
            <i class="fas fa-chart-line"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop2') !!}
          </a>
          <a
          id="op-3"
          class="nav-link"
          href="#justice-chart-3"
          role="tab"
          aria-controls="justice-chart-3"
          aria-selected="true"
          data-tab-number="3"
          data-context="victims-sexual-assault"
          data-div-id="chartdiv-3"
          data-div-filter-id="mapdiv-filter-3"
          data-div-reference-id="mapdiv-reference-3"
          data-chart-type="line"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J003"
          data-description="
          {{ $keyValues['J003'] }}
          "
          data-reference-key="J023"
          data-reference="{{ $keyValues['J023'] }}">
            <i class="fas fa-chart-line"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop3') !!}
          </a>
          <a
          id="op-4"
          class="nav-link"
          href="#justice-chart-4"
          role="tab"
          aria-controls="justice-chart-4"
          aria-selected="true"
          data-tab-number="4"
          data-context="victims-feminicides"
          data-div-id="chartdiv-4"
          data-div-filter-id="mapdiv-filter-4"
          data-div-reference-id="mapdiv-reference-4"
          data-chart-type="line"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J004"
          data-description="
          {{ $keyValues['J004'] }}
          "
          data-reference-key="J024"
          data-reference="{{ $keyValues['J024'] }}">
            <i class="fas fa-chart-line"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop4') !!}
          </a>
          <a
          id="op-5"
          class="nav-link"
          href="#justice-chart-5"
          role="tab"
          aria-controls="justice-chart-5"
          aria-selected="true"
          data-tab-number="5"
          data-context="domestic-violence-on-women-registered-by-fgr"
          data-div-id="mapdiv-5"
          data-div-filter-id="mapdiv-filter-5"
          data-div-reference-id="mapdiv-reference-5"
          data-chart-type="map"
          data-grid-id="grid-5"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J005"
          data-description="
          {{ $keyValues['J005'] }}
          "
          data-reference-key="J025"
          data-reference="{{ $keyValues['J025'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop5') !!}.
          </a>
          <a
          id="op-6"
          class="nav-link"
          href="#justice-chart-6"
          role="tab"
          aria-controls="justice-chart-6"
          aria-selected="true"
          data-tab-number="6"
          data-context="domestic-violence-on-women-processed-cases"
          data-div-id="mapdiv-6"
          data-div-filter-id="mapdiv-filter-6"
          data-div-reference-id="mapdiv-reference-6"
          data-chart-type="map"
          data-grid-id="grid-6"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J006"
          data-description="
          {{ $keyValues['J006'] }}
          "
          data-reference-key="J026"
          data-reference="{{ $keyValues['J026'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop6') !!}
          </a>
          <a
          id="op-7"
          class="nav-link"
          href="#justice-chart-7"
          role="tab"
          aria-controls="justice-chart-7"
          aria-selected="true"
          data-tab-number="7"
          data-context="domestic-violence-on-women-judgments"
          data-div-id="chartdiv-7"
          data-div-filter-id="mapdiv-filter-7"
          data-div-reference-id="mapdiv-reference-7"
          data-chart-type='dbar'
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J007"
          data-description="
          {{ $keyValues['J007'] }}
          "
          data-reference-key="J027"
          data-reference="{{ $keyValues['J027'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop7') !!}
          </a>
          <a
          id="op-8"
          class="nav-link"
          href="#justice-chart-8"
          role="tab"
          aria-controls="justice-chart-8"
          aria-selected="true"
          data-tab-number="8"
          data-context="sexual-assault-on-women-registered-by-fgr"
          data-div-id="mapdiv-8"
          data-div-filter-id="mapdiv-filter-8"
          data-div-reference-id="mapdiv-reference-8"
          data-chart-type="map"
          data-grid-id="grid-8"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
						{ "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J008"
          data-description="
          {{ $keyValues['J008'] }}
          "
          data-reference-key="J028"
          data-reference="{{ $keyValues['J028'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop8') !!}
          </a>
          <a
          id="op-9"
          class="nav-link"
          href="#justice-chart-9"
          role="tab"
          aria-controls="justice-chart-9"
          aria-selected="true"
          data-tab-number="9"
          data-context="sexual-assault-on-women-processed-cases"
          data-div-id="mapdiv-9"
          data-div-filter-id="mapdiv-filter-9"
          data-div-reference-id="mapdiv-reference-9"
          data-chart-type="map"
          data-grid-id="grid-9"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
						{ "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J009"
          data-description="
          {{ $keyValues['J009'] }}
          "
          data-reference-key="J029"
          data-reference="{{ $keyValues['J029'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop9') !!}
          </a>
          <a
          id="op-10"
          class="nav-link"
          href="#justice-chart-10"
          role="tab"
          aria-controls="justice-chart-10"
          aria-selected="true"
          data-tab-number="10"
          data-context="sexual-assault-on-women-judgments"
          data-div-id="chartdiv-10"
          data-div-filter-id="mapdiv-filter-10"
          data-div-reference-id="mapdiv-reference-10"
          data-chart-type='dbar'
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J010"
          data-description="
          {{ $keyValues['J010'] }}
          "
          data-reference-key="J030"
          data-reference="{{ $keyValues['J030'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop10') !!}
          </a>
          <a
          id="op-11"
          class="nav-link"
          href="#justice-chart-11"
          role="tab"
          aria-controls="justice-chart-11"
          aria-selected="true"
          data-tab-number="11"
          data-context="crimes-on-women-on-leiv-registered-by-fgr"
          data-div-id="mapdiv-11"
          data-div-filter-id="mapdiv-filter-11"
          data-div-reference-id="mapdiv-reference-11"
          data-chart-type="map"
          data-grid-id="grid-11"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
						{ "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J011"
          data-description="
          {{ $keyValues['J011'] }}
          "
          data-reference-key="J031"
          data-reference="{{ $keyValues['J031'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop11') !!}
          </a>
          <a
          id="op-12"
          class="nav-link"
          href="#justice-chart-12"
          role="tab"
          aria-controls="justice-chart-12"
          aria-selected="true"
          data-tab-number="12"
          data-context="crimes-on-women-on-leiv-processed-cases"
          data-div-id="mapdiv-12"
          data-div-filter-id="mapdiv-filter-12"
          data-div-reference-id="mapdiv-reference-12"
          data-chart-type="map"
          data-grid-id="grid-12"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
						{ "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J012"
          data-description="
          {{ $keyValues['J012'] }}
          "
          data-reference-key="J032"
          data-reference="{{ $keyValues['J032'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop12') !!}
          </a>
          <a
          id="op-13"
          class="nav-link"
          href="#justice-chart-13"
          role="tab"
          aria-controls="justice-chart-13"
          aria-selected="true"
          data-tab-number="13"
          data-context="crimes-on-women-on-leiv-judgments"
          data-div-id="chartdiv-13"
          data-div-filter-id="mapdiv-filter-13"
          data-div-reference-id="mapdiv-reference-13"
          data-chart-type='dbar'
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J013"
          data-description="
          {{ $keyValues['J013'] }}
          "
          data-reference-key="J033"
          data-reference="{{ $keyValues['J033'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop13') !!}
          </a>
          <a
          id="op-14"
          class="nav-link"
          href="#justice-chart-14"
          role="tab"
          aria-controls="justice-chart-14"
          aria-selected="true"
          data-tab-number="14"
          data-context="feminicide-victims-registered-by-fgr"
          data-div-id="mapdiv-14"
          data-div-filter-id="mapdiv-filter-14"
          data-div-reference-id="mapdiv-reference-14"
          data-chart-type="map"
          data-grid-id="grid-14"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
						{ "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J014"
          data-description="
          {{ $keyValues['J014'] }}
          "
          data-reference-key="J034"
          data-reference="{{ $keyValues['J034'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop14') !!}
          </a>
          <a
          id="op-15"
          class="nav-link"
          href="#justice-chart-15"
          role="tab"
          aria-controls="justice-chart-15"
          aria-selected="true"
          data-tab-number="15"
          data-context="feminicide-victims-processed-cases"
          data-div-id="mapdiv-15"
          data-div-filter-id="mapdiv-filter-15"
          data-div-reference-id="mapdiv-reference-15"
          data-chart-type="map"
          data-grid-id="grid-15"
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
						{ "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J015"
          data-description="
          {{ $keyValues['J015'] }}
          "
          data-reference-key="J035"
          data-reference="{{ $keyValues['J035'] }}">
            <i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop15') !!}
          </a>
          <a
          id="op-16"
          class="nav-link"
          href="#justice-chart-16"
          role="tab"
          aria-controls="justice-chart-16"
          aria-selected="true"
          data-tab-number="16"
          data-context="feminicide-victims-judgments"
          data-div-id="chartdiv-16"
          data-div-filter-id="mapdiv-filter-16"
          data-div-reference-id="mapdiv-reference-16"
          data-chart-type='dbar'
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J016"
          data-description="
          {{ $keyValues['J016'] }}
          "
          data-reference-key="J036"
          data-reference="{{ $keyValues['J036'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop16') !!}
          </a>
          <a
          id="op-17"
          class="nav-link"
          href="#justice-chart-17"
          role="tab"
          aria-controls="justice-chart-17"
          aria-selected="true"
          data-tab-number="17"
          data-context="total-anual-domestic-violence"
          data-div-id="chartdiv-17"
          data-div-filter-id="mapdiv-filter-17"
          data-div-reference-id="mapdiv-reference-17"
          data-chart-type='hbar'
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J017"
          data-description="
          {{ $keyValues['J017'] }}
          "
          data-reference-key="J037"
          data-reference="{{ $keyValues['J037'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop17') !!}
          </a>
          <a
          id="op-18"
          class="nav-link"
          href="#justice-chart-18"
          role="tab"
          aria-controls="justice-chart-18"
          aria-selected="true"
          data-tab-number="18"
          data-context="total-anual-sexual-assault"
          data-div-id="chartdiv-18"
          data-div-filter-id="mapdiv-filter-18"
          data-div-reference-id="mapdiv-reference-18"
          data-chart-type='hbar'
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J018"
          data-description="
          {{ $keyValues['J018'] }}
          "
          data-reference-key="J038"
          data-reference="{{ $keyValues['J038'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop18') !!}
          </a>
          <a
          id="op-19"
          class="nav-link"
          href="#justice-chart-19"
          role="tab"
          aria-controls="justice-chart-19"
          aria-selected="true"
          data-tab-number="19"
          data-context="total-anual-on-leiv"
          data-div-id="chartdiv-19"
          data-div-filter-id="mapdiv-filter-19"
          data-div-reference-id="mapdiv-reference-19"
          data-chart-type='hbar'
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J019"
          data-description="
          {{ $keyValues['J019'] }}
          "
          data-reference-key="J039"
          data-reference="{{ $keyValues['J039'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop19') !!}
          </a>
          <a
          id="op-20"
          class="nav-link"
          href="#justice-chart-20"
          role="tab"
          aria-controls="justice-chart-20"
          aria-selected="true"
          data-tab-number="20"
          data-context="total-anual-feminicides"
          data-div-id="chartdiv-20"
          data-div-filter-id="mapdiv-filter-20"
          data-div-reference-id="mapdiv-reference-20"
          data-chart-type='hbar'
          data-filters='[
            { "filter": "year", "type": "slider","label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true }
          ]'
          data-description-key="J020"
          data-description="
          {{ $keyValues['J020'] }}
          "
          data-reference-key="J040"
          data-reference="{{ $keyValues['J040'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop20') !!}
          </a>
          <a
          id="op-21"
          class="nav-link"
          href="#justice-chart-21"
          role="tab"
          aria-controls="justice-chart-21"
          aria-selected="true"
          data-tab-number="21"
          data-context="processed-resolved-protection-cases"
          data-div-id="chartdiv-21"
          data-div-filter-id="mapdiv-filter-21"
          data-div-reference-id="mapdiv-reference-21"
          data-chart-type="line"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="J044"
          data-description="
          {{ $keyValues['J044'] }}
          "
          data-reference-key="J045"
          data-reference="{{ $keyValues['J045'] }}">
            <i class="fas fa-chart-line"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop21') !!}
          </a>
          <a
          id="op-22"
          class="nav-link"
          href="#justice-chart-22"
          role="tab"
          aria-controls="justice-chart-22"
          aria-selected="true"
          data-tab-number="22"
          data-context="habeas-corpus-resolved-protection-cases"
          data-div-id="chartdiv-22"
          data-div-filter-id="mapdiv-filter-22"
          data-div-reference-id="mapdiv-reference-22"
          data-chart-type="line"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="J046"
          data-description="
          {{ $keyValues['J046'] }}
          "
          data-reference-key="J047"
          data-reference="{{ $keyValues['J047'] }}">
            <i class="fas fa-chart-line"></i>  {!! Config::get('system-lang.' . $lang . '.justiciaop22') !!}
          </a>
          <a
          id="op-23"
          class="nav-link"
          href="#justice-chart-23"
          role="tab"
          aria-controls="justice-chart-23"
          aria-selected="true"
          data-tab-number="23"
          data-context="unconstitutional-resolved-protection-cases"
          data-div-id="chartdiv-23"
          data-div-filter-id="mapdiv-filter-23"
          data-div-reference-id="mapdiv-reference-23"
          data-chart-type="line"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false }
          ]'
          data-description-key="J048"
          data-description="
          {{ $keyValues['J048'] }}
          "
          data-reference-key="J049"
          data-reference="{{ $keyValues['J049'] }}">
            <i class="fas fa-chart-line"></i>  {!! Config::get('system-lang.' . $lang . '.justiciaop23') !!}
          </a>
          <a
          id="op-24"
          class="nav-link"
          href="#justice-chart-24"
          role="tab"
          aria-controls="justice-chart-24"
          aria-selected="true"
          data-tab-number="24"
          data-context="get-anual-comparison-domestic-violence"
          data-div-id="chartdiv-24"
          data-div-filter-id="mapdiv-filter-24"
          data-div-reference-id="mapdiv-reference-24"
          data-chart-type="cbar"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true, "limit": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J050"
          data-description="
          {{ $keyValues['J050'] }}
          "
          data-reference-key="J051"
          data-reference="{{ $keyValues['J051'] }}">
            <i class="fas fa-chart-bar"></i>  {!! Config::get('system-lang.' . $lang . '.justiciaop24') !!}
          </a>
          <a
          id="op-25"
          class="nav-link"
          href="#justice-chart-25"
          role="tab"
          aria-controls="justice-chart-25"
          aria-selected="true"
          data-tab-number="25"
          data-context="get-anual-comparison-sexual-assault"
          data-div-id="chartdiv-25"
          data-div-filter-id="mapdiv-filter-25"
          data-div-reference-id="mapdiv-reference-25"
          data-chart-type="cbar"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true, "limit": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J052"
          data-description="
          {{ $keyValues['J052'] }}
          "
          data-reference-key="J053"
          data-reference="{{ $keyValues['J053'] }}">
            <i class="fas fa-chart-bar"></i>  {!! Config::get('system-lang.' . $lang . '.justiciaop25') !!}
          </a>
          <a
          id="op-26"
          class="nav-link"
          href="#justice-chart-26"
          role="tab"
          aria-controls="justice-chart-26"
          aria-selected="true"
          data-tab-number="26"
          data-context="get-anual-comparison-leiv"
          data-div-id="chartdiv-26"
          data-div-filter-id="mapdiv-filter-26"
          data-div-reference-id="mapdiv-reference-26"
          data-chart-type="cbar"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true, "limit": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J054"
          data-description="
          {{ $keyValues['J054'] }}
          "
          data-reference-key="J055"
          data-reference="{{ $keyValues['J055'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop26') !!}
          </a>
          <a
          id="op-27"
          class="nav-link"
          href="#justice-chart-27"
          role="tab"
          aria-controls="justice-chart-27"
          aria-selected="true"
          data-tab-number="26"
          data-context="get-anual-comparison-feminicides"
          data-div-id="chartdiv-27"
          data-div-filter-id="mapdiv-filter-27"
          data-div-reference-id="mapdiv-reference-27"
          data-chart-type="cbar"
          data-filters='[
            { "filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true, "limit": true },
            { "filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}", "multiple": true },
						{ "filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}", "multiple": true },
            { "filter": "ageRange", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.rangoedad') !!}", "multiple": true },
            { "filter": "crime", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.delito') !!}", "multiple": true },
            { "filter": "month", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.mes') !!}", "multiple": true }
          ]'
          data-description-key="J056"
          data-description="
          {{ $keyValues['J056'] }}
          "
          data-reference-key="J057"
          data-reference="{{ $keyValues['J057'] }}">
            <i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.justiciaop27') !!}
          </a>
        </li>
      </ul>
    </div>
    <div id="mobile-menu" class="col-12 text-left d-sm-block d-md-block d-lg-none">
			<h4 class="menu-title">{!! Config::get('system-lang.' . $lang . '.derechojusticia') !!}</h4>
			<div class="text-center">
				<button type="button" id="btn-mmenu" class="btn btn-secondary oadh-bg-primary-color btn-lg w-100 my-3"><i class="fas fa-bars"></i> {!! Config::get('system-lang.' . $lang . '.veropciones') !!}</button>
			</div>
		</div>
    <div class="col-lg-9">
      <h4 class="oadh-text-primary-color text-center" id="oadh-chart-title"></h4>
			<div class="card card-filter">
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-sm-4">
							{!! Config::get('system-lang.' . $lang . '.disponibles') !!}
						</div>
						<div class="col-sm-8 text-right">
              <div class="btn-toolbar justify-content-end" role="toolbar" aria-label="accesibility buttons">
                <div class="btn-group" role="group" aria-label="filters">
                  <button type="button" id="btn-download-xlsx" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-arrow-down"></i> {{ Config::get('system-lang.' . $lang . '.descargar') }}
                  </button>
                </div>
                <div class="btn-group ml-2" role="group" aria-label="filters">
                  <button type="button" id="btn-filter" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-filter"></i> {!! Config::get('system-lang.' . $lang . '.aplicar') !!}
                  </button>
                </div>
              </div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="mapdiv-filter-1" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-2" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-3" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-4" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-5" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-6" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-7" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-8" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-9" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-10" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-11" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-12" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-13" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-14" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-15" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-16" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-17" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-18" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-19" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-20" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-21" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-22" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-23" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-24" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-25" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-26" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-27" style="display: none;" class="mapdiv-filter"></div>
					<p id="filter-description" data-key='' class="editable-element"></p>
				</div>
			</div>
			<div class="row align-items-center rights-description">
				<div class="col-lg-6">
					{{-- <img src="{{ URL::asset('oadh/inicios/justicia-v1.jpg') }}" alt="logo" class="d-block w-100 img-fluid mx-auto"> --}}
          <div class="home-rights-container oadh-bg-primary-color">
            <div class="d-flex justify-content-center">
              <div class="rounded-circle home-rights-circle">
                <img src="{{ URL::asset('oadh/inicios/justicia-v2.png') }}" alt="vida" class="img-fluid">
              </div>
            </div>
            <div class="mt-3">
              <h3 class="oadh-text-white-color text-center">
                {{ Config::get('system-lang.' . $lang . '.derechojusticia') }}
              </h3>
            </div>
          </div>
				</div>
				<div class="col-lg-6">
					<h3 class="oadh-text-primary-color editable-element" data-key="J041"> {{ $keyValues['J041'] }}</h3>
					<p class="text-justify editable-element" data-key="J042">
						{!! $keyValues['J042'] !!}
					</p>
					<p class="text-justify editable-element" data-key="J043">
						{!! $keyValues['J043'] !!}
					</p>
				</div>
			</div>
			<div class="my-2" data-spy="scroll" data-target="nav-option-list" data-offset="0">
        <div class="oadh-chart" id="tab-content-1" role="tabpanel" aria-labelledby="tc-1">
          <div id="chartdiv-1" class="chartdiv"></div>
          <p id="mapdiv-reference-1" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart">
          <div id="chartdiv-2" class="chartdiv"></div>
          <p id="mapdiv-reference-2" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart">
          <div id="chartdiv-3" class="chartdiv"></div>
          <p id="mapdiv-reference-3" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart">
          <div id="chartdiv-4" class="chartdiv"></div>
          <p id="mapdiv-reference-4" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart">
          <div id="mapdiv-5" class="chartdiv"></div>
          <p id="mapdiv-reference-5" class="reference-text editable-element"></p>
          <div id="grid-5">
            <div class="smt-body"></div>
          </div>
        </div>
        <div class="oadh-chart">
          <div id="mapdiv-6" class="chartdiv"></div>
          <p id="mapdiv-reference-6" class="reference-text editable-element"></p>
          <div id="grid-6">
            <div class="smt-body"></div>
          </div>
        </div>
        <div class="oadh-chart" id="tab-content-7" role="tabpanel" aria-labelledby="tc-7">
          <div id="chartdiv-7" class="chartdiv"></div>
          <p id="mapdiv-reference-7" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-8" role="tabpanel" aria-labelledby="tc-8">
          <div id="mapdiv-8" class="chartdiv"></div>
          <p id="mapdiv-reference-8" class="reference-text editable-element"></p>
          <div id="grid-8">
            <div class="smt-body"></div>
          </div>
        </div>
        <div class="oadh-chart" id="tab-content-9" role="tabpanel" aria-labelledby="tc-9">
          <div id="mapdiv-9" class="chartdiv"></div>
          <p id="mapdiv-reference-9" class="reference-text editable-element"></p>
          <div id="grid-9">
            <div class="smt-body"></div>
          </div>
        </div>
        <div class="oadh-chart" id="tab-content-10" role="tabpanel" aria-labelledby="tc-10">
          <div id="chartdiv-10" class="chartdiv"></div>
          <p id="mapdiv-reference-10" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-11" role="tabpanel" aria-labelledby="tc-11">
          <div id="mapdiv-11" class="chartdiv"></div>
          <p id="mapdiv-reference-11" class="reference-text editable-element"></p>
          <div id="grid-11">
            <div class="smt-body"></div>
          </div>
        </div>
        <div class="oadh-chart" id="tab-content-12" role="tabpanel" aria-labelledby="tc-12">
          <div id="mapdiv-12" class="chartdiv"></div>
          <p id="mapdiv-reference-12" class="reference-text editable-element"></p>
          <div id="grid-12">
            <div class="smt-body"></div>
          </div>
        </div>
        <div class="oadh-chart" id="tab-content-13" role="tabpanel" aria-labelledby="tc-13">
          <div id="chartdiv-13" class="chartdiv"></div>
          <p id="mapdiv-reference-13" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-14" role="tabpanel" aria-labelledby="tc-14">
          <div id="mapdiv-14" class="chartdiv"></div>
          <p id="mapdiv-reference-14" class="reference-text editable-element"></p>
          <div id="grid-14">
            <div class="smt-body"></div>
          </div>
        </div>
        <div class="oadh-chart" id="tab-content-15" role="tabpanel" aria-labelledby="tc-15">
          <div id="mapdiv-15" class="chartdiv"></div>
          <p id="mapdiv-reference-15" class="reference-text editable-element"></p>
          <div id="grid-15">
            <div class="smt-body"></div>
          </div>
        </div>
        <div class="oadh-chart" id="tab-content-16" role="tabpanel" aria-labelledby="tc-16">
          <div id="chartdiv-16" class="chartdiv"></div>
          <p id="mapdiv-reference-16" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-17" role="tabpanel" aria-labelledby="tc-17">
          <div id="chartdiv-17" class="chartdiv"></div>
          <p id="mapdiv-reference-17" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-18" role="tabpanel" aria-labelledby="tc-18">
          <div id="chartdiv-18" class="chartdiv"></div>
          <p id="mapdiv-reference-18" class="editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-19" role="tabpanel" aria-labelledby="tc-19">
          <div id="chartdiv-19" class="chartdiv"></div>
          <p id="mapdiv-reference-19" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-20" role="tabpanel" aria-labelledby="tc-20">
          <div id="chartdiv-20" class="chartdiv"></div>
          <p id="mapdiv-reference-20" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-21" role="tabpanel" aria-labelledby="tc-21">
          <div id="chartdiv-21" class="chartdiv"></div>
          <p id="mapdiv-reference-21" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-22" role="tabpanel" aria-labelledby="tc-22">
          <div id="chartdiv-22" class="chartdiv"></div>
          <p id="mapdiv-reference-22" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-23" role="tabpanel" aria-labelledby="tc-23">
          <div id="chartdiv-23" class="chartdiv"></div>
          <p id="mapdiv-reference-23" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-24" role="tabpanel" aria-labelledby="tc-24">
          <div id="chartdiv-24" class="chartdiv"></div>
          <p id="mapdiv-reference-24" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-25" role="tabpanel" aria-labelledby="tc-25">
          <div id="chartdiv-25" class="chartdiv"></div>
          <p id="mapdiv-reference-25" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-26" role="tabpanel" aria-labelledby="tc-26">
          <div id="chartdiv-26" class="chartdiv"></div>
          <p id="mapdiv-reference-26" class="reference-text editable-element"></p>
        </div>
        <div class="oadh-chart" id="tab-content-27" role="tabpanel" aria-labelledby="tc-27">
          <div id="chartdiv-27" class="chartdiv"></div>
          <p id="mapdiv-reference-27" class="reference-text editable-element"></p>
        </div>
			</div>
		</div>
  </div>
</div>
{!! Form::open(array('id' => 'oadh-xlsx-download-form', 'url' => URL::to('/cms/derechos/download-data-xlsx'), 'role'  =>  'form', 'class' => 'form-horizontal')) !!}
  {!! Form::hidden('oadh-xlsx-data', null, ['id' => 'oadh-xlsx-data', 'data-details' => '[]']) !!}
  {!! Form::hidden('oadh-xlsx-data-titles', null, ['id' => 'oadh-xlsx-data-titles', 'data-details-titles' => '[]']) !!}
{!! Form::close() !!}
@parent
@stop