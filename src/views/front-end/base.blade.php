<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=yes">
		@include('decima-oadh::front-end/css')
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23678568-11"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		  gtag('config', 'UA-23678568-11');
		</script>
		<!-- Design libreries -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
			integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css"
			integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet"
			href="https://storage.googleapis.com/decimaerp-cdn-bucket/multiselect-v0.2.18/multiselect.min.css">
		<link rel="stylesheet"
			href="https://storage.googleapis.com/decimaerp-cdn-bucket/jquery-multiselect-v0.9.12/multi-select.min.css">
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/css/bootstrap-slider.min.css" />
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.0.6/jquery.mmenu.all.css">
		<link rel="shortcut icon" href="https://storage.googleapis.com/decimaerp-cdn-bucket/oudh/inicio/OUDH.ico" type="image/x-icon">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" />
		<!-- scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
			integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
		</script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" 
			integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
		</script>
		<!-- Resources -->
		<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
		<script src="https://cdn.amcharts.com/lib/4/maps.js"></script>
		<script src='https://cdn.amcharts.com/lib/4/charts.js'></script>
		<script src='https://cdn.amcharts.com/lib/4/lang/es_ES.js'></script>
		<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
		<script src="https://cdn.amcharts.com/lib/4/themes/amchartsdark.js"></script>
		<script src="https://cdn.amcharts.com/lib/4/geodata/elSalvadorLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/4/geodata/continentsLow.js"></script>
		<script src="https://cdn.amcharts.com/lib/4/geodata/worldLow.js"></script>	
		<script src="https://cdn.amcharts.com/lib/4/plugins/timeline.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.2.0/chart.min.js" integrity="sha512-VMsZqo0ar06BMtg0tPsdgRADvl0kDHpTbugCBBrL55KmucH6hP9zWdLIWY//OTfMnzz6xWQRxQqsUFefwHuHyg==" crossorigin="anonymous"></script>
		<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
		
		<script src="https://storage.googleapis.com/decimaerp-cdn-bucket/multiselect-v0.2.18/multiselect.min.js"></script>
		<script src="https://storage.googleapis.com/decimaerp-cdn-bucket/kwaai/js/helpers-v1.0.3.js"></script>
		<script src="https://storage.googleapis.com/decimaerp-cdn-bucket/jquery-multiselect-v0.9.12/jquery.multi-select.min.js"></script>
		<script src="https://decima-bucket.s3.us-east-2.amazonaws.com/cdn/jquery-ezview-v1.0.0/EZView.js"></script>
		<script src="https://decima-bucket.s3.us-east-2.amazonaws.com/cdn/jquery-ezview-v1.0.0/draggable.js"></script>
		<script src="https://storage.googleapis.com/decimaerp-cdn-bucket/jquery-highlight-v4.0/jquery.highlight.min.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.1/bootstrap-slider.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.6/tinymce.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.6/jquery.tinymce.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.6/plugins/table/plugin.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.6/plugins/code/plugin.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.all.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.0.6/jquery.mmenu.all.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" integrity="sha512-yDlE7vpGDP7o2eftkCiPZ+yuUyEcaBwoJoIhdXv71KZWugFqEphIS3PU60lEkFaz8RxaVsMpSvQxMBaKVwA5xg==" crossorigin="anonymous"></script>

		@include('decima-oadh::front-end/helpers-js')
		@yield('javascript')
		<title>OUDH</title>
		<!-- for Google -->
		<meta name="description" content="Observatorio Universitario de Derechos Humanos | OUDH" />
		<!-- for Facebook -->
		<meta property="og:title" content="Observatorio Universitario de Derechos Humanos | OUDH" />
	</head>
	<body id="body">
		<div id="page-wrapper">
			<nav class="navbar navbar-expand-lg fixed-top navbar-dark oadh-bg-primary-color oadh-nav-text-color">
				<div class="container">
					<a class="navbar-brand" href="/cms/inicio{{ '?lang=' . $lang }}">
						<img src="{{ URL::asset('oadh/logos/logo-secundario-v1.png') }}" class="d-inline-block align-center brand-logo">
					</a>
					<div class="d-flex flex-row order-2 order-lg-3 ml-1">
						<ul class="navbar-nav flex-row icons">
							@if(!Auth::check())
							<li class="nav-item d-none d-md-block d-lg-block d-xl-block"><a class="nav-link px-2 oadh-text-primary-color" href="/login"><i class="fab fas fa-sign-in-alt"></i></a></li>
							@else
							<li class="nav-item d-none d-md-block d-lg-block d-xl-block"><a class="nav-link px-2 oadh-text-primary-color" href="/dashboard"><i class="fab fas fa-user"></i></a></li>
							@endif
							<li class="nav-item"><a class="nav-link px-2 oadh-text-primary-color" href="https://www.facebook.com/oudhsv/"><i class="fab fa-facebook"></i></a></li>
							<li class="nav-item"><a class="nav-link px-2 oadh-text-primary-color" href="https://twitter.com/oudhsv"><i class="fab fa-twitter"></i></a></li>
							<li class="nav-item dropdown oadh-nav-item">
								<a class="nav-link px-2 dropdown-toggle oadh-text-primary-color" href="#" id="languages" role="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false"><span class="dropdown-title"><i class="fab fas fa-globe-europe"></i></span></a>
								<div class="dropdown-menu  mx-2" aria-labelledby="languages">
									<a class="dropdown-item" href="{{ URL::current() . '?lang=es' }}">{{ Config::get('system-lang.' . $lang . '.espanol') }}</a>
									<a class="dropdown-item" href="{{ URL::current() . '?lang=en' }}">{!! Config::get('system-lang.' . $lang . '.ingles') !!}</a>
								</div>
							</li>
						</ul>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"></span>
						</button>
				</div>
					<div class="collapse navbar-collapse order-3 order-lg-2" id="navigation">
						<ul class="navbar-nav ml-auto nav-menu-right oadh-navbar">
							<li class="nav-item oadh-nav-item">
								<a class="nav-link oadh-nav-link mx-2" href="/cms/acerca-de{{ '?lang=' . $lang }}" id="AboutUs" role="button" data-toggle="AboutUs" aria-haspopup="true" aria-expanded="false">{{ Config::get('system-lang.' . $lang . '.acercade') }}</a>
							</li>
							<li class="nav-item dropdown oadh-nav-item">
								<a class="nav-link mx-2 dropdown-toggle oadh-nav-link" href="#" id="rights" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdown-title">{{ Config::get('system-lang.' . $lang . '.derechos') }}</span></a>
								<div class="dropdown-menu  mx-2" aria-labelledby="rights">
									<a class="dropdown-item" href="/cms/derechos/vida{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.derechovida') }}</a>
									<a class="dropdown-item" href="/cms/derechos/integridad{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.derechointegridad') }}</a>
									<a class="dropdown-item" href="/cms/derechos/libertad{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.derecholibertad') }}</a>
									<a class="dropdown-item" href="/cms/derechos/acceso-a-la-justicia{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.derechojusticia') }}</a>
									<a class="dropdown-item" href="/cms/derechos/reparacion-integral-de-victimas{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.derechoreparacion') }}</a>
								</div>
							</li>
							<li class="nav-item oadh-nav-item">
								<a class="nav-link mx-2 oadh-nav-link" href="/cms/publicaciones{{ '?lang=' . $lang }}" id="Publications" role="button" data-toggle="Publications" aria-haspopup="true" aria-expanded="false"><span class="dropdown-title">{{ Config::get('system-lang.' . $lang . '.publicaciones') }}</span></a>
							</li>
							<li class="nav-item oadh-nav-item">
								<a class="nav-link mx-2 oadh-nav-link" href="/cms/recomendaciones{{ '?lang=' . $lang }}" id="Recommendations" role="button" data-toggle="Recommendations" aria-haspopup="true" aria-expanded="false"><span class="dropdown-title">{{ Config::get('system-lang.' . $lang . '.recomendaciones') }}</span></a>
							</li>
							
							{{-- <li class="nav-item oadh-nav-item">
								<a class="nav-link mx-2 oadh-nav-link" href="/cms/medios-de-prensa{{ '?lang=' . $lang }}" id="Media" role="button" data-toggle="Media" aria-haspopup="true" aria-expanded="false"><span class="dropdown-title">{{ Config::get('system-lang.' . $lang . '.prensa') }}</span></a>
							</li> --}}

							<li class="nav-item dropdown oadh-nav-item">
								<a class="nav-link mx-2 dropdown-toggle oadh-nav-link" href="#" id="rights" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="dropdown-title">{{ Config::get('system-lang.' . $lang . '.prensa') }}</span></a>
								<div class="dropdown-menu  mx-2" aria-labelledby="rights">
									<a class="dropdown-item" href="/cms/acerca-monitoreo-medios{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.prensa01') }}</a>
									<a class="dropdown-item" href="/cms/medios-de-prensa{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.prensa03') }}</a>
									<a class="dropdown-item" href="/cms/monitoreo-de-medios{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.prensa02') }}</a>
								</div>
							</li>

							<li class="nav-item oadh-nav-item">
								<a class="nav-link mx-2 oadh-nav-link" href="/cms/videos{{ '?lang=' . $lang }}" id="Videos" role="button" data-toggle="Videos" aria-haspopup="true" aria-expanded="false"><span class="dropdown-title">{{ Config::get('system-lang.' . $lang . '.videos') }}</span></a>
							</li>
							<li class="nav-item oadh-nav-item">
								<a class="nav-link mx-2 oadh-nav-link" href="/cms/actividades{{ '?lang=' . $lang }}" id="Activities" role="button" data-toggle="Activities" aria-haspopup="true" aria-expanded="false"><span class="dropdown-title">{{ Config::get('system-lang.' . $lang . '.actividades') }}</span></a>
							</li>
						</ul>					
					</div>
				</div>
			</nav>
			<!-- Page Content -->
			<div id='page-container'>
				<fieldset id="main-panel-fieldset">
					@section('container')
					@show
					{!! Form::hidden('app-url', URL::to('/'), array('id' => 'app-url')) !!}
					{!! Form::hidden('app-token', Session::token(), array('id' => 'app-token')) !!}
					{!! Form::hidden('app-kwaai', '', array('id' => 'app-kwaai')) !!}
				</fieldset>
				{!! Form::button('<i class="fa fa-spinner fa-spin fa-lg"></i> ' . Lang::get('form.loadButton'), array('id' => 'app-loader', 'class' => 'btn btn-dark btn-disable btn-lg', 'disabled' => 'disabled', 'style' => 'display:none;position: absolute;z-index: 10510;')) !!}
			</div>
			<!-- /.container -->
			@include('decima-oadh::front-end/footer')

			@yield('mobile-menu')
		</div>
	</body>
</html>
