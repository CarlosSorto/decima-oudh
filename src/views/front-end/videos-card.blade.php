@if (!empty($videos['galleries']))
  <div class="col-sm-12">
    <div class="carousel slide video-carousel" data-ride="carousel" id="multi_item">
      <div class="carousel-inner">
        @php
        $count = 0;
        @endphp
        @foreach ($videos['galleries'] as $gallery)
        @if ($count == 0)
        <div class="carousel-item p-2 active">
          <div class="row">
            @endif
            @if ($count > 0 && $count % 6 == 0)
          </div>
        </div>
        <div class="carousel-item p-2">
          <div class="row">
            @endif
            <div class="col-sm-12">
              <div class="card video-card mb-3">
                <div class="row no-gutters align-items-center">
                  <div class="col-md-5">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="{{$gallery['image_url'] }}" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    {{-- <img src="{{$gallery['image_url'] }}" alt="" class="card-img img-fluid"> --}}
                  </div>
                  <div class="col-md-7">
                    <div class="card-body">
                      <h5 class="card-title oadh-text-secondary-color text-left">{{$gallery['title'] }}</h5>
                      <p class="card-text oadh-text-white-color text-justify">{{$gallery['description'] }}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @php
            $count++
            @endphp
            @endforeach
            @if ($count > 0 && $count % 2 != 0)
          </div>
        </div>
        @else
      </div>
    </div>
    @endif
  </div>
  <a class="carousel-control-prev" href="#multi_item" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    {{-- <span class="">Previous</span> --}}
  </a>
  <a class="carousel-control-next" href="#multi_item" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    {{-- <span class="sr-only">Next</span> --}}
  </a>
@endif