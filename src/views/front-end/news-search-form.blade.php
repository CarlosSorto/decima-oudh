<div class="row justify-content-center">
	<div class="col-sm-12 col-md-12" id="oadh-news-search-group">
		<input type="hidden" id="oadh-news-search-form" data-action="{{url('cms/medios-de-prensa/advanced-smt-rows')}}">
		<div class="form-row justify-content-center">
			<div class="input-group">
				<div class="input-group-prepend" data-toggle="tooltip" data-placement="top" title="Búsqueda avanzada">
					<button type="button" id="oadh-news-btn-collapse-filter"
						class="btn oadh-news-search-control oadh-bg-primary-color oadh-outline-none oadh-text-white-color oadh-dropdown-toggle"
						data-toggle="collapse" href="#oadh-news-collapse-filter-form" role="button" aria-expanded="false"
						aria-controls="oadh-news-collapse-filter-form">
						<span class="sr-only">Toggle collapse</span>
						{!! Config::get('system-lang.' . $lang . '.opciones') !!}
					</button>
				</div>
				<input type="text" class="form-control oadh-outline-none" id="oadh-news-search-text" name="oadh-news-search-text"
					required placeholder="{{ Config::get('system-lang.' . $lang . '.escribir') }}">
				<div class="input-group-append">
					<button type="button" class="btn oadh-news-search-control oadh-bg-primary-color oadh-outline-none oadh-text-white-color"
						id="oadh-news-btn-search" type="button">{{ Config::get('system-lang.' . $lang . '.buscar') }}</button>
				</div>
				<div class="input-group-append">
					<button type="button" class="btn oadh-news-search-control oadh-bg-secondary-color oadh-outline-none oadh-text-white-color"
						id="oadh-news-btn-clear-filter" type="button">{{ Config::get('system-lang.' . $lang . '.limpiar') }}</button>
				</div>
			</div>
			<div class="col-sm-12 p-0">
				<div class="collapse" id="oadh-news-collapse-filter-form">
					<div class="card card-body rounded-0 oadh-card">
						<div class="form-row">
							<div class="col-sm-12 col-md-8 col-lg-6 mb-3 d-none" id="oadh-news-date-time-group">
								<label for="validationCustom01">{{ Config::get('system-lang.' . $lang . '.rangofecha') }}</label>
								<div class="input-group">
									<div class="input-group-prepend d-none d-sm-block">
										<p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.de') }}</p>
									</div>
									<input type="date" class="oadh-outline-none form-control" id="oadh-news-daterange-from"
										name="oadh-news-daterange-from">
									<div class="input-group-prepend input-group-append d-none d-sm-block">
										<p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.al') }}</p>
									</div>
									<input type="date" class="oadh-outline-none form-control" id="oadh-news-daterange-to"
										name="oadh-news-daterange-to">
									<div class="input-group-append">
										<button
											class="btn oadh-bg-primary-color oadh-outline-none oadh-text-white-color oadh-news-btn-change-date-datetime"
											data-visibility='oadh-news-date-time-group' type="button" data-toggle="tooltip"
											data-placement="top" title="{{ Config::get('system-lang.' . $lang . '.cambiarfecha') }}">
											<i class="fas fa-exchange-alt"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="oadh-news-system">{{ Config::get('system-lang.' . $lang . '.medio') }}</label>
								<select id="oadh-news-media" name="oadh-news-media" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
									@foreach ($digitalMedias as $digitalMedia)
									<option value="{{$digitalMedia['value']}}">{{$digitalMedia['label']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3" id="oadh-news-date-group">
								<label for="validationCustom01">{{ Config::get('system-lang.' . $lang . '.fecha') }}</label>
								<div class="input-group">
									<input type="date" class="oadh-outline-none form-control" id="oadh-news-date" name="oadh-news-date">
									<div class="input-group-append">
										<button
											class="btn oadh-bg-primary-color oadh-outline-none oadh-text-white-color oadh-news-btn-change-date-datetime"
											data-visibility='oadh-news-date-group' type="button" data-toggle="tooltip" data-placement="top"
											title="{{ Config::get('system-lang.' . $lang . '.cambiarrangofecha') }}">
											<i class="fas fa-exchange-alt"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="validationCustom03">{{ Config::get('system-lang.' . $lang . '.departamento') }}</label>
								<select id="oadh-news-department" name="oadh-news-department" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..."
									multiple>
									@foreach ($departments as $department)
										<option value="{{$department['value']}}">{{$department['label']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="">{{ Config::get('system-lang.' . $lang . '.municipio') }}</label>
								<select id="oadh-news-municipality" name="oadh-news-municipality" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="">{{ Config::get('system-lang.' . $lang . '.derecho') }}</label>
								<select id="oadh-news-right" name="oadh-news-right" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
									@foreach ($rights as $right)
										<option value="{{$right['value']}}">{{$right['label']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="">{{ Config::get('system-lang.' . $lang . '.tema') }}</label>
								<select id="oadh-news-topic" name="oadh-news-topic" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="">{{ Config::get('system-lang.' . $lang . '.hecho') }}</label>
								<select id="oadh-news-violated-fact" name="oadh-news-violated-fact" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="">{{ Config::get('system-lang.' . $lang . '.fuente') }}</label>
								<select id="oadh-news-source" name="oadh-news-source" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
									@foreach ($sources as $source)
									<option value="{{$source['value']}}">{{$source['label']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="oadh-news-population-affected">{{ Config::get('system-lang.' . $lang . '.poblacion') }}</label>
								<select id="oadh-news-population-affected" name="oadh-news-population-affected" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
									@foreach ($populations as $population)
									<option value="{{$population['value']}}">{{$population['label']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="">{!! Config::get('system-lang.' . $lang . '.generovictima') !!}</label>
								<select id="oadh-news-victim-gender" name="oadh-news-victim-gender" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
									<option value="H">{!! Config::get('system-lang.' . $lang . '.hombre') !!}</option>
									<option value="M">{!! Config::get('system-lang.' . $lang . '.mujer') !!}</option>
									<option value="N">{!! Config::get('system-lang.' . $lang . '.noespecificado') !!}</option>
								</select>
							</div>
							<div class="col-sm-12 col-md-8 col-lg-6 mb-3" id="oadh-news-victim-age-group">
								<label for="validationCustom01">{{ Config::get('system-lang.' . $lang . '.rangovictima') }}</label>
								<div class="input-group">
									<div class="input-group-prepend d-none d-sm-block">
										<p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.de') }}</p>
									</div>
									<input type="text" class="oadh-outline-none form-control" id="oadh-news-victim-age-from"
										name="oadh-news-victim-age-from" placeholder="{!! Config::get('system-lang.' . $lang . '.digiteedad') !!}..." maxlength="3">
									<div class="input-group-prepend input-group-append d-none d-sm-block">
										<p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.al') }}</p>
									</div>
									<input type="text" class="oadh-outline-none form-control" id="oadh-news-victim-age-to"
										name="oadh-news-victim-age-to" placeholder="{!! Config::get('system-lang.' . $lang . '.digiteedad') !!}..." maxlength="3">
								</div>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="">{{ Config::get('system-lang.' . $lang . '.tipoarma') }}</label>
								<select id="oadh-news-weapon-type" name="oadh-news-weapon-type" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
									@foreach ($weapons as $weapon)
									<option value="{{$weapon['value']}}">{{$weapon['label']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-3 mb-3">
								<label for="">{!! Config::get('system-lang.' . $lang . '.generovictimario') !!}</label>
								<select id="oadh-news-victimizer-gender" name="oadh-news-victimizer-gender" class="form-control selectpicker" data-live-search="true" data-actions-box="true" 
										data-selected-text-format="count > 2" data-size="5" title="{!! Config::get('system-lang.' . $lang . '.escoja') !!}..." 
										multiple>
									<option value="H">{!! Config::get('system-lang.' . $lang . '.hombre') !!}</option>
									<option value="M">{!! Config::get('system-lang.' . $lang . '.mujer') !!}</option>
									<option value="N">{!! Config::get('system-lang.' . $lang . '.noespecificado') !!}</option>
								</select>
							</div>
							<div class="col-sm-12 col-md-8 col-lg-6 mb-3" id="oadh-news-victimizer-age-group">
								<label for="validationCustom01">{{ Config::get('system-lang.' . $lang . '.rangovictimario') }}</label>
								<div class="input-group">
									<div class="input-group-prepend d-none d-sm-block">
										<p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.de') }}</p>
									</div>
									<input type="text" class="oadh-outline-none form-control" id="oadh-news-victimizer-age-from"
										name="oadh-news-victimizer-age-from" placeholder="{!! Config::get('system-lang.' . $lang . '.digiteedad') !!}..." maxlength="3">
									<div class="input-group-prepend input-group-append d-none d-sm-block">
										<p class="input-group-text">{{ Config::get('system-lang.' . $lang . '.al') }}</p>
									</div>
									<input type="text" class="oadh-outline-none form-control" id="oadh-news-victimizer-age-to"
										name="oadh-news-victimizer-age-to" placeholder="{!! Config::get('system-lang.' . $lang . '.digiteedad') !!}..." maxlength="3">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>