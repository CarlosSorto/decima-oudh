@extends('decima-oadh::front-end/base')

@section('container')
<script type='text/javascript'>
	$(document).ready(function ()
	{

	});
</script>
<header>
	<div id="homeCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active" data-interval="10000">
				<div class="oadh-bg-primary-color">
					<div class="container ">
						<div class="row align-items-center oadh-max-height ">
							<div class="col-md-12 col-lg-5">
								<h3 class="oadh-text-tertiary-color editable-element mt-3" data-key="H001">{!! $keyValues['H001'] !!}</h3>
								<p class="oadh-text-tertiary-color text-justify editable-element" data-key="H002">{!! $keyValues['H002'] !!}</p>
								<a class="btn btn-lg btn-block oadh-bg-secondary-color oadh-text-tertiary-color mb-3" href="/cms/derechos/vida{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.explorar') }}</a>
							</div>
							<div class="col-sm-7">
								<div id="homemap" class="homemap d-none d-lg-block d-md-none d-sm-none"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="carousel-item" data-interval="10000">
				<div class="oadh-bg-secondary-color">
					<div class="container">
						<div class="row align-items-center oadh-max-height">
							<div class="col-md-12 col-lg-5">
								<h4 class="oadh-text-tertiary-color editable-element mt-3" data-key="H003">{!! $keyValues['H003'] !!}</h4>
								<p class="oadh-text-tertiary-color text-justify editable-element" data-key="H004">{!! $keyValues['H004']
									!!}</p>
								<a class="btn btn-lg btn-block oadh-bg-primary-color oadh-text-tertiary-color mb-3" href="/cms/publicaciones{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.descubre') }}</a>
							</div>
							<div class="col-sm-7 d-none d-lg-block d-md-none d-sm-none">
								{{-- <img src="{{ public_path('/oadh/inicios/home-banner-2-v3.png') }}" --}}
								<img src="{{ URL::asset('oadh/inicios/home-banner-2-v3.png') }}"
									class="d-block w-100">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="carousel-item" data-interval="10000">
				<div class="oadh-bg-secondary-color">
					<div class="container">
						<div class="row justify-content-center align-items-end oadh-max-height">
							<div class="col-sm-12">
								<img src="{{ URL::asset('oadh/inicios/home-banner-1-v4.png') }}" class="d-block w-100">
							</div>
							<div class="col-sm-6 col-md-3 text-center oadh-position-absolute">
									<a class="btn btn-lg btn-block oadh-bg-primary-color oadh-text-tertiary-color mb-4" href="/cms/acerca-de{{ '?lang=' . $lang }}">{!! Config::get('system-lang.' . $lang . '.leermas') !!}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev" href="#homeCarousel" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			{{-- <span class="sr-only">Previous</span> --}}
		</a>
		<a class="carousel-control-next" href="#homeCarousel" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			{{-- <span class="sr-only">Next</span> --}}
		</a>
	</div>
</header>
<div class="container pb-4">
	
	<div class="row">
		
		<div class="col-sm-12 col-md-4 mt-4">
			<div class="home-rights-container oadh-bg-primary-color">
				<div class="d-flex justify-content-center">
					<div class="rounded-circle home-rights-circle">
						<a href="/cms/derechos/vida{{ '?lang=' . $lang }}">
							<img src="{{ URL::asset('oadh/inicios/vida-v4.png') }}" alt="vida" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="mt-3">
					<h3 class="oadh-text-white-color text-center">
						{{ Config::get('system-lang.' . $lang . '.derechovida') }}
					</h3>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-md-4 mt-4">
			<div class="home-rights-container oadh-bg-primary-color">
				<div class="d-flex justify-content-center">
					<div class="rounded-circle home-rights-circle">
						<a href="/cms/derechos/integridad{{ '?lang=' . $lang }}">
							<img src="{{ URL::asset('oadh/inicios/integridad-v4.png') }}" alt="vida" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="mt-3">
					<h3 class="oadh-text-white-color text-center">
						{{ Config::get('system-lang.' . $lang . '.derechointegridad') }}
					</h3>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-md-4 mt-4">
			<div class="home-rights-container oadh-bg-primary-color">
				<div class="d-flex justify-content-center">
					<div class="rounded-circle home-rights-circle">
						<a href="/cms/derechos/libertad{{ '?lang=' . $lang }}">
							<img src="{{ URL::asset('oadh/inicios/libertad-v4.png') }}" alt="vida" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="mt-3">
					<h3 class="oadh-text-white-color text-center">
						{{ Config::get('system-lang.' . $lang . '.derecholibertad') }}
					</h3>
				</div>
			</div>
		</div>

	</div>
	
	<div class="row justify-content-center">

		<div class="col-sm-12 col-md-4 mt-4">
			<div class="home-rights-container oadh-bg-primary-color">
				<div class="d-flex justify-content-center">
					<div class="rounded-circle home-rights-circle">
						<a href="/cms/derechos/acceso-a-la-justicia{{ '?lang=' . $lang }}">
							<img src="{{ URL::asset('oadh/inicios/justicia-v2.png') }}" alt="vida" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="mt-3">
					<h3 class="oadh-text-white-color text-center">
						{{ Config::get('system-lang.' . $lang . '.derechojusticia') }}
					</h3>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-md-4 mt-4">
			<div class="home-rights-container oadh-bg-primary-color">
				<div class="d-flex justify-content-center">
					<div class="rounded-circle home-rights-circle">
						<a href="/cms/derechos/reparacion-integral-de-victimas{{ '?lang=' . $lang }}">
							<img src="{{ URL::asset('oadh/inicios/reparacion-v2.png') }}" alt="vida" class="img-fluid">
						</a>
					</div>
				</div>
				<div class="mt-3">
					<h3 class="oadh-text-white-color text-center">
						{{ Config::get('system-lang.' . $lang . '.derechoreparacion') }}
					</h3>
				</div>
			</div>
		</div>
	</div>
	
	{{-- <div class="row text-center">
		<div class="col-sm-12 col-md-4 oadh-lg-padding-30">
			<a href="/cms/derechos/vida">
				<img src="{{ URL::asset('oadh/inicios/vida-v3.png') }}" alt="vida" class="img-fluid">
			</a>
		</div>
		<div class="col-sm-12 col-md-4 oadh-lg-padding-30">
			<a href="/cms/derechos/integridad">
				<img src="{{ URL::asset('oadh/inicios/integridad-v3.png') }}" alt="integridad" class="img-fluid">
			</a>
		</div>
		<div class="col-sm-12 col-md-4 oadh-lg-padding-30">
			<a href="/cms/derechos/libertad">
				<img src="{{ URL::asset('oadh/inicios/libertad-v3.png') }}" alt="libertad" class="img-fluid">
			</a>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-sm-12 col-md-4 oadh-lg-padding-30">
			<a href="/cms/derechos/acceso-a-la-justicia">
				<img src="{{ URL::asset('oadh/inicios/justicia-v1.jpg') }}" alt="vida" class="img-fluid">
			</a>
		</div>
		<div class="col-sm-12 col-md-4 oadh-lg-padding-30">
			<a href="/cms/derechos/reparacion-integral-de-victimas">
				<img src="{{ URL::asset('oadh/inicios/reparacion-v1.jpg') }}" alt="integridad" class="img-fluid">
			</a>
		</div>
	</div> --}}


</div>
<div class="oadh-bg-secondary-color">
	<div class="container py-5">
		<div class="row align-items-center">
			<div class="col-sm-4">
				<img src="{{ URL::asset('oadh/inicios/suscripcion-v1.jpeg') }}" alt="boletin" class="img-fluid">
			</div>
			<div class="col-sm-8">
				<h4 class="oadh-text-tertiary-color editable-element" data-key="H005">{!! $keyValues['H005'] !!}</h4>
				<P class="oadh-text-tertiary-color editable-element" data-key="H006">{!! $keyValues['H006'] !!}</P>
				<div class="input-group mb-3">
					<input type="text" name="email" class="form-control input-email" id="email" placeholder="email@example.com">
					<div class="input-group-append">
						<button id="btn-email" class="btn oadh-bg-primary-color oadh-text-tertiary-color btn-email" type="button"
							name="btn-email">{{ Config::get('system-lang.' . $lang . '.suscribite') }}</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@parent
@stop
