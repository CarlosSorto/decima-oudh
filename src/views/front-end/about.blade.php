@extends('decima-oadh::front-end/base')
@section('container')

<style>
  .oadh-bottom-border {
    border-bottom: 1px solid;
    border-color: rgb(255, 152, 21) !important;
  }
</style>

<div class="container my-4">
  <div class="row align-items-center">
    <div class="col-sm-6 align-content-center">
      <img src="{{ URL::asset('oadh/logos/logo-primario-v2.png') }}" alt="logo"
        class="d-block w-100 img-fluid mx-auto">
    </div>
    <div class="col-sm-6">
      <h3 class="oadh-text-primary-color editable-element" data-key="A001">{!! $keyValues['A001'] !!}</h3>
      <p class="text-justify editable-element" data-key="A002">
        {!! $keyValues['A002'] !!}
      </p>
      <p class="text-justify editable-element" data-key="A003">
        {!! $keyValues['A003'] !!}
      </p>
    </div>
  </div>
  <div class="row mt-2">
    <div class="col-sm-12">
      <div class="row justify-content-center">
        <div class="col-sm-6">
          <h2 class="oadh-text-primary-color text-center oadh-bottom-border pb-2 editable-element" data-key="A004">{!! $keyValues['A004'] !!}</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="row align-items-end mb-5 pb-5 text-center">
    <div class="col-sm-4">
      <img src="{{ URL::asset('oadh/logos/logo-usaid-v1.png') }}" alt="logo" class="img-fluid">
    </div>
    <div class="col-sm-4">
      <img src="{{ URL::asset('oadh/logos/logo-union-europea-v1.png') }}" alt="logo" class="img-fluid">
    </div>
    <div class="col-sm-4">
      <img src="{{ URL::asset('oadh/logos/logo-uca-azul-v1.png') }}" alt="logo" class="img-fluid">
    </div>
  </div>
</div>

@parent
@stop
