	<!-- Footer -->
	<footer class="oadh-lg-py-2 oadh-lg-py-5 oadh-bg-primary-color">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-sm-3 col-md-3 col-lg-3 d-none d-lg-block d-md-none d-sm-none icons">
					<a href="/cms/inicio"><img src="{{ URL::asset('oadh/logos/logo-secundario-v1.png') }}" class="d-inline-block align-top brand-logo"></a>
					<p class="text-left editable-element oadh-text-tertiary-color" data-key="P001">
						{!! $keyValues['P001'] !!}
					</p>
					<a href="https://www.facebook.com/oudhsv/" class="oadh-text-primary-color mx-1"><i class="fab fa-facebook"></i></a>
					<a href="https://twitter.com/oudhsv" class="oadh-text-primary-color mx-1"><i class="fab fa-twitter"></i></a>
				</div>
				<div class="col-sm-2 col-md-2 col-lg-2">
						<ul class="nav flex-column flex-nowrap mx-1 footer-links">
								<li class="nav-item mr-2 mb-2">
									<a class="oadh-text-tertiary-color" href="/cms/acerca-de{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.acercade') }}</a>
								</li>
								<li class="nav-item mr-2 mb-2">
									<a class="oadh-text-tertiary-color" href="/cms/derechos/vida{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.derechos') }}</a>
								</li>
								<li class="nav-item mr-2 mb-2">
									<a class="oadh-text-tertiary-color" href="/cms/publicaciones{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.publicaciones') }}</a>
								</li>
								<li class="nav-item mr-2 mb-2">
									<a class="oadh-text-tertiary-color" href="/cms/medios-de-prensa{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.prensa') }}</a>
								</li>
								<li class="nav-item mr-2 mb-2">
									<a class="oadh-text-tertiary-color" href="/cms/publicaciones{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.recomendaciones') }}</a>
								</li>
								<li class="nav-item mr-2 mb-2">
									<a class="oadh-text-tertiary-color" href="/cms/videos{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.videos') }}</a>
								</li>
								<li class="nav-item mr-2 mb-2">
									<a class="oadh-text-tertiary-color" href="/cms/actividades{{ '?lang=' . $lang }}">{{ Config::get('system-lang.' . $lang . '.actividades') }}</a>
								</li>
							</ul>
				</div>
				<div class="col-sm-4 col-md-7 col-lg-5">
					<div class="text-left oadh-text-tertiary-color about">
						<h4 data-key="P002" class="editable-element mx-1">{!! $keyValues['P002'] !!}</h4>
						<p data-key="P003" class="editable-element"><i class="fa fa-envelope mx-1 oadh-text-primary-color"></i>{!! $keyValues['P003'] !!}</p>
						<p data-key="P004" class="editable-element"><i class="fa fa-phone mx-1 oadh-text-primary-color"></i>{!! $keyValues['P004'] !!}</p>
						<p data-key="P005" class="editable-element"><i class="fa fa-map-marker-alt mx-1 oadh-text-primary-color"></i>{!! $keyValues['P005'] !!}</p>
					</div>
				</div>
				<div class="col-sm-3 col-md-12 col-lg-2">
						<div class="input-group my-2">
							<input type="text" name="email" id="footer-email" class="form-control input-email2">
						</div>
						<button id="btn-footer-email" class="btn oadh-bg-secondary-color oadh-text-tertiary-color w-100 btn-email2" type="button" name="btn-footer-email">{{ Config::get('system-lang.' . $lang . '.suscribite') }}</button>
				</div>
			</div>
		</div>
	</footer>
