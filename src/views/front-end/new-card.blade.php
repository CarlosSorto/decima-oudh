@extends('decima-oadh::front-end/base')
@section('container')
<style>
	.oadh-bottom-border {
		border-bottom: 1px solid;
		border-color: rgb(255, 152, 21) !important;
	}

	form .oadh-outline-none:focus {
		outline: none;
		box-shadow: none;
	}

	input[type="text"].oadh-outline-none:focus {
		border-color: rgb(0, 22, 70);
	}

	form .btn:hover {
		background-color: rgba(0, 22, 70, 0.9) !important;
	}

	.oadh-dropdown-toggle {
		white-space: nowrap;
	}

	.oadh-dropdown-toggle:active {
		color: rgb(255, 152, 21) !important;
	}

	.oadh-dropdown-toggle::after {
		display: inline-block;
		margin-left: .255em;
		vertical-align: .255em;
		content: "";
		border-top: .3em solid;
		border-right: .3em solid transparent;
		border-bottom: 0;
		border-left: .3em solid transparent;
	}

	.oadh-input-group-text {
		background-color: rgb(0, 22, 70) !important;
		border: 0 !important;
		color: white !important;
	}

	.oadh-input-group-text-border-right {
		border-right: 0.125rem solid white !important;
	}

	.oadh-border-card {
		border-color: rgb(0, 22, 70);
	}

	a.oadh-text-primary-color:hover {
		color: rgba(0, 22, 70, 0.7) !important;
	}

	.pagination>.page-item>a.page-link {
		color: rgba(0, 22, 70, 0.7) !important;
	}

	.badge-primary {
		background-color: rgb(0, 22, 70);
	}

	.table-striped>tbody>tr:nth-of-type(odd) {
		background-color: rgba(255, 152, 21, 0.5);
	}

	.table-striped>tbody>tr:nth-of-type(even) {
		background-color: rgba(255, 152, 21, 0.8);
	}

	.thead-primary {
		background-color: rgb(0, 22, 70);
		color: white;
	}

	.bootstrap-select>button {
		color: #495057 !important;
		background-color: #fff !important;
		border: 1px solid #ced4da !important;
		border-radius: .25rem !important;
	}

	.bootstrap-select>button:hover {
		color: #495057 !important;
		background-color: #fff !important;
	}

	.actions-btn:hover {
		color: white !important;
	}

	.pagination>.page-item.active>a.page-link {
		color: white !important;
		background-color: rgb(0, 22, 70) !important;
	}

	.file-man-box {
		padding: 20px;
		border: 1px solid #e3eaef;
		border-radius: 5px;
		position: relative;
		margin-bottom: 20px
	}

	.file-man-box .file-img-box {
		line-height: 120px;
		text-align: center
	}

	.file-man-box .file-img-box i {
		color: rgb(0, 22, 70);
	}

	.file-man-box .file-download {
		font-size: 32px;
		color: #98a6ad;
		position: absolute;
		right: 10px
	}

	.file-man-box .file-download {
		font-size: 32px;
		color: #98a6ad;
		position: absolute;
		right: 10px
	}

	.file-man-box .file-download:hover {
		color: #313a46
	}

	.file-man-box .file-man-title {
		padding-right: 25px
	}

	.file-man-box:hover {
		-webkit-box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02);
		box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02)
	}

	.file-man-box:hover .file-close {
		visibility: visible
	}

	.text-overflow {
		text-overflow: ellipsis;
		white-space: nowrap;
		display: block;
		width: 100%;
		overflow: hidden;
	}
</style>
@section('javascript')
<script type="text/javascript">
	$(document).ready(function()
		{
      $('body').on('click', '.oadh-news-single-download', function()
      {
        let recId = $(this).attr('data-id')
        if (empty(recId)) 
        {
          return;
        }

        $('#oadh-news-export-data-pdf').val((!empty(recId)) ? JSON.stringify(recId) : JSON.stringify(''));

        $('#oadh-news-export-form-pdf').submit();
      })

			$(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })

			$('.oadh-news-file').click(function()
			{
				let data = JSON.stringify($(this).data());

				$('#oadh-news-download-file').val(data)

				$('#oadh-news-download-file-form').submit();
			})
		})
</script>
@endsection
<div class="container my-4">
	<div class="row">
		<div class="col-md-12 my-4">
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<thead class="thead-primary">
						<th scope="col" colspan="6" class="text-center">{!! $keyValues['N001'] !!}</th>
					</thead>
					<tbody>
						<tr>
							<th>{{ Config::get('system-lang.' . $lang . '.fecha') }}</th>
							<td colspan="2">
								{{\Carbon\Carbon::createFromFormat('Y-m-d', $new['date'])->format(Lang::get('form.phpShortDateFormat'))}}
							</td>
							<th >{!! Config::get('system-lang.' . $lang . '.mediocomunicacion') !!}</th>
							<td colspan="2">{{$new['digital_media']}}</td>
						</tr>
						<tr>
							<th >{{ Config::get('system-lang.' . $lang . '.titular') }}</th>
							<td colspan="5" class="text-justify">{{$new['title']}}</td>
						</tr>
						<tr>
							<th>{{ Config::get('system-lang.' . $lang . '.resumen') }}</th>
							<td colspan="5" class="text-justify">{{$new['summary']}}</td>
						</tr>
						<tr>
							<th >{{ Config::get('system-lang.' . $lang . '.lugarhechos') }}</th>
							<td colspan="2">{{$new['context']}}</td>
							<th >{{ Config::get('system-lang.' . $lang . '.fuentes') }}</th>
							<td class="text-justify" colspan="2">{{$new['source']}}</td>
						</tr>
						<tr>
							<th >{{ Config::get('system-lang.' . $lang . '.derechoshumanos') }}</th>
							<td class="text-justify" colspan="5">
								<ul class="list-group" style="list-style-type: none;">
								@foreach ($new['rights'] as $right)
										<li class="list-item">
											<span class="badge badge-pill badge-primary">{{$right}}</span>
										</li>
										@endforeach
									</ul>
							</td>
						</tr>
						<tr>
							<th >{{ Config::get('system-lang.' . $lang . '.temas') }}</th>
							<td class="text-justify" colspan="5">{{$new['topics']}}</td>
						</tr>
						<tr>
							<th >{{ Config::get('system-lang.' . $lang . '.hechos') }}</th>
							<td class="text-justify" colspan="5">{{$new['violated_fact']}}</td>
						</tr>
						<tr>
							<th >{{ Config::get('system-lang.' . $lang . '.hipotesis') }}</th>
							<td class="text-justify" colspan="5">{{$new['procedures']}}</td>
						</tr>
						<tr>
							<th >{{ Config::get('system-lang.' . $lang . '.poblacion') }}</th>
							<td class="text-justify" colspan="2">{{$new['affected']}}</td>
							<th >{{ Config::get('system-lang.' . $lang . '.tipoarma') }}</th>
							<td class="text-justify" colspan="2">{{$new['weapon']}}</td>
						</tr>
						<tr>
							<th>{!! Config::get('system-lang.' . $lang . '.victimas') !!}</th>
							<td colspan="2" class="text-justify">{{$new['victim']}}</td>
							<th >{!! Config::get('system-lang.' . $lang . '.victimarios') !!}</th>
							<td colspan="2" class="text-justify">{{$new['victimizer']}}</td>
						</tr>
						<tr>
							<th >{!! Config::get('system-lang.' . $lang . '.enlace') !!}</th>
							<td colspan="5" class="text-justify" style="vertical-align: middle">
							@if (!empty($new['new_link']))
									<a href="{{$new['new_link']}}" target="_blank">{{$new['new_link']}}</a>
							@else
									{!! Config::get('system-lang.' . $lang . '.noaplica') !!}
							@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		@if (!empty($new['files']))
		<div class="col-md-12">
			<h4 class="oadh-text-primary-color text-center editable-element">{{ Config::get('system-lang.' . $lang . '.resbusquedas') }}</h4>
			<div class="d-flex flex-row">
				@foreach ($new['files'] as $file)
				<div class="col-sm-12 col-md-4 col-lg-3">
					<div class="file-man-box">
						<div class="file-img-box">
							<a href="#" class="oadh-news-file" data-id="{{$file['file_id']}}" data-name="{{$file['name']}}"
								data-type="{{$file['system_type']}}" data-route="{{$file['system_route']}}"><i
									class="{{$file['icon']}} fa-5x"></i></a>
						</div>
						<div class="file-man-title">
							<h5 class="mb-0 text-overflow" data-toggle="tooltip" data-placement="top" title="{{$file['name']}}">
								{{$file['name']}}</h5>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		@endif
		<div class="col-md-12">
			<button class="btn oadh-bg-secondary-color oadh-text-tertiary-color float-right ml-3 oadh-news-single-download"
				data-id="{{$new['id']}}">{{ Config::get('system-lang.' . $lang . '.descargar') }}</button>
			<a href="{{url('cms/medios-de-prensa')}}"
				class="btn oadh-bg-primary-color oadh-text-tertiary-color float-right">{{ Config::get('system-lang.' . $lang . '.regresar') }}</a>
		</div>
	</div>
</div>
<form action="{{url('/cms/medios-de-prensa/download-single')}}" method="POST" id="oadh-news-export-form-pdf"
	class="d-none">
	<input type="hidden" name="oadh-news-export-data-pdf" id="oadh-news-export-data-pdf">
	<input type="hidden" name="lang" id="oadh-news-export-data-lang" value="{{ $lang }}">
</form>
<form action="{{url('/cms/medios-de-prensa/download-file')}}" method="POST" id="oadh-news-download-file-form"
	class="d-none" target="_blank">
	<input type="hidden" name="oadh-news-download-file" id="oadh-news-download-file">
	<input type="hidden" name="lang" id="oadh-news-download-lang" value="{{ $lang }}">
</form>
@parent
@stop