/**
 * Handle server exceptions
 *
 * @param object jqXHR
 * @param string id
 *
 *  @returns void
 */
function handleServerExceptions(jqXHR, id, alertAsFirstChild)
{
	alertAsFirstChild = (alertAsFirstChild == undefined ? true : alertAsFirstChild);

	// console.log(jqXHR);

	try
	{
		response = JSON.parse(jqXHR.responseText);
		response = response.error.type;
		// console.log('1');
	}
	catch (e)
	{
		// console.log('2');
		response = $.trim(jqXHR.responseText);
	}

	switch (response)
	{
		case 'TokenMismatchException':
			alert(lang.tokenMismatchException);
			window.location.reload();
			return;
			break;
		// case 'Kwaai\\Security\\Exceptions\\AuthenticationException':
		case 'Unauthorized.':
			alert(lang.authenticationException);
			window.location.reload();
			return;
			break;
		default:
			if(alertAsFirstChild)
			{
				$('#' + id).showAlertAsFirstChild('alert-danger', lang.defaultErrorMessage, 7000);
			}
			else
			{
				$('#' + id).showAlertAfterElement('alert-danger alert-custom', lang.defaultErrorMessage, 7000);
			}

	}

	$('#app-loader').addClass('hidden hidden-xs-up');
	enableAll();
}
