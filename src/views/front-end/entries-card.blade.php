@if(!empty($entries[$entryType]))
  {{-- @if (!empty($entries[$entryType]['highlight']))
    <div class="row mt-5">
        @foreach ($entries[$entryType]['highlight'] as $entry)
          <div class="col-sm-8">
            <img src="{{$entry['image'] }}" alt="" class="w-100 rounded img-fluid">
          </div>
          <div class="col-sm-4">
            <p class="font-weight-bold title-entry-text">{{$entry['title'] }}</p>
            <p class="text-justify entry-text">{{$entry['description'] }}</p>
            <a href="{{$entry['download'] }}" target="_blank"><button class="btn oadh-bg-primary-color oadh-text-white-color w-50 btn-block float-right btn-download" data-publication-id="{{$entry['id'] }}"><span>{{ Config::get('system-lang.' . $lang . '.descargar') }}</span></button></a>
          </div>
        @endforeach
    </div>
  @endif --}}

  <div class="row">
    @if (!empty($entries[$entryType]['publications']))
      <div class="col-sm-12">
        @php
        $count = 0
        @endphp
        @foreach ($entries[$entryType]['publications'] as $publication)
          @if ($count == 0)
            <div class="card-group mt-5">
          @endif
          @if ($count > 0 && $count % 2 == 0)
            </div>
            <div class="card-group mt-5">
          @endif
            {{-- <div class="card {{($count % 2 == 0) ? 'oadh-mr-5' : ''}}"> --}}
            <div class="card">
              <div class="text-center">
                <img src="{{$publication['image'] }}" class="rounded img-fluid">
              </div>
              <div class="card-body">
                <h5 class="card-title">{{$publication['title'] }}</h5>
                <p class="card-text text-justify">{{$publication['description'] }}</p>
                <div class="btn-group float-right" role="group">
                  @if (!empty($publication['summary_url']))
                    <button class="btn oadh-bg-primary-color oadh-text-white-color btn-summary" data-publication-id="{{$publication['id'] }}" onclick="$('#ezview-pub-summary-{{$publication['id'] }}').click();">
                      <span>{{ Config::get('system-lang.' . $lang . '.verresumen') }}</span>
                    </button>
                    <img id="ezview-pub-summary-{{$publication['id'] }}" href="{{ $publication['summary_url'] }}" style="display:none;width:100%;height:auto;"  class="ezview">
                  @endif
                  @if (!empty($publication['infographic_url']))
                    <button class="btn oadh-bg-primary-color oadh-text-white-color btn-infographic" data-publication-id="{{$publication['id'] }}"  onclick="$('#ezview-pub-infographic-{{$publication['id'] }}').click();">
                      <span>{{ Config::get('system-lang.' . $lang . '.verinfografico') }}</span>
                    </button>
                    <img id="ezview-pub-infographic-{{$publication['id'] }}" href="{{ $publication['infographic_url'] }}" style="display:none;width:100%;height:auto;" class="ezview">
                  @endif
                  <a href="{{$publication['download'] }}" target="_blank" class="btn oadh-bg-secondary-color oadh-text-white-color btn-download" data-publication-id="{{$publication['id'] }}">
                    <span>{{ Config::get('system-lang.' . $lang . '.descargar') }}</span>
                  </a>
                </div>

                
                {{-- <a href="{{$publication['download'] }}" target="_blank">
                  <button class="btn oadh-bg-primary-color oadh-text-white-color w-30 btn-block float-right btn-summary" data-publication-id="{{$publication['id'] }}">
                    <span>{{ Config::get('system-lang.' . $lang . '.verresumen') }}</span>
                  </button>
                </a>
                <a href="{{$publication['download'] }}" target="_blank">
                  <button class="btn oadh-bg-primary-color oadh-text-white-color w-30 btn-block float-right btn-infographic" data-publication-id="{{$publication['id'] }}">
                    <span>{{ Config::get('system-lang.' . $lang . '.verinfografico') }}</span>
                  </button>
                </a>
                <a href="{{$publication['download'] }}" target="_blank">
                  <button class="btn oadh-bg-secondary-color oadh-text-white-color w-30 btn-block float-right btn-download" data-publication-id="{{$publication['id'] }}">
                    <span>{{ Config::get('system-lang.' . $lang . '.descargar') }}</span>
                  </button>
                </a> --}}

              </div>
            </div>
            @php
              $count++
            @endphp
            @endforeach
        </div>
      </div>
    @endif
  </div>
  {{-- 
  @else
    <div class="row mt-5 justify-content-center">
        <div class="row">
            <div class="col-sm-12">
                <img src="{{ URL::asset('oadh/publicaciones/boletines-proximamente.png') }}" alt="" class="img-fluid">
              </div>
        </div>
    </div> 
  --}}
@endif