@extends('decima-oadh::front-end/base')
@section('container')

<style>
  .clickable {
    cursor: pointer;
  }

  .oadh-bottom-border {
    border-bottom: 1px solid;
    border-color: rgb(255, 152, 21) !important;
  }

  #oadh-rdt-search-group>.oadh-outline-none:focus {
    outline: none;
    box-shadow: none;
  }

  input[type="text"].oadh-outline-none:focus {
    border-color: rgb(0, 22, 70);
  }

  #oadh-rdt-search-group .oadh-rdt-search-control.oadh-bg-primary-color:hover {
    background-color: rgba(0, 22, 70, 0.8) !important;
  }

  #oadh-rdt-search-group .oadh-rdt-search-control.oadh-bg-secondary-color:hover {
    background-color: rgba(255, 152, 21, 0.8) !important;
  }

  .oadh-dropdown-toggle {
    white-space: nowrap;
  }

  .oadh-dropdown-toggle:active {
    color: rgb(255, 152, 21) !important;
  }

  .oadh-dropdown-toggle::after {
    display: inline-block;
    margin-left: .255em;
    vertical-align: .255em;
    content: "";
    border-top: .3em solid;
    border-right: .3em solid transparent;
    border-bottom: 0;
    border-left: .3em solid transparent;
  }

  .oadh-input-group-text {
    background-color: rgb(0, 22, 70) !important;
    color: white !important;
  }

  p.input-group-text {
    background-color: rgb(0, 22, 70) !important;
    color: white !important;
  }

  .oadh-input-group-text-border-right {
    border-right: 0.125rem solid white !important;
  }

  .oadh-border-card {
    border-color: rgb(0, 22, 70);
  }

  a.oadh-text-primary-color:hover {
    color: rgba(0, 22, 70, 0.7) !important;
  }

  .pagination>.page-item>a.page-link {
    color: rgba(0, 22, 70, 0.7) !important;
  }

  .badge-primary {
    background-color: rgb(0, 22, 70);
  }

  .bootstrap-select>button {
    color: #495057 !important;
    background-color: #fff !important;
    border: 1px solid #ced4da !important;
    border-radius: .25rem !important;
  }

  .bootstrap-select>button:hover {
    color: #495057 !important;
    background-color: #fff !important;
  }

  .actions-btn:hover {
    color: white !important;
    background-color: rgb(0, 22, 70) !important;
  }

  .pagination>.page-item.active>a.page-link {
    color: white !important;
    background-color: rgb(0, 22, 70) !important;
  }

  .bootstrap-select .dropdown-menu {
    width: 100%;
  }

  .bootstrap-select .dropdown-menu li a {
    white-space: normal;
  }

  .bootstrap-select .dropdown-menu li a:focus {
    color: white !important;
    background-color: rgb(0, 22, 70) !important;
  }

  .dropdown-item.active,
  .dropdown-item:active {
    color: #fff;
    text-decoration: none;
    background-color: rgb(0, 22, 70) !important;
  }

  .clickable a {
    text-decoration: none;
    color: rgb(0, 22, 70);
  }

  .clickable a:hover {
    text-decoration: none;
    color: rgba(0, 22, 70, 0.9);
  }
</style>

@section('javascript')
  <script type="text/javascript">
    var oadhRdtSystems = {!!json_encode($systems)!!};
    function getSearchFormData()
    {
      let filters = [];
      let data = '';

      if(!$('#oadh-rdt-search-text').isEmpty())
      {
        filters = [...filters, {'field': '', 'op': 'like', 'data': $('#oadh-rdt-search-text').val(), 'id': '#oadh-rdt-search-text'}]
      }

      // if(!empty($('#oadh-rdt-date').val()))
      // {
      //   filters = [...filters, {'field': 'rec.date', 'op': 'eq', 'data': $('#oadh-rdt-date').val(), 'id': '#oadh-rdt-date'}]
      // }

      if(!empty($('#oadh-rdt-date-from').val()) || !empty($('#oadh-rdt-daterange-to').val()))
      {
        filters = [...filters, {'field': 'rec.date', 'op': 'date_range', 'data': null, 'data_from': $('#oadh-rdt-date-from').val(), 'data_to': $('#oadh-rdt-date-to').val(), 'id_from': '#oadh-rdt-date-from', 'id_to': '#oadh-rdt-date-to'}]
      }

      if(!$('#oadh-rdt-system').isEmpty())
      {
        filters = [...filters, {'field': 'rec.system', 'op': 'eq_in', 'data': $('#oadh-rdt-system').val(), 'id': '#oadh-rdt-system'}]
      }

      if(!$('#oadh-rdt-mechanism').isEmpty())
      {
        filters = [...filters, {'field': 'rec.mechanism', 'op': 'eq_in', 'data': $('#oadh-rdt-mechanism').val(), 'id': '#oadh-rdt-mechanism'}]
      }

      if(!$('#oadh-rdt-institution').isEmpty())
      {
        filters = [...filters, {'field': 'rec.institution', 'op': 'institution_in', 'data': $('#oadh-rdt-institution').val(), 'id': '#oadh-rdt-institution'}]
      }

      if(!$('#oadh-rdt-right').isEmpty())
      {
        filters = [...filters, {'field': 'rec.right', 'op': 'right_in', 'data': $('#oadh-rdt-right').val(), 'id': '#oadh-rdt-right'}]
      }

      if(empty(filters))
      {
        return JSON.stringify({ '_token': $('#app-token').val(), 'lang': '{{ $lang }}'});
      }

      return JSON.stringify({ '_token': $('#app-token').val(), 'lang': '{{ $lang }}', filters});
    }

    function getFilteredData()
    {
      data = localStorage.getItem('dataFilters')
      url =  localStorage.getItem('dataUrl')

      $.ajax({
          type: 'POST',
          dataType: 'json',
          url: url,
          data: data,
          beforeSend: function()
          {
            $('#app-loader').css({ display: "inherit"});
            $('#main-panel-fieldset').attr('disabled', true);
          }, 
        })
      .done(function (data)
      {
        $('#app-loader').css({ display: "none"});
        $('#main-panel-fieldset').attr('disabled', false);
        $('#oadh-rdt-result-title').hasClass('d-none') && $('#oadh-rdt-result-title').removeClass('d-none')
        $('#oadh-rdt-recommendations').html(data.html);
        $('#oadh-rdt-recommendations').highlight($('#oadh-rdt-search-text').val());
      })
      .always(function()
      {
        let dataFilters = !empty(localStorage.getItem('dataFilters')) ? JSON.parse(localStorage.getItem('dataFilters')) : null;
        let {filters} = dataFilters;

        if(!$('#oadh-rdt-collapse-filter-form').hasClass('show'))
        {
          $('#oadh-rdt-btn-collapse-filter').click();
        }

        if(!empty(filters))
        {
          filters.forEach(element => {
            if(!empty(element.data))
            {
              $(element.id).val(element.data)

              if(element.id == '#oadh-rdt-system')
              {
                $('#oadh-rdt-system').trigger('change');
              }
            }

            if(element.op == 'date_range')
            {
              $(element.id_from).val(element.data_from);
              $(element.id_to).val(element.data_to);
            }
          });
        }

        $('.selectpicker').selectpicker('refresh')
      })
    }

    function loadLastPage()
    {
      let data = !empty(localStorage.getItem('dataFilters')) ? JSON.parse(localStorage.getItem('dataFilters')) : null;
      let pagePos = !empty(localStorage.getItem('dataPagePos')) ? JSON.parse(localStorage.getItem('dataPagePos')) : null;

      if(!empty(data))
      {
        getFilteredData();
      }

      if(!empty(pagePos) && pagePos > 1)
      {
        getFilteredData()
      }
    }

    $(document).ready(function()
    {
      (function($) {
          $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
              if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
              } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
              } else {
                this.value = "";
              }
            });
          };
        }(jQuery));
      //recommendations

      $('#oadh-rdt-date-from, #oadh-rdt-date-to').inputFilter(function(value)
      {
        return /^\d*$/.test(value);
      })

      $('#oadh-rdt-btn-search').click(function() 
      {
        let data = getSearchFormData();
        localStorage.setItem('dataUrl', $('#oadh-rdt-search-form').attr('data-action'))
        localStorage.setItem('dataFilters', data);
        localStorage.setItem('dataPagePos', null);
        getFilteredData()
      })

      // $('.oadh-rdt-btn-change-date-datetime').click(function() 
      // {
      //   $('.oadh-rdt-btn-change-date-datetime').each(function(index, element)
      //   {
      //     if($(`#${$(element).attr('data-visibility')}`).hasClass('d-none'))
      //     {
      //       $(`#${$(element).attr('data-visibility')}`).removeClass('d-none') 
      //     }
      //     else
      //     {
      //       $(`#${$(element).attr('data-visibility')}`).addClass('d-none') 
      //       $(`#${$(element).attr('data-visibility')}`).find('input').val('')
      //     }
      //   });
      // })

      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })

      $('#oadh-rdt-btn-clear-filter').click(function() 
      {
        $('.bs-deselect-all').click();
        $('#oadh-rdt-collapse-filter-form').find('input').val('');
        $('#oadh-rdt-search-text').val('');

        localStorage.removeItem('dataUrl');
        localStorage.removeItem('dataFilters');
        localStorage.removeItem('dataPagePos');
      })

      // $('#oadh-rdt-btn-collapse-filter').click(function() {
      //   if($('#oadh-rdt-collapse-filter-form').hasClass('collapse show'))
      //   {
      //     $('.bs-deselect-all').click();
      //     $('#oadh-rdt-collapse-filter-form').find('input').val('');
      //   }
      // })

      $('body').on('click', '.pagination a', function(e)
      {
        e.preventDefault();

        
        var page = $(this).attr('href').slice($(this).attr('href').indexOf('?'));
        var pagePos = page.slice(page.indexOf('=') + 1);
        var url = `${$('#app-url').val()}/cms/recomendaciones/advanced-smt-rows${page}`;

        localStorage.setItem('dataUrl', url);
        localStorage.setItem('dataPagePos', pagePos);

        getFilteredData();
      })

      $('body').on('click', '.oadh-rdt-single-download', function()
      {
        let recId = $(this).attr('data-id')
        if (empty(recId)) 
        {
          return;
        }

        $('#oadh-rdt-export-data-pdf').val((!empty(recId)) ? JSON.stringify(recId) : JSON.stringify(''));
        $('#oadh-rdt-export-form-pdf').submit();
      })

      $('#oadh-rdt-download').click(function()
      {
        let data = getSearchFormData();

        $('#oadh-rdt-export-data').val((!empty(data)) ? data : JSON.stringify([]));

        $('#oadh-rdt-export-form').submit();
      })

      $('.selectpicker').selectpicker({
        liveSearch: true,
        actionsBox: true,
        selectedTextFormat: 'count > 2',
        deselectAllText: 'Deseleccionar todos',
        selectAllText: 'Seleccionar todos',
        title: 'Nada seleccionado',
      });

      $('#oadh-rdt-search-text').onEnter(function()
      {
        if(empty($('#oadh-rdt-search-text').val())) return;
        $('#oadh-rdt-btn-search').click();
      })

      $('#oadh-rdt-system').change(function()
      {
        let rec = $(this).val();
        let mechanisms = [];
        rec.forEach(val => {
          mechanisms = [...mechanisms, oadhRdtSystems[val].mechanism]
        });

        mechanisms = [].concat.apply([], mechanisms);

        $('#oadh-rdt-mechanism').html('');

        mechanisms.forEach(mechanism => {
          $('#oadh-rdt-mechanism').append(`<option value="${mechanism}">${mechanism}</option>`)
        });

        $('#oadh-rdt-mechanism').selectpicker('refresh');

      });

      setTimeout(() => {
        loadLastPage()
      }, 500);
    })
  </script>
@endsection

<div class="container my-4">
  <h3 class="oadh-text-primary-color text-center editable-element" data-key="R001">{!! $keyValues['R001'] !!}</h3>
  <p class="text-justify editable-element" data-key="R002">
    {!! $keyValues['R002'] !!}
  </p>
  @include('decima-oadh::front-end/search-form')
  <div class="row">
    <div class="col-sm-12 mt-5">
      <h3 class="d-none" id="oadh-rdt-result-title">{{ Config::get('system-lang.' . $lang . '.resbusquedas') }}</h3>
    </div>
  </div>
  <div id="oadh-rdt-recommendations" class="row">
    @include('decima-oadh::front-end/recommendation-cards')
  </div>
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn oadh-bg-secondary-color oadh-outline-none oadh-text-white-color float-right"
        id="oadh-rdt-download" type="button">{{ Config::get('system-lang.' . $lang . '.descargartodos') }}</button>
    </div>
  </div>
</div>
<form action="{{url('/cms/recomendaciones/download-all')}}" method="POST" id="oadh-rdt-export-form" class="d-none">
  <input type="hidden" name="oadh-rdt-export-data" id="oadh-rdt-export-data">
  <input type="hidden" name="lang" id="oadh-rdt-export-data-lang" value="{{ $lang }}">
</form>
<form action="{{url('/cms/recomendaciones/download-single')}}" method="POST" id="oadh-rdt-export-form-pdf" class="d-none">
  <input type="hidden" name="oadh-rdt-export-data-pdf" id="oadh-rdt-export-data-pdf">
  <input type="hidden" name="lang" id="oadh-rdt-export-data-pdf-lang" value="{{ $lang }}">
</form>
@parent
@stop