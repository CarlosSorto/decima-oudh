@php
  $count = 0
@endphp
@if(!empty($entries[$entryType]))
  @if(!empty($entries[$entryType]['highlight']))
    <div class="card">
      <div class="card-body">
        <div class="row">
          @if ($count == 0)
            @foreach ($entries[$entryType]['highlight'] as $publication)
              <div class="col-sm-12 text-center">
                <img src="{{$publication['image'] }}" alt="" class="rounded img-fluid">
              </div>
              <div class="card-body">
                {{-- <p class="font-weight-bold title-entry-text">{{$entry['title'] }}</p>
                <p class="text-justify entry-text">{{$entry['description'] }}</p> --}}

                <h5 class="card-title">{{$publication['title'] }}</h5>
                <p class="card-text text-justify">{{$publication['description'] }}</p>
                <div class="btn-group float-right" role="group">
                  @if (!empty($publication['summary_url']))
                    <button class="btn oadh-bg-primary-color oadh-text-white-color btn-summary" data-publication-id="{{$publication['id'] }}" onclick="$('#ezview-pub-summary-{{$publication['id'] }}').click();">
                      <span>{{ Config::get('system-lang.' . $lang . '.verresumen') }}</span>
                    </button>
                    <img id="ezview-pub-summary-{{$publication['id'] }}" href="{{ $publication['summary_url'] }}" style="display:none;width:100%;height:auto;"  class="ezview">
                  @endif
                  @if (!empty($publication['infographic_url']))
                    <button class="btn oadh-bg-primary-color oadh-text-white-color btn-infographic" data-publication-id="{{$publication['id'] }}"  onclick="$('#ezview-pub-infographic-{{$publication['id'] }}').click();">
                      <span>{{ Config::get('system-lang.' . $lang . '.verinfografico') }}</span>
                    </button>
                    <img id="ezview-pub-infographic-{{$publication['id'] }}" href="{{ $publication['infographic_url'] }}" style="display:none;width:100%;height:auto;" class="ezview">
                  @endif
                  <a href="{{$publication['download'] }}" target="_blank" class="btn oadh-bg-secondary-color oadh-text-white-color btn-download" data-publication-id="{{$publication['id'] }}">
                    <span>{{ Config::get('system-lang.' . $lang . '.descargar') }}</span>
                  </a>
                </div>
              </div> 
              @php
                $count++
              @endphp
            @endforeach
          @endif
        </div>
      </div>
    </div>
  @endif
@else
  @if ($count == 0)
    <div class="card">
      <div class="card-body">
        <div class="card-body text-center">
          <img src="{{ URL::asset('oadh/publicaciones/investigaciones-proximamente.png') }}" alt="" class="img-fluid">
        </div>
      </div>
    </div>
  @endif    
@endif