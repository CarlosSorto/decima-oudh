@extends('decima-oadh::front-end/base')

@section('mobile-menu')
<div id="left-rights-on-mobile-menu">
	<ul class="my-3" id="nav-mobile-list">
		<li>
			<a id="op-m-1" href="#"data-link-id="op-1">{!! Config::get('system-lang.' . $lang . '.vidadop1') !!}</a>
		</li>
		<li>
			<a id="op-m-2" href="#"data-link-id="op-2">{!! Config::get('system-lang.' . $lang . '.vidadop2') !!}</a>
		</li>
		<li>
			<a id="op-m-8" href="#"data-link-id="op-8">{!! Config::get('system-lang.' . $lang . '.vidadop8') !!}</a>
		</li>
		<li>
			<a id="op-m-9" href="#"data-link-id="op-9">{!! Config::get('system-lang.' . $lang . '.vidadop9') !!}</a>
		</li>
		<li>
			<a id="op-m-6" href="#"data-link-id="op-6">{!! Config::get('system-lang.' . $lang . '.vidadop6') !!}</a>
		</li>
		<li>
			<a id="op-m-7" href="#"data-link-id="op-7">{!! Config::get('system-lang.' . $lang . '.vidadop7') !!}</a>
		</li>
		<li>
			<a id="op-m-19" href="#"data-link-id="op-19">{!! Config::get('system-lang.' . $lang . '.vidadop19') !!}</a>
		</li>
		<li>
			<a id="op-m-11" href="#"data-link-id="op-11">{!! Config::get('system-lang.' . $lang . '.vidadop11') !!}</a>
		</li>
		<li>
			<a id="op-m-3" href="#"data-link-id="op-3">{!! Config::get('system-lang.' . $lang . '.vidadop3') !!}</a>
		</li>
		<li>
			<a id="op-m-4" href="#"data-link-id="op-4">{!! Config::get('system-lang.' . $lang . '.vidadop4') !!}</a>
		</li>
		<li>
			<a id="op-m-10" href="#"data-link-id="op-10">{!! Config::get('system-lang.' . $lang . '.vidadop10') !!}</a>
		</li>
		<li>
			<a id="op-m-20" href="#"data-link-id="op-20">{!! Config::get('system-lang.' . $lang . '.vidadop20') !!}</a>
		</li>
		<li>
			<a id="op-m-12" href="#"data-link-id="op-12">{!! Config::get('system-lang.' . $lang . '.vidadop12') !!}</a>
		</li>
		<li>
			<a id="op-m-5" href="#"data-link-id="op-5">{!! Config::get('system-lang.' . $lang . '.vidadop5') !!}</a>
		</li>
		<li>
			<a id="op-m-13" href="#"data-link-id="op-13">{!! Config::get('system-lang.' . $lang . '.vidadop13') !!}</a>
		</li>
		<li>
			<a id="op-m-14" href="#"data-link-id="op-14">{!! Config::get('system-lang.' . $lang . '.vidadop14') !!}</a>
		</li>
		<li>
			<a id="op-m-15" href="#"data-link-id="op-15">{!! Config::get('system-lang.' . $lang . '.vidadop15') !!}</a>
		</li>
		<li>
			<a id="op-m-22" href="#"data-link-id="op-22">{!! Config::get('system-lang.' . $lang . '.vidadop22') !!}</a>
		</li>
		<li>
			<a id="op-m-17" href="#"data-link-id="op-17">{!! Config::get('system-lang.' . $lang . '.vidadop17') !!}</a>
		</li>
		<li>
			<a id="op-m-21" href="#"data-link-id="op-21">{!! Config::get('system-lang.' . $lang . '.vidadop21') !!}</a>
		</li>
		<li>
			<a id="op-m-18" href="#"data-link-id="op-18">{!! Config::get('system-lang.' . $lang . '.vidadop18') !!}</a>
		</li>
		<li>
			<a id="op-m-23" href="#"data-link-id="op-23">{!! Config::get('system-lang.' . $lang . '.vidadop23') !!}</a>
		</li>
		<li>
			<a id="op-m-16" href="#"data-link-id="op-16">{!! Config::get('system-lang.' . $lang . '.vidadop16') !!}</a>
		</li>
	</ul>
</div>
@endsection

@section('container')
<div class="container">
	<div class="row my-4">
		<div id="sidebar" class="col-md-3 d-none d-lg-block d-md-none d-sm-none">
			<h4 class="menu-title main-title">{!! Config::get('system-lang.' . $lang . '.derechovida') !!}</h4>
			<ul class="nav flex-column flex-nowrap" id="nav-option-list">
				<li class="nav-item">
					<a
						id="op-1"
						class="nav-link"
						href="#life-chart-1"
						role="tab"
						aria-controls="life-chart-1"
						aria-selected="true"
						data-tab-number="1"
						data-context="homicides-incident"
						data-div-id="mapdiv-1"
						data-div-filter-id="mapdiv-filter-1"
						data-div-reference-id="mapdiv-reference-1"
						data-chart-type='map'
						data-grid-id="grid-1"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
						]'
						data-description-key="V001"
						data-description="{{ $keyValues['V001'] }}"
						data-reference-key="V024"
						data-reference="{{ $keyValues['V024'] }}">
							<i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.vidadop1') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						id="op-2"
						class="nav-link"
						href="#life-chart-2"
						role="tab"
						aria-controls="life-chart-2"
						aria-selected="true"
						data-tab-number="2"
						data-context="homicides-rate"
						data-div-id="mapdiv-2"
						data-div-filter-id="mapdiv-filter-2"
						data-div-reference-id="mapdiv-reference-2"
						data-chart-type='map'
						data-grid-id="grid-2"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false}
						]'
						data-description-key="V002"
						data-description="{{ $keyValues['V002'] }}"
						data-reference-key="V025"
						data-reference="{{ $keyValues['V025'] }}"><i
							class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.vidadop2') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-8"
						id="op-8"
						role="tab"
						aria-controls="life-chart-8"
						aria-selected="true"
						data-tab-number="8"
						data-context="homicides-incident-by-age"
						data-div-id="mapdiv-8"
						data-div-filter-id="mapdiv-filter-8"
						data-div-reference-id="mapdiv-reference-8"
						data-chart-type='bar'
						data-grid-id="grid-8"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}"},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"},
							{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
						]'
						data-description-key="V003"
						data-description="{{ $keyValues['V003'] }}"
						data-reference-key="V026"
						data-reference="{{ $keyValues['V026'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop8') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-9"
						id="op-9" role="tab"
						aria-controls="life-chart-9"
						aria-selected="true"
						data-tab-number="9"
						data-context="homicides-rate-by-age"
						data-div-id="mapdiv-9"
						data-div-filter-id="mapdiv-filter-9"
						data-div-reference-id="mapdiv-reference-9"
						data-chart-type='bar'
						data-grid-id="grid-9"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false},
							{"filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}"},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V004"
						data-description="{{ $keyValues['V004'] }}"
						data-reference-key="V027"
						data-reference="{{ $keyValues['V027'] }}"><i
							class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop9') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-6"
						id="op-6"
						role="tab"
						aria-controls="life-chart-6"
						aria-selected="true"
						data-tab-number="6"
						data-context="weapon-type-agressions"
						data-div-id="mapdiv-6"
						data-div-filter-id="mapdiv-filter-6"
						data-div-reference-id="mapdiv-reference-6"
						data-chart-type='pie'
						data-grid-id="grid-6"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}"},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V005"
						data-reference-key="V028"
						data-description="{{ $keyValues['V005'] }}"
						data-reference="{{ $keyValues['V028'] }}"><i class="fas fa-chart-pie"></i> {!! Config::get('system-lang.' . $lang . '.vidadop6') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-7"
						id="op-7"
						role="tab"
						aria-controls="life-chart-7"
						aria-selected="true"
						data-tab-number="7"
						data-context="gender-agressions"
						data-div-id="mapdiv-7"
						data-div-filter-id="mapdiv-filter-7"
						data-div-reference-id="mapdiv-reference-7"
						data-chart-type='pie'
						data-grid-id="grid-7"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"},
							{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
						]'
						data-description-key="V006"
						data-reference-key="V029"
						data-description="{{ $keyValues['V006'] }}"
						data-reference="{{ $keyValues['V029'] }}"><i class="fas fa-chart-pie"></i> {!! Config::get('system-lang.' . $lang . '.vidadop7') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-19"
						id="op-19"
						role="tab"
						aria-controls="life-chart-19"
						aria-selected="true"
						data-tab-number="19"
						data-context="homicides-incident-by-month"
						data-div-id="mapdiv-19"
						data-div-filter-id="mapdiv-filter-19"
						data-div-reference-id="mapdiv-reference-19"
						data-chart-type='bar'
						data-grid-id="grid-19"
						data-filters='[
								{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
								{"filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}"},
								{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
								{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"},
								{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
							]'
						data-description-key="V007"
						data-reference-key="V030"
						data-description="{{ $keyValues['V007'] }}"
						data-reference="{{ $keyValues['V030'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop19') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-11"
						id="op-11"
						role="tab"
						aria-controls="life-chart-11"
						aria-selected="true"
						data-tab-number="11"
						data-context="homicides-incident-by-time-aggression"
						data-div-id="mapdiv-11"
						data-div-filter-id="mapdiv-filter-11"
						data-div-reference-id="mapdiv-reference-11"
						data-chart-type='bar'
						data-grid-id="grid-11"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "gender", "type": "select", "label": "{!! Config::get('system-lang.' . $lang . '.sex') !!}"},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"},
							{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
						]'
						data-description-key="V008"
						data-reference-key="V031"
						data-description="{{ $keyValues['V008'] }}"
						data-reference="{{ $keyValues['V031'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop11') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-3"
						id="op-3"
						role="tab"
						aria-controls="life-chart-3"
						aria-selected="true"
						data-tab-number="3"
						data-context="homicides-of-women"
						data-div-id="mapdiv-3"
						data-div-filter-id="mapdiv-filter-3"
						data-div-reference-id="mapdiv-reference-3"
						data-chart-type='map'
						data-grid-id="grid-3"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
						]'
						data-description-key="V009"
						data-reference-key="V032"
						data-description="{{ $keyValues['V009'] }}"
						data-reference="{{ $keyValues['V032'] }}"><i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.vidadop3') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-4"
						id="op-4"
						role="tab"
						aria-controls="life-chart-4"
						aria-selected="true"
						data-tab-number="4"
						data-context="homicides-rate-of-women"
						data-div-id="mapdiv-4"
						data-div-filter-id="mapdiv-filter-4"
						data-div-reference-id="mapdiv-reference-4"
						data-chart-type='map'
						data-grid-id="grid-4"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": false},
							{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
						]'
						data-description-key="V010"
						data-reference-key="V033"
						data-description="{{ $keyValues['V010'] }}"
						data-reference="{{ $keyValues['V033'] }}"><i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.vidadop4') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-10"
						id="op-10"
						role="tab"
						aria-controls="life-chart-10"
						aria-selected="true"
						data-tab-number="10"
						data-context="homicides-incident-on-women"
						data-div-id="mapdiv-10"
						data-div-filter-id="mapdiv-filter-10"
						data-div-reference-id="mapdiv-reference-10"
						data-chart-type='bar'
						data-grid-id="grid-10"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"},
							{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
						]'
						data-description-key="V011"
						data-reference-key="V034"
						data-description="{{ $keyValues['V011'] }}"
						data-reference="{{ $keyValues['V034'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop10') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-20"
						id="op-20"
						role="tab"
						aria-controls="life-chart-20"
						aria-selected="true"
						data-tab-number="20"
						data-context="homicides-incident-on-women-by-month"
						data-div-id="mapdiv-20"
						data-div-filter-id="mapdiv-filter-20"
						data-div-reference-id="mapdiv-reference-20"
						data-chart-type='bar'
						data-grid-id="grid-20"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"},
							{"filter": "weapon_type", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.tipoarma') }}"}
						]'
						data-description-key="V012"
						data-reference-key="V035"
						data-description="{{ $keyValues['V012'] }}"
						data-reference="{{ $keyValues['V035'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop20') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-12"
						id="op-12" role="tab"
						aria-controls="life-chart-12"
						aria-selected="true"
						data-tab-number="12"
						data-context="homicides-incident-by-illegitimate-aggression-by-year"
						data-div-id="mapdiv-12"
						data-div-filter-id="mapdiv-filter-12"
						data-div-reference-id="mapdiv-reference-12"
						data-chart-type='bar'
						data-grid-id="grid-12"
						data-filters='[
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V013"
						data-reference-key="V036"
						data-description="{{ $keyValues['V013'] }}"
						data-reference="{{ $keyValues['V036'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop12') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-5"
						id="op-5"
						role="tab"
						aria-controls="life-chart-5"
						aria-selected="true"
						data-tab-number="5"
						data-context="homicides-illegitimate-agressions"
						data-div-id="mapdiv-5"
						data-div-filter-id="mapdiv-filter-5"
						data-div-reference-id="mapdiv-reference-5"
						data-chart-type='map' data-grid-id="grid-5"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true}
						]'
						data-description-key="V014"
						data-reference-key="V037"
						data-description="{{ $keyValues['V014'] }}"
						data-reference="{{ $keyValues['V037'] }}"><i class="fas fa-map"></i> {!! Config::get('system-lang.' . $lang . '.vidadop5') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-13"
						id="op-13"
						role="tab"
						aria-controls="life-chart-13"
						aria-selected="true"
						data-tab-number="13"
						data-context="homicides-incident-by-illegitimate-aggression-by-week-day"
						data-div-id="mapdiv-13"
						data-div-filter-id="mapdiv-filter-13"
						data-div-reference-id="mapdiv-reference-13"
						data-chart-type='bar'
						data-grid-id="grid-13"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V015"
						data-reference-key="V038"
						data-description="{{ $keyValues['V015'] }}"
						data-reference="{{ $keyValues['V038'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop13') !!}.
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-14"
						id="op-14"
						role="tab"
						aria-controls="life-chart-14"
						aria-selected="true"
						data-tab-number="14"
						data-context="homicides-incident-by-illegitimate-aggression-by-time"
						data-div-id="mapdiv-14"
						data-div-filter-id="mapdiv-filter-14"
						data-div-reference-id="mapdiv-reference-14"
						data-chart-type='bar'
						data-grid-id="grid-14"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V016"
						data-reference-key="V039"
						data-description="{{ $keyValues['V016'] }}"
						data-reference="{{ $keyValues['V039'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop14') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-15"
						id="op-15"
						role="tab"
						aria-controls="life-chart-15"
						aria-selected="true"
						data-tab-number="15"
						data-context="homicides-incident-by-illegitimate-aggression-by-gang"
						data-div-id="mapdiv-15"
						data-div-filter-id="mapdiv-filter-15"
						data-div-reference-id="mapdiv-reference-15"
						data-chart-type='bar'
						data-grid-id="grid-15"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V017"
						data-reference-key="V040"
						data-description="{{ $keyValues['V017'] }}"
						data-reference="{{ $keyValues['V040'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop15') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-22"
						id="op-22"
						role="tab"
						aria-controls="life-chart-22"
						aria-selected="true"
						data-tab-number="22"
						data-context="homicides-by-deceased"
						data-div-id="mapdiv-22"
						data-div-filter-id="mapdiv-filter-22"
						data-div-reference-id="mapdiv-reference-22"
						data-chart-type='bar'
						data-grid-id="grid-22"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V018"
						data-reference-key="V041"
						data-description="{{ $keyValues['V018'] }}"
						data-reference="{{ $keyValues['V041'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop22') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-17"
						id="op-17"
						role="tab"
						aria-controls="life-chart-17"
						aria-selected="true"
						data-tab-number="17"
						data-context="homicides-incident-by-deceased-cops"
						data-div-id="mapdiv-17"
						data-div-filter-id="mapdiv-filter-17"
						data-div-reference-id="mapdiv-reference-17"
						data-chart-type='bar'
						data-grid-id="grid-17"
						data-filters='[
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V019"
						data-reference-key="V042"
						data-description="{{ $keyValues['V019'] }}"
						data-reference="{{ $keyValues['V042'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop17') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-21"
						id="op-21"
						role="tab"
						aria-controls="life-chart-21"
						aria-selected="true"
						data-tab-number="21"
						data-context="proportional-ratio-on-illegitimate-aggressions"
						data-div-id="mapdiv-21"
						data-div-filter-id="mapdiv-filter-21"
						data-div-reference-id="mapdiv-reference-21"
						data-chart-type='bar'
						data-grid-id="grid-21"
						data-filters='[
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V020"
						data-reference-key="V043"
						data-description="{{ $keyValues['V020'] }}"
						data-reference="{{ $keyValues['V043'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop21') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-18"
						id="op-18"
						role="tab"
						aria-controls="life-chart-18"
						aria-selected="true"
						data-tab-number="18"
						data-context="police-investigated-by-crimes"
						data-div-id="mapdiv-18"
						data-div-filter-id="mapdiv-filter-18"
						data-div-reference-id="mapdiv-reference-18"
						data-chart-type='bar'
						data-grid-id="grid-18"
						data-filters='[
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V021"
						data-reference-key="V044"
						data-description="{{ $keyValues['V021'] }}"
						data-reference="{{ $keyValues['V044'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop18') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-23"
						id="op-23"
						role="tab"
						aria-controls="life-chart-23"
						aria-selected="true"
						data-tab-number="23"
						data-context="officers-investigated-by-type"
						data-div-id="mapdiv-23"
						data-div-filter-id="mapdiv-filter-23"
						data-div-reference-id="mapdiv-reference-23"
						data-chart-type='bar'
						data-grid-id="grid-23"
						data-filters='[
							{"filter": "year", "type": "slider", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "label": "{!! Config::get('system-lang.' . $lang . '.anio') !!}", "range": true},
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V022"
						data-reference-key="V045"
						data-description="{{ $keyValues['V022'] }}"
						data-reference="{{ $keyValues['V045'] }}"><i class="fas fa-chart-bar"></i> {!! Config::get('system-lang.' . $lang . '.vidadop23') !!}
					</a>
				</li>
				<li class="nav-item">
					<a
						class="nav-link"
						href="#life-chart-16"
						id="op-16"
						role="tab"
						aria-controls="life-chart-16"
						aria-selected="true"
						data-tab-number="16"
						data-context="police-investigated-by-homicide-feminicide"
						data-div-id="chartdiv-16"
						data-div-filter-id="mapdiv-filter-16"
						data-div-reference-id="mapdiv-reference-16"
						data-chart-type='dbar'
						data-grid-id="grid-16" data-filters='[
							{"filter": "department", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.departamento') }}"},
							{"filter": "municipality", "type": "select", "label": "{{ Config::get('system-lang.' . $lang . '.municipio') }}"}
						]'
						data-description-key="V023"
						data-reference-key="V046"
						data-description="{{ $keyValues['V023'] }}"
						data-reference="{{ $keyValues['V046'] }}"><i class="fas fa-chart-bar"></i>{!! Config::get('system-lang.' . $lang . '.vidadop16') !!}
					</a>
				</li>
			</ul>
		</div>
		<div id="mobile-menu" class="col-12 text-left d-sm-block d-md-block d-lg-none">
			<h4 class="menu-title">{!! Config::get('system-lang.' . $lang . '.derechovida') !!}</h4>
			<div class="text-center">
				<button type="button" id="btn-mmenu" class="btn btn-secondary  oadh-bg-primary-color btn-lg w-100 my-3"><i class="fas fa-bars"></i>{!! Config::get('system-lang.' . $lang . '.veropciones') !!}</button>
			</div>
		</div>
		<div class="col-lg-9">
			<h4 class="oadh-text-primary-color text-center" id="oadh-chart-title"></h4>
			<div class="card card-filter">
				<div class="card-header">
					<div class="row align-items-center">
						<div class="col-sm-4">
							{!! Config::get('system-lang.' . $lang . '.disponibles') !!}
						</div>
						<div class="col-sm-8 text-right">
              <div class="btn-toolbar justify-content-end" role="toolbar" aria-label="accesibility buttons">
                <div class="btn-group" role="group" aria-label="filters">
                  <button type="button" id="btn-download-xlsx" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-arrow-down"></i> {{ Config::get('system-lang.' . $lang . '.descargar') }}
                  </button>
                </div>
                <div class="btn-group ml-2" role="group" aria-label="filters">
                  <button type="button" id="btn-filter" class="btn btn-secondary  oadh-bg-primary-color btn-sm w-50">
                    <i class="fas fa-filter"></i> {!! Config::get('system-lang.' . $lang . '.aplicar') !!}
                  </button>
                </div>
              </div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="mapdiv-filter-1" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-2" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-3" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-4" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-5" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-6" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-7" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-8" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-9" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-10" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-11" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-12" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-13" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-14" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-15" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-16" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-17" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-18" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-19" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-20" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-21" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-22" style="display: none;" class="mapdiv-filter"></div>
					<div id="mapdiv-filter-23" style="display: none;" class="mapdiv-filter"></div>
					<p id="filter-description" data-key='' class="editable-element"></p>
				</div>
			</div>
			<div class="row align-items-center rights-description">
				<div class="col-lg-6">
					{{-- <img src="{{ URL::asset('oadh/inicios/vida-v3.png') }}" alt="logo" class="d-block w-100 img-fluid mx-auto"> --}}
					<div class="home-rights-container oadh-bg-primary-color">
						<div class="d-flex justify-content-center">
							<div class="rounded-circle home-rights-circle">
								<img src="{{ URL::asset('oadh/inicios/vida-v4.png') }}" alt="vida" class="img-fluid">
							</div>
						</div>
						<div class="mt-3">
							<h3 class="oadh-text-white-color text-center">
								{{ Config::get('system-lang.' . $lang . '.derechovida') }}
							</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<h3 class="oadh-text-primary-color editable-element" data-key="V047">{{ $keyValues['V047'] }}</h3>
					<p class="text-justify editable-element" data-key="V048">
						{!! $keyValues['V048'] !!}
					</p>
					<p class="text-justify editable-element" data-key="V049">
						{!! $keyValues['V049'] !!}
					</p>
				</div>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-1" class="chartdiv"></div>
				<p id="mapdiv-reference-1" class="reference-text editable-element"></p>
				<div id="grid-1">
					<div class="smt-body"></div>
				</div>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-2" class="chartdiv"></div>
				<p id="mapdiv-reference-2" class="reference-text editable-element"></p>
				<div id="grid-2">
					<div class="smt-body"></div>
				</div>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-3" class="chartdiv"></div>
				<p id="mapdiv-reference-3" class="reference-text editable-element"></p>
				<div id="grid-3">
					<div class="smt-body"></div>
				</div>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-4" class="chartdiv"></div>
				<p id="mapdiv-reference-4" class="reference-text editable-element"></p>
				<div id="grid-4">
					<div class="smt-body"></div>
				</div>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-5" class="chartdiv"></div>
				<p id="mapdiv-reference-5" class="reference-text editable-element"></p>
				<div id="grid-5">
					<div class="smt-body"></div>
				</div>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-6" class="chartdiv"></div>
				<p id="mapdiv-reference-6" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart">
				<div id="mapdiv-7" class="chartdiv"></div>
				<p id="mapdiv-reference-7" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-8" role="tabpanel" aria-labelledby="tc-8">
				<div id="mapdiv-8" class="chartdiv"></div>
				<p id="mapdiv-reference-8" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-9" role="tabpanel" aria-labelledby="tc-9">
				<div id="mapdiv-9" class="chartdiv"></div>
				<p id="mapdiv-reference-9" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-10" role="tabpanel" aria-labelledby="tc-10">
				<div id="mapdiv-10" class="chartdiv"></div>
				<p id="mapdiv-reference-10" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-11" role="tabpanel" aria-labelledby="tc-11">
				<div id="mapdiv-11" class="chartdiv"></div>
				<p id="mapdiv-reference-11" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-12" role="tabpanel" aria-labelledby="tc-12">
				<div id="mapdiv-12" class="chartdiv"></div>
				<p id="mapdiv-reference-12" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-13" role="tabpanel" aria-labelledby="tc-13">
				<div id="mapdiv-13" class="chartdiv"></div>
				<p id="mapdiv-reference-13" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-14" role="tabpanel" aria-labelledby="tc-14">
				<div id="mapdiv-14" class="chartdiv"></div>
				<p id="mapdiv-reference-14" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-15" role="tabpanel" aria-labelledby="tc-15">
				<div id="mapdiv-15" class="chartdiv"></div>
				<p id="mapdiv-reference-15" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-16" role="tabpanel" aria-labelledby="tc-16">
				<div id="chartdiv-16" class="chartdiv"></div>
				<p id="mapdiv-reference-16" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-17" role="tabpanel" aria-labelledby="tc-17">
				<div id="mapdiv-17" class="chartdiv"></div>
				<p id="mapdiv-reference-17" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-18" role="tabpanel" aria-labelledby="tc-18">
				<div id="mapdiv-18" class="chartdiv"></div>
				<p id="mapdiv-reference-18" class="editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-19" role="tabpanel" aria-labelledby="tc-19">
				<div id="mapdiv-19" class="chartdiv"></div>
				<p id="mapdiv-reference-19" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-20" role="tabpanel" aria-labelledby="tc-20">
				<div id="mapdiv-20" class="chartdiv"></div>
				<p id="mapdiv-reference-20" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-21" role="tabpanel" aria-labelledby="tc-21">
				<div id="mapdiv-21" class="chartdiv"></div>
				<p id="mapdiv-reference-21" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-22" role="tabpanel" aria-labelledby="tc-22">
				<div id="mapdiv-22" class="chartdiv"></div>
				<p id="mapdiv-reference-22" class="reference-text editable-element"></p>
			</div>
			<div class="oadh-chart" id="tab-content-23" role="tabpanel" aria-labelledby="tc-23">
				<div id="mapdiv-23" class="chartdiv"></div>
				<p id="mapdiv-reference-23" class="reference-text editable-element"></p>
			</div>
		</div>
	</div>
</div>
{!! Form::open(array('id' => 'oadh-xlsx-download-form', 'url' => URL::to('/cms/derechos/download-data-xlsx'), 'role'  =>  'form', 'class' => 'form-horizontal')) !!}
  {!! Form::hidden('oadh-xlsx-data', null, ['id' => 'oadh-xlsx-data', 'data-details' => '[]']) !!}
  {!! Form::hidden('oadh-xlsx-data-titles', null, ['id' => 'oadh-xlsx-data-titles', 'data-details-titles' => '[]']) !!}
{!! Form::close() !!}
@parent
@stop
