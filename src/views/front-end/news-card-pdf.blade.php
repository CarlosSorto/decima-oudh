<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>{{ Config::get('system-lang.' . $lang . '.medioprensa') }}</title>
  <style type="text/css">
    @page {
      margin: 0cm 0cm;
    }

    * {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 14px;
    }

    .main-section {
      background-repeat: no-repeat;
      background-position: center;
      opacity: 0.1;
      margin-right: 25px;
      margin-left: 25px;
      background-image: url({{ public_path('/oadh/logos/logo-primario-v2.png') }}) ;
    }

    .header-section {
      background-repeat: no-repeat;
      background-position: center;
      margin-top: 10px;
      margin-right: 25px;
      margin-left: 25px;
      height: 100px;
      background-image: url({{ public_path('/oadh/pdf/logos_header.png') }}) ;
    }

    table,
    th {
      text-align: center;
    }

    table {
      border-collapse: collapse;
      background-color: transparent;
      border: 1px solid #000;
    }

    .table {
      width: 100%;
      max-width: 100%;
      line-height: 1.25;
      margin-bottom: 1rem;
    }

    table thead tr th {
      background-color: rgb(0, 22, 70);
      color: white;
      vertical-align: middle;
    }

    table tbody tr td {
      font-size: 12px;
    }

    th,
    td {
      border: 1px solid #000;
      padding: 0 5px;
    }

    .badge {
      padding: .25em .4em;
      font-size: 75%;
      font-weight: 700;
      line-height: 1.25;
    }

    .badge-pill {
      padding-right: .6em;
      padding-left: .6em;
      border-radius: 10rem;
    }

    .badge-primary {
      background-color: rgb(0, 22, 70);
      color: white;
    }

    footer {
      position: fixed;
      bottom: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2.6cm;

      /** Extra personal styles **/
    }

    .image {
      position: relative;
      width: 816px;
      object-fit: fill;
    }
  </style>
</head>

<body>
  <header class="header-section">

  </header>
  <section class="main-section">
    <table class="table">
      <thead>
        <tr>
          <th colspan="6" style="font-weight: bold">{{ $keyValue }}</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th style="width: 10%; vertical-align: text-top;">{{ Config::get('system-lang.' . $lang . '.fecha') }}</th>
          <td style="width: 10%; vertical-align: text-top;" colspan="2">
            {{\Carbon\Carbon::createFromFormat('Y-m-d', $date)->format(Lang::get('form.phpShortDateFormat'))}}
          </td>
          <th style="width: 10%; vertical-align: text-top;">{!! Config::get('system-lang.' . $lang . '.mediocomunicacion') !!}</th>
          <td align="left" colspan="2">{{$digital_media}}</td>
        </tr>
        <tr>
          <th>{{ Config::get('system-lang.' . $lang . '.titular') }}</th>
          <td colspan="5" align="justify">{{$title}}</td>
        </tr>
        <tr>
          <th>{{ Config::get('system-lang.' . $lang . '.resumen') }}</th>
          <td colspan="5" align="justify">{{$summary}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: text-top;">{{ Config::get('system-lang.' . $lang . '.lugarhechos') }}</th>
          <td colspan="2" align="left">{{$context}}</td>
          <th style="width: 10%; vertical-align: text-top;">{{ Config::get('system-lang.' . $lang . '.fuentes') }}</th>
          <td style="width: 10%; vertical-align: middle;" colspan="2" align="left">{{$source}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: middle;">{{ Config::get('system-lang.' . $lang . '.derechoshumanos') }}</th>
          <td  colspan="5" align="left">
            <ul style="padding: 0; margin-left: 10px;">
            @foreach ($rights as $right)
                <li>
                  {{$right}}
                </li>
            @endforeach
            </ul>
          </td>
        </tr>
        <tr>
          <th>{{ Config::get('system-lang.' . $lang . '.temas') }}</th>
          <td colspan="5" align="justify">{{$topics}}</td>
        </tr>
        <tr>
          <th>{{ Config::get('system-lang.' . $lang . '.hechos') }}</th>
          <td colspan="5" align="justify">{{$violated_fact}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: middle;">{{ Config::get('system-lang.' . $lang . '.hipotesis') }}</th>
          <td colspan="5" align="justify">{{$procedures}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: middle;">{{ Config::get('system-lang.' . $lang . '.poblacion') }}</th>
          <td style="width: 10%; vertical-align: middle;" colspan="2" align="left">{{$affected}}</td>
          <th style="width: 10%; vertical-align: middle;">{{ Config::get('system-lang.' . $lang . '.tipoarma') }}</th>
          <td style="width: 10%; vertical-align: middle;" colspan="2" align="left">{{$weapon}}</td>
        </tr>
        <tr>
          <th style="width: 10%; vertical-align: middle;">{!! Config::get('system-lang.' . $lang . '.victimas') !!}</th>
          <td colspan="2" align="left" style="vertical-align: text-top;">{{$victim}}</td>
          <th style="width: 10%; vertical-align: middle;">{!! Config::get('system-lang.' . $lang . '.victimarios') !!}</th>
          <td colspan="2" align="left" style="vertical-align: middle;">{{$victimizer}}</td>
        </tr>
        <tr>
          <th>{!! Config::get('system-lang.' . $lang . '.enlace') !!}</th>
          <td colspan="5" align="justify" style="vertical-align: middle">
            @if (!empty($new_link))
            <a href="{{$new_link}}" target="_blank">{{$new_link}}</a>
            @else
              {!! Config::get('system-lang.' . $lang . '.noaplica') !!}
            @endif
          </td>
        </tr>
      </tbody>
    </table>
    <table class="table" style="width: 80%; margin: 50px auto; text-align: left; font-size: 16px;">
      <tr>
        <td style="line-height: 2">
          {{ Config::get('system-lang.' . $lang . '.descargadoel') }} {{\Carbon\Carbon::now()->format(Lang::get('form.phpShortDateFormat'))}} <br>
          {{ Config::get('system-lang.' . $lang . '.fuente') }}: <a href="{{URL::to('/cms/medios-de-prensa/fichas', $id)}}"
            style="text-decoration: none;">{{URL::to('/cms/medios-de-prensa/fichas', $id)}}</a>
        </td>
      </tr>
    </table>
  </section>
  <footer>
    <img src="{{ public_path('oadh/pdf/pie.jpeg')}}" class="image" alt="pie">
  </footer>
</body>

</html>