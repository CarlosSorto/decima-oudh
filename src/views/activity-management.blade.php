@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-am-new-action', null, array('id' => 'oadh-am-new-action')) !!}
{!! Form::hidden('oadh-am-edit-action', null, array('id' => 'oadh-am-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-am-remove-action', null, array('id' => 'oadh-am-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-am-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-am-btn-delete-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-am-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

	function oadhAmDisabledDetailForm()
	{
		$('#oadh-am-ai-form-fieldset').attr('disabled', 'disabled');
	}

	function oadhAmOnSelectRowEvent()
	{
		var id = $('#oadh-am-grid').getSelectedRowId('oadh_am_id'), rowData = $('#oadh-am-grid').getRowData($('#oadh-am-grid').jqGrid('getGridParam', 'selrow'));

		if($('#oadh-am-grid-section').attr('data-id') != id)
		{
			$('#oadh-am-grid-section').attr('data-id', id);

			// getAppJournals('oadh-am-', 'firstPage', id);
			// getElementFiles('oadh-am-', id);

			//$('#oadh-am-front-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'activity_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');
			$('#oadh-am-front-detail-grid').jqGrid('clearGridData');

			cleanJournals('oadh-am-');
			cleanFiles('oadh-am-');
		}

		$('#oadh-am-btn-group-2').enableButtonGroup();

		// if(rowData.oadh_am_status != 'P')
		// {
		// 	$('#oadh-am-btn-authorize, #oadh-am-btn-edit').attr('disabled', 'disabled');
		// }
		//
		// if(rowData.oadh_am_status == 'A')
		// {
		// 	$('#oadh-am-btn-void').attr('disabled', 'disabled');
		// }
	}

	function oadhAmDetailOnSelectRowEvent()
	{
		var selRowIds = $('#oadh-am-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

		if(selRowIds.length == 0)
		{
			$('#oadh-am-ai-btn-group-2').disabledButtonGroup();
		}
		else if(selRowIds.length == 1)
		{
			$('#oadh-am-ai-btn-group-2').enableButtonGroup();
		}
		else if(selRowIds.length > 1)
		{
			$('#oadh-am-ai-btn-group-2').disabledButtonGroup();
			$('#oadh-am-ai-btn-delete').removeAttr('disabled');
		}
	}

	function oadhAmOnLoadCompleteEvent()
	{
		// $('#oadh-am-front-detail-grid').jqGrid('clearGridData');
		//
		// $('#oadh-am-front-detail-grid').jqGrid('footerData','set', {
		// 	'oadh_am_detail_': 0,
		// });
	}

	function oadhAmDetailOnLoadCompleteEvent()
	{
		// var amount = $(this).jqGrid('getCol', 'oadh_am_detail_', false, 'sum');
		//
		// $(this).jqGrid('footerData','set', {
		// 	'oadh_am_detail_name': '{{ Lang::get('form.total') }}',
		// 	'oadh_am_detail_': amount,
		// });
		//
		// $('#oadh-am-ai-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
	}

	$(document).ready(function()
	{
		loadSmtRows('oadhAmSmtRows', $('#oadh-am-form').attr('action') + '/smt-rows');

		oadhAmDisabledDetailForm();

		$('.oadh-am-btn-tooltip').tooltip();

		$('#oadh-am-form, #oadh-am-ai-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-am-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-am-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-am-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-am-grid-section').collapse('show');

			$('#oadh-am-filters').show();
		});

		$('#oadh-am-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-am-').focus();
		});

		$('#oadh-am-').focusout(function()
		{
			$('#oadh-am-btn-save').focus();
		});

		$('#oadh-am-btn-new').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

      $('#oadh-am-form, #oadh-am-ai-form').jqMgVal('clearForm');

			$('#oadh-am-ai-btn-toolbar, #oadh-am-ot-btn-toolbar, #oadh-am-btn-toolbar').disabledButtonGroup();

			// $('#oadh-am-status-label').val($('#oadh-am-status-label').attr('data-default-value'));
			// $('#oadh-am-status').val($('#oadh-am-status').attr('data-default-value'));

			//$('#oadh-am-btn-new, #oadh-am-btn-edit').removeAttr('disabled');
			$('#oadh-am-btn-new').removeAttr('disabled');

			$('#oadh-am-btn-group-3').enableButtonGroup();

			oadhAmDisabledDetailForm();

			if(!$('#oadh-am-form-section').is(":visible"))
			{
				$('#oadh-am-journals-section').attr('data-target-id', '#oadh-am-form-section');

				$('#oadh-am-grid-section').collapse('hide');
			}
			else
			{
				$('#oadh-am-ai-activity-id').val(-1);

				$('#oadh-am-back-detail-grid').jqGrid('clearGridData');
			}

			$('#oadh-am-filters').hide();

			$(this).removeAttr('disabled');

			$('#oadh-am-form-new-title').removeClass('hidden');

			$('#oadh-am-form-edit-title').addClass('hidden');
		});

		$('#oadh-am-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-am-btn-toolbar').disabledButtonGroup();

			$('#oadh-am-btn-group-1').enableButtonGroup();

			if($('#oadh-am-journals-section').attr('data-target-id') == '' || $('#oadh-am-journals-section').attr('data-target-id') == '#oadh-am-form-section')
			{
				$('#oadh-am-grid').trigger('reloadGrid');

				$('#oadh-am-grid-section').attr('data-id', '');

				cleanJournals('oadh-am-');
				cleanJournals('oadh-am-');
			}
			else
			{

			}
		});

		$('#oadh-am-ai-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-am-back-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'activity_id','op':'eq','data':'" + $('#oadh-am-ai-activity-id').val() + "'}]}"}}).trigger('reloadGrid');
		});

		$('#oadh-am-ai-btn-export-xls').click(function()
		{
			$('#oadh-am-back-detail-gridXlsButton').click();
		});

		$('#oadh-am-ai-btn-export-csv').click(function()
		{
			$('#oadh-am-back-detail-gridCsvButton').click();
		});

		$('#oadh-am-btn-export-xls').click(function()
		{
			if($('#oadh-am-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-am-gridXlsButton').click();
			}
		});

		$('#oadh-am-btn-export-csv').click(function()
		{
			if($('#oadh-am-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-am-gridCsvButton').click();
			}
		});

		$('#oadh-am-image-url-uploader').click(function(){
			$('#oadh-am-file-uploader-modal').attr('data-field', '#oadh-am-image-url');
		});

		$('#oadh-am-ai-image-url-uploader').click(function(){
			$('#oadh-am-file-uploader-modal').attr('data-field', '#oadh-am-ai-image-url');
		});

		$('#oadh-am-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-am-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-am-grid').isRowSelected())
				{
					$('#oadh-am-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);

					return;
				}

				$('#oadh-am-btn-toolbar').disabledButtonGroup();

				$('#oadh-am-btn-group-3').enableButtonGroup();

				//$('#oadh-am-btn-new, #oadh-am-btn-edit, #oadh-am-btn-upload, #oadh-am-btn-show-files, #oadh-am-btn-show-history').removeAttr('disabled');

				$('#oadh-am-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-am-grid').getRowData($('#oadh-am-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				if (rowData.oadh_am_tags.length > 0)
				{
					var ids = rowData.oadh_am_tags.split(',');
					$.each(ids, function (index, token)
					{
						$('#oadh-am-tags').tokenfield('createToken', token);
					});
				}

				$('#oadh-am-filters').hide();

				$('#oadh-am-journals-section').attr('data-target-id', '#oadh-am-form-section');

				$('#oadh-am-grid-section').collapse('hide');

				$('#oadh-am-ai-form-fieldset').removeAttr('disabled');

				$('#oadh-am-id').val(rowData.oadh_am_id);
				$('#oadh-am-ai-activity-id').val(rowData.oadh_am_id);

				$('#oadh-am-lang-label').setAutocompleteLabel(rowData.oadh_am_lang);

				// $('.oadh-am-number').html(rowData.oadh_am_number);

				$('#oadh-am-ai-btn-refresh').click();

				$('#oadh-am-ai-btn-toolbar').disabledButtonGroup();

				$('#oadh-am-ai-btn-group-1').enableButtonGroup();
				$('#oadh-am-ai-btn-group-3').enableButtonGroup();
			}
			else if($('#oadh-am-journals-section').attr('data-target-id') == '#oadh-am-form-section')
			{
				$('#oadh-am-smt').createTable('oadh-am-grid', 'oadhAmSmtRows', 10);
				$('#oadh-am-smt').modal('show');
			}
			else
			{

			}
		});


		$('#oadh-am-ai-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if(!$('#oadh-am-back-detail-grid').isRowSelected())
			{
				$('#oadh-am-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
				return;
			}

			$('#oadh-am-ai-btn-toolbar').disabledButtonGroup();

			$('#oadh-am-ai-btn-group-3').enableButtonGroup();

			rowData = $('#oadh-am-back-detail-grid').getRowData($('#oadh-am-back-detail-grid').jqGrid('getGridParam', 'selrow'));

			populateFormFields(rowData);

			// $('#oadh-am-ai-label').setAutocompleteLabel(rowData.oadh_am_detail_id);
			// $('#oadh-am-lang-label').setAutocompleteLabel(rowData.oadh_am_lang);

			$('#oadh-am-ai-name').focus();
		});

		$('#oadh-am-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-am-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-am-grid').isRowSelected())
				{
					$('#oadh-am-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-am-grid').getRowData($('#oadh-am-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-am-delete-message').html($('#oadh-am-delete-message').attr('data-default-label').replace(':name', rowData.name));
			}
			else
			{

			}

			$('#oadh-am-modal-delete').modal('show');
		});

		$('#oadh-am-btn-view').click(function()
		{
			var id = $('#oadh-am-grid').getSelectedRowId('oadh_am_id');

			$('#oadh-am-front-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'activity_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');
		});

		$('#oadh-am-btn-show-files').click(function()
		{
			var rowData, id;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-am-grid').is(":visible"))
			{
				if(!$('#oadh-am-grid').isRowSelected())
				{
					$('#oadh-am-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
					return;
				}

				rowData = $('#oadh-am-grid').getRowData($('#oadh-am-grid').jqGrid('getGridParam', 'selrow'));
				id = rowData.oadh_am_id;
			}
			else
			{
				id = $('#oadh-am-id').val();
			}

			if($('#oadh-am-btn-file-modal-delete').attr('data-system-reference-id') != id)
			{
				getElementFiles('oadh-am-', id);
			}

			$.scrollTo({top: $('#oadh-am-file-viewer').offset().top - 100, left:0});
		});

		$('#oadh-am-btn-show-history').click(function()
		{
			var rowData, id;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-am-grid').is(":visible"))
			{
				if(!$('#oadh-am-grid').isRowSelected())
				{
					$('#oadh-am-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
					return;
				}

				rowData = $('#oadh-am-grid').getRowData($('#oadh-am-grid').jqGrid('getGridParam', 'selrow'));
				id = rowData.oadh_am_id;
			}
			else
			{
				id = $('#oadh-am-id').val();
			}

			if($('#oadh-am-journals').attr('data-journalized-id') != id)
			{
				getAppJournals('oadh-am-', 'firstPage', id);
			}

			$.scrollTo({top: $('#oadh-am-journals').offset().top - 100, left:0});
		});

		$('#oadh-am-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-am-journals-section').attr('data-target-id') == '')
			{
			  url = $('#oadh-am-form').attr('action') + '/delete-master';
			  id = $('#oadh-am-grid').getSelectedRowId('oadh_am_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id
					}
				),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-am-btn-toolbar', false);
					$('#oadh-am-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-am-btn-refresh').click();
						$("#oadh-am-btn-group-2").disabledButtonGroup();
						$('#oadh-am-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					if(json.info)
					{
						$('#oadh-am-btn-refresh').click();
						$("#oadh-am-btn-group-2").disabledButtonGroup();
						$('#oadh-am-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
					}

					if(!empty(json.smtRows))
					{
						loadSmtRows('oadhAmSmtRows', '', json.smtRows);
					}

					$('#oadh-am-modal-delete').modal('hide');

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-am-ai-btn-delete').click(function()
		{
			var id = $('#oadh-am-back-detail-grid').getSelectedRowsIdCell('oadh_am_ai_id');

			if(!$('#oadh-am-back-detail-grid').isRowSelected())
			{
				$('#oadh-am-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
				return;
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id,
						'activity_id': $('#oadh-am-ai-activity-id').val()
					}
				),
				dataType : 'json',
				url:  $('#oadh-am-ai-form').attr('action') + '/delete-details',
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-am-ai-btn-toolbar', false);
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-am-ai-btn-refresh').click();
						$("#oadh-am-ai-btn-group-2").disabledButtonGroup();
						$('#oadh-am-ai-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-am-btn-save').click(function()
		{
			var url = $('#oadh-am-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-am-journals-section').attr('data-target-id') == '#oadh-am-form-section')
			{
				if(!$('#oadh-am-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-am-id').isEmpty())
				{
					url = url + '/create-master';
				}
				else
				{
					url = url + '/update-master';
					action = 'edit';
				}

				data = $('#oadh-am-form').formToObject('oadh-am-');
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-am-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-am-journals-section').attr('data-target-id') == '#oadh-am-form-section')
						{
							if(action == 'new')
							{
								$('#oadh-am-id').val(json.id);
								$('#oadh-am-ai-activity-id').val(json.id);
								// $('.oadh-am-number').html('#' + json.number);
							}

							$('#oadh-am-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);

							$('#oadh-am-form-new-title').addClass('hidden');

							$('#oadh-am-form-edit-title').removeClass('hidden');

							$('#oadh-am-form').jqMgVal('clearContextualClasses');

							$('#oadh-am-ai-btn-toolbar').disabledButtonGroup();

							$('#oadh-am-ai-btn-group-1').enableButtonGroup();
							$('#oadh-am-ai-btn-group-3').enableButtonGroup();

							$('#oadh-am-btn-new, #oadh-am-btn-upload, #oadh-am-btn-show-files, #oadh-am-btn-show-history').removeAttr('disabled');
							$('#oadh-am-ai-form-fieldset').removeAttr('disabled');
						}
						else
						{
							// $('#oadh-am-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-am-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-am-journals-section').attr('data-target-id') == '#oadh-am-form-section')
						{
							$('#oadh-am-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-am-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					if(!empty(json.smtRows))
					{
						loadSmtRows('oadhAmSmtRows', '', json.smtRows);
					}

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
					$('#oadh-am-ai-name').focus();
				}
			});
		});

		$('#oadh-am-ai-btn-save').click(function()
		{
			var url = $('#oadh-am-ai-form').attr('action'), action = 'new';

			if(!$('#oadh-am-ai-form').jqMgVal('isFormValid'))
			{
				return;
			}

			if($('#oadh-am-ai-id').isEmpty())
			{
				url = url + '/create-detail';
			}
			else
			{
				url = url + '/update-detail';
				action = 'edit';
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify($('#oadh-am-ai-form').formToObject('oadh-am-ai-')),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-am-ai-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-am-ai-form').jqMgVal('clearForm');
						$('#oadh-am-ai-btn-toolbar').disabledButtonGroup();
						$('#oadh-am-ai-btn-group-1').enableButtonGroup();
						$('#oadh-am-ai-btn-group-3').enableButtonGroup();

						$('#oadh-am-ai-btn-refresh').click();
					}
					else if(json.info)
					{
							$('#oadh-am-ai-form').showAlertAsFirstChild('alert-info', json.info);
					}

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
					$('#oadh-am-ai-article-label').focus();
				}
			});
		});

		$('#oadh-am-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-am-journals-section').attr('data-target-id') == '#oadh-am-form-section')
			{
				$('#oadh-am-form-new-title').addClass('hidden');
				$('#oadh-am-form-edit-title').addClass('hidden');
				$('#oadh-am-btn-refresh').click();
				$('#oadh-am-ai-activity-id').val(-1);

				$('#oadh-am-back-detail-grid').jqGrid('clearGridData');

				$('#oadh-am-form, #oadh-am-ai-form').jqMgVal('clearForm');
				$('#oadh-am-form-section').collapse('hide');
			}
			else
			{

			}

			$('#oadh-am-form').clearTags();

			$('#oadh-am-btn-group-1').enableButtonGroup();

			$('#oadh-am-btn-group-3').disabledButtonGroup();

			$('#oadh-am-journals-section').attr('data-target-id', '');

			oadhAmDisabledDetailForm();
		});

		$('#oadh-am-file-uploader-modal').on('hidden.bs.modal', function (e)
		{
			dataFiles = $.parseJSON($("#oadh-am-file-uploader-modal").attr('data-files'));

			if(dataFiles.length == 2)
			{
				$($(this).attr('data-field')).val(dataFiles[1]['url']);
			}
			else if(dataFiles.length == 1)
			{
				$($(this).attr('data-field')).val(dataFiles[0]['url']);
			}
		});

		$('#oadh-am-smt-btn-select').click(function()
		{
			var rowData = $('#oadh-am-smt').getSelectedSmtRow();

			if(!empty(rowData))
			{
				$('#oadh-am-btn-new').click();

				populateFormFields(rowData);

				$('#oadh-am-smt').modal('hide');

				$('#oadh-am-ai-form-fieldset').removeAttr('disabled');

				$('#oadh-am-id').val(rowData.oadh_am_id);
				$('#oadh-am-ai-activity-id').val(rowData.oadh_am_id);

				// $('.oadh-am-number').html(rowData.oadh_am_number);

				$('#oadh-am-ai-btn-refresh').click();

				$('#oadh-am-ai-btn-toolbar').disabledButtonGroup();

				$('#oadh-am-ai-btn-group-1').enableButtonGroup();
				$('#oadh-am-ai-btn-group-3').enableButtonGroup();
			}
		});

		$('#oadh-am-smt-btn-refresh').click(function()
		{
			loadSmtRows('oadhAmSmtRows', $('#oadh-am-form').attr('action') + '/smt-rows', '', true, true);
		});

		if(!$('#oadh-am-new-action').isEmpty())
		{
			$('#oadh-am-btn-new').click();
		}

		$('#oadh-am-btn-edit-helper').click(function()
		{
			showButtonHelper('oadh-am-btn-close', 'oadh-am-btn-group-2', $('#oadh-am-edit-action').attr('data-content'));
		});

		if(!$('#oadh-am-edit-action').isEmpty())
		{
			showButtonHelper('oadh-am-btn-close', 'oadh-am-btn-group-2', $('#oadh-am-edit-action').attr('data-content'));
		}

		$('#oadh-am-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-am-btn-close', 'oadh-am-btn-group-2', $('#oadh-am-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-am-delete-action').isEmpty())
		{
			showButtonHelper('oadh-am-btn-close', 'oadh-am-btn-group-2', $('#oadh-am-delete-action').attr('data-content'));
		}

		setTimeout(function(){
			$('#oadh-am-tags').tokenfield(
			{
				showAutocompleteOnFocus: true,
				beautify:false
			});
		});
	});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-am-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-am-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new'), array('id' => 'oadh-am-btn-new', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-oadh::activity-management.newMaster'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-am-btn-refresh', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
         		<li><a id='oadh-am-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
         		<li><a id='oadh-am-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
       		</ul>
				</div>
			</div>
			<div id="oadh-am-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-am-btn-edit', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::activity-management.editMaster'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-am-btn-delete', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::activity-management.deleteMaster'))) !!}
				{!! Form::button('<i class="fa fa-eye"></i> ', array('id' => 'oadh-am-btn-view', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.viewLongText'))) !!}
				{{-- Form::button('<i class="fa fa-upload"></i> ', array('id' => 'oadh-am-btn-upload', 'class' => 'btn btn-default oadh-am-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-folder' => Lang::get('decima-oadh::activity-management.folder'), 'data-original-title' => Lang::get('toolbar.uploadLongText'))) --}}
				{{-- Form::button('<i class="fa fa-files-o"></i> ', array('id' => 'oadh-am-btn-show-files', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showFilesLongText'))) --}}
				{!! Form::button('<i class="fa fa-history"></i> ', array('id' => 'oadh-am-btn-show-history', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showHistoryLongText'))) !!}
			</div>
			<div id="oadh-am-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-am-btn-save', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::activity-management.saveMaster'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-am-btn-close', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-am-grid-section' class='collapse in' data-id=''>
			<div class='app-grid' data-app-grid-id='oadh-am-grid'>
				{!!
				GridRender::setGridId('oadh-am-grid')
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('height', 'auto')
					->setGridOption('multiselect', false)
					->setGridOption('rowNum', 5)
					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
		    	->setGridOption('url', URL::to('ucaoadh/cms/activity-management/grid-data-master'))
		    	->setGridOption('caption', Lang::get('decima-oadh::activity-management.gridMasterTitle'))
					->setGridOption('filename', Lang::get('decima-oadh::activity-management.gridMasterTitle'))
		    	->setGridOption('postData', array('_token' => Session::token()))
					->setGridEvent('loadComplete', 'oadhAmOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhAmOnSelectRowEvent')
					->addColumn(array('index' => 'a.id', 'name' => 'oadh_am_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('form.date'), 'index' => 'a.date' ,'name' => 'oadh_am_date', 'formatter' => 'date', 'align' => 'center'))
		    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.title'), 'index' => 'a.title' ,'name' => 'oadh_am_title'))
					->addColumn(array('label' => Lang::get('decima-oadh::publication-management.place'), 'index' => 'a.place' ,'name' => 'oadh_am_place'))
					->addColumn(array('label' => Lang::get('decima-oadh::publication-management.description'), 'index' => 'a.descripcion' ,'name' => 'oadh_am_description'))
					->addColumn(array('label' => Lang::get('decima-oadh::publication-management.lang'), 'index' => 'p.lang' ,'name' => 'oadh_am_lang', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::publication-management.langGridText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70))
		    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.tags'), 'index' => 'a.tags' ,'name' => 'oadh_am_tags'))
		    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.imageUrl'), 'index' => 'a.image_url' ,'name' => 'oadh_am_image_url'))
		    	->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-am-front-detail-grid'>
				{!!
				GridRender::setGridId('oadh-am-front-detail-grid')
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url',URL::to('ucaoadh/cms/activity-management/grid-data-detail'))
					->setGridOption('datatype', 'local')
					->setGridOption('filename', Lang::get('decima-oadh::activity-management.gridDetailTitle'))
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('footerrow', true)
					->setGridOption('multiselect', false)
					//->setGridOption('postData',array('_token' => Session::token()))
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'activity_id','op':'eq','data':'-1'}]}"))
					//->setGridEvent('loadComplete', 'oadhAmDetailOnLoadCompleteEvent')
					->addColumn(array('index' => 'ai.id', 'name' => 'oadh_am_ai_id', 'hidden' => true))
		    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.imageUrl1140'), 'index' => 'ai.image_url' ,'name' => 'oadh_am_ai_image_url'))
					->addColumn(array('label' => Lang::get('decima-oadh::publication-management.description'), 'index' => 'ai.desciption' ,'name' => 'oadh_am_ai_description'))
					->renderGrid();
				!!}
			</div>
		</div>
	</div>
</div>
<div id='oadh-am-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container form-container-followed-by-grid-section">
			{!! Form::open(array('id' => 'oadh-am-form', 'url' => URL::to('ucaoadh/cms/activity-management'), 'role' => 'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-am-form-new-title" class="hidden">{{ Lang::get('decima-oadh::activity-management.formNewTitle') }}</legend>
				<legend id="oadh-am-form-edit-title" class="hidden">{{ Lang::get('decima-oadh::activity-management.formEditTitle') }}<label class="pull-right oadh-am-number"></label></legend>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-am-date', Lang::get('form.date'), array('class' => 'control-label')) !!}
							{!! Form::date('oadh-am-date', array('class' => 'form-control', 'data-mg-required' => '')) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-am-title', Lang::get('decima-oadh::publication-management.title'), array('class' => 'control-label')) !!}
							{!! Form::text('oadh-am-title', null , array('id' => 'oadh-am-title', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							{!! Form::hidden('oadh-am-id', null, array('id' => 'oadh-am-id')) !!}
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-am-lang', Lang::get('decima-oadh::publication-management.lang'), array('class' => 'control-label ')) !!}
							{!! Form::autocomplete('oadh-am-lang-label', array(array('label'=>Lang::get('decima-oadh::publication-management.es'), 'value'=>'es'), array('label'=>Lang::get('decima-oadh::publication-management.en'), 'value'=>'en')), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-am-lang-label', 'oadh-am-lang', null, null) !!}
							{!! Form::hidden('oadh-am-lang', null, array('id'=> 'oadh-am-lang')) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-am-description', Lang::get('decima-oadh::publication-management.description'), array('class' => 'control-label')) !!}
							{!! Form::textareacustom('oadh-am-description', 2, 300 , array('id' => 'oadh-am-description', 'class' => 'form-control', 'data-mg-required' => '')) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-am-place', Lang::get('decima-oadh::publication-management.place'), array('class' => 'control-label')) !!}
							{!! Form::text('oadh-am-place', null , array('id' => 'oadh-am-place', 'class' => 'form-control', 'data-mg-required' => '')) !!}
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-am-tags', Lang::get('decima-oadh::publication-management.tags'), array('class' => 'control-label')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-tags"></i>
								</span>
								{!! Form::text('oadh-am-tags', null , array('id' => 'oadh-am-tags', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-am-image-url', Lang::get('decima-oadh::publication-management.imageUrl1140'), array('class' => 'control-label')) !!}
							{!! Form::imageuploader('oadh-am-image-url', array('id' => 'oadh-am-image-url', 'class' => 'form-control','data-mg-required' => ''), $prefix, "Actividades", 480, '""', '[480]') !!}
						</div>
					</div>
				</div>
				<div class="row">
				</div>

			{!! Form::close() !!}
			{!! Form::open(array('id' => 'oadh-am-ai-form', 'url' => URL::to('ucaoadh/cms/activity-management'), 'role' => 'form', 'onsubmit' => 'return false;')) !!}
			<fieldset id="oadh-am-ai-form-fieldset" disabled="disabled">
				<legend id="oadh-am-ai-model-form-new-title" class="">{{ Lang::get('decima-oadh::activity-management.formDetailTitle') }}</legend>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-am-ai-image-url', Lang::get('decima-oadh::publication-management.imageUrl1140'), array('class' => 'control-label')) !!}
									{!! Form::imageuploader('oadh-am-ai-image-url', array('id' => 'oadh-am-ai-image-url', 'class' => 'form-control','data-mg-required' => ''), $prefix, "Actividades", 480, '""', '[480]') !!}
									{!! Form::hidden('oadh-am-ai-activity-id', null, array('id' => 'oadh-am-ai-activity-id', 'data-mg-clear-ignored' => '')) !!}
									{!! Form::hidden('oadh-am-ai-id', null, array('id' => 'oadh-am-ai-id')) !!}
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-am-ai-description', Lang::get('decima-oadh::publication-management.description'), array('class' => 'control-label')) !!}
									{!! Form::text('oadh-am-ai-description', null , array('id' => 'oadh-am-ai-description', 'class' => 'form-control', 'data-mg-required' => '')) !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		{!! Form::close() !!}
		</div>
		<div id="oadh-am-ai-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
			<div id="oadh-am-ai-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-am-ai-btn-save', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => 'false', 'data-original-title' => Lang::get('decima-oadh::activity-management.saveDetail'))) !!}
			</div>
			<div id="oadh-am-ai-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-am-ai-btn-refresh', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
						<li><a id='oadh-am-ai-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
						<li><a id='oadh-am-ai-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
					</ul>
				</div>
			</div>
			<div id="oadh-am-ai-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-am-ai-btn-edit', 'class' => 'btn btn-default oadh-am-ai-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::activity-management.editDetail'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-am-ai-btn-delete', 'class' => 'btn btn-default oadh-am-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::activity-management.deleteDetail'))) !!}
			</div>
		</div>
		<div id='oadh-am-back-detail-grid-section' class='app-grid collapse in' data-app-grid-id='oadh-am-back-detail-grid'>
			{!!
			GridRender::setGridId('oadh-am-back-detail-grid')
				->hideXlsExporter()
				->hideCsvExporter()
				->setGridOption('url',URL::to('ucaoadh/cms/activity-management/grid-data-detail'))
				->setGridOption('datatype', 'local')
				->setGridOption('filename', Lang::get('decima-oadh::activity-management.gridDetailTitle'))
				->setGridOption('rowList', array())
				->setGridOption('rowNum', 100000)
				->setGridOption('footerrow',false)
				->setGridOption('multiselect', true)
				//->setGridOption('postData',array('_token' => Session::token()))
				->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'activity_id','op':'eq','data':'-1'}]}"))
				->setGridEvent('onSelectRow', 'oadhAmDetailOnSelectRowEvent')
				//->setGridEvent('loadComplete', 'oadhAmDetailOnLoadCompleteEvent')
				->addColumn(array('index' => 'ai.id', 'name' => 'oadh_am_ai_id', 'hidden' => true))
				->addColumn(array('label' => Lang::get('decima-oadh::publication-management.imageUrl1140'), 'index' => 'ai.image_url' ,'name' => 'oadh_am_ai_image_url'))
				->addColumn(array('label' => Lang::get('decima-oadh::publication-management.description'), 'index' => 'ai.desciption' ,'name' => 'oadh_am_ai_description'))
				->renderGrid();
			!!}
		</div>
	</div>
</div>
<div id='oadh-am-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals($prefix, $appInfo['id']) !!}
</div>
@include('decima-file::file-uploader')
<div id='oadh-am-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-am-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-am-delete-message" data-default-label="{{ Lang::get('form.deleteMessageConfirmation') }}">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
				 <!-- <p id="oadh-am-delete-message" data-default-label="{{ Lang::get('decima-oadh::activity-management.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-am-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
