@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-st-new-action', null, array('id' => 'oadh-st-new-action')) !!}
{!! Form::hidden('oadh-st-edit-action', null, array('id' => 'oadh-st-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-st-remove-action', null, array('id' => 'oadh-st-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-st-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-st-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-st-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-st-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

	function oadhStDisabledDetailForm()
	{
		$('#oadh-st-detail-form-fieldset').attr('disabled', 'disabled');
	}

	function oadhStOnSelectRowEvent()
	{
		$(this).jqGrid('footerData', 'set',
		{
			'oadh_st_count': $(this).jqGrid('getCol', 'oadh_st_count', false, 'sum'),
		});


	}

	function oadhStDetailOnSelectRowEvent()
	{
		var selRowIds = $('#oadh-st-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

		if(selRowIds.length == 0)
		{
			$('#oadh-st-detail-btn-group-2').disabledButtonGroup();
		}
		else if(selRowIds.length == 1)
		{
			$('#oadh-st-detail-btn-group-2').enableButtonGroup();
		}
		else if(selRowIds.length > 1)
		{
			$('#oadh-st-detail-btn-group-2').disabledButtonGroup();
			$('#oadh-st-detail-btn-delete').removeAttr('disabled');
		}
	}

	function oadhStOnLoadCompleteEvent()
	{
		// $('#oadh-st-front-detail-grid').jqGrid('clearGridData');
		//
		// $('#oadh-st-front-detail-grid').jqGrid('footerData','set', {
		// 	'oadh_st_detail_': 0,
		// });

		$(this).jqGrid('footerData', 'set',
		{
			'oadh_st_dl_downloads': $(this).jqGrid('getCol', 'oadh_st_dl_downloads', false, 'sum'),
			'oadh_st_acs_access': $(this).jqGrid('getCol', 'oadh_st_acs_access', false, 'sum'),
			'oadh_st_count': $(this).jqGrid('getCol', 'oadh_st_count', false, 'sum')
		});
	}

	function oadhStDetailOnLoadCompleteEvent()
	{
		// var amount = $(this).jqGrid('getCol', 'oadh_st_detail_', false, 'sum');
		//
		// $(this).jqGrid('footerData','set', {
		// 	'oadh_st_detail_name': '{{ Lang::get('form.total') }}',
		// 	'oadh_st_detail_': amount,
		// });
		//
		// $('#oadh-st-detail-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
	}

	$(document).ready(function()
	{
		// loadSmtRows('oadhStSmtRows', $('#oadh-st-form').attr('action') + '/smt-rows');

		oadhStDisabledDetailForm();

		$('.oadh-st-btn-tooltip').tooltip();

		$('#oadh-st-form, #oadh-st-detail-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-st-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-st-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-st-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-st-grid-section').collapse('show');

			$('#oadh-st-filters').show();
		});

		$('#oadh-st-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-st-').focus();
		});

		$('#oadh-st-').focusout(function()
		{
			$('#oadh-st-btn-save').focus();
		});

		$('#oadh-st-btn-new').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

      $('#oadh-st-form, #oadh-st-detail-form').jqMgVal('clearForm');

			$('#oadh-st-detail-btn-toolbar, #oadh-st-ot-btn-toolbar, #oadh-st-btn-toolbar').disabledButtonGroup();

			// $('#oadh-st-status-label').val($('#oadh-st-status-label').attr('data-default-value'));
			// $('#oadh-st-status').val($('#oadh-st-status').attr('data-default-value'));

			$('#oadh-st-btn-new, #oadh-st-btn-edit').removeAttr('disabled');

			$('#oadh-st-btn-group-3').enableButtonGroup();

			oadhStDisabledDetailForm();

			if(!$('#oadh-st-form-section').is(":visible"))
			{
				$('#oadh-st-journals-section').attr('data-target-id', '#oadh-st-form-section');

				$('#oadh-st-grid-section').collapse('hide');
			}
			else
			{
				$('#oadh-st-detail-master-id').val(-1);

				$('#oadh-st-back-detail-grid').jqGrid('clearGridData');
			}

			$('#oadh-st-filters').hide();

			$(this).removeAttr('disabled');

			$('#oadh-st-form-new-title').removeClass('hidden');

			$('#oadh-st-form-edit-title').addClass('hidden');
		});

		$('#oadh-st-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-st-btn-toolbar').disabledButtonGroup();

			$('#oadh-st-btn-group-1').enableButtonGroup();

			if($('#oadh-st-journals-section').attr('data-target-id') == '' || $('#oadh-st-journals-section').attr('data-target-id') == '#oadh-st-form-section')
			{
				$('#oadh-st-grid').trigger('reloadGrid');

				$('#oadh-st-grid-section').attr('data-id', '');

				cleanJournals('oadh-st-');
				cleanJournals('oadh-st-');
			}
			else
			{

			}
		});

		$('#oadh-st-detail-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-st-back-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'master_id','op':'eq','data':'" + $('#oadh-st-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
		});

		$('#oadh-st-detail-btn-export-xls').click(function()
		{
			$('#oadh-st-back-detail-gridXlsButton').click();
		});

		$('#oadh-st-detail-btn-export-csv').click(function()
		{
			$('#oadh-st-back-detail-gridCsvButton').click();
		});

		$('#oadh-st-btn-export-xls').click(function()
		{
				$('#oadh-st-gridXlsButton').click();
		});

		$('#oadh-st-btn-detail-export-xls').click(function()
		{
				$('#oadh-st-front-detail-gridXlsButton').click();
		});

		$('#oadh-st-btn-detail2-export-xls').click(function()
		{
				$('#oadh-st-front-detail-grid2XlsButton').click();
		});

		$('#oadh-st-btn-detail3-export-xls').click(function()
		{
				$('#oadh-st-front-detail-grid3XlsButton').click();
		});

		$('#oadh-st-btn-detail4-export-xls').click(function()
		{
				$('#oadh-st-front-detail-grid4XlsButton').click();
		});

		$('#oadh-st-btn-export-csv').click(function()
		{
			if($('#oadh-st-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-st-gridCsvButton').click();
			}
		});

		$('#oadh-st-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-st-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-st-grid').isRowSelected())
				{
					$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);

					return;
				}

				$('#oadh-st-btn-toolbar').disabledButtonGroup();

				$('#oadh-st-btn-group-3').enableButtonGroup();

				$('#oadh-st-btn-new, #oadh-st-btn-edit, #oadh-st-btn-upload, #oadh-st-btn-show-files, #oadh-st-btn-show-history').removeAttr('disabled');

				$('#oadh-st-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-st-grid').getRowData($('#oadh-st-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				$('#oadh-st-filters').hide();

				$('#oadh-st-journals-section').attr('data-target-id', '#oadh-st-form-section');

				$('#oadh-st-grid-section').collapse('hide');

				$('#oadh-st-detail-form-fieldset').removeAttr('disabled');

				$('#oadh-st-id').val(rowData.oadh_st_id);
				$('#oadh-st-detail-master-id').val(rowData.oadh_st_id);

				// $('.oadh-st-number').html(rowData.oadh_st_number);

				$('#oadh-st-detail-btn-refresh').click();

				$('#oadh-st-detail-btn-toolbar').disabledButtonGroup();

				$('#oadh-st-detail-btn-group-1').enableButtonGroup();
				$('#oadh-st-detail-btn-group-3').enableButtonGroup();
			}
			else if($('#oadh-st-journals-section').attr('data-target-id') == '#oadh-st-form-section')
			{
				$('#oadh-st-smt').createTable('oadh-st-grid', 'oadhStSmtRows', 10);
				$('#oadh-st-smt').modal('show');
			}
			else
			{

			}
		});


		$('#oadh-st-detail-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if(!$('#oadh-st-back-detail-grid').isRowSelected())
			{
				$('#oadh-st-detail-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
				return;
			}

			$('#oadh-st-detail-btn-toolbar').disabledButtonGroup();

			$('#oadh-st-detail-btn-group-3').enableButtonGroup();

			rowData = $('#oadh-st-back-detail-grid').getRowData($('#oadh-st-back-detail-grid').jqGrid('getGridParam', 'selrow'));

			populateFormFields(rowData);

			// $('#oadh-st-detail-label').setAutocompleteLabel(rowData.oadh_st_detail_id);

			$('#oadh-st-detail-name').focus();
		});

		$('#oadh-st-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-st-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-st-grid').isRowSelected())
				{
					$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-st-grid').getRowData($('#oadh-st-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-st-delete-message').html($('#oadh-st-delete-message').attr('data-default-label').replace(':name', rowData.name));
			}
			else
			{

			}

			$('#oadh-st-modal-delete').modal('show');
		});

		$('#oadh-st-btn-view').click(function()
		{
			var id = $('#oadh-st-grid').getSelectedRowId('oadh_st_id');

			$('#oadh-st-front-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'master_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');
		});

		$('#oadh-st-btn-show-files').click(function()
		{
			var rowData, id;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-st-grid').is(":visible"))
			{
				if(!$('#oadh-st-grid').isRowSelected())
				{
					$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
					return;
				}

				rowData = $('#oadh-st-grid').getRowData($('#oadh-st-grid').jqGrid('getGridParam', 'selrow'));
				id = rowData.oadh_st_id;
			}
			else
			{
				id = $('#oadh-st-id').val();
			}

			if($('#oadh-st-btn-file-modal-delete').attr('data-system-reference-id') != id)
			{
				getElementFiles('oadh-st-', id);
			}

			$.scrollTo({top: $('#oadh-st-file-viewer').offset().top - 100, left:0});
		});

		$('#oadh-st-btn-show-history').click(function()
		{
			var rowData, id;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-st-grid').is(":visible"))
			{
				if(!$('#oadh-st-grid').isRowSelected())
				{
					$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
					return;
				}

				rowData = $('#oadh-st-grid').getRowData($('#oadh-st-grid').jqGrid('getGridParam', 'selrow'));
				id = rowData.oadh_st_id;
			}
			else
			{
				id = $('#oadh-st-id').val();
			}

			if($('#oadh-st-journals').attr('data-journalized-id') != id)
			{
				getAppJournals('oadh-st-', 'firstPage', id);
			}

			$.scrollTo({top: $('#oadh-st-journals').offset().top - 100, left:0});
		});

		$('#oadh-st-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-st-journals-section').attr('data-target-id') == '')
			{
			  url = $('#oadh-st-form').attr('action') + '/delete-master';
			  id = $('#oadh-st-grid').getSelectedRowId('oadh_st_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id
					}
				),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-st-btn-toolbar', false);
					$('#oadh-st-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-st-btn-refresh').click();
						$("#oadh-st-btn-group-2").disabledButtonGroup();
						$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					if(json.info)
					{
						$('#oadh-st-btn-refresh').click();
						$("#oadh-st-btn-group-2").disabledButtonGroup();
						$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
					}

					if(!empty(json.smtRowId))
					{
						deleteSmtRow('oadhStSmtRows', json.smtRowId);
					}

					$('#oadh-st-modal-delete').modal('hide');

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-st-btn-authorize').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-st-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-st-grid').isRowSelected())
				{
					$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);

					return;
				}

				rowData = $('#oadh-st-grid').getRowData($('#oadh-st-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-st-ma-authorize-message').html($('#oadh-st-ma-authorize-message').attr('data-default-label').replace(':name', rowData.name));
			}
			else
			{

			}

			$('#oadh-st-ma').modal('show');
		});

		$('#oadh-st-ma-btn-authorize').click(function()
		{
			var id, url;

			if($('#oadh-st-journals-section').attr('data-target-id') == '')
			{
			  url = $('#oadh-st-form').attr('action') + '/authorize-master';
			  id = $('#oadh-st-grid').getSelectedRowId('oadh_st_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id
					}
				),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-st-btn-toolbar', false);
					$('#oadh-st-ma').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-st-btn-refresh').click();
						$("#oadh-st-btn-group-2").disabledButtonGroup();
						$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					if(json.info)
					{
						$('#oadh-st-btn-refresh').click();
						$("#oadh-st-btn-group-2").disabledButtonGroup();
						$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
					}

					if(!empty(json.smtRow))
					{
						updateSmtRow('oadhStSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					$('#oadh-st-ma').modal('hide');

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-st-btn-void').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-st-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-st-grid').isRowSelected())
				{
					$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-st-grid').getRowData($('#oadh-st-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-st-mv-void-message').html($('#oadh-st-mv-void-message').attr('data-default-label').replace(':name', rowData.name));
			}
			else
			{

			}

			$('#oadh-st-mv').modal('show');
		});

		$('#oadh-st-mv-btn-void').click(function()
		{
			var id, url;

			if($('#oadh-st-journals-section').attr('data-target-id') == '')
			{
				url = $('#oadh-st-form').attr('action') + '/void-master';
				id = $('#oadh-st-grid').getSelectedRowId('oadh_st_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id
					}
				),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-st-btn-toolbar', false);
					$('#oadh-st-mv').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-st-btn-refresh').click();
						$("#oadh-st-btn-group-2").disabledButtonGroup();
						$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					if(json.info)
					{
						$('#oadh-st-btn-refresh').click();
						$("#oadh-st-btn-group-2").disabledButtonGroup();
						$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
					}

					if(!empty(json.smtRow))
					{
						updateSmtRow('oadhStSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					$('#oadh-st-mv').modal('hide');

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-st-detail-btn-delete').click(function()
		{
			var id = $('#oadh-st-back-detail-grid').getSelectedRowsIdCell('oadh_st_detail_id');

			if(!$('#oadh-st-back-detail-grid').isRowSelected())
			{
				$('#oadh-st-detail-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
				return;
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id,
						'detail_id': $('#oadh-st-detail-id').val()
					}
				),
				dataType : 'json',
				url:  $('#oadh-st-detail-form').attr('action') + '/delete-details',
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-st-detail-btn-toolbar', false);
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-st-detail-btn-refresh').click();
						$("#oadh-st-detail-btn-group-2").disabledButtonGroup();
						$('#oadh-st-detail-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-st-btn-save').click(function()
		{
			var url = $('#oadh-st-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-st-journals-section').attr('data-target-id') == '#oadh-st-form-section')
			{
				if(!$('#oadh-st-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-st-id').isEmpty())
				{
					url = url + '/create-master';
				}
				else
				{
					url = url + '/update-master';
					action = 'edit';
				}

				data = $('#oadh-st-form').formToObject('oadh-st-');
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-st-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-st-journals-section').attr('data-target-id') == '#oadh-st-form-section')
						{
							if(action == 'new')
							{
								$('#oadh-st-id').val(json.id);
								$('#oadh-st-detail-master-id').val(json.id);
								// $('.oadh-st-number').html('#' + json.number);
							}

							if(action == 'edit')
							{
								$('#oadh-st-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							}

							$('#oadh-st-form-new-title').addClass('hidden');

							$('#oadh-st-form-edit-title').removeClass('hidden');

							$('#oadh-st-form').jqMgVal('clearContextualClasses');

							$('#oadh-st-detail-btn-toolbar').disabledButtonGroup();

							$('#oadh-st-detail-btn-group-1').enableButtonGroup();
							$('#oadh-st-detail-btn-group-3').enableButtonGroup();

							$('#oadh-st-btn-new, #oadh-st-btn-upload, #oadh-st-btn-show-files, #oadh-st-btn-show-history').removeAttr('disabled');
							$('#oadh-st-detail-form-fieldset').removeAttr('disabled');
						}
						else
						{
							// $('#oadh-st-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-st-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-st-journals-section').attr('data-target-id') == '#oadh-st-form-section')
						{
							$('#oadh-st-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-st-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					if(action == 'new' && !empty(json.smtRow))
					{
						addSmtRow('oadhStSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					if(action == 'edit' && !empty(json.smtRow))
					{
						updateSmtRow('oadhStSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					$('#app-loader').addClass('hidden');

					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');

					$('#oadh-st-detail-name').focus();
				}
			});
		});

		$('#oadh-st-detail-btn-save').click(function()
		{
			var url = $('#oadh-st-detail-form').attr('action'), action = 'new';

			if(!$('#oadh-st-detail-form').jqMgVal('isFormValid'))
			{
				return;
			}

			if($('#oadh-st-detail-id').isEmpty())
			{
				url = url + '/create-detail';
			}
			else
			{
				url = url + '/update-detail';
				action = 'edit';
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify($('#oadh-st-detail-form').formToObject('oadh-st-detail-')),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-st-detail-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-st-detail-form').jqMgVal('clearForm');
						$('#oadh-st-detail-btn-toolbar').disabledButtonGroup();
						$('#oadh-st-detail-btn-group-1').enableButtonGroup();
						$('#oadh-st-detail-btn-group-3').enableButtonGroup();

						$('#oadh-st-detail-btn-refresh').click();
					}
					else if(json.info)
					{
							$('#oadh-st-detail-form').showAlertAsFirstChild('alert-info', json.info);
					}

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
					$('#oadh-st-detail-article-label').focus();
				}
			});
		});

		$('#oadh-st-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-st-journals-section').attr('data-target-id') == '#oadh-st-form-section')
			{
				$('#oadh-st-form-new-title').addClass('hidden');
				$('#oadh-st-form-edit-title').addClass('hidden');
				$('#oadh-st-btn-refresh').click();
				$('#oadh-st-detail-master-id').val(-1);

				$('#oadh-st-back-detail-grid').jqGrid('clearGridData');

				$('#oadh-st-form, #oadh-st-detail-form').jqMgVal('clearForm');
				$('#oadh-st-form-section').collapse('hide');
			}
			else
			{

			}

			$('#oadh-st-finished-goods-warehouse-ids').clearTags();

			$('#oadh-st-btn-group-1').enableButtonGroup();

			$('#oadh-st-btn-group-3').disabledButtonGroup();

			$('#oadh-st-journals-section').attr('data-target-id', '');

			oadhStDisabledDetailForm();
		});

		$('#oadh-st-btn-clear-filter').click(function()
		{
			$('#oadh-st-filters-form').find('.tokenfield').find('.close').click();

			$('#oadh-st-filters-form').jqMgVal('clearForm');

			$('#oadh-st-btn-filter').click();
		});

		$('#oadh-st-btn-filter').click(function()
		{
			var filters = [];

			var filtersDl = [];

			var filterAccs = [];

			$(this).removeClass('btn-default').addClass('btn-warning');

			if(!$('#oadh-st-filters-form').jqMgVal('isFormValid'))
			{
				return;
			}

			$('#oadh-st-filters-form').jqMgVal('clearContextualClasses');

			if($("#oadh-st-date-from-date-from-filter").val() != '__/__/____' && !$("#oadh-st-date-from-date-from-filter").isEmpty())
			{
				filters.push({'field':'st.datetime', 'op':'ge', 'data': $.datepicker.formatDate("yy-mm-dd", $("#oadh-st-date-from-date-from-filter").datepicker("getDate")) + ' 00:00:00'});
				filtersDl.push({'field':'dl.datetime', 'op':'ge', 'data': $.datepicker.formatDate("yy-mm-dd", $("#oadh-st-date-from-date-from-filter").datepicker("getDate")) + ' 00:00:00'});
				filterAccs.push({'field':'acs.datetime', 'op':'ge', 'data': $.datepicker.formatDate("yy-mm-dd", $("#oadh-st-date-from-date-from-filter").datepicker("getDate")) + ' 00:00:00'});
			}
			if($("#oadh-st-date-from-date-to-filter").val() != '__/__/____' && !$("#oadh-st-date-from-date-to-filter").isEmpty())
			{
				filters.push({'field':'st.datetime', 'op':'le', 'data': $.datepicker.formatDate("yy-mm-dd", $("#oadh-st-date-from-date-to-filter").datepicker("getDate")) + ' 23:59:59'});
				filtersDl.push({'field':'dl.datetime', 'op':'le', 'data': $.datepicker.formatDate("yy-mm-dd", $("#oadh-st-date-from-date-to-filter").datepicker("getDate")) + ' 23:59:59'});
				filterAccs.push({'field':'acs.datetime', 'op':'le', 'data': $.datepicker.formatDate("yy-mm-dd", $("#oadh-st-date-from-date-to-filter").datepicker("getDate")) + ' 23:59:59'});
			}


			if(!$('#oadh-st-name-filter').isEmpty())
			{
				filters.push({'field':'b.name', 'op':'in', 'data': $('#oadh-st-name-filter').val()});
			}

			if(!$('#oadh-st-name-filter').isEmpty())
			{
				filters.push({'field':'m.name', 'op':'in', 'data': $('#oadh-st-name-filter').val()});
			}

			if(filters.length == 0)
			{
				$('#oadh-st-btn-filter').removeClass('btn-warning').addClass('btn-default');
			}

			$('#oadh-st-grid').jqGrid('setGridParam', {'postData':{'datatype':'json', "filters":"{'groupOp':'AND','rules':" + JSON.stringify(filters) + "}"}}).trigger('reloadGrid');
			$('#oadh-st-front-detail-grid').jqGrid('setGridParam', {'postData':{'datatype':'json', "filters":"{'groupOp':'AND','rules':" + JSON.stringify(filtersDl) + "}"}}).trigger('reloadGrid');
			$('#oadh-st-front-detail-grid2').jqGrid('setGridParam', {'postData':{'datatype':'json', "filters":"{'groupOp':'AND','rules':" + JSON.stringify(filterAccs) + "}"}}).trigger('reloadGrid');
		});

		$('#oadh-st-smt-btn-select').click(function()
		{
			var rowData = $('#oadh-st-smt').getSelectedSmtRow();

			if(empty(rowData))
			{
				return;
			}

			$('#oadh-st-btn-new').click();

			populateFormFields(rowData);

			$('#oadh-st-smt').modal('hide');

			$('#oadh-st-detail-form-fieldset').removeAttr('disabled');

			$('#oadh-st-id').val(rowData.oadh_st_id);
			$('#oadh-st-detail-master-id').val(rowData.oadh_st_id);

			// $('.oadh-st-number').html(rowData.oadh_st_number);

			$('#oadh-st-detail-btn-refresh').click();

			$('#oadh-st-detail-btn-toolbar').disabledButtonGroup();

			$('#oadh-st-detail-btn-group-1').enableButtonGroup();
			$('#oadh-st-detail-btn-group-3').enableButtonGroup();
		});

		$('#oadh-st-smt-btn-refresh').click(function()
		{
			loadSmtRows('oadhStSmtRows', $('#oadh-st-form').attr('action') + '/smt-rows', '', true, true);
		});

		if(!$('#oadh-st-new-action').isEmpty())
		{
			$('#oadh-st-btn-new').click();
		}

		$('#oadh-st-btn-edit-helper').click(function()
		{
			showButtonHelper('oadh-st-btn-close', 'oadh-st-btn-group-2', $('#oadh-st-edit-action').attr('data-content'));
		});

		if(!$('#oadh-st-edit-action').isEmpty())
		{
			showButtonHelper('oadh-st-btn-close', 'oadh-st-btn-group-2', $('#oadh-st-edit-action').attr('data-content'));
		}

		$('#oadh-st-btn-delete-helper').click(function()
		{
			showButtonHelper('oadh-st-btn-close', 'oadh-st-btn-group-2', $('#oadh-st-delete-action').attr('data-content'));
		});

		if(!$('#oadh-st-delete-action').isEmpty())
		{
			showButtonHelper('oadh-st-btn-close', 'oadh-st-btn-group-2', $('#oadh-st-delete-action').attr('data-content'));
		}
	});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		{!! Form::open(array('id' => 'oadh-st-filters-form', 'url' => URL::to('/'), 'role' => 'form', 'onsubmit' => 'return false;', 'class' => 'form-horizontal')) !!}
			<div id="oadh-st-filters" class="panel panel-default">
				<div class="panel-heading custom-panel-heading clearfix">
					<h3 class="panel-title custom-panel-title pull-left">
						{{ Lang::get('form.filtersTitle') }}
					</h3>
					{!! Form::button('<i class="fa fa-filter"></i> ' . Lang::get('form.filterButton'), array('id' => 'oadh-st-btn-filter', 'class' => 'btn btn-default btn-sm pull-right btn-filter-left-margin')) !!}
					{!! Form::button('<i class="fa fa-eraser"></i> ' . Lang::get('form.clearFilterButton'), array('id' => 'oadh-st-btn-clear-filter', 'class' => 'btn btn-default btn-sm pull-right')) !!}
				</div>
				<div id="oadh-st-filters-body" class="panel-body">
					<div class="row">
						<div class="col-lg-8 col-md-12">
							<div class="form-group">
								{!! Form::label('oadh-st-date-from-filter', Lang::get('form.date'), array('class' => 'col-sm-2 control-label')) !!}
								<div class="col-sm-10 mg-hm">
									{!! Form::daterange('oadh-st-date-from-date-from-filter', 'oadh-st-date-from-date-to-filter' , array('class' => 'form-control')) !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
		
		<div id="oadh-st-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-st-btn-group-1" class="btn-group btn-group-app-toolbar">
				{{-- {!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-st-btn-refresh', 'class' => 'btn btn-default oadh-st-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!} --}}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
						<li><a id='oadh-st-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> Hoja de cálculo</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id='oadh-st-grid-section' class='collapse in' data-id=''>
			
			<div class='app-grid' data-app-grid-id='oadh-st-grid'>
				{!!
				GridRender::setGridId('oadh-st-grid')
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('height', 'auto')
					->setGridOption('multiselect', false)
					->setGridOption('rowNum', 31)
					->setGridOption('rowList', array(31, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
					->setGridOption('url', URL::to('ucaoadh/cms/statistics/grid-data-master'))
					->setGridOption('caption', Lang::get('decima-oadh::statistics-management.gridMasterTitle'))
					->setGridOption('filename', Lang::get('decima-oadh::statistics-management.gridMasterTitle'))
					->setGridOption('postData', array('_token' => Session::token()))
					->setGridOption('footerrow', true)
					->setGridEvent('loadComplete', 'oadhStOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhStOnSelectRowEvent')
					->addColumn(array('index' => 'id', 'name' => 'oadh_st_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('form.date'), 'index' => 'DATE(st.datetime)' ,'name' => 'oadh_st_datetime', 'modalwidth' => '10%', 'formatter' => 'date', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.subscriberCount'), 'name' => 'oadh_st_count', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>
	
			<div id="oadh-st-detail-btn-toolbar" class="section-header btn-toolbar section-block"  role="toolbar">
				<div id="oadh-st-btn-group-1" class="btn-group btn-group-app-toolbar">
					{{-- {!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-st-btn-refresh', 'class' => 'btn btn-default oadh-st-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!} --}}
					<div class="btn-group">
						{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
						<ul class="dropdown-menu">
							<li><a id='oadh-st-btn-detail-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> Hoja de cálculo</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class='app-grid' data-app-grid-id='oadh-st-front-detail-grid'>
				{!!
				GridRender::setGridId('oadh-st-front-detail-grid')
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url',URL::to('ucaoadh/cms/statistics/grid-data-detail'))
					->setGridOption('caption', Lang::get('decima-oadh::statistics-management.gridDetailTitle'))
					->setGridOption('filename', Lang::get('decima-oadh::statistics-management.gridDetailTitle'))
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('footerrow', true)
					->setGridEvent('loadComplete', 'oadhStOnLoadCompleteEvent')
					->setGridOption('multiselect', false)
					->setGridOption('postData',array('_token' => Session::token()))
					->addColumn(array('index' => 'id', 'name' => 'oadh_st_detail_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.name'), 'index' => 'pb.title', 'name' => 'oadh_st_dl_title', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('form.type'), 'index' => 'pb.type', 'name' => 'oadh_st_dl_type', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::publication-management.typeGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select'))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.downloads'), 'index' => 'oadh_st_dl_downloads', 'name' => 'oadh_st_dl_downloads', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>

			<div class="section-header btn-toolbar section-block"  role="toolbar">
				<div class="btn-group btn-group-app-toolbar">
					{{-- {!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-st-btn-refresh', 'class' => 'btn btn-default oadh-st-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!} --}}
					<div class="btn-group">
						{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
						<ul class="dropdown-menu">
							<li><a id='oadh-st-btn-detail3-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> Hoja de cálculo</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class='app-grid' data-app-grid-id='oadh-st-front-detail-grid'>
				{!!
				GridRender::setGridId('oadh-st-front-detail-grid3')
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url',URL::to('ucaoadh/cms/statistics/grid-data-detail3'))
					->setGridOption('caption', Lang::get('decima-oadh::statistics-management.gridDetailTitle3'))
					->setGridOption('filename', Lang::get('decima-oadh::statistics-management.gridDetailTitle3'))
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('footerrow', true)
					->setGridEvent('loadComplete', 'oadhStOnLoadCompleteEvent')
					->setGridOption('multiselect', false)
					->setGridOption('postData',array('_token' => Session::token()))
					->addColumn(array('index' => 'id', 'name' => 'oadh_st_detail_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.digitalMedia'), 'index' => 'pb.type', 'name' => 'oadh_st_dl_type', 'align' => 'center', 'hidden' => false))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.name'), 'index' => 'pb.title', 'name' => 'oadh_st_dl_title', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.downloads'), 'index' => 'oadh_st_dl_downloads', 'name' => 'oadh_st_dl_downloads', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>

			<div class="section-header btn-toolbar section-block"  role="toolbar">
				<div class="btn-group btn-group-app-toolbar">
					{{-- {!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-st-btn-refresh', 'class' => 'btn btn-default oadh-st-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!} --}}
					<div class="btn-group">
						{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
						<ul class="dropdown-menu">
							<li><a id='oadh-st-btn-detail4-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> Hoja de cálculo</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class='app-grid' data-app-grid-id='oadh-st-front-detail-grid'>
				{!!
				GridRender::setGridId('oadh-st-front-detail-grid4')
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url',URL::to('ucaoadh/cms/statistics/grid-data-detail4'))
					->setGridOption('caption', Lang::get('decima-oadh::statistics-management.gridDetailTitle4'))
					->setGridOption('filename', Lang::get('decima-oadh::statistics-management.gridDetailTitle4'))
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('footerrow', true)
					->setGridEvent('loadComplete', 'oadhStOnLoadCompleteEvent')
					->setGridOption('multiselect', false)
					->setGridOption('postData',array('_token' => Session::token()))
					//->setGridEvent('loadComplete', 'oadhStDetailOnLoadCompleteEvent')
					->addColumn(array('index' => 'id', 'name' => 'oadh_st_detail_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.system'), 'index' => 'pb.type', 'name' => 'oadh_st_dl_type', 'align' => 'center', 'hidden' => false))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.recommendation'), 'index' => 'pb.title', 'name' => 'oadh_st_dl_title', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.downloads'), 'index' => 'oadh_st_dl_downloads', 'name' => 'oadh_st_dl_downloads', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>

			<div id="oadh-st-detail2-btn-toolbar" class="section-header btn-toolbar section-block"  role="toolbar">
				<div id="oadh-st-btn-group-1" class="btn-group btn-group-app-toolbar">
					{{-- {!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-st-btn-refresh', 'class' => 'btn btn-default oadh-st-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!} --}}
					<div class="btn-group">
						{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
						<ul class="dropdown-menu">
							<li><a id='oadh-st-btn-detail2-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> Hoja de cálculo</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class='app-grid' data-app-grid-id='oadh-st-front-detail-grid2'>
				{!!
				GridRender::setGridId('oadh-st-front-detail-grid2')
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url',URL::to('ucaoadh/cms/statistics/grid-data-detail2'))
					->setGridOption('caption', Lang::get('decima-oadh::statistics-management.gridDetailTitle2'))
					->setGridOption('filename', Lang::get('decima-oadh::statistics-management.gridDetailTitle2'))
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('footerrow', true)
					->setGridEvent('loadComplete', 'oadhStOnLoadCompleteEvent')
					->setGridOption('multiselect', false)
					->setGridOption('postData',array('_token' => Session::token()))
					//->setGridEvent('loadComplete', 'oadhStDetailOnLoadCompleteEvent')
					->addColumn(array('index' => 'id', 'name' => 'oadh_st_acs_detail_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.right'), 'index' => 'acs.right_to', 'name' => 'oadh_st_acs_right_to', 'align' => 'left', 'width' => '50'))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.name'), 'index' => 'acs.name', 'name' => 'oadh_st_acs_name', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('decima-oadh::statistics-management.access'), 'index' => 'oadh_st_acs_access', 'name' => 'oadh_st_acs_access', 'align' => 'center', 'width' => '30'))
					->renderGrid();
				!!}
			</div>
		</div>
	</div>
</div>

@include('layouts.search-modal-table')
@parent
@stop
