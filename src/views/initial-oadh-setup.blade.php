@extends('layouts.base')

@section('container')
<style>
	.pl-1{
		padding-left: 0.50rem;
	}
	.pr-1{
		padding-right: 0.50rem;
	}
	.ml-1{
		margin-left: 1rem;
	}
	.px-1{
		padding-right: 0.50rem;
		padding-left: 0.50rem;
	}
</style>
<script type='text/javascript'>

	var oadhIsPopulationAffected = '{!! $currentSettingConfiguration['population_affected'] !!}';
	var oadhIsNoteLocations = '{!! $currentSettingConfiguration['note_locations'] !!}';
	var oadhIsJournalisticGenre = '{!! $currentSettingConfiguration['journalistic_genre'] !!}';
	var oadhIsSources = '{!! $currentSettingConfiguration['sources'] !!}';
	var oadhIsSexualDiversities = '{!! $currentSettingConfiguration['sexual_diversities'] !!}';
	var oadhIsGunsTypes = '{!! $currentSettingConfiguration['guns_types'] !!}';

	$(document).ready(function()
	{
		$('#oadh-is-settings-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-is-btn-update').click(function()
		{
			if(!$('#oadh-is-settings-form').jqMgVal('isFormValid'))
			{
				return;
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify($('#oadh-is-settings-form').formToObject('oadh-is-')),
				dataType : 'json',
				url: $('#oadh-is-settings-form').attr('action'),
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-is-settings-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					$('#oadh-is-settings-form').showAlertAsFirstChild('alert-success',json.success, 5000);
					$('#oadh-is-settings-form').find('.has-success').each(function()
					{
						$(this).removeClass('has-success');
					});

					$('#oadh-is-settings-form').find('.mg-hmt').remove();

					$('#oadh-is-form-setting-message').show();
					$('#app-loader').addClass('hidden');
					enableAll();
					cleanJournals('oadh-is-');
					getAppJournals('oadh-is-','firstPage', $('#oadh-is-setting-id').val());
					$('#oadh-is-settings-form').jqMgVal('clearContextualClasses');
				}
			});
		});

		setTimeout(function(){
			$('#oadh-is-population-affected, #oadh-is-note-locations, #oadh-is-journalistic-genre, #oadh-is-sexual-diversities, #oadh-is-guns-types, #oadh-is-sources').tokenfield(
			{
				showAutocompleteOnFocus: true,
				beautify:false
			});

			if (oadhIsPopulationAffected.length > 0)
			{
				var ids = oadhIsPopulationAffected.split(',');
				$.each(ids, function (index, token)
				{
					// token = token.replace(/-/g, ',');
					$('#oadh-is-population-affected').tokenfield('createToken', token);
				});
			}

			if (oadhIsNoteLocations.length > 0)
			{
				var ids = oadhIsNoteLocations.split(',');
				$.each(ids, function (index, token)
				{
					// token = token.replace(/-/g, ',');
					$('#oadh-is-note-locations').tokenfield('createToken', token);
				});
			}

			if (oadhIsJournalisticGenre.length > 0)
			{
				var ids = oadhIsJournalisticGenre.split(',');
				$.each(ids, function (index, token)
				{
					// token = token.replace(/-/g, ',');
					$('#oadh-is-journalistic-genre').tokenfield('createToken', token);
				});
			}

			if (oadhIsSources.length > 0)
			{
				var ids = oadhIsSources.split(',');
				$.each(ids, function (index, token)
				{
					// token = token.replace(/-/g, ',');
					$('#oadh-is-sources').tokenfield('createToken', token);
				});
			}

			if (oadhIsSexualDiversities.length > 0)
			{
				var ids = oadhIsSexualDiversities.split(',');
				$.each(ids, function (index, token)
				{
					// token = token.replace(/-/g, ',');
					$('#oadh-is-sexual-diversities').tokenfield('createToken', token);
				});
			}

			if (oadhIsGunsTypes.length > 0)
			{
				var ids = oadhIsGunsTypes.split(',');
				$.each(ids, function (index, token)
				{
					// token = token.replace(/-/g, ',');
					$('#oadh-is-guns-types').tokenfield('createToken', token);
				});
			}
		}, 500);
	});
</script>
<div class="row">
	<fieldset id="oadh-is-form-fieldset">
		<div class="col-lg-6 col-md-6">
			<div id="oadh-is-form-container" class="form-container form-container-custom clearfix">
				{!! Form::open(array('id' => 'oadh-is-settings-form', 'url' => URL::to('ucaoadh/setup/initial-setup/update-settings'), 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<legend style="border-bottom:none;margin-bottom: 10px;">{{ Lang::get('decima-oadh::initial-oadh-setup.initialOadhSetup') }}</legend>
					<legend style="font-size: 19px">{{ Lang::get('decima-oadh::menu.mediaMonitoring') }}</legend>
					<div class="form-group mg-hm">
						{!! Form::label('oadh-is-population-affected', Lang::get('decima-oadh::media-monitoring.populationAffected'), array('class' => 'control-label')) !!}
						{!! Form::text('oadh-is-population-affected', '' , array('id' => 'oadh-is-population-affected', 'class' => 'form-control')) !!}
					</div>
					<div class="form-group mg-hm">
						{!! Form::label('oadh-is-note-locations', Lang::get('decima-oadh::media-monitoring.noteLocations'), array('class' => 'control-label')) !!}
						{!! Form::text('oadh-is-note-locations', '' , array('id' => 'oadh-is-note-locations', 'class' => 'form-control')) !!}
					</div>
					<div class="form-group mg-hm">
						{!! Form::label('oadh-is-journalistic-genre', Lang::get('decima-oadh::media-monitoring.journalisticGenre'), array('class' => 'control-label')) !!}
						{!! Form::text('oadh-is-journalistic-genre', '' , array('id' => 'oadh-is-journalistic-genre', 'class' => 'form-control')) !!}
					</div>
					<div class="form-group mg-hm">
						{!! Form::label('oadh-is-sources', Lang::get('decima-oadh::initial-oadh-setup.sources'), array('class' => 'control-label')) !!}
						{!! Form::text('oadh-is-sources', '' , array('id' => 'oadh-is-sources', 'class' => 'form-control')) !!}
					</div>
					<div class="form-group mg-hm">
						{!! Form::label('oadh-is-sexual-diversities', Lang::get('decima-oadh::initial-oadh-setup.sexualDiversities'), array('class' => 'control-label')) !!}
						{!! Form::text('oadh-is-sexual-diversities', '' , array('id' => 'oadh-is-sexual-diversities', 'class' => 'form-control')) !!}
					</div>
					<div class="form-group mg-hm">
						{!! Form::label('oadh-is-guns-types', Lang::get('decima-oadh::initial-oadh-setup.gunsTypes'), array('class' => 'control-label')) !!}
						{!! Form::text('oadh-is-guns-types', '' , array('id' => 'oadh-is-guns-types', 'class' => 'form-control')) !!}
					</div>

					<div>
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('decima-oadh::initial-oadh-setup.update'), array('id' => 'oadh-is-btn-update', 'class' => 'btn btn-default btn-lg pull-right')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
				<div class="col-lg-6 col-md-6">
					<div class="row">
						{!! Form::journals('oadh-is-', 'initial-oadh-setup', false, '', $currentSettingConfiguration['id'], false, '', $journals) !!}
					</div>
				</div>
	</fieldset>
</div>
@parent
@stop
