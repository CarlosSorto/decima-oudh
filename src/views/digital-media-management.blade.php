@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-dmm-new-action', null, array('id' => 'oadh-dmm-new-action')) !!}
{!! Form::hidden('oadh-dmm-edit-action', null, array('id' => 'oadh-dmm-edit-action', 'data-content' => Lang::get('decima-oadh::digital-media-management.editHelpText'))) !!}
{!! Form::hidden('oadh-dmm-remove-action', null, array('id' => 'oadh-dmm-remove-action', 'data-content' => Lang::get('decima-oadh::digital-media-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-dmm-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-dmm-btn-delete-helper', 'class' => 'hidden')) !!}
<style></style>

<script type='text/javascript'>
	//For grids with multiselect enabled
	// function oadhDmmOnSelectRowEvent(id)
	// {
	// 	var selRowIds = $('#oadh-dmm-grid').jqGrid('getGridParam', 'selarrrow'), id;
	//
	// 	if(selRowIds.length == 0)
	// 	{
	// 		$('#oadh-dmm-btn-group-2').disabledButtonGroup();
	// 		cleanJournals('oadh-dmm-');
	// 		// cleanFiles('oadh-dmm-')
	// 	}
	// 	else if(selRowIds.length == 1)
	// 	{
	// 		$('#oadh-dmm-btn-group-2').enableButtonGroup();
	//
	// 		id = $('#oadh-dmm-grid').getSelectedRowId('oadh_dmm_id');
	//
	// 		if($('#oadh-dmm-journals').attr('data-journalized-id') != id)
	// 		{
	// 			cleanJournals('oadh-dmm-');
	// 			// getElementFiles('oadh-dmm-', id);
	// 			getAppJournals('oadh-dmm-','firstPage', id);
	// 		}
	//
	// 	}
	// 	else if(selRowIds.length > 1)
	// 	{
	// 		$('#oadh-dmm-btn-group-2').disabledButtonGroup();
	// 		$('#oadh-dmm-btn-delete').removeAttr('disabled');
	// 		cleanJournals('oadh-dmm-');
	// 		// cleanFiles('oadh-dmm-')
	// 	}
	// }


	//For grids with multiselect disabled
	function oadhDmmOnSelectRowEvent()
	{
		var id = $('#oadh-dmm-grid').getSelectedRowId('oadh_dmm_id');

		if($('#oadh-dmm-journals').attr('data-journalized-id') != id)
		{
			getAppJournals('oadh-dmm-', 'firstPage', id);
			// getElementFiles('oadh-dmm-', id);
		}

		$('#oadh-dmm-btn-group-2').enableButtonGroup();
	}


	$(document).ready(function()
	{
		$('.oadh-dmm-btn-tooltip').tooltip();

		$('#oadh-dmm-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-dmm-journals-section').on('hidden.bs.collapse', function ()
		{
			$($(this).attr('data-target-id')).collapse('show');
		});

		$('#oadh-dmm-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-dmm-').focus();
		});

		$('#oadh-dmm-').focusout(function()
		{
			$('#oadh-dmm-btn-save').focus();
		});

		$('#oadh-dmm-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-dmm-grid-section').collapse('show');

			$('#oadh-dmm-journals-section').collapse('show');
		});

		$('#oadh-dmm-btn-new-digital-media').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-dmm-form-new-title-section').addClass('hidden');
			$('#oadh-dmm-form-new-title-digital-media').removeClass('hidden');

			$('#oadh-dmm-form-edit-title-digital-media, #oadh-dmm-form-edit-title-section').addClass('hidden');

			$('#oadh-dmm-abbreviation-div').removeClass('hidden');
			$('#oadh-dmm-parent-div').addClass('hidden');

			$('#oadh-dmm-name-div').addClass('col-lg-8');
			$('#oadh-dmm-name-div').removeClass('col-lg-12');

			$('#oadh-dmm-btn-toolbar').disabledButtonGroup();
			$('#oadh-dmm-btn-group-3').enableButtonGroup();
			$('#oadh-dmm-grid-section').collapse('hide');
			$('#oadh-dmm-journals-section').attr('data-target-id', '#oadh-dmm-form-section');
			$('#oadh-dmm-journals-section').collapse('hide');
			$('.decima-erp-tooltip').tooltip('hide');
		});

		$('#oadh-dmm-btn-new-section').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-dmm-form-new-title-digital-media').addClass('hidden');
			$('#oadh-dmm-form-new-title-section').removeClass('hidden');
			$('#oadh-dmm-form-edit-title-digital-media, #oadh-dmm-form-edit-title-section').addClass('hidden');

			$('#oadh-dmm-abbreviation-div').addClass('hidden');
			$('#oadh-dmm-parent-div').removeClass('hidden');

			$('#oadh-dmm-name-div').removeClass('col-lg-8');
			$('#oadh-dmm-name-div').addClass('col-lg-12');

			$('#oadh-dmm-btn-toolbar').disabledButtonGroup();
			$('#oadh-dmm-btn-group-3').enableButtonGroup();
			$('#oadh-dmm-grid-section').collapse('hide');
			$('#oadh-dmm-journals-section').attr('data-target-id', '#oadh-dmm-form-section');
			$('#oadh-dmm-journals-section').collapse('hide');
			$('.decima-erp-tooltip').tooltip('hide');
		});

		$('#oadh-dmm-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-dmm-btn-toolbar').disabledButtonGroup();
			$('#oadh-dmm-btn-group-1').enableButtonGroup();

			if($('#oadh-dmm-journals-section').attr('data-target-id') == '' || $('#oadh-dmm-journals-section').attr('data-target-id') == '#oadh-dmm-form-section')
			{
				$('#oadh-dmm-grid').trigger('reloadGrid');
				cleanJournals('oadh-dmm-');
				// cleanFiles('oadh-dmm-')
			}
			else
			{

			}
		});

		$('#oadh-dmm-btn-export-xls').click(function()
		{
			if($('#oadh-dmm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-dmm-gridXlsButton').click();
			}
			else
			{

			}
		});

		$('#oadh-dmm-btn-export-csv').click(function()
		{
			if($('#oadh-dmm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-dmm-gridCsvButton').click();
			}
			else
			{

			}
		});

		$('#oadh-dmm-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-dmm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-dmm-grid').isRowSelected())
				{
					$('#oadh-dmm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				$('#oadh-dmm-btn-toolbar').disabledButtonGroup();
				$('#oadh-dmm-btn-group-3').enableButtonGroup();

				$('#oadh-dmm-form-edit-title-digital-media, #oadh-dmm-form-edit-title-section').addClass('hidden');


				rowData = $('#oadh-dmm-grid').getRowData($('#oadh-dmm-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				if(rowData.oadh_dmm_type == 'M')
				{
					$('#oadh-dmm-form-edit-title-digital-media').removeClass('hidden');
					$('#oadh-dmm-abbreviation-div').removeClass('hidden');
					$('#oadh-dmm-parent-div').addClass('hidden');

					$('#oadh-dmm-name-div').addClass('col-lg-8');
					$('#oadh-dmm-name-div').removeClass('col-lg-12');
				}
				else
				{
					$('#oadh-dmm-form-edit-title-section').removeClass('hidden');
					$('#oadh-dmm-abbreviation-div').addClass('hidden');
					$('#oadh-dmm-parent-div').removeClass('hidden');

					$('#oadh-dmm-name-div').removeClass('col-lg-8');
					$('#oadh-dmm-name-div').addClass('col-lg-12');
				}
				$('#oadh-dmm-grid-section').collapse('hide');
				$('#oadh-dmm-journals-section').attr('data-target-id', '#oadh-dmm-form-section');
				$('#oadh-dmm-journals-section').collapse('hide');
			}
			else
			{

			}
		});

		$('#oadh-dmm-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-dmm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-dmm-grid').isRowSelected())
				{
					$('#oadh-dmm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-dmm-grid').getRowData($('#oadh-dmm-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-dmm-delete-message').html($('#oadh-dmm-delete-message').attr('data-default-label').replace(':name', rowData.oadh_dmm_name));
			}
			else
			{

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-dmm-modal-delete').modal('show');
		});

		$('#oadh-dmm-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-dmm-journals-section').attr('data-target-id') == '')
			{
				url = $('#oadh-dmm-form').attr('action') + '/delete';

				id = $('#oadh-dmm-grid').getSelectedRowId('oadh_dmm_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify({'_token':$('#app-token').val(), 'id':id}),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-dmm-btn-toolbar', false);
					$('#oadh-dmm-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-dmm-btn-refresh').click();
						$('#oadh-dmm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}
					else if(json.info)
					{
						$('#oadh-dmm-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 12000);
					}

					$('#oadh-dmm-modal-delete').modal('hide');
					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-dmm-btn-save').click(function()
		{
			var url = $('#oadh-dmm-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-dmm-journals-section').attr('data-target-id') == '#oadh-dmm-form-section')
			{
				if(!$('#oadh-dmm-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-dmm-id').isEmpty())
				{
					url = url + '/create';
				}
				else
				{
					url = url + '/update';
					action = 'edit';
				}

				data = $('#oadh-dmm-form').formToObject('oadh-dmm-');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-dmm-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-dmm-journals-section').attr('data-target-id') == '#oadh-dmm-form-section')
						{
							$('#oadh-dmm-btn-close').click();
							$('#oadh-dmm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);

							$('#oadh-digital-media-label').autocomplete('option', 'source', json.digitalMedias);
						}
						else
						{
							// $('#oadh-dmm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-dmm-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-dmm-journals-section').attr('data-target-id') == '#oadh-dmm-form-section')
						{
							$('#oadh-dmm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-dmm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-dmm-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-dmm-form-new-title-digital-media, #oadh-dmm-form-new-title-section').addClass('hidden');

			$('#oadh-dmm-form-edit-title-digital-media, #oadh-dmm-form-edit-title-section').addClass('hidden');

			// oadh-dmm-form-section
			if($('#oadh-dmm-journals-section').attr('data-target-id') == '#oadh-dmm-form-section')
			{
				$('#oadh-dmm-btn-refresh').click();
				$('#oadh-dmm-form').jqMgVal('clearForm');
				$('#oadh-dmm-form-section').collapse('hide');
			}
			else
			{

			}

			$('#oadh-dmm-btn-group-1').enableButtonGroup();
			$('#oadh-dmm-btn-group-3').disabledButtonGroup();
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-dmm-journals-section').attr('data-target-id', '')
		});

		$('#oadh-dmm-btn-edit-helper').click(function()
	  {
			showButtonHelper('oadh-dmm-btn-close', 'oadh-dmm-btn-group-2', $('#oadh-dmm-edit-action').attr('data-content'));
	  });

		$('#oadh-dmm-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-dmm-btn-close', 'oadh-dmm-btn-group-2', $('#oadh-dmm-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-dmm-edit-action').isEmpty())
		{
			showButtonHelper('oadh-dmm-btn-close', 'oadh-dmm-btn-group-2', $('#oadh-dmm-edit-action').attr('data-content'));
		}

		if(!$('#oadh-dmm-delete-action').isEmpty())
		{
			showButtonHelper('oadh-dmm-btn-close', 'oadh-dmm-btn-group-2', $('#oadh-dmm-delete-action').attr('data-content'));
		}
	});
</script>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-dmm-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-dmm-btn-group-1" class="btn-group btn-group-app-toolbar">
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new') . ' <span class="caret"></span>', array('id' => 'purch-pom-btn-new-all', 'class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
						<li><a id='oadh-dmm-btn-new-digital-media' class="btn fake-link"><i class="fa fa-plus"></i> {{Lang::get('decima-oadh::digital-media-management.newMedia')}}</a></li>
						<li><a id='oadh-dmm-btn-new-section' class="btn fake-link"><i class="fa fa-plus"></i> {{Lang::get('decima-oadh::digital-media-management.newSection')}}</a></li>
					</ul>
				</div>
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-dmm-btn-refresh', 'class' => 'btn btn-default oadh-dmm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
         		<li><a id='oadh-dmm-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> {{ Lang::get('decima-accounting::journal-management.standardSpreadsheet') . ' (' . Lang::get('form.spreadsheet') . ')' }}</a></li>
         		<!-- <li><a id='oadh-dmm-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li> -->
       		</ul>
				</div>
			</div>
			<div id="oadh-dmm-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-dmm-btn-edit', 'class' => 'btn btn-default oadh-dmm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::digital-media-management.edit'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-dmm-btn-delete', 'class' => 'btn btn-default oadh-dmm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::digital-media-management.delete'))) !!}
			</div>
			<div id="oadh-dmm-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-dmm-btn-save', 'class' => 'btn btn-default oadh-dmm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::digital-media-management.save'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-dmm-btn-close', 'class' => 'btn btn-default oadh-dmm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-dmm-grid-section' class='app-grid collapse in' data-app-grid-id='oadh-dmm-grid'>
			{!!
			GridRender::setGridId("oadh-dmm-grid")
				->enablefilterToolbar(false, false)
				->hideXlsExporter()
  			->hideCsvExporter()
	    	->setGridOption('url',URL::to('/ucaoadh/setup/digital-media-management/grid-data'))
	    	->setGridOption('caption', Lang::get('decima-oadh::digital-media-management.gridTitle'))
				->setGridOption('filename', Lang::get('decima-oadh::digital-media-management.gridTitle'))
	    	->setGridOption('postData',array('_token' => Session::token()))
				->setGridEvent('onSelectRow', 'oadhDmmOnSelectRowEvent')
				->setGridOption('multiselect', false)
				->setGridOption('treeGrid', true)
				->setGridOption('treeIcons', array('leaf' => 'fas fa-leaf'))
				->setGridOption('ExpandColumn', 'oadh_dmm_abbreviation')
				->setGridOption('treeReader', array('parent_id_field' => 'oadh_dmm_parent_id', 'leaf_field' => 'oadh_dmm_is_leaf'))
				->addColumn(array('index' => 'oadh_dmm_is_leaf', 'name' => 'oadh_dmm_is_leaf', 'hidden' => true))
				->addColumn(array('index' => 'dm.id', 'name' => 'oadh_dmm_id', 'hidden' => true, 'key' => true))
				->addColumn(array('index' => 'dm.parent_id', 'name' => 'oadh_dmm_parent_label', 'hidden' => true))
				->addColumn(array('index' => 'dm.type', 'name' => 'oadh_dmm_type', 'hidden' => true))
	    	->addColumn(array('label' => '', 'index' => 'dm.abbreviation' ,'name' => 'oadh_dmm_abbreviation', 'width' => '15'))
	    	->addColumn(array('label' => Lang::get('form.name'), 'index' => 'dm.name' ,'name' => 'oadh_dmm_name'))
	    	->renderGrid();
			!!}
		</div>
	</div>
</div>
<div id='oadh-dmm-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-dmm-form', 'url' => URL::to('/ucaoadh/setup/digital-media-management'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-dmm-form-new-title-digital-media" class="hidden">{{ Lang::get('decima-oadh::digital-media-management.formNewTitleDigitalMedia') }}</legend>
				<legend id="oadh-dmm-form-edit-title-digital-media" class="hidden">{{ Lang::get('decima-oadh::digital-media-management.formEditTitleDigitalMedia') }}</legend>

				<legend id="oadh-dmm-form-new-title-section" class="hidden">{{ Lang::get('decima-oadh::digital-media-management.formNewTitleSection') }}</legend>
				<legend id="oadh-dmm-form-edit-title-section" class="hidden">{{ Lang::get('decima-oadh::digital-media-management.formEditTitleSection') }}</legend>
				<div class="row">
					<div class="col-lg-6 col-md-6" id='oadh-dmm-parent-div'>
						<div class="form-group mg-hm">
							{!! Form::label('oadh-dmm-name', Lang::get('decima-oadh::digital-media-management.mainDigitalMedia'), array('class' => 'control-label')) !!}
							{!! Form::autocomplete('oadh-dmm-parent-label', $digitalMedias, array('class' => 'form-control'), 'oadh-dmm-parent-label', 'oadh-dmm-parent-id', null, 'fa-files-o') !!}
							{!! Form::hidden('oadh-dmm-parent-id', null, array('id'  =>  'oadh-dmm-parent-id')) !!}
					    {!! Form::hidden('oadh-dmm-id', null, array('id' => 'oadh-dmm-id')) !!}
			  		</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="row">
							<div class="col-lg-4" id="oadh-dmm-abbreviation-div">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-dmm-abbreviation', Lang::get('decima-oadh::digital-media-management.abbreviation'), array('class' => 'control-label')) !!}
							    {!! Form::text('oadh-dmm-abbreviation', null , array('id' => 'oadh-dmm-abbreviation', 'class' => 'form-control')) !!}
					  		</div>
							</div>
							<div class="col-lg-8" id="oadh-dmm-name-div">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-dmm-name', Lang::get('form.name'), array('class' => 'control-label')) !!}
							    {!! Form::text('oadh-dmm-name', null , array('id' => 'oadh-dmm-name', 'class' => 'form-control', 'data-mg-required' => '')) !!}
					  		</div>
							</div>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div id='oadh-dmm-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals('oadh-dmm-', $appInfo['id']) !!}
</div>
<div id='oadh-dmm-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-dmm-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-dmm-delete-message">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-dmm-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
