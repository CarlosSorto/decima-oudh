@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-mm-new-action', null, array('id' => 'oadh-mm-new-action')) !!}
{!! Form::hidden('oadh-mm-edit-action', null, array('id' => 'oadh-mm-edit-action', 'data-content' => Lang::get('decima-oadh::multimedia-management.editHelpText'))) !!}
{!! Form::hidden('oadh-mm-remove-action', null, array('id' => 'oadh-mm-remove-action', 'data-content' => Lang::get('decima-oadh::multimedia-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-mm-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-mm-btn-delete-helper', 'class' => 'hidden')) !!}
<style></style>

<script type='text/javascript'>

	//For grids with multiselect enabled
	// function oadhMmOnSelectRowEvent(id)
	// {
	// 	var selRowIds = $('#oadh-mm-grid').jqGrid('getGridParam', 'selarrrow'), id;
	//
	// 	if(selRowIds.length == 0)
	// 	{
	// 		$('#oadh-mm-btn-group-2').disabledButtonGroup();
	// 		cleanJournals('oadh-mm-');
	// 		// cleanFiles('oadh-mm-')
	// 	}
	// 	else if(selRowIds.length == 1)
	// 	{
	// 		$('#oadh-mm-btn-group-2').enableButtonGroup();
	//
	// 		id = $('#oadh-mm-grid').getSelectedRowId('oadh_mm_id');
	//
	// 		if($('#oadh-mm-journals').attr('data-journalized-id') != id)
	// 		{
	// 			cleanJournals('oadh-mm-');
	// 			// getElementFiles('oadh-mm-', id);
	// 			getAppJournals('oadh-mm-','firstPage', id);
	// 		}
	//
	// 	}
	// 	else if(selRowIds.length > 1)
	// 	{
	// 		$('#oadh-mm-btn-group-2').disabledButtonGroup();
	// 		$('#oadh-mm-btn-delete').removeAttr('disabled');
	// 		cleanJournals('oadh-mm-');
	// 		// cleanFiles('oadh-mm-')
	// 	}
	// }


	//For grids with multiselect disabled
	function oadhMmOnSelectRowEvent()
	{
		var id = $('#oadh-mm-grid').getSelectedRowId('oadh_mm_id');

		if($('#oadh-mm-grid-section').attr('data-id') != id)
		{
			$('#oadh-mm-grid-section').attr('data-id', id);
			getAppJournals('oadh-mm-', 'firstPage', id);
			//getElementFiles('oadh-mm-', id);
		}

		$('#oadh-mm-btn-group-2').enableButtonGroup();
	}


	$(document).ready(function()
	{
		$('.oadh-mm-btn-tooltip').tooltip();

		$('#oadh-mm-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-mm-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-mm-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-mm-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-mm-').focus();
		});

		$('#oadh-mm-').focusout(function()
		{
			$('#oadh-mm-btn-save').focus();
		});

		$('#oadh-mm-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-mm-grid-section').collapse('show');
		});

		$('#oadh-mm-btn-new').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-mm-btn-toolbar').disabledButtonGroup();

			$('#oadh-mm-btn-group-3').enableButtonGroup();

			$('#oadh-mm-form-new-title').removeClass('hidden');

			$('#oadh-mm-journals-section').attr('data-target-id', '#oadh-mm-form-section');

			$('#oadh-mm-grid-section').collapse('hide');

			$('.decima-erp-tooltip').tooltip('hide');
		});

		$('#oadh-mm-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-mm-btn-toolbar').disabledButtonGroup();

			$('#oadh-mm-btn-group-1').enableButtonGroup();

			if($('#oadh-mm-journals-section').attr('data-target-id') == '' || $('#oadh-mm-journals-section').attr('data-target-id') == '#oadh-mm-form-section')
			{
				$('#oadh-mm-grid').trigger('reloadGrid');

				$('#oadh-mm-grid-section').attr('data-id', '');

				cleanJournals('oadh-mm-');
				// cleanFiles('oadh-mm-');
			}
			else
			{

			}
		});

		$('#oadh-mm-btn-export-xls').click(function()
		{
			if($('#oadh-mm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-mm-gridXlsButton').click();
			}
			else
			{

			}
		});

		$('#oadh-mm-btn-export-csv').click(function()
		{
			if($('#oadh-mm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-mm-gridCsvButton').click();
			}
			else
			{

			}
		});

		$('#oadh-mm-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-mm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-mm-grid').isRowSelected())
				{
					$('#oadh-mm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				$('#oadh-mm-btn-toolbar').disabledButtonGroup();

				$('#oadh-mm-btn-group-3').enableButtonGroup();

				$('#oadh-mm-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-mm-grid').getRowData($('#oadh-mm-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				// $('#oadh-mm-type-label').setAutocompleteLabel(rowData.oadh_mm_type);
				$('#oadh-mm-lang-label').setAutocompleteLabel(rowData.oadh_mm_lang);

				if (rowData.oadh_mm_tags.length > 0)
				{
					var ids = rowData.oadh_mm_tags.split(',');
					$.each(ids, function (index, token)
					{
						$('#oadh-mm-tags').tokenfield('createToken', token);
					});
				}

				$('#oadh-mm-form-edit-title').removeClass('hidden');

				$('#oadh-mm-journals-section').attr('data-target-id', '#oadh-mm-form-section');

				$('#oadh-mm-grid-section').collapse('hide');
			}
			else
			{

			}
		});

		$('#oadh-mm-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-mm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-mm-grid').isRowSelected())
				{
					$('#oadh-mm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-mm-grid').getRowData($('#oadh-mm-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-mm-delete-message').html($('#oadh-mm-delete-message').attr('data-default-label').replace(':name', rowData.oadh_mm_name));
			}
			else
			{

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-mm-modal-delete').modal('show');
		});

		$('#oadh-mm-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-mm-journals-section').attr('data-target-id') == '')
			{
				url = $('#oadh-mm-form').attr('action') + '/delete';
				//For grids with multiselect enabled
				//id = $('#oadh-mm-grid').getSelectedRowsIdCell('oadh_mm_id');
				//For grids with multiselect disabled
				id = $('#oadh-mm-grid').getSelectedRowId('oadh_mm_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify({'_token':$('#app-token').val(), 'id':id}),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-mm-btn-toolbar', false);
					$('#oadh-mm-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-mm-btn-refresh').click();
						$('#oadh-mm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}
					else if(json.info)
					{
						$('#oadh-mm-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 12000);
					}

					$('#oadh-mm-modal-delete').modal('hide');
					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-mm-btn-save').click(function()
		{
			var url = $('#oadh-mm-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-mm-journals-section').attr('data-target-id') == '#oadh-mm-form-section')
			{
				if(!$('#oadh-mm-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-mm-id').isEmpty())
				{
					url = url + '/create';
				}
				else
				{
					url = url + '/update';
					action = 'edit';
				}

				data = $('#oadh-mm-form').formToObject('oadh-mm-');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-mm-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-mm-journals-section').attr('data-target-id') == '#oadh-mm-form-section')
						{
							$('#oadh-mm-btn-close').click();
							$('#oadh-mm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
						}
						else
						{
							// $('#oadh-mm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-mm-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-mm-journals-section').attr('data-target-id') == '#oadh-mm-form-section')
						{
							$('#oadh-mm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-mm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-mm-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			// oadh-mm-form-section
			if($('#oadh-mm-journals-section').attr('data-target-id') == '#oadh-mm-form-section')
			{
				$('#oadh-mm-form-new-title').addClass('hidden');
				$('#oadh-mm-form-edit-title').addClass('hidden');
				$('#oadh-mm-btn-refresh').click();
				$('#oadh-mm-form').jqMgVal('clearForm');
				$('#oadh-mm-form-section').collapse('hide');

				$('#oadh-mm-form').clearTags();
			}
			else
			{

			}

			$('#oadh-mm-btn-group-1').enableButtonGroup();
			$('#oadh-mm-btn-group-3').disabledButtonGroup();
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-mm-journals-section').attr('data-target-id', '')
		});

		$('#oadh-mm-btn-edit-helper').click(function()
	  {
			showButtonHelper('oadh-mm-btn-close', 'oadh-mm-btn-group-2', $('#oadh-mm-edit-action').attr('data-content'));
	  });

		$('#oadh-mm-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-mm-btn-close', 'oadh-mm-btn-group-2', $('#oadh-mm-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-mm-new-action').isEmpty())
		{
			$('#oadh-mm-btn-new').click();
		}

		if(!$('#oadh-mm-edit-action').isEmpty())
		{
			showButtonHelper('oadh-mm-btn-close', 'oadh-mm-btn-group-2', $('#oadh-mm-edit-action').attr('data-content'));
		}

		if(!$('#oadh-mm-delete-action').isEmpty())
		{
			showButtonHelper('oadh-mm-btn-close', 'oadh-mm-btn-group-2', $('#oadh-mm-delete-action').attr('data-content'));
		}

		setTimeout(function(){
			$('#oadh-mm-tags').tokenfield(
			{
				showAutocompleteOnFocus: true,
				beautify:false
			});
		});
	});
</script>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-mm-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-mm-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new'), array('id' => 'oadh-mm-btn-new', 'class' => 'btn btn-default oadh-mm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-oadh::multimedia-management.new'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-mm-btn-refresh', 'class' => 'btn btn-default oadh-mm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
         		<li><a id='oadh-mm-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> {{ Lang::get('decima-accounting::journal-management.standardSpreadsheet') . ' (' . Lang::get('form.spreadsheet') . ')' }}</a></li>
         		<!-- <li><a id='oadh-mm-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li> -->
       		</ul>
				</div>
			</div>
			<div id="oadh-mm-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-mm-btn-edit', 'class' => 'btn btn-default oadh-mm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::multimedia-management.edit'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-mm-btn-delete', 'class' => 'btn btn-default oadh-mm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::multimedia-management.delete'))) !!}
				{{-- Form::button('<i class="fa fa-upload"></i> ', array('id' => 'oadh-mm-btn-upload', 'class' => 'btn btn-default oadh-mm-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-folder' => Lang::get('decima-oadh::multimedia-management.folder'), 'data-original-title' => Lang::get('toolbar.uploadLongText'))) --}}
				{{-- Form::button('<i class="fa fa-files-o"></i> ', array('id' => 'oadh-mm-btn-show-files', 'class' => 'btn btn-default oadh-mm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showFilesLongText'))) --}}
				{{-- Form::button('<i class="fa fa-history"></i> ', array('id' => 'oadh-mm-btn-show-history', 'class' => 'btn btn-default oadh-mm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showHistoryLongText'))) --}}
			</div>
			<div id="oadh-mm-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-mm-btn-save', 'class' => 'btn btn-default oadh-mm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::multimedia-management.save'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-mm-btn-close', 'class' => 'btn btn-default oadh-mm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-mm-grid-section' class='app-grid collapse in' data-id='' data-app-grid-id='oadh-mm-grid'>
			{!!
			GridRender::setGridId('oadh-mm-grid')
				->enablefilterToolbar(false, false)
				->hideXlsExporter()
  			->hideCsvExporter()
	    	->setGridOption('url',URL::to('ucaoadh/cms/multimedia-management/grid-data'))
	    	->setGridOption('caption', Lang::get('decima-oadh::multimedia-management.gridTitle'))
				->setGridOption('filename', Lang::get('decima-oadh::multimedia-management.gridTitle'))
				->setGridOption('height', 'auto')
				->setGridOption('multiselect', false)
				->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
	    	->setGridOption('postData',array('_token' => Session::token()))
				->setGridEvent('onSelectRow', 'oadhMmOnSelectRowEvent')
	    	->addColumn(array('index' => 'm.id', 'name' => 'oadh_mm_id', 'hidden' => true))
				->addColumn(array('label' => Lang::get('form.date'), 'index' => 'p.date' ,'name' => 'oadh_mm_date', 'formatter' => 'date', 'align' => 'center'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.title'), 'index' => 'm.title' ,'name' => 'oadh_mm_title'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.description'), 'index' => 'm.descripcion' ,'name' => 'oadh_mm_description'))
				->addColumn(array('label' => Lang::get('decima-oadh::publication-management.lang'), 'index' => 'p.lang' ,'name' => 'oadh_mm_lang', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::publication-management.langGridText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.tags'), 'index' => 'm.tags' ,'name' => 'oadh_mm_tags'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::multimedia-management.imageUrl'), 'index' => 'm.image_url' ,'name' => 'oadh_mm_image_url'))
				->addColumn(array('label' => Lang::get('decima-oadh::multimedia-management.isHighlighted'), 'index' => 'm.is_highlighted' ,'name' => 'oadh_mm_is_highlighted', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.booleanText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70, 'hidden' => false))
	    	->renderGrid();
			!!}
		</div>
	</div>
</div>
<div id='oadh-mm-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-mm-form', 'url' => URL::to('ucaoadh/cms/multimedia-management'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-mm-form-new-title" class="hidden">{{ Lang::get('decima-oadh::multimedia-management.formNewTitle') }}</legend>
				<legend id="oadh-mm-form-edit-title" class="hidden">{{ Lang::get('decima-oadh::multimedia-management.formEditTitle') }}</legend>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-mm-date', Lang::get('form.date'), array('class' => 'control-label')) !!}
							{!! Form::date('oadh-mm-date', array('class' => 'form-control', 'data-mg-required' => '')) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-mm-title', Lang::get('decima-oadh::publication-management.title'), array('class' => 'control-label')) !!}
							{!! Form::text('oadh-mm-title', null , array('id' => 'oadh-mm-title', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							{!! Form::hidden('oadh-mm-id', null, array('id' => 'oadh-mm-id')) !!}
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-mm-lang', Lang::get('decima-oadh::publication-management.lang'), array('class' => 'control-label ')) !!}
							{!! Form::autocomplete('oadh-mm-lang-label', array(array('label'=>Lang::get('decima-oadh::publication-management.es'), 'value'=>'es'), array('label'=>Lang::get('decima-oadh::publication-management.en'), 'value'=>'en')), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-mm-lang-label', 'oadh-mm-lang', null, null) !!}
							{!! Form::hidden('oadh-mm-lang', null, array('id'=> 'oadh-mm-lang')) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-mm-description', Lang::get('decima-oadh::publication-management.description'), array('class' => 'control-label')) !!}
							{!! Form::textareacustom('oadh-mm-description', 2, 600, array('id' => 'oadh-mm-description', 'class' => 'form-control', 'data-mg-required' => '')) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-mm-tags', Lang::get('decima-oadh::publication-management.tags'), array('class' => 'control-label')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-tags"></i>
								</span>
								{!! Form::text('oadh-mm-tags', null , array('id' => 'oadh-mm-tags', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-mm-image-url', Lang::get('decima-oadh::multimedia-management.imageUrl'), array('class' => 'control-label')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-link"></i>
								</span>
								{!! Form::text('oadh-mm-image-url', null , array('id' => 'oadh-mm-image-url', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group checkbox">
							<label class="control-label">
								{!! Form::checkbox('oadh-mm-is-highlighted', 'S',false, array('id' => 'oadh-mm-is-highlighted')) !!}
								{{ Lang::get('decima-oadh::multimedia-management.isHighlightedForm') }}
							</label>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div id='oadh-mm-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals('oadh-mm-', $appInfo['id']) !!}
</div>
<div id='oadh-mm-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-mm-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-mm-delete-message">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
				<!-- <p id="oadh-mm-delete-message" data-default-label="{{ Lang::get('decima-oadh::multimedia-management.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-mm-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
