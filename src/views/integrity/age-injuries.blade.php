@extends('layouts.base')

@section('container')
{!! Form::hidden('inte-ai-new-action', null, array('id' => 'inte-ai-new-action')) !!}
{!! Form::hidden('inte-ai-edit-action', null, array('id' => 'inte-ai-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('inte-ai-remove-action', null, array('id' => 'inte-ai-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'inte-ai-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'inte-ai-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_inte-ai-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeAiOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#inte-ai-grid').getSelectedRowId('file_id'), rowData = $('#inte-ai-grid').getRowData($('#inte-ai-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#inte-ai-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('inte-ai-', 'firstPage', id);

				$('#inte-ai-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'ai.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#inte-ai-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'ai.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#inte-ai-btn-group-2').enableButtonGroup();

		}

		function freeAiDetailOnSelectRowEvent()
		{
			var selRowIds = $('#inte-ai-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#inte-ai-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#inte-ai-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#inte-ai-detail-btn-group-2').disabledButtonGroup();
				$('#inte-ai-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeAiOnLoadCompleteEvent()
		{
			$('#inte-ai-temp-data').jqGrid('clearGridData');
			$('#inte-ai-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.inte-ai-btn-tooltip').tooltip();

			$('#inte-ai-mp-form').jqMgVal('addFormFieldsValidations');

			$('#inte-ai-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#inte-ai-form-section').on('hidden.bs.collapse', function ()
			{
				$('#inte-ai-grid-section').collapse('show');

				$('#inte-ai-journals-section').collapse('show');

				$('#inte-ai-filters').show();
			});

			$('#inte-ai-form-section').on('shown.bs.collapse', function ()
			{
				// $('#inte-ai-').focus();
			});

			$('#inte-ai-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#inte-ai-btn-refresh').click();
	    });

			$('#inte-ai-').focusout(function()
			{
				$('#inte-ai-btn-save').focus();
			});

			$('#inte-ai-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.inte-ai-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Aino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#inte-ai-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#inte-ai-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('inte-ai-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('inte-ai-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('inte-ai-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#inte-ai-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-ai-btn-toolbar').disabledButtonGroup();
				$('#inte-ai-btn-group-1').enableButtonGroup();

				if($('#inte-ai-journals-section').attr('data-target-id') == '' || $('#inte-ai-journals-section').attr('data-target-id') == '#inte-ai-form-section')
				{
					$('#inte-ai-grid').trigger('reloadGrid');
					cleanJournals('inte-ai-');
				}
				else
				{

				}
			});

			$('#inte-ai-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#inte-ai-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#inte-ai-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#inte-ai-detail-btn-export-xls').click(function()
			{
				$('#inte-ai-back-detail-gridXlsButton').click();
			});

			$('#inte-ai-detail-btn-export-csv').click(function()
			{
				$('#inte-ai-back-detail-gridCsvButton').click();
			});

			$('#inte-ai-btn-export-xls').click(function()
			{
				if($('#inte-ai-journals-section').attr('data-target-id') == '')
				{
					$('#inte-ai-gridXlsButton').click();
				}
			});

			$('#inte-ai-btn-export-csv').click(function()
			{
				if($('#inte-ai-journals-section').attr('data-target-id') == '')
				{
					$('#inte-ai-gridCsvButton').click();
				}
			});

			$('#inte-ai-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-ai-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-ai-grid').isRowSelected())
					{
						$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-ai-modal-delete-file').modal('show');
			});

			$('#inte-ai-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-ai-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-ai-btn-toolbar', false);
						$('#inte-ai-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-ai-modal-delete-prod').click();
							$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-ai-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#inte-ai-grid').trigger('reloadGrid');
						$('#inte-ai-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#inte-ai-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-ai-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-ai-grid').isRowSelected())
					{
						$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-ai-modal-delete-prod').modal('show');
			});

			$('#inte-ai-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-ai-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-ai-btn-toolbar', false);
						$('#inte-ai-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-ai-modal-delete-prod').click();
							$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#inte-ai-prod-data').trigger('reloadGrid');
							$('#inte-ai-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-ai-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-ai-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-ai-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-ai-grid').isRowSelected())
					{
						$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-ai-modal-generate').modal('show');
			});

			$('#inte-ai-btn-modal-generate').click(function()
			{
				var rowData = $('#inte-ai-grid').getRowData($('#inte-ai-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-ai-btn-toolbar', false);
						$('#inte-ai-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-ai-btn-refresh').click();
							$("#inte-ai-btn-group-2").disabledButtonGroup();
							$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-ai-btn-refresh').click();
							$("#inte-ai-btn-group-2").disabledButtonGroup();
							$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-ai-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-ai-btn-process').click(function()
			{

				if(!$('#inte-ai-grid').isRowSelected())
				{
					$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#inte-ai-grid').getRowData($('#inte-ai-grid').jqGrid('getGridParam', 'selrow'));

				$('#inte-ai-mp-form').jqMgVal('clearContextualClasses');
				$('#inte-ai-id').val(rowData.file_id);
				$('#inte-ai-name').val(rowData.file_name);
				$('#inte-ai-system-route').val(rowData.file_system_route);

				$('#inte-ai-mp-modal').modal('show');
			});

			$('#inte-ai-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#inte-ai-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#inte-ai-mp-form').formToObject('inte-ai-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-ai-btn-toolbar', false);
						$('#inte-ai-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#inte-ai-btn-refresh').click();
							$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#inte-ai-temp-data, #inte-ai-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'ai.file_id','op':'eq','data':'" + $('#inte-ai-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-ai-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#inte-ai-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#inte-ai-edit-action').isEmpty())
			{
				showButtonHelper('inte-ai-btn-close', 'inte-ai-btn-group-2', $('#inte-ai-edit-action').attr('data-content'));
			}

			$('#inte-ai-btn-delete-helper').click(function()
		  {
				showButtonHelper('inte-ai-btn-close', 'inte-ai-btn-group-2', $('#inte-ai-delete-action').attr('data-content'));
		  });

			if(!$('#inte-ai-delete-action').isEmpty())
			{
				showButtonHelper('inte-ai-btn-close', 'inte-ai-btn-group-2', $('#inte-ai-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="inte-ai-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="inte-ai-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'inte-ai-btn-upload', 'class' => 'btn btn-default inte-ai-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'inte-ai-btn-refresh', 'class' => 'btn btn-default inte-ai-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="inte-ai-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'inte-ai-btn-process', 'class' => 'btn btn-default inte-ai-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'inte-ai-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'inte-ai-btn-delete-prod', 'class' => 'btn btn-default inte-ai-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'inte-ai-btn-delete-file', 'class' => 'btn btn-default inte-ai-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='inte-ai-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='inte-ai-grid'>
				{!!
				GridRender::setGridId("inte-ai-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeAiOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeAiOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#inte-ai-temp-data-tab" aria-controls="inte-ai-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#inte-ai-processed-data-tab" aria-controls="inte-ai-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="inte-ai-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-ai-front-temp-data'>
      				{!!
      				GridRender::setGridId('inte-ai-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'ai.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'ai.id', 'name' => 'oadh_ai_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'ai.file_id', 'name' => 'oadh_ai_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'ai.year', 'name' => 'oadh_ai_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'ai.sex', 'name' => 'oadh_ai_sex', 'align' => 'center'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'ai.crime', 'name' => 'oadh_ai_crime', 'align' => 'center'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.ageRange'), 'index' => 'ai.age_range', 'name' => 'oadh_ai_age_range', 'align' => 'center', 'hidden' => false))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.total'), 'index' => 'ai.total', 'name' => 'oadh_ai_total', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'ai.status', 'name' => 'oadh_ai_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '40'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="inte-ai-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-ai-front-processed-grid'>
              {!!
              GridRender::setGridId('inte-ai-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'ai.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'ai.id', 'name' => 'oadh_ai_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'ai.file_id', 'name' => 'oadh_ai_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'ai.year', 'name' => 'oadh_ai_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'ai.sex', 'name' => 'oadh_ai_sex', 'align' => 'center'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'ai.crime', 'name' => 'oadh_ai_crime', 'align' => 'center'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.ageRange'), 'index' => 'ai.age_range', 'name' => 'oadh_ai_age_range', 'align' => 'center', 'hidden' => false))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.total'), 'index' => 'ai.total', 'name' => 'oadh_ai_total', 'align' => 'center', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='inte-ai-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='inte-ai-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'inte-ai-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ai-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-ai-header-rows-number', 1 , array('id' => 'inte-ai-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('inte-ai-id', null, array('id' => 'inte-ai-id')) !!}
											{!! Form::hidden('inte-ai-name', null, array('id' => 'inte-ai-name')) !!}
											{!! Form::hidden('inte-ai-system-route', null, array('id' => 'inte-ai-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ai-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-ai-last-row-number', null, array('id' => 'inte-ai-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ai-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('inte-ai-mp-date-format', null, array('id' => 'inte-ai-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ai-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ai-year', 'A' , array('id' => 'inte-ai-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ai-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ai-sex', 'B' , array('id' => 'inte-ai-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ai-crime', Lang::get('decima-oadh::back-end-column.crime'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ai-crime', 'C' , array('id' => 'inte-ai-crime', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ai-age-range', Lang::get('decima-oadh::back-end-column.ageRange'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ai-age-range', 'D' , array('id' => 'inte-ai-age-range', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
                <!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ai-total', Lang::get('decima-oadh::back-end-column.total'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ai-total', 'D' , array('id' => 'inte-ai-total', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="inte-ai-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='inte-ai-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-ai-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-ai-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-ai-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-ai-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-ai-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-ai-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-ai-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="inte-ai-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-ai-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
