@extends('layouts.base')

@section('container')
{!! Form::hidden('inte-wo-new-action', null, array('id' => 'inte-wo-new-action')) !!}
{!! Form::hidden('inte-wo-edit-action', null, array('id' => 'inte-wo-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('inte-wo-remove-action', null, array('id' => 'inte-wo-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'inte-wo-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'inte-wo-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_inte-wo-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeWoOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#inte-wo-grid').getSelectedRowId('file_id'), rowData = $('#inte-wo-grid').getRowData($('#inte-wo-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#inte-wo-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('inte-wo-', 'firstPage', id);

				$('#inte-wo-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'wo.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#inte-wo-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'wo.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#inte-wo-btn-group-2').enableButtonGroup();

		}

		function freeWoDetailOnSelectRowEvent()
		{
			var selRowIds = $('#inte-wo-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#inte-wo-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#inte-wo-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#inte-wo-detail-btn-group-2').disabledButtonGroup();
				$('#inte-wo-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeWoOnLoadCompleteEvent()
		{
			$('#inte-wo-temp-data').jqGrid('clearGridData');
			$('#inte-wo-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.inte-wo-btn-tooltip').tooltip();

			$('#inte-wo-mp-form').jqMgVal('addFormFieldsValidations');

			$('#inte-wo-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#inte-wo-form-section').on('hidden.bs.collapse', function ()
			{
				$('#inte-wo-grid-section').collapse('show');

				$('#inte-wo-journals-section').collapse('show');

				$('#inte-wo-filters').show();
			});

			$('#inte-wo-form-section').on('shown.bs.collapse', function ()
			{
				// $('#inte-wo-').focus();
			});

			$('#inte-wo-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#inte-wo-btn-refresh').click();
	    });

			$('#inte-wo-').focusout(function()
			{
				$('#inte-wo-btn-save').focus();
			});

			$('#inte-wo-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.inte-wo-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#inte-wo-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#inte-wo-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('inte-wo-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('inte-wo-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('inte-wo-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#inte-wo-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-wo-btn-toolbar').disabledButtonGroup();
				$('#inte-wo-btn-group-1').enableButtonGroup();

				if($('#inte-wo-journals-section').attr('data-target-id') == '' || $('#inte-wo-journals-section').attr('data-target-id') == '#inte-wo-form-section')
				{
					$('#inte-wo-grid').trigger('reloadGrid');
					cleanJournals('inte-wo-');
				}
				else
				{

				}
			});

			$('#inte-wo-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#inte-wo-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#inte-wo-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#inte-wo-detail-btn-export-xls').click(function()
			{
				$('#inte-wo-back-detail-gridXlsButton').click();
			});

			$('#inte-wo-detail-btn-export-csv').click(function()
			{
				$('#inte-wo-back-detail-gridCsvButton').click();
			});

			$('#inte-wo-btn-export-xls').click(function()
			{
				if($('#inte-wo-journals-section').attr('data-target-id') == '')
				{
					$('#inte-wo-gridXlsButton').click();
				}
			});

			$('#inte-wo-btn-export-csv').click(function()
			{
				if($('#inte-wo-journals-section').attr('data-target-id') == '')
				{
					$('#inte-wo-gridCsvButton').click();
				}
			});

			$('#inte-wo-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-wo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-wo-grid').isRowSelected())
					{
						$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-wo-modal-delete-file').modal('show');
			});

			$('#inte-wo-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-wo-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-wo-btn-toolbar', false);
						$('#inte-wo-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-wo-modal-delete-prod').click();
							$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-wo-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#inte-wo-grid').trigger('reloadGrid');
						$('#inte-wo-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#inte-wo-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-wo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-wo-grid').isRowSelected())
					{
						$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-wo-modal-delete-prod').modal('show');
			});

			$('#inte-wo-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-wo-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-wo-btn-toolbar', false);
						$('#inte-wo-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-wo-modal-delete-prod').click();
							$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#inte-wo-prod-data').trigger('reloadGrid');
							$('#inte-wo-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-wo-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-wo-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-wo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-wo-grid').isRowSelected())
					{
						$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-wo-modal-generate').modal('show');
			});

			$('#inte-wo-btn-modal-generate').click(function()
			{
				var rowData = $('#inte-wo-grid').getRowData($('#inte-wo-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-wo-btn-toolbar', false);
						$('#inte-wo-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-wo-btn-refresh').click();
							$("#inte-wo-btn-group-2").disabledButtonGroup();
							$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-wo-btn-refresh').click();
							$("#inte-wo-btn-group-2").disabledButtonGroup();
							$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-wo-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-wo-btn-process').click(function()
			{

				if(!$('#inte-wo-grid').isRowSelected())
				{
					$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#inte-wo-grid').getRowData($('#inte-wo-grid').jqGrid('getGridParam', 'selrow'));

				$('#inte-wo-mp-form').jqMgVal('clearContextualClasses');
				$('#inte-wo-id').val(rowData.file_id);
				$('#inte-wo-name').val(rowData.file_name);
				$('#inte-wo-system-route').val(rowData.file_system_route);

				$('#inte-wo-mp-modal').modal('show');
			});

			$('#inte-wo-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#inte-wo-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#inte-wo-mp-form').formToObject('inte-wo-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-wo-btn-toolbar', false);
						$('#inte-wo-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#inte-wo-btn-refresh').click();
							$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#inte-wo-temp-data, #inte-wo-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'wo.file_id','op':'eq','data':'" + $('#inte-wo-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-wo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#inte-wo-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#inte-wo-edit-action').isEmpty())
			{
				showButtonHelper('inte-wo-btn-close', 'inte-wo-btn-group-2', $('#inte-wo-edit-action').attr('data-content'));
			}

			$('#inte-wo-btn-delete-helper').click(function()
		  {
				showButtonHelper('inte-wo-btn-close', 'inte-wo-btn-group-2', $('#inte-wo-delete-action').attr('data-content'));
		  });

			if(!$('#inte-wo-delete-action').isEmpty())
			{
				showButtonHelper('inte-wo-btn-close', 'inte-wo-btn-group-2', $('#inte-wo-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="inte-wo-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="inte-wo-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'inte-wo-btn-upload', 'class' => 'btn btn-default inte-wo-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'inte-wo-btn-refresh', 'class' => 'btn btn-default inte-wo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="inte-wo-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'inte-wo-btn-process', 'class' => 'btn btn-default inte-wo-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'inte-wo-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'inte-wo-btn-delete-prod', 'class' => 'btn btn-default inte-wo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'inte-wo-btn-delete-file', 'class' => 'btn btn-default inte-wo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='inte-wo-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='inte-wo-grid'>
				{!!
				GridRender::setGridId("inte-wo-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeWoOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeWoOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#inte-wo-temp-data-tab" aria-controls="inte-wo-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#inte-wo-processed-data-tab" aria-controls="inte-wo-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="inte-wo-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-wo-front-temp-data'>
      				{!!
      				GridRender::setGridId('inte-wo-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'wo.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'wo.id', 'name' => 'oadh_wo_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'wo.file_id', 'name' => 'oadh_wo_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'wo.year', 'name' => 'oadh_wo_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'wo.crime', 'name' => 'oadh_wo_crime', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.category'), 'index' => 'wo.category', 'name' => 'oadh_wo_category', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'wo.sex', 'name' => 'oadh_wo_sex', 'align' => 'center', 'width' => '15'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'wo.department', 'name' => 'oadh_wo_department', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'wo.status', 'name' => 'oadh_wo_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="inte-wo-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-wo-front-processed-grid'>
              {!!
              GridRender::setGridId('inte-wo-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'wo.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'wo.id', 'name' => 'oadh_wo_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'wo.file_id', 'name' => 'oadh_wo_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'wo.year', 'name' => 'oadh_wo_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'wo.crime', 'name' => 'oadh_wo_crime', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.category'), 'index' => 'wo.category', 'name' => 'oadh_wo_category', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'wo.sex', 'name' => 'oadh_wo_sex', 'align' => 'center', 'width' => '15'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'wo.department', 'name' => 'oadh_wo_department', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='inte-wo-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='inte-wo-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'inte-wo-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-wo-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-wo-header-rows-number', 1 , array('id' => 'inte-wo-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('inte-wo-id', null, array('id' => 'inte-wo-id')) !!}
											{!! Form::hidden('inte-wo-name', null, array('id' => 'inte-wo-name')) !!}
											{!! Form::hidden('inte-wo-system-route', null, array('id' => 'inte-wo-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-wo-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-wo-last-row-number', null, array('id' => 'inte-wo-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-wo-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('inte-wo-mp-date-format', null, array('id' => 'inte-wo-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-wo-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-wo-year', 'A' , array('id' => 'inte-wo-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-wo-crime', Lang::get('decima-oadh::back-end-column.crime'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-wo-crime', 'B' , array('id' => 'inte-wo-crime', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-wo-category', Lang::get('decima-oadh::back-end-column.category'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-wo-category', 'C' , array('id' => 'inte-wo-category', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-wo-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-wo-sex', 'D' , array('id' => 'inte-wo-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-wo-department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-wo-department', 'E' , array('id' => 'inte-wo-department', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="inte-wo-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='inte-wo-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-wo-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-wo-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-wo-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-wo-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-wo-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-wo-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-wo-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="inte-wo-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-wo-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
