@extends('layouts.base')

@section('container')
{!! Form::hidden('inte-si-new-action', null, array('id' => 'inte-si-new-action')) !!}
{!! Form::hidden('inte-si-edit-action', null, array('id' => 'inte-si-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('inte-si-remove-action', null, array('id' => 'inte-si-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'inte-si-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'inte-si-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_inte-si-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeSiOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#inte-si-grid').getSelectedRowId('file_id'), rowData = $('#inte-si-grid').getRowData($('#inte-si-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#inte-si-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('inte-si-', 'firstPage', id);

				$('#inte-si-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'si.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#inte-si-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'si.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#inte-si-btn-group-2').enableButtonGroup();

		}

		function freeSiDetailOnSelectRowEvent()
		{
			var selRowIds = $('#inte-si-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#inte-si-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#inte-si-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#inte-si-detail-btn-group-2').disabledButtonGroup();
				$('#inte-si-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeSiOnLoadCompleteEvent()
		{
			$('#inte-si-temp-data').jqGrid('clearGridData');
			$('#inte-si-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.inte-si-btn-tooltip').tooltip();

			$('#inte-si-mp-form').jqMgVal('addFormFieldsValidations');

			$('#inte-si-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#inte-si-form-section').on('hidden.bs.collapse', function ()
			{
				$('#inte-si-grid-section').collapse('show');

				$('#inte-si-journals-section').collapse('show');

				$('#inte-si-filters').show();
			});

			$('#inte-si-form-section').on('shown.bs.collapse', function ()
			{
				// $('#inte-si-').focus();
			});

			$('#inte-si-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#inte-si-btn-refresh').click();
	    });

			$('#inte-si-').focusout(function()
			{
				$('#inte-si-btn-save').focus();
			});

			$('#inte-si-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.inte-si-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#inte-si-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#inte-si-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('inte-si-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('inte-si-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('inte-si-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#inte-si-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-si-btn-toolbar').disabledButtonGroup();
				$('#inte-si-btn-group-1').enableButtonGroup();

				if($('#inte-si-journals-section').attr('data-target-id') == '' || $('#inte-si-journals-section').attr('data-target-id') == '#inte-si-form-section')
				{
					$('#inte-si-grid').trigger('reloadGrid');
					cleanJournals('inte-si-');
				}
				else
				{

				}
			});

			$('#inte-si-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#inte-si-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#inte-si-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#inte-si-detail-btn-export-xls').click(function()
			{
				$('#inte-si-back-detail-gridXlsButton').click();
			});

			$('#inte-si-detail-btn-export-csv').click(function()
			{
				$('#inte-si-back-detail-gridCsvButton').click();
			});

			$('#inte-si-btn-export-xls').click(function()
			{
				if($('#inte-si-journals-section').attr('data-target-id') == '')
				{
					$('#inte-si-gridXlsButton').click();
				}
			});

			$('#inte-si-btn-export-csv').click(function()
			{
				if($('#inte-si-journals-section').attr('data-target-id') == '')
				{
					$('#inte-si-gridCsvButton').click();
				}
			});

			$('#inte-si-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-si-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-si-grid').isRowSelected())
					{
						$('#inte-si-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-si-modal-delete-file').modal('show');
			});

			$('#inte-si-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-si-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-si-btn-toolbar', false);
						$('#inte-si-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-si-modal-delete-prod').click();
							$('#inte-si-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-si-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-si-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#inte-si-grid').trigger('reloadGrid');
						$('#inte-si-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#inte-si-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-si-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-si-grid').isRowSelected())
					{
						$('#inte-si-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-si-modal-delete-prod').modal('show');
			});

			$('#inte-si-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-si-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-si-btn-toolbar', false);
						$('#inte-si-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-si-modal-delete-prod').click();
							$('#inte-si-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#inte-si-prod-data').trigger('reloadGrid');
							$('#inte-si-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-si-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-si-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-si-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-si-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-si-grid').isRowSelected())
					{
						$('#inte-si-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-si-modal-generate').modal('show');
			});

			$('#inte-si-btn-modal-generate').click(function()
			{
				var rowData = $('#inte-si-grid').getRowData($('#inte-si-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-si-btn-toolbar', false);
						$('#inte-si-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-si-btn-refresh').click();
							$("#inte-si-btn-group-2").disabledButtonGroup();
							$('#inte-si-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-si-btn-refresh').click();
							$("#inte-si-btn-group-2").disabledButtonGroup();
							$('#inte-si-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-si-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-si-btn-process').click(function()
			{

				if(!$('#inte-si-grid').isRowSelected())
				{
					$('#inte-si-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#inte-si-grid').getRowData($('#inte-si-grid').jqGrid('getGridParam', 'selrow'));

				$('#inte-si-mp-form').jqMgVal('clearContextualClasses');
				$('#inte-si-id').val(rowData.file_id);
				$('#inte-si-name').val(rowData.file_name);
				$('#inte-si-system-route').val(rowData.file_system_route);

				$('#inte-si-mp-modal').modal('show');
			});

			$('#inte-si-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#inte-si-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#inte-si-mp-form').formToObject('inte-si-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-si-btn-toolbar', false);
						$('#inte-si-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#inte-si-btn-refresh').click();
							$('#inte-si-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#inte-si-temp-data, #inte-si-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'si.file_id','op':'eq','data':'" + $('#inte-si-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-si-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#inte-si-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#inte-si-edit-action').isEmpty())
			{
				showButtonHelper('inte-si-btn-close', 'inte-si-btn-group-2', $('#inte-si-edit-action').attr('data-content'));
			}

			$('#inte-si-btn-delete-helper').click(function()
		  {
				showButtonHelper('inte-si-btn-close', 'inte-si-btn-group-2', $('#inte-si-delete-action').attr('data-content'));
		  });

			if(!$('#inte-si-delete-action').isEmpty())
			{
				showButtonHelper('inte-si-btn-close', 'inte-si-btn-group-2', $('#inte-si-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="inte-si-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="inte-si-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'inte-si-btn-upload', 'class' => 'btn btn-default inte-si-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'inte-si-btn-refresh', 'class' => 'btn btn-default inte-si-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="inte-si-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'inte-si-btn-process', 'class' => 'btn btn-default inte-si-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'inte-si-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'inte-si-btn-delete-prod', 'class' => 'btn btn-default inte-si-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'inte-si-btn-delete-file', 'class' => 'btn btn-default inte-si-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='inte-si-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='inte-si-grid'>
				{!!
				GridRender::setGridId("inte-si-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeSiOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeSiOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#inte-si-temp-data-tab" aria-controls="inte-si-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#inte-si-processed-data-tab" aria-controls="inte-si-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="inte-si-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-si-front-temp-data'>
      				{!!
      				GridRender::setGridId('inte-si-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'si.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'si.id', 'name' => 'oadh_si_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'si.file_id', 'name' => 'oadh_si_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'si.year', 'name' => 'oadh_si_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'si.department', 'name' => 'oadh_si_department', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'si.municipality', 'name' => 'oadh_si_municipality', 'align' => 'center', 'hidden' => false))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.total'), 'index' => 'si.total', 'name' => 'oadh_si_total', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'si.status', 'name' => 'oadh_si_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '40'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="inte-si-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-si-front-processed-grid'>
              {!!
              GridRender::setGridId('inte-si-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'si.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'si.id', 'name' => 'oadh_si_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'si.file_id', 'name' => 'oadh_si_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'si.year', 'name' => 'oadh_si_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'si.department', 'name' => 'oadh_si_department', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'si.municipality', 'name' => 'oadh_si_municipality', 'align' => 'center', 'hidden' => false))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.total'), 'index' => 'si.total', 'name' => 'oadh_si_total', 'align' => 'center', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='inte-si-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='inte-si-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'inte-si-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-si-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-si-header-rows-number', 1 , array('id' => 'inte-si-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('inte-si-id', null, array('id' => 'inte-si-id')) !!}
											{!! Form::hidden('inte-si-name', null, array('id' => 'inte-si-name')) !!}
											{!! Form::hidden('inte-si-system-route', null, array('id' => 'inte-si-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-si-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-si-last-row-number', null, array('id' => 'inte-si-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-si-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('inte-si-mp-date-format', null, array('id' => 'inte-si-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-si-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-si-year', 'A' , array('id' => 'inte-si-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-si-department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-si-department', 'B' , array('id' => 'inte-si-department', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-si-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-si-municipality', 'C' , array('id' => 'inte-si-municipality', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
                <!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-si-total', Lang::get('decima-oadh::back-end-column.total'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-si-total', 'D' , array('id' => 'inte-si-total', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->


							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="inte-si-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='inte-si-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-si-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-si-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-si-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-si-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-si-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-si-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-si-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="inte-si-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-si-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
