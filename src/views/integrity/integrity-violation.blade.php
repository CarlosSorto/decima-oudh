@extends('layouts.base')

@section('container')
{!! Form::hidden('inte-iv-new-action', null, array('id' => 'inte-iv-new-action')) !!}
{!! Form::hidden('inte-iv-edit-action', null, array('id' => 'inte-iv-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('inte-iv-remove-action', null, array('id' => 'inte-iv-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'inte-iv-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'inte-iv-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_inte-iv-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeIvOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#inte-iv-grid').getSelectedRowId('file_id'), rowData = $('#inte-iv-grid').getRowData($('#inte-iv-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#inte-iv-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('inte-iv-', 'firstPage', id);

				$('#inte-iv-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'iv.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#inte-iv-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'iv.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#inte-iv-btn-group-2').enableButtonGroup();

		}

		function freeIvDetailOnSelectRowEvent()
		{
			var selRowIds = $('#inte-iv-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#inte-iv-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#inte-iv-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#inte-iv-detail-btn-group-2').disabledButtonGroup();
				$('#inte-iv-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeIvOnLoadCompleteEvent()
		{
			$('#inte-iv-temp-data').jqGrid('clearGridData');
			$('#inte-iv-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.inte-iv-btn-tooltip').tooltip();

			$('#inte-iv-mp-form').jqMgVal('addFormFieldsValidations');

			$('#inte-iv-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#inte-iv-form-section').on('hidden.bs.collapse', function ()
			{
				$('#inte-iv-grid-section').collapse('show');

				$('#inte-iv-journals-section').collapse('show');

				$('#inte-iv-filters').show();
			});

			$('#inte-iv-form-section').on('shown.bs.collapse', function ()
			{
				// $('#inte-iv-').focus();
			});

			$('#inte-iv-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#inte-iv-btn-refresh').click();
	    });

			$('#inte-iv-').focusout(function()
			{
				$('#inte-iv-btn-save').focus();
			});

			$('#inte-iv-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.inte-iv-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Ivno lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#inte-iv-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#inte-iv-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('inte-iv-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('inte-iv-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('inte-iv-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#inte-iv-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-iv-btn-toolbar').disabledButtonGroup();
				$('#inte-iv-btn-group-1').enableButtonGroup();

				if($('#inte-iv-journals-section').attr('data-target-id') == '' || $('#inte-iv-journals-section').attr('data-target-id') == '#inte-iv-form-section')
				{
					$('#inte-iv-grid').trigger('reloadGrid');
					cleanJournals('inte-iv-');
				}
				else
				{

				}
			});

			$('#inte-iv-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#inte-iv-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#inte-iv-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#inte-iv-detail-btn-export-xls').click(function()
			{
				$('#inte-iv-back-detail-gridXlsButton').click();
			});

			$('#inte-iv-detail-btn-export-csv').click(function()
			{
				$('#inte-iv-back-detail-gridCsvButton').click();
			});

			$('#inte-iv-btn-export-xls').click(function()
			{
				if($('#inte-iv-journals-section').attr('data-target-id') == '')
				{
					$('#inte-iv-gridXlsButton').click();
				}
			});

			$('#inte-iv-btn-export-csv').click(function()
			{
				if($('#inte-iv-journals-section').attr('data-target-id') == '')
				{
					$('#inte-iv-gridCsvButton').click();
				}
			});

			$('#inte-iv-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-iv-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-iv-grid').isRowSelected())
					{
						$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-iv-modal-delete-file').modal('show');
			});

			$('#inte-iv-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-iv-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-iv-btn-toolbar', false);
						$('#inte-iv-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-iv-modal-delete-prod').click();
							$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-iv-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#inte-iv-grid').trigger('reloadGrid');
						$('#inte-iv-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#inte-iv-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-iv-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-iv-grid').isRowSelected())
					{
						$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-iv-modal-delete-prod').modal('show');
			});

			$('#inte-iv-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-iv-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-iv-btn-toolbar', false);
						$('#inte-iv-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-iv-modal-delete-prod').click();
							$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#inte-iv-prod-data').trigger('reloadGrid');
							$('#inte-iv-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-iv-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-iv-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-iv-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-iv-grid').isRowSelected())
					{
						$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-iv-modal-generate').modal('show');
			});

			$('#inte-iv-btn-modal-generate').click(function()
			{
				var rowData = $('#inte-iv-grid').getRowData($('#inte-iv-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-iv-btn-toolbar', false);
						$('#inte-iv-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-iv-btn-refresh').click();
							$("#inte-iv-btn-group-2").disabledButtonGroup();
							$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-iv-btn-refresh').click();
							$("#inte-iv-btn-group-2").disabledButtonGroup();
							$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-iv-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-iv-btn-process').click(function()
			{

				if(!$('#inte-iv-grid').isRowSelected())
				{
					$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#inte-iv-grid').getRowData($('#inte-iv-grid').jqGrid('getGridParam', 'selrow'));

				$('#inte-iv-mp-form').jqMgVal('clearContextualClasses');
				$('#inte-iv-id').val(rowData.file_id);
				$('#inte-iv-name').val(rowData.file_name);
				$('#inte-iv-system-route').val(rowData.file_system_route);

				$('#inte-iv-mp-modal').modal('show');
			});

			$('#inte-iv-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#inte-iv-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#inte-iv-mp-form').formToObject('inte-iv-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-iv-btn-toolbar', false);
						$('#inte-iv-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#inte-iv-btn-refresh').click();
							$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#inte-iv-temp-data, #inte-iv-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'iv.file_id','op':'eq','data':'" + $('#inte-iv-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-iv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#inte-iv-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#inte-iv-edit-action').isEmpty())
			{
				showButtonHelper('inte-iv-btn-close', 'inte-iv-btn-group-2', $('#inte-iv-edit-action').attr('data-content'));
			}

			$('#inte-iv-btn-delete-helper').click(function()
		  {
				showButtonHelper('inte-iv-btn-close', 'inte-iv-btn-group-2', $('#inte-iv-delete-action').attr('data-content'));
		  });

			if(!$('#inte-iv-delete-action').isEmpty())
			{
				showButtonHelper('inte-iv-btn-close', 'inte-iv-btn-group-2', $('#inte-iv-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="inte-iv-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="inte-iv-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'inte-iv-btn-upload', 'class' => 'btn btn-default inte-iv-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'inte-iv-btn-refresh', 'class' => 'btn btn-default inte-iv-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="inte-iv-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'inte-iv-btn-process', 'class' => 'btn btn-default inte-iv-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'inte-iv-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'inte-iv-btn-delete-prod', 'class' => 'btn btn-default inte-iv-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'inte-iv-btn-delete-file', 'class' => 'btn btn-default inte-iv-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='inte-iv-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='inte-iv-grid'>
				{!!
				GridRender::setGridId("inte-iv-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeIvOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeIvOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#inte-iv-temp-data-tab" aria-controls="inte-iv-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#inte-iv-processed-data-tab" aria-controls="inte-iv-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="inte-iv-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-iv-front-temp-data'>
      				{!!
      				GridRender::setGridId('inte-iv-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'iv.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'iv.id', 'name' => 'oadh_iv_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'iv.file_id', 'name' => 'oadh_iv_file_id', 'hidden' => true))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'iv.date', 'name' => 'oadh_iv_date', 'align' => 'center', 'hidden' => false, 'formatter' => 'date'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'iv.year', 'name' => 'oadh_iv_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'iv.sex', 'name' => 'oadh_iv_sex', 'align' => 'center'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'iv.age', 'name' => 'oadh_iv_age', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'iv.department', 'name' => 'oadh_iv_department', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'iv.municipality', 'name' => 'oadh_iv_municipality', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.rightTo'), 'index' => 'iv.right_to', 'name' => 'oadh_iv_right_to', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.institutions'), 'index' => 'iv.institutions', 'name' => 'oadh_iv_institutions', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.dependencies'), 'index' => 'iv.dependencies', 'name' => 'oadh_iv_dependencies', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'iv.status', 'name' => 'oadh_iv_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '40'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="inte-iv-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-iv-front-processed-grid'>
              {!!
              GridRender::setGridId('inte-iv-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'iv.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'iv.id', 'name' => 'oadh_iv_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'iv.file_id', 'name' => 'oadh_iv_file_id', 'hidden' => true))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'iv.date', 'name' => 'oadh_iv_date', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'iv.year', 'name' => 'oadh_iv_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'iv.sex', 'name' => 'oadh_iv_sex', 'align' => 'center'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'iv.age', 'name' => 'oadh_iv_age', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'iv.department', 'name' => 'oadh_iv_department', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'iv.municipality', 'name' => 'oadh_iv_municipality', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.rightTo'), 'index' => 'iv.right_to', 'name' => 'oadh_iv_right_to', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.institutions'), 'index' => 'iv.institutions', 'name' => 'oadh_iv_institutions', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.dependencies'), 'index' => 'iv.dependencies', 'name' => 'oadh_iv_dependencies', 'align' => 'center', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='inte-iv-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='inte-iv-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'inte-iv-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-iv-header-rows-number', 1 , array('id' => 'inte-iv-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('inte-iv-id', null, array('id' => 'inte-iv-id')) !!}
											{!! Form::hidden('inte-iv-name', null, array('id' => 'inte-iv-name')) !!}
											{!! Form::hidden('inte-iv-system-route', null, array('id' => 'inte-iv-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-iv-last-row-number', null, array('id' => 'inte-iv-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<!-- <div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('inte-iv-date-format', 'd/m/Y', array('id' => 'inte-iv-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div>
								</div> -->
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-iv-year', 'A' , array('id' => 'inte-iv-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-date', Lang::get('decima-oadh::back-end-column.date'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-iv-date', 'A' , array('id' => 'inte-iv-date', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->
                <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-iv-sex', 'B' , array('id' => 'inte-iv-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-age', Lang::get('decima-oadh::back-end-column.age'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-iv-age', 'C' , array('id' => 'inte-iv-age', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
                <div class="col-md-2" style="padding-right: 5px;">
                  <div class="form-group mg-hm help-block-hidden">
                    {!! Form::label('inte-iv-department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
                      {!! Form::text('inte-iv-department', 'D' , array('id' => 'inte-iv-department', 'class' => 'form-control', 'maxlength' => '1')) !!}
                    </div>
                  </div>
                </div>
                <div class="col-md-2" style="padding-right: 5px;">
                  <div class="form-group mg-hm help-block-hidden">
                    {!! Form::label('inte-iv-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden')) !!}
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
                      {!! Form::text('inte-iv-municipality', 'E' , array('id' => 'inte-iv-municipality', 'class' => 'form-control', 'maxlength' => '1')) !!}
                    </div>
                  </div>
                </div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-right-to', Lang::get('decima-oadh::back-end-column.rightTo'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-iv-right-to', 'F' , array('id' => 'inte-iv-right-to', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-violated-fact', Lang::get('decima-oadh::back-end-column.violatedFact'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-iv-violated-fact', 'G' , array('id' => 'inte-iv-violated-fact', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>

                <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-institutions', Lang::get('decima-oadh::back-end-column.institutions'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-iv-institutions', 'H' , array('id' => 'inte-iv-institutions', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
                <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-iv-dependencies', Lang::get('decima-oadh::back-end-column.dependencies'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-iv-dependencies', 'I' , array('id' => 'inte-iv-dependencies', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>


							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="inte-iv-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='inte-iv-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-iv-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-iv-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-iv-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-iv-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-iv-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-iv-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-iv-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="inte-iv-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-iv-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
