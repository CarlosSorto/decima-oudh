@extends('layouts.base')

@section('container')
{!! Form::hidden('inte-cv-new-action', null, array('id' => 'inte-cv-new-action')) !!}
{!! Form::hidden('inte-cv-edit-action', null, array('id' => 'inte-cv-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('inte-cv-remove-action', null, array('id' => 'inte-cv-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'inte-cv-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'inte-cv-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_inte-cv-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeCvOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#inte-cv-grid').getSelectedRowId('file_id'), rowData = $('#inte-cv-grid').getRowData($('#inte-cv-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#inte-cv-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('inte-cv-', 'firstPage', id);

				$('#inte-cv-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'cv.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#inte-cv-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'cv.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#inte-cv-btn-group-2').enableButtonGroup();

		}

		function freeCvDetailOnSelectRowEvent()
		{
			var selRowIds = $('#inte-cv-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#inte-cv-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#inte-cv-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#inte-cv-detail-btn-group-2').disabledButtonGroup();
				$('#inte-cv-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeCvOnLoadCompleteEvent()
		{
			$('#inte-cv-temp-data').jqGrid('clearGridData');
			$('#inte-cv-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.inte-cv-btn-tooltip').tooltip();

			$('#inte-cv-mp-form').jqMgVal('addFormFieldsValidations');

			$('#inte-cv-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#inte-cv-form-section').on('hidden.bs.collapse', function ()
			{
				$('#inte-cv-grid-section').collapse('show');

				$('#inte-cv-journals-section').collapse('show');

				$('#inte-cv-filters').show();
			});

			$('#inte-cv-form-section').on('shown.bs.collapse', function ()
			{
				// $('#inte-cv-').focus();
			});

			$('#inte-cv-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#inte-cv-btn-refresh').click();
	    });

			$('#inte-cv-').focusout(function()
			{
				$('#inte-cv-btn-save').focus();
			});

			$('#inte-cv-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.inte-cv-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#inte-cv-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#inte-cv-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('inte-cv-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('inte-cv-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('inte-cv-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#inte-cv-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-cv-btn-toolbar').disabledButtonGroup();
				$('#inte-cv-btn-group-1').enableButtonGroup();

				if($('#inte-cv-journals-section').attr('data-target-id') == '' || $('#inte-cv-journals-section').attr('data-target-id') == '#inte-cv-form-section')
				{
					$('#inte-cv-grid').trigger('reloadGrid');
					cleanJournals('inte-cv-');
				}
				else
				{

				}
			});

			$('#inte-cv-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#inte-cv-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#inte-cv-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#inte-cv-detail-btn-export-xls').click(function()
			{
				$('#inte-cv-back-detail-gridXlsButton').click();
			});

			$('#inte-cv-detail-btn-export-csv').click(function()
			{
				$('#inte-cv-back-detail-gridCsvButton').click();
			});

			$('#inte-cv-btn-export-xls').click(function()
			{
				if($('#inte-cv-journals-section').attr('data-target-id') == '')
				{
					$('#inte-cv-gridXlsButton').click();
				}
			});

			$('#inte-cv-btn-export-csv').click(function()
			{
				if($('#inte-cv-journals-section').attr('data-target-id') == '')
				{
					$('#inte-cv-gridCsvButton').click();
				}
			});

			$('#inte-cv-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-cv-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-cv-grid').isRowSelected())
					{
						$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-cv-modal-delete-file').modal('show');
			});

			$('#inte-cv-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-cv-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-cv-btn-toolbar', false);
						$('#inte-cv-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-cv-modal-delete-prod').click();
							$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-cv-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#inte-cv-grid').trigger('reloadGrid');
						$('#inte-cv-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#inte-cv-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-cv-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-cv-grid').isRowSelected())
					{
						$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-cv-modal-delete-prod').modal('show');
			});

			$('#inte-cv-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-cv-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-cv-btn-toolbar', false);
						$('#inte-cv-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-cv-modal-delete-prod').click();
							$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#inte-cv-prod-data').trigger('reloadGrid');
							$('#inte-cv-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-cv-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-cv-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-cv-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-cv-grid').isRowSelected())
					{
						$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-cv-modal-generate').modal('show');
			});

			$('#inte-cv-btn-modal-generate').click(function()
			{
				var rowData = $('#inte-cv-grid').getRowData($('#inte-cv-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-cv-btn-toolbar', false);
						$('#inte-cv-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-cv-btn-refresh').click();
							$("#inte-cv-btn-group-2").disabledButtonGroup();
							$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-cv-btn-refresh').click();
							$("#inte-cv-btn-group-2").disabledButtonGroup();
							$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-cv-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-cv-btn-process').click(function()
			{

				if(!$('#inte-cv-grid').isRowSelected())
				{
					$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#inte-cv-grid').getRowData($('#inte-cv-grid').jqGrid('getGridParam', 'selrow'));

				$('#inte-cv-mp-form').jqMgVal('clearContextualClasses');
				$('#inte-cv-id').val(rowData.file_id);
				$('#inte-cv-name').val(rowData.file_name);
				$('#inte-cv-system-route').val(rowData.file_system_route);

				$('#inte-cv-mp-modal').modal('show');
			});

			$('#inte-cv-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#inte-cv-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#inte-cv-mp-form').formToObject('inte-cv-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-cv-btn-toolbar', false);
						$('#inte-cv-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#inte-cv-btn-refresh').click();
							$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#inte-cv-temp-data, #inte-cv-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'cv.file_id','op':'eq','data':'" + $('#inte-cv-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-cv-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#inte-cv-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#inte-cv-edit-action').isEmpty())
			{
				showButtonHelper('inte-cv-btn-close', 'inte-cv-btn-group-2', $('#inte-cv-edit-action').attr('data-content'));
			}

			$('#inte-cv-btn-delete-helper').click(function()
		  {
				showButtonHelper('inte-cv-btn-close', 'inte-cv-btn-group-2', $('#inte-cv-delete-action').attr('data-content'));
		  });

			if(!$('#inte-cv-delete-action').isEmpty())
			{
				showButtonHelper('inte-cv-btn-close', 'inte-cv-btn-group-2', $('#inte-cv-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="inte-cv-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="inte-cv-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'inte-cv-btn-upload', 'class' => 'btn btn-default inte-cv-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'inte-cv-btn-refresh', 'class' => 'btn btn-default inte-cv-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="inte-cv-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'inte-cv-btn-process', 'class' => 'btn btn-default inte-cv-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'inte-cv-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'inte-cv-btn-delete-prod', 'class' => 'btn btn-default inte-cv-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'inte-cv-btn-delete-file', 'class' => 'btn btn-default inte-cv-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='inte-cv-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='inte-cv-grid'>
				{!!
				GridRender::setGridId("inte-cv-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeCvOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeCvOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#inte-cv-temp-data-tab" aria-controls="inte-cv-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#inte-cv-processed-data-tab" aria-controls="inte-cv-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="inte-cv-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-cv-front-temp-data'>
      				{!!
      				GridRender::setGridId('inte-cv-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'cv.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'cv.id', 'name' => 'oadh_cv_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'cv.file_id', 'name' => 'oadh_cv_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'cv.year', 'name' => 'oadh_cv_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'cv.crime', 'name' => 'oadh_cv_crime', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'cv.sex', 'name' => 'oadh_cv_sex', 'align' => 'center', 'width' => '15'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.ageRange'), 'index' => 'cv.age_range', 'name' => 'oadh_cv_age_range', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'cv.status', 'name' => 'oadh_cv_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="inte-cv-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-cv-front-processed-grid'>
              {!!
              GridRender::setGridId('inte-cv-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'cv.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'cv.id', 'name' => 'oadh_cv_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'cv.file_id', 'name' => 'oadh_cv_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'cv.year', 'name' => 'oadh_cv_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'cv.crime', 'name' => 'oadh_cv_crime', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'cv.sex', 'name' => 'oadh_cv_sex', 'align' => 'center', 'width' => '15'))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.ageRange'), 'index' => 'cv.age_range', 'name' => 'oadh_cv_age_range', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='inte-cv-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='inte-cv-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'inte-cv-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-cv-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-cv-header-rows-number', 1 , array('id' => 'inte-cv-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('inte-cv-id', null, array('id' => 'inte-cv-id')) !!}
											{!! Form::hidden('inte-cv-name', null, array('id' => 'inte-cv-name')) !!}
											{!! Form::hidden('inte-cv-system-route', null, array('id' => 'inte-cv-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-cv-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-cv-last-row-number', null, array('id' => 'inte-cv-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-cv-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('inte-cv-mp-date-format', null, array('id' => 'inte-cv-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-cv-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-cv-year', 'A' , array('id' => 'inte-cv-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-cv-crime', Lang::get('decima-oadh::back-end-column.crime'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-cv-crime', 'B' , array('id' => 'inte-cv-crime', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-cv-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-cv-sex', 'C' , array('id' => 'inte-cv-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-cv-age-range', Lang::get('decima-oadh::back-end-column.ageRange'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-cv-age-range', 'D' , array('id' => 'inte-cv-age-range', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="inte-cv-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='inte-cv-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-cv-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-cv-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-cv-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-cv-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-cv-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-cv-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-cv-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="inte-cv-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-cv-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
