@extends('layouts.base')

@section('container')
{!! Form::hidden('inte-aw-new-action', null, array('id' => 'inte-aw-new-action')) !!}
{!! Form::hidden('inte-aw-edit-action', null, array('id' => 'inte-aw-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('inte-aw-remove-action', null, array('id' => 'inte-aw-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'inte-aw-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'inte-aw-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_inte-aw-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeAwOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#inte-aw-grid').getSelectedRowId('file_id'), rowData = $('#inte-aw-grid').getRowData($('#inte-aw-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#inte-aw-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('inte-aw-', 'firstPage', id);

				$('#inte-aw-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'aw.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#inte-aw-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'aw.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#inte-aw-btn-group-2').enableButtonGroup();

		}

		function freeAwDetailOnSelectRowEvent()
		{
			var selRowIds = $('#inte-aw-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#inte-aw-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#inte-aw-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#inte-aw-detail-btn-group-2').disabledButtonGroup();
				$('#inte-aw-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeAwOnLoadCompleteEvent()
		{
			$('#inte-aw-temp-data').jqGrid('clearGridData');
			$('#inte-aw-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.inte-aw-btn-tooltip').tooltip();

			$('#inte-aw-mp-form').jqMgVal('addFormFieldsValidations');

			$('#inte-aw-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#inte-aw-form-section').on('hidden.bs.collapse', function ()
			{
				$('#inte-aw-grid-section').collapse('show');

				$('#inte-aw-journals-section').collapse('show');

				$('#inte-aw-filters').show();
			});

			$('#inte-aw-form-section').on('shown.bs.collapse', function ()
			{
				// $('#inte-aw-').focus();
			});

			$('#inte-aw-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#inte-aw-btn-refresh').click();
	    });

			$('#inte-aw-').focusout(function()
			{
				$('#inte-aw-btn-save').focus();
			});

			$('#inte-aw-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.inte-aw-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#inte-aw-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#inte-aw-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('inte-aw-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('inte-aw-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('inte-aw-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#inte-aw-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-aw-btn-toolbar').disabledButtonGroup();
				$('#inte-aw-btn-group-1').enableButtonGroup();

				if($('#inte-aw-journals-section').attr('data-target-id') == '' || $('#inte-aw-journals-section').attr('data-target-id') == '#inte-aw-form-section')
				{
					$('#inte-aw-grid').trigger('reloadGrid');
					cleanJournals('inte-aw-');
				}
				else
				{

				}
			});

			$('#inte-aw-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#inte-aw-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#inte-aw-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#inte-aw-detail-btn-export-xls').click(function()
			{
				$('#inte-aw-back-detail-gridXlsButton').click();
			});

			$('#inte-aw-detail-btn-export-csv').click(function()
			{
				$('#inte-aw-back-detail-gridCsvButton').click();
			});

			$('#inte-aw-btn-export-xls').click(function()
			{
				if($('#inte-aw-journals-section').attr('data-target-id') == '')
				{
					$('#inte-aw-gridXlsButton').click();
				}
			});

			$('#inte-aw-btn-export-csv').click(function()
			{
				if($('#inte-aw-journals-section').attr('data-target-id') == '')
				{
					$('#inte-aw-gridCsvButton').click();
				}
			});

			$('#inte-aw-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-aw-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-aw-grid').isRowSelected())
					{
						$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-aw-modal-delete-file').modal('show');
			});

			$('#inte-aw-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-aw-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-aw-btn-toolbar', false);
						$('#inte-aw-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-aw-modal-delete-prod').click();
							$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-aw-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#inte-aw-grid').trigger('reloadGrid');
						$('#inte-aw-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#inte-aw-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-aw-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-aw-grid').isRowSelected())
					{
						$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-aw-modal-delete-prod').modal('show');
			});

			$('#inte-aw-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-aw-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-aw-btn-toolbar', false);
						$('#inte-aw-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-aw-modal-delete-prod').click();
							$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#inte-aw-prod-data').trigger('reloadGrid');
							$('#inte-aw-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-aw-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-aw-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-aw-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-aw-grid').isRowSelected())
					{
						$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-aw-modal-generate').modal('show');
			});

			$('#inte-aw-btn-modal-generate').click(function()
			{
				var rowData = $('#inte-aw-grid').getRowData($('#inte-aw-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-aw-btn-toolbar', false);
						$('#inte-aw-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-aw-btn-refresh').click();
							$("#inte-aw-btn-group-2").disabledButtonGroup();
							$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-aw-btn-refresh').click();
							$("#inte-aw-btn-group-2").disabledButtonGroup();
							$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-aw-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-aw-btn-process').click(function()
			{

				if(!$('#inte-aw-grid').isRowSelected())
				{
					$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#inte-aw-grid').getRowData($('#inte-aw-grid').jqGrid('getGridParam', 'selrow'));

				$('#inte-aw-mp-form').jqMgVal('clearContextualClasses');
				$('#inte-aw-id').val(rowData.file_id);
				$('#inte-aw-name').val(rowData.file_name);
				$('#inte-aw-system-route').val(rowData.file_system_route);

				$('#inte-aw-mp-modal').modal('show');
			});

			$('#inte-aw-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#inte-aw-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#inte-aw-mp-form').formToObject('inte-aw-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-aw-btn-toolbar', false);
						$('#inte-aw-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#inte-aw-btn-refresh').click();
							$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#inte-aw-temp-data, #inte-aw-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'aw.file_id','op':'eq','data':'" + $('#inte-aw-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#inte-aw-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#inte-aw-edit-action').isEmpty())
			{
				showButtonHelper('inte-aw-btn-close', 'inte-aw-btn-group-2', $('#inte-aw-edit-action').attr('data-content'));
			}

			$('#inte-aw-btn-delete-helper').click(function()
		  {
				showButtonHelper('inte-aw-btn-close', 'inte-aw-btn-group-2', $('#inte-aw-delete-action').attr('data-content'));
		  });

			if(!$('#inte-aw-delete-action').isEmpty())
			{
				showButtonHelper('inte-aw-btn-close', 'inte-aw-btn-group-2', $('#inte-aw-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="inte-aw-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="inte-aw-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'inte-aw-btn-upload', 'class' => 'btn btn-default inte-aw-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'inte-aw-btn-refresh', 'class' => 'btn btn-default inte-aw-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="inte-aw-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'inte-aw-btn-process', 'class' => 'btn btn-default inte-aw-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'inte-aw-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'inte-aw-btn-delete-prod', 'class' => 'btn btn-default inte-aw-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'inte-aw-btn-delete-file', 'class' => 'btn btn-default inte-aw-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='inte-aw-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='inte-aw-grid'>
				{!!
				GridRender::setGridId("inte-aw-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeAwOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeAwOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#inte-aw-temp-data-tab" aria-controls="inte-aw-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#inte-aw-processed-data-tab" aria-controls="inte-aw-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="inte-aw-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-aw-front-temp-data'>
      				{!!
      				GridRender::setGridId('inte-aw-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'aw.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'aw.id', 'name' => 'oadh_aw_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'aw.file_id', 'name' => 'oadh_aw_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'aw.year', 'name' => 'oadh_aw_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.ageRange'), 'index' => 'aw.age_range', 'name' => 'oadh_aw_age_range', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'aw.status', 'name' => 'oadh_aw_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '40'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="inte-aw-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-aw-front-processed-grid'>
              {!!
              GridRender::setGridId('inte-aw-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'aw.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'aw.id', 'name' => 'oadh_aw_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'aw.file_id', 'name' => 'oadh_aw_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'aw.year', 'name' => 'oadh_aw_year', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.ageRange'), 'index' => 'aw.age_range', 'name' => 'oadh_aw_age_range', 'align' => 'center', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='inte-aw-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='inte-aw-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'inte-aw-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-aw-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-aw-header-rows-number', 1 , array('id' => 'inte-aw-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('inte-aw-id', null, array('id' => 'inte-aw-id')) !!}
											{!! Form::hidden('inte-aw-name', null, array('id' => 'inte-aw-name')) !!}
											{!! Form::hidden('inte-aw-system-route', null, array('id' => 'inte-aw-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-aw-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-aw-last-row-number', null, array('id' => 'inte-aw-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-aw-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('inte-aw-mp-date-format', null, array('id' => 'inte-aw-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-aw-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-aw-year', 'A' , array('id' => 'inte-aw-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-aw-age-range', Lang::get('decima-oadh::back-end-column.ageRange'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-aw-age-range', 'B' , array('id' => 'inte-aw-age-range', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>


							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="inte-aw-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='inte-aw-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-aw-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-aw-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-aw-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-aw-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-aw-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-aw-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-aw-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="inte-aw-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-aw-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
