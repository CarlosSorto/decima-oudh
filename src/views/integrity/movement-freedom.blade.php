@extends('layouts.base')

@section('container')
{!! Form::hidden('inte-mf-new-action', null, array('id' => 'inte-mf-new-action')) !!}
{!! Form::hidden('inte-mf-edit-action', null, array('id' => 'inte-mf-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('inte-mf-remove-action', null, array('id' => 'inte-mf-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'inte-mf-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'inte-mf-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_inte-mf-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeMfOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#inte-mf-grid').getSelectedRowId('file_id'), rowData = $('#inte-mf-grid').getRowData($('#inte-mf-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#inte-mf-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('inte-mf-', 'firstPage', id);

				$('#inte-mf-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'mf.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#inte-mf-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'mf.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#inte-mf-btn-group-2').enableButtonGroup();

		}

		function freeMfDetailOnSelectRowEvent()
		{
			var selRowIds = $('#inte-mf-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#inte-mf-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#inte-mf-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#inte-mf-detail-btn-group-2').disabledButtonGroup();
				$('#inte-mf-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeMfOnLoadCompleteEvent()
		{
			$('#inte-mf-temp-data').jqGrid('clearGridData');
			$('#inte-mf-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.inte-mf-btn-tooltip').tooltip();

			$('#inte-mf-mp-form').jqMgVal('addFormFieldsValidations');

			$('#inte-mf-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#inte-mf-form-section').on('hidden.bs.collapse', function ()
			{
				$('#inte-mf-grid-section').collapse('show');

				$('#inte-mf-journals-section').collapse('show');

				$('#inte-mf-filters').show();
			});

			$('#inte-mf-form-section').on('shown.bs.collapse', function ()
			{
				// $('#inte-mf-').focus();
			});

			$('#inte-mf-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#inte-mf-btn-refresh').click();
	    });

			$('#inte-mf-').focusout(function()
			{
				$('#inte-mf-btn-save').focus();
			});

			$('#inte-mf-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.inte-mf-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#inte-mf-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#inte-mf-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('inte-mf-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('inte-mf-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('inte-mf-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#inte-mf-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-mf-btn-toolbar').disabledButtonGroup();
				$('#inte-mf-btn-group-1').enableButtonGroup();

				if($('#inte-mf-journals-section').attr('data-target-id') == '' || $('#inte-mf-journals-section').attr('data-target-id') == '#inte-mf-form-section')
				{
					$('#inte-mf-grid').trigger('reloadGrid');
					cleanJournals('inte-mf-');
				}
				else
				{

				}
			});

			$('#inte-mf-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#inte-mf-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#inte-mf-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#inte-mf-detail-btn-export-xls').click(function()
			{
				$('#inte-mf-back-detail-gridXlsButton').click();
			});

			$('#inte-mf-detail-btn-export-csv').click(function()
			{
				$('#inte-mf-back-detail-gridCsvButton').click();
			});

			$('#inte-mf-btn-export-xls').click(function()
			{
				if($('#inte-mf-journals-section').attr('data-target-id') == '')
				{
					$('#inte-mf-gridXlsButton').click();
				}
			});

			$('#inte-mf-btn-export-csv').click(function()
			{
				if($('#inte-mf-journals-section').attr('data-target-id') == '')
				{
					$('#inte-mf-gridCsvButton').click();
				}
			});

			$('#inte-mf-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-mf-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-mf-grid').isRowSelected())
					{
						$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-mf-modal-delete-file').modal('show');
			});

			$('#inte-mf-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-mf-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-mf-btn-toolbar', false);
						$('#inte-mf-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-mf-modal-delete-prod').click();
							$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-mf-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#inte-mf-grid').trigger('reloadGrid');
						$('#inte-mf-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#inte-mf-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-mf-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-mf-grid').isRowSelected())
					{
						$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-mf-modal-delete-prod').modal('show');
			});

			$('#inte-mf-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-mf-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-mf-btn-toolbar', false);
						$('#inte-mf-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-mf-modal-delete-prod').click();
							$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#inte-mf-prod-data').trigger('reloadGrid');
							$('#inte-mf-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-mf-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-mf-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-mf-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-mf-grid').isRowSelected())
					{
						$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-mf-modal-generate').modal('show');
			});

			$('#inte-mf-btn-modal-generate').click(function()
			{
				var rowData = $('#inte-mf-grid').getRowData($('#inte-mf-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-mf-btn-toolbar', false);
						$('#inte-mf-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-mf-btn-refresh').click();
							$("#inte-mf-btn-group-2").disabledButtonGroup();
							$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-mf-btn-refresh').click();
							$("#inte-mf-btn-group-2").disabledButtonGroup();
							$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-mf-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-mf-btn-process').click(function()
			{

				if(!$('#inte-mf-grid').isRowSelected())
				{
					$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#inte-mf-grid').getRowData($('#inte-mf-grid').jqGrid('getGridParam', 'selrow'));

				$('#inte-mf-mp-form').jqMgVal('clearContextualClasses');
				$('#inte-mf-id').val(rowData.file_id);
				$('#inte-mf-name').val(rowData.file_name);
				$('#inte-mf-system-route').val(rowData.file_system_route);

				$('#inte-mf-mp-modal').modal('show');
			});

			$('#inte-mf-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#inte-mf-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#inte-mf-mp-form').formToObject('inte-mf-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-mf-btn-toolbar', false);
						$('#inte-mf-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#inte-mf-btn-refresh').click();
							$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#inte-mf-temp-data, #inte-mf-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'mf.file_id','op':'eq','data':'" + $('#inte-mf-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-mf-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#inte-mf-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#inte-mf-edit-action').isEmpty())
			{
				showButtonHelper('inte-mf-btn-close', 'inte-mf-btn-group-2', $('#inte-mf-edit-action').attr('data-content'));
			}

			$('#inte-mf-btn-delete-helper').click(function()
		  {
				showButtonHelper('inte-mf-btn-close', 'inte-mf-btn-group-2', $('#inte-mf-delete-action').attr('data-content'));
		  });

			if(!$('#inte-mf-delete-action').isEmpty())
			{
				showButtonHelper('inte-mf-btn-close', 'inte-mf-btn-group-2', $('#inte-mf-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="inte-mf-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="inte-mf-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'inte-mf-btn-upload', 'class' => 'btn btn-default inte-mf-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'inte-mf-btn-refresh', 'class' => 'btn btn-default inte-mf-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="inte-mf-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'inte-mf-btn-process', 'class' => 'btn btn-default inte-mf-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'inte-mf-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'inte-mf-btn-delete-prod', 'class' => 'btn btn-default inte-mf-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'inte-mf-btn-delete-file', 'class' => 'btn btn-default inte-mf-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='inte-mf-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='inte-mf-grid'>
				{!!
				GridRender::setGridId("inte-mf-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeMfOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeMfOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#inte-mf-temp-data-tab" aria-controls="inte-mf-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#inte-mf-processed-data-tab" aria-controls="inte-mf-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="inte-mf-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-mf-front-temp-data'>
      				{!!
      				GridRender::setGridId('inte-mf-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'mf.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'mf.id', 'name' => 'oadh_mf_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'mf.file_id', 'name' => 'oadh_mf_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'mf.year', 'name' => 'oadh_mf_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'mf.crime', 'name' => 'oadh_mf_crime', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'mf.department', 'name' => 'oadh_mf_department', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'mf.municipality', 'name' => 'oadh_mf_municipality', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.ageRange'), 'index' => 'mf.age_range', 'name' => 'oadh_mf_age_range', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'mf.sex', 'name' => 'oadh_mf_sex', 'align' => 'center'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'mf.status', 'name' => 'oadh_mf_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '40'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="inte-mf-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-mf-front-processed-grid'>
              {!!
              GridRender::setGridId('inte-mf-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'mf.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'mf.id', 'name' => 'oadh_mf_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'mf.file_id', 'name' => 'oadh_mf_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'mf.year', 'name' => 'oadh_mf_year', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'mf.crime', 'name' => 'oadh_mf_crime', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'mf.department', 'name' => 'oadh_mf_department', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'mf.municipality', 'name' => 'oadh_mf_municipality', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.ageRange'), 'index' => 'mf.age_range', 'name' => 'oadh_mf_age_range', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'mf.sex', 'name' => 'oadh_mf_sex', 'align' => 'center'))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='inte-mf-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='inte-mf-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'inte-mf-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-mf-header-rows-number', 1 , array('id' => 'inte-mf-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('inte-mf-id', null, array('id' => 'inte-mf-id')) !!}
											{!! Form::hidden('inte-mf-name', null, array('id' => 'inte-mf-name')) !!}
											{!! Form::hidden('inte-mf-system-route', null, array('id' => 'inte-mf-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-mf-last-row-number', null, array('id' => 'inte-mf-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('inte-mf-mp-date-format', null, array('id' => 'inte-mf-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-mf-year', 'A' , array('id' => 'inte-mf-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-crime', Lang::get('decima-oadh::back-end-column.crime'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-mf-crime', 'B' , array('id' => 'inte-mf-crime', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-mf-department', 'C' , array('id' => 'inte-mf-department', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-mf-municipality', 'D' , array('id' => 'inte-mf-municipality', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-age-range', Lang::get('decima-oadh::back-end-column.ageRange'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-mf-age-range', 'E' , array('id' => 'inte-mf-age-range', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-mf-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-mf-sex', 'F' , array('id' => 'inte-mf-sex', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="inte-mf-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='inte-mf-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-mf-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-mf-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-mf-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-mf-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-mf-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-mf-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-mf-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="inte-mf-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-mf-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
