@extends('layouts.base')

@section('container')
{!! Form::hidden('inte-ci-new-action', null, array('id' => 'inte-ci-new-action')) !!}
{!! Form::hidden('inte-ci-edit-action', null, array('id' => 'inte-ci-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('inte-ci-remove-action', null, array('id' => 'inte-ci-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'inte-ci-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'inte-ci-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_inte-ci-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeCiOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#inte-ci-grid').getSelectedRowId('file_id'), rowData = $('#inte-ci-grid').getRowData($('#inte-ci-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#inte-ci-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('inte-ci-', 'firstPage', id);

				$('#inte-ci-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'ci.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#inte-ci-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'ci.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#inte-ci-btn-group-2').enableButtonGroup();

		}

		function freeCiDetailOnSelectRowEvent()
		{
			var selRowIds = $('#inte-ci-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#inte-ci-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#inte-ci-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#inte-ci-detail-btn-group-2').disabledButtonGroup();
				$('#inte-ci-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeCiOnLoadCompleteEvent()
		{
			$('#inte-ci-temp-data').jqGrid('clearGridData');
			$('#inte-ci-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.inte-ci-btn-tooltip').tooltip();

			$('#inte-ci-mp-form').jqMgVal('addFormFieldsValidations');

			$('#inte-ci-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#inte-ci-form-section').on('hidden.bs.collapse', function ()
			{
				$('#inte-ci-grid-section').collapse('show');

				$('#inte-ci-journals-section').collapse('show');

				$('#inte-ci-filters').show();
			});

			$('#inte-ci-form-section').on('shown.bs.collapse', function ()
			{
				// $('#inte-ci-').focus();
			});

			$('#inte-ci-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#inte-ci-btn-refresh').click();
	    });

			$('#inte-ci-').focusout(function()
			{
				$('#inte-ci-btn-save').focus();
			});

			$('#inte-ci-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.inte-ci-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Cino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#inte-ci-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#inte-ci-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('inte-ci-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('inte-ci-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('inte-ci-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#inte-ci-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-ci-btn-toolbar').disabledButtonGroup();
				$('#inte-ci-btn-group-1').enableButtonGroup();

				if($('#inte-ci-journals-section').attr('data-target-id') == '' || $('#inte-ci-journals-section').attr('data-target-id') == '#inte-ci-form-section')
				{
					$('#inte-ci-grid').trigger('reloadGrid');
					cleanJournals('inte-ci-');
				}
				else
				{

				}
			});

			$('#inte-ci-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#inte-ci-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#inte-ci-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#inte-ci-detail-btn-export-xls').click(function()
			{
				$('#inte-ci-back-detail-gridXlsButton').click();
			});

			$('#inte-ci-detail-btn-export-csv').click(function()
			{
				$('#inte-ci-back-detail-gridCsvButton').click();
			});

			$('#inte-ci-btn-export-xls').click(function()
			{
				if($('#inte-ci-journals-section').attr('data-target-id') == '')
				{
					$('#inte-ci-gridXlsButton').click();
				}
			});

			$('#inte-ci-btn-export-csv').click(function()
			{
				if($('#inte-ci-journals-section').attr('data-target-id') == '')
				{
					$('#inte-ci-gridCsvButton').click();
				}
			});

			$('#inte-ci-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-ci-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-ci-grid').isRowSelected())
					{
						$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-ci-modal-delete-file').modal('show');
			});

			$('#inte-ci-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-ci-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-ci-btn-toolbar', false);
						$('#inte-ci-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-ci-modal-delete-prod').click();
							$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-ci-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#inte-ci-grid').trigger('reloadGrid');
						$('#inte-ci-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#inte-ci-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-ci-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-ci-grid').isRowSelected())
					{
						$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-ci-modal-delete-prod').modal('show');
			});

			$('#inte-ci-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#inte-ci-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-ci-btn-toolbar', false);
						$('#inte-ci-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-ci-modal-delete-prod').click();
							$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#inte-ci-prod-data').trigger('reloadGrid');
							$('#inte-ci-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-ci-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-ci-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#inte-ci-journals-section').attr('data-target-id') == '')
				{
					if(!$('#inte-ci-grid').isRowSelected())
					{
						$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#inte-ci-modal-generate').modal('show');
			});

			$('#inte-ci-btn-modal-generate').click(function()
			{
				var rowData = $('#inte-ci-grid').getRowData($('#inte-ci-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-ci-btn-toolbar', false);
						$('#inte-ci-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#inte-ci-btn-refresh').click();
							$("#inte-ci-btn-group-2").disabledButtonGroup();
							$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#inte-ci-btn-refresh').click();
							$("#inte-ci-btn-group-2").disabledButtonGroup();
							$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#inte-ci-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#inte-ci-btn-process').click(function()
			{

				if(!$('#inte-ci-grid').isRowSelected())
				{
					$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#inte-ci-grid').getRowData($('#inte-ci-grid').jqGrid('getGridParam', 'selrow'));

				$('#inte-ci-mp-form').jqMgVal('clearContextualClasses');
				$('#inte-ci-id').val(rowData.file_id);
				$('#inte-ci-name').val(rowData.file_name);
				$('#inte-ci-system-route').val(rowData.file_system_route);

				$('#inte-ci-mp-modal').modal('show');
			});

			$('#inte-ci-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#inte-ci-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#inte-ci-mp-form').formToObject('inte-ci-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/integrity/integrity-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'inte-ci-btn-toolbar', false);
						$('#inte-ci-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#inte-ci-btn-refresh').click();
							$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#inte-ci-temp-data, #inte-ci-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'ci.file_id','op':'eq','data':'" + $('#inte-ci-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#inte-ci-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#inte-ci-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#inte-ci-edit-action').isEmpty())
			{
				showButtonHelper('inte-ci-btn-close', 'inte-ci-btn-group-2', $('#inte-ci-edit-action').attr('data-content'));
			}

			$('#inte-ci-btn-delete-helper').click(function()
		  {
				showButtonHelper('inte-ci-btn-close', 'inte-ci-btn-group-2', $('#inte-ci-delete-action').attr('data-content'));
		  });

			if(!$('#inte-ci-delete-action').isEmpty())
			{
				showButtonHelper('inte-ci-btn-close', 'inte-ci-btn-group-2', $('#inte-ci-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="inte-ci-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="inte-ci-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'inte-ci-btn-upload', 'class' => 'btn btn-default inte-ci-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'inte-ci-btn-refresh', 'class' => 'btn btn-default inte-ci-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="inte-ci-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'inte-ci-btn-process', 'class' => 'btn btn-default inte-ci-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'inte-ci-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'inte-ci-btn-delete-prod', 'class' => 'btn btn-default inte-ci-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'inte-ci-btn-delete-file', 'class' => 'btn btn-default inte-ci-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='inte-ci-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='inte-ci-grid'>
				{!!
				GridRender::setGridId("inte-ci-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeCiOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeCiOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#inte-ci-temp-data-tab" aria-controls="inte-ci-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#inte-ci-processed-data-tab" aria-controls="inte-ci-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="inte-ci-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-ci-front-temp-data'>
      				{!!
      				GridRender::setGridId('inte-ci-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'ci.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'ci.id', 'name' => 'oadh_ci_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'ci.file_id', 'name' => 'oadh_ci_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'ci.year', 'name' => 'oadh_ci_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.weaponType'), 'index' => 'ci.weapon_type', 'name' => 'oadh_ci_weapon_type', 'align' => 'center', 'hidden' => false))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.time'), 'index' => 'ci.time', 'name' => 'oadh_ci_time', 'align' => 'center', 'hidden' => false))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.total'), 'index' => 'ci.total', 'name' => 'oadh_ci_total', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'ci.status', 'name' => 'oadh_ci_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '40'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="inte-ci-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='inte-ci-front-processed-grid'>
              {!!
              GridRender::setGridId('inte-ci-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/integrity/integrity-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'ci.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'ci.id', 'name' => 'oadh_ci_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'ci.file_id', 'name' => 'oadh_ci_file_id', 'hidden' => true))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'ci.year', 'name' => 'oadh_ci_year', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.weaponType'), 'index' => 'ci.weapon_type', 'name' => 'oadh_ci_weapon_type', 'align' => 'center', 'hidden' => false))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.time'), 'index' => 'ci.time', 'name' => 'oadh_ci_time', 'align' => 'center', 'hidden' => false))
                //->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.total'), 'index' => 'ci.total', 'name' => 'oadh_ci_total', 'align' => 'center', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='inte-ci-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='inte-ci-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'inte-ci-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ci-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-ci-header-rows-number', 1 , array('id' => 'inte-ci-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('inte-ci-id', null, array('id' => 'inte-ci-id')) !!}
											{!! Form::hidden('inte-ci-name', null, array('id' => 'inte-ci-name')) !!}
											{!! Form::hidden('inte-ci-system-route', null, array('id' => 'inte-ci-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ci-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('inte-ci-last-row-number', null, array('id' => 'inte-ci-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ci-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('inte-ci-mp-date-format', null, array('id' => 'inte-ci-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ci-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ci-year', 'A' , array('id' => 'inte-ci-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ci-weapon-type', Lang::get('decima-oadh::back-end-column.weaponType'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ci-weapon-type', 'B' , array('id' => 'inte-ci-weapon-type', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ci-time', Lang::get('decima-oadh::back-end-column.time'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ci-time', 'C' , array('id' => 'inte-ci-time', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
                <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('inte-ci-total', Lang::get('decima-oadh::back-end-column.total'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('inte-ci-total', 'D' , array('id' => 'inte-ci-total', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->


							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="inte-ci-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='inte-ci-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-ci-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-ci-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-ci-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-ci-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-ci-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='inte-ci-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm inte-ci-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="inte-ci-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="inte-ci-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
