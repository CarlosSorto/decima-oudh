@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-hrm-new-action', null, array('id' => 'oadh-hrm-new-action')) !!}
{!! Form::hidden('oadh-hrm-edit-action', null, array('id' => 'oadh-hrm-edit-action', 'data-content' => Lang::get('decima-oadh::human-right-management.editHelpText'))) !!}
{!! Form::hidden('oadh-hrm-remove-action', null, array('id' => 'oadh-hrm-remove-action', 'data-content' => Lang::get('decima-oadh::human-right-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-hrm-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-hrm-btn-delete-helper', 'class' => 'hidden')) !!}
<style></style>

<script type='text/javascript'>
	//For grids with multiselect enabled
	// function oadhHrmOnSelectRowEvent(id)
	// {
	// 	var selRowIds = $('#oadh-hrm-grid').jqGrid('getGridParam', 'selarrrow'), id;
	//
	// 	if(selRowIds.length == 0)
	// 	{
	// 		$('#oadh-hrm-btn-group-2').disabledButtonGroup();
	// 		cleanJournals('oadh-hrm-');
	// 		// cleanFiles('oadh-hrm-')
	// 	}
	// 	else if(selRowIds.length == 1)
	// 	{
	// 		$('#oadh-hrm-btn-group-2').enableButtonGroup();
	//
	// 		id = $('#oadh-hrm-grid').getSelectedRowId('oadh_hrm_id');
	//
	// 		if($('#oadh-hrm-journals').attr('data-journalized-id') != id)
	// 		{
	// 			cleanJournals('oadh-hrm-');
	// 			// getElementFiles('oadh-hrm-', id);
	// 			getAppJournals('oadh-hrm-','firstPage', id);
	// 		}
	//
	// 	}
	// 	else if(selRowIds.length > 1)
	// 	{
	// 		$('#oadh-hrm-btn-group-2').disabledButtonGroup();
	// 		$('#oadh-hrm-btn-delete').removeAttr('disabled');
	// 		cleanJournals('oadh-hrm-');
	// 		// cleanFiles('oadh-hrm-')
	// 	}
	// }


	//For grids with multiselect disabled
	function oadhHrmOnSelectRowEvent(id)
	{
		var id = $('#oadh-hrm-grid').getSelectedRowId('oadh_hrm_id');

		if($('#oadh-hrm-journals').attr('data-journalized-id') != id)
		{
			getAppJournals('oadh-hrm-', 'firstPage', id);
			// getElementFiles('oadh-hrm-', id);
		}

		$('#oadh-hrm-btn-group-2').enableButtonGroup();
	}

	$(document).ready(function()
	{
		$('.oadh-hrm-btn-tooltip').tooltip();

		$('#oadh-hrm-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-hrm-journals-section').on('hidden.bs.collapse', function ()
		{
			$($(this).attr('data-target-id')).collapse('show');
		});

		$('#oadh-hrm-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-hrm-').focus();
		});

		$('#oadh-hrm-').focusout(function()
		{
			$('#oadh-hrm-btn-save').focus();
		});

		$('#oadh-hrm-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-hrm-grid-section').collapse('show');

			$('#oadh-hrm-journals-section').collapse('show');
		});

		$('#oadh-hrm-btn-new-tracing').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}
			$('#oadh-hrm-tracing-select').addClass('hidden');
			$('#oadh-hrm-human-right-select').addClass('hidden');

			$('#oadh-hrm-parent-div').addClass('hidden');

			$('#oadh-hrm-form-new-title-tracing').removeClass('hidden');
			$('#oadh-hrm-form-new-title-human-right, #oadh-hrm-form-new-title-topic, #oadh-hrm-form-new-title-violated-fact').addClass('hidden');

			$('#oadh-hrm-btn-toolbar').disabledButtonGroup();
			$('#oadh-hrm-btn-group-3').enableButtonGroup();
			$('#oadh-hrm-grid-section').collapse('hide');
			$('#oadh-hrm-journals-section').attr('data-target-id', '#oadh-hrm-form-section');
			$('#oadh-hrm-journals-section').collapse('hide');
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-hrm-es-description-div, #oadh-hrm-en-description-div, #oadh-hrm-position-div').hide();
		});

		$('#oadh-hrm-btn-new-human-right').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}
			$('#oadh-hrm-tracing-select').removeClass('hidden');
			$('#oadh-hrm-human-right-select').addClass('hidden');

			$('#oadh-hrm-parent-div').removeClass('hidden');

			$('#oadh-hrm-form-new-title-human-right').removeClass('hidden');
			$('#oadh-hrm-form-new-title-tracing, #oadh-hrm-form-new-title-topic, #oadh-hrm-form-new-title-violated-fact').addClass('hidden');

			$('#oadh-hrm-btn-toolbar').disabledButtonGroup();
			$('#oadh-hrm-btn-group-3').enableButtonGroup();
			$('#oadh-hrm-grid-section').collapse('hide');
			$('#oadh-hrm-journals-section').attr('data-target-id', '#oadh-hrm-form-section');
			$('#oadh-hrm-journals-section').collapse('hide');
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-hrm-es-description-div, #oadh-hrm-en-description-div, #oadh-hrm-position-div').show();
		});

		$('#oadh-hrm-btn-new-topic').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-hrm-human-right-select').removeClass('hidden');
			$('#oadh-hrm-tracing-select').addClass('hidden');

			$('#oadh-hrm-parent-div').removeClass('hidden');

			$('#oadh-hrm-form-new-title-topic').removeClass('hidden');
			$('#oadh-hrm-form-new-title-tracing, #oadh-hrm-form-new-title-human-right, #oadh-hrm-form-new-title-violated-fact').addClass('hidden');

			$('#oadh-hrm-btn-toolbar').disabledButtonGroup();
			$('#oadh-hrm-btn-group-3').enableButtonGroup();
			$('#oadh-hrm-grid-section').collapse('hide');
			$('#oadh-hrm-journals-section').attr('data-target-id', '#oadh-hrm-form-section');
			$('#oadh-hrm-journals-section').collapse('hide');
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-hrm-es-description-div, #oadh-hrm-en-description-div, #oadh-hrm-position-div').hide();

			$('#oadh-hrm-type').val('T');
		});

		$('#oadh-hrm-btn-new-violated-fact').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-hrm-human-right-select').removeClass('hidden');
			$('#oadh-hrm-tracing-select').addClass('hidden');

			$('#oadh-hrm-parent-div').removeClass('hidden');

			$('#oadh-hrm-form-new-title-violated-fact').removeClass('hidden');
			$('#oadh-hrm-form-new-title-tracing, #oadh-hrm-form-new-title-human-right, #oadh-hrm-form-new-title-topic').addClass('hidden');

			$('#oadh-hrm-btn-toolbar').disabledButtonGroup();
			$('#oadh-hrm-btn-group-3').enableButtonGroup();
			$('#oadh-hrm-grid-section').collapse('hide');
			$('#oadh-hrm-journals-section').attr('data-target-id', '#oadh-hrm-form-section');
			$('#oadh-hrm-journals-section').collapse('hide');
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-hrm-es-description-div, #oadh-hrm-en-description-div, #oadh-hrm-position-div').hide();

			$('#oadh-hrm-type').val('H');
		});

		$('#oadh-hrm-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-hrm-btn-toolbar').disabledButtonGroup();
			$('#oadh-hrm-btn-group-1').enableButtonGroup();

			if($('#oadh-hrm-journals-section').attr('data-target-id') == '' || $('#oadh-hrm-journals-section').attr('data-target-id') == '#oadh-hrm-form-section')
			{
				$('#oadh-hrm-grid').trigger('reloadGrid');
				cleanJournals('oadh-hrm-');
				// cleanFiles('oadh-hrm-')
			}
			else
			{

			}
		});

		$('#oadh-hrm-btn-export-xls').click(function()
		{
			if($('#oadh-hrm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-hrm-gridXlsButton').click();
			}
			else
			{

			}
		});

		$('#oadh-hrm-btn-export-csv').click(function()
		{
			if($('#oadh-hrm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-hrm-gridCsvButton').click();
			}
			else
			{

			}
		});

		$('#oadh-hrm-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-hrm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-hrm-grid').isRowSelected())
				{
					$('#oadh-hrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				$('#oadh-hrm-form-new-title-tracing, #oadh-hrm-form-new-title-human-right, #oadh-hrm-form-new-title-topic, #oadh-hrm-form-new-title-violated-fact').addClass('hidden');

				$('#oadh-hrm-btn-toolbar').disabledButtonGroup();
				$('#oadh-hrm-btn-group-3').enableButtonGroup();

				$('#oadh-hrm-parent-div').addClass('hidden');

				$('#oadh-hrm-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-hrm-grid').getRowData($('#oadh-hrm-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				$('#oadh-hrm-form-edit-title').removeClass('hidden');
				$('#oadh-hrm-grid-section').collapse('hide');
				$('#oadh-hrm-journals-section').attr('data-target-id', '#oadh-hrm-form-section');
				$('#oadh-hrm-journals-section').collapse('hide');
				
				if(empty(rowData.oadh_hrm_type) && !empty(rowData.oadh_hrm_parent_id))
				{
					$('#oadh-hrm-es-description-div, #oadh-hrm-en-description-div, #oadh-hrm-position-div').show();
				}
			}
			else
			{

			}
		});

		$('#oadh-hrm-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-hrm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-hrm-grid').isRowSelected())
				{
					$('#oadh-hrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-hrm-grid').getRowData($('#oadh-hrm-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-hrm-delete-message').html($('#oadh-hrm-delete-message').attr('data-default-label').replace(':name', rowData.oadh_hrm_name));
			}
			else
			{

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-hrm-modal-delete').modal('show');
		});

		$('#oadh-hrm-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-hrm-journals-section').attr('data-target-id') == '')
			{
				url = $('#oadh-hrm-form').attr('action') + '/delete';
				//For grids with multiselect enabled
				// id = $('#oadh-hrm-grid').getSelectedRowsIdCell('oadh_hrm_id');
				//For grids with multiselect disabled
				id = $('#oadh-hrm-grid').getSelectedRowId('oadh_hrm_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify({'_token':$('#app-token').val(), 'id':id}),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-hrm-btn-toolbar', false);
					$('#oadh-hrm-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-hrm-btn-refresh').click();
						$('#oadh-hrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}
					else if(json.info)
					{
						$('#oadh-hrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 12000);
					}

					$('#oadh-hrm-modal-delete').modal('hide');
					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-hrm-btn-save').click(function()
		{
			var url = $('#oadh-hrm-form').attr('action'), action = 'new', rowId;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-hrm-journals-section').attr('data-target-id') == '#oadh-hrm-form-section')
			{
				if(!$('#oadh-hrm-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-hrm-id').isEmpty())
				{
					url = url + '/create';
				}
				else
				{
					url = url + '/update';
					action = 'edit';
				}

				data = $('#oadh-hrm-form').formToObject('oadh-hrm-');
			}
			else
			{

			}

			console.log(data);
			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-hrm-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-hrm-journals-section').attr('data-target-id') == '#oadh-hrm-form-section')
						{
							if(action == 'edit')
							{
								rowId = $('#oadh-hrm-grid').getSelectedRowId('oadh_hrm_id');
								$('#oadh-hrm-grid').setCell(rowId, 'oadh_hrm_name', $('#oadh-hrm-name').val());
								$('#oadh-hrm-grid').setCell(rowId, 'oadh_hrm_es_name', $('#oadh-hrm-es-name').val());
								$('#oadh-hrm-grid').setCell(rowId, 'oadh_hrm_en_name', $('#oadh-hrm-en-name').val());
								$('#oadh-hrm-grid').setCell(rowId, 'oadh_hrm_es_description', $('#oadh-hrm-es-description').val());
								$('#oadh-hrm-grid').setCell(rowId, 'oadh_hrm_en_description', $('#oadh-hrm-en-description').val());
								$('#oadh-hrm-grid').setCell(rowId, 'oadh_hrm_position', $('#oadh-hrm-position').val());

								if($('#oadh-hrm-is-web-visible').is(":checked"))
								{
									$('#oadh-hrm-grid').setCell(rowId, 'oadh_hrm_is_web_visible', 1);
								}
								else
								{
									$('#oadh-hrm-grid').setCell(rowId, 'oadh_hrm_is_web_visible', 0);
								}

								//oadh_hrm_parent_id si ha cambiado refrescar, pendiente
							}
							else
							{
								$('#oadh-hrm-btn-refresh').click();
							}

							$('#oadh-hrm-btn-close').click();
							$('#oadh-hrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);

							$('#oadh-human-right-tracing-type-label').autocomplete('option', 'source', json.humanRights);

						}
						else
						{
							// $('#oadh-hrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-hrm-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-hrm-journals-section').attr('data-target-id') == '#oadh-hrm-form-section')
						{
							$('#oadh-hrm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-hrm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-hrm-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			// oadh-hrm-form-section
			if($('#oadh-hrm-journals-section').attr('data-target-id') == '#oadh-hrm-form-section')
			{
				$('#oadh-hrm-form-new-title').addClass('hidden');
				$('#oadh-hrm-form-edit-title').addClass('hidden');
				// $('#oadh-hrm-btn-refresh').click();
				$('#oadh-hrm-form').jqMgVal('clearForm');
				$('#oadh-hrm-form-section').collapse('hide');
			}
			else
			{

			}

			$('#oadh-hrm-btn-group-1').enableButtonGroup();
			$('#oadh-hrm-btn-group-3').disabledButtonGroup();
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-hrm-journals-section').attr('data-target-id', '')

			$('#oadh-hrm-es-description-div, #oadh-hrm-en-description-div, #oadh-hrm-position-div').hide();
		});

		$('#oadh-hrm-btn-edit-helper').click(function()
	  {
			showButtonHelper('oadh-hrm-btn-close', 'oadh-hrm-btn-group-2', $('#oadh-hrm-edit-action').attr('data-content'));
	  });

		$('#oadh-hrm-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-hrm-btn-close', 'oadh-hrm-btn-group-2', $('#oadh-hrm-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-hrm-new-action').isEmpty())
		{
			$('#oadh-hrm-btn-new').click();
		}

		if(!$('#oadh-hrm-edit-action').isEmpty())
		{
			showButtonHelper('oadh-hrm-btn-close', 'oadh-hrm-btn-group-2', $('#oadh-hrm-edit-action').attr('data-content'));
		}

		if(!$('#oadh-hrm-delete-action').isEmpty())
		{
			showButtonHelper('oadh-hrm-btn-close', 'oadh-hrm-btn-group-2', $('#oadh-hrm-delete-action').attr('data-content'));
		}
	});
</script>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-hrm-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-hrm-btn-group-1" class="btn-group btn-group-app-toolbar">
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new') . ' <span class="caret"></span>', array('id' => 'purch-pom-btn-new-all', 'class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
						<li><a id='oadh-hrm-btn-new-tracing' class="btn fake-link"><i class="fa fa-plus"></i> {{Lang::get('decima-oadh::human-right-management.formNewTitleTracing')}}</a></li>
						<li><a id='oadh-hrm-btn-new-human-right' class="btn fake-link"><i class="fa fa-plus"></i> {{Lang::get('decima-oadh::human-right-management.formNewTitleHumanRight')}}</a></li>
						<li><a id='oadh-hrm-btn-new-topic' class="btn fake-link"><i class="fa fa-plus"></i> {{Lang::get('decima-oadh::human-right-management.formNewTitleTopic')}}</a></li>
						<li><a id='oadh-hrm-btn-new-violated-fact' class="btn fake-link"><i class="fa fa-plus"></i> {{Lang::get('decima-oadh::human-right-management.formNewTitleViolatedFact')}}</a></li>
					</ul>
				</div>
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-hrm-btn-refresh', 'class' => 'btn btn-default oadh-hrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
         		<li><a id='oadh-hrm-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> {{ Lang::get('decima-accounting::journal-management.standardSpreadsheet') . ' (' . Lang::get('form.spreadsheet') . ')' }}</a></li>
         		<!-- <li><a id='oadh-hrm-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li> -->
       		</ul>
				</div>
			</div>
			<div id="oadh-hrm-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-hrm-btn-edit', 'class' => 'btn btn-default oadh-hrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::human-right-management.edit'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-hrm-btn-delete', 'class' => 'btn btn-default oadh-hrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::human-right-management.delete'))) !!}
			</div>
			<div id="oadh-hrm-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-hrm-btn-save', 'class' => 'btn btn-default oadh-hrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::human-right-management.save'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-hrm-btn-close', 'class' => 'btn btn-default oadh-hrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-hrm-grid-section' class='app-grid collapse in' data-app-grid-id='oadh-hrm-grid'>
			{!!
			GridRender::setGridId("oadh-hrm-grid")
				->enablefilterToolbar(false, false)
				->hideXlsExporter()
  			->hideCsvExporter()
	    	->setGridOption('url',URL::to('/ucaoadh/setup/human-right-management/grid-data'))
	    	->setGridOption('caption', Lang::get('decima-oadh::human-right-management.gridTitle'))
				->setGridOption('filename', Lang::get('decima-oadh::human-right-management.gridTitle'))
	    	->setGridOption('postData',array('_token' => Session::token()))
				->setGridEvent('onSelectRow', 'oadhHrmOnSelectRowEvent')
				->setGridOption('multiselect', false)
				->setGridOption('treeGrid', true)
				->setGridOption('treeIcons', array('leaf' => 'fas fa-leaf'))
				->setGridOption('ExpandColumn', 'oadh_hrm_name')
				->setGridOption('treeReader', array('parent_id_field' => 'oadh_hrm_parent_id', 'leaf_field' => 'oadh_hrm_is_leaf'))
				->addColumn(array('index' => 'oadh_hrm_is_leaf', 'name' => 'oadh_hrm_is_leaf', 'hidden' => true))
				->addColumn(array('index' => 'dm.parent_id', 'name' => 'oadh_hrm_parent_id', 'hidden' => true))
	    	->addColumn(array('index' => 'id', 'name' => 'oadh_hrm_id', 'hidden' => true, 'key' => true))
				->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.esDescription'), 'index' => 'hr.es_description' ,'name' => 'oadh_hrm_es_description', 'hidden' => true))
	    	->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.enDescription'), 'index' => 'hr.en_description' ,'name' => 'oadh_hrm_en_description', 'hidden' => true))
	    	->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.name'), 'index' => 'hr.name' ,'name' => 'oadh_hrm_name'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.esName'), 'index' => 'hr.es_name' ,'name' => 'oadh_hrm_es_name'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.enName'), 'index' => 'hr.en_name' ,'name' => 'oadh_hrm_en_name'))
	    	->addColumn(array('label' => Lang::get('form.type'), 'index' => 'h.type' ,'name' => 'oadh_hrm_type', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::human-right-management.typeGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select'))
				->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.position'), 'index' => 'p.position' ,'name' => 'oadh_hrm_position', 'align' => 'center', 'width' => 70))
				->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.isWebVisible'), 'index' => 'p.is_web_visible' ,'name' => 'oadh_hrm_is_web_visible', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.booleanText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70))
	    	->renderGrid();
			!!}
		</div>
	</div>
</div>
<div id='oadh-hrm-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-hrm-form', 'url' => URL::to('/ucaoadh/setup/human-right-management'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-hrm-form-new-title-tracing" class="hidden">{{ Lang::get('decima-oadh::human-right-management.formNewTitleTracing') }}</legend>
				<legend id="oadh-hrm-form-new-title-human-right" class="hidden">{{ Lang::get('decima-oadh::human-right-management.formNewTitleHumanRight') }}</legend>
				<legend id="oadh-hrm-form-new-title-topic" class="hidden">{{ Lang::get('decima-oadh::human-right-management.formNewTitleTopic') }}</legend>
				<legend id="oadh-hrm-form-new-title-violated-fact" class="hidden">{{ Lang::get('decima-oadh::human-right-management.formNewTitleViolatedFact') }}</legend>

				<legend id="oadh-hrm-form-edit-title-tracing" class="hidden">{{ Lang::get('decima-oadh::human-right-management.formEditTitleTracing') }}</legend>
				<legend id="oadh-hrm-form-edit-title-human-right" class="hidden">{{ Lang::get('decima-oadh::human-right-management.formEditTitleHumanRight') }}</legend>
				<legend id="oadh-hrm-form-edit-title-topic" class="hidden">{{ Lang::get('decima-oadh::human-right-management.formEditTitleTopic') }}</legend>
				<legend id="oadh-hrm-form-edit-title-violated-fact" class="hidden">{{ Lang::get('decima-oadh::human-right-management.formEditTitleViolatedFact') }}</legend>
				<div class="row">
					<div class="col-md-12" id="oadh-hrm-parent-div">
						<div class="form-group mg-hm"id="oadh-hrm-tracing-select">
							{!! Form::label('oadh-hrm-parent-label', Lang::get('decima-oadh::human-right-management.tracing'), array('class' => 'control-label')) !!}
							{!! Form::autocomplete('oadh-hrm-tracing-label', $tracings, array('class' => 'form-control'), 'oadh-hrm-tracing-label', 'oadh-hrm-tracing-id', null, 'fa-files-o') !!}
							{!! Form::hidden('oadh-hrm-tracing-id', null, array('id'  =>  'oadh-hrm-tracing-id')) !!}
			  		</div>
						<div class="form-group mg-hm"id="oadh-hrm-human-right-select">
							{!! Form::label('oadh-hrm-parent-label', Lang::get('decima-oadh::human-right-management.humanRight'), array('class' => 'control-label')) !!}
							{!! Form::autocomplete('oadh-hrm-right-label', $humanRights, array('class' => 'form-control'), 'oadh-hrm-right-label', 'oadh-hrm-right-id', null, 'fa-files-o') !!}
							{!! Form::hidden('oadh-hrm-right-id', null, array('id'  =>  'oadh-hrm-right-id')) !!}
			  		</div>
					</div>
					<div class="col-md-12">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-hrm-name', Lang::get('decima-oadh::human-right-management.name'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-hrm-name', null , array('id' => 'oadh-hrm-name', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							{!! Form::hidden('oadh-hrm-id', null, array('id' => 'oadh-hrm-id')) !!}
							{!! Form::hidden('oadh-hrm-type', null, array('id' => 'oadh-hrm-type')) !!}
			  		</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-hrm-es-name', Lang::get('decima-oadh::human-right-management.esName'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-hrm-es-name', null , array('id' => 'oadh-hrm-es-name', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-hrm-en-name', Lang::get('decima-oadh::human-right-management.enName'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-hrm-en-name', null , array('id' => 'oadh-hrm-en-name', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
					<div class="col-md-12">
						<div id="oadh-hrm-es-description-div" class="form-group mg-hm">
							{!! Form::label('oadh-hrm-es-description', Lang::get('decima-oadh::human-right-management.esDescription'), array('class' => 'control-label')) !!}
					    {{-- {!! Form::text('oadh-hrm-es-description', null , array('id' => 'oadh-hrm-es-description', 'class' => 'form-control')) !!} --}}
							{!! Form::textareacustom('oadh-hrm-es-description', 2, 10000, array('id' => 'oadh-hrm-es-description', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
					<div class="col-md-12">
						<div id="oadh-hrm-en-description-div" class="form-group mg-hm">
							{!! Form::label('oadh-hrm-en-description', Lang::get('decima-oadh::human-right-management.enDescription'), array('class' => 'control-label')) !!}
					    {{-- {!! Form::text('oadh-hrm-en-description', null , array('id' => 'oadh-hrm-en-description', 'class' => 'form-control')) !!} --}}
							{!! Form::textareacustom('oadh-hrm-en-description', 2, 10000, array('id' => 'oadh-hrm-en-description', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
					<div class="col-md-2">
						<div id="oadh-hrm-position-div" class="form-group mg-hm">
							{!! Form::label('oadh-hrm-position', Lang::get('decima-oadh::human-right-management.position'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-hrm-position', null , array('id' => 'oadh-hrm-position', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger')) !!}
			  		</div>
					</div>
					<div class="col-md-6">
						<div class="form-group checkbox">
							<label class="control-label" style="margin-top: 20px;">
								{!! Form::checkbox('oadh-hrm-is-web-visible', 'S',false, array('id' => 'oadh-hrm-is-web-visible')) !!}
								{{ Lang::get('decima-oadh::human-right-management.isWebVisible') }}
							</label>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div id='oadh-hrm-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals('oadh-hrm-', $appInfo['id']) !!}
</div>
<div id='oadh-hrm-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-hrm-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-hrm-delete-message">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
				<!-- <p id="oadh-hrm-delete-message" data-default-label="{{ Lang::get('decima-oadh::human-right-management.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-hrm-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
