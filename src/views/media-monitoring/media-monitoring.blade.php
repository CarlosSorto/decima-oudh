@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-new-action', null, array('id' => 'oadh-new-action')) !!}
{!! Form::hidden('oadh-edit-action', null, array('id' => 'oadh-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-remove-action', null, array('id' => 'oadh-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-btn-void-helper', 'class' => 'hidden')) !!}

@include('decima-oadh::media-monitoring/media-monitoring-master-js')
@include('decima-oadh::media-monitoring/media-monitoring-human-right-js')
@include('decima-oadh::media-monitoring/media-monitoring-context-js')
@include('decima-oadh::media-monitoring/media-monitoring-source-js')
@include('decima-oadh::media-monitoring/media-monitoring-victim-js')
@include('decima-oadh::media-monitoring/media-monitoring-victimizer-js')
@include('decima-oadh::media-monitoring/media-monitoring-producedure-js')
@include('decima-oadh::media-monitoring/media-monitoring-news-news-js')
<style>
	#gbox_oadh-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}

	#oadh-human-right-justification-label, #oadh-summary-label-container{
		float:right;
	}
</style>
<div class="row">
	<div class="col-lg-12 col-md-12">
		{!! Form::open(array('id' => 'oadh-filters-form', 'url' => URL::to('/'), 'role' => 'form', 'onsubmit' => 'return false;', 'class' => 'form-horizontal')) !!}
			<div id="oadh-filters" class="panel panel-default">
				<div class="panel-heading custom-panel-heading clearfix">
					<h3 class="panel-title custom-panel-title pull-left">
						{{ Lang::get('form.filtersTitle') }}
					</h3>
					{!! Form::button('<i class="fa fa-filter"></i> ' . Lang::get('form.filterButton'), array('id' => 'oadh-btn-filter', 'class' => 'btn btn-default btn-sm pull-right btn-filter-left-margin')) !!}
					{!! Form::button('<i class="fa fa-eraser"></i> ' . Lang::get('form.clearFilterButton'), array('id' => 'oadh-btn-clear-filter', 'class' => 'btn btn-default btn-sm pull-right')) !!}
				</div>
				<div id="oadh-filters-body" class="panel-body">
					<ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
						<li role="presentation" class="active"><a href="#general-filters" aria-controls="general-filters" role="tab" data-toggle="tab">{{ Lang::get('Generales') }}</a></li>
						<li role="presentation" ><a href="#humans-rights-filters" aria-controls="humans-rights-filters" role="tab" data-toggle="tab">{{ Lang::get('Derechos humanos') }}</a></li>
						<li role="presentation"><a href="#info-source-filters" aria-controls="info-source-filters" role="tab" data-toggle="tab">{{ Lang::get('Tratamiendo de la informacion') }}</a></li>
						<li role="presentation"><a href="#data-victim-filters" aria-controls="data-victim-filters" role="tab" data-toggle="tab">{{ Lang::get('Victimas') }}</a></li>
						<li role="presentation"><a href="#data-victimizer-filters" aria-controls="data-victimizer-filters" role="tab" data-toggle="tab">{{ Lang::get('Victimarios') }}</a></li>
						<li role="presentation"><a href="#data-facts-filters" aria-controls="data-facts-filters" role="tab" data-toggle="tab">{{ Lang::get('Hechos') }}</a></li>
					</ul>

					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="general-filters">
							<br/>
							<div class="row">
								<div class="col-lg-6 col-md-12">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-code-filter', Lang::get('Código'), array('class' => 'col-sm-4 control-label')) !!}
												<div class="col-sm-8 mg-hm">
													{!! Form::text('oadh-code-filter', null , array('id' => 'oadh-code-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-title-filter', Lang::get('Titular'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-title-filter', null , array('id' => 'oadh-title-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('oadh-date-filter', Lang::get('Fecha'), array('class' => 'col-sm-2 control-label label-filter-padding label-filter-double-line-padding-top')) !!}
										<div class="col-sm-10 mg-hm">
											{!! Form::daterange('oadh-date-from-filter', 'oadh-date-to-filter' , array('class' => 'form-control')) !!}
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('oadh-time-filter', Lang::get('Hora'), array('class' => 'col-sm-2 control-label label-filter-padding label-filter-double-line-padding-top')) !!}
										<div class="col-sm-10 mg-hm">
											<div class="input-group">
												<span class="input-group-addon">Desde</span>
												<input class="form-control" type="time" name="oadh-time-from-filter" id="oadh-time-from-filter">
												<span class="input-group-addon">Hasta</span>
												<input class="form-control" type="time" name="oadh-time-to-filter" id="oadh-time-to-filter">
											</div>
										</div>
									</div>

								</div>
								<div class="col-lg-6 col-md-12">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-digital-media-filter', Lang::get('decima-oadh::media-monitoring.digitalMediaShort'), array('class' => 'col-sm-3 control-label label-filter-padding label-filter-double-line-padding-top')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-digital-media-filter', null , array('id' => 'oadh-digital-media-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-section-filter', Lang::get('decima-oadh::media-monitoring.sectionShort'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-section-filter', null , array('id' => 'oadh-section-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-note-location-filter', Lang::get('Ubicación'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-note-location-filter', null , array('id' => 'oadh-note-location-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-journalistic-genre-filter', Lang::get('decima-oadh::media-monitoring.gender'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-journalistic-genre-filter', null , array('id' => 'oadh-journalistic-genre-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('oadh-summary-filter', Lang::get('decima-oadh::media-monitoring.summary'), array('class' => 'col-sm-2 control-label')) !!}
										<div class="col-sm-10 mg-hm">
											{!! Form::text('oadh-summary-filter', null , array('id' => 'oadh-summary-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="humans-rights-filters">
							<br/>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-department-filter', Lang::get('decima-oadh::media-monitoring.departmentShort'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::text('oadh-department-filter', null , array('id' => 'oadh-department-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-municipality-filter', Lang::get('decima-oadh::media-monitoring.municipality'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::text('oadh-municipality-filter', null , array('id' => 'oadh-municipality-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('oadh-neighborhood-filter', Lang::get('Barrio'), array('class' => 'col-sm-2 control-label')) !!}
										<div class="col-sm-10 mg-hm">
											{!! Form::text('oadh-neighborhood-filter', null , array('id' => 'oadh-neighborhood-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<legend style="font-size: 16px;">{{ Lang::get('decima-oadh::media-monitoring.humanRightFilterTitle0') }}</legend>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-tracing-filter', Lang::get('Eje'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::text('oadh-tracing-filter', null , array('id' => 'oadh-tracing-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-right-filter', Lang::get('Derechos'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::text('oadh-right-filter', null , array('id' => 'oadh-right-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-topic-filter', Lang::get('Temas'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::text('oadh-topic-filter', null , array('id' => 'oadh-topic-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-violated-fact-filter', Lang::get('Hechos'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::text('oadh-violated-fact-filter', null , array('id' => 'oadh-violated-fact-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<legend style="font-size: 16px;">{{ Lang::get('decima-oadh::media-monitoring.humanRightFilterTitle1') }}</legend>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-tracing-closed-filter-label', Lang::get('Eje'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::autocomplete('oadh-tracing-closed-filter-label', $humanRights, array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-tracing-closed-filter-label', 'oadh-tracing-closed-filter-id', null, '') !!}
											{!! Form::hidden('oadh-tracing-closed-filter-id', null, array('id'  =>  'oadh-tracing-closed-filter-id')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-right-closed-filter-label', Lang::get('Derechos'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::autocomplete('oadh-right-closed-filter-label', array(), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-right-closed-filter-label', 'oadh-right-closed-filter-id', null, '') !!}
											{!! Form::hidden('oadh-right-closed-filter-id', null, array('id'  =>  'oadh-right-closed-filter-id')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-topic-closed-filter-label', Lang::get('Temas'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::autocomplete('oadh-topic-closed-filter-label', array(), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-topic-closed-filter-label', 'oadh-topic-closed-filter-id', null, '') !!}
											{!! Form::hidden('oadh-topic-closed-filter-id', null, array('id'  =>  'oadh-topic-closed-filter-id')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										{!! Form::label('oadh-violated-fact-closed-filter-label', Lang::get('Hechos'), array('class' => 'col-sm-3 control-label')) !!}
										<div class="col-sm-9 mg-hm">
											{!! Form::autocomplete('oadh-violated-fact-closed-filter-label', array(), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-violated-fact-closed-filter-label', 'oadh-violated-fact-closed-filter-id', null, '') !!}
											{!! Form::hidden('oadh-violated-fact-closed-filter-id', null, array('id'  =>  'oadh-violated-fact-closed-filter-id')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="info-source-filters">
							<br/>
							<div class="row">
								<div class="col-lg-6 col-md-12">
									<div class="form-group">
										{!! Form::label('oadh-source-filter', Lang::get('Fuente'), array('class' => 'col-sm-2 control-label')) !!}
										<div class="col-sm-10 mg-hm">
											{!! Form::text('oadh-source-filter', null , array('id' => 'oadh-source-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
									<div class="form-group">
										{!! Form::label('oadh-instance-filter', Lang::get('Instancia'), array('class' => 'col-sm-2 control-label')) !!}
										<div class="col-sm-10 mg-hm">
											{!! Form::text('oadh-instance-filter', null , array('id' => 'oadh-instance-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-12">
									<div class="form-group">
										{!! Form::label('oadh-responsable-filter', Lang::get('Responsable'), array('class' => 'col-sm-2 control-label')) !!}
										<div class="col-sm-10 mg-hm">
											{!! Form::text('oadh-responsable-filter', null , array('id' => 'oadh-responsable-filter', 'class' => 'form-control')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="data-victim-filters">
							<br/>
							<div class="row">
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-victim-name-filter', Lang::get('Nombre'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-victim-name-filter', null , array('id' => 'oadh-victim-name-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-victim-last-name-filter', Lang::get('Apellido'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-victim-last-name-filter', null , array('id' => 'oadh-victim-last-name-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('oadh-victim-age-filter', Lang::get('Edad'), array('class' => 'col-sm-2 control-label label-filter-padding label-filter-double-line-padding-top')) !!}
										<div class="col-sm-10 mg-hm">
											<div class="input-group">
												<span class="input-group-addon">Desde</span>
												<input class="form-control" type="text" name="oadh-victim-age-from-filter" id="oadh-victim-age-from-filter">
												<span class="input-group-addon">Hasta</span>
												<input class="form-control" type="text" name="oadh-victim-age-to-filter" id="oadh-victim-age-to-filter">
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-victim-profesion-filter', Lang::get('Profesión'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-victim-profesion-filter', null , array('id' => 'oadh-victim-profesion-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-victim-gender-filter', Lang::get('decima-oadh::media-monitoring.gender'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::select('oadh-victim-gender-filter', array('' => '', 'M' => 'Mujer', 'H' => 'Hombre'), null, array('id' => 'oadh-victim-gender-filter', 'class' => 'form-control',  'data-mg-required' => '')) !!}
												</div>
											</div>
										</div>
									</div>
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-type-filter', Lang::get('decima-oadh::media-monitoring.type'), array('class' => 'col-sm-3 control-label')) !!}
										<div class='col-sm-9'>
											<label class="radio-inline">
												{!! Form::checkbox('oadh-victim-type-filter', 'P', false, array('id' => 'oadh-victim-type-filter-p', 'class' => 'oadh-victim-type-filter')) !!} {{ Lang::get('decima-oadh::media-monitoring.principal') }}
											</label>
											<label class="radio-inline">
												{!! Form::checkbox('oadh-victim-type-filter', 'S', false, array('id' => 'oadh-victim-type-filter-s', 'class' => 'oadh-victim-type-filter')) !!} {{ Lang::get('decima-oadh::media-monitoring.secondary') }}
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="data-victimizer-filters">
							<br/>
							<div class="row">
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-victimizer-name-filter', Lang::get('Nombre'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-victimizer-name-filter', null , array('id' => 'oadh-victimizer-name-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-victimizer-last-name-filter', Lang::get('Apellido'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-victimizer-last-name-filter', null , array('id' => 'oadh-victimizer-last-name-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('oadh-victimizer-age-filter', Lang::get('Edad'), array('class' => 'col-sm-2 control-label label-filter-padding label-filter-double-line-padding-top')) !!}
										<div class="col-sm-10 mg-hm">
											<div class="input-group">
												<span class="input-group-addon">Desde</span>
												<input class="form-control" type="text" name="oadh-victimizer-age-from-filter" id="oadh-victimizer-age-from-filter">
												<span class="input-group-addon">Hasta</span>
												<input class="form-control" type="text" name="oadh-victimizer-age-to-filter" id="oadh-victimizer-age-to-filter">
											</div>
										</div>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-victimizer-profesion-filter', Lang::get('Profesión'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-victimizer-profesion-filter', null , array('id' => 'oadh-victimizer-profesion-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-victimizer-gender-filter', Lang::get('decima-oadh::media-monitoring.gender'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::select('oadh-victimizer-gender-filter', array('' => '', 'M' => 'Mujer', 'H' => 'Hombre'), null, array('id' => 'oadh-victimizer-gender-filter', 'class' => 'form-control',  'data-mg-required' => '')) !!}
												</div>
											</div>
										</div>
									</div>
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-type-filter', Lang::get('decima-oadh::media-monitoring.type'), array('class' => 'col-sm-3 control-label')) !!}
										<div class='col-sm-9'>
											<label class="radio-inline">
												{!! Form::checkbox('oadh-victimizer-type-filter', 'P', false, array('id' => 'oadh-victimizer-type-filter-p', 'class' => 'oadh-victimizer-type-filter')) !!} {{ Lang::get('decima-oadh::media-monitoring.principal') }}
											</label>
											<label class="radio-inline">
												{!! Form::checkbox('oadh-victimizer-type-filter', 'S', false, array('id' => 'oadh-victimizer-type-filter-s', 'class' => 'oadh-victimizer-type-filter')) !!} {{ Lang::get('decima-oadh::media-monitoring.secondary') }}
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="data-facts-filters">
							<br/>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										{!! Form::label('oadh-weapon-filter', Lang::get('decima-oadh::media-monitoring.weaponShort'), array('class' => 'col-sm-2 control-label label-filter-padding label-filter-double-line-padding-top')) !!}
										<div class="col-sm-10 mg-hm">
											{!! Form::text('oadh-weapon-filter', null , array('id' => 'oadh-weapon-filter', 'class' => 'form-control')) !!}
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('oadh-place-filter', Lang::get('decima-oadh::media-monitoring.place'), array('class' => 'col-sm-2 control-label label-filter-padding label-filter-double-line-padding-top')) !!}
										<div class="col-sm-10 mg-hm">
											{!! Form::text('oadh-place-filter', null , array('id' => 'oadh-place-filter', 'class' => 'form-control')) !!}
										</div>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													{!! Form::label('oadh-hipotesis-filter', Lang::get('decima-oadh::media-monitoring.hypothesisFactShort'), array('class' => 'col-sm-2 control-label')) !!}
													<div class="col-sm-10 mg-hm">
														{!! Form::text('oadh-hipotesis-filter', null , array('id' => 'oadh-hipotesis-filter', 'class' => 'form-control')) !!}
													</div>
												</div>
											</div>
											{{-- <div class="col-lg-6">
												<div class="form-group">
													{!! Form::label('oadh-event-filter', Lang::get('Tipo evento'), array('class' => 'col-sm-3 control-label')) !!}
													<div class="col-sm-9 mg-hm">
														{!! Form::text('oadh-event-filter', null , array('id' => 'oadh-event-filter', 'class' => 'form-control')) !!}
													</div>
												</div>
											</div> --}}
									</div>

									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-description-filter', Lang::get('Narración'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-description-filter', null , array('id' => 'oadh-description-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												{!! Form::label('oadh-context-filter', Lang::get('decima-oadh::media-monitoring.contextShort'), array('class' => 'col-sm-3 control-label')) !!}
												<div class="col-sm-9 mg-hm">
													{!! Form::text('oadh-context-filter', null , array('id' => 'oadh-context-filter', 'class' => 'form-control')) !!}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
		<div id="oadh-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new'), array('id' => 'oadh-btn-new', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-oadh::media-monitoring.newNew'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-btn-refresh', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
         		<li><a id='oadh-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> Hoja de cálculo (solo encabezado)</a></li>
       		</ul>
				</div>
			</div>
			<div id="oadh-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-eye"></i> ' . Lang::get('toolbar.view'), array('id' => 'oadh-btn-view', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::media-monitoring.view'))) !!}
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-btn-edit', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::media-monitoring.editNew'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-btn-delete', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::media-monitoring.deleteNew'))) !!}
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('toolbar.upload'), array('id' => 'oadh-btn-upload', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-folder' => Lang::get('decima-oadh::media-monitoring.folder'))) !!}
				{!! Form::button('<i class="fa fa-files-o"></i> ' . Lang::get('toolbar.showFiles'), array('id' => 'oadh-btn-show-files', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip hidden', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showFilesLongText'))) !!}
				{!! Form::button('<i class="fa fa-history"></i> ' . Lang::get('toolbar.showHistoryLongText'), array('id' => 'oadh-btn-show-history', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showHistoryLongText'))) !!}
			</div>
			<div id="oadh-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::media-monitoring.saveNew'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-btn-close', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-grid'>
				{!!
				GridRender::setGridId("oadh-grid")
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('height', 'auto')
					->setGridOption('multiselect', false)
					->setGridOption('rowNum', 5)
					->setGridOption('shrinkToFit', true)
					//->setGridOption('forceFit', true)
					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
		    	->setGridOption('url',URL::to('ucaoadh/transactions/media-monitoring/grid-data-master'))
		    	->setGridOption('caption', Lang::get('decima-oadh::media-monitoring.gridMasterTitle'))
		    	->setGridOption('postData',array('_token' => Session::token()))
					->setGridOption('grouping', true)
					->setGridEvent('loadComplete', 'oadhOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhOnSelectRowEvent')
					->addColumn(array('index' => 'id', 'name' => 'oadh_id', 'hidden' => true))
					->addColumn(array('index' => 'n.twitter_link', 'name' => 'oadh_links_twitter_link', 'hidden' => true))
					->addColumn(array('index' => 'n.facebook_link', 'name' => 'oadh_links_facebook_link', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.code'), 'index' => 'n.code' ,'name' => 'oadh_code', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('form.date'), 'index' => 'n.date' ,'name' => 'oadh_date', 'formatter' => 'date', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.time'), 'index' => 'n.time' ,'name' => 'oadh_time', 'align' => 'center'))
		    	->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.title'), 'index' => 'n.title' ,'name' => 'oadh_title', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.pageShort'), 'index' => 'n.page' ,'name' => 'oadh_page', 'width'=>60 ,'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.journalisticGenre'), 'index' => 'n.journalistic_genre' ,'name' => 'oadh_journalistic_genre', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.summary'), 'index' => 'n.summary' ,'name' => 'oadh_summary', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.newLink'), 'index' => 'n.new_link' ,'name' => 'oadh_new_link', 'hidden' => true, 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.noteLocations'), 'index' => 'n.note_locations' ,'name' => 'oadh_note_locations', 'hidden' => true, 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.digitalMediaShort'), 'index' => 'n.digital_media_id' ,'name' => 'oadh_digital_media_label', 'align' => 'center'))
					->addColumn(array('index' => 'n.digital_media_id' ,'name' => 'oadh_digital_media_id', 'hidden' => true, 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.section'), 'index' => 'n.section_id' ,'name' => 'oadh_section_label', 'align' => 'center'))
					->addColumn(array('index' => 'n.section_id' ,'name' => 'oadh_section_id', 'hidden' => true, 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.newLinkShort'), 'index' => 'oadh_news_html_link' ,'name' => 'oadh_news_html_link', 'width' => '60', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-front-human-right-grid'>
				{!!
				GridRender::setGridId("oadh-front-human-right-grid")
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-human-rights'))
					->setGridOption('caption', Lang::get('decima-oadh::media-monitoring.gridHumanRights'))
					->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
					->setGridOption('multiselect', false)
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('datatype', 'local')
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'hr.news_id','op':'eq','data':'-1'}]}"))
					->setGridEvent('onSelectRow', 'oadhHumanRightOnSelectRowEvent')
					->addColumn(array('index' => 'hr.id', 'name' => 'oadh_human_right_id', 'hidden' => true))
					->addColumn(array('index' => 'hr.news_id', 'name' => 'oadh_human_right_news_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.tracingType'), 'index' => 'hr.tracing_type_id' ,'name' => 'oadh_human_right_tracing_type_label', 'align' => 'center'))
					->addColumn(array('index' => 'hr.tracing_type_id' ,'name' => 'oadh_human_right_tracing_type_id', 'hidden' => true, 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.right'), 'index' => 'hr.right_id' ,'name' => 'oadh_human_right_right_label', 'align' => 'center'))
					->addColumn(array('index' => 'hr.right_id' ,'name' => 'oadh_human_right_right_id', 'hidden' => true, 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.topic'), 'index' => 'hr.topic_id' ,'name' => 'oadh_human_right_topic_label', 'align' => 'center'))
					->addColumn(array('index' => 'hr.topic_id' ,'name' => 'oadh_human_right_topic_id', 'hidden' => true, 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.violatedFact'), 'index' => 'hr.violated_fact_id' ,'name' => 'oadh_human_right_violated_fact_label', 'align' => 'center'))
					->addColumn(array('index' => 'hr.violated_fact_id' ,'name' => 'oadh_human_right_violated_fact_id', 'hidden' => true, 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.populationAffected'), 'index' => 'hr.population_affected' ,'name' => 'oadh_human_right_population_affected', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.qualification'), 'index' => 'hr.qualification' ,'name' => 'oadh_human_right_qualification', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.justification'), 'index' => 'hr.justification' ,'name' => 'oadh_human_right_justification', 'align' => 'center'))

					->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-front-context-grid'>
				{!!
				GridRender::setGridId("oadh-front-context-grid")
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-context'))
					->setGridOption('caption', Lang::get('decima-oadh::media-monitoring.gridContext'))
					->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
					->setGridOption('multiselect', false)
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('datatype', 'local')
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'con.news_id','op':'eq','data':'-1'}]}"))
					->setGridEvent('onSelectRow', 'oadhContextOnSelectRowEvent')
					->addColumn(array('index' => 'con.id', 'name' => 'oadh_context_id', 'hidden' => true))
					->addColumn(array('index' => 'con.news_id', 'name' => 'oadh_context_news_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.department'), 'index' => 'con.department' ,'name' => 'oadh_context_department', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.municipality'), 'index' => 'con.municipality' ,'name' => 'oadh_context_municipality', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.neighborhood'), 'index' => 'con.neighborhood' ,'name' => 'oadh_context_neighborhood', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-front-source-grid'>
				{!!
				GridRender::setGridId("oadh-front-source-grid")
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-source'))
					->setGridOption('caption', Lang::get('decima-oadh::media-monitoring.gridSource'))
					->setGridOption('filename', Lang::get('decima-sale::sale-order-management.orderTaxGridTitle'))
					->setGridOption('multiselect', false)
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('datatype', 'local')
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'so.news_id','op':'eq','data':'-1'}]}"))
					->setGridEvent('onSelectRow', 'oadhSourceOnSelectRowEvent')
					->addColumn(array('index' => 'pro.id', 'name' => 'oadh_source_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.source'), 'index' => 'pro.source', 'name' => 'oadh_source_source'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.instance'), 'index' => 'pro.source', 'name' => 'oadh_source_instance'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.responsable'), 'index' => 'pro.source', 'name' => 'oadh_source_responsable'))
					->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-front-victim-grid'>
				{!!
				GridRender::setGridId("oadh-front-victim-grid")
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-victim'))
					->setGridOption('caption', Lang::get('decima-oadh::media-monitoring.gridVictim'))
					->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
					->setGridOption('multiselect', false)
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('datatype', 'local')
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'p.news_id','op':'eq','data':'-1'}]}"))
					->setGridEvent('onSelectRow', 'oadhVictimOnSelectRowEvent')
					->addColumn(array('index' => 'p.id', 'name' => 'oadh_victim_id', 'hidden' => true))
					->addColumn(array('index' => 'p.type', 'name' => 'oadh_victim_type', 'hidden' => true))
					->addColumn(array('label' => Lang::get('form.name'), 'index' => 'p.first_name', 'name' => 'oadh_victim_first_name', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('security/user-management.lastname'), 'index' => 'p.last_name', 'name' => 'oadh_victim_last_name', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.type'), 'index' => 'p.subtype', 'name' => 'oadh_victim_subtype', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.typeGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.victimizerRelation'), 'index' => 'p.oadh_victimizer_relation', 'name' => 'oadh_victimizer_relation' , 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.relationGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.secondVictimRelation'), 'index' => 'p.main_victim_relation', 'name' => 'oadh_main_victim_relation' , 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.relationGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.age'), 'index' => 'p.age', 'name' => 'oadh_victim_age', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.profesion'), 'index' => 'p.profesion', 'name' => 'oadh_victim_profesion', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.gender'), 'index' => 'p.gender', 'name' => 'oadh_victim_gender', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-front-victimizer-grid'>
				{!!
				GridRender::setGridId("oadh-front-victimizer-grid")
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-victimizer'))
					->setGridOption('caption', Lang::get('decima-oadh::media-monitoring.gridVictimizer'))
					->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
					->setGridOption('multiselect', false)
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('datatype', 'local')
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'p.news_id','op':'eq','data':'-1'}]}"))
					->setGridEvent('onSelectRow', 'oadhVictimizerOnSelectRowEvent')
					->addColumn(array('index' => 'p.id', 'name' => 'oadh_victimizer_id', 'hidden' => true))
					->addColumn(array('index' => 'p.type', 'name' => 'oadh_victimizer_type', 'hidden' => true))
					->addColumn(array('label' => Lang::get('form.name'), 'index' => 'p.first_name', 'name' => 'oadh_victimizer_first_name', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('security/user-management.lastname'), 'index' => 'p.last_name', 'name' => 'oadh_victimizer_last_name', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.victimizerType'), 'index' => 'p.subtype', 'name' => 'oadh_victimizer_subtype', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.typeGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.victimRelation'), 'index' => 'p.victim_relation', 'name' => 'oadh_victim_relation' , 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.relationGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.victimizerRelation'), 'index' => 'p.victimizer_relation', 'name' => 'oadh_victimizer_relation' , 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.relationGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.age'), 'index' => 'p.age', 'name' => 'oadh_victimizer_age', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.profesion'), 'index' => 'p.profesion', 'name' => 'oadh_victimizer_profesion', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.gender'), 'index' => 'p.gender', 'name' => 'oadh_victimizer_gender', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-front-producedure-grid'>
				{!!
					GridRender::setGridId("oadh-front-producedure-grid")
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-producedure'))
					->setGridOption('caption', Lang::get('decima-oadh::media-monitoring.gridProducedure'))
					->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
					->setGridOption('multiselect', false)
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('datatype', 'local')
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'pro.news_id','op':'eq','data':'-1'}]}"))
					->setGridEvent('onSelectRow', 'oadhProducedureOnSelectRowEvent')
					->addColumn(array('index' => 'pro.id', 'name' => 'oadh_producedure_id', 'hidden' => true))
					->addColumn(array('index' => 'pro.news_id', 'name' => 'oadh_producedure_news_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.placeType'), 'index' => 'pro.place', 'name' => 'oadh_producedure_place', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.weaponType'), 'index' => 'pro.weapon', 'name' => 'oadh_producedure_weapon', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.hypothesisFact'), 'index' => 'pro.hypothesis_fact', 'name' => 'oadh_producedure_hypothesis_fact', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.context'), 'index' => 'pro.context', 'name' => 'oadh_producedure_context', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.accidentType'), 'index' => 'pro.accident_type', 'name' => 'oadh_producedure_accident_type', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.summaryPro'), 'index' => 'pro.accident_type', 'name' => 'oadh_producedure_summary', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-front-news-news-grid'>
				{!!
					GridRender::setGridId("oadh-front-news-news-grid")
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-news-news'))
					->setGridOption('caption', Lang::get('Relación entre noticias'))
					->setGridOption('filename', Lang::get('Relación entre noticias'))
					->setGridOption('multiselect', false)
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('datatype', 'local')
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'nn.news_id','op':'eq','data':'-1'}]}"))
					->setGridEvent('onSelectRow', 'oadhNewsNewsOnSelectRowEvent')
					->addColumn(array('index' => 'nn.id', 'name' => 'oadh_news_news_id', 'hidden' => true))
					->addColumn(array('index' => 'nn.news_id', 'name' => 'oadh_news_news_related_news_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.title'), 'index' => 'n.title', 'name' => 'oadh_news_news_related_news_label', 'align' => 'center'))
					->renderGrid();
				!!}
			</div>
		</div>
	</div>
</div>
<div id='oadh-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-form-new-title" class="hidden">{{ Lang::get('decima-oadh::media-monitoring.formNewTitle') }}</legend>
				<legend id="oadh-form-edit-title" class="hidden">{{ Lang::get('decima-oadh::media-monitoring.formEditTitle') }}<label class="pull-right oadh-number"></label></legend>
				<div class="row">
					<div class="col-md-6 form-division-line">
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-code', Lang::get('decima-oadh::media-monitoring.code'), array('class' => 'control-label')) !!}
									{!! Form::text('oadh-code', null , array('id' => 'oadh-code', 'class' => 'form-control', 'disabled' => 'disabled')) !!}
									{!! Form::hidden('oadh-id', null, array('id' => 'oadh-id')) !!}
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-date', Lang::get('decima-oadh::media-monitoring.date'), array('class' => 'control-label')) !!}
									{!! Form::date('oadh-date', array('class' => 'form-control', 'data-mg-required' => '')) !!}
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group mg-hm clearfix">
									{!! Form::label('oadh-time', Lang::get('decima-oadh::media-monitoring.time'), array('class' => 'control-label')) !!}
									<input class="form-control" type="time" name="oadh-time" id="oadh-time" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group mg-hm clearfix">
									{!! Form::label('oadh-digital-media', Lang::get('decima-oadh::media-monitoring.digitalMedia'), array('class' => 'control-label ')) !!}
									{!! Form::autocomplete('oadh-digital-media-label', $digitalMedias, array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-digital-media-label', 'oadh-digital-media-id', null, 'fa-files-o') !!}
									{!! Form::hidden('oadh-digital-media-id', null, array('id'  =>  'oadh-digital-media-id')) !!}
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group mg-hm clearfix">
									{!! Form::label('oadh-section', Lang::get('decima-oadh::media-monitoring.section'), array('class' => 'control-label')) !!}
									{!! Form::autocomplete('oadh-section-label', array(), array('class' => 'form-control', 'data-mg-required' => ''), 'oadh-section-label', 'oadh-section-id', null, 'fa-files-o') !!}
									{!! Form::hidden('oadh-section-id', null, array('id'  =>  'oadh-section-id')) !!}
								</div>
							</div>
						</div>

						<div class="form-group mg-hm">
							{!! Form::label('oadh-title', Lang::get('decima-oadh::media-monitoring.title'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-title', null , array('id' => 'oadh-title', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>

						<div class="row">
							<div class="col-md-8">
								<div class="form-group mg-hm clearfix">
									{!! Form::label('oadh-note-locations', Lang::get('decima-oadh::media-monitoring.noteLocations'), array('class' => 'control-label')) !!}
									{!! Form::autocomplete('oadh-note-locations', $noteLocations, array('class' => 'form-control', 'data-mg-required' => ''), 'oadh-note-locations', '', null, 'fa-files-o') !!}
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-page', Lang::get('decima-oadh::media-monitoring.page'), array('class' => 'control-label')) !!}
									{!! Form::text('oadh-page', null , array('id' => 'oadh-page', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger')) !!}
					  		</div>
							</div>
						</div>

					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-journalistic-genre', Lang::get('decima-oadh::media-monitoring.journalisticGenre'), array('class' => 'control-label')) !!}
							{!! Form::autocomplete('oadh-journalistic-genre', $journalisticGenres, array('class' => 'form-control', 'data-mg-required' => ''), 'oadh-journalistic-genre', '', null, 'fa-files-o') !!}
						</div>
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-new-link', Lang::get('decima-oadh::media-monitoring.newLink'), array('class' => 'control-label')) !!}
							{!! Form::text('oadh-new-link', null , array('id' => 'oadh-new-link', 'class' => 'form-control')) !!}
						</div>
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-summary', Lang::get('decima-oadh::media-monitoring.summary'), array('class' => 'control-label')) !!}
							{!! Form::textareacustom('oadh-summary', 6, 400 , array('id' => 'oadh-summary', 'class' => 'form-control', 'data-mg-required' => '')) !!}
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>


		<ul class="nav nav-tabs" role="tablist" style="margin-top:10px;">
			<li role="presentation" class="active"><a href="#humans-rights" aria-controls="humans-rights" role="tab" data-toggle="tab">{{ Lang::get('Derechos humanos') }}</a></li>
			<li role="presentation"><a href="#context" aria-controls="context" role="tab" data-toggle="tab">{{ Lang::get('Contextuales') }}</a></li>
			<li role="presentation"><a href="#info-source" aria-controls="info-source" role="tab" data-toggle="tab">{{ Lang::get('Tratamiendo de la informacion') }}</a></li>
			<li role="presentation"><a href="#data-victim" aria-controls="data-victim" role="tab" data-toggle="tab">{{ Lang::get('Victimas') }}</a></li>
			<li role="presentation"><a href="#data-victimizer" aria-controls="data-victimizer" role="tab" data-toggle="tab">{{ Lang::get('Victimarios') }}</a></li>
			<li role="presentation"><a href="#data-producedure" aria-controls="data-producedure" role="tab" data-toggle="tab">{{ Lang::get('Hechos') }}</a></li>
			<li role="presentation"><a href="#data-links" aria-controls="data-links" role="tab" data-toggle="tab">{{ Lang::get('Redes sociales') }}</a></li>
			<li role="presentation"><a href="#relationship-between-news" aria-controls="relationship-between-news" role="tab" data-toggle="tab">{{ Lang::get('Relación entre noticias') }}</a></li>
		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="humans-rights">

				<div class="form-container form-container-followed-by-grid-section">
					{!! Form::open(array('id' => 'oadh-human-right-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
					<fieldset id="oadh-human-right-form-fieldset" disabled="disabled">
						<div id="oadh-human-right-section" class="row">
							<div class="col-md-6 form-division-line">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group mg-hm">
											{!! Form::label('oadh-human-right-tracing-type-label', Lang::get('decima-oadh::media-monitoring.tracingType'), array('class' => 'control-label')) !!}
											{!! Form::autocomplete('oadh-human-right-tracing-type-label', $humanRights, array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-human-right-tracing-type-label', 'oadh-human-right-tracing-type-id', null, 'fa-files-o') !!}
											{!! Form::hidden('oadh-human-right-tracing-type-id', null, array('id'  =>  'oadh-human-right-tracing-type-id')) !!}
											{!! Form::hidden('oadh-human-right-news-id', null, array('id'  =>  'oadh-human-right-news-id', 'data-mg-clear-ignored' => '')) !!}
											{!! Form::hidden('oadh-human-right-id', null, array('id'  =>  'oadh-human-right-id')) !!}
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group mg-hm">
											{!! Form::label('oadh-human-right-right-label', Lang::get('decima-oadh::media-monitoring.right'), array('class' => 'control-label')) !!}
											{!! Form::autocomplete('oadh-human-right-right-label', array(), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-human-right-right-label', 'oadh-human-right-right-id', null, 'fa-files-o') !!}
											{!! Form::hidden('oadh-human-right-right-id', null, array('id'  =>  'oadh-human-right-right-id')) !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group mg-hm">
											{!! Form::label('oadh-human-right-topic-label', Lang::get('decima-oadh::media-monitoring.topic'), array('class' => 'control-label')) !!}
											{!! Form::autocomplete('oadh-human-right-topic-label', array(), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-human-right-topic-label', 'oadh-human-right-topic-id', null, 'fa-files-o') !!}
											{!! Form::hidden('oadh-human-right-topic-id', null, array('id'  =>  'oadh-human-right-topic-id')) !!}
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group mg-hm">
											{!! Form::label('oadh-human-right-violated-fact-label', Lang::get('decima-oadh::media-monitoring.violatedFact'), array('class' => 'control-label')) !!}
											{!! Form::autocomplete('oadh-human-right-violated-fact-label', array(), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-human-right-violated-fact-label', 'oadh-human-right-violated-fact-id', null, 'fa-files-o') !!}
											{!! Form::hidden('oadh-human-right-violated-fact-id', null, array('id'  =>  'oadh-human-right-violated-fact-id')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-human-right-population-affected', Lang::get('decima-oadh::media-monitoring.populationAffected'), array('class' => 'control-label')) !!}
									{!! Form::autocomplete('oadh-human-right-population-affected', $populationAffected, array('class' => 'form-control'), 'oadh-human-right-population-affected', '', null, 'fa-files-o') !!}
									<p class="help-block">&nbsp;</p>
								</div>
								<div class="row">
									<div class="col-lg-4">
										<div class="form-group mg-hm">
											{!! Form::label('oadh-human-right-qualification', Lang::get('decima-oadh::media-monitoring.qualification'), array('class' => 'control-label')) !!}
											{!! Form::text('oadh-human-right-qualification', null , array('id' => 'oadh-human-right-qualification', 'class' => 'form-control', 'data-mg-required' => '', 'data-mg-validator' => 'signedInteger', 'maxlength' => '2')) !!}
										</div>
									</div>
									<div class="col-lg-8">
										<div class="form-group mg-hm">
											{!! Form::label('oadh-human-right-justification', Lang::get('decima-oadh::media-monitoring.justification'), array('class' => 'control-label')) !!}
											{!! Form::textareacustom('oadh-human-right-justification', 1, 200 , array('id' => 'oadh-human-right-justification', 'class' => 'form-control', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
					{!! Form::close() !!}
				</div>
				<div id="oadh-human-right-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-human-right-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-human-right-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => 'false')) !!}
					</div>
					<div id="oadh-human-right-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-human-right-btn-refresh', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						<div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-human-right-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-human-right-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div>
					</div>
					<div id="oadh-human-right-btn-group-2" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-human-right-btn-edit', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-human-right-btn-delete', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
					</div>
				</div>
				<div id='oadh-human-right-grid-section' class='app-grid oadh-human-right-grid-section collapse in' data-app-grid-id='oadh-human-right-grid'>
					{!!
					GridRender::setGridId("oadh-human-right-grid")
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-human-rights'))
						->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('datatype', 'local')
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'hr.news_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhHumanRightOnSelectRowEvent')
						->addColumn(array('index' => 'hr.id', 'name' => 'oadh_human_right_id', 'hidden' => true))
						->addColumn(array('index' => 'hr.news_id', 'name' => 'oadh_human_right_news_id', 'hidden' => true))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.tracingType'), 'index' => 'hr.tracing_type_id' ,'name' => 'oadh_human_right_tracing_type_label', 'align' => 'center'))
						->addColumn(array('index' => 'hr.tracing_type_id' ,'name' => 'oadh_human_right_tracing_type_id', 'hidden' => true, 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.right'), 'index' => 'hr.right_id' ,'name' => 'oadh_human_right_right_label', 'align' => 'center'))
						->addColumn(array('index' => 'hr.right_id' ,'name' => 'oadh_human_right_right_id', 'hidden' => true, 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.topic'), 'index' => 'hr.topic_id' ,'name' => 'oadh_human_right_topic_label', 'align' => 'center'))
						->addColumn(array('index' => 'hr.topic_id' ,'name' => 'oadh_human_right_topic_id', 'hidden' => true, 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.violatedFact'), 'index' => 'hr.violated_fact_id' ,'name' => 'oadh_human_right_violated_fact_label', 'align' => 'center'))
						->addColumn(array('index' => 'hr.violated_fact_id' ,'name' => 'oadh_human_right_violated_fact_id', 'hidden' => true, 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.populationAffected'), 'index' => 'hr.population_affected' ,'name' => 'oadh_human_right_population_affected', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.qualification'), 'index' => 'hr.qualification' ,'name' => 'oadh_human_right_qualification', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.justification'), 'index' => 'hr.justification' ,'name' => 'oadh_human_right_justification', 'align' => 'center'))
						->renderGrid();
					!!}
				</div>
			</div>


			<div role="tabpanel" class="tab-pane" id="context">
				<div class="form-container form-container-followed-by-grid-section">
					{!! Form::open(array('id' => 'oadh-context-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
					<fieldset id="oadh-context-form-fieldset" disabled="disabled">
						<div id="oadh-context-section" class="row">
							<div class="col-lg-6 col-md-6 form-division-line">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group mg-hm">
											{!! Form::label('oadh-context-department', Lang::get('decima-oadh::media-monitoring.department'), array('class' => 'control-label')) !!}
											{!! Form::autocomplete('oadh-context-department', $departments, array('class' => 'form-control'), 'oadh-context-department', 'oadh-context-department', null, 'fa-files-o') !!}
											{!! Form::hidden('oadh-context-id', null , array('id' => 'oadh-context-id', 'class' => 'form-control', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-context-news-id', null , array('id' => 'oadh-context-news-id', 'class' => 'form-control', 'data-mg-required' => '', 'data-mg-clear-ignored' => '')) !!}
							  		</div>
									</div>
									<div class="col-md-6">
										<div class="form-group mg-hm">
											{!! Form::label('oadh-context-municipality', Lang::get('decima-oadh::media-monitoring.municipality'), array('class' => 'control-label')) !!}
											{!! Form::autocomplete('oadh-context-municipality', array(), array('class' => 'form-control'), 'oadh-context-municipality', 'oadh-context-municipality', null, 'fa-files-o') !!}
							  		</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group mg-hm">
									{!! Form::label('oadh-context-neighborhood', Lang::get('decima-oadh::media-monitoring.neighborhood'), array('class' => 'control-label')) !!}
									{!! Form::text('oadh-context-neighborhood', null , array('id' => 'oadh-context-neighborhood', 'class' => 'form-control')) !!}
								</div>
							</div>
						</div>
					</fieldset>
					{!! Form::close() !!}
				</div>
				<div id="oadh-context-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-context-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-context-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => 'false')) !!}
					</div>
					<div id="oadh-context-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-context-btn-refresh', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						<div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-context-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-context-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div>
					</div>
					<div id="oadh-context-btn-group-2" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-context-btn-edit', 'class' => 'btn btn-default oadh-context-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-context-btn-delete', 'class' => 'btn btn-default oadh-context-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
					</div>
				</div>
				<div id='oadh-context-grid-section' class='app-grid collapse in' data-app-grid-id='oadh-context-grid'>
					{!!
					GridRender::setGridId("oadh-context-grid")
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-context'))
						->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('datatype', 'local')
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'con.news_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhContextOnSelectRowEvent')
						->addColumn(array('index' => 'con.id', 'name' => 'oadh_context_id', 'hidden' => true))
						->addColumn(array('index' => 'con.news_id', 'name' => 'oadh_context_news_id', 'hidden' => true))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.department'), 'index' => 'con.department' ,'name' => 'oadh_context_department', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.municipality'), 'index' => 'con.municipality' ,'name' => 'oadh_context_municipality', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.neighborhood'), 'index' => 'con.neighborhood' ,'name' => 'oadh_context_neighborhood', 'align' => 'center'))
						->renderGrid();
					!!}
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="info-source">
				<div class="form-container form-container-followed-by-grid-section">
					{!! Form::open(array('id' => 'oadh-source-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
					<fieldset id="oadh-source-form-fieldset" disabled="disabled">
						<!-- <legend id="oadh-order-tax-form-new-title" class="">{{ Lang::get('decima-sale::sale-order-management.formOrderTax') }}</legend> -->
						<div id="oadh-source-section" class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group mg-hm clearfix">
									{!! Form::label('oadh-source', Lang::get('decima-oadh::media-monitoring.source'), array('class' => 'control-label')) !!}
									{!! Form::autocomplete('oadh-source-source', $sources, array('class' => 'form-control'), 'oadh-source-source', 'oadh-source-source', null, 'fa-files-o') !!}
									{!! Form::hidden('oadh-source-id', null , array('id' => 'oadh-source-id', 'class' => 'form-control')) !!}
									{!! Form::hidden('oadh-source-news-id', null , array('id' => 'oadh-source-news-id', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
								</div>
								<div class="form-group mg-hm clearfix">
									{!! Form::label('oadh-source-instance', Lang::get('decima-oadh::media-monitoring.instance'), array('class' => 'control-label')) !!}
									{!! Form::text('oadh-source-instance', null , array('id' => 'oadh-source-instance', 'class' => 'form-control' )) !!}
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group mg-hm clearfix">
									{!! Form::label('oadh-source-responsable', Lang::get('decima-oadh::media-monitoring.responsable'), array('class' => 'control-label')) !!}
									{!! Form::text('oadh-source-responsable', null , array('id' => 'oadh-source-responsable', 'class' => 'form-control' )) !!}
								</div>
							</div>
						</div>
					</fieldset>
					{!! Form::close() !!}
				</div>
				<div id="oadh-source-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-source-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-source-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => 'false')) !!}
					</div>
					<div id="oadh-source-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-source-btn-refresh', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						<div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-source-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-source-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div>
					</div>
					<div id="oadh-source-btn-group-2" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-source-btn-edit', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-source-btn-delete', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
					</div>
				</div>
				<div id='oadh-source-grid-section' class='app-grid collapse in' data-app-grid-id='oadh-source-grid'>
					{!!
					GridRender::setGridId("oadh-source-grid")
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-source'))
						->setGridOption('filename', Lang::get('decima-sale::sale-order-management.orderTaxGridTitle'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('datatype', 'local')
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'so.news_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhSourceOnSelectRowEvent')
						->addColumn(array('index' => 'pro.id', 'name' => 'oadh_source_id', 'hidden' => true))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.source'), 'index' => 'pro.source', 'name' => 'oadh_source_source'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.instance'), 'index' => 'pro.source', 'name' => 'oadh_source_instance'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.responsable'), 'index' => 'pro.source', 'name' => 'oadh_source_responsable'))
						->renderGrid();
					!!}
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="data-victim">
				<div class="form-container form-container-followed-by-grid-section">
				{!! Form::open(array('id' => 'oadh-victim-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<fieldset id="oadh-victim-form-fieldset" disabled="disabled">
					<div id="oadh-victim-section" class="row">
						<div class="col-md-6 form-division-line">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-first-name', Lang::get('form.name'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-victim-first-name', null , array('id' => 'oadh-victim-first-name', 'class' => 'form-control')) !!}
										{!! Form::hidden('oadh-victim-id', null , array('id' => 'oadh-victim-id', 'class' => 'form-control')) !!}
										{!! Form::hidden('oadh-victim-news-id', null , array('id' => 'oadh-victim-news-id', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-victim-type', "V" , array('id' => 'oadh-victim-type', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-victim-subtype', "P" , array('id' => 'oadh-victim-subtype', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-victim-victimizer-relation', '1' , array('id' => 'oadh-victim-victimizer-relation', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-victim-main-victim-relation', null , array('id' => 'oadh-victim-main-victim-relation', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-last-name', Lang::get('security/user-management.lastname'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-victim-last-name', null , array('id' => 'oadh-victim-last-name', 'class' => 'form-control')) !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-2">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-age', Lang::get('decima-oadh::media-monitoring.age'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-victim-age', null , array('id' => 'oadh-victim-age', 'class' => 'form-control')) !!}
									</div>
								</div>
								<div class="col-lg-10">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-profesion', Lang::get('decima-oadh::media-monitoring.profesion'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-victim-profesion', null , array('id' => 'oadh-victim-profesion', 'class' => 'form-control')) !!}
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-gender', Lang::get('decima-oadh::media-monitoring.sex'), array('class' => 'control-label')) !!}
										{!! Form::select('oadh-victim-gender', array('' => '', 'M' => 'Mujer', 'H' => 'Hombre', 'N' => 'No especificado'), null, array('class' => 'form-control',  'data-mg-required' => '')) !!}
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-sexual-diversity', Lang::get('decima-oadh::media-monitoring.sexualDiversity'), array('class' => 'control-label')) !!}
										{!! Form::autocomplete('oadh-victim-sexual-diversity', $sexualDiversities, array('class' => 'form-control'), 'oadh-victim-sexual-diversity', 'oadh-victim-sexual-diversity', null, 'fa-files-o') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-subtype', Lang::get('decima-oadh::media-monitoring.type'), array('class' => 'control-label')) !!}
										<div style="margin-top:5px;">
											<label class="radio-inline">
												{!! Form::radio('oadh-victim-subtype', 'P', true, array('id' => 'oadh-victim-subtype-p', 'class' => 'oadh-victim-subtype')) !!} {{ Lang::get('decima-oadh::media-monitoring.principal') }}
											</label>
											<label class="radio-inline">
												{!! Form::radio('oadh-victim-subtype', 'S', false, array('id' => 'oadh-victim-subtype-s', 'class' => 'oadh-victim-subtype')) !!} {{ Lang::get('decima-oadh::media-monitoring.secondary') }}
											</label>
										</div>
									</div>
								</div>
								<div class="col-md-6" id="oadh-div-victim-relation">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-relation', Lang::get('decima-oadh::media-monitoring.victimizerRelation'), array('class' => 'control-label')) !!}
										<div style="margin-top:5px;">
											<label class="radio-inline">
												{!! Form::radio('oadh-victim-relation', true, true, array('id' => 'oadh-victim-relation-yes', 'class' => 'oadh-victim-relation', 'data-val' => '1')) !!} {{ Lang::get('form.yes') }}
											</label>
											<label class="radio-inline">
												{!! Form::radio('oadh-victim-relation', false, false, array('id' => 'oadh-victim-relation-no', 'class' => 'oadh-victim-relation', 'data-val' => '0')) !!} {{ Lang::get('form.no') }}
											</label>
										</div>
									</div>
								</div>
								<div class="col-md-6" id="oadh-div-second-victim-relation">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victim-second-relation', Lang::get('decima-oadh::media-monitoring.secondVictimRelation'), array('class' => 'control-label')) !!}
										<div style="margin-top:5px;">
											<label class="radio-inline">
												{!! Form::radio('oadh-victim-second-relation', true, true, array('id' => 'oadh-victim-second-relation-yes', 'class' => 'oadh-victim-second-relation', 'data-val' => '1')) !!} {{ Lang::get('form.yes') }}
											</label>
											<label class="radio-inline">
												{!! Form::radio('oadh-victim-second-relation', false, false, array('id' => 'oadh-victim-second-relation-no', 'class' => 'oadh-victim-second-relation', 'data-val' => '0')) !!} {{ Lang::get('form.no') }}
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
				{!! Form::close() !!}
				</div>
				<div id="oadh-victim-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-victim-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-victim-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => 'false')) !!}
					</div>
					<div id="oadh-victim-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-victim-btn-refresh', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						<div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-victim-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-victim-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div>
					</div>
					<div id="oadh-victim-btn-group-2" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-victim-btn-edit', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-victim-btn-delete', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
					</div>
				</div>
				<div id='oadh-victim-grid-section' class='app-grid oadh-victim-grid-section collapse in' data-app-grid-id='oadh-victim-grid'>
					{!!
					GridRender::setGridId("oadh-victim-grid")
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-victim'))
						->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('datatype', 'local')
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'p.news_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhVictimOnSelectRowEvent')
						->addColumn(array('index' => 'p.id', 'name' => 'oadh_victim_id', 'hidden' => true))
						->addColumn(array('index' => 'p.type', 'name' => 'oadh_victim_type', 'hidden' => true))
						->addColumn(array('label' => Lang::get('form.name'), 'index' => 'p.first_name', 'name' => 'oadh_victim_first_name', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('security/user-management.lastname'), 'index' => 'p.last_name', 'name' => 'oadh_victim_last_name', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.type'), 'index' => 'p.subtype', 'name' => 'oadh_victim_subtype', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.typeGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.victimizerRelation'), 'index' => 'p.oadh_victimizer_relation', 'name' => 'oadh_victimizer_relation' , 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.relationGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.secondVictimRelation'), 'index' => 'p.main_victim_relation', 'name' => 'oadh_main_victim_relation' , 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.relationGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.age'), 'index' => 'p.age', 'name' => 'oadh_victim_age', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.profesion'), 'index' => 'p.profesion', 'name' => 'oadh_victim_profesion', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.gender'), 'index' => 'p.gender', 'name' => 'oadh_victim_gender', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.sexualDiversity'), 'index' => 'p.sexual_diversity', 'name' => 'oadh_victim_sexual_diversity', 'align' => 'center'))
						->renderGrid();
					!!}
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="data-victimizer">
				<div class="form-container form-container-followed-by-grid-section">
				{!! Form::open(array('id' => 'oadh-victimizer-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<fieldset id="oadh-victimizer-form-fieldset" disabled="disabled">
					<div id="oadh-victimizer-section" class="row">
						<div class="col-md-6 form-division-line">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-first-name', Lang::get('form.name'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-victimizer-first-name', null , array('id' => 'oadh-victimizer-first-name', 'class' => 'form-control')) !!}
										{!! Form::hidden('oadh-victimizer-id', null , array('id' => 'oadh-victimizer-id', 'class' => 'form-control')) !!}
										{!! Form::hidden('oadh-victimizer-news-id', null , array('id' => 'oadh-victimizer-news-id', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-victimizer-type', "R" , array('id' => 'oadh-victimizer-type', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-victimizer-subtype', "P" , array('id' => 'oadh-victimizer-subtype', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-victimizer-victim-relation', '1' , array('id' => 'oadh-victimizer-victim-relation', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-victimizer-victimizer-relation', null , array('id' => 'oadh-victimizer-victimizer-relation', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-last-name', Lang::get('security/user-management.lastname'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-victimizer-last-name', null , array('id' => 'oadh-victimizer-last-name', 'class' => 'form-control')) !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-2">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-age', Lang::get('decima-oadh::media-monitoring.age'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-victimizer-age', null , array('id' => 'oadh-victimizer-age', 'class' => 'form-control')) !!}
									</div>
								</div>
								<div class="col-lg-10">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-profesion', Lang::get('decima-oadh::media-monitoring.profesion'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-victimizer-profesion', null , array('id' => 'oadh-victimizer-profesion', 'class' => 'form-control')) !!}
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-gender', Lang::get('decima-oadh::media-monitoring.sex'), array('class' => 'control-label')) !!}
										{!! Form::select('oadh-victimizer-gender', array('' => '', 'M' => 'Mujer', 'H' => 'Hombre', 'N' => 'No especificado'), null, array('class' => 'form-control', 'data-mg-required' => '')) !!}
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-sexual-diversity', Lang::get('decima-oadh::media-monitoring.sexualDiversity'), array('class' => 'control-label')) !!}
										{!! Form::autocomplete('oadh-victimizer-sexual-diversity', $sexualDiversities, array('class' => 'form-control'), 'oadh-victimizer-sexual-diversity', 'oadh-victimizer-sexual-diversity', null, 'fa-files-o') !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-subtype', Lang::get('decima-oadh::media-monitoring.victimizerType'), array('class' => 'control-label')) !!}
										<div style="margin-top:5px;">
											<label class="radio-inline">
												{!! Form::radio('oadh-victimizer-subtype', 'P', true  , array('id' => 'oadh-victimizer-subtype-p', 'class' => 'oadh-victimizer-subtype')) !!} {{ Lang::get('decima-oadh::media-monitoring.principal') }}
											</label>
											<label class="radio-inline">
												{!! Form::radio('oadh-victimizer-subtype', 'S', false  , array('id' => 'oadh-victimizer-subtype-s', 'class' => 'oadh-victimizer-subtype')) !!} {{ Lang::get('decima-oadh::media-monitoring.secondary') }}
											</label>
										</div>
									</div>
								</div>
								<div class="col-md-6" id="oadh-div-victimizer-relation">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-victimizer-subtype', Lang::get('decima-oadh::media-monitoring.victimRelation'), array('class' => 'control-label')) !!}
										<div style="margin-top:5px;">
											<label class="radio-inline">
												{!! Form::radio('oadh-victimizer-relation', true, true, array('id' => 'oadh-victimizer-relation-yes', 'class' => 'oadh-victimizer-relation', 'data-val' => '1')) !!} {{ Lang::get('form.yes') }}
											</label>
											<label class="radio-inline">
												{!! Form::radio('oadh-victimizer-relation', false, false, array('id' => 'oadh-victimizer-relation-no', 'class' => 'oadh-victimizer-relation', 'data-val' => '0')) !!} {{ Lang::get('form.no') }}
											</label>
										</div>
									</div>
								</div>
								<div class="col-md-6" id="oadh-div-second-victimizer-relation">
									<div class="form-group mg-hm" id='relation-field'>
										{!! Form::label('oadh-victimizer-second-relation', Lang::get('decima-oadh::media-monitoring.victimizerRelation'), array('class' => 'control-label')) !!}
										<div style="margin-top:5px;">
											<label class="radio-inline">
												{!! Form::radio('oadh-victimizer-second-relation', true, true, array('id' => 'oadh-victimizer-second-relation-yes', 'class' => 'oadh-victimizer-second-relation', 'data-val' => '1')) !!} {{ Lang::get('form.yes') }}
											</label>
											<label class="radio-inline">
												{!! Form::radio('oadh-victimizer-second-relation', false, false, array('id' => 'oadh-victimizer-second-relation-no', 'class' => 'oadh-victimizer-second-relation', 'data-val' => '0')) !!} {{ Lang::get('form.no') }}
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
				{!! Form::close() !!}
				</div>
				<div id="oadh-victimizer-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-victimizer-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-victimizer-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => 'false')) !!}
					</div>
					<div id="oadh-victimizer-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-victimizer-btn-refresh', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						<div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-victimizer-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-victimizer-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div>
					</div>
					<div id="oadh-victimizer-btn-group-2" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-victimizer-btn-edit', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-victimizer-btn-delete', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
					</div>
				</div>
				<div id='oadh-victimizer-grid-section' class='app-grid oadh-victimizer-grid-section collapse in' data-app-grid-id='oadh-victimizer-grid'>
					{!!
					GridRender::setGridId("oadh-victimizer-grid")
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-victimizer'))
						->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('datatype', 'local')
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'p.news_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhVictimizerOnSelectRowEvent')
						->addColumn(array('index' => 'p.id', 'name' => 'oadh_victimizer_id', 'hidden' => true))
						->addColumn(array('index' => 'p.type', 'name' => 'oadh_victimizer_type', 'hidden' => true))
						->addColumn(array('label' => Lang::get('form.name'), 'index' => 'p.first_name', 'name' => 'oadh_victimizer_first_name', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('security/user-management.lastname'), 'index' => 'p.last_name', 'name' => 'oadh_victimizer_last_name', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.victimizerType'), 'index' => 'p.subtype', 'name' => 'oadh_victimizer_subtype', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.typeGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.victimRelation'), 'index' => 'p.victim_relation', 'name' => 'oadh_victim_relation' , 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.relationGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.victimizerRelation'), 'index' => 'p.victimizer_relation', 'name' => 'oadh_victimizer_relation' , 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.relationGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.age'), 'index' => 'p.age', 'name' => 'oadh_victimizer_age', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.profesion'), 'index' => 'p.profesion', 'name' => 'oadh_victimizer_profesion', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.gender'), 'index' => 'p.gender', 'name' => 'oadh_victimizer_gender', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::media-monitoring.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.sexualDiversity'), 'index' => 'p.sexual_diversity', 'name' => 'oadh_victimizer_sexual_diversity', 'align' => 'center'))
						->renderGrid();
					!!}
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="data-producedure">
				<div class="form-container form-container-followed-by-grid-section">
				{!! Form::open(array('id' => 'oadh-producedure-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<fieldset id="oadh-producedure-form-fieldset" disabled="disabled">
					<div id="oadh-producedure-section" class="row">
						<div class="col-md-6 form-division-line">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-producedure-place', Lang::get('decima-oadh::media-monitoring.placeType'), array('class' => 'control-label')) !!}
										{!! Form::text('oadh-producedure-place', null , array('id' => 'oadh-producedure-place', 'class' => 'form-control')) !!}
										{!! Form::hidden('oadh-producedure-id', null , array('id' => 'oadh-producedure-id', 'class' => 'form-control')) !!}
										{!! Form::hidden('oadh-producedure-news-id', null , array('id' => 'oadh-producedure-news-id', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-producedure-weapon', Lang::get('decima-oadh::media-monitoring.weaponType'), array('class' => 'control-label')) !!}
										{!! Form::autocomplete('oadh-producedure-weapon', $gunsTypes, array('class' => 'form-control'), 'oadh-producedure-weapon', 'oadh-producedure-weapon', null, 'fa-files-o') !!}
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-producedure-hypothesis-fact', Lang::get('decima-oadh::media-monitoring.hypothesisFact'), array('class' => 'control-label')) !!}
										{!! Form::textareacustom('oadh-producedure-hypothesis-fact', 1, 280 , array('id' => 'oadh-producedure-hypothesis-fact', 'class' => 'form-control')) !!}
									</div>
								</div>
							</div>
							{{-- <div class="form-group mg-hm">
								{!! Form::label('oadh-producedure-accident-type', Lang::get('decima-oadh::media-monitoring.accidentType'), array('class' => 'control-label')) !!}
								{!! Form::text('oadh-producedure-accident-type', null , array('id' => 'oadh-producedure-accident-type', 'class' => 'form-control')) !!}
							</div> --}}
						</div>
					</div>
				</fieldset>
				{!! Form::close() !!}
				</div>
				<div id="oadh-producedure-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-producedure-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-producedure-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => 'false')) !!}
					</div>
					<div id="oadh-producedure-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-producedure-btn-refresh', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						<div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-producedure-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-producedure-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div>
					</div>
					<div id="oadh-producedure-btn-group-2" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-producedure-btn-edit', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-producedure-btn-delete', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
					</div>
				</div>
				<div id='oadh-producedure-grid-section' class='app-grid oadh-producedure-grid-section collapse in' data-app-grid-id='oadh-producedure-grid'>
					{!!
						GridRender::setGridId("oadh-producedure-grid")
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-producedure'))
						->setGridOption('filename', Lang::get('decima-sale::sale-order-management.formOrderDetail'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('datatype', 'local')
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'pro.news_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhProducedureOnSelectRowEvent')
						->addColumn(array('index' => 'pro.id', 'name' => 'oadh_producedure_id', 'hidden' => true))
						->addColumn(array('index' => 'pro.news_id', 'name' => 'oadh_producedure_news_id', 'hidden' => true))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.placeType'), 'index' => 'pro.place', 'name' => 'oadh_producedure_place', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.weaponType'), 'index' => 'pro.weapon', 'name' => 'oadh_producedure_weapon', 'align' => 'center'))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.hypothesisFact'), 'index' => 'pro.hypothesis_fact', 'name' => 'oadh_producedure_hypothesis_fact', 'align' => 'center'))
						//->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.accidentType'), 'index' => 'pro.accident_type', 'name' => 'oadh_producedure_accident_type', 'align' => 'center'))
						->renderGrid();
					!!}
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="data-links">
				<div class="form-container form-container-followed-by-grid-section">
				{!! Form::open(array('id' => 'oadh-links-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<fieldset id="oadh-links-form-fieldset" disabled="disabled">
					<div id="oadh-links-section" class="row">
						<div class="col-md-6 form-division-line">
							<div class="form-group mg-hm">
								{!! Form::label('oadh-links-twitter-link', Lang::get('decima-oadh::media-monitoring.twitterLink'), array('class' => 'control-label')) !!}
								{!! Form::text('oadh-links-twitter-link', null , array('id' => 'oadh-links-twitter-link', 'class' => 'form-control')) !!}
								{!! Form::hidden('oadh-links-id', null , array('id' => 'oadh-links-id', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mg-hm">
								{!! Form::label('oadh-links-facebook-link', Lang::get('decima-oadh::media-monitoring.facebookLink'), array('class' => 'control-label')) !!}
								{!! Form::text('oadh-links-facebook-link', null , array('id' => 'oadh-links-facebook-link', 'class' => 'form-control')) !!}
							</div>
						</div>
					</div>
				</fieldset>
				{!! Form::close() !!}
				</div>
				<div id="oadh-links-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-links-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-links-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => 'false')) !!}
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="relationship-between-news">
				<div class="form-container form-container-followed-by-grid-section">
				{!! Form::open(array('id' => 'oadh-news-news-form', 'url' => URL::to('ucaoadh/transactions/media-monitoring'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<fieldset id="oadh-news-news-form-fieldset" disabled="disabled">
					<div id="oadh-news-news-section" class="row">
						<div class="col-md-6 form-division-line">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-news-news-related-news-label', Lang::get('decima-oadh::media-monitoring.title'), array('class' => 'control-label')) !!}
										{{--Form::autocomplete('oadh-news-news-related-news-label', array(), array('class' => 'form-control'), 'oadh-news-news-related-news-label', 'oadh-news-news-related-news-id', null, 'fa-files-o') --}}
										<div class="input-group">
											<span class="input-group-addon">
												<i class="fa fa-files-o"></i>
											</span>
											<input class="form-control ui-autocomplete-input" name="oadh-news-news-related-news-label" data-autocomplete-value-name="oadh-news-news-related-news-id" id="oadh-news-news-related-news-label" type="text" data-mg-required="" disabled="disabled">
											<span class="input-group-btn">
												<button class="btn btn-default" type="button" onclick="oadhNewsNewsShowSmtRows();">
													<i class="fa fa-table"></i>
												</button>
												<button class="btn btn-default" type="button" onclick="oadhNewsNewsClearSmtRows();">
													<i class="fa fa-ban"></i>
												</button>
											</span>
										</div>
										{!! Form::hidden('oadh-news-news-related-news-id', null , array('id' => 'oadh-news-news-related-news-id', 'class' => 'form-control')) !!}
										{!! Form::hidden('oadh-news-news-id', null , array('id' => 'oadh-news-news-id', 'class' => 'form-control')) !!}
										{!! Form::hidden('oadh-news-news-news-id', null , array('id' => 'oadh-news-news-news-id', 'class' => 'form-control', 'data-mg-clear-ignored' => '')) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
				{!! Form::close() !!}
				</div>
				<div id="oadh-news-news-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-news-news-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-news-news-btn-save', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => 'false')) !!}
					</div>
					<div id="oadh-news-news-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-news-news-btn-refresh', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						<div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-news-news-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-news-news-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div>
					</div>
					<div id="oadh-news-news-btn-group-2" class="btn-group btn-group-app-toolbar">
						{{-- {!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-news-news-btn-edit', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!} --}}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-news-news-btn-delete', 'class' => 'btn btn-default oadh-btn-tooltip decima-erp-tooltip', 'disabled' => '')) !!}
					</div>
				</div>
				<div id='oadh-news-news-grid-section' class='app-grid oadh-news-news-grid-section collapse in' data-app-grid-id='oadh-news-news-grid'>
					{!!
						GridRender::setGridId("oadh-news-news-grid")
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url', URL::to('ucaoadh/transactions/media-monitoring/grid-data-news-news'))
						->setGridOption('filename', Lang::get('Relación entre noticias'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('datatype', 'local')
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'nn.news_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhNewsNewsOnSelectRowEvent')
						->addColumn(array('index' => 'nn.id', 'name' => 'oadh_news_news_id', 'hidden' => true))
						->addColumn(array('index' => 'nn.news_id', 'name' => 'oadh_news_news_related_news_id', 'hidden' => true))
						->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.title'), 'index' => 'n.title', 'name' => 'oadh_news_news_related_news_label', 'align' => 'center'))
						->renderGrid();
					!!}
				</div>
			</div>
		</div>
	</div>
</div>
@include('decima-file::file-viewer')
<div id='oadh-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals('oadh-', $appInfo['id']) !!}
</div>
@include('decima-file::file-uploader')
@include('layouts.search-modal-table')
<div id='oadh-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-delete-message" data-default-label="{{ Lang::get('decima-oadh::media-monitoring.deleteMessageConfirmation') }}">{{ Lang::get('decima-oadh::media-monitoring.deleteMessageConfirmation') }}</p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
