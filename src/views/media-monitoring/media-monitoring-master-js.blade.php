<script type="text/javascript">

var oadhDigitalMedias = {!! json_encode($digitalMedias) !!};
var oadhSections = {!! json_encode($sections) !!};
var oadhNoteLocations = {!! json_encode($noteLocations) !!};
var oadhJournalisticGenres = {!! json_encode($journalisticGenres) !!};
var oadhTracing = {!! json_encode($humanRights) !!};
var oadhDepartment = {!! json_encode($departments) !!};
var oadhRights = {!! json_encode($rights) !!};
var oadhTopics = {!! json_encode($topics) !!};
var oadhViolatedFacts = {!! json_encode($violatedFacts) !!};
var oadhMunicipality = {!! json_encode($municipality) !!};
var oadhNews = "";

function loadNews()
{
	$.ajax(
	{
		type: 'POST',
		data: JSON.stringify({'_token':$('#app-token').val()}),
		dataType : 'json',
		url: $('#oadh-form').attr('action') + '/news',
		error: function (jqXHR, textStatus, errorThrown)
		{
			handleServerExceptions(jqXHR, 'oadh-btn-toolbar', false);
		},
		success:function(json)
		{
			// $.grep(organizationEvents, function (element, index)
			// {
			// 	element.total = $.fmatter.NumberFormat(element.total.toFixed(2), $.fn.jqMgVal.defaults.validators.money.formatter);
			// 	element.tax = $.fmatter.NumberFormat(element.tax.toFixed(2), $.fn.jqMgVal.defaults.validators.money.formatter);
			// 	element.total_with_tax = $.fmatter.NumberFormat(element.total_with_tax.toFixed(2), $.fn.jqMgVal.defaults.validators.money.formatter);
			// });
			oadhNews = json;
		}
	});
}

function oadhOnSelectRowEvent()
{
	var id = $('#oadh-grid').getSelectedRowId('oadh_id'), rowData = $('#oadh-grid').getRowData($('#oadh-grid').jqGrid('getGridParam', 'selrow'));

	if($('#oadh-journals').attr('data-journalized-id') != id)
	{
		cleanFiles('oadh-');

		// getAppJournals('oadh-', 'firstPage', id);
		// getElementFiles('oadh-', id);

		// $('#oadh-front-human-right-grid, #oadh-front-context-grid, #oadh-front-source-grid, #oadh-front-victim-grid, #oadh-front-victimizer-grid, #oadh-front-producedure-grid, #oadh-front-news-news-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');
	}

	$('#oadh-btn-group-2').enableButtonGroup();
}

function oadhOnLoadCompleteEvent()
{
	// var id = $('#oadh-grid').getSelectedRowId('oadh_id');
	//
	// $('#oadh-front-human-right-grid').jqGrid('clearGridData');
	//
	// $('#oadh-front-human-right-grid').jqGrid('footerData','set', {
	// 	'news_id': id,
	// });
}

function oadhEnableDetails(id)
{

	//Setteando el id en los lugares correspondientes
	$('#oadh-id').val(id);
	$('#oadh-human-right-news-id').val(id);
	$('#oadh-context-news-id').val(id);
	$('#oadh-source-news-id').val(id);
	$('#oadh-victim-news-id').val(id);
	$('#oadh-victimizer-news-id').val(id);
	$('#oadh-producedure-news-id').val(id);
	$('#oadh-news-news-news-id').val(id);
	$('#oadh-links-id').val(id);

	//PLantilla para demas pantallas

	$('#oadh-human-right-btn-toolbar').disabledButtonGroup();
	$('#oadh-human-right-btn-group-1').enableButtonGroup();
	$('#oadh-human-right-btn-group-3').enableButtonGroup();

	//Context

	$('#oadh-context-btn-toolbar').disabledButtonGroup();
	$('#oadh-context-btn-group-1').enableButtonGroup();
	$('#oadh-context-btn-group-3').enableButtonGroup();

	//Source

	$('#oadh-source-btn-toolbar').disabledButtonGroup();
	$('#oadh-source-btn-group-1').enableButtonGroup();
	$('#oadh-source-btn-group-3').enableButtonGroup();

	//Victim

	$('#oadh-victim-btn-toolbar').disabledButtonGroup();
	$('#oadh-victim-btn-group-1').enableButtonGroup();
	$('#oadh-victim-btn-group-3').enableButtonGroup();

	//Victimizer
	$('#oadh-victimizer-btn-toolbar').disabledButtonGroup();
	$('#oadh-victimizer-btn-group-1').enableButtonGroup();
	$('#oadh-victimizer-btn-group-3').enableButtonGroup();

	$('#oadh-producedure-btn-toolbar').disabledButtonGroup();
	$('#oadh-producedure-btn-group-1').enableButtonGroup();
	$('#oadh-producedure-btn-group-3').enableButtonGroup();

	$('#oadh-news-news-btn-toolbar').disabledButtonGroup();
	$('#oadh-news-news-btn-group-1').enableButtonGroup();
	$('#oadh-news-news-btn-group-3').enableButtonGroup();

	$('#oadh-links-btn-group-3').enableButtonGroup();

	//Liberando fielsets
	$('#oadh-human-right-form-fieldset').removeAttr('disabled');
	$('#oadh-context-form-fieldset').removeAttr('disabled');
	$('#oadh-source-form-fieldset').removeAttr('disabled');
	$('#oadh-victim-form-fieldset').removeAttr('disabled');
	$('#oadh-victimizer-form-fieldset').removeAttr('disabled');
	$('#oadh-producedure-form-fieldset').removeAttr('disabled');
	$('#oadh-links-form-fieldset').removeAttr('disabled');
	$('#oadh-news-news-form-fieldset').removeAttr('disabled');
	// $('#oadh-producedure-form-fieldset').removeAttr('disabled');
}

function oadhSearchEvents()
{
	var value = $('#oadh-edit-modal-search-box').val().toLowerCase();
	var returnedData

	if (value == '')
	{
		returnedData = oadhNews.slice(0,10);
	}
	else
	{
		returnedData = $.grep(oadhNews, function (element, index)
		{
				return String(element.code).toLowerCase().indexOf(value) > -1
						|| String(element.title).toLowerCase().indexOf(value) > -1
						|| String(element.date).indexOf(value) > -1
						|| String(element.time).indexOf(value) > -1
						|| String(element.digital_media_label).toLowerCase().indexOf(value) > -1
						|| String(element.facebook_link).toLowerCase().indexOf(value) > -1
						|| String(element.journalistic_genre).toLowerCase().indexOf(value) > -1
						|| String(element.new_link).toLowerCase().indexOf(value) > -1
						|| String(element.note_locations).toLowerCase().indexOf(value) > -1
						|| String(element.page).indexOf(value) > -1
						|| String(element.section_label).toLowerCase().indexOf(value) > -1
						|| String(element.summary).toLowerCase().indexOf(value) > -1
						|| String(element.twitter_link).toLowerCase().indexOf(value) > -1
				;
		});
	}

	oadhFillNewsTable(returnedData);
}

function oadhFillNewsTable(news)
{
	var table = $('#oadh-edit-modale-template');

	$('#oadh-edit-modal-body').html(table.html());

	if(!news)
	{
		news = oadhNews.slice(0,10);
	}

	$.each(news, function(row, event)
	{
		var rowElement = document.createElement('tr');

		var columnElement;

		$.each(event, function(key, value)
		{
			if (key == 'id' || key == 'digital_media_id' || key == 'facebook_link' || key == 'new_link' || key == 'page' || key == 'section_id' || key == 'summary' || key == 'time' || key == 'twitter_link' || key == 'note_locations')
			{
				var hidden = document.createAttribute('hidden');
				hidden.value = true;

				var text = document.createTextNode(value);

				var columnElement = document.createElement('td');
				columnElement.setAttributeNode(hidden);
				columnElement.appendChild(text);
				$(rowElement).append(columnElement);
			}
			else
			{
				var columnElement = $(document.createElement('td'));
				columnElement.html(value);
				$(rowElement).append(columnElement);
			}

		});
		$('#oadh-edit-modal-content-section').find('table').append(rowElement);
	});
}

$(document).ready(function()
{
	// loadNews()
	// loadSmtRows('oadhSmtRows', $('#oadh-form').attr('action') + '/smt-rows');

	$('#oadh-digital-media-label').on('autocompleteselect', function( event, ui )
	{
		if($('#oadh-digital-media-label').attr('data-id') != ui.item.value)
		{
			$('#oadh-digital-media-label').attr('data-id', ui.item.value);
			$('#oadh-section-label').autocomplete('option', 'source', ui.item.sections);
		}
	});

	$('#oadh-human-right-justification-label-container, #oadh-summary-label-container').removeClass('clearfix');
	$('.oadh-btn-tooltip').tooltip();

  //Agregar los demas forms
	$('#oadh-form, #oadh-human-right-form, #oadh-context-form, #oadh-source-form, #oadh-victim-form, #oadh-victimizer-form, #oadh-producedure-form, #oadh-links-form, #oadh-news-news-form').jqMgVal('addFormFieldsValidations');

	$('#oadh-file-uploader-modal').on('hidden.bs.modal', function (e)
	{
		 dataFiles = $.parseJSON($("#oadh-file-uploader-modal").attr('data-files'));

		 if(dataFiles.length > 0)
		 {
			 getElementFiles('oadh-', $('#oadh-grid').getSelectedRowId('oadh_id'));
		 }
	});

	$('#oadh-grid-section').on('hidden.bs.collapse', function ()
	{
		$($('#oadh-journals-section').attr('data-target-id')).collapse('show');
	});

	// $('#oadh-journals-section').on('hidden.bs.collapse', function ()
	// {
	// 	$($(this).attr('data-target-id')).collapse('show');
	// });

	$('#oadh-form-section').on('hidden.bs.collapse', function ()
	{
		$('#oadh-grid-section').collapse('show');
		// $('#oadh-journals-section').collapse('show');

		$('#oadh-filters').show();
	});

	$('#oadh-form-section').on('shown.bs.collapse', function ()
	{
		$('#oadh-date').focus();
	});

	$('#oadh-new-link').focusout(function()
	{
		$('#oadh-summary').focus();
	});

	$('#oadh-summary').focusout(function()
	{
		$('#oadh-btn-save').focus();
	});

	$('#oadh-btn-new').click(function()
	{
		if($(this).hasAttr('disabled'))
		{
			return;
		}

    $('#oadh-form, #oadh-detail-form').jqMgVal('clearForm');

		$('#oadh-btn-toolbar, #oadh-btn-toolbar, #oadh-btn-toolbar').disabledButtonGroup();

		$('#oadh-btn-group-3').enableButtonGroup();

		oadhDisabledHumanRightForm();
		oadhDisabledProducedureForm()
		oadhDisabledSourceForm();
		oadhDisabledVictimForm();
		oadhDisabledVictimizerForm();
		oadhDisabledHumanRightForm();
		oadhDisabledContextForm();

		$('#oadh-links-form-fieldset').attr('disabled', 'disabled');
		$('#oadh-news-news-form-fieldset').attr('disabled', 'disabled');
		$('#oadh-digital-media-label').attr('data-id', '');

		$('#oadh-links-form').jqMgVal('clearForm');
		$('#oadh-news-news-form').jqMgVal('clearForm');

		$('#oadh-btn-new, #oadh-btn-delete').removeAttr('disabled');
		// $('#oadh-btn-new, #oadh-btn-edit, #oadh-btn-delete').removeAttr('disabled');
		$('#oadh-digital-media-label, #oadh-digital-media-label-show-all-button').removeAttr('disabled');


		if(!$('#oadh-form-section').is(":visible"))
		{
			// $('#oadh-grid-section, #oadh-journals-section').collapse('hide');
			$('#oadh-grid-section').collapse('hide');
			$('#oadh-journals-section').attr('data-target-id', '#oadh-form-section');
		}
		else
		{
			$('#oadh-context-grid').jqGrid('clearGridData');
			$('#oadh-human-right-grid').jqGrid('clearGridData');
			$('#oadh-producedure-grid').jqGrid('clearGridData');
			$('#oadh-source-grid').jqGrid('clearGridData');
			$('#oadh-victim-grid').jqGrid('clearGridData');
			$('#oadh-victimizer-grid').jqGrid('clearGridData');

			$('#oadh-context-news-id').val(-1);
			$('#oadh-human-right-news-id').val(-1);
			$('#oadh-producedure-news-id').val(-1);
			$('#oadh-source-news-id').val(-1);
			$('#oadh-victim-news-id').val(-1);
			$('#oadh-victimizer-news-id').val(-1);
		}

		cleanJournals('oadh-');
		cleanFiles('oadh-');

		$('#oadh-filters').hide();

		$('.decima-erp-tooltip').tooltip('hide');

		$('#oadh-form-new-title').removeClass('hidden');
		$('#oadh-form-edit-title').addClass('hidden');
	});

	$('#oadh-btn-refresh').click(function()
	{
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-btn-toolbar').disabledButtonGroup();
		$('#oadh-btn-group-1').enableButtonGroup();
		$('#oadh-front-context-grid, #oadh-front-human-right-grid, #oadh-front-producedure-grid, #oadh-front-source-grid, #oadh-front-victim-grid, #oadh-front-victimizer-grid, #oadh-front-human-right-grid, #oadh-front-producedure-grid,  #oadh-front-news-news-grid, #oadh-front-source-grid, #oadh-front-victim-grid, #oadh-front-victimizer-grid').jqGrid('clearGridData');

		if($('#oadh-journals-section').attr('data-target-id') == '' || $('#oadh-journals-section').attr('data-target-id') == '#oadh-form-section')
		{
			$('#oadh-grid').trigger('reloadGrid');
			cleanJournals('oadh-');
		}
		else
		{

		}
	});

	$('#oadh-btn-export-xls').click(function()
	{
		if($('#oadh-journals-section').attr('data-target-id') == '')
		{
			$('#oadh-gridXlsButton').click();
		}
	});

	$('#oadh-btn-export-csv').click(function()
	{
		if($('#oadh-journals-section').attr('data-target-id') == '')
		{
			$('#oadh-gridCsvButton').click();
		}
	});

	$('#oadh-btn-save').click(function()
	{
		var url = $('#oadh-form').attr('action'), action = 'new';

		$('.decima-erp-tooltip').tooltip('hide');

		if($('#oadh-journals-section').attr('data-target-id') == '#oadh-form-section')
		{
			if(!$('#oadh-form').jqMgVal('isFormValid'))
			{
				return;
			}

			if($('#oadh-id').isEmpty())
			{
				url = url + '/create-master';
			}
			else
			{
				url = url + '/update-master';
				action = 'edit';
			}

			data = $('#oadh-form').formToObject('oadh-');
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(data),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					// oadhNews = json.news;
					// oadhNewsNewsRelatedLabelArrayData = json.news_related;
					// $('#oadh-news-news-related-news-label').autocomplete('option', 'source', json.news_related);
					// $('#oadh-news-news-related-news-label').attr('data-id', '');
					$('#oadh-digital-media-label, #oadh-digital-media-label-show-all-button').attr('disabled', 'disabled');

					if($('#oadh-journals-section').attr('data-target-id') == '#oadh-form-section')
					{
						if(action == 'new')
						{
							$('#oadh-id').val(json.id);
							$('#oadh-code').val(json.code);

							oadhEnableDetails(json.id);

							// $('#oadh-detail-master-id').val(json.id);
							// $('.oadh-number').html('#' + json.number);
						}

						if(action == 'edit')
						{
							$('#oadh-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
						}

						$('#oadh-form-new-title').addClass('hidden');
						$('#oadh-form-edit-title').removeClass('hidden');

						$('#oadh-form').jqMgVal('clearContextualClasses');

						$('#oadh-detail-btn-toolbar').disabledButtonGroup();

						$('#oadh-detail-btn-group-1').enableButtonGroup();
						$('#oadh-detail-btn-group-3').enableButtonGroup();

						// $('#oadh-btn-new, #oadh-btn-edit, #oadh-btn-upload, #oadh-btn-show-files, #oadh-btn-show-history').removeAttr('disabled');
						$('#oadh-btn-new, #oadh-btn-upload, #oadh-btn-show-files, #oadh-btn-show-history').removeAttr('disabled');
						$('#oadh-detail-form-fieldset').removeAttr('disabled');
					}
					else
					{
						// $('#oadh-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
						// $('#oadh-form').showAlertAsFirstChild('alert-success', json.success, 6000)
					}


					// $('#oadh-human-right-form-fieldset').removeAttr('disabled');
					// $('#oadh-context-form-fieldset').removeAttr('disabled');
					// $('#oadh-source-form-fieldset').removeAttr('disabled');
					// $('#oadh-victim-form-fieldset').removeAttr('disabled');
					// $('#oadh-victimizer-form-fieldset').removeAttr('disabled');
					// $('#oadh-producedure-form-fieldset').removeAttr('disabled');
					// $('#oadh-producedure-form-fieldset').removeAttr('disabled');

					// $('#oadh-links-form-fieldset').removeAttr('disabled');
				}
				else if(json.info)
				{
					if($('#oadh-journals-section').attr('data-target-id') == '#oadh-form-section')
					{
						$('#oadh-form').showAlertAsFirstChild('alert-info', json.info, 12000);
					}
					else
					{
						// $('#oadh-form').showAlertAsFirstChild('alert-info', json.info, 12000);
					}
				}

				if(action == 'new' && !empty(json.smtRows))
				{
					// addSmtRow('oadhSmtRows', Object.keys(json.smtRows)[0], Object.values(json.smtRows)[0]);
				}

				if(action == 'edit' && !empty(json.smtRows))
				{
					// updateSmtRow('oadhSmtRows', Object.keys(json.smtRows)[0], Object.values(json.smtRows)[0]);
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-detail-name').focus();
			}
		});
	});

	$('#oadh-links-btn-save').click(function()
	{
		var url = $('#oadh-links-form').attr('action'), action = 'new';

		$('.decima-erp-tooltip').tooltip('hide');

		if($('#oadh-journals-section').attr('data-target-id') == '#oadh-form-section')
		{
			if(!$('#oadh-form').jqMgVal('isFormValid'))
			{
				return;
			}

			url = url + '/update-master';
			action = 'edit';

			data = $('#oadh-links-form').formToObject('oadh-links-');
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(data),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-links-btn-toolbar').disabledButtonGroup();
					$('#oadh-links-btn-group-1').enableButtonGroup();
					$('#oadh-links-btn-group-3').enableButtonGroup();

					$('#oadh-links-form').showAlertAsFirstChild('alert-success', json.success, 5000);
				}
				else if(json.info)
				{
						$('#oadh-links-form').showAlertAsFirstChild('alert-info', json.info, 12000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});

	// $('#oadh-smt-btn-refresh').click(function()
	// {
	// 	loadSmtRows('oadhSmtRows', $('#oadh-form').attr('action') + '/smt-rows', '', true, true);
	// });

	// $('#oadh-smt-btn-select').click(function()
	// {
	// 	var rowData = $('#oadh-smt').getSelectedSmtRow();

	// 	if(empty(rowData))
	// 	{
	// 		$('.btn-modal-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 2000);
	// 		return;
	// 	}

	// 	$('.decima-erp-tooltip').tooltip('hide');

	// 	$('#oadh-btn-toolbar').disabledButtonGroup();

	// 	$('#oadh-btn-group-3').enableButtonGroup();

	// 	$('#oadh-btn-new, #oadh-btn-delete, #oadh-btn-upload, #oadh-btn-show-files, #oadh-btn-show-history').removeAttr('disabled');
	// 	// $('#oadh-btn-new, #oadh-btn-edit, #oadh-btn-delete, #oadh-btn-upload, #oadh-btn-show-files, #oadh-btn-show-history').removeAttr('disabled');

	// 	$('#oadh-form-new-title').addClass('hidden');
	// 	$('#oadh-form-edit-title').removeClass('hidden');

	// 	$('#oadh-btn-new').click();

	// 	populateFormFields(rowData);

	// 	$('#oadh-date').val($.datepicker.formatDate(lang.dateFormat, new Date(rowData.oadh_date)));

	// 	$('#oadh-smt').modal('hide');
	// 	$('#oadh-id').val(rowData.oadh_id);

	// 	$('#oadh-digital-media-label, #oadh-digital-media-label-show-all-button').attr('disabled', 'disabled');

	// 	$('#oadh-journals-section').attr('data-target-id', '#oadh-form-section');

	// 	$('#oadh-filters').hide();

	// 	$('#oadh-journals-section').attr('data-target-id', '#oadh-form-section');

	// 	oadhEnableDetails(rowData.oadh_id);

	// 	$('#oadh-human-right-btn-refresh').click();
	// 	$('#oadh-context-btn-refresh').click();
	// 	$('#oadh-source-btn-refresh').click();
	// 	$('#oadh-victim-btn-refresh').click();
	// 	$('#oadh-victimizer-btn-refresh').click();
	// 	$('#oadh-producedure-btn-refresh').click();
	// 	$('#oadh-news-news-btn-refresh').click();
	// });

	$('#oadh-btn-view').click(function()
	{
		var rowData, id;

		$('.decima-erp-tooltip').tooltip('hide');

		if($(this).hasAttr('disabled'))
		{
			return;
		}

		if(!$('#oadh-grid').isRowSelected())
		{
			$('#oadh-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
			return;
		}
		$('#oadh-btn-show-files').click();
		$('#oadh-front-human-right-grid, #oadh-front-context-grid, #oadh-front-source-grid, #oadh-front-victim-grid, #oadh-front-victimizer-grid, #oadh-front-producedure-grid, #oadh-front-news-news-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + $('#oadh-grid').getSelectedRowId('oadh_id') + "'}]}"}}).trigger('reloadGrid');
	});

	$('#oadh-btn-edit').click(function()
	{
		var rowData;

		if($('#oadh-grid').is(":visible"))
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-btn-toolbar').disabledButtonGroup();

			$('#oadh-btn-group-3').enableButtonGroup();

			$('#oadh-digital-media-label, #oadh-digital-media-label-show-all-button').attr('disabled', 'disabled');

			// $('#oadh-btn-new, #oadh-btn-edit, #oadh-btn-delete, #oadh-btn-upload, #oadh-btn-show-files, #oadh-btn-show-history').removeAttr('disabled');
			$('#oadh-btn-new, #oadh-btn-delete, #oadh-btn-upload, #oadh-btn-show-files, #oadh-btn-show-history').removeAttr('disabled');

			if($('#oadh-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-grid').isRowSelected())
				{
					$('#oadh-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				$('#oadh-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-grid').getRowData($('#oadh-grid').jqGrid('getGridParam', 'selrow'));
				populateFormFields(rowData);
				$('#oadh-digital-media-label').setAutocompleteLabel(rowData.oadh_digital_media_id);
			}
			else
			{

			}

			$('#oadh-filters').hide();

			$('#oadh-journals-section').attr('data-target-id', '#oadh-form-section');

			$('#oadh-grid-section').collapse('hide');
			// $('#oadh-journals-section').collapse('hide');

			//Plantilla para demas pantallas
			// $('#oadh-detail-btn-refresh').click();
			// $('#oadh-detail-btn-toolbar').disabledButtonGroup();
			// $('#oadh-detail-btn-group-1').enableButtonGroup();
			// $('#oadh-detail-btn-group-3').enableButtonGroup();

			oadhEnableDetails(rowData.oadh_id);

			$('#oadh-human-right-btn-refresh').click();
			$('#oadh-context-btn-refresh').click();
			$('#oadh-source-btn-refresh').click();
			$('#oadh-victim-btn-refresh').click();
			$('#oadh-victimizer-btn-refresh').click();
			$('#oadh-producedure-btn-refresh').click();
			$('#oadh-news-news-btn-refresh').click();
		}
		else
		{
			// $('#oadh-smt').createTable('oadh-grid', 'oadhSmtRows', 10);
			// $('#oadh-smt').modal('show');
		}
	});

	$('#oadh-btn-upload').click(function()
	{
		var rowData, folder = $(this).attr('data-folder');

		if($(this).hasAttr('disabled'))
		{
			return;
		}

		$('.decima-erp-tooltip').tooltip('hide');

		if(!$('#oadh-grid').isRowSelected())
		{
			$('#oadh-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		rowData = $('#oadh-grid').getRowData($('#oadh-grid').jqGrid('getGridParam', 'selrow'));

		folder = folder + '/' + rowData.oadh_code;

		openUploader('oadh-', rowData.oadh_id, folder);
	});

	$('#oadh-btn-delete').click(function()
	{
		var rowData;

		if($(this).hasAttr('disabled'))
		{
			return;
		}

		if($('#oadh-journals-section').attr('data-target-id') == '')
		{
			if($('#oadh-grid').is(":visible"))
			{
				if(!$('#oadh-grid').isRowSelected())
				{
					$('#oadh-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

			}
		}

		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-modal-delete').modal('show');
	});

	$('#oadh-btn-show-files').click(function()
	{
		var rowData, id;

		$('.decima-erp-tooltip').tooltip('hide');

		if($(this).hasAttr('disabled'))
		{
			return;
		}

		if($('#oadh-grid').is(":visible"))
		{
			if(!$('#oadh-grid').isRowSelected())
			{
				$('#oadh-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
				return;
			}

			rowData = $('#oadh-grid').getRowData($('#oadh-grid').jqGrid('getGridParam', 'selrow'));
			id = rowData.oadh_id;
		}
		else
		{
			id = $('#oadh-id').val();
		}

		if($('#oadh-btn-file-modal-delete').attr('data-system-reference-id') != id)
		{
			getElementFiles('oadh-', id);
		}

		$.scrollTo({top: $('#oadh-file-viewer').offset().top - 100, left:0});
	});

	$('#oadh-btn-show-history').click(function()
	{
		var rowData, id;

		$('.decima-erp-tooltip').tooltip('hide');

		if($(this).hasAttr('disabled'))
		{
			return;
		}

		if($('#oadh-grid').is(":visible"))
		{
			if(!$('#oadh-grid').isRowSelected())
			{
				$('#oadh-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
				return;
			}

			rowData = $('#oadh-grid').getRowData($('#oadh-grid').jqGrid('getGridParam', 'selrow'));
			id = rowData.oadh_id;
		}
		else
		{
			id = $('#oadh-id').val();
		}

		if($('#oadh-journals').attr('data-journalized-id') != id)
		{
			getAppJournals('oadh-', 'firstPage', id);
		}

		$.scrollTo({top: $('#oadh-journals').offset().top - 100, left:0});
	});

	$('#oadh-btn-modal-delete').click(function()
	{
		var id, url;

		url = $('#oadh-form').attr('action') + '/delete-master';

		if($('#oadh-grid').is(":visible"))
		{
			if(!$('#oadh-grid').isRowSelected())
			{
				$('#oadh-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
				return;
			}

			rowData = $('#oadh-grid').getRowData($('#oadh-grid').jqGrid('getGridParam', 'selrow'));
			id = rowData.oadh_id;
		}
		else
		{
			id = $('#oadh-id').val();
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token':$('#app-token').val(),
					'id': id
				}
			),
			dataType : 'json',
			url:  url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-btn-toolbar', false);
				$('#oadh-modal-delete').modal('hide');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					// oadhNews = json.news;
					// oadhNewsNewsRelatedLabelArrayData = json.news_related;
					// $('#oadh-news-news-related-news-label').autocomplete('option', 'source', json.news_related);
					// $('#oadh-news-news-related-news-label').attr('data-id', '');
					if($('#oadh-grid').is(":visible"))
					{
						$('#oadh-btn-refresh').click();
						$("#oadh-btn-group-2").disabledButtonGroup();
					}
					else
					{
						$('#oadh-btn-new').click();
					}

					$('#oadh-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
				}

				if(json.info)
				{
					$('#oadh-btn-refresh').click();
					$("#oadh-btn-group-2").disabledButtonGroup();
					$('#oadh-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
				}

				if(!empty(json.smtRowId))
				{
					// console.log(json.smtRowId)
					// deleteSmtRow('oadhSmtRows', json.smtRowId);
				}

				$('#oadh-modal-delete').modal('hide');

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});

	$('#oadh-btn-close').click(function()
	{
		if($(this).hasAttr('disabled'))
		{
			return;
		}

		if($('#oadh-journals-section').attr('data-target-id') == '#oadh-form-section')
		{
			$('#oadh-form-new-title').addClass('hidden');
			$('#oadh-form-edit-title').addClass('hidden');
			$('#oadh-btn-refresh').click();

			$('#oadh-context-news-id').val(-1);
			$('#oadh-human-right-news-id').val(-1);
			$('#oadh-producedure-news-id').val(-1);
			$('#oadh-source-news-id').val(-1);
			$('#oadh-victim-news-id').val(-1);
			$('#oadh-victimizer-news-id').val(-1);

			$('#oadh-digital-media-label').attr('data-id', '');

			$('#oadh-digital-media-label, #oadh-digital-media-label-show-all-button').removeAttr('disabled');

			$('#oadh-context-grid, #oadh-human-right-grid, #oadh-producedure-grid, #oadh-source-grid, #oadh-victim-grid, #oadh-victimizer-grid, #oadh-human-right-grid, #oadh-producedure-grid, #oadh-source-grid, #oadh-victim-grid, #oadh-victimizer-grid').jqGrid('clearGridData');

			$('#oadh-form, #oadh-context-form, #oadh-human-form, #oadh-producedure-form, #oadh-source-form, #oadh-victim-form, #oadh-victimizer-form').jqMgVal('clearForm');
			$('#oadh-form-section').collapse('hide');
		}
		else
		{

		}


		$('#oadh-btn-group-1').enableButtonGroup();
		$('#oadh-btn-group-3').disabledButtonGroup();
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-journals-section').attr('data-target-id', '');

		oadhDisabledHumanRightForm();
		oadhDisabledProducedureForm()
		oadhDisabledSourceForm();
		oadhDisabledVictimForm();
		oadhDisabledVictimizerForm();
		oadhDisabledHumanRightForm();
		oadhDisabledContextForm();
	});

	$('#oadh-btn-clear-filter').click(function()
	{
		$('#oadh-front-context-grid, #oadh-front-human-right-grid, #oadh-front-producedure-grid,#oadh-front-news-news-grid, #oadh-front-source-grid, #oadh-front-victim-grid, #oadh-front-victimizer-grid, #oadh-front-human-right-grid, #oadh-front-producedure-grid, #oadh-front-news-news-grid, #oadh-front-source-grid, #oadh-front-victim-grid, #oadh-front-victimizer-grid').jqGrid('clearGridData');

		$('#oadh-filters-form').find('.tokenfield').find('.close').click();

		$('#oadh-filters-form').jqMgVal('clearForm');

		$('#oadh-btn-filter').click();
	});

	$('#oadh-btn-filter').click(function()
	{
		var filters = [];

		$(this).removeClass('btn-default').addClass('btn-warning');

		if(!$('#oadh-filters-form').jqMgVal('isFormValid'))
		{
			return;
		}

		$('#oadh-filters-form').jqMgVal('clearContextualClasses');

		if(!$('#oadh-code-filter').isEmpty())
		{
			filters.push({'field':'n.code', 'op':'cn', 'data': $('#oadh-code-filter').val()});
		}

		if(!$('#oadh-title-filter').isEmpty())
		{
			filters.push({'field':'n.title', 'op':'cn', 'data': $('#oadh-title-filter').val()});
		}

		if($("#oadh-date-from-filter").val() != '__/__/____' && !$("#oadh-date-from-filter").isEmpty())
		{
			filters.push({'field':'n.date', 'op':'ge', 'data': $.datepicker.formatDate("yy-mm-dd", $("#oadh-date-from-filter").datepicker("getDate"))});
		}

		if($("#oadh-date-to-filter").val() != '__/__/____' && !$("#oadh-date-to-filter").isEmpty())
		{
			filters.push({'field':'n.date', 'op':'le', 'data': $.datepicker.formatDate("yy-mm-dd", $("#oadh-date-to-filter").datepicker("getDate"))});
		}

		if(!$("#oadh-time-from-filter").isEmpty())
		{
			filters.push({'field':'n.time', 'op':'ge', 'data':  $("#oadh-time-from-filter").val()});
		}

		if(!$("#oadh-time-to-filter").isEmpty())
		{
			filters.push({'field':'n.time', 'op':'le', 'data':  $("#oadh-time-to-filter").val()});
		}

		if(!$("#oadh-digital-media-filter").isEmpty())
		{
			filters.push({'field':'n.digital_media_id', 'op':'in', 'data': $("#oadh-digital-media-filter").val()});
		}

		if(!$("#oadh-section-filter").isEmpty())
		{
			filters.push({'field':'n.section_id', 'op':'in', 'data': $("#oadh-section-filter").val()});
		}

		if(!$("#oadh-note-location-filter").isEmpty())
		{
			filters.push({'field':'n.note_locations', 'op':'in', 'data': $("#oadh-note-location-filter").val()});
		}

		if(!$("#oadh-journalistic-genre-filter").isEmpty())
		{
			filters.push({'field':'n.journalistic_genre', 'op':'in', 'data': $("#oadh-journalistic-genre-filter").val()});
		}

		if(!$('#oadh-summary-filter').isEmpty())
		{
			filters.push({'field':'n.summary', 'op':'cn', 'data': $('#oadh-summary-filter').val()});
		}

		if(!$('#oadh-source-filter').isEmpty())
		{
			filters.push({'field':'source', 'op':'cn', 'data': $('#oadh-source-filter').val()});
		}

		if(!$('#oadh-instance-filter').isEmpty())
		{
			filters.push({'field':'instance', 'op':'cn', 'data': $('#oadh-instance-filter').val()});
		}

		if(!$('#oadh-responsable-filter').isEmpty())
		{
			filters.push({'field':'responsable', 'op':'cn', 'data': $('#oadh-responsable-filter').val()});
		}

		if(!$('#oadh-weapon-filter').isEmpty())
		{
			filters.push({'field':'weapon', 'op':'cn', 'data': $('#oadh-weapon-filter').val()});
		}

		if(!$('#oadh-place-filter').isEmpty())
		{
			filters.push({'field':'place', 'op':'cn', 'data': $('#oadh-place-filter').val()});
		}

		if(!$('#oadh-hipotesis-filter').isEmpty())
		{
			filters.push({'field':'hypothesis_fact', 'op':'cn', 'data': $('#oadh-hipotesis-filter').val()});
		}

		if(!$('#oadh-event-filter').isEmpty())
		{
			filters.push({'field':'accident_type', 'op':'cn', 'data': $('#oadh-event-filter').val()});
		}

		if(!$('#oadh-context-filter').isEmpty())
		{
			filters.push({'field':'context', 'op':'cn', 'data': $('#oadh-context-filter').val()});
		}

		if(!$('#oadh-description-filter').isEmpty())
		{
			filters.push({'field':'summary', 'op':'cn', 'data': $('#oadh-description-filter').val()});
		}


		if(!$('#oadh-victim-name-filter').isEmpty())
		{
			filters.push({'field':'vic_first_name', 'op':'eq', 'data': 'V' + '|' + $('#oadh-victim-name-filter').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'V'});
		}

		if(!$('#oadh-victim-last-name-filter').isEmpty())
		{
			filters.push({'field':'vic_last_name', 'op':'eq', 'data': 'V' + '|' + $('#oadh-victim-last-name-filter').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'V'});
		}

		if(!$("#oadh-victim-age-from-filter").isEmpty() && !$("#oadh-victim-age-to-filter").isEmpty())
		{
			filters.push({'field':'vic_age', 'op':'ge', 'data':  'V' + '|' + $("#oadh-victim-age-from-filter").val() + '|' + $("#oadh-victim-age-to-filter").val()});
		}

		if(!$("#oadh-victim-age-from-filter").isEmpty() && $("#oadh-victim-age-to-filter").isEmpty())
		{
			filters.push({'field':'vic_age_from', 'op':'le', 'data':  'V' + '|' + $("#oadh-victim-age-from-filter").val()});
		}

		if($("#oadh-victim-age-from-filter").isEmpty() && !$("#oadh-victim-age-to-filter").isEmpty())
		{
			filters.push({'field':'vic_age_to', 'op':'le', 'data':  'V' + '|' + $("#oadh-victim-age-to-filter").val()});
		}

		if(!$('#oadh-victim-profesion-filter').isEmpty())
		{
			filters.push({'field':'vic_profesion', 'op':'eq', 'data': 'V' + '|' + $('#oadh-victim-profesion-filter').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'V'});
		}

		if(!$('#oadh-victim-gender-filter').isEmpty())
		{
			filters.push({'field':'vic_gender', 'op':'eq', 'data': 'V' + '|' + $('#oadh-victim-gender-filter').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'V'});
		}

		if($('#oadh-victim-type-filter-p').is(':checked'))
		{
			filters.push({'field':'vic_subtype', 'op':'eq', 'data': 'V' + '|' + $('#oadh-victim-type-filter-p').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'V'});
		}

		if($('#oadh-victim-type-filter-s').is(':checked'))
		{
			filters.push({'field':'vic_subtype', 'op':'eq', 'data': 'V' + '|' + $('#oadh-victim-type-filter-s').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'V'});
		}

		if(!$('#oadh-victimizer-name-filter').isEmpty())
		{
			filters.push({'field':'vic_first_name', 'op':'eq', 'data': 'R' + '|' + $('#oadh-victimizer-name-filter').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'R'});
		}

		if(!$('#oadh-victimizer-last-name-filter').isEmpty())
		{
			filters.push({'field':'vic_last_name', 'op':'eq', 'data': 'R' + '|' + $('#oadh-victimizer-last-name-filter').val()});
			// filters.push({'field':'vic_type', 'op':'eq', 'data': 'R'});
		}

		if(!$("#oadh-victimizer-age-from-filter").isEmpty() && !$("#oadh-victimizer-age-to-filter").isEmpty())
		{
			filters.push({'field':'vic_age', 'op':'ge', 'data':  'R' + '|' + $("#oadh-victimizer-age-from-filter").val() + '|' + $("#oadh-victimizer-age-to-filter").val()});
		}

		if(!$("#oadh-victimizer-age-from-filter").isEmpty() && $("#oadh-victimizer-age-to-filter").isEmpty())
		{
			filters.push({'field':'vic_age_from', 'op':'le', 'data':  'R' + '|' + $("#oadh-victimizer-age-from-filter").val()});
		}

		if($("#oadh-victimizer-age-from-filter").isEmpty() && !$("#oadh-victimizer-age-to-filter").isEmpty())
		{
			filters.push({'field':'vic_age_to', 'op':'le', 'data':  'R' + '|' + $("#oadh-victimizer-age-to-filter").val()});
		}

		if(!$('#oadh-victimizer-profesion-filter').isEmpty())
		{
			filters.push({'field':'vic_profesion', 'op':'eq', 'data': 'R' + '|' + $('#oadh-victimizer-profesion-filter').val()});
			// filters.push({'field':'vic_type', 'op':'eq', 'data': 'R'});
		}

		if(!$('#oadh-victimizer-gender-filter').isEmpty())
		{
			filters.push({'field':'vic_gender', 'op':'eq', 'data': 'R' + '|' + $('#oadh-victimizer-gender-filter').val()});
			// filters.push({'field':'vic_type', 'op':'eq', 'data': 'R'});
		}

		if($('#oadh-victimizer-type-filter-p').is(':checked'))
		{
			filters.push({'field':'vic_subtype', 'op':'eq', 'data': 'R' + '|' + $('#oadh-victimizer-type-filter-p').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'R'});
		}

		if($('#oadh-victimizer-type-filter-s').is(':checked'))
		{
			filters.push({'field':'vic_subtype', 'op':'eq', 'data': 'R' + '|' + $('#oadh-victimizer-type-filter-s').val()});
			// filters.push({'field':'vic_type', 'op':'cn', 'data': 'R'});
		}

		if(!$('#oadh-tracing-filter').isEmpty())
		{
			filters.push({'field':'tracing_type', 'op':'in', 'data': $('#oadh-tracing-filter').val()});
		}

		if(!$('#oadh-tracing-closed-filter-id').isEmpty())
		{
			filters.push({'field':'tracing_type', 'op':'in', 'data': $('#oadh-tracing-closed-filter-id').val()});
		}

		if(!$('#oadh-right-filter').isEmpty())
		{
			filters.push({'field':'right', 'op':'in', 'data': $('#oadh-right-filter').val()});
		}

		if(!$('#oadh-right-closed-filter-id').isEmpty())
		{
			filters.push({'field':'right', 'op':'in', 'data': $('#oadh-right-closed-filter-id').val()});
		}

		if(!$('#oadh-topic-filter').isEmpty())
		{
			filters.push({'field':'topic', 'op':'in', 'data': $('#oadh-topic-filter').val()});
		}

		if(!$('#oadh-topic-closed-filter-id').isEmpty())
		{
			filters.push({'field':'topic', 'op':'in', 'data': $('#oadh-topic-closed-filter-id').val()});
		}

		if(!$('#oadh-violated-fact-filter').isEmpty())
		{
			filters.push({'field':'violated_fact', 'op':'in', 'data': $('#oadh-violated-fact-filter').val()});
		}

		if(!$('#oadh-violated-fact-closed-filter-id').isEmpty())
		{
			filters.push({'field':'violated_fact', 'op':'in', 'data': $('#oadh-violated-fact-closed-filter-id').val()});
		}

		if(!$('#oadh-department-filter').isEmpty())
		{
			filters.push({'field':'department', 'op':'in', 'data': $('#oadh-department-filter').val()});
		}

		if(!$('#oadh-municipality-filter').isEmpty())
		{
			filters.push({'field':'municipality', 'op':'in', 'data': $('#oadh-municipality-filter').val()});
		}

		if(!$('#oadh-neighborhood-filter').isEmpty())
		{
			filters.push({'field':'neighborhood', 'op':'eq', 'data': $('#oadh-neighborhood-filter').val()});
		}

		if(filters.length == 0)
		{
			$('#oadh-btn-filter').removeClass('btn-warning').addClass('btn-default');
		}

		$('#oadh-front-context-grid, #oadh-front-human-right-grid, #oadh-front-producedure-grid, #oadh-front-source-grid, #oadh-front-victim-grid, #oadh-front-victimizer-grid, #oadh-front-human-right-grid, #oadh-front-producedure-grid, #oadh-front-news-news-grid,#oadh-front-source-grid, #oadh-front-victim-grid, #oadh-front-victimizer-grid').jqGrid('clearGridData');

		$('#oadh-grid').jqGrid('setGridParam', {'postData':{'datatype':'json', "filters":"{'groupOp':'AND','rules':" + JSON.stringify(filters) + "}"}}).trigger('reloadGrid');
	});

	if(!$('#oadh-new-action').isEmpty())
	{
		$('#oadh-btn-new').click();
	}

	$('#oadh-btn-edit-helper').click(function()
	{
		showButtonHelper('oadh-btn-close', 'oadh-btn-group-2', $('#oadh-edit-action').attr('data-content'));
	});

	if(!$('#oadh-edit-action').isEmpty())
	{
		showButtonHelper('oadh-btn-close', 'oadh-btn-group-2', $('#oadh-edit-action').attr('data-content'));
	}

	$('#oadh-btn-delete-helper').click(function()
	{
		showButtonHelper('oadh-btn-close', 'oadh-btn-group-2', $('#oadh-delete-action').attr('data-content'));
	});

	if(!$('#oadh-delete-action').isEmpty())
	{
		showButtonHelper('oadh-btn-close', 'oadh-btn-group-2', $('#oadh-delete-action').attr('data-content'));
	}

	setTimeout(function() {

		$('#oadh-digital-media-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhDigitalMedias, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-digital-media-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhDigitalMedias);
		});

		$('#oadh-section-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhSections, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-section-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhSections);
		});

		$('#oadh-note-location-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhNoteLocations, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-note-location-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhNoteLocations);
		});

		$('#oadh-journalistic-genre-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhJournalisticGenres, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-journalistic-genre-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhJournalisticGenres);
		});

		$('#oadh-tracing-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhTracing, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-tracing-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhTracing);
		});

		$('#oadh-department-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhDepartment, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-department-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhDepartment);
		});

		$('#oadh-right-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhRights, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-right-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhRights);
		});

		$('#oadh-topic-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhTopics, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-topic-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhTopics);
		});

		$('#oadh-violated-fact-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhViolatedFacts, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-violated-fact-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhViolatedFacts);
		});

		$('#oadh-municipality-filter').tokenfield(
		{
			autocomplete:
			{
				// source: oadhDigitalMedias,
				source: function(request, response)
				{
					var results = $.ui.autocomplete.filter(oadhMunicipality, request.term);
					response(results.slice(0, 10));
				},
				delay: 100,
				focus: function( event, ui ) {return false;}
			},
			showAutocompleteOnFocus: true,
			beautify:false
		});

		$('#oadh-municipality-filter').on('tokenfield:createtoken', function (event)
		{
			return validateToken(event, oadhMunicipality);
		});

		$('#oadh-tracing-closed-filter-label').on('autocompleteselect', function( event, ui )
		{
			if($('#oadh-tracing-closed-filter-label').attr('data-id') != ui.item.value)
			{
				$('#oadh-tracing-closed-filter-label').attr('data-id', ui.item.value);
				$('#oadh-right-closed-filter-label').autocomplete('option', 'source', ui.item.rights);
			}
		});

		$('#oadh-right-closed-filter-label').on('autocompleteselect', function( event, ui )
		{
			if($('#oadh-right-closed-filter-label').attr('data-id') != ui.item.value)
			{
				$('#oadh-right-closed-filter-label').attr('data-id', ui.item.value);
				$('#oadh-topic-closed-filter-label').autocomplete('option', 'source', ui.item.topics);
				$('#oadh-violated-fact-closed-filter-label').autocomplete('option', 'source', ui.item.violated_fact);
			}
		});
	}, 500);
});
</script>
