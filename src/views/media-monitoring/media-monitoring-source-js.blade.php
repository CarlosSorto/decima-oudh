<script type="text/javascript">
function oadhDisabledSourceForm()
{
	$('#oadh-source-form-fieldset').attr('disabled', 'disabled');
}

function oadhSourceOnSelectRowEvent()
{
	var selRowIds = $('#oadh-source-grid').jqGrid('getGridParam', 'selarrrow');

	if(selRowIds.length == 0)
	{
		$('#oadh-source-btn-group-2').disabledButtonGroup();
	}
	else if(selRowIds.length == 1)
	{
		$('#oadh-source-btn-group-2').enableButtonGroup();
	}
	else if(selRowIds.length > 1)
	{
		$('#oadh-source-btn-group-2').disabledButtonGroup();
		$('#oadh-source-btn-delete').removeAttr('disabled');
	}
}

function oadhSourceOnLoadCompleteEvent()
{
	// var amount = $(this).jqGrid('getCol', 'module_app_source_', false, 'sum');
	//
	// $(this).jqGrid('footerData','set', {
	// 	'module_app_source_name': '{{ Lang::get('form.total') }}',
	// 	'module_app_source_': amount,
	// });
	//
	// $('#oadh-source-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
}

$(document).ready(function()
{
	$('#oadh-source-btn-refresh').click(function()
	{
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-source-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + $('#oadh-source-news-id').val() + "'}]}"}}).trigger('reloadGrid');
	});

	$('#oadh-source-btn-export-xls').click(function()
	{
		$('#oadh-source-gridXlsButton').click();
	});

	$('#oadh-source-btn-export-csv').click(function()
	{
		$('#oadh-source-gridCsvButton').click();
	});


	$('#oadh-source-btn-save').click(function()
	{
		var url = $('#oadh-source-form').attr('action'), action = 'new';

		if(!$('#oadh-source-form').jqMgVal('isFormValid'))
		{
			return;
		}

		if($('#oadh-source-source').isEmpty() && $('#oadh-source-instance').isEmpty() && $('#oadh-source-responsable').isEmpty())
		{
			return $('#oadh-source-form').showAlertAsFirstChild('alert-info', '{{Lang::get('decima-oadh::media-monitoring.contextIsEmpty')}}', 12000);
		}

		if($('#oadh-source-id').isEmpty())
		{
			url = url + '/create-source';
		}
		else
		{
			url = url + '/update-source';
			action = 'edit';
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify($('#oadh-source-form').formToObject('oadh-source-')),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-source-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-source-form').jqMgVal('clearForm');
					$('#oadh-source-btn-toolbar').disabledButtonGroup();
					$('#oadh-source-btn-group-1').enableButtonGroup();
					$('#oadh-source-btn-group-3').enableButtonGroup();

					$('#oadh-source-btn-refresh').click();

					// $('#oadh-source-form').showAlertAsFirstChild('alert-success', json.success, 5000);
				}
				else if(json.info)
				{
						$('#oadh-source-form').showAlertAsFirstChild('alert-info', json.info, 12000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-source-source').focus();
			}
		});
	});

	$('#oadh-source-btn-edit').click(function()
	{
		var rowData;

		$('.decima-erp-tooltip').tooltip('hide');

		if(!$('#oadh-source-grid').isRowSelected())
		{
			$('#oadh-source-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$('#oadh-source-btn-toolbar').disabledButtonGroup();
		$('#oadh-source-btn-group-3').enableButtonGroup();


		rowData = $('#oadh-source-grid').getRowData($('#oadh-source-grid').jqGrid('getGridParam', 'selrow'));
		populateFormFields(rowData);

		// $('#oadh-source-label').setAutocompleteLabel(rowData.module_app_source_id);

		$('#oadh-source-department').focus();
	});

	$('#oadh-source-btn-delete').click(function()
	{
		var id = $('#oadh-source-grid').getSelectedRowsIdCell('oadh_source_id');

		if(!$('#oadh-source-grid').isRowSelected())
		{
			$('#oadh-source-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token':$('#app-token').val(),
					'id': id,
					'news_id': $('#oadh-source-news-id').val()
				}
			),
			dataType : 'json',
			url:  $('#oadh-source-form').attr('action') + '/delete-source',
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-source-btn-toolbar', false);
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-source-btn-refresh').click();
					$("#oadh-source-btn-group-2").disabledButtonGroup();
					$('#oadh-source-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();
				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});

});
</script>
