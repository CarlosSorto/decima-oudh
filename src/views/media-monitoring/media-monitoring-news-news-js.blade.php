<script type="text/javascript">
function oadhDisabledProducedureForm()
{
	$('#oadh-news-news-form-fieldset').attr('disabled', 'disabled');
}

function oadhNewsNewsOnSelectRowEvent()
{
	var selRowIds = $('#oadh-news-news-grid').jqGrid('getGridParam', 'selarrrow');

	if(selRowIds.length == 0)
	{
		$('#oadh-news-news-btn-group-2').disabledButtonGroup();
	}
	else if(selRowIds.length == 1)
	{
		$('#oadh-news-news-btn-group-2').enableButtonGroup();
	}
	else if(selRowIds.length > 1)
	{
		$('#oadh-news-news-btn-group-2').disabledButtonGroup();
		$('#oadh-news-news-btn-delete').removeAttr('disabled');
	}
}

function oadhNewsNewsOnLoadCompleteEvent()
{
	// var amount = $(this).jqGrid('getCol', 'module_app_producedure_', false, 'sum');
	//
	// $(this).jqGrid('footerData','set', {
	// 	'module_app_producedure_name': '{{ Lang::get('form.total') }}',
	// 	'module_app_producedure_': amount,
	// });
	//
	// $('#oadh-news-news-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
}

function oadhNewsNewsShowSmtRows()
{
	$.scrollTo({top: 0, left:0});

	$('#oadh-smt').createTable('', '', 10, '',{
		'oadh_code':{'label':'{{ Lang::get('decima-oadh::media-monitoring.code') }}', 'width':'10%', 'align':'center'},
		'oadh_date':{'label':'{{ Lang::get('form.date') }}', 'width':'10%', 'formatter':'date', 'align':'center'},
		'oadh_title':{'label':'{{Lang::get('decima-oadh::media-monitoring.title') }}', 'width':'30%'},
		'oadh_summary':{'label':'{{ Lang::get('decima-oadh::media-monitoring.summary') }}', 'width':'30%'},
		'oadh_digital_media_label':{'label':'{{ Lang::get('decima-oadh::media-monitoring.digitalMediaShort') }}', 'width':'10%'},
		'oadh_section_label':{'label':'{{ Lang::get('decima-oadh::media-monitoring.section') }}', 'width':'10%', 'align':'left'},
	}, null, 'post', null, null, null, $('#app-url').val() + '/ucaoadh/transactions/media-monitoring/smt-rows', 1);

	$('#oadh-smt').modal('show');
}

function oadhNewsNewsClearSmtRows()
{
	$('#oadh-news-news-related-news-label, #oadh-news-news-related-news-id').val('');
}

$(document).ready(function()
{

	$('#oadh-news-news-btn-refresh').click(function()
	{
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-news-news-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + $('#oadh-news-news-news-id').val() + "'}]}"}}).trigger('reloadGrid');
	});

	$('#oadh-news-news-btn-export-xls').click(function()
	{
		$('#oadh-news-news-gridXlsButton').click();
	});

	$('#oadh-news-news-btn-export-csv').click(function()
	{
		$('#oadh-news-news-gridCsvButton').click();
	});


	$('#oadh-news-news-btn-save').click(function()
	{
		var url = $('#oadh-news-news-form').attr('action'), action = 'new';

		if(!$('#oadh-news-news-form').jqMgVal('isFormValid'))
		{
			return;
		}

		if($('#oadh-news-news-id').isEmpty())
		{
			url = url + '/create-news-news';
		}
		else
		{
			url = url + '/update-news-news';
			action = 'edit';
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify($('#oadh-news-news-form').formToObject('oadh-news-news-')),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-news-news-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-news-news-form').jqMgVal('clearForm');
					$('#oadh-news-news-btn-toolbar').disabledButtonGroup();
					$('#oadh-news-news-btn-group-1').enableButtonGroup();
					$('#oadh-news-news-btn-group-3').enableButtonGroup();

					$('#oadh-news-news-btn-refresh').click();

					// $('#oadh-news-news-form').showAlertAsFirstChild('alert-success', json.success, 5000);
				}
				else if(json.info)
				{
						$('#oadh-news-news-form').showAlertAsFirstChild('alert-info', json.info, 12000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-news-news-place').focus();
			}
		});
	});

	$('#oadh-news-news-btn-edit').click(function()
	{
		var rowData;

		$('.decima-erp-tooltip').tooltip('hide');

		if(!$('#oadh-news-news-grid').isRowSelected())
		{
			$('#oadh-news-news-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$('#oadh-news-news-btn-toolbar').disabledButtonGroup();
		$('#oadh-news-news-btn-group-3').enableButtonGroup();


		rowData = $('#oadh-news-news-grid').getRowData($('#oadh-news-news-grid').jqGrid('getGridParam', 'selrow'));
		populateFormFields(rowData);

		// $('#oadh-news-news-label').setAutocompleteLabel(rowData.module_app_producedure_id);

		$('#oadh-news-news-department').focus();
	});

	$('#oadh-news-news-btn-delete').click(function()
	{
		var id = $('#oadh-news-news-grid').getSelectedRowsIdCell('oadh_news_news_id');

		if(!$('#oadh-news-news-grid').isRowSelected())
		{
			$('#oadh-news-news-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token':$('#app-token').val(),
					'id': id,
					'news_id': $('#oadh-news-news-news-id').val()
				}
			),
			dataType : 'json',
			url:  $('#oadh-news-news-form').attr('action') + '/delete-news-news',
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-news-news-btn-toolbar', false);
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-news-news-btn-refresh').click();
					$("#oadh-news-news-btn-group-2").disabledButtonGroup();
					$('#oadh-news-news-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();
				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});

	$('#oadh-smt-btn-select').click(function()
	{
		var rowData = $('#oadh-smt').getSelectedSmtRow();

		if(empty(rowData))
		{
			return;
		}

		$('#oadh-news-news-related-news-label').val(rowData.oadh_title);
		$('#oadh-news-news-related-news-id').val(rowData.oadh_id);

		$.scrollTo({top: $('#oadh-news-news-related-news-label').offset().top, left:0});

		$('#oadh-smt').modal('hide');
	});

	$('#oadh-smt-btn-refresh').click(function()
	{
		$('#oadh-smt-btn-search').click();
	});

});
</script>
