<script type="text/javascript">
function oadhDisabledVictimForm()
{
	$('#oadh-victim-form-fieldset').attr('disabled', 'disabled');
}

function oadhVictimOnSelectRowEvent()
{
	var selRowIds = $('#oadh-victim-grid').jqGrid('getGridParam', 'selarrrow');

	if(selRowIds.length == 0)
	{
		$('#oadh-victim-btn-group-2').disabledButtonGroup();
	}
	else if(selRowIds.length == 1)
	{
		$('#oadh-victim-btn-group-2').enableButtonGroup();
	}
	else if(selRowIds.length > 1)
	{
		$('#oadh-victim-btn-group-2').disabledButtonGroup();
		$('#oadh-victim-btn-delete').removeAttr('disabled');
	}
}

function oadhVictimOnLoadCompleteEvent()
{
	// var amount = $(this).jqGrid('getCol', 'module_app_victim_', false, 'sum');
	//
	// $(this).jqGrid('footerData','set', {
	// 	'module_app_victim_name': '{{ Lang::get('form.total') }}',
	// 	'module_app_victim_': amount,
	// });
	//
	// $('#oadh-victim-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
}

$(document).ready(function()
{
	$('#oadh-div-second-victim-relation').hide();

	$('.oadh-victim-subtype').change(function()
	{
		if($(this).val() == 'P')
		{
			$('#oadh-div-second-victim-relation').hide();
			$('#oadh-div-victim-relation').show();

			$('#oadh-victim-relation-yes').click();

			if($(".oadh-victim-relation:checked").val()==true)
			{
				$('#oadh-victim-victimizer-relation').val('1');
				$('#oadh-victim-main-victim-relation').val(null);
			}
			else
			{
				$('#oadh-victim-victimizer-relation').val('0');
				$('#oadh-victim-main-victim-relation').val(null);
			}
		}
		else
		{
			$('#oadh-div-victim-relation').hide();
			$('#oadh-div-second-victim-relation').show();

			$('#oadh-victim-second-relation-yes').click();

			if($(".oadh-victim-second-relation:checked").val()==true)
			{
				$('#oadh-victim-victimizer-relation').val(null);
				$('#oadh-victim-main-victim-relation').val('1');
			}
			else
			{
				$('#oadh-victim-victimizer-relation').val(null);
				$('#oadh-victim-main-victim-relation').val('0');
			}

		}

		$('#oadh-victim-subtype').val($(this).val());
	});

	$('.oadh-victim-relation').change(function()
	{
		$('#oadh-victim-victimizer-relation').val($(this).attr('data-val'));
		$('#oadh-victim-main-victim-relation').val(null);

	});

	$('.oadh-victim-second-relation').change(function()
	{
		$('#oadh-victim-victimizer-relation').val(null);
		$('#oadh-victim-main-victim-relation').val($(this).attr('data-val'));
	});

	$('#oadh-victim-btn-refresh').click(function()
	{
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-victim-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + $('#oadh-victim-news-id').val() + "'}]}"}}).trigger('reloadGrid');
	});

	$('#oadh-victim-btn-export-xls').click(function()
	{
		$('#oadh-victim-gridXlsButton').click();
	});

	$('#oadh-victim-btn-export-csv').click(function()
	{
		$('#oadh-victim-gridCsvButton').click();
	});


	$('#oadh-victim-btn-save').click(function()
	{
    var url = $('#oadh-victim-form').attr('action'), action = 'new';

		if(!$('#oadh-victim-form').jqMgVal('isFormValid'))
		{
			return;
		}

		if($('#oadh-victim-id').isEmpty())
		{
			url = url + '/create-people';
		}
		else
		{
			url = url + '/update-people';
			action = 'edit';
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify($('#oadh-victim-form').formToObject('oadh-victim-')),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-victim-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-victim-form').jqMgVal('clearForm');
					$('#oadh-victim-btn-toolbar').disabledButtonGroup();
					$('#oadh-victim-btn-group-1').enableButtonGroup();
					$('#oadh-victim-btn-group-3').enableButtonGroup();

					$('#oadh-victim-btn-refresh').click();

					// $('#oadh-victim-form').showAlertAsFirstChild('alert-success', json.success, 5000);
				}
				else if(json.info)
				{
					$('#oadh-victim-form').showAlertAsFirstChild('alert-info', json.info, 12000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-victim-name').focus();
			}
		});
	});

	$('#oadh-victim-btn-edit').click(function()
	{
		var rowData;

		$('.decima-erp-tooltip').tooltip('hide');

		if(!$('#oadh-victim-grid').isRowSelected())
		{
			$('#oadh-victim-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$('#oadh-victim-btn-toolbar').disabledButtonGroup();
		$('#oadh-victim-btn-group-3').enableButtonGroup();


		rowData = $('#oadh-victim-grid').getRowData($('#oadh-victim-grid').jqGrid('getGridParam', 'selrow'));
		populateFormFields(rowData);

		// console.log(rowData);

		if(rowData.oadh_victim_subtype == 'P')
		{
			$('#oadh-victim-subtype-p').click();

			setTimeout(function() {
				if(rowData.oadh_victimizer_relation == "true")
				{
					$('#oadh-victim-relation-yes').click();
				}
				else
				{
					$('#oadh-victim-relation-no').click();
				}
			}, 500);
		}
		else
		{
			$('#oadh-victim-subtype-s').click();

			setTimeout(function() {
				if(rowData.oadh_main_victim_relation == "true")
				{
					$('#oadh-victim-second-relation-yes').click();
				}
				else
				{
					$('#oadh-victim-second-relation-no').click();
				}
			}, 500);
		}

		// $('#oadh-victim-label').setAutocompleteLabel(rowData.module_app_victim_id);

		$('#oadh-victim-department').focus();
	});

	$('#oadh-victim-btn-delete').click(function()
	{
		var id = $('#oadh-victim-grid').getSelectedRowsIdCell('oadh_victim_id');

		if(!$('#oadh-victim-grid').isRowSelected())
		{
			$('#oadh-victim-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token':$('#app-token').val(),
					'id': id,
					'news_id': $('#oadh-victim-news-id').val()
				}
			),
			dataType : 'json',
			url:  $('#oadh-victim-form').attr('action') + '/delete-people',
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-victim-btn-toolbar', false);
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-victim-btn-refresh').click();
					$("#oadh-victim-btn-group-2").disabledButtonGroup();
					$('#oadh-victim-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();
				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});

});
</script>
