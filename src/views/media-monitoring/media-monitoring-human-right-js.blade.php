<script type="text/javascript">
function oadhDisabledHumanRightForm()
{
	$('#oadh-human-right-form-fieldset').attr('disabled', 'disabled');
}

function oadhHumanRightOnSelectRowEvent()
{
	var selRowIds = $('#oadh-human-right-grid').jqGrid('getGridParam', 'selarrrow');

	if(selRowIds.length == 0)
	{
		$('#oadh-human-right-btn-group-2').disabledButtonGroup();
	}
	else if(selRowIds.length == 1)
	{
		$('#oadh-human-right-btn-group-2').enableButtonGroup();
	}
	else if(selRowIds.length > 1)
	{
		$('#oadh-human-right-btn-group-2').disabledButtonGroup();
		$('#oadh-human-right-btn-delete').removeAttr('disabled');
	}
}

function oadhHumanRightOnLoadCompleteEvent()
{
	// var amount = $(this).jqGrid('getCol', 'module_app_human-right_', false, 'sum');
	//
	// $(this).jqGrid('footerData','set', {
	// 	'module_app_human-right_name': '{{ Lang::get('form.total') }}',
	// 	'module_app_human-right_': amount,
	// });
	//
	// $('#oadh-human-right-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
}

$(document).ready(function()
{
  $('#oadh-human-right-tracing-type-label').on('autocompleteselect', function( event, ui )
  {
    if($('#oadh-human-right-tracing-type-label').attr('data-id') != ui.item.value)
    {
			$('#oadh-human-right-tracing-type-label').attr('data-id', ui.item.value);
      $('#oadh-human-right-right-label').autocomplete('option', 'source', ui.item.rights);
    }
  });

  $('#oadh-human-right-right-label').on('autocompleteselect', function( event, ui )
  {
    if($('#oadh-human-right-right-label').attr('data-id') != ui.item.value)
    {
			$('#oadh-human-right-right-label').attr('data-id', ui.item.value);
      $('#oadh-human-right-topic-label').autocomplete('option', 'source', ui.item.topics);
      $('#oadh-human-right-violated-fact-label').autocomplete('option', 'source', ui.item.violated_fact);
    }
  });

	$('#oadh-human-right-btn-refresh').click(function()
	{
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-human-right-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + $('#oadh-human-right-news-id').val() + "'}]}"}}).trigger('reloadGrid');
	});

	$('#oadh-human-right-btn-export-xls').click(function()
	{
		$('#oadh-human-right-gridXlsButton').click();
	});

	$('#oadh-human-right-btn-export-csv').click(function()
	{
		$('#oadh-human-right-gridCsvButton').click();
	});


	$('#oadh-human-right-btn-save').click(function()
	{
		var url = $('#oadh-human-right-form').attr('action'), action = 'new';

		if(!$('#oadh-human-right-form').jqMgVal('isFormValid'))
		{
			return;
		}

		if($('#oadh-human-right-id').isEmpty())
		{
			url = url + '/create-human-rights';
		}
		else
		{
			url = url + '/update-human-rights';
			action = 'edit';
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify($('#oadh-human-right-form').formToObject('oadh-human-right-')),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-human-right-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-human-right-form').jqMgVal('clearForm');
					$('#oadh-human-right-btn-toolbar').disabledButtonGroup();
					$('#oadh-human-right-btn-group-1').enableButtonGroup();
					$('#oadh-human-right-btn-group-3').enableButtonGroup();

					$('#oadh-human-right-btn-refresh').click();

					$('#oadh-human-right-tracing-type').focus();

					// $('#oadh-human-right-form').showAlertAsFirstChild('alert-success', json.success, 5000);
				}
				else if(json.info)
				{
						$('#oadh-human-right-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						$('#oadh-human-right-qualification').focus();
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');

			}
		});
	});

	$('#oadh-human-right-btn-edit').click(function()
	{
		var rowData;

		$('.decima-erp-tooltip').tooltip('hide');

		if(!$('#oadh-human-right-grid').isRowSelected())
		{
			$('#oadh-human-right-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$('#oadh-human-right-btn-toolbar').disabledButtonGroup();
		$('#oadh-human-right-btn-group-3').enableButtonGroup();

		rowData = $('#oadh-human-right-grid').getRowData($('#oadh-human-right-grid').jqGrid('getGridParam', 'selrow'));
		populateFormFields(rowData);

		// $('#oadh-human-right-label').setAutocompleteLabel(rowData.module_app_human-right_id);

		$('#oadh-human-right-name').focus();
	});
	
	$('#oadh-human-right-btn-delete').click(function()
	{
		var id = $('#oadh-human-right-grid').getSelectedRowsIdCell('oadh_human_right_id');

		if(!$('#oadh-human-right-grid').isRowSelected())
		{
			$('#oadh-human-right-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token':$('#app-token').val(),
					'id': id,
					'news_id': $('#oadh-human-right-news-id').val()
				}
			),
			dataType : 'json',
			url:  $('#oadh-human-right-form').attr('action') + '/delete-human-rights',
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-human-right-btn-toolbar', false);
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-human-right-btn-refresh').click();
					$("#oadh-human-right-btn-group-2").disabledButtonGroup();
					$('#oadh-human-right-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();
				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});
});
</script>
