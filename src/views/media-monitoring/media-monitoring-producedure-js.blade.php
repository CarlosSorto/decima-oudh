<script type="text/javascript">
function oadhDisabledProducedureForm()
{
	$('#oadh-producedure-form-fieldset').attr('disabled', 'disabled');
}

function oadhProducedureOnSelectRowEvent()
{
	var selRowIds = $('#oadh-producedure-grid').jqGrid('getGridParam', 'selarrrow');

	if(selRowIds.length == 0)
	{
		$('#oadh-producedure-btn-group-2').disabledButtonGroup();
	}
	else if(selRowIds.length == 1)
	{
		$('#oadh-producedure-btn-group-2').enableButtonGroup();
	}
	else if(selRowIds.length > 1)
	{
		$('#oadh-producedure-btn-group-2').disabledButtonGroup();
		$('#oadh-producedure-btn-delete').removeAttr('disabled');
	}
}

function oadhProducedureOnLoadCompleteEvent()
{
	// var amount = $(this).jqGrid('getCol', 'module_app_producedure_', false, 'sum');
	//
	// $(this).jqGrid('footerData','set', {
	// 	'module_app_producedure_name': '{{ Lang::get('form.total') }}',
	// 	'module_app_producedure_': amount,
	// });
	//
	// $('#oadh-producedure-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
}

$(document).ready(function()
{

	$('#oadh-producedure-btn-refresh').click(function()
	{
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-producedure-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + $('#oadh-producedure-news-id').val() + "'}]}"}}).trigger('reloadGrid');
	});

	$('#oadh-producedure-btn-export-xls').click(function()
	{
		$('#oadh-producedure-gridXlsButton').click();
	});

	$('#oadh-producedure-btn-export-csv').click(function()
	{
		$('#oadh-producedure-gridCsvButton').click();
	});


	$('#oadh-producedure-btn-save').click(function()
	{
		var url = $('#oadh-producedure-form').attr('action'), action = 'new';

		if(!$('#oadh-producedure-form').jqMgVal('isFormValid'))
		{
			return;
		}

		if($('#oadh-producedure-place').isEmpty() && $('#oadh-producedure-weapon').isEmpty() && $('#oadh-producedure-hypothesis-fact').isEmpty() && $('#oadh-producedure-hypothesis-fact').isEmpty() && $('#oadh-producedure-accident-type').isEmpty())
		{
			return $('#oadh-producedure-form').showAlertAsFirstChild('alert-info', '{{Lang::get('decima-oadh::media-monitoring.contextIsEmpty')}}', 12000);
		}

		if($('#oadh-producedure-id').isEmpty())
		{
			url = url + '/create-producedure';
		}
		else
		{
			url = url + '/update-producedure';
			action = 'edit';
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify($('#oadh-producedure-form').formToObject('oadh-producedure-')),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-producedure-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-producedure-form').jqMgVal('clearForm');
					$('#oadh-producedure-btn-toolbar').disabledButtonGroup();
					$('#oadh-producedure-btn-group-1').enableButtonGroup();
					$('#oadh-producedure-btn-group-3').enableButtonGroup();

					$('#oadh-producedure-btn-refresh').click();

					// $('#oadh-producedure-form').showAlertAsFirstChild('alert-success', json.success, 5000);
				}
				else if(json.info)
				{
						$('#oadh-producedure-form').showAlertAsFirstChild('alert-info', json.info, 12000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-producedure-place').focus();
			}
		});
	});

	$('#oadh-producedure-btn-edit').click(function()
	{
		var rowData;

		$('.decima-erp-tooltip').tooltip('hide');

		if(!$('#oadh-producedure-grid').isRowSelected())
		{
			$('#oadh-producedure-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$('#oadh-producedure-btn-toolbar').disabledButtonGroup();
		$('#oadh-producedure-btn-group-3').enableButtonGroup();


		rowData = $('#oadh-producedure-grid').getRowData($('#oadh-producedure-grid').jqGrid('getGridParam', 'selrow'));
		populateFormFields(rowData);

		// $('#oadh-producedure-label').setAutocompleteLabel(rowData.module_app_producedure_id);

		$('#oadh-producedure-department').focus();
	});

	$('#oadh-producedure-btn-delete').click(function()
	{
		var id = $('#oadh-producedure-grid').getSelectedRowsIdCell('oadh_producedure_id');

		if(!$('#oadh-producedure-grid').isRowSelected())
		{
			$('#oadh-producedure-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token':$('#app-token').val(),
					'id': id,
					'news_id': $('#oadh-producedure-news-id').val()
				}
			),
			dataType : 'json',
			url:  $('#oadh-producedure-form').attr('action') + '/delete-producedure',
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-producedure-btn-toolbar', false);
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-producedure-btn-refresh').click();
					$("#oadh-producedure-btn-group-2").disabledButtonGroup();
					$('#oadh-producedure-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();
				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});

});
</script>
