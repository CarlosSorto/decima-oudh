<script type="text/javascript">
function oadhDisabledContextForm()
{
	$('#oadh-context-form-fieldset').attr('disabled', 'disabled');
}

function oadhContextOnSelectRowEvent()
{
	var selRowIds = $('#oadh-context-grid').jqGrid('getGridParam', 'selarrrow');

	if(selRowIds.length == 0)
	{
		$('#oadh-context-btn-group-2').disabledButtonGroup();
	}
	else if(selRowIds.length == 1)
	{
		$('#oadh-context-btn-group-2').enableButtonGroup();
	}
	else if(selRowIds.length > 1)
	{
		$('#oadh-context-btn-group-2').disabledButtonGroup();
		$('#oadh-context-btn-delete').removeAttr('disabled');
	}
}

function oadhContextOnLoadCompleteEvent()
{
	// var amount = $(this).jqGrid('getCol', 'module_app_context_', false, 'sum');
	//
	// $(this).jqGrid('footerData','set', {
	// 	'module_app_context_name': '{{ Lang::get('form.total') }}',
	// 	'module_app_context_': amount,
	// });
	//
	// $('#oadh-context-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
}

$(document).ready(function()
{

  $('#oadh-context-department').on('autocompleteselect', function( event, ui )
  {
    if($('#oadh-context-department').attr('data-sale-point-id') != ui.item.value)
    {
      // $('#oadh-sale-point-label').attr('data-sale-point-id', ui.item.value);

			$('#oadh-context-municipality').autocomplete('option', 'source', ui.item.child);
    }
  });

	$('#oadh-context-btn-refresh').click(function()
	{
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-context-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + $('#oadh-context-news-id').val() + "'}]}"}}).trigger('reloadGrid');
	});

	$('#oadh-context-btn-export-xls').click(function()
	{
		$('#oadh-context-gridXlsButton').click();
	});

	$('#oadh-context-btn-export-csv').click(function()
	{
		$('#oadh-context-gridCsvButton').click();
	});


	$('#oadh-context-btn-save').click(function()
	{
		var url = $('#oadh-context-form').attr('action'), action = 'new';

		if(!$('#oadh-context-form').jqMgVal('isFormValid'))
		{
			return;
		}

		if($('#oadh-context-department').isEmpty() && $('#oadh-context-municipality').isEmpty() && $('#oadh-context-neighborhood').isEmpty())
		{
			return $('#oadh-context-form').showAlertAsFirstChild('alert-info', '{{Lang::get('decima-oadh::media-monitoring.contextIsEmpty')}}', 12000);
		}

		if($('#oadh-context-id').isEmpty())
		{
			url = url + '/create-context';
		}
		else
		{
			url = url + '/update-context';
			action = 'edit';
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify($('#oadh-context-form').formToObject('oadh-context-')),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-context-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-context-form').jqMgVal('clearForm');
					$('#oadh-context-btn-toolbar').disabledButtonGroup();
					$('#oadh-context-btn-group-1').enableButtonGroup();
					$('#oadh-context-btn-group-3').enableButtonGroup();

					$('#oadh-context-btn-refresh').click();

					// $('#oadh-context-form').showAlertAsFirstChild('alert-success', json.success, 5000);
				}
				else if(json.info)
				{
						$('#oadh-context-form').showAlertAsFirstChild('alert-info', json.info, 12000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-context-department').focus();
			}
		});
	});

	$('#oadh-context-btn-edit').click(function()
	{
		var rowData;

		$('.decima-erp-tooltip').tooltip('hide');

		if(!$('#oadh-context-grid').isRowSelected())
		{
			$('#oadh-context-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$('#oadh-context-btn-toolbar').disabledButtonGroup();
		$('#oadh-context-btn-group-3').enableButtonGroup();


		rowData = $('#oadh-context-grid').getRowData($('#oadh-context-grid').jqGrid('getGridParam', 'selrow'));
		populateFormFields(rowData);

		// $('#oadh-context-label').setAutocompleteLabel(rowData.module_app_context_id);

		$('#oadh-context-department').focus();
	});

	$('#oadh-context-btn-delete').click(function()
	{
		var id = $('#oadh-context-grid').getSelectedRowsIdCell('oadh_context_id');

		if(!$('#oadh-context-grid').isRowSelected())
		{
			$('#oadh-context-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token':$('#app-token').val(),
					'id': id,
					'news_id': $('#oadh-context-news-id').val()
				}
			),
			dataType : 'json',
			url:  $('#oadh-context-form').attr('action') + '/delete-context',
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-context-btn-toolbar', false);
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-context-btn-refresh').click();
					$("#oadh-context-btn-group-2").disabledButtonGroup();
					$('#oadh-context-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();
				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});

});
</script>
