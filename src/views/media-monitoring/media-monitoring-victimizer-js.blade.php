<script type="text/javascript">
function oadhDisabledVictimizerForm()
{
	$('#oadh-victimizer-form-fieldset').attr('disabled', 'disabled');
}

function oadhVictimizerOnSelectRowEvent()
{
	var selRowIds = $('#oadh-victimizer-grid').jqGrid('getGridParam', 'selarrrow');

	if(selRowIds.length == 0)
	{
		$('#oadh-victimizer-btn-group-2').disabledButtonGroup();
	}
	else if(selRowIds.length == 1)
	{
		$('#oadh-victimizer-btn-group-2').enableButtonGroup();
	}
	else if(selRowIds.length > 1)
	{
		$('#oadh-victimizer-btn-group-2').disabledButtonGroup();
		$('#oadh-victimizer-btn-delete').removeAttr('disabled');
	}
}

function oadhVictimizerOnLoadCompleteEvent()
{
	// var amount = $(this).jqGrid('getCol', 'module_app_victimizer_', false, 'sum');
	//
	// $(this).jqGrid('footerData','set', {
	// 	'module_app_victimizer_name': '{{ Lang::get('form.total') }}',
	// 	'module_app_victimizer_': amount,
	// });
	//
	// $('#oadh-victimizer-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
}

$(document).ready(function()
{
	$('#oadh-div-second-victimizer-relation').hide();

	// $('#oadh-victimizer-subtype[type=radio]').change(function(){
	// 	if($(this).val() == 'P')
	// 	{
	// 		$('#oadh-div-second-victimizer-relation').hide();
	// 		$('#oadh-div-victimizer-relation').show();
	// 	}
	// 	else
	// 	{
	// 		$('#oadh-div-victimizer-relation').hide();
	// 		$('#oadh-div-second-victimizer-relation').show();
	// 	}
	// });

	$('.oadh-victimizer-subtype').change(function()
	{
		if($(this).val() == 'P')
		{
			$('#oadh-div-second-victimizer-relation').hide();
			$('#oadh-div-victimizer-relation').show();

			$('#oadh-victimizer-relation-yes').click();

			if($(".oadh-victimizer-relation:checked").val()==true)
			{
				$('#oadh-victimizer-victim-relation').val('1');
				$('#oadh-victimizer-victimizer-relation').val(null);
			}
			else
			{
				$('#oadh-victimizer-victim-relation').val('0');
				$('#oadh-victimizer-victimizer-relation').val(null);
			}
		}
		else
		{
			$('#oadh-div-victimizer-relation').hide();
			$('#oadh-div-second-victimizer-relation').show();

			$('#oadh-victim-second-relation-yes').click();

			if($(".oadh-victimizer-second-relation:checked").val()==true)
			{
				$('oadh-victimizer-victim-relation').val(null);
				$('#oadh-victimizer-victimizer-relation').val('1');
			}
			else
			{
				$('#oadh-victimizer-victim-relation').val(null);
				$('#oadh-victimizer-victimizer-relation').val('0');
			}
		}

		$('#oadh-victimizer-subtype').val($(this).val());
	});

	$('.oadh-victimizer-relation').change(function()
	{
		$('#oadh-victimizer-victim-relation').val($(this).attr('data-val'));
		$('#oadh-victimizer-victimizer-relation').val(null);
	});

	$('.oadh-victimizer-second-relation').change(function()
	{
		$('#oadh-victimizer-victim-relation').val(null);
		$('#oadh-victimizer-victimizer-relation').val($(this).attr('data-val'));
	});

	$('#oadh-victimizer-btn-refresh').click(function()
	{
		$('.decima-erp-tooltip').tooltip('hide');
		$('#oadh-victimizer-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'news_id','op':'eq','data':'" + $('#oadh-victimizer-news-id').val() + "'}]}"}}).trigger('reloadGrid');
	});

	$('#oadh-victimizer-btn-export-xls').click(function()
	{
		$('#oadh-victimizer-gridXlsButton').click();
	});

	$('#oadh-victimizer-btn-export-csv').click(function()
	{
		$('#oadh-victimizer-gridCsvButton').click();
	});


	$('#oadh-victimizer-btn-save').click(function()
	{
    var url = $('#oadh-victimizer-form').attr('action'), action = 'new';

		if(!$('#oadh-victimizer-form').jqMgVal('isFormValid'))
		{
			return;
		}

		if($('#oadh-victimizer-id').isEmpty())
		{
			url = url + '/create-people';
		}
		else
		{
			url = url + '/update-people';
			action = 'edit';
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify($('#oadh-victimizer-form').formToObject('oadh-victimizer-')),
			dataType : 'json',
			url: url,
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-victimizer-form');
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-victimizer-form').jqMgVal('clearForm');
					$('#oadh-victimizer-btn-toolbar').disabledButtonGroup();
					$('#oadh-victimizer-btn-group-1').enableButtonGroup();
					$('#oadh-victimizer-btn-group-3').enableButtonGroup();

					$('#oadh-victimizer-btn-refresh').click();

					// $('#oadh-victimizer-form').showAlertAsFirstChild('alert-success', json.success, 5000);
				}
				else if(json.info)
				{
						$('#oadh-victimizer-form').showAlertAsFirstChild('alert-info', json.info, 12000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();

				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-victim-name').focus();
			}
		});
	});

	$('#oadh-victimizer-btn-edit').click(function()
	{
		var rowData;

		$('.decima-erp-tooltip').tooltip('hide');

		if(!$('#oadh-victimizer-grid').isRowSelected())
		{
			$('#oadh-victimizer-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$('#oadh-victimizer-btn-toolbar').disabledButtonGroup();
		$('#oadh-victimizer-btn-group-3').enableButtonGroup();


		rowData = $('#oadh-victimizer-grid').getRowData($('#oadh-victimizer-grid').jqGrid('getGridParam', 'selrow'));
		populateFormFields(rowData);

		// console.log(rowData);

		if(rowData.oadh_victimizer_subtype == 'P')
		{
			$('#oadh-victimizer-subtype-p').click();

			setTimeout(function() {
				if(rowData.oadh_victim_relation == "true")
				{
					$('#oadh-victimizer-relation-yes').click();
				}
				else
				{
					$('#oadh-victimizer-relation-no').click();
				}
			}, 500);
		}
		else
		{
			$('#oadh-victimizer-subtype-s').click();

			setTimeout(function() {
				if(rowData.oadh_victimizer_relation == "true")
				{
					$('#oadh-victimizer-second-relation-yes').click();
				}
				else
				{
					$('#oadh-victimizer-second-relation-no').click();
				}
			}, 500);
		}

		// $('#oadh-victimizer-label').setAutocompleteLabel(rowData.module_app_victimizer_id);

		$('#oadh-victimizer-department').focus();
	});

	$('#oadh-victimizer-btn-delete').click(function()
	{
		var id = $('#oadh-victimizer-grid').getSelectedRowsIdCell('oadh_victimizer_id');

		if(!$('#oadh-victimizer-grid').isRowSelected())
		{
			$('#oadh-victimizer-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
			return;
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token':$('#app-token').val(),
					'id': id,
					'news_id': $('#oadh-victimizer-news-id').val()
				}
			),
			dataType : 'json',
			url:  $('#oadh-victimizer-form').attr('action') + '/delete-people',
			error: function (jqXHR, textStatus, errorThrown)
			{
				handleServerExceptions(jqXHR, 'oadh-victimizer-btn-toolbar', false);
			},
			beforeSend:function()
			{
				$('#app-loader').removeClass('hidden');
				disabledAll();
			},
			success:function(json)
			{
				if(json.success)
				{
					$('#oadh-victimizer-btn-refresh').click();
					$("#oadh-victimizer-btn-group-2").disabledButtonGroup();
					$('#oadh-victimizer-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
				}

				$('#app-loader').addClass('hidden');
				enableAll();
				$('.decima-erp-tooltip').tooltip('hide');
			}
		});
	});

});
</script>
