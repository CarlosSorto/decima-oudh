@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-rim-new-action', null, array('id' => 'oadh-rim-new-action')) !!}
{!! Form::hidden('oadh-rim-edit-action', null, array('id' => 'oadh-rim-edit-action', 'data-content' => Lang::get('decima-oadh::institution-management.editHelpText'))) !!}
{!! Form::hidden('oadh-rim-remove-action', null, array('id' => 'oadh-rim-remove-action', 'data-content' => Lang::get('decima-oadh::institution-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-rim-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-rim-btn-delete-helper', 'class' => 'hidden')) !!}
<style></style>

<script type='text/javascript'>

	//For grids with multiselect enabled
	function oadhRimOnSelectRowEvent(id)
	{
		var selRowIds = $('#oadh-rim-grid').jqGrid('getGridParam', 'selarrrow'), id;

		if(selRowIds.length == 0)
		{
			$('#oadh-rim-btn-group-2').disabledButtonGroup();
			cleanJournals('oadh-rim-');
			// cleanFiles('oadh-rim-')
		}
		else if(selRowIds.length == 1)
		{
			$('#oadh-rim-btn-group-2').enableButtonGroup();

			id = $('#oadh-rim-grid').getSelectedRowId('oadh_rim_id');

			if($('#oadh-rim-journals').attr('data-journalized-id') != id)
			{
				cleanJournals('oadh-rim-');
				// getElementFiles('oadh-rim-', id);
				getAppJournals('oadh-rim-','firstPage', id);
			}

		}
		else if(selRowIds.length > 1)
		{
			$('#oadh-rim-btn-group-2').disabledButtonGroup();
			$('#oadh-rim-btn-delete').removeAttr('disabled');
			cleanJournals('oadh-rim-');
			// cleanFiles('oadh-rim-')
		}
	}

	/*
	//For grids with multiselect disabled
	function oadhRimOnSelectRowEvent()
	{
		var id = $('#oadh-rim-grid').getSelectedRowId('oadh_rim_id');

		if($('#oadh-rim-grid-section').attr('data-id') != id)
		{
			$('#oadh-rim-grid-section').attr('data-id', id);
			getAppJournals('oadh-rim-', 'firstPage', id);
			// getElementFiles('oadh-rim-', id);
		}

		$('#oadh-rim-btn-group-2').enableButtonGroup();
	}
	*/

	$(document).ready(function()
	{
		// loadSmtRows('oadhRimSmtRows', $('#oadh-rim-form').attr('action') + '/smt-rows');

		$('.oadh-rim-btn-tooltip').tooltip();

		$('#oadh-rim-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-rim-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-rim-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-rim-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-rim-').focus();
		});

		$('#oadh-rim-').focusout(function()
		{
			$('#oadh-rim-btn-save').focus();
		});

		$('#oadh-rim-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-rim-grid-section').collapse('show');
		});

		$('#oadh-rim-btn-new').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-rim-btn-toolbar').disabledButtonGroup();

			$('#oadh-rim-btn-group-3').enableButtonGroup();

			$('#oadh-rim-form-new-title').removeClass('hidden');

			$('#oadh-rim-journals-section').attr('data-target-id', '#oadh-rim-form-section');

			$('#oadh-rim-grid-section').collapse('hide');

			$('.decima-erp-tooltip').tooltip('hide');
		});

		$('#oadh-rim-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-rim-btn-toolbar').disabledButtonGroup();

			$('#oadh-rim-btn-group-1').enableButtonGroup();

			if($('#oadh-rim-journals-section').attr('data-target-id') == '' || $('#oadh-rim-journals-section').attr('data-target-id') == '#oadh-rim-form-section')
			{
				$('#oadh-rim-grid').trigger('reloadGrid');

				$('#oadh-rim-grid-section').attr('data-id', '');

				cleanJournals('oadh-rim-');
				// cleanFiles('oadh-rim-');
			}
			else
			{

			}
		});

		$('#oadh-rim-btn-export-xls').click(function()
		{
			if($('#oadh-rim-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-rim-gridXlsButton').click();
			}
			else
			{

			}
		});

		$('#oadh-rim-btn-export-csv').click(function()
		{
			if($('#oadh-rim-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-rim-gridCsvButton').click();
			}
			else
			{

			}
		});

		$('#oadh-rim-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rim-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rim-grid').isRowSelected())
				{
					$('#oadh-rim-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				$('#oadh-rim-btn-toolbar').disabledButtonGroup();

				$('#oadh-rim-btn-group-3').enableButtonGroup();

				$('#oadh-rim-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-rim-grid').getRowData($('#oadh-rim-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				$('#oadh-rim-form-edit-title').removeClass('hidden');

				$('#oadh-rim-journals-section').attr('data-target-id', '#oadh-rim-form-section');

				$('#oadh-rim-grid-section').collapse('hide');
			}
			else
			{

			}
		});

		$('#oadh-rim-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-rim-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rim-grid').isRowSelected())
				{
					$('#oadh-rim-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-rim-grid').getRowData($('#oadh-rim-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-rim-delete-message').html($('#oadh-rim-delete-message').attr('data-default-label').replace(':name', rowData.oadh_rim_name));
			}
			else
			{

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-rim-modal-delete').modal('show');
		});

		$('#oadh-rim-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-rim-journals-section').attr('data-target-id') == '')
			{
				url = $('#oadh-rim-form').attr('action') + '/delete';
				//For grids with multiselect enabled
				id = $('#oadh-rim-grid').getSelectedRowsIdCell('oadh_rim_id');
				//For grids with multiselect disabled
				// id = $('#oadh-rim-grid').getSelectedRowId('oadh_rim_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify({'_token':$('#app-token').val(), 'id':id}),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rim-btn-toolbar', false);
					$('#oadh-rim-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rim-btn-refresh').click();
						$('#oadh-rim-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}
					else if(json.info)
					{
						$('#oadh-rim-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 12000);
					}

					if(!empty(json.smtRowId))
					{
						deleteSmtRow('oadhRimSmtRows', json.smtRowId);
					}

					$('#oadh-rim-modal-delete').modal('hide');
					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rim-btn-save').click(function()
		{
			var url = $('#oadh-rim-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rim-journals-section').attr('data-target-id') == '#oadh-rim-form-section')
			{
				if(!$('#oadh-rim-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-rim-id').isEmpty())
				{
					url = url + '/create';
				}
				else
				{
					url = url + '/update';
					action = 'edit';
				}

				data = $('#oadh-rim-form').formToObject('oadh-rim-');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rim-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-rim-journals-section').attr('data-target-id') == '#oadh-rim-form-section')
						{
							$('#oadh-rim-btn-close').click();
							$('#oadh-rim-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
						}
						else
						{
							// $('#oadh-rim-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-rim-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-rim-journals-section').attr('data-target-id') == '#oadh-rim-form-section')
						{
							$('#oadh-rim-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-rim-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					if(action == 'new' && !empty(json.smtRow))
					{
						addSmtRow('oadhRimSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					if(action == 'edit' && !empty(json.smtRow))
					{
						updateSmtRow('oadhRimSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					$('#app-loader').addClass('hidden');

					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rim-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			// oadh-rim-form-section
			if($('#oadh-rim-journals-section').attr('data-target-id') == '#oadh-rim-form-section')
			{
				$('#oadh-rim-form-new-title').addClass('hidden');
				$('#oadh-rim-form-edit-title').addClass('hidden');

				$('#oadh-rim-btn-refresh').click();

				$('#oadh-rim-form').jqMgVal('clearForm');

				$('#oadh-rim-form-section').collapse('hide');
			}
			else
			{

			}

			$('#oadh-rim-btn-group-1').enableButtonGroup();
			$('#oadh-rim-btn-group-3').disabledButtonGroup();
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-rim-journals-section').attr('data-target-id', '')
		});

		$('#oadh-rim-smt-btn-select').click(function()
		{
			var rowData = $('#oadh-rim-smt').getSelectedSmtRow();

			if(empty(rowData))
			{
				return;
			}

			$('#oadh-rim-btn-new').click();

			populateFormFields(rowData);

			$('#oadh-rim-form-new-title').addClass('hidden');

			$('#oadh-rim-form-edit-title').removeClass('hidden');

			$('#oadh-rim-smt').modal('hide');

			$('#oadh-rim-id').val(rowData.oadh_rim_id);
		});

		$('#oadh-rim-smt-btn-refresh').click(function()
		{
			// loadSmtRows('oadhRimSmtRows', $('#oadh-rim-form').attr('action') + '/smt-rows', '', true, true);
		});

		$('#oadh-rim-btn-edit-helper').click(function()
	  {
			showButtonHelper('oadh-rim-btn-close', 'oadh-rim-btn-group-2', $('#oadh-rim-edit-action').attr('data-content'));
	  });

		$('#oadh-rim-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-rim-btn-close', 'oadh-rim-btn-group-2', $('#oadh-rim-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-rim-new-action').isEmpty())
		{
			$('#oadh-rim-btn-new').click();
		}

		if(!$('#oadh-rim-edit-action').isEmpty())
		{
			showButtonHelper('oadh-rim-btn-close', 'oadh-rim-btn-group-2', $('#oadh-rim-edit-action').attr('data-content'));
		}

		if(!$('#oadh-rim-delete-action').isEmpty())
		{
			showButtonHelper('oadh-rim-btn-close', 'oadh-rim-btn-group-2', $('#oadh-rim-delete-action').attr('data-content'));
		}
	});
</script>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-rim-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-rim-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new'), array('id' => 'oadh-rim-btn-new', 'class' => 'btn btn-default oadh-rim-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-oadh::institution-management.new'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-rim-btn-refresh', 'class' => 'btn btn-default oadh-rim-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
         		<li><a id='oadh-rim-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> {{ Lang::get('decima-accounting::journal-management.standardSpreadsheet') . ' (' . Lang::get('form.spreadsheet') . ')' }}</a></li>
         		<!-- <li><a id='oadh-rim-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li> -->
       		</ul>
				</div>
			</div>
			<div id="oadh-rim-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-rim-btn-edit', 'class' => 'btn btn-default oadh-rim-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::institution-management.edit'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-rim-btn-delete', 'class' => 'btn btn-default oadh-rim-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::institution-management.delete'))) !!}
				{{-- Form::button('<i class="fa fa-upload"></i> ', array('id' => 'oadh-rim-btn-upload', 'class' => 'btn btn-default oadh-rim-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-folder' => Lang::get('decima-oadh::institution-management.folder'), 'data-original-title' => Lang::get('toolbar.uploadLongText'))) --}}
				{{-- Form::button('<i class="fa fa-files-o"></i> ', array('id' => 'oadh-rim-btn-show-files', 'class' => 'btn btn-default oadh-rim-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showFilesLongText'))) --}}
				{{-- Form::button('<i class="fa fa-history"></i> ', array('id' => 'oadh-rim-btn-show-history', 'class' => 'btn btn-default oadh-rim-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showHistoryLongText'))) --}}
			</div>
			<div id="oadh-rim-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-rim-btn-save', 'class' => 'btn btn-default oadh-rim-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::institution-management.save'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-rim-btn-close', 'class' => 'btn btn-default oadh-rim-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-rim-grid-section' class='app-grid collapse in' data-id='' data-app-grid-id='oadh-rim-grid'>
			{!!
			GridRender::setGridId('oadh-rim-grid')
				->enablefilterToolbar(false, false)
				->hideXlsExporter()
  			->hideCsvExporter()
	    	->setGridOption('url',URL::to('ucaoadh/recommendations/institutions-management/grid-data'))
	    	->setGridOption('caption', Lang::get('decima-oadh::institution-management.gridTitle'))
				->setGridOption('filename', Lang::get('decima-oadh::institution-management.gridTitle'))
	    	->setGridOption('postData',array('_token' => Session::token()))
				->setGridEvent('onSelectRow', 'oadhRimOnSelectRowEvent')
	    	->addColumn(array('index' => 'id', 'name' => 'oadh_rim_id', 'hidden' => true))
	    	->addColumn(array('label' => Lang::get('form.abbreviation'), 'index' => 'abbreviation' ,'name' => 'oadh_rim_abbreviation'))
	    	->addColumn(array('label' => Lang::get('form.name'), 'index' => 'name' ,'name' => 'oadh_rim_name'))
				//->addColumn(array('label' => Lang::get('form.status'), 'index' => 'status', 'name' => 'oadh_rim_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'width' => 80))
				// ->addColumn(array('label' => Lang::get('form.status'), 'index' => 'status', 'name' => 'oadh_rim_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::institution-management.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select'))
				// ->addColumn(array('label' => Lang::get('form.active'), 'index' => 'is_active' ,'name' => 'oadh_rim_is_active', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.booleanText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70))
				// ->addColumn(array('label' => Lang::get('decima-oadh::institution-management.money'), 'index' => 'money', 'name' => 'money', 'formatter' => 'currency', 'align'=>'right', 'width' => 100, 'hidden' => false, 'formatoptions' => array('prefix' => OrganizationManager::getOrganizationCurrencySymbol() . ' ')))
	    	->renderGrid();
			!!}
		</div>
	</div>
</div>
<div id='oadh-rim-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-rim-form', 'url' => URL::to('ucaoadh/recommendations/institutions-management'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-rim-form-new-title" class="hidden">{{ Lang::get('decima-oadh::institution-management.formNewTitle') }}</legend>
				<legend id="oadh-rim-form-edit-title" class="hidden">{{ Lang::get('decima-oadh::institution-management.formEditTitle') }}</legend>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rim-abbreviation', Lang::get('form.abbreviation'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rim-abbreviation', null , array('id' => 'oadh-rim-abbreviation', 'class' => 'form-control', 'data-mg-required' => '')) !!}
					    {!! Form::hidden('oadh-rim-id', null, array('id' => 'oadh-rim-id')) !!}
			  		</div>
					</div>
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rim-name', Lang::get('form.name'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rim-name', null , array('id' => 'oadh-rim-name', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div id='oadh-rim-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals('oadh-rim-', $appInfo['id']) !!}
</div>
@include('layouts.search-modal-table')
<div id='oadh-rim-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-rim-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-rim-delete-message">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
				<!-- <p id="oadh-rim-delete-message" data-default-label="{{ Lang::get('decima-oadh::institution-management.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-rim-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
