@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-rrm-new-action', null, array('id' => 'oadh-rrm-new-action')) !!}
{!! Form::hidden('oadh-rrm-edit-action', null, array('id' => 'oadh-rrm-edit-action', 'data-content' => Lang::get('decima-oadh::right-management.editHelpText'))) !!}
{!! Form::hidden('oadh-rrm-remove-action', null, array('id' => 'oadh-rrm-remove-action', 'data-content' => Lang::get('decima-oadh::right-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-rrm-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-rrm-btn-delete-helper', 'class' => 'hidden')) !!}
<style></style>

<script type='text/javascript'>

	//For grids with multiselect enabled
	function oadhRrmOnSelectRowEvent(id)
	{
		var selRowIds = $('#oadh-rrm-grid').jqGrid('getGridParam', 'selarrrow'), id;

		if(selRowIds.length == 0)
		{
			$('#oadh-rrm-btn-group-2').disabledButtonGroup();
			cleanJournals('oadh-rrm-');
			// cleanFiles('oadh-rrm-')
		}
		else if(selRowIds.length == 1)
		{
			$('#oadh-rrm-btn-group-2').enableButtonGroup();

			id = $('#oadh-rrm-grid').getSelectedRowId('oadh_rrm_id');

			if($('#oadh-rrm-journals').attr('data-journalized-id') != id)
			{
				cleanJournals('oadh-rrm-');
				// getElementFiles('oadh-rrm-', id);
				getAppJournals('oadh-rrm-','firstPage', id);
			}

		}
		else if(selRowIds.length > 1)
		{
			$('#oadh-rrm-btn-group-2').disabledButtonGroup();
			$('#oadh-rrm-btn-delete').removeAttr('disabled');
			cleanJournals('oadh-rrm-');
			// cleanFiles('oadh-rrm-')
		}
	}

	/*
	//For grids with multiselect disabled
	function oadhRrmOnSelectRowEvent()
	{
		var id = $('#oadh-rrm-grid').getSelectedRowId('oadh_rrm_id');

		if($('#oadh-rrm-grid-section').attr('data-id') != id)
		{
			$('#oadh-rrm-grid-section').attr('data-id', id);
			getAppJournals('oadh-rrm-', 'firstPage', id);
			// getElementFiles('oadh-rrm-', id);
		}

		$('#oadh-rrm-btn-group-2').enableButtonGroup();
	}
	*/

	$(document).ready(function()
	{
		// loadSmtRows('oadhRrmSmtRows', $('#oadh-rrm-form').attr('action') + '/smt-rows');

		$('.oadh-rrm-btn-tooltip').tooltip();

		$('#oadh-rrm-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-rrm-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-rrm-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-rrm-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-rrm-').focus();
		});

		$('#oadh-rrm-').focusout(function()
		{
			$('#oadh-rrm-btn-save').focus();
		});

		$('#oadh-rrm-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-rrm-grid-section').collapse('show');
		});

		$('#oadh-rrm-btn-new').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-rrm-btn-toolbar').disabledButtonGroup();

			$('#oadh-rrm-btn-group-3').enableButtonGroup();

			$('#oadh-rrm-form-new-title').removeClass('hidden');

			$('#oadh-rrm-journals-section').attr('data-target-id', '#oadh-rrm-form-section');

			$('#oadh-rrm-grid-section').collapse('hide');

			$('.decima-erp-tooltip').tooltip('hide');
		});

		$('#oadh-rrm-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-rrm-btn-toolbar').disabledButtonGroup();

			$('#oadh-rrm-btn-group-1').enableButtonGroup();

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '' || $('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
			{
				$('#oadh-rrm-grid').trigger('reloadGrid');

				$('#oadh-rrm-grid-section').attr('data-id', '');

				cleanJournals('oadh-rrm-');
				// cleanFiles('oadh-rrm-');
			}
			else
			{

			}
		});

		$('#oadh-rrm-btn-export-xls').click(function()
		{
			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-rrm-gridXlsButton').click();
			}
			else
			{

			}
		});

		$('#oadh-rrm-btn-export-csv').click(function()
		{
			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-rrm-gridCsvButton').click();
			}
			else
			{

			}
		});

		$('#oadh-rrm-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rrm-grid').isRowSelected())
				{
					$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				$('#oadh-rrm-btn-toolbar').disabledButtonGroup();

				$('#oadh-rrm-btn-group-3').enableButtonGroup();

				$('#oadh-rrm-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-rrm-grid').getRowData($('#oadh-rrm-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				$('#oadh-rrm-form-edit-title').removeClass('hidden');

				$('#oadh-rrm-journals-section').attr('data-target-id', '#oadh-rrm-form-section');

				$('#oadh-rrm-grid-section').collapse('hide');
			}
			else
			{

			}
		});

		$('#oadh-rrm-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rrm-grid').isRowSelected())
				{
					$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-rrm-grid').getRowData($('#oadh-rrm-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-rrm-delete-message').html($('#oadh-rrm-delete-message').attr('data-default-label').replace(':name', rowData.oadh_rrm_name));
			}
			else
			{

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-rrm-modal-delete').modal('show');
		});

		$('#oadh-rrm-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				url = $('#oadh-rrm-form').attr('action') + '/delete';
				//For grids with multiselect enabled
				id = $('#oadh-rrm-grid').getSelectedRowsIdCell('oadh_rrm_id');
				//For grids with multiselect disabled
				// id = $('#oadh-rrm-grid').getSelectedRowId('oadh_rrm_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify({'_token':$('#app-token').val(), 'id':id}),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrm-btn-toolbar', false);
					$('#oadh-rrm-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrm-btn-refresh').click();
						$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}
					else if(json.info)
					{
						$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 12000);
					}

					if(!empty(json.smtRowId))
					{
						deleteSmtRow('oadhRrmSmtRows', json.smtRowId);
					}

					$('#oadh-rrm-modal-delete').modal('hide');
					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rrm-btn-save').click(function()
		{
			var url = $('#oadh-rrm-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
			{
				if(!$('#oadh-rrm-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-rrm-id').isEmpty())
				{
					url = url + '/create';
				}
				else
				{
					url = url + '/update';
					action = 'edit';
				}

				data = $('#oadh-rrm-form').formToObject('oadh-rrm-');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrm-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
						{
							$('#oadh-rrm-btn-close').click();
							$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
						}
						else
						{
							// $('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-rrm-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
						{
							$('#oadh-rrm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-rrm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					if(action == 'new' && !empty(json.smtRow))
					{
						addSmtRow('oadhRrmSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					if(action == 'edit' && !empty(json.smtRow))
					{
						updateSmtRow('oadhRrmSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					$('#app-loader').addClass('hidden');

					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rrm-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			// oadh-rrm-form-section
			if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
			{
				$('#oadh-rrm-form-new-title').addClass('hidden');
				$('#oadh-rrm-form-edit-title').addClass('hidden');

				$('#oadh-rrm-btn-refresh').click();

				$('#oadh-rrm-form').jqMgVal('clearForm');

				$('#oadh-rrm-form-section').collapse('hide');
			}
			else
			{

			}

			$('#oadh-rrm-btn-group-1').enableButtonGroup();
			$('#oadh-rrm-btn-group-3').disabledButtonGroup();
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-rrm-journals-section').attr('data-target-id', '')
		});

		$('#oadh-rrm-smt-btn-select').click(function()
		{
			var rowData = $('#oadh-rrm-smt').getSelectedSmtRow();

			if(empty(rowData))
			{
				return;
			}

			$('#oadh-rrm-btn-new').click();

			populateFormFields(rowData);

			$('#oadh-rrm-form-new-title').addClass('hidden');

			$('#oadh-rrm-form-edit-title').removeClass('hidden');

			$('#oadh-rrm-smt').modal('hide');

			$('#oadh-rrm-id').val(rowData.oadh_rrm_id);
		});

		$('#oadh-rrm-smt-btn-refresh').click(function()
		{
			// loadSmtRows('oadhRrmSmtRows', $('#oadh-rrm-form').attr('action') + '/smt-rows', '', true, true);
		});

		$('#oadh-rrm-btn-edit-helper').click(function()
	  {
			showButtonHelper('oadh-rrm-btn-close', 'oadh-rrm-btn-group-2', $('#oadh-rrm-edit-action').attr('data-content'));
	  });

		$('#oadh-rrm-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-rrm-btn-close', 'oadh-rrm-btn-group-2', $('#oadh-rrm-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-rrm-new-action').isEmpty())
		{
			$('#oadh-rrm-btn-new').click();
		}

		if(!$('#oadh-rrm-edit-action').isEmpty())
		{
			showButtonHelper('oadh-rrm-btn-close', 'oadh-rrm-btn-group-2', $('#oadh-rrm-edit-action').attr('data-content'));
		}

		if(!$('#oadh-rrm-delete-action').isEmpty())
		{
			showButtonHelper('oadh-rrm-btn-close', 'oadh-rrm-btn-group-2', $('#oadh-rrm-delete-action').attr('data-content'));
		}
	});
</script>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-rrm-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-rrm-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new'), array('id' => 'oadh-rrm-btn-new', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-oadh::right-management.new'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-rrm-btn-refresh', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
         		<li><a id='oadh-rrm-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> {{ Lang::get('decima-accounting::journal-management.standardSpreadsheet') . ' (' . Lang::get('form.spreadsheet') . ')' }}</a></li>
         		<!-- <li><a id='oadh-rrm-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li> -->
       		</ul>
				</div>
			</div>
			<div id="oadh-rrm-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-rrm-btn-edit', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::right-management.edit'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-rrm-btn-delete', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::right-management.delete'))) !!}
				{{-- Form::button('<i class="fa fa-upload"></i> ', array('id' => 'oadh-rrm-btn-upload', 'class' => 'btn btn-default oadh-rrm-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-folder' => Lang::get('decima-oadh::right-management.folder'), 'data-original-title' => Lang::get('toolbar.uploadLongText'))) --}}
				{{-- Form::button('<i class="fa fa-files-o"></i> ', array('id' => 'oadh-rrm-btn-show-files', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showFilesLongText'))) --}}
				{{-- Form::button('<i class="fa fa-history"></i> ', array('id' => 'oadh-rrm-btn-show-history', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showHistoryLongText'))) --}}
			</div>
			<div id="oadh-rrm-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-rrm-btn-save', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::right-management.save'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-rrm-btn-close', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-rrm-grid-section' class='app-grid collapse in' data-id='' data-app-grid-id='oadh-rrm-grid'>
			{!!
			GridRender::setGridId('oadh-rrm-grid')
				->enablefilterToolbar(false, false)
				->hideXlsExporter()
  			->hideCsvExporter()
	    	->setGridOption('url',URL::to('ucaoadh/recommendations/right-management/grid-data'))
	    	->setGridOption('caption', Lang::get('decima-oadh::right-management.gridTitle'))
				->setGridOption('filename', Lang::get('decima-oadh::right-management.gridTitle'))
	    	->setGridOption('postData',array('_token' => Session::token()))
				->setGridEvent('onSelectRow', 'oadhRrmOnSelectRowEvent')
	    	->addColumn(array('index' => 'id', 'name' => 'oadh_rrm_id', 'hidden' => true))
	    	->addColumn(array('label' => Lang::get('form.name'), 'index' => 'name' ,'name' => 'oadh_rrm_name'))
				//->addColumn(array('label' => Lang::get('form.status'), 'index' => 'status', 'name' => 'oadh_rrm_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'width' => 80))
				// ->addColumn(array('label' => Lang::get('form.status'), 'index' => 'status', 'name' => 'oadh_rrm_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::right-management.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select'))
				// ->addColumn(array('label' => Lang::get('form.active'), 'index' => 'is_active' ,'name' => 'oadh_rrm_is_active', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.booleanText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70))
				// ->addColumn(array('label' => Lang::get('decima-oadh::right-management.money'), 'index' => 'money', 'name' => 'money', 'formatter' => 'currency', 'align'=>'right', 'width' => 100, 'hidden' => false, 'formatoptions' => array('prefix' => OrganizationManager::getOrganizationCurrencySymbol() . ' ')))
	    	->renderGrid();
			!!}
		</div>
	</div>
</div>
<div id='oadh-rrm-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-rrm-form', 'url' => URL::to('ucaoadh/recommendations/right-management'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-rrm-form-new-title" class="hidden">{{ Lang::get('decima-oadh::right-management.formNewTitle') }}</legend>
				<legend id="oadh-rrm-form-edit-title" class="hidden">{{ Lang::get('decima-oadh::right-management.formEditTitle') }}</legend>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-name', Lang::get('form.name'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rrm-name', null , array('id' => 'oadh-rrm-name', 'class' => 'form-control', 'data-mg-required' => '')) !!}
					    {!! Form::hidden('oadh-rrm-id', null, array('id' => 'oadh-rrm-id')) !!}
			  		</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div id='oadh-rrm-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals('oadh-rrm-', $appInfo['id']) !!}
</div>
@include('layouts.search-modal-table')
<div id='oadh-rrm-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-rrm-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-rrm-delete-message">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
				<!-- <p id="oadh-rrm-delete-message" data-default-label="{{ Lang::get('decima-oadh::right-management.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-rrm-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
