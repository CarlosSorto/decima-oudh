@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-rrm-new-action', null, array('id' => 'oadh-rrm-new-action')) !!}
{!! Form::hidden('oadh-rrm-edit-action', null, array('id' => 'oadh-rrm-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-rrm-remove-action', null, array('id' => 'oadh-rrm-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-rrm-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-rrm-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-rrm-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-rrm-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">
	var oadhRrmInstituions = {!! json_encode($institutions) !!};
	var oadhRrmRights = {!! json_encode($rights) !!};
	var organizationVariableName;
	/*
	* Funcion para llamar todas las variables globales y/o almacenadas que se requieran,
	* Agregar parametros dependiendo las variables que necesite
	* Se agregan parametros despues del forceAjaxRequest, dejandoles valor por defecto false
	*/
	function oadhRrmLoadFiltersData(initializeFields = false, forceAjaxRequest, articles = false , showLoader = false) 
	{
		if(initializeFields)
		{
			if(isInDecimaStorage('organizationVariableName', 'globalJs'))
			{
				articles = false
			}
		}

		$.ajax(
		{
			type: 'POST',
			data: JSON.stringify(
				{
					'_token': $('#app-token').val(),
					'articles': articles
				}),
			dataType: 'json',
			url: $('#app-url').val() + '/ucaoadh/recommendations/recommendations-management/filters-data',
			error: function (jqXHR, textStatus, errorThrown) 
			{
				handleServerExceptions(jqXHR, '', false);
			},
			beforeSend: function() 
			{
				spinBreadcrumbLoader();
			},
			success: function(json)
			{
				if(!empty(json.articles))
				{
					// setDecimaDataSource('organizationVariableName', json.articles, 'globalJs');
				}

				stopBreadcrumbLoader();

				if(showLoader)
				{
					$('#app-loader').addClass('hidden hidden-xs-up')
					enableAll();
				}
			}
		})
	}

	function oadhRrmEnableInsDetailForm()
	{
		$('#oadh-rrm-ins-detail-form-fieldset').removeAttr('disabled');
	}

	function oadhRrmDisabledInsDetailForm()
	{
		$('#oadh-rrm-ins-detail-form-fieldset').attr('disabled', 'disabled');
		$('#oadh-rrm-ins-detail-btn-toolbar').disabledButtonGroup();
	}

	function oadhRrmEnableRigDetailForm()
	{
		$('#oadh-rrm-rig-detail-form-fieldset').removeAttr('disabled');
	}

	function oadhRrmDisabledRigDetailForm()
	{
		$('#oadh-rrm-rig-detail-form-fieldset').attr('disabled', 'disabled');
		$('#oadh-rrm-rig-detail-btn-toolbar').disabledButtonGroup();
	}

	function oadhRrmOnSelectRowEvent()
	{
		var id = $('#oadh-rrm-grid').getSelectedRowId('oadh_rrt_id'), rowData = $('#oadh-rrm-grid').getRowData($('#oadh-rrm-grid').jqGrid('getGridParam', 'selrow'));

		if($('#oadh-rrm-grid-section').attr('data-id') != id)
		{
			$('#oadh-rrm-grid-section').attr('data-id', id);

			// getAppJournals('oadh-rrm-', 'firstPage', id);
			// getElementFiles('oadh-rrm-', id);

			// $('#oadh-rrm-front-ins-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');
			// $('#oadh-rrm-front-rig-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');

			// cleanJournals('oadh-rrm-');
			// cleanFiles('oadh-rrm-');
		}

		$('#oadh-rrm-btn-group-2, #oadh-rrm-btn-group-4').enableButtonGroup();

		// if(rowData.oadh_rrt_status != 'P')
		// {
		// 	$('#oadh-rrm-btn-authorize').attr('disabled', 'disabled');
		// }
		//
		// if(rowData.oadh_rrt_status == 'A')
		// {
		// 	$('#oadh-rrm-btn-void').attr('disabled', 'disabled');
		// }
	}

	function oadhRrmInsDetailOnSelectRowEvent()
	{
		var selRowIds = $('#oadh-rrm-back-ins-detail-grid').jqGrid('getGridParam', 'selarrrow');

		if(selRowIds.length == 0)
		{
			$('#oadh-rrm-ins-detail-btn-group-2').disabledButtonGroup();
		}
		else if(selRowIds.length == 1)
		{
			$('#oadh-rrm-ins-detail-btn-group-2').enableButtonGroup();
		}
		else if(selRowIds.length > 1)
		{
			$('#oadh-rrm-ins-detail-btn-group-2').disabledButtonGroup();
			$('#oadh-rrm-ins-detail-btn-delete').removeAttr('disabled');
		}
	}

	function oadhRrmRigDetailOnSelectRowEvent()
	{
		var selRowIds = $('#oadh-rrm-back-rig-detail-grid').jqGrid('getGridParam', 'selarrrow');

		if(selRowIds.length == 0)
		{
			$('#oadh-rrm-rig-detail-btn-group-2').disabledButtonGroup();
		}
		else if(selRowIds.length == 1)
		{
			$('#oadh-rrm-rig-detail-btn-group-2').enableButtonGroup();
		}
		else if(selRowIds.length > 1)
		{
			$('#oadh-rrm-rig-detail-btn-group-2').disabledButtonGroup();
			$('#oadh-rrm-rig-detail-btn-delete').removeAttr('disabled');
		}
	}

	function oadhRrmEnableSelectedField()
	{
		//enable selected fields that could be disabled after authorize or void when form is visible	
	}

	function oadhRrmDisableSelectedField()
	{
		//disable selected fields after authorize or void when form is visible	
	}

	function oadhRrmOnLoadCompleteEvent()
	{
		// $('#oadh-rrm-ins-front-detail-grid').jqGrid('clearGridData');
		// $('#oadh-rrm-rig-front-detail-grid').jqGrid('clearGridData');
		//
		// $('#oadh-rrm-ins-front-detail-grid').jqGrid('footerData','set', {
		// $('#oadh-rrm-rig-front-detail-grid').jqGrid('footerData','set', {
		// 	'oadh_rrm_detail_': 0,
		// });
	}

	function oadhRrmInsDetailOnLoadCompleteEvent()
	{
		// var amount = $(this).jqGrid('getCol', 'oadh_rrm_detail_', false, 'sum');
		//
		// $(this).jqGrid('footerData','set', {
		// 	'oadh_rrm_ins_detail_name': '{{ Lang::get('form.total') }}',
		// 	'oadh_rrm_detail_': amount,
		// });
		//
		// $('#oadh-rrm-ins-detail-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
	}

	function oadhRrmRigDetailOnLoadCompleteEvent()
	{
		// var amount = $(this).jqGrid('getCol', 'oadh_rrm_detail_', false, 'sum');
		//
		// $(this).jqGrid('footerData','set', {
		// 	'oadh_rrm_rig_detail_name': '{{ Lang::get('form.total') }}',
		// 	'oadh_rrm_detail_': amount,
		// });
		//
		// $('#oadh-rrm-rig-detail-').val($.fmatter.NumberFormat(amount, $.fn.jqMgVal.defaults.validators.money.formatter));
	}

	function oadhRrmInitForm(data)
	{
		data = data || {};

		$('#oadh-rrm-form, #oadh-rrm-ins-detail-form, #oadh-rrm-rig-detail-form').jqMgVal('clearForm');

		$('#oadh-rrm-btn-toolbar, #oadh-rrm-ins-detail-btn-toolbar, #oadh-rrm-rig-detail-btn-toolbar').disabledButtonGroup();

		// $('#oadh-rrm-status-label').val($('#oadh-rrm-status-label').attr('data-default-value'));
		// $('#oadh-rrm-status').val($('#oadh-rrm-status').attr('data-default-value'));

		// $('#oadh-rrm-btn-new, #oadh-rrm-btn-edit').removeAttr('disabled');
		$('#oadh-rrm-btn-new').removeAttr('disabled');

		$('#oadh-rrm-btn-group-3').enableButtonGroup();

		$('#oadh-rrm-form-new-title').removeClass('hidden');

		$('#oadh-rrm-form-edit-title').addClass('hidden');

		oadhRrmEnableSelectedField();
		oadhRrmDisabledInsDetailForm();
		oadhRrmDisabledRigDetailForm();

		if(!empty(data))
		{
			$('#oadh-rrm-btn-group-2, #oadh-rrm-btn-group-3, #oadh-rrm-btn-group-4').enableButtonGroup();
			$('#oadh-rrm-ins-detail-btn-group-1, #oadh-rrm-ins-detail-btn-group-3, #oadh-rrm-rig-detail-btn-group-1, #oadh-rrm-rig-detail-btn-group-3').enableButtonGroup();

			$('#oadh-rrm-form-edit-title').removeClass('hidden');

			$('#oadh-rrm-form-new-title').addClass('hidden');

			$('#oadh-rrm-id, #oadh-rrm-ins-detail-recommendation-id, #oadh-rrm-rig-detail-recommendation-id').val(data.oadh_rrt_id);

			// $('#oadh-rrm-label').setAutocompleteLabel(data.oadh_rrt_id);

			// $('#oadh-rrm-number').attr('data-number', data.oadh_rrt_number);
			$('#oadh-rrm-btn-view').attr('disabled', 'disabled');

			// $('#oadh-rrm-number').html($('#oadh-rrm-number').attr('data-default-label').replace(':number', data.oadh_rrt_number));
			// $('#oadh-rrm-number').html(rowData.oadh_rrt_number);

			// $('#oadh-rrm-status-label').val($('#oadh-rrm-status-label').attr('data-label-' + data.oadh_rrt_status));

			$('#oadh-rrm-ins-detail-btn-refresh').click();
			$('#oadh-rrm-rig-detail-btn-refresh').click();

			oadhRrmEnableInsDetailForm();
			oadhRrmEnableRigDetailForm();

			// if(data.oadh_rrt_status == 'U' || data.oadh_rrt_status == 'A')
			// {
			// 	oadhRrmDisableSelectedField();
			// 	oadhRrmDisabledInsDetailForm();
			// 	oadhRrmDisabledRigDetailForm();
			// }
		}
	}

	$(document).ready(function()
	{
		// loadSmtRows('oadhRrmSmtRows', $('#oadh-rrm-form').attr('action') + '/smt-rows');
		// oadhRrmLoadFiltersData(true, true, true, true)

		oadhRrmDisabledInsDetailForm();
		oadhRrmDisabledRigDetailForm();

		$('.oadh-rrm-btn-tooltip').tooltip();

		$('#oadh-rrm-form, #oadh-rrm-ins-detail-form, #oadh-rrm-rig-detail-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-rrm-grid-section').on('hide.bs.collapse', function ()
		{
			$('#oadh-rrm-filters').hide();
		});

		$('#oadh-rrm-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-rrm-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-rrm-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-rrm-grid-section').collapse('show');

			$('#oadh-rrm-filters').show();
		});

		$('#oadh-rrm-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-rrm-').focus();
		});

		$('#oadh-rrm-').focusout(function()
		{
			$('#oadh-rrm-btn-save').focus();
		});

		$('#oadh-rrm-btn-new').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

      // $('#oadh-rrm-form, #oadh-rrm-ins-detail-form, #oadh-rrm-rig-detail-form').jqMgVal('clearForm');

			// $('#oadh-rrm-ins-detail-btn-toolbar, #oadh-rrm-rig-btn-toolbar, #oadh-rrm-btn-toolbar').disabledButtonGroup();

			// // $('#oadh-rrm-status-label').val($('#oadh-rrm-status-label').attr('data-default-value'));
			// // $('#oadh-rrm-status').val($('#oadh-rrm-status').attr('data-default-value'));

			// $('#oadh-rrm-btn-new, #oadh-rrm-btn-edit').removeAttr('disabled');

			// $('#oadh-rrm-btn-group-3').enableButtonGroup();

			// oadhRrmEnableSelectedField();
			// oadhRrmDisabledInsDetailForm();
			// oadhRrmDisabledRigDetailForm();

			if(!$('#oadh-rrm-form-section').is(":visible"))
			{
				$('#oadh-rrm-journals-section').attr('data-target-id', '#oadh-rrm-form-section');

				$('#oadh-rrm-grid-section').collapse('hide');
				$('#oadh-rrm-btn-close').attr('disabled', false);
				$('#oadh-rrm-btn-save').attr('disabled', false);
			}
			else
			{
				$('#oadh-rrm-ins-detail-recommendation-id').val(-1);
				$('#oadh-rrm-rig-detail-recommendation-id').val(-1);

				$('#oadh-rrm-back-ins-detail-grid').jqGrid('clearGridData');
				$('#oadh-rrm-back-rig-detail-grid').jqGrid('clearGridData');
			}

			// $(this).removeAttr('disabled');

			// $('#oadh-rrm-form-new-title').removeClass('hidden');

			// $('#oadh-rrm-form-edit-title').addClass('hidden');
		});

		$('#oadh-rrm-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-rrm-btn-toolbar').disabledButtonGroup();

			$('#oadh-rrm-btn-group-1').enableButtonGroup();

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '' || $('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
			{
				$('#oadh-rrm-grid').trigger('reloadGrid');

				$('#oadh-rrm-grid-section').attr('data-id', '');

				cleanJournals('oadh-rrm-');
				cleanJournals('oadh-rrm-');
			}
			else
			{

			}
		});

		$('#oadh-rrm-ins-detail-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-rrm-back-ins-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'" + $('#oadh-rrm-ins-detail-recommendation-id').val() + "'}]}"}}).trigger('reloadGrid');
		});

		$('#oadh-rrm-ins-detail-btn-export-xls').click(function()
		{
			$('#oadh-rrm-back-ins-detail-gridXlsButton').click();
		});

		$('#oadh-rrm-ins-detail-btn-export-csv').click(function()
		{
			$('#oadh-rrm-back-ins-detail-gridCsvButton').click();
		});

		$('#oadh-rrm-rig-detail-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-rrm-back-rig-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'" + $('#oadh-rrm-ins-detail-recommendation-id').val() + "'}]}"}}).trigger('reloadGrid');
		});

		$('#oadh-rrm-rig-detail-btn-export-xls').click(function()
		{
			$('#oadh-rrm-back-rig-detail-gridXlsButton').click();
		});

		$('#oadh-rrm-rig-detail-btn-export-csv').click(function()
		{
			$('#oadh-rrm-back-rig-detail-gridCsvButton').click();
		});

		$('#oadh-rrm-btn-export-xls').click(function()
		{
			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-rrm-gridXlsButton').click();
			}
		});

		$('#oadh-rrm-btn-export-csv').click(function()
		{
			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-rrm-gridCsvButton').click();
			}
		});

		$('#oadh-rrm-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rrm-grid').isRowSelected())
				{
					$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);

					return;
				}

				// $('#oadh-rrm-btn-toolbar').disabledButtonGroup();
				// $('#oadh-rrm-ins-detail-btn-toolbar, #oadh-rrm-rig-detail-btn-toolbar').disabledButtonGroup();

				// $('#oadh-rrm-btn-group-2, #oadh-rrm-btn-group-3').enableButtonGroup();
				// $('#oadh-rrm-ins-detail-btn-group-1, #oadh-rrm-ins-detail-btn-group-3').enableButtonGroup();
				// $('#oadh-rrm-rig-detail-btn-group-1, #oadh-rrm-rig-detail-btn-group-3').enableButtonGroup();

				// $('#oadh-rrm-btn-new, #oadh-rrm-btn-upload, #oadh-rrm-btn-show-files, #oadh-rrm-btn-show-history').removeAttr('disabled');

				// $('#oadh-rrm-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-rrm-grid').getRowData($('#oadh-rrm-grid').jqGrid('getGridParam', 'selrow'));

				oadhRrmInitForm(rowData);
				$('#oadh-rrm-system').val(rowData.oadh_rrt_system);
				$('#oadh-rrm-recommendation').val(rowData.oadh_rrt_recommendation);
				$('#oadh-rrm-code').val(rowData.oadh_rrt_code);
				$('#oadh-rrm-date').val(rowData.oadh_rrt_date);
				$('#oadh-rrm-mechanism').val(rowData.oadh_rrt_mechanism);
				$('#oadh-rrm-source').val(rowData.oadh_rrt_source);
				$('#oadh-rrm-legal-base').val(rowData.oadh_rrt_legal_base);
				$('#oadh-rrm-last-modified-date').val(rowData.oadh_rrt_last_modified_date);
				$('#oadh-rrm-observations').val(rowData.oadh_rrt_observations);
				// populateFormFields(rowData);

				$('#oadh-rrm-journals-section').attr('data-target-id', '#oadh-rrm-form-section');

				$('#oadh-rrm-grid-section').collapse('hide');
			}
			else if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
			{
				$('#oadh-rrm-smt').createTable('oadh-rrm-grid', 'oadhRrmSmtRows', 10);
				$('#oadh-rrm-smt').modal('show');
			}
			else
			{

			}
		});

		// $('#oadh-rrm-ins-detail-btn-edit').click(function()
		// {
		// 	var rowData;

		// 	$('.decima-erp-tooltip').tooltip('hide');

		// 	if(!$('#oadh-rrm-back-ins-detail-grid').isRowSelected())
		// 	{
		// 		$('#oadh-rrm-ins-detail-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
		// 		return;
		// 	}

		// 	$('#oadh-rrm-ins-detail-btn-toolbar').disabledButtonGroup();

		// 	$('#oadh-rrm-ins-detail-btn-group-3').enableButtonGroup();

		// 	rowData = $('#oadh-rrm-back-ins-detail-grid').getRowData($('#oadh-rrm-back-ins-detail-grid').jqGrid('getGridParam', 'selrow'));

		// 	populateFormFields(rowData);

		// 	$('#oadh-rrm-ins-detail-institution-label').setAutocompleteLabel(rowData.oadh_rrm_ins_detail_id);

		// 	$('#oadh-rrm-ins-detail-institution-label').focus();
		// });

		// $('#oadh-rrm-rig-detail-btn-edit').click(function()
		// {
		// 	var rowData;

		// 	$('.decima-erp-tooltip').tooltip('hide');

		// 	if(!$('#oadh-rrm-back-rig-detail-grid').isRowSelected())
		// 	{
		// 		$('#oadh-rrm-rig-detail-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
		// 		return;
		// 	}

		// 	$('#oadh-rrm-rig-detail-btn-toolbar').disabledButtonGroup();

		// 	$('#oadh-rrm-rig-detail-btn-group-3').enableButtonGroup();

		// 	rowData = $('#oadh-rrm-back-rig-detail-grid').getRowData($('#oadh-rrm-back-rig-detail-grid').jqGrid('getGridParam', 'selrow'));

		// 	populateFormFields(rowData);

		// 	$('#oadh-rrm-rig-detail-right-label').setAutocompleteLabel(rowData.oadh_rrm_rig_detail_id);

		// 	$('#oadh-rrm-rig-detail-right-label').focus();
		// });

		$('#oadh-rrm-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');
			$.scrollTo({top: top, left:0});

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rrm-grid').isRowSelected())
				{
					$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-rrm-grid').getRowData($('#oadh-rrm-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-rrm-delete-message').html($('#oadh-rrm-delete-message').attr('data-default-label').replace(':name', rowData.name));
			}
			else
			{

			}

			$('#oadh-rrm-modal-delete').modal('show');
		});

		$('#oadh-rrm-btn-view').click(function()
		{
			var id = $('#oadh-rrm-grid').getSelectedRowId('oadh_rrt_id');

			$('#oadh-rrm-front-ins-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');
			$('#oadh-rrm-front-rig-detail-grid').jqGrid('setGridParam', {'datatype':'json', 'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');
		});

		$('#oadh-rrm-btn-show-files').click(function()
		{
			var rowData, id;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rrm-grid').is(":visible"))
			{
				if(!$('#oadh-rrm-grid').isRowSelected())
				{
					$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
					return;
				}

				rowData = $('#oadh-rrm-grid').getRowData($('#oadh-rrm-grid').jqGrid('getGridParam', 'selrow'));
				id = rowData.oadh_rrt_id;
			}
			else
			{
				id = $('#oadh-rrm-id').val();
			}

			getElementFiles('oadh-rrm-', id);

			$.scrollTo({top: $('#oadh-rrm-file-viewer').offset().top - 100, left:0});
		});

		$('#oadh-rrm-btn-show-history').click(function()
		{
			var rowData, id;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rrm-grid').is(":visible"))
			{
				if(!$('#oadh-rrm-grid').isRowSelected())
				{
					$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
					return;
				}

				rowData = $('#oadh-rrm-grid').getRowData($('#oadh-rrm-grid').jqGrid('getGridParam', 'selrow'));
				id = rowData.oadh_rrt_id;
			}
			else
			{
				id = $('#oadh-rrm-id').val();
			}

			getAppJournals('oadh-rrm-', 'firstPage', id);

			$.scrollTo({top: $('#oadh-rrm-journals').offset().top - 100, left:0});
		});

		$('#oadh-rrm-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			{
			  url = $('#oadh-rrm-form').attr('action') + '/delete-master';
			  id = $('#oadh-rrm-grid').getSelectedRowId('oadh_rrt_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id
					}
				),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrm-btn-toolbar', false);
					$('#oadh-rrm-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrm-btn-refresh').click();
						$("#oadh-rrm-btn-group-2").disabledButtonGroup();
						$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					if(json.info)
					{
						$('#oadh-rrm-btn-refresh').click();
						$("#oadh-rrm-btn-group-2").disabledButtonGroup();
						$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
					}

					if(!empty(json.smtRowId))
					{
						deleteSmtRow('oadhRrmSmtRows', json.smtRowId);
					}

					$('#oadh-rrm-modal-delete').modal('hide');

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rrm-btn-authorize').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			// if($('#oadh-rrm-journals-section').attr('data-target-id') == '')
			// {

			// }
			// else
			// {

			// }

			if($('#oadh-rrm-grid').is(":visible"))
			{
				if(!$('#oadh-rrm-grid').isRowSelected())
				{
					$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 20000);
					return;
				}

				rowData = $('#oadh-rrm-grid').getRowData($('#oadh-rrm-grid').jqGrid('getGridParam', 'selrow'));
				name = rowData.oadh_rrt_name;
				// number = rowData.oadh_rrt_number;
			}
			else
			{
				name = $('#oadh-rrm-name').val();
				// number = $('#oadh-rrm-number').attr('data-number');
			}

			$('#oadh-rrm-ma-authorize-message').html($('#oadh-rrm-ma-authorize-message').attr('data-default-label').replace(':name', name));
			// $('#oadh-rrm-ma-authorize-message').html($('#oadh-rrm-ma-authorize-message').attr('data-default-label').replace(':number', number));

			$('#oadh-rrm-ma').modal('show');
		});


		$('#oadh-rrm-ins-detail-btn-delete').click(function()
		{
			var id = $('#oadh-rrm-back-ins-detail-grid').getSelectedRowsIdCell('oadh_rrm_ins_detail_id');

			if(!$('#oadh-rrm-back-ins-detail-grid').isRowSelected())
			{
				$('#oadh-rrm-ins-detail-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
				return;
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id,
						// 'detail_id': $('#oadh-rrm-ins-detail-id').val()
					}
				),
				dataType : 'json',
				url:  $('#oadh-rrm-ins-detail-form').attr('action') + '/delete-ins-details',
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrm-ins-detail-btn-toolbar', false);
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrm-ins-detail-btn-refresh').click();
						$("#oadh-rrm-ins-detail-btn-group-2").disabledButtonGroup();
						$('#oadh-rrm-ins-detail-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rrm-rig-detail-btn-delete').click(function()
		{
			var id = $('#oadh-rrm-back-rig-detail-grid').getSelectedRowsIdCell('oadh_rrm_rig_detail_id');

			if(!$('#oadh-rrm-back-rig-detail-grid').isRowSelected())
			{
				$('#oadh-rrm-rig-detail-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
				return;
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'id': id,
						// 'detail_id': $('#oadh-rrm-rig-detail-id').val()
					}
				),
				dataType : 'json',
				url:  $('#oadh-rrm-rig-detail-form').attr('action') + '/delete-rig-details',
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrm-rig-detail-btn-toolbar', false);
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrm-rig-detail-btn-refresh').click();
						$("#oadh-rrm-rig-detail-btn-group-2").disabledButtonGroup();
						$('#oadh-rrm-rig-detail-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rrm-btn-save').click(function()
		{
			var url = $('#oadh-rrm-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
			{
				if(!$('#oadh-rrm-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-rrm-id').isEmpty())
				{
					url = url + '/create-master';
				}
				else
				{
					url = url + '/update-master';
					action = 'edit';
				}

				data = $('#oadh-rrm-form').formToObject('oadh-rrm-');
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrm-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
						{
							if(action == 'new')
							{
								$('#oadh-rrm-id').val(json.id);
								$('#oadh-rrm-ins-detail-recommendation-id').val(json.id);
								$('#oadh-rrm-rig-detail-recommendation-id').val(json.id);
								// $('#oadh-rrm-number').html($('#oadh-rrm-number').attr('data-default-label').replace(':number', json.number));
								// $('#oadh-rrm-number').html(json.number);
							}

							$('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);

							$('#oadh-rrm-form-new-title').addClass('hidden');

							$('#oadh-rrm-form-edit-title').removeClass('hidden');

							$('#oadh-rrm-form').jqMgVal('clearContextualClasses');

							$('#oadh-rrm-ins-detail-btn-toolbar').disabledButtonGroup();
							$('#oadh-rrm-rig-detail-btn-toolbar').disabledButtonGroup();

							$('#oadh-rrm-ins-detail-btn-group-1').enableButtonGroup();
							$('#oadh-rrm-ins-detail-btn-group-3').enableButtonGroup();
							$('#oadh-rrm-rig-detail-btn-group-1').enableButtonGroup();
							$('#oadh-rrm-rig-detail-btn-group-3').enableButtonGroup();

							$('#oadh-rrm-btn-new, #oadh-rrm-btn-upload, #oadh-rrm-btn-show-files, #oadh-rrm-btn-show-history').removeAttr('disabled');
							$('#oadh-rrm-ins-detail-form-fieldset').removeAttr('disabled');
							$('#oadh-rrm-rig-detail-form-fieldset').removeAttr('disabled');
						}
						else
						{
							// $('#oadh-rrm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-rrm-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
						{
							$('#oadh-rrm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-rrm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					if(action == 'new' && !empty(json.smtRow))
					{
						addSmtRow('oadhRrmSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					if(action == 'edit' && !empty(json.smtRow))
					{
						updateSmtRow('oadhRrmSmtRows', Object.keys(json.smtRow)[0], Object.values(json.smtRow)[0]);
					}

					$('#app-loader').addClass('hidden');

					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');

					$('#oadh-rrm-ins-detail-name').focus();
					// $('#oadh-rrm-rig-detail-name').focus();
				}
			});
		});

		$('#oadh-rrm-ins-detail-btn-save').click(function()
		{
			var url = $('#oadh-rrm-ins-detail-form').attr('action'), action = 'new';

			if(!$('#oadh-rrm-ins-detail-form').jqMgVal('isFormValid'))
			{
				return;
			}

			if($('#oadh-rrm-ins-detail-id').isEmpty())
			{
				url = url + '/create-ins-detail';
			}
			else
			{
				url = url + '/update-ins-detail';
				action = 'edit';
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify($('#oadh-rrm-ins-detail-form').formToObject('oadh-rrm-ins-detail-')),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrm-ins-detail-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrm-ins-detail-form').jqMgVal('clearForm');
						$('#oadh-rrm-ins-detail-btn-toolbar').disabledButtonGroup();
						$('#oadh-rrm-ins-detail-btn-group-1').enableButtonGroup();
						$('#oadh-rrm-ins-detail-btn-group-3').enableButtonGroup();

						$('#oadh-rrm-ins-detail-btn-refresh').click();
					}
					else if(json.info)
					{
							$('#oadh-rrm-ins-detail-form').showAlertAsFirstChild('alert-info', json.info);
					}

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
					$('#oadh-rrm-ins-detail-article-label').focus();
				}
			});
		});

		$('#oadh-rrm-rig-detail-btn-save').click(function()
		{
			var url = $('#oadh-rrm-rig-detail-form').attr('action'), action = 'new';

			if(!$('#oadh-rrm-rig-detail-form').jqMgVal('isFormValid'))
			{
				return;
			}

			if($('#oadh-rrm-rig-detail-id').isEmpty())
			{
				url = url + '/create-rig-detail';
			}
			else
			{
				url = url + '/update-rig-detail';
				action = 'edit';
			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify($('#oadh-rrm-rig-detail-form').formToObject('oadh-rrm-rig-detail-')),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrm-rig-detail-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrm-rig-detail-form').jqMgVal('clearForm');
						$('#oadh-rrm-rig-detail-btn-toolbar').disabledButtonGroup();
						$('#oadh-rrm-rig-detail-btn-group-1').enableButtonGroup();
						$('#oadh-rrm-rig-detail-btn-group-3').enableButtonGroup();

						$('#oadh-rrm-rig-detail-btn-refresh').click();
					}
					else if(json.info)
					{
							$('#oadh-rrm-rig-detail-form').showAlertAsFirstChild('alert-info', json.info);
					}

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
					$('#oadh-rrm-rig-detail-article-label').focus();
				}
			});
		});

		$('#oadh-rrm-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-rrm-journals-section').attr('data-target-id') == '#oadh-rrm-form-section')
			{
				$('#oadh-rrm-form-new-title').addClass('hidden');
				$('#oadh-rrm-form-edit-title').addClass('hidden');
				$('#oadh-rrm-btn-refresh').click();
				$('#oadh-rrm-ins-detail-recommendation-id').val(-1);
				$('#oadh-rrm-rig-detail-recommendation-id').val(-1);

				$('#oadh-rrm-back-ins-detail-grid').jqGrid('clearGridData');
				$('#oadh-rrm-back-rig-detail-grid').jqGrid('clearGridData');

				$('#oadh-rrm-form, #oadh-rrm-ins-detail-form, #oadh-rrm-rig-detail-form').jqMgVal('clearForm');
				$('#oadh-rrm-form-section').collapse('hide');
			}
			else
			{

			}

			$('#oadh-rrm-finished-goods-warehouse-ids').clearTags();

			$('#oadh-rrm-btn-group-1').enableButtonGroup();

			$('#oadh-rrm-btn-group-3').disabledButtonGroup();

			$('#oadh-rrm-journals-section').attr('data-target-id', '');

			oadhRrmDisabledInsDetailForm();
			oadhRrmDisabledRigDetailForm();
			oadhRrmEnableSelectedField();
		});

		$('#oadh-rrm-btn-clear-filter').click(function()
		{
			$('#oadh-rrm-filters-form').find('.tokenfield').find('.close').click();

			$('#oadh-rrm-filters-form').jqMgVal('clearForm');

			$('#oadh-rrm-btn-filter').click();
		});

		$('#oadh-rrm-btn-filter').click(function()
		{
			var filters = [];

			$(this).removeClass('btn-default').addClass('btn-warning');

			if(!$('#oadh-rrm-filters-form').jqMgVal('isFormValid'))
			{
				return;
			}

			$('#oadh-rrm-filters-form').jqMgVal('clearContextualClasses');

			if(!$('#oadh-rrm-system-filter').isEmpty())
			{
				filters.push({'field':'system', 'op':'cn', 'data': $('#oadh-rrm-system-filter').val()});
			}

			if(!$('#oadh-rrm-recommendation-filter').isEmpty())
			{
				filters.push({'field':'recommendation', 'op':'cn', 'data': $('#oadh-rrm-recommendation-filter').val()});
			}

			if(!$('#oadh-rrm-code-filter').isEmpty())
			{
				filters.push({'field':'code', 'op':'cn', 'data': $('#oadh-rrm-code-filter').val()});
			}

			if(!$('#oadh-rrm-mechanism-filter').isEmpty())
			{
				filters.push({'field':'mechanism', 'op':'cn', 'data': $('#oadh-rrm-mechanism-filter').val()});
			}

			if(!$('#oadh-rrm-source-filter').isEmpty())
			{
				filters.push({'field':'source', 'op':'cn', 'data': $('#oadh-rrm-source-filter').val()});
			}

			if(!$('#oadh-rrm-legal-base-filter').isEmpty())
			{
				filters.push({'field':'legal_base', 'op':'cn', 'data': $('#oadh-rrm-legal-base-filter').val()});
			}

			if(filters.length == 0)
			{
			  $('#oadh-rrm-btn-filter').removeClass('btn-warning').addClass('btn-default');
			}

			$('#oadh-rrm-grid').jqGrid('setGridParam', {'postData':{'datatype':'json', "filters":"{'groupOp':'AND','rules':" + JSON.stringify(filters) + "}"}}).trigger('reloadGrid');
		});

		// $('#oadh-rrm-smt-btn-select').click(function()
		// {
		// 	var rowData = $('#oadh-rrm-smt').getSelectedSmtRow();

		// 	if(empty(rowData))
		// 	{
		// 		return;
		// 	}

		// 	$('#oadh-rrm-btn-new').click();

		// 	populateFormFields(rowData);

		// 	$('#oadh-rrm-smt').modal('hide');

		// 	$('#oadh-rrm-ins-detail-form-fieldset').removeAttr('disabled');

		// 	$('#oadh-rrm-id').val(rowData.oadh_rrt_id);
		// 	$('#oadh-rrm-ins-detail-recommendation-id').val(rowData.oadh_rrt_id);

		// 	// $('#oadh-rrm-number').html($('#oadh-rrm-number').attr('data-default-label').replace(':number', rowData.oadh_rrt_number));
		// 	// $('#oadh-rrm-number').html(rowData.oadh_rrt_number);

		// 	$('#oadh-rrm-ins-detail-btn-refresh').click();

		// 	$('#oadh-rrm-ins-detail-btn-toolbar').disabledButtonGroup();

		// 	$('#oadh-rrm-ins-detail-btn-group-1').enableButtonGroup();
		// 	$('#oadh-rrm-ins-detail-btn-group-3').enableButtonGroup();
		// });

		$('#oadh-rrm-smt-btn-refresh').click(function()
		{
			// loadSmtRows('oadhRrmSmtRows', $('#oadh-rrm-form').attr('action') + '/smt-rows', '', true, true);
		});

		if(!$('#oadh-rrm-new-action').isEmpty())
		{
			$('#oadh-rrm-btn-new').click();
		}

		$('#oadh-rrm-btn-edit-helper').click(function()
		{
			showButtonHelper('oadh-rrm-btn-close', 'oadh-rrm-btn-group-2', $('#oadh-rrm-edit-action').attr('data-content'));
		});

		if(!$('#oadh-rrm-edit-action').isEmpty())
		{
			showButtonHelper('oadh-rrm-btn-close', 'oadh-rrm-btn-group-2', $('#oadh-rrm-edit-action').attr('data-content'));
		}

		$('#oadh-rrm-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-rrm-btn-close', 'oadh-rrm-btn-group-2', $('#oadh-rrm-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-rrm-delete-action').isEmpty())
		{
			showButtonHelper('oadh-rrm-btn-close', 'oadh-rrm-btn-group-2', $('#oadh-rrm-delete-action').attr('data-content'));
		}
	});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		{!! Form::open(array('id' => 'oadh-rrm-filters-form', 'url' => URL::to('/'), 'role' => 'form', 'onsubmit' => 'return false;', 'class' => 'form-horizontal')) !!}
			<div id="oadh-rrm-filters" class="panel panel-default">
				<div class="panel-heading custom-panel-heading clearfix">
					<h3 class="panel-title custom-panel-title pull-left">
						{{ Lang::get('form.filtersTitle') }}
					</h3>
					{!! Form::button('<i class="fa fa-filter"></i> ' . Lang::get('form.filterButton'), array('id' => 'oadh-rrm-btn-filter', 'class' => 'btn btn-default btn-sm pull-right btn-filter-left-margin')) !!}
					{!! Form::button('<i class="fa fa-eraser"></i> ' . Lang::get('form.clearFilterButton'), array('id' => 'oadh-rrm-btn-clear-filter', 'class' => 'btn btn-default btn-sm pull-right')) !!}
				</div>
				<div id="oadh-rrm-filters-body" class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								{!! Form::label('oadh-rrm-system-filter', Lang::get('decima-oadh::back-end-column.system'), array('class' => 'col-sm-2 control-label')) !!}
								<div class="col-sm-10 mg-hm">
									{!! Form::text('oadh-rrm-system-filter', null , array('id' => 'oadh-rrm-system-filter', 'class' => 'form-control')) !!}
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{!! Form::label('oadh-rrm-recommendation-filter', 'Recome.', array('class' => 'col-sm-2 control-label')) !!}
								<div class="col-sm-10 mg-hm">
									{!! Form::text('oadh-rrm-recommendation-filter', null , array('id' => 'oadh-rrm-recommendation-filter', 'class' => 'form-control')) !!}
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{!! Form::label('oadh-rrm-code-filter', Lang::get('decima-oadh::back-end-column.code'), array('class' => 'col-sm-2 control-label')) !!}
								<div class="col-sm-10 mg-hm">
									{!! Form::text('oadh-rrm-code-filter', null , array('id' => 'oadh-rrm-code-filter', 'class' => 'form-control')) !!}
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{!! Form::label('oadh-rrm-mechanism-filter', Lang::get('decima-oadh::back-end-column.mechanism'), array('class' => 'col-sm-2 control-label')) !!}
								<div class="col-sm-10 mg-hm">
									{!! Form::text('oadh-rrm-mechanism-filter', null , array('id' => 'oadh-rrm-mechanism-filter', 'class' => 'form-control')) !!}
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{!! Form::label('oadh-rrm-source-filter', Lang::get('decima-oadh::back-end-column.source'), array('class' => 'col-sm-2 control-label')) !!}
								<div class="col-sm-10 mg-hm">
									{!! Form::text('oadh-rrm-source-filter', null , array('id' => 'oadh-rrm-source-filter', 'class' => 'form-control')) !!}
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{!! Form::label('oadh-rrm-legal-base-filter', Lang::get('decima-oadh::back-end-column.legalBase'), array('class' => 'col-sm-2 control-label')) !!}
								<div class="col-sm-10 mg-hm">
									{!! Form::text('oadh-rrm-legal-base-filter', null , array('id' => 'oadh-rrm-legal-base-filter', 'class' => 'form-control')) !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
		<div id="oadh-rrm-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-rrm-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new'), array('id' => 'oadh-rrm-btn-new', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.newMaster'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-rrm-btn-refresh', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
     				<li><a id='oadh-rrm-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> {{ Lang::get('decima-accounting::journal-management.standardSpreadsheet') . ' (' . Lang::get('form.spreadsheet') . ')' }}</a></li>
       		</ul>
				</div>
			</div>
			<div id="oadh-rrm-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-rrm-btn-edit', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.editMaster'))) !!}
				<!-- {!! Form::button('<i class="fa fa-check"></i> ' . Lang::get('toolbar.authorize'), array('id' => 'oadh-rrm-btn-authorize', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.authorizeMaster'))) !!} -->
				<!-- {!! Form::button('<i class="fa fa-ban"></i> ' . Lang::get('toolbar.nulify'), array('id' => 'oadh-rrm-btn-void', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.voidMaster'))) !!} -->
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-rrm-btn-delete', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.deleteMaster'))) !!}
			</div>
			<div id="oadh-rrm-btn-group-4" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-eye"></i> ', array('id' => 'oadh-rrm-btn-view', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.viewLongText'))) !!}
				<!-- {!! Form::button('<i class="fa fa-upload"></i> ', array('id' => 'oadh-rrm-btn-upload', 'class' => 'btn btn-default oadh-rrm-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-folder' => Lang::get('decima-oadh::recommendation-management.folder'), 'data-original-title' => Lang::get('toolbar.uploadLongText'))) !!} -->
				<!-- {!! Form::button('<i class="fa fa-files-o"></i> ', array('id' => 'oadh-rrm-btn-show-files', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showFilesLongText'))) !!} -->
				{!! Form::button('<i class="fa fa-history"></i> ', array('id' => 'oadh-rrm-btn-show-history', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showHistoryLongText'))) !!}
			</div>
			<div id="oadh-rrm-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-rrm-btn-save', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.saveMaster'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-rrm-btn-close', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-rrm-grid-section' class='collapse in' data-id=''>
			<div class='app-grid' data-app-grid-id='oadh-rrm-grid'>
				{!!
				GridRender::setGridId('oadh-rrm-grid')
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('height', 'auto')
					->setGridOption('multiselect', false)
					->setGridOption('rowNum', 5)
					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
		    	->setGridOption('url', URL::to('ucaoadh/recommendations/recommendations-management/grid-data-master'))
		    	->setGridOption('caption', Lang::get('decima-oadh::recommendation-management.gridMasterTitle'))
					->setGridOption('filename', Lang::get('decima-oadh::recommendation-management.gridMasterTitle'))
		    	->setGridOption('postData', array('_token' => Session::token()))
					// ->setGridOption('grouping', true)
					// ->setGridOption('groupingView', array('groupField' => array('oadh_rrt_header'), 'groupColumnShow' => array(false), 'groupSummary' => array(true), 'showSummaryOnHide' => true, 'groupOrder' => array('asc')))
					->setGridEvent('loadComplete', 'oadhRrmOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhRrmOnSelectRowEvent')
					//->addGroupHeader(array('startColumnName' => 'oadh_rrt_name', 'numberOfColumns' => 1, 'titleText' => Lang::get('decima-oadh::recommendation-management.gridHeader')))
					// ->addColumn(array('index' => 'oadh_rrt_header'))
					->setGridOption('sortname', 'oadh_rrt_date')
					->setGridOption('sortorder', 'desc')
					->addColumn(array('index' => 'id', 'name' => 'oadh_rrt_id', 'hidden' => true))
					->addColumn(array('index' => 'file_id', 'name' => 'oadh_rrt_file_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.relatedRights'), 'index' => 'related_rights', 'name' => 'oadh_rrt_related_rights', 'align' => 'left', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.institutions'), 'index' => 'responsible_institutions', 'name' => 'oadh_rrt_responsible_institutions', 'align' => 'left', 'hidden' => true))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'date', 'name' => 'oadh_rrt_date', 'align' => 'center', 'formatter' => 'date', 'width' => 115))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.system'), 'index' => 'system', 'name' => 'oadh_rrt_system', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.recommendation'), 'index' => 'recommendation', 'name' => 'oadh_rrt_recommendation', 'align' => 'left', 'width' => 300))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.code'), 'index' => 'code', 'name' => 'oadh_rrt_code', 'align' => 'center'))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.mechanism'), 'index' => 'mechanism', 'name' => 'oadh_rrt_mechanism', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.source'), 'index' => 'source', 'name' => 'oadh_rrt_source', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.legalBase'), 'index' => 'legal_base', 'name' => 'oadh_rrt_legal_base', 'align' => 'left'))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.lastModifiedDate'), 'index' => 'last_modified_date', 'name' => 'oadh_rrt_last_modified_date', 'align' => 'center', 'formatter' => 'date', 'width' => 115))
					->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.observations'), 'index' => 'observations', 'name' => 'oadh_rrt_observations', 'align' => 'left'))
		    	->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-rrm-front-ins-detail-grid'>
				{!!
				GridRender::setGridId('oadh-rrm-front-ins-detail-grid')
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('caption', Lang::get('decima-oadh::recommendation-management.gridInsDetailTitle'))
					->setGridOption('url',URL::to('ucaoadh/recommendations/recommendations-management/grid-data-ins-detail'))
					->setGridOption('datatype', 'local')
					->setGridOption('filename', Lang::get('decima-oadh::recommendation-management.gridInsDetailTitle'))
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('footerrow', true)
					->setGridOption('multiselect', false)
					//->setGridOption('postData',array('_token' => Session::token()))
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'-1'}]}"))
					//->setGridEvent('loadComplete', 'oadhRrmInsDetailOnLoadCompleteEvent')
					->addColumn(array('index' => 'id', 'name' => 'oadh_rrm_ins_detail_id', 'hidden' => true))
					->addColumn(array('label' => Lang::get('form.abbreviation'), 'index' => 'abbreviation', 'name' => 'oadh_rrm_ins_abbreviation'))
					->addColumn(array('label' => Lang::get('form.name'), 'index' => 'name', 'name' => 'oadh_rrm_ins_name'))
					->renderGrid();
				!!}
			</div>
			<div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-rrm-front-rig-detail-grid'>
				{!!
				GridRender::setGridId('oadh-rrm-front-rig-detail-grid')
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('caption', Lang::get('decima-oadh::recommendation-management.gridRigDetailTitle'))
					->setGridOption('url',URL::to('ucaoadh/recommendations/recommendations-management/grid-data-rig-detail'))
					->setGridOption('datatype', 'local')
					->setGridOption('filename', Lang::get('decima-oadh::recommendation-management.gridRigDetailTitle'))
					->setGridOption('rowList', array())
					->setGridOption('rowNum', 100000)
					->setGridOption('footerrow', true)
					->setGridOption('multiselect', false)
					//->setGridOption('postData',array('_token' => Session::token()))
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'-1'}]}"))
					//->setGridEvent('loadComplete', 'oadhRrmRigDetailOnLoadCompleteEvent')
					->addColumn(array('index' => 'id', 'name' => 'oadh_rrm_rig_detail_id', 'hidden' => true))
						->addColumn(array('label' => Lang::get('form.name'), 'index' => 'name', 'name' => 'oadh_rrm_rig_detail_name'))
					->renderGrid();
				!!}
			</div>
		</div>
	</div>
</div>
<div id='oadh-rrm-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		{{-- <div class="form-container form-container-followed-by-grid-section"> --}}
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-rrm-form', 'url' => URL::to('ucaoadh/recommendations/recommendations-management'), 'role' => 'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-rrm-form-new-title" class="hidden">{{ Lang::get('decima-oadh::recommendation-management.formNewTitle') }}</legend>
				<legend id="oadh-rrm-form-edit-title" class="hidden">{{ Lang::get('decima-oadh::recommendation-management.formEditTitle') }}<label id="oadh-rrm-number" class="pull-right" data-default-label="{{ Lang::get('decima-oadh::recommendation-management.numberLabel') }}"></label></legend>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-system', Lang::get('decima-oadh::back-end-column.system'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rrm-system', null , array('id' => 'oadh-rrm-system', 'class' => 'form-control', 'data-mg-required' => '')) !!}
					    {!! Form::hidden('oadh-rrm-id', null, array('id' => 'oadh-rrm-id')) !!}
			  		</div>
					</div>
					<div class="col-md-9">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-recommendation', Lang::get('decima-oadh::back-end-column.recommendation'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rrm-recommendation', null , array('id' => 'oadh-rrm-recommendation', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-code', Lang::get('decima-oadh::back-end-column.code'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rrm-code', null , array('id' => 'oadh-rrm-code', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
					
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-date', Lang::get('decima-oadh::back-end-column.date'), array('class' => 'control-label')) !!}
							{!! Form::date('oadh-rrm-date', array('class' => 'form-control', 'data-mg-required' => ''), '', '', '', '', 'fa-calendar-check-o', false) !!}
			  		</div>
					</div>
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-mechanism', Lang::get('decima-oadh::back-end-column.mechanism'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rrm-mechanism', null , array('id' => 'oadh-rrm-mechanism', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-source', Lang::get('decima-oadh::back-end-column.source'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rrm-source', null , array('id' => 'oadh-rrm-source', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>

					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-legal-base', Lang::get('decima-oadh::back-end-column.legalBase'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rrm-legal-base', null , array('id' => 'oadh-rrm-legal-base', 'class' => 'form-control', 'data-mg-required' => '')) !!}
			  		</div>
					</div>
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-last-modified-date', Lang::get('decima-oadh::back-end-column.lastModifiedDate'), array('class' => 'control-label')) !!}
							{!! Form::date('oadh-rrm-last-modified-date', array('class' => 'form-control'), '', '', '', '', 'fa-calendar-check-o', false) !!}
			  		</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-rrm-observations', Lang::get('decima-oadh::back-end-column.observations'), array('class' => 'control-label')) !!}
					    {!! Form::text('oadh-rrm-observations', null , array('id' => 'oadh-rrm-observations', 'class' => 'form-control')) !!}
			  		</div>
					</div>
					
				</div>
			{!! Form::close() !!}
		</div>

		<ul class="nav nav-tabs" role="tablist" style="margin-top:10px;">
			<li role="presentation" class="active"><a href="#oadh-rrm-details" aria-controls="oadh-rrm-details" role="tab" data-toggle="tab">{{ Lang::get('decima-sale::sale-order-management.formOrderDetail') }}</a></li>
			<li role="presentation"><a href="#oadh-rrm-rights" aria-controls="oadh-rrm-rights" role="tab" data-toggle="tab">{{ Lang::get('decima-sale::sale-order-management.formOrderTax') }}</a></li>
		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="oadh-rrm-details">
				
				
				<div class="form-container form-container-followed-by-grid-section">
					{!! Form::open(array('id' => 'oadh-rrm-ins-detail-form', 'url' => URL::to('ucaoadh/recommendations/recommendations-management'), 'role' => 'form', 'onsubmit' => 'return false;')) !!}
						<fieldset id="oadh-rrm-ins-detail-form-fieldset" disabled="disabled">
							<legend id="oadh-rrm-ins-detail-model-form-new-title" class="">{{ Lang::get('decima-oadh::recommendation-management.formInsDetailTitle') }}</legend>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-rrm-ins-detail-institution-label', 'Institución', array('class' => 'control-label')) !!}
										{!! Form::autocomplete('oadh-rrm-ins-detail-institution-label', $institutions, array('class' => 'form-control', 'data-mg-required' => ''), 'oadh-rrm-ins-detail-institution-label', 'oadh-rrm-ins-detail-institution-id', null, 'fa-cube', '', 10) !!}
										{!! Form::hidden('oadh-rrm-ins-detail-institution-id', null, array('id'  =>  'oadh-rrm-ins-detail-institution-id')) !!}

										{!! Form::hidden('oadh-rrm-ins-detail-recommendation-id', null, array('id' => 'oadh-rrm-ins-detail-recommendation-id', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-rrm-ins-detail-id', null, array('id' => 'oadh-rrm-ins-detail-id')) !!}
									</div>
								</div>
								<div class="col-md-3">

								</div>
							</div>
						</fieldset>
					{!! Form::close() !!}
				</div>
				<div id="oadh-rrm-ins-detail-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-rrm-ins-detail-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-rrm-ins-detail-btn-save', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => 'false', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.saveInsDetail'))) !!}
					</div>
					<div id="oadh-rrm-ins-detail-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-rrm-ins-detail-btn-refresh', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						{{-- <div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-rrm-ins-detail-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-rrm-ins-detail-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div> --}}
					</div>
					<div id="oadh-rrm-ins-detail-btn-group-2" class="btn-group btn-group-app-toolbar">
						{{-- {!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-rrm-ins-detail-btn-edit', 'class' => 'btn btn-default oadh-rrm-ins-detail-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.editInsDetail'))) !!} --}}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-rrm-ins-detail-btn-delete', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.deleteInsDetail'))) !!}
					</div>
				</div>
				<div id='oadh-rrm-back-ins-detail-grid-section' class='app-grid collapse in' data-app-grid-id='oadh-rrm-back-ins-detail-grid'>
					{!!
					GridRender::setGridId('oadh-rrm-back-ins-detail-grid')
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url',URL::to('ucaoadh/recommendations/recommendations-management/grid-data-ins-detail'))
						->setGridOption('datatype', 'local')
						->setGridOption('filename', Lang::get('decima-oadh::recommendation-management.gridInsDetailTitle'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('footerrow',true)
						->setGridOption('multiselect', true)
						//->setGridOption('postData',array('_token' => Session::token()))
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhRrmInsDetailOnSelectRowEvent')
						//->setGridEvent('loadComplete', 'oadhRrmInsDetailOnLoadCompleteEvent')
						->addColumn(array('index' => 'id', 'name' => 'oadh_rrm_ins_detail_id', 'hidden' => true))
						->addColumn(array('label' => Lang::get('form.abbreviation'), 'index' => 'abbreviation', 'name' => 'oadh_rrm_ins_abbreviation'))
						->addColumn(array('label' => Lang::get('form.name'), 'index' => 'name', 'name' => 'oadh_rrm_ins_name'))
						->renderGrid();
					!!}
				</div>

			</div>
			<div role="tabpanel" class="tab-pane" id="oadh-rrm-rights">
				
				<div class="form-container form-container-followed-by-grid-section">
					{!! Form::open(array('id' => 'oadh-rrm-rig-detail-form', 'url' => URL::to('ucaoadh/recommendations/recommendations-management'), 'role' => 'form', 'onsubmit' => 'return false;')) !!}
						<fieldset id="oadh-rrm-rig-detail-form-fieldset" disabled="disabled">
							<legend id="oadh-rrm-rig-detail-model-form-new-title" class="">{{ Lang::get('decima-oadh::recommendation-management.formRigDetailTitle') }}</legend>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group mg-hm">
										{!! Form::label('oadh-rrm-rig-detail-right-label', 'Derecho', array('class' => 'control-label')) !!}
										{!! Form::autocomplete('oadh-rrm-rig-detail-right-label', $rights, array('class' => 'form-control', 'data-mg-required' => ''), 'oadh-rrm-rig-detail-right-label', 'oadh-rrm-rig-detail-right-id', null, 'fa-cube', '', 10) !!}
										{!! Form::hidden('oadh-rrm-rig-detail-right-id', null, array('id'  =>  'oadh-rrm-rig-detail-right-id')) !!}
										{!! Form::hidden('oadh-rrm-rig-detail-recommendation-id', null, array('id' => 'oadh-rrm-rig-detail-recommendation-id', 'data-mg-clear-ignored' => '')) !!}
										{!! Form::hidden('oadh-rrm-rig-detail-id', null, array('id' => 'oadh-rrm-rig-detail-id')) !!}
									</div>
								</div>
								<div class="col-md-3">

								</div>
							</div>
						</fieldset>
					{!! Form::close() !!}
				</div>
				<div id="oadh-rrm-rig-detail-btn-toolbar" class="section-header btn-toolbar toolbar-preceded-by-form-section" role="toolbar" disabled="disabled">
					<div id="oadh-rrm-rig-detail-btn-group-3" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-rrm-rig-detail-btn-save', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => 'false', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.saveRigDetail'))) !!}
					</div>
					<div id="oadh-rrm-rig-detail-btn-group-1" class="btn-group btn-group-app-toolbar">
						{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-rrm-rig-detail-btn-refresh', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
						{{-- <div class="btn-group">
							{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
							<ul class="dropdown-menu">
								<li><a id='oadh-rrm-rig-detail-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xls</a></li>
								<li><a id='oadh-rrm-rig-detail-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
							</ul>
						</div> --}}
					</div>
					<div id="oadh-rrm-rig-detail-btn-group-2" class="btn-group btn-group-app-toolbar">
						{{-- {!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-rrm-rig-detail-btn-edit', 'class' => 'btn btn-default oadh-rrm-rig-detail-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.editRigDetail'))) !!} --}}
						{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-rrm-rig-detail-btn-delete', 'class' => 'btn btn-default oadh-rrm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::recommendation-management.deleteRigDetail'))) !!}
					</div>
				</div>
				<div id='oadh-rrm-back-rig-detail-grid-section' class='app-grid collapse in' data-app-grid-id='oadh-rrm-back-rig-detail-grid'>
					{!!
					GridRender::setGridId('oadh-rrm-back-rig-detail-grid')
						->hideXlsExporter()
						->hideCsvExporter()
						->setGridOption('url',URL::to('ucaoadh/recommendations/recommendations-management/grid-data-rig-detail'))
						->setGridOption('datatype', 'local')
						->setGridOption('filename', Lang::get('decima-oadh::recommendation-management.gridRigDetailTitle'))
						->setGridOption('rowList', array())
						->setGridOption('rowNum', 100000)
						->setGridOption('footerrow',true)
						->setGridOption('multiselect', true)
						//->setGridOption('postData',array('_token' => Session::token()))
						->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'recommendation_id','op':'eq','data':'-1'}]}"))
						->setGridEvent('onSelectRow', 'oadhRrmRigDetailOnSelectRowEvent')
						//->setGridEvent('loadComplete', 'oadhRrmRigDetailOnLoadCompleteEvent')
						->addColumn(array('index' => 'id', 'name' => 'oadh_rrm_rig_detail_id', 'hidden' => true))
						->addColumn(array('label' => Lang::get('form.name'), 'index' => 'name', 'name' => 'oadh_rrm_rig_detail_name'))
						->renderGrid();
					!!}
				</div>


			</div>
		</div>
	</div>
</div>
@include('decima-file::file-viewer')
<div id='oadh-rrm-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals($prefix, $appInfo['id']) !!}
</div>
{{-- @include('decima-file::file-uploader') --}}
@include('layouts.search-modal-table')
<div id='oadh-rrm-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-rrm-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-rrm-delete-message">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
				 <!-- <p id="oadh-rrm-delete-message" data-default-label="{{ Lang::get('decima-oadh::recommendation-management.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-rrm-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
<div id='oadh-rrm-ma' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-rrm-btn-authorize">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p id="oadh-rrm-ma-authorize-message" data-default-label="{{ Lang::get('form.voidMessageConfirmation') }}"></p> -->
				 <p id="oadh-rrm-ma-authorize-message" data-default-label="{{ Lang::get('decima-oadh::recommendation-management.authorizedMessageConfirmation') }}"></p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-rrm-ma-btn-authorize" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
<div id='oadh-rrm-mv' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-rrm-btn-void">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p id="oadh-rrm-mv-void-message" data-default-label="{{ Lang::get('form.voidMessageConfirmation') }}"></p> -->
				 <p id="oadh-rrm-mv-void-message" data-default-label="{{ Lang::get('decima-oadh::recommendation-management.voidMessageConfirmation') }}"></p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-rrm-mv-btn-void" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
