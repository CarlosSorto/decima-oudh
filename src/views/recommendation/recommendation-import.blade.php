@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-rrt-new-action', null, array('id' => 'oadh-rrt-new-action')) !!}
{!! Form::hidden('oadh-rrt-edit-action', null, array('id' => 'oadh-rrt-edit-action', 'data-content' => Lang::get('module::app.editHelpText'))) !!}
{!! Form::hidden('oadh-rrt-remove-action', null, array('id' => 'oadh-rrt-remove-action', 'data-content' => Lang::get('module::app.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-rrt-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-rrt-btn-delete-helper', 'class' => 'hidden')) !!}
<style></style>

<script type='text/javascript'>


	function oadhRrtOnLoadCompleteEvent()
	{
		// $('#oadh-rrt-front-detail-grid').jqGrid('clearGridData');
		$('#oadh-rrt-temp-data').jqGrid('clearGridData');
		$('#oadh-rrt-prod-data').jqGrid('clearGridData');
	}

	//For grids with multiselect disabled
	function oadhRrtOnSelectRowEvent()
	{
		var id = $('#oadh-rrt-grid').getSelectedRowId('file_id');

		// $('#oadh-rrt-front-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + id + "'}]}"}}).trigger('reloadGrid');

		$('#oadh-rrt-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
		$('#oadh-rrt-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');

		$('#oadh-rrt-btn-group-2').enableButtonGroup();
	}

	$(document).ready(function()
	{
		// loadSmtRows('oadhRrtSmtRows', $('#oadh-rrt-form').attr('action') + '/smt-rows');

		$('.oadh-rrt-btn-tooltip').tooltip();

		$('#oadh-rrt-import-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-rrt-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-rrt-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-rrt-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-rrt-').focus();
		});

		// $('#oadh-rrt-').focusout(function()
		// {
		// 	$('#oadh-rrt-btn-save').focus();
		// });

		$('#oadh-rrt-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-rrt-grid-section').collapse('show');
		});

		$('#oadh-rrt-btn-upload').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('.oadh-rrt-btn-tooltip').tooltip('hide');

			// openUploader('oadh-rrt-', '', '', ['spreadsheet'], '', false, [], 0, false, 1);
			openUploader('oadh-rrt-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
		});

		$('#oadh-rrt-file-uploader-modal').on('hidden.bs.modal', function()
		{
			$('#oadh-rrt-btn-refresh').click();
		});

		$('#oadh-rrt-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-rrt-btn-toolbar').disabledButtonGroup();
			$('#oadh-rrt-btn-group-1').enableButtonGroup();
			$('#oadh-rrt-grid').trigger('reloadGrid');
			cleanJournals('oadh-rrt-');
		});

		$('#oadh-rrt-btn-delete-file').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-rrt-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rrt-grid').isRowSelected())
				{
					$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-rrt-modal-delete-file').modal('show');
		});

		$('#oadh-rrt-btn-modal-delete-file').click(function()
		{
			var id, url;

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'file_id': $('#oadh-rrt-grid').getSelectedRowId('file_id')
					}
				),
				dataType: 'json',
				url: $('#app-url').val() + '/ucaoadh/recommendations/recommendations-import/delete-file',
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrt-btn-toolbar', false);
					$('#oadh-rrt-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrt-modal-delete-prod').click();
						$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					if(json.info)
					{
						$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
					}

					$('#oadh-rrt-modal-delete-file').modal('hide');

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
					$('#oadh-rrt-grid').trigger('reloadGrid');
					$('#oadh-rrt-prod-data').trigger('reloadGrid');
				}
			});
		});

		$('#oadh-rrt-btn-delete-prod').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-rrt-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rrt-grid').isRowSelected())
				{
					$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-rrt-modal-delete-prod').modal('show');
		});

		$('#oadh-rrt-btn-modal-delete-prod').click(function()
		{
			var id, url;

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'file_id': $('#oadh-rrt-grid').getSelectedRowId('file_id')
					}
				),
				dataType: 'json',
				url: $('#app-url').val() + '/ucaoadh/recommendations/recommendations-import/delete-from-production',
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrt-btn-toolbar', false);
					$('#oadh-rrt-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrt-modal-delete-prod').click();
						$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

						$('#oadh-rrt-prod-data').trigger('reloadGrid');
						$('#oadh-rrt-grid').trigger('reloadGrid');
					}

					if(json.info)
					{
						$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
					}

					$('#oadh-rrt-modal-delete-prod').modal('hide');

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rrt-btn-generate').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-rrt-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-rrt-grid').isRowSelected())
				{
					$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

			}
			else
			{

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-rrt-modal-generate').modal('show');
		});

		$('#oadh-rrt-btn-modal-generate').click(function()
		{
			var rowData = $('#oadh-rrt-grid').getRowData($('#oadh-rrt-grid').jqGrid('getGridParam', 'selrow'));

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(
					{
						'_token':$('#app-token').val(),
						'file_id': rowData['file_id']
					}
				),
				dataType : 'json',
				url:  $('#app-url').val() + '/ucaoadh/recommendations/recommendations-import/copy-to-production',
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrt-btn-toolbar', false);
					$('#oadh-rrt-modal-generate').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-rrt-btn-refresh').click();
						$("#oadh-rrt-btn-group-2").disabledButtonGroup();
						$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}

					if(json.info)
					{
						$('#oadh-rrt-btn-refresh').click();
						$("#oadh-rrt-btn-group-2").disabledButtonGroup();
						$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 15000);
					}

					$('#oadh-rrt-modal-generate').modal('hide');

					$('#app-loader').addClass('hidden');
					enableAll();

					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-rrt-btn-process').click(function()
		{

			if(!$('#oadh-rrt-grid').isRowSelected())
			{
				$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
				return;
			}

			rowData = $('#oadh-rrt-grid').getRowData($('#oadh-rrt-grid').jqGrid('getGridParam', 'selrow'));

			$('#oadh-rrt-import-form').jqMgVal('clearContextualClasses');
			$('#oadh-rrt-import-id').val(rowData.file_id);
			$('#oadh-rrt-import-name').val(rowData.file_name);
			$('#oadh-rrt-import-system-route').val(rowData.file_system_route);

			$('#oadh-rrt-import-modal').modal('show');
		});

		$('#oadh-rrt-import-btn-process').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			if(!$('#oadh-rrt-import-form').jqMgVal('isFormValid'))
			{
				return;
			}

			//Cambio parametrizacion
			//Agrego un campo de columns para hacerlo mas dinamico
			let data = $('#oadh-rrt-import-form').formToObject('oadh-rrt-import-');
			// data['columns'] = ['year', 'court', 'sex'];
			// data['view'] = 'HabeasCorpusRequest';

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url:  $('#app-url').val() + '/ucaoadh/recommendations/recommendations-import/process',
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-rrt-btn-toolbar', false);
					$('#oadh-rrt-import-modal').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						// $('#oadh-rrt-btn-refresh').click();
						$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
						$('#oadh-rrt-temp-data, #oadh-rrt-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-rrt-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
					}

					if(json.info)
					{
						$('#oadh-rrt-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
					}

					$('#app-loader').addClass('hidden');
					enableAll();

					$('#oadh-rrt-import-modal').modal('hide');
				}
			});
		});		
	});
</script>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-rrt-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-rrt-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-rrt-btn-upload', 'class' => 'btn btn-default oadh-rrt-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-rrt-btn-refresh', 'class' => 'btn btn-default oadh-rrt-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-rrt-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-rrt-btn-process', 'class' => 'btn btn-default oadh-rrt-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-rrt-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-rrt-btn-delete-prod', 'class' => 'btn btn-default oadh-rrt-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-rrt-btn-delete-file', 'class' => 'btn btn-default oadh-rrt-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-rrt-grid-section' class='collapse in'>
			<div class="app-grid" data-app-grid-id='oadh-rrt-grid'>
				{!!
					GridRender::setGridId('oadh-rrt-grid')
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
					->hideCsvExporter()
					->setGridOption('url',URL::to('/ucaoadh/file'))
					->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					// ->setGridOption('filename', Lang::get('module::app.gridTitle'))
					->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':' " . Config::get('folders.' . $appInfo['id']) . "'}]}"))
					->setGridEvent('loadComplete', 'oadhRrtOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhRrtOnSelectRowEvent')
					->setGridOption('multiselect', false)
					->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
					->renderGrid();
					!!}
			</div>

			<div id="oadh-rrt-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-rrt-temp-data-tab" aria-controls="oadh-rrt-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-rrt-processed-data-tab" aria-controls="oadh-rrt-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-rrt-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-rrt-front-temp-data'>
      				{!!
								GridRender::setGridId('oadh-rrt-temp-data')
								->hideXlsExporter()
								->hideCsvExporter()
								->setGridOption('multiselect', false)
								->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250))
								->setGridOption('url',URL::to('ucaoadh/recommendations/recommendations-import/temp-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
								->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
								->setGridOption('shrinkToFit', false)
								->setGridOption('forceFit', true)
								->addColumn(array('index' => 'id', 'name' => 'oadh_rrt_id', 'hidden' => true))
								->addColumn(array('index' => 'file_id', 'name' => 'oadh_rrt_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.system'), 'index' => 'system', 'name' => 'oadh_rrt_system', 'align' => 'center'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.recommendation'), 'index' => 'recommendation', 'name' => 'oadh_rrt_recommendation', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.code'), 'index' => 'code', 'name' => 'oadh_rrt_code', 'align' => 'center'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'date', 'name' => 'oadh_rrt_date', 'align' => 'center', 'formatter' => 'date'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.mechanism'), 'index' => 'mechanism', 'name' => 'oadh_rrt_mechanism', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.relatedRights'), 'index' => 'related_rights', 'name' => 'oadh_rrt_related_rights', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.institutions'), 'index' => 'responsible_institutions', 'name' => 'oadh_rrt_responsible_institutions', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.source'), 'index' => 'source', 'name' => 'oadh_rrt_source', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.legalBase'), 'index' => 'legal_base', 'name' => 'oadh_rrt_legal_base', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.observations'), 'index' => 'observations', 'name' => 'oadh_rrt_observations', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.lastModifiedDate'), 'index' => 'last_modified_date', 'name' => 'oadh_rrt_last_modified_date', 'align' => 'center', 'formatter' => 'date'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_rrt_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
								// ->addColumn(array('label' => Lang::get('form.syncFem'), 'index' => 'syncronized' ,'name' => 'oadh_rrt_syncronized', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.booleanText')), 'align' => 'center' , 'stype' => 'select', 'width' => 85))
								->renderGrid();
								!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-rrt-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-rrt-front-processed-grid'>
              {!!
								GridRender::setGridId('oadh-rrt-prod-data')
								->hideXlsExporter()
								->hideCsvExporter()
								->setGridOption('multiselect', false)
								->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250))
								->setGridOption('url',URL::to('ucaoadh/recommendations/recommendations-import/prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
								->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
								->setGridOption('shrinkToFit', false)
								->setGridOption('forceFit', true)
								->addColumn(array('index' => 'id', 'name' => 'oadh_rrt_id', 'hidden' => true))
								->addColumn(array('index' => 'file_id', 'name' => 'oadh_rrt_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.system'), 'index' => 'system', 'name' => 'oadh_rrt_system', 'align' => 'center'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.recommendation'), 'index' => 'recommendation', 'name' => 'oadh_rrt_recommendation', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.code'), 'index' => 'code', 'name' => 'oadh_rrt_code', 'align' => 'center'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'date', 'name' => 'oadh_rrt_date', 'align' => 'center', 'formatter' => 'date'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.mechanism'), 'index' => 'mechanism', 'name' => 'oadh_rrt_mechanism', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.relatedRights'), 'index' => 'related_rights', 'name' => 'oadh_rrt_related_rights', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.institutions'), 'index' => 'responsible_institutions', 'name' => 'oadh_rrt_responsible_institutions', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.source'), 'index' => 'source', 'name' => 'oadh_rrt_source', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.legalBase'), 'index' => 'legal_base', 'name' => 'oadh_rrt_legal_base', 'align' => 'left'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.lastModifiedDate'), 'index' => 'last_modified_date', 'name' => 'oadh_rrt_last_modified_date', 'align' => 'center', 'formatter' => 'date'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.observations'), 'index' => 'observations', 'name' => 'oadh_rrt_observations', 'align' => 'left'))
								// ->addColumn(array('label' => Lang::get('form.syncFem'), 'index' => 'syncronized' ,'name' => 'oadh_rrt_syncronized', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.booleanText')), 'align' => 'center' , 'stype' => 'select', 'width' => 85))
								->renderGrid();
							!!}
            </div>
          </div>
        </div>
      </div>

		</div>
	</div>
</div>

<div id='oadh-rrt-import-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-rrt-import-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-rrt-import-header-rows-number', 1, array('id' => 'oadh-rrt-import-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-rrt-import-id', null, array('id' => 'oadh-rrt-import-id')) !!}
											{!! Form::hidden('oadh-rrt-import-name', null, array('id' => 'oadh-rrt-import-name')) !!}
											{!! Form::hidden('oadh-rrt-import-system-route', null, array('id' => 'oadh-rrt-import-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-rrt-import-last-row-number', null, array('id' => 'oadh-rrt-import-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">

											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-rrt-import-date-format', 'd/m/Y', array('id' => 'oadh-rrt-import-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							{{--Fila 1--}}
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-system',Lang::get('decima-oadh::back-end-column.system'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-system','A', array('id' => 'oadh-rrt-import-system','class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-recommendation',Lang::get('decima-oadh::back-end-column.recommendation'), array('class' => 'control-label control-label-hidden', 'style' => 'overflow: hidden;')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-recommendation','B', array('id' => 'oadh-rrt-import-recommendation','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>


								<div class="col-md-2" style="padding-right: 5px;">
									{!! Form::label('oadh-rrt-import-code',Lang::get('decima-oadh::back-end-column.code'), array('class' => 'control-label control-label-hidden', 'style' => '')) !!}
									<div class="form-group mg-hm help-block-hidden">
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-code','C', array('id' => 'oadh-rrt-import-code','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-date',Lang::get('decima-oadh::back-end-column.date'), array('class' => 'control-label control-label-hidden', 'style' => '')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-date','D', array('id' => 'oadh-rrt-import-date','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-mechanism',Lang::get('decima-oadh::back-end-column.mechanism'), array('class' => 'control-label control-label-hidden', 'style' => '')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-mechanism','E', array('id' => 'oadh-rrt-import-mechanism','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-related-rights',Lang::get('decima-oadh::back-end-column.relatedRights'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-related-rights','F', array('id' => 'oadh-rrt-import-related-rights','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>

								
							</div>

							{{--Fila 2--}}
							<div class="row">

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-responsible-institutions',Lang::get('decima-oadh::back-end-column.institutions'), array('class' => 'control-label control-label-hidden', 'style' => '')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-responsible-institutions','G', array('id' => 'oadh-rrt-import-responsible-institutions','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-source',Lang::get('decima-oadh::back-end-column.source'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-source','H', array('id' => 'oadh-rrt-import-source','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>							

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-legal-base',Lang::get('decima-oadh::back-end-column.legalBase'), array('class' => 'control-label control-label-hidden', 'style' => '')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-legal-base','I', array('id' => 'oadh-rrt-import-legal-base','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>

								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-observations',Lang::get('decima-oadh::back-end-column.observations'), array('class' => 'control-label control-label-hidden', 'style' => '')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-observations','J', array('id' => 'oadh-rrt-import-observations','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>
								
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-rrt-import-last-modified-date',Lang::get('decima-oadh::back-end-column.lastModifiedDate'), array('class' => 'control-label control-label-hidden', 'style' => '')) !!}
										<div class="input-group">
											<span class="input-group-addon" style="padding-left: 8px; padding-right: 8px"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-rrt-import-last-modified-date','K', array('id' => 'oadh-rrt-import-last-modified-date','class' => 'form-control', 'maxlength' => '2')) !!}
										</div>
									</div>
								</div>
								
							</div>

							{{-- Fila 3--}}
							<div class="row">
								

								
							</div>

							{{-- Fila 4 --}}
							<div class="row">
								
							</div>
							{{-- Fila 5 --}}
							<div class="row">
								
							</div>
							{{-- Fila 6 --}}
							<div class="row">
								
							</div>
							{{-- Fila 7 --}}
							<div class="row">
								
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-rrt-import-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
		</div>
	</div>
</div>

<div id='oadh-rrt-journals-section' class="row collapse in section-block" data-target-id="">
	
</div>
@include('layouts.search-modal-table')

<div id='oadh-rrt-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-rrt-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-rrt-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-rrt-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-rrt-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFileMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-rrt-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-rrt-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-rrt-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-rrt-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-rrt-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

@include('decima-file::file-uploader')
@parent
@stop
