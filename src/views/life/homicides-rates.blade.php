@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-life-hr-new-action', null, array('id' => 'oadh-life-hr-new-action')) !!}
{!! Form::hidden('oadh-life-hr-edit-action', null, array('id' => 'oadh-life-hr-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-life-hr-remove-action', null, array('id' => 'oadh-life-hr-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-life-hr-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-life-hr-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-life-hr-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhLifeHrOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#oadh-life-hr-grid').getSelectedRowId('file_id'), rowData = $('#oadh-life-hr-grid').getRowData($('#oadh-life-hr-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-life-hr-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-life-hr-', 'firstPage', id);

				$('#oadh-life-hr-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-life-hr-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-life-hr-btn-group-2').enableButtonGroup();

		}

		function oadhLifeHrOnLoadCompleteEvent()
		{
			$('#oadh-life-hr-temp-data').jqGrid('clearGridData');
			$('#oadh-life-hr-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-life-hr-btn-tooltip').tooltip();

			$('#oadh-life-hr-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-life-hr-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-life-hr-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-life-hr-grid-section').collapse('show');

				$('#oadh-life-hr-journals-section').collapse('show');

				$('#oadh-life-hr-filters').show();
			});

			$('#oadh-life-hr-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-life-hr-').focus();
			});

			$('#oadh-life-hr-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-life-hr-btn-refresh').click();
	    });

			$('#oadh-life-hr-').focusout(function()
			{
				$('#oadh-life-hr-btn-save').focus();
			});

			$('#oadh-life-hr-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-life-hr-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-life-hr-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-life-hr-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-life-hr-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-life-hr-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-life-hr-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-life-hr-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-hr-btn-toolbar').disabledButtonGroup();
				$('#oadh-life-hr-btn-group-1').enableButtonGroup();

				if($('#oadh-life-hr-journals-section').attr('data-target-id') == '' || $('#oadh-life-hr-journals-section').attr('data-target-id') == '#oadh-life-hr-form-section')
				{
					$('#oadh-life-hr-grid').trigger('reloadGrid');
					cleanJournals('oadh-life-hr-');
				}
				else
				{

				}
			});

			$('#oadh-life-hr-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-life-hr-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-life-hr-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#oadh-life-hr-detail-btn-export-xls').click(function()
			{
				$('#oadh-life-hr-back-detail-gridXlsButton').click();
			});

			$('#oadh-life-hr-detail-btn-export-csv').click(function()
			{
				$('#oadh-life-hr-back-detail-gridCsvButton').click();
			});

			$('#oadh-life-hr-btn-export-xls').click(function()
			{
				if($('#oadh-life-hr-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-life-hr-gridXlsButton').click();
				}
			});

			$('#oadh-life-hr-btn-export-csv').click(function()
			{
				if($('#oadh-life-hr-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-life-hr-gridCsvButton').click();
				}
			});

			$('#oadh-life-hr-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-hr-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-hr-grid').isRowSelected())
					{
						$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-hr-modal-delete-file').modal('show');
			});

			$('#oadh-life-hr-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-life-hr-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-hr-btn-toolbar', false);
						$('#oadh-life-hr-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-hr-modal-delete-prod').click();
							$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-hr-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-life-hr-grid').trigger('reloadGrid');
						$('#oadh-life-hr-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-life-hr-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-hr-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-hr-grid').isRowSelected())
					{
						$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-hr-modal-delete-prod').modal('show');
			});

			$('#oadh-life-hr-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-life-hr-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-hr-btn-toolbar', false);
						$('#oadh-life-hr-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-hr-modal-delete-prod').click();
							$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-life-hr-prod-data').trigger('reloadGrid');
							$('#oadh-life-hr-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-hr-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-life-hr-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-hr-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-hr-grid').isRowSelected())
					{
						$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-hr-modal-generate').modal('show');
			});

			$('#oadh-life-hr-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-life-hr-grid').getRowData($('#oadh-life-hr-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-hr-btn-toolbar', false);
						$('#oadh-life-hr-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-hr-btn-refresh').click();
							$("#oadh-life-hr-btn-group-2").disabledButtonGroup();
							$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-life-hr-btn-refresh').click();
							$("#oadh-life-hr-btn-group-2").disabledButtonGroup();
							$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-hr-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-life-hr-btn-process').click(function()
			{

				if(!$('#oadh-life-hr-grid').isRowSelected())
				{
					$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-life-hr-grid').getRowData($('#oadh-life-hr-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-life-hr-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-life-hr-id').val(rowData.file_id);
				$('#oadh-life-hr-name').val(rowData.file_name);
				$('#oadh-life-hr-system-route').val(rowData.file_system_route);

				$('#oadh-life-hr-mp-modal').modal('show');
			});

			$('#oadh-life-hr-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-life-hr-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-life-hr-mp-form').formToObject('oadh-life-hr-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-hr-btn-toolbar', false);
						$('#oadh-life-hr-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-life-hr-btn-refresh').click();
							$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-life-hr-temp-data, #oadh-life-hr-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-life-hr-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-life-hr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-life-hr-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-life-hr-edit-action').isEmpty())
			{
				showButtonHelper('oadh-life-hr-btn-close', 'oadh-life-hr-btn-group-2', $('#oadh-life-hr-edit-action').attr('data-content'));
			}

			$('#oadh-life-hr-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-life-hr-btn-close', 'oadh-life-hr-btn-group-2', $('#oadh-life-hr-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-life-hr-delete-action').isEmpty())
			{
				showButtonHelper('oadh-life-hr-btn-close', 'oadh-life-hr-btn-group-2', $('#oadh-life-hr-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-life-hr-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-life-hr-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-life-hr-btn-upload', 'class' => 'btn btn-default oadh-life-hr-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-life-hr-btn-refresh', 'class' => 'btn btn-default oadh-life-hr-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-life-hr-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-life-hr-btn-process', 'class' => 'btn btn-default oadh-life-hr-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-life-hr-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-life-hr-btn-delete-prod', 'class' => 'btn btn-default oadh-life-hr-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-life-hr-btn-delete-file', 'class' => 'btn btn-default oadh-life-hr-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-life-hr-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-life-hr-grid'>
				{!!
				GridRender::setGridId("oadh-life-hr-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhLifeHrOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhLifeHrOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-life-hr-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-life-hr-temp-data-tab" aria-controls="oadh-life-hr-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-life-hr-processed-data-tab" aria-controls="oadh-life-hr-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-life-hr-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-life-hr-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-life-hr-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/life/life-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
								->addColumn(array('index' => 'id', 'name' => 'oadh_life_hr_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_life_hr_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.record'), 'index' => 'record', 'name'=> 'oadh_life_hr_record', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name'=> 'oadh_life_hr_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.month'), 'index' => 'month', 'name'=> 'oadh_life_hr_month', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.day'), 'index' => 'day', 'name'=> 'oadh_life_hr_day', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.timeAggression'), 'index' => 'time_aggression', 'name'=> 'oadh_life_hr_time_aggression', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'age', 'name'=> 'oadh_life_hr_age', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'sex', 'name'=> 'oadh_life_hr_sex', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.occupation'), 'index' => 'occupation', 'name'=> 'oadh_life_hr_occupation', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'municipality', 'name'=> 'oadh_life_hr_municipality', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'department', 'name'=> 'oadh_life_hr_department', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.weaponType'), 'index' => 'weapon_type', 'name'=> 'oadh_life_hr_weapon_type', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_life_hr_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-life-hr-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-life-hr-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-life-hr-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/life/life-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_life_hr_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_life_hr_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.record'), 'index' => 'record', 'name'=> 'oadh_life_hr_record', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name'=> 'oadh_life_hr_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.month'), 'index' => 'month', 'name'=> 'oadh_life_hr_month', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.day'), 'index' => 'day', 'name'=> 'oadh_life_hr_day', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.timeAggression'), 'index' => 'time_aggression', 'name'=> 'oadh_life_hr_time_aggression', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'age', 'name'=> 'oadh_life_hr_age', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'sex', 'name'=> 'oadh_life_hr_sex', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.occupation'), 'index' => 'occupation', 'name'=> 'oadh_life_hr_occupation', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'municipality', 'name'=> 'oadh_life_hr_municipality', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'department', 'name'=> 'oadh_life_hr_department', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.weaponType'), 'index' => 'weapon_type', 'name'=> 'oadh_life_hr_weapon_type', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-life-hr-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-life-hr-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-life-hr-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-life-hr-header-rows-number', 1 , array('id' => 'oadh-life-hr-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-life-hr-id', null, array('id' => 'oadh-life-hr-id')) !!}
											{!! Form::hidden('oadh-life-hr-name', null, array('id' => 'oadh-life-hr-name')) !!}
											{!! Form::hidden('oadh-life-hr-system-route', null, array('id' => 'oadh-life-hr-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-life-hr-last-row-number', null, array('id' => 'oadh-life-hr-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-time-format', Lang::get('decima-oadh::back-end-general.timeFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-life-hr-time-format', 'Hi', array('id' => 'oadh-life-hr-time-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-record', Lang::get('decima-oadh::back-end-column.record'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-record', 'A' , array('id' => 'oadh-life-hr-record', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-year', 'B' , array('id' => 'oadh-life-hr-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-month', Lang::get('decima-oadh::back-end-column.month'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-month', 'C' , array('id' => 'oadh-life-hr-month', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-day', Lang::get('decima-oadh::back-end-column.day'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-day', 'D' , array('id' => 'oadh-life-hr-day', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-time-aggression', Lang::get('decima-oadh::back-end-column.timeAggression'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-time-aggression', 'E' , array('id' => 'oadh-life-hr-time-aggression', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-age', Lang::get('decima-oadh::back-end-column.age'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-age', 'F' , array('id' => 'oadh-life-hr-age', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-sex', 'G' , array('id' => 'oadh-life-hr-sex', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-occupation', Lang::get('decima-oadh::back-end-column.occupation'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-occupation', 'H' , array('id' => 'oadh-life-hr-occupation', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-municipality', 'I' , array('id' => 'oadh-life-hr-municipality', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-department', 'J' , array('id' => 'oadh-life-hr-department', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-hr-weapon-type', Lang::get('decima-oadh::back-end-column.weaponType'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-hr-weapon-type', 'K' , array('id' => 'oadh-life-hr-weapon-type', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-life-hr-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-life-hr-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-hr-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-hr-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-life-hr-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-hr-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-hr-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-life-hr-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-hr-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-life-hr-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-hr-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
