@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-life-aw-new-action', null, array('id' => 'oadh-life-aw-new-action')) !!}
{!! Form::hidden('oadh-life-aw-edit-action', null, array('id' => 'oadh-life-aw-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-life-aw-remove-action', null, array('id' => 'oadh-life-aw-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-life-aw-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-life-aw-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-life-aw-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhLifeAwOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#oadh-life-aw-grid').getSelectedRowId('file_id'), rowData = $('#oadh-life-aw-grid').getRowData($('#oadh-life-aw-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-life-aw-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-life-aw-', 'firstPage', id);

				$('#oadh-life-aw-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-life-aw-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-life-aw-btn-group-2').enableButtonGroup();

		}

		function oadhLifeAwOnLoadCompleteEvent()
		{
			$('#oadh-life-aw-temp-data').jqGrid('clearGridData');
			$('#oadh-life-aw-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-life-aw-btn-tooltip').tooltip();

			$('#oadh-life-aw-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-life-aw-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-life-aw-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-life-aw-grid-section').collapse('show');

				$('#oadh-life-aw-journals-section').collapse('show');

				$('#oadh-life-aw-filters').show();
			});

			$('#oadh-life-aw-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-life-aw-').focus();
			});

			$('#oadh-life-aw-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-life-aw-btn-refresh').click();
	    });

			$('#oadh-life-aw-').focusout(function()
			{
				$('#oadh-life-aw-btn-save').focus();
			});

			$('#oadh-life-aw-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-life-aw-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-life-aw-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-life-aw-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-life-aw-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-life-aw-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-life-aw-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-life-aw-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-aw-btn-toolbar').disabledButtonGroup();
				$('#oadh-life-aw-btn-group-1').enableButtonGroup();

				if($('#oadh-life-aw-journals-section').attr('data-target-id') == '' || $('#oadh-life-aw-journals-section').attr('data-target-id') == '#oadh-life-aw-form-section')
				{
					$('#oadh-life-aw-grid').trigger('reloadGrid');
					cleanJournals('oadh-life-aw-');
				}
				else
				{

				}
			});

			$('#oadh-life-aw-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-life-aw-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-life-aw-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#oadh-life-aw-detail-btn-export-xls').click(function()
			{
				$('#oadh-life-aw-back-detail-gridXlsButton').click();
			});

			$('#oadh-life-aw-detail-btn-export-csv').click(function()
			{
				$('#oadh-life-aw-back-detail-gridCsvButton').click();
			});

			$('#oadh-life-aw-btn-export-xls').click(function()
			{
				if($('#oadh-life-aw-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-life-aw-gridXlsButton').click();
				}
			});

			$('#oadh-life-aw-btn-export-csv').click(function()
			{
				if($('#oadh-life-aw-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-life-aw-gridCsvButton').click();
				}
			});

			$('#oadh-life-aw-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-aw-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-aw-grid').isRowSelected())
					{
						$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-aw-modal-delete-file').modal('show');
			});

			$('#oadh-life-aw-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-life-aw-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-aw-btn-toolbar', false);
						$('#oadh-life-aw-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-aw-modal-delete-prod').click();
							$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-aw-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-life-aw-grid').trigger('reloadGrid');
						$('#oadh-life-aw-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-life-aw-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-aw-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-aw-grid').isRowSelected())
					{
						$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-aw-modal-delete-prod').modal('show');
			});

			$('#oadh-life-aw-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-life-aw-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-aw-btn-toolbar', false);
						$('#oadh-life-aw-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-aw-modal-delete-prod').click();
							$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-life-aw-prod-data').trigger('reloadGrid');
							$('#oadh-life-aw-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-aw-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-life-aw-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-aw-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-aw-grid').isRowSelected())
					{
						$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-aw-modal-generate').modal('show');
			});

			$('#oadh-life-aw-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-life-aw-grid').getRowData($('#oadh-life-aw-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-aw-btn-toolbar', false);
						$('#oadh-life-aw-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-aw-btn-refresh').click();
							$("#oadh-life-aw-btn-group-2").disabledButtonGroup();
							$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-life-aw-btn-refresh').click();
							$("#oadh-life-aw-btn-group-2").disabledButtonGroup();
							$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-aw-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-life-aw-btn-process').click(function()
			{

				if(!$('#oadh-life-aw-grid').isRowSelected())
				{
					$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-life-aw-grid').getRowData($('#oadh-life-aw-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-life-aw-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-life-aw-id').val(rowData.file_id);
				$('#oadh-life-aw-name').val(rowData.file_name);
				$('#oadh-life-aw-system-route').val(rowData.file_system_route);

				$('#oadh-life-aw-mp-modal').modal('show');
			});

			$('#oadh-life-aw-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-life-aw-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-life-aw-mp-form').formToObject('oadh-life-aw-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-aw-btn-toolbar', false);
						$('#oadh-life-aw-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-life-aw-btn-refresh').click();
							$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-life-aw-temp-data, #oadh-life-aw-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-life-aw-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-life-aw-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-life-aw-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-life-aw-edit-action').isEmpty())
			{
				showButtonHelper('oadh-life-aw-btn-close', 'oadh-life-aw-btn-group-2', $('#oadh-life-aw-edit-action').attr('data-content'));
			}

			$('#oadh-life-aw-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-life-aw-btn-close', 'oadh-life-aw-btn-group-2', $('#oadh-life-aw-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-life-aw-delete-action').isEmpty())
			{
				showButtonHelper('oadh-life-aw-btn-close', 'oadh-life-aw-btn-group-2', $('#oadh-life-aw-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-life-aw-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-life-aw-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-life-aw-btn-upload', 'class' => 'btn btn-default oadh-life-aw-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-life-aw-btn-refresh', 'class' => 'btn btn-default oadh-life-aw-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-life-aw-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-life-aw-btn-process', 'class' => 'btn btn-default oadh-life-aw-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-life-aw-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-life-aw-btn-delete-prod', 'class' => 'btn btn-default oadh-life-aw-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-life-aw-btn-delete-file', 'class' => 'btn btn-default oadh-life-aw-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-life-aw-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-life-aw-grid'>
				{!!
				GridRender::setGridId("oadh-life-aw-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhLifeAwOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhLifeAwOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-life-aw-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-life-aw-temp-data-tab" aria-controls="oadh-life-aw-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-life-aw-processed-data-tab" aria-controls="oadh-life-aw-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-life-aw-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-life-aw-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-life-aw-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/life/life-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh-life-aw-id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh-life-aw-file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'department', 'name' => 'oadh_life_aw_department', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'municipality', 'name' => 'oadh_life_aw_municipality', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.day'), 'index' => 'day', 'name' => 'oadh_life_aw_day', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.dayNumber'), 'index' => 'day_number', 'name' => 'oadh_life_aw_day_number', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.month'), 'index' => 'month', 'name' => 'oadh_life_aw_month', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_life_aw_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'date', 'name' => 'oadh_life_aw_date', 'align' => 'center', 'width' => '10', 'hidden' => false, 'formatter' => 'date'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.time'), 'index' => 'time', 'name' => 'oadh_life_aw_time', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.gang'), 'index' => 'gang', 'name' => 'oadh_life_aw_gang', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.division'), 'index' => 'division', 'name' => 'oadh_life_aw_division', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.aggressions'), 'index' => 'aggressions', 'name' => 'oadh_life_aw_aggressions', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.deceased'), 'index' => 'deceased', 'name' => 'oadh_life_aw_deceased', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_life_aw_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-life-aw-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-life-aw-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-life-aw-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/life/life-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh-life-aw-id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh-life-aw-file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'department', 'name' => 'oadh_life_aw_department', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'municipality', 'name' => 'oadh_life_aw_municipality', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.day'), 'index' => 'day', 'name' => 'oadh_life_aw_day', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.dayNumber'), 'index' => 'day_number', 'name' => 'oadh_life_aw_day_number', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.month'), 'index' => 'month', 'name' => 'oadh_life_aw_month', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_life_aw_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'date', 'name' => 'oadh_life_aw_date', 'align' => 'center', 'width' => '10', 'hidden' => false, 'formatter' => 'date'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.time'), 'index' => 'time', 'name' => 'oadh_life_aw_time', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.gang'), 'index' => 'gang', 'name' => 'oadh_life_aw_gang', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.division'), 'index' => 'division', 'name' => 'oadh_life_aw_division', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.aggressions'), 'index' => 'aggressions', 'name' => 'oadh_life_aw_aggressions', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.deceased'), 'index' => 'deceased', 'name' => 'oadh_life_aw_deceased', 'align' => 'center', 'width' => '10', 'hidden' => false))
      		    	->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-life-aw-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-life-aw-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-life-aw-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-life-aw-header-rows-number', 1 , array('id' => 'oadh-life-aw-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-life-aw-id', null, array('id' => 'oadh-life-aw-id')) !!}
											{!! Form::hidden('oadh-life-aw-name', null, array('id' => 'oadh-life-aw-name')) !!}
											{!! Form::hidden('oadh-life-aw-system-route', null, array('id' => 'oadh-life-aw-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-life-aw-last-row-number', null, array('id' => 'oadh-life-aw-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-life-aw-date-format', 'm/d/Y', array('id' => 'oadh-life-aw-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-time-format', Lang::get('decima-oadh::back-end-general.timeFormat'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-life-aw-time-format', 'H:i', array('id' => 'oadh-life-aw-time-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					 <div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-department', 'A' , array('id' => 'oadh-life-aw-department', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-municipality', 'B' , array('id' => 'oadh-life-aw-municipality', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-day', Lang::get('decima-oadh::back-end-column.day'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-day', 'C' , array('id' => 'oadh-life-aw-day', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-day_number', Lang::get('decima-oadh::back-end-column.dayNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-day_number', 'D' , array('id' => 'oadh-life-aw-day_number', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-day', Lang::get('decima-oadh::back-end-column.month'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-month', 'E' , array('id' => 'oadh-life-aw-month', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-year', 'F' , array('id' => 'oadh-life-aw-year', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-date', Lang::get('decima-oadh::back-end-column.date'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-date', 'G' , array('id' => 'oadh-life-aw-date', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-time', Lang::get('decima-oadh::back-end-column.time'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-time', 'H' , array('id' => 'oadh-life-aw-time', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-gang', Lang::get('decima-oadh::back-end-column.gang'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-gang', 'I' , array('id' => 'oadh-life-aw-gang', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-division', Lang::get('decima-oadh::back-end-column.division'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-division', 'J' , array('id' => 'oadh-life-aw-division', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-aggressions', Lang::get('decima-oadh::back-end-column.aggressions'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-aggressions', 'K' , array('id' => 'oadh-life-aw-aggressions', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-aw-deceased', Lang::get('decima-oadh::back-end-column.deceased'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-aw-deceased', 'L' , array('id' => 'oadh-life-aw-deceased', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-life-aw-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-life-aw-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-aw-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-aw-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-life-aw-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-aw-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-aw-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-life-aw-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-aw-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-life-aw-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-aw-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
