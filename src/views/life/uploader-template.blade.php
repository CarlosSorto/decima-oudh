@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-derecho-prefijo-new-action', null, array('id' => 'oadh-derecho-prefijo-new-action')) !!}
{!! Form::hidden('oadh-derecho-prefijo-edit-action', null, array('id' => 'oadh-derecho-prefijo-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-derecho-prefijo-remove-action', null, array('id' => 'oadh-derecho-prefijo-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-derecho-prefijo-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-derecho-prefijo-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-derecho-prefijo-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhDerechoPrefijoOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#oadh-derecho-prefijo-grid').getSelectedRowId('file_id'), rowData = $('#oadh-derecho-prefijo-grid').getRowData($('#oadh-derecho-prefijo-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-derecho-prefijo-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-derecho-prefijo-', 'firstPage', id);

				$('#oadh-derecho-prefijo-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-derecho-prefijo-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-derecho-prefijo-btn-group-2').enableButtonGroup();

		}

		function oadhDerechoPrefijoOnLoadCompleteEvent()
		{
			$('#oadh-derecho-prefijo-temp-data').jqGrid('clearGridData');
			$('#oadh-derecho-prefijo-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-derecho-prefijo-btn-tooltip').tooltip();

			$('#oadh-derecho-prefijo-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-derecho-prefijo-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-derecho-prefijo-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-derecho-prefijo-grid-section').collapse('show');

				$('#oadh-derecho-prefijo-journals-section').collapse('show');

				$('#oadh-derecho-prefijo-filters').show();
			});

			$('#oadh-derecho-prefijo-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-derecho-prefijo-').focus();
			});

			$('#oadh-derecho-prefijo-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-derecho-prefijo-btn-refresh').click();
	    });

			$('#oadh-derecho-prefijo-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-derecho-prefijo-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-derecho-prefijo-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-derecho-prefijo-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-derecho-prefijo-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-derecho-prefijo-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-derecho-prefijo-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-derecho-prefijo-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-derecho-prefijo-btn-toolbar').disabledButtonGroup();
				$('#oadh-derecho-prefijo-btn-group-1').enableButtonGroup();
				$('#oadh-derecho-prefijo-grid').trigger('reloadGrid');
				cleanJournals('oadh-derecho-prefijo-');
			});

			// $('#oadh-derecho-prefijo-detail-btn-refresh').click(function()
			// {
			// 	$('.decima-erp-tooltip').tooltip('hide');

			// 	$('#oadh-derecho-prefijo-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-derecho-prefijo-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			// });

			// $('#oadh-derecho-prefijo-detail-btn-export-xls').click(function()
			// {
			// 	$('#oadh-derecho-prefijo-back-detail-gridXlsButton').click();
			// });

			// $('#oadh-derecho-prefijo-detail-btn-export-csv').click(function()
			// {
			// 	$('#oadh-derecho-prefijo-back-detail-gridCsvButton').click();
			// });

			// $('#oadh-derecho-prefijo-btn-export-xls').click(function()
			// {
			// 	if($('#oadh-derecho-prefijo-journals-section').attr('data-target-id') == '')
			// 	{
			// 		$('#oadh-derecho-prefijo-gridXlsButton').click();
			// 	}
			// });

			// $('#oadh-derecho-prefijo-btn-export-csv').click(function()
			// {
			// 	if($('#oadh-derecho-prefijo-journals-section').attr('data-target-id') == '')
			// 	{
			// 		$('#oadh-derecho-prefijo-gridCsvButton').click();
			// 	}
			// });

			$('#oadh-derecho-prefijo-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-derecho-prefijo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-derecho-prefijo-grid').isRowSelected())
					{
						$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-derecho-prefijo-modal-delete-file').modal('show');
			});

			$('#oadh-derecho-prefijo-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-derecho-prefijo-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/derecho/derecho-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-derecho-prefijo-btn-toolbar', false);
						$('#oadh-derecho-prefijo-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-derecho-prefijo-modal-delete-prod').click();
							$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-derecho-prefijo-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-derecho-prefijo-grid').trigger('reloadGrid');
						$('#oadh-derecho-prefijo-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-derecho-prefijo-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-derecho-prefijo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-derecho-prefijo-grid').isRowSelected())
					{
						$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-derecho-prefijo-modal-delete-prod').modal('show');
			});

			$('#oadh-derecho-prefijo-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-derecho-prefijo-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/derecho/derecho-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-derecho-prefijo-btn-toolbar', false);
						$('#oadh-derecho-prefijo-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-derecho-prefijo-modal-delete-prod').click();
							$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-derecho-prefijo-prod-data').trigger('reloadGrid');
							$('#oadh-derecho-prefijo-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-derecho-prefijo-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-derecho-prefijo-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-derecho-prefijo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-derecho-prefijo-grid').isRowSelected())
					{
						$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-derecho-prefijo-modal-generate').modal('show');
			});

			$('#oadh-derecho-prefijo-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-derecho-prefijo-grid').getRowData($('#oadh-derecho-prefijo-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/derecho/derecho-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-derecho-prefijo-btn-toolbar', false);
						$('#oadh-derecho-prefijo-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-derecho-prefijo-btn-refresh').click();
							$("#oadh-derecho-prefijo-btn-group-2").disabledButtonGroup();
							$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-derecho-prefijo-btn-refresh').click();
							$("#oadh-derecho-prefijo-btn-group-2").disabledButtonGroup();
							$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-derecho-prefijo-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-derecho-prefijo-btn-process').click(function()
			{

				if(!$('#oadh-derecho-prefijo-grid').isRowSelected())
				{
					$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-derecho-prefijo-grid').getRowData($('#oadh-derecho-prefijo-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-derecho-prefijo-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-derecho-prefijo-id').val(rowData.file_id);
				$('#oadh-derecho-prefijo-name').val(rowData.file_name);
				$('#oadh-derecho-prefijo-system-route').val(rowData.file_system_route);

				$('#oadh-derecho-prefijo-mp-modal').modal('show');
			});

			$('#oadh-derecho-prefijo-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-derecho-prefijo-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-derecho-prefijo-mp-form').formToObject('oadh-derecho-prefijo-');
				// data['columns'] = ['year', 'court', 'sex'];
				// data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/derecho/derecho-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-derecho-prefijo-btn-toolbar', false);
						$('#oadh-derecho-prefijo-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-derecho-prefijo-btn-refresh').click();
							$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-derecho-prefijo-temp-data, #oadh-derecho-prefijo-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-derecho-prefijo-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-derecho-prefijo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-derecho-prefijo-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-derecho-prefijo-edit-action').isEmpty())
			{
				showButtonHelper('oadh-derecho-prefijo-btn-close', 'oadh-derecho-prefijo-btn-group-2', $('#oadh-derecho-prefijo-edit-action').attr('data-content'));
			}

			$('#oadh-derecho-prefijo-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-derecho-prefijo-btn-close', 'oadh-derecho-prefijo-btn-group-2', $('#oadh-derecho-prefijo-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-derecho-prefijo-delete-action').isEmpty())
			{
				showButtonHelper('oadh-derecho-prefijo-btn-close', 'oadh-derecho-prefijo-btn-group-2', $('#oadh-derecho-prefijo-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-derecho-prefijo-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-derecho-prefijo-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-derecho-prefijo-btn-upload', 'class' => 'btn btn-default oadh-derecho-prefijo-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-derecho-prefijo-btn-refresh', 'class' => 'btn btn-default oadh-derecho-prefijo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-derecho-prefijo-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-derecho-prefijo-btn-process', 'class' => 'btn btn-default oadh-derecho-prefijo-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-derecho-prefijo-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-derecho-prefijo-btn-delete-prod', 'class' => 'btn btn-default oadh-derecho-prefijo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-derecho-prefijo-btn-delete-file', 'class' => 'btn btn-default oadh-derecho-prefijo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-derecho-prefijo-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-derecho-prefijo-grid'>
				{!!
				GridRender::setGridId("oadh-derecho-prefijo-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhDerechoPrefijoOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhDerechoPrefijoOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-derecho-prefijo-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-derecho-prefijo-temp-data-tab" aria-controls="oadh-derecho-prefijo-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-derecho-prefijo-processed-data-tab" aria-controls="oadh-derecho-prefijo-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-derecho-prefijo-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-derecho-prefijo-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-derecho-prefijo-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/derecho/derecho-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_derecho_prefijo_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_derecho_prefijo_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_derecho_prefijo_year', 'align' => 'center', 'width' => 40, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'sex', 'name' => 'oadh_derecho_prefijo_sex', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.court'), 'index' => 'court', 'name' => 'oadh_derecho_prefijo_court', 'align' => 'center', 'width' => 80, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_derecho_prefijo_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => 50))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-derecho-prefijo-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-derecho-prefijo-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-derecho-prefijo-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/derecho/derecho-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_derecho_prefijo_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_derecho_prefijo_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_derecho_prefijo_year', 'align' => 'center', 'width' => 40, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'sex', 'name' => 'oadh_derecho_prefijo_sex', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.court'), 'index' => 'court', 'name' => 'oadh_derecho_prefijo_court', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '100'))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-derecho-prefijo-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-derecho-prefijo-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-derecho-prefijo-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-derecho-prefijo-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-derecho-prefijo-header-rows-number', 1 , array('id' => 'oadh-derecho-prefijo-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-derecho-prefijo-id', null, array('id' => 'oadh-derecho-prefijo-id')) !!}
											{!! Form::hidden('oadh-derecho-prefijo-name', null, array('id' => 'oadh-derecho-prefijo-name')) !!}
											{!! Form::hidden('oadh-derecho-prefijo-system-route', null, array('id' => 'oadh-derecho-prefijo-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-derecho-prefijo-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-derecho-prefijo-last-row-number', null, array('id' => 'oadh-derecho-prefijo-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-derecho-prefijo-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-derecho-prefijo-mp-date-format', null, array('id' => 'oadh-derecho-prefijo-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-derecho-prefijo-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-derecho-prefijo-year', 'A' , array('id' => 'oadh-derecho-prefijo-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-derecho-prefijo-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-derecho-prefijo-sex', 'B' , array('id' => 'oadh-derecho-prefijo-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-derecho-prefijo-court', Lang::get('decima-oadh::back-end-column.court'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-derecho-prefijo-court', 'C' , array('id' => 'oadh-derecho-prefijo-court', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-derecho-prefijo-status', Lang::get('decima-oadh::back-end-general.status'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-derecho-prefijo-status', 'D' , array('id' => 'oadh-derecho-prefijo-status', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-derecho-prefijo-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-derecho-prefijo-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-derecho-prefijo-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-derecho-prefijo-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-derecho-prefijo-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-derecho-prefijo-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-derecho-prefijo-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-derecho-prefijo-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-derecho-prefijo-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-derecho-prefijo-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-derecho-prefijo-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
