@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-life-oi-new-action', null, array('id' => 'oadh-life-oi-new-action')) !!}
{!! Form::hidden('oadh-life-oi-edit-action', null, array('id' => 'oadh-life-oi-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-life-oi-remove-action', null, array('id' => 'oadh-life-oi-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-life-oi-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-life-oi-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-life-oi-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhLifeOiOnSelectRowEvent()
		{
			// var id = 1; oadh-life-oi
			var id = $('#oadh-life-oi-grid').getSelectedRowId('file_id'), rowData = $('#oadh-life-oi-grid').getRowData($('#oadh-life-oi-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-life-oi-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-life-oi-', 'firstPage', id);

				$('#oadh-life-oi-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-life-oi-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-life-oi-btn-group-2').enableButtonGroup();

		}

		function oadhLifeOiOnLoadCompleteEvent()
		{
			$('#oadh-life-oi-temp-data').jqGrid('clearGridData');
			$('#oadh-life-oi-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-life-oi-btn-tooltip').tooltip();

			$('#oadh-life-oi-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-life-oi-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-life-oi-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-life-oi-grid-section').collapse('show');

				$('#oadh-life-oi-journals-section').collapse('show');

				$('#oadh-life-oi-filters').show();
			});

			$('#oadh-life-oi-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-life-oi-').focus();
			});

			$('#oadh-life-oi-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-life-oi-btn-refresh').click();
	    });

			$('#oadh-life-oi-').focusout(function()
			{
				$('#oadh-life-oi-btn-save').focus();
			});

			$('#oadh-life-oi-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-life-oi-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-life-oi-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-life-oi-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-life-oi-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-life-oi-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-life-oi-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-life-oi-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-oi-btn-toolbar').disabledButtonGroup();
				$('#oadh-life-oi-btn-group-1').enableButtonGroup();

				if($('#oadh-life-oi-journals-section').attr('data-target-id') == '' || $('#oadh-life-oi-journals-section').attr('data-target-id') == '#oadh-life-oi-form-section')
				{
					$('#oadh-life-oi-grid').trigger('reloadGrid');
					cleanJournals('oadh-life-oi-');
				}
				else
				{

				}
			});

			$('#oadh-life-oi-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-life-oi-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-life-oi-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#oadh-life-oi-detail-btn-export-xls').click(function()
			{
				$('#oadh-life-oi-back-detail-gridXlsButton').click();
			});

			$('#oadh-life-oi-detail-btn-export-csv').click(function()
			{
				$('#oadh-life-oi-back-detail-gridCsvButton').click();
			});

			$('#oadh-life-oi-btn-export-xls').click(function()
			{
				if($('#oadh-life-oi-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-life-oi-gridXlsButton').click();
				}
			});

			$('#oadh-life-oi-btn-export-csv').click(function()
			{
				if($('#oadh-life-oi-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-life-oi-gridCsvButton').click();
				}
			});

			$('#oadh-life-oi-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-oi-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-oi-grid').isRowSelected())
					{
						$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-oi-modal-delete-file').modal('show');
			});

			$('#oadh-life-oi-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-life-oi-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-oi-btn-toolbar', false);
						$('#oadh-life-oi-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-oi-modal-delete-prod').click();
							$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-oi-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-life-oi-grid').trigger('reloadGrid');
						$('#oadh-life-oi-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-life-oi-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-oi-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-oi-grid').isRowSelected())
					{
						$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-oi-modal-delete-prod').modal('show');
			});

			$('#oadh-life-oi-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-life-oi-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-oi-btn-toolbar', false);
						$('#oadh-life-oi-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-oi-modal-delete-prod').click();
							$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-life-oi-prod-data').trigger('reloadGrid');
							$('#oadh-life-oi-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-oi-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-life-oi-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-life-oi-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-life-oi-grid').isRowSelected())
					{
						$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-life-oi-modal-generate').modal('show');
			});

			$('#oadh-life-oi-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-life-oi-grid').getRowData($('#oadh-life-oi-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-oi-btn-toolbar', false);
						$('#oadh-life-oi-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-life-oi-btn-refresh').click();
							$("#oadh-life-oi-btn-group-2").disabledButtonGroup();
							$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-life-oi-btn-refresh').click();
							$("#oadh-life-oi-btn-group-2").disabledButtonGroup();
							$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-life-oi-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-life-oi-btn-process').click(function()
			{

				if(!$('#oadh-life-oi-grid').isRowSelected())
				{
					$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-life-oi-grid').getRowData($('#oadh-life-oi-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-life-oi-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-life-oi-id').val(rowData.file_id);
				$('#oadh-life-oi-name').val(rowData.file_name);
				$('#oadh-life-oi-system-route').val(rowData.file_system_route);

				$('#oadh-life-oi-mp-modal').modal('show');
			});

			$('#oadh-life-oi-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-life-oi-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-life-oi-mp-form').formToObject('oadh-life-oi-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/life/life-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-life-oi-btn-toolbar', false);
						$('#oadh-life-oi-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-life-oi-btn-refresh').click();
							$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-life-oi-temp-data, #oadh-life-oi-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-life-oi-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-life-oi-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-life-oi-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-life-oi-edit-action').isEmpty())
			{
				showButtonHelper('oadh-life-oi-btn-close', 'oadh-life-oi-btn-group-2', $('#oadh-life-oi-edit-action').attr('data-content'));
			}

			$('#oadh-life-oi-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-life-oi-btn-close', 'oadh-life-oi-btn-group-2', $('#oadh-life-oi-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-life-oi-delete-action').isEmpty())
			{
				showButtonHelper('oadh-life-oi-btn-close', 'oadh-life-oi-btn-group-2', $('#oadh-life-oi-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-life-oi-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-life-oi-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-life-oi-btn-upload', 'class' => 'btn btn-default oadh-life-oi-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-life-oi-btn-refresh', 'class' => 'btn btn-default oadh-life-oi-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-life-oi-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-life-oi-btn-process', 'class' => 'btn btn-default oadh-life-oi-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-life-oi-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-life-oi-btn-delete-prod', 'class' => 'btn btn-default oadh-life-oi-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-life-oi-btn-delete-file', 'class' => 'btn btn-default oadh-life-oi-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-life-oi-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-life-oi-grid'>
				{!!
				GridRender::setGridId("oadh-life-oi-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhLifeOiOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhLifeOiOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-life-oi-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-life-oi-temp-data-tab" aria-controls="oadh-life-oi-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-life-oi-processed-data-tab" aria-controls="oadh-life-oi-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-life-oi-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-life-oi-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-life-oi-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/life/life-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_life_oi_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_life_oi_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.code'), 'index' => 'record', 'name' => 'oadh_life_oi_record', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.investigationType'), 'index' => 'investigation_type', 'name' => 'oadh_life_oi_investigation_type', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_life_oi_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'crime', 'name' => 'oadh_life_oi_crime', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.category'), 'index' => 'category', 'name' => 'oadh_life_oi_category', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.position'), 'index' => 'position', 'name' => 'oadh_life_oi_position', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'officer_sex', 'name' => 'oadh_life_oi_officer_sex', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.locationPlace'), 'index' => 'location_place', 'name' => 'oadh_life_oi_location_place', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'location_department', 'name' => 'oadh_life_oi_location_department', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'location_municipality', 'name' => 'oadh_life_oi_location_municipality', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'date', 'name' => 'oadh_life_oi_date', 'align' => 'center', 'width' => '10', 'hidden' => false, 'formatter' => 'date'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.actPlace'), 'index' => 'act_place', 'name' => 'oadh_life_oi_act_place', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'act_department', 'name' => 'oadh_life_oi_act_department', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'act_municipality', 'name' => 'oadh_life_oi_act_municipality', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.weaponType'), 'index' => 'weapon_type', 'name' => 'oadh_life_oi_weapon_type', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.victimQuantity'), 'index' => 'victim_quantity', 'name' => 'oadh_life_oi_victim_quantity', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.victimSex'), 'index' => 'victim_sex', 'name' => 'oadh_life_oi_victim_sex', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.criminalJusticeProcess'), 'index' => 'criminal_justice_process', 'name' => 'oadh_life_oi_criminal_justice_process', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_life_oi_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-life-oi-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-life-oi-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-life-oi-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/life/life-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_life_oi_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_life_oi_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.code'), 'index' => 'record', 'name' => 'oadh_life_oi_record', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.investigationType'), 'index' => 'investigation_type', 'name' => 'oadh_life_oi_investigation_type', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_life_oi_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'crime', 'name' => 'oadh_life_oi_crime', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.category'), 'index' => 'category', 'name' => 'oadh_life_oi_category', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.position'), 'index' => 'position', 'name' => 'oadh_life_oi_position', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'officer_sex', 'name' => 'oadh_life_oi_officer_sex', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.locationPlace'), 'index' => 'location_place', 'name' => 'oadh_life_oi_location_place', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'location_department', 'name' => 'oadh_life_oi_location_department', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'location_municipality', 'name' => 'oadh_life_oi_location_municipality', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.date'), 'index' => 'date', 'name' => 'oadh_life_oi_date', 'align' => 'center', 'width' => '10', 'hidden' => false, 'formatter' => 'date'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.actPlace'), 'index' => 'act_place', 'name' => 'oadh_life_oi_act_place', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'act_department', 'name' => 'oadh_life_oi_act_department', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'act_municipality', 'name' => 'oadh_life_oi_act_municipality', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.weaponType'), 'index' => 'weapon_type', 'name' => 'oadh_life_oi_weapon_type', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.victimQuantity'), 'index' => 'victim_quantity', 'name' => 'oadh_life_oi_victim_quantity', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'victim_sex', 'name' => 'oadh_life_oi_victim_sex', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.sexGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.criminalJusticeProcess'), 'index' => 'criminal_justice_process', 'name' => 'oadh_life_oi_criminal_justice_process', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-life-oi-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-life-oi-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-life-oi-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-life-oi-header-rows-number', 1 , array('id' => 'oadh-life-oi-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-life-oi-id', null, array('id' => 'oadh-life-oi-id')) !!}
											{!! Form::hidden('oadh-life-oi-name', null, array('id' => 'oadh-life-oi-name')) !!}
											{!! Form::hidden('oadh-life-oi-system-route', null, array('id' => 'oadh-life-oi-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-life-oi-last-row-number', null, array('id' => 'oadh-life-oi-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-life-oi-date-format', 'd/m/Y', array('id' => 'oadh-life-oi-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-record', Lang::get('decima-oadh::back-end-column.code'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-record', 'A' , array('id' => 'oadh-life-oi-record', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-investigation_type', Lang::get('decima-oadh::back-end-column.investigationType'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-investigation_type', 'B' , array('id' => 'oadh-life-oi-investigation_type', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-year', 'C' , array('id' => 'oadh-life-oi-year', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-crime', Lang::get('decima-oadh::back-end-column.crime'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-crime', 'D' , array('id' => 'oadh-life-oi-crime', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-category', Lang::get('decima-oadh::back-end-column.category'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-category', 'E' , array('id' => 'oadh-life-oi-category', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-position', Lang::get('decima-oadh::back-end-column.position'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-position', 'F' , array('id' => 'oadh-life-oi-position', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-officer_sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-officer_sex', 'G' , array('id' => 'oadh-life-oi-officer_sex', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-location_place', Lang::get('decima-oadh::back-end-column.locationPlace'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-location_place', 'H' , array('id' => 'oadh-life-oi-location_place', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-location_department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-location_department', 'I' , array('id' => 'oadh-life-oi-location_department', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-location_municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-location_municipality', 'J' , array('id' => 'oadh-life-oi-location_municipality', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-date', Lang::get('decima-oadh::back-end-column.date'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-date', 'K' , array('id' => 'oadh-life-oi-date', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-act_place', Lang::get('decima-oadh::back-end-column.actPlace'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-act_place', 'L' , array('id' => 'oadh-life-oi-act_place', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-act_department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-act_department', 'M' , array('id' => 'oadh-life-oi-act_department', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-act_municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-act_municipality', 'N' , array('id' => 'oadh-life-oi-act_municipality', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-weapon_type', Lang::get('decima-oadh::back-end-column.weaponType'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-weapon_type', 'O' , array('id' => 'oadh-life-oi-weapon_type', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-victim_quantity', Lang::get('decima-oadh::back-end-column.victimQuantity'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-victim_quantity', 'P' , array('id' => 'oadh-life-oi-victim_quantity', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-victim_sex', Lang::get('decima-oadh::back-end-column.victimSex'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-victim_sex', 'Q' , array('id' => 'oadh-life-oi-victim_sex', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-criminal_justice_process', Lang::get('decima-oadh::back-end-column.criminalJusticeProcess'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-criminal_justice_process', 'R' , array('id' => 'oadh-life-oi-criminal_justice_process', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-life-oi-status', Lang::get('decima-oadh::back-end-general.status'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-life-oi-status', 'D' , array('id' => 'oadh-life-oi-status', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->

							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-life-oi-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-life-oi-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-oi-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-oi-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-life-oi-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-oi-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-oi-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-life-oi-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-life-oi-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-life-oi-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-life-oi-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
