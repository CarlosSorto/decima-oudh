@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-pop-tp-new-action', null, array('id' => 'oadh-pop-tp-new-action')) !!}
{!! Form::hidden('oadh-pop-tp-edit-action', null, array('id' => 'oadh-pop-tp-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-pop-tp-remove-action', null, array('id' => 'oadh-pop-tp-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-pop-tp-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-pop-tp-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-pop-tp-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhPopTpOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#oadh-pop-tp-grid').getSelectedRowId('file_id'), rowData = $('#oadh-pop-tp-grid').getRowData($('#oadh-pop-tp-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-pop-tp-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-pop-tp-', 'firstPage', id);

				$('#oadh-pop-tp-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-pop-tp-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-pop-tp-btn-group-2').enableButtonGroup();

		}

		function oadhPopTpOnLoadCompleteEvent()
		{
			$('#oadh-pop-tp-temp-data').jqGrid('clearGridData');
			$('#oadh-pop-tp-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-pop-tp-btn-tooltip').tooltip();

			$('#oadh-pop-tp-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-pop-tp-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-pop-tp-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-pop-tp-grid-section').collapse('show');

				$('#oadh-pop-tp-journals-section').collapse('show');

				$('#oadh-pop-tp-filters').show();
			});

			$('#oadh-pop-tp-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-pop-tp-').focus();
			});

			$('#oadh-pop-tp-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-pop-tp-btn-refresh').click();
	    });

			$('#oadh-pop-tp-').focusout(function()
			{
				$('#oadh-pop-tp-btn-save').focus();
			});

			$('#oadh-pop-tp-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-pop-tp-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-pop-tp-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-pop-tp-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-pop-tp-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-pop-tp-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-pop-tp-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-pop-tp-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-pop-tp-btn-toolbar').disabledButtonGroup();
				$('#oadh-pop-tp-btn-group-1').enableButtonGroup();

				if($('#oadh-pop-tp-journals-section').attr('data-target-id') == '' || $('#oadh-pop-tp-journals-section').attr('data-target-id') == '#oadh-pop-tp-form-section')
				{
					$('#oadh-pop-tp-grid').trigger('reloadGrid');
					cleanJournals('oadh-pop-tp-');
				}
				else
				{

				}
			});

			$('#oadh-pop-tp-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-pop-tp-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-pop-tp-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#oadh-pop-tp-detail-btn-export-xls').click(function()
			{
				$('#oadh-pop-tp-back-detail-gridXlsButton').click();
			});

			$('#oadh-pop-tp-detail-btn-export-csv').click(function()
			{
				$('#oadh-pop-tp-back-detail-gridCsvButton').click();
			});

			$('#oadh-pop-tp-btn-export-xls').click(function()
			{
				if($('#oadh-pop-tp-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-pop-tp-gridXlsButton').click();
				}
			});

			$('#oadh-pop-tp-btn-export-csv').click(function()
			{
				if($('#oadh-pop-tp-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-pop-tp-gridCsvButton').click();
				}
			});

			$('#oadh-pop-tp-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-pop-tp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-pop-tp-grid').isRowSelected())
					{
						$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-pop-tp-modal-delete-file').modal('show');
			});

			$('#oadh-pop-tp-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-pop-tp-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/population/population-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-pop-tp-btn-toolbar', false);
						$('#oadh-pop-tp-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-pop-tp-modal-delete-prod').click();
							$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-pop-tp-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-pop-tp-grid').trigger('reloadGrid');
						$('#oadh-pop-tp-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-pop-tp-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-pop-tp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-pop-tp-grid').isRowSelected())
					{
						$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-pop-tp-modal-delete-prod').modal('show');
			});

			$('#oadh-pop-tp-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-pop-tp-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/population/population-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-pop-tp-btn-toolbar', false);
						$('#oadh-pop-tp-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-pop-tp-modal-delete-prod').click();
							$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-pop-tp-prod-data').trigger('reloadGrid');
							$('#oadh-pop-tp-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-pop-tp-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-pop-tp-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-pop-tp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-pop-tp-grid').isRowSelected())
					{
						$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-pop-tp-modal-generate').modal('show');
			});

			$('#oadh-pop-tp-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-pop-tp-grid').getRowData($('#oadh-pop-tp-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/population/population-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-pop-tp-btn-toolbar', false);
						$('#oadh-pop-tp-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-pop-tp-btn-refresh').click();
							$("#oadh-pop-tp-btn-group-2").disabledButtonGroup();
							$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-pop-tp-btn-refresh').click();
							$("#oadh-pop-tp-btn-group-2").disabledButtonGroup();
							$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-pop-tp-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-pop-tp-btn-process').click(function()
			{

				if(!$('#oadh-pop-tp-grid').isRowSelected())
				{
					$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-pop-tp-grid').getRowData($('#oadh-pop-tp-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-pop-tp-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-pop-tp-id').val(rowData.file_id);
				$('#oadh-pop-tp-name').val(rowData.file_name);
				$('#oadh-pop-tp-system-route').val(rowData.file_system_route);

				$('#oadh-pop-tp-mp-modal').modal('show');
			});

			$('#oadh-pop-tp-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-pop-tp-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-pop-tp-mp-form').formToObject('oadh-pop-tp-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/population/population-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-pop-tp-btn-toolbar', false);
						$('#oadh-pop-tp-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-pop-tp-btn-refresh').click();
							$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-pop-tp-temp-data, #oadh-pop-tp-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-pop-tp-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-pop-tp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-pop-tp-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-pop-tp-edit-action').isEmpty())
			{
				showButtonHelper('oadh-pop-tp-btn-close', 'oadh-pop-tp-btn-group-2', $('#oadh-pop-tp-edit-action').attr('data-content'));
			}

			$('#oadh-pop-tp-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-pop-tp-btn-close', 'oadh-pop-tp-btn-group-2', $('#oadh-pop-tp-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-pop-tp-delete-action').isEmpty())
			{
				showButtonHelper('oadh-pop-tp-btn-close', 'oadh-pop-tp-btn-group-2', $('#oadh-pop-tp-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-pop-tp-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-pop-tp-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-pop-tp-btn-upload', 'class' => 'btn btn-default oadh-pop-tp-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-pop-tp-btn-refresh', 'class' => 'btn btn-default oadh-pop-tp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-pop-tp-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-pop-tp-btn-process', 'class' => 'btn btn-default oadh-pop-tp-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-pop-tp-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-pop-tp-btn-delete-prod', 'class' => 'btn btn-default oadh-pop-tp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-pop-tp-btn-delete-file', 'class' => 'btn btn-default oadh-pop-tp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-pop-tp-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-pop-tp-grid'>
				{!!
				GridRender::setGridId("oadh-pop-tp-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhPopTpOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhPopTpOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-pop-tp-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-pop-tp-temp-data-tab" aria-controls="oadh-pop-tp-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-pop-tp-processed-data-tab" aria-controls="oadh-pop-tp-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-pop-tp-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-pop-tp-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-pop-tp-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/population/population-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_pop_tp_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_pop_tp_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_pop_tp_year', 'align' => 'center', 'width' => '30', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'department', 'name' => 'oadh_pop_tp_department', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'municipality', 'name' => 'oadh_pop_tp_municipality', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'sex', 'name' => 'oadh_pop_tp_sex', 'align' => 'center', 'hidden' => false, 'width' => '30'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.population'), 'index' => 'population', 'name' => 'oadh_pop_tp_population', 'align' => 'center', 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_pop_tp_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '25'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-pop-tp-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-pop-tp-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-pop-tp-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/population/population-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_pop_tp_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_pop_tp_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_pop_tp_year', 'align' => 'center', 'width' => '30', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'department', 'name' => 'oadh_pop_tp_department', 'align' => 'center', 'hidden' => false))
                ->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'municipality', 'name' => 'oadh_pop_tp_municipality', 'align' => 'center', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'sex', 'name' => 'oadh_pop_tp_sex', 'align' => 'center', 'hidden' => false, 'width' => '30'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.population'), 'index' => 'population', 'name' => 'oadh_pop_tp_population', 'align' => 'center', 'hidden' => false, 'width' => '90'))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-pop-tp-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-pop-tp-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-pop-tp-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-tp-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-pop-tp-header-rows-number', 1 , array('id' => 'oadh-pop-tp-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-pop-tp-id', null, array('id' => 'oadh-pop-tp-id')) !!}
											{!! Form::hidden('oadh-pop-tp-name', null, array('id' => 'oadh-pop-tp-name')) !!}
											{!! Form::hidden('oadh-pop-tp-system-route', null, array('id' => 'oadh-pop-tp-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-tp-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-pop-tp-last-row-number', null, array('id' => 'oadh-pop-tp-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-tp-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-pop-tp-mp-date-format', null, array('id' => 'oadh-pop-tp-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-tp-department', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-tp-department', 'A' , array('id' => 'oadh-pop-tp-department', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-tp-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-tp-municipality', 'B' , array('id' => 'oadh-pop-tp-municipality', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-tp-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-tp-sex', 'C' , array('id' => 'oadh-pop-tp-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-tp-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-tp-year', 'D' , array('id' => 'oadh-pop-tp-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-tp-population', Lang::get('decima-oadh::back-end-column.population'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-tp-population', 'E' , array('id' => 'oadh-pop-tp-population', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-pop-tp-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-pop-tp-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-pop-tp-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-pop-tp-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-pop-tp-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-pop-tp-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-pop-tp-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-pop-tp-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-pop-tp-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-pop-tp-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-pop-tp-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
