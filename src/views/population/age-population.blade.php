@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-pop-ap-new-action', null, array('id' => 'oadh-pop-ap-new-action')) !!}
{!! Form::hidden('oadh-pop-ap-edit-action', null, array('id' => 'oadh-pop-ap-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('oadh-pop-ap-remove-action', null, array('id' => 'oadh-pop-ap-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-pop-ap-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-pop-ap-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_oadh-pop-ap-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function oadhPopApOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#oadh-pop-ap-grid').getSelectedRowId('file_id'), rowData = $('#oadh-pop-ap-grid').getRowData($('#oadh-pop-ap-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#oadh-pop-ap-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('oadh-pop-ap-', 'firstPage', id);

				$('#oadh-pop-ap-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#oadh-pop-ap-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#oadh-pop-ap-btn-group-2').enableButtonGroup();

		}

		function oadhPopApOnLoadCompleteEvent()
		{
			$('#oadh-pop-ap-temp-data').jqGrid('clearGridData');
			$('#oadh-pop-ap-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.oadh-pop-ap-btn-tooltip').tooltip();

			$('#oadh-pop-ap-mp-form').jqMgVal('addFormFieldsValidations');

			$('#oadh-pop-ap-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#oadh-pop-ap-form-section').on('hidden.bs.collapse', function ()
			{
				$('#oadh-pop-ap-grid-section').collapse('show');

				$('#oadh-pop-ap-journals-section').collapse('show');

				$('#oadh-pop-ap-filters').show();
			});

			$('#oadh-pop-ap-form-section').on('shown.bs.collapse', function ()
			{
				// $('#oadh-pop-ap-').focus();
			});

			$('#oadh-pop-ap-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#oadh-pop-ap-btn-refresh').click();
	    });

			$('#oadh-pop-ap-').focusout(function()
			{
				$('#oadh-pop-ap-btn-save').focus();
			});

			$('#oadh-pop-ap-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.oadh-pop-ap-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#oadh-pop-ap-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#oadh-pop-ap-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('oadh-pop-ap-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('oadh-pop-ap-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('oadh-pop-ap-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#oadh-pop-ap-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-pop-ap-btn-toolbar').disabledButtonGroup();
				$('#oadh-pop-ap-btn-group-1').enableButtonGroup();

				if($('#oadh-pop-ap-journals-section').attr('data-target-id') == '' || $('#oadh-pop-ap-journals-section').attr('data-target-id') == '#oadh-pop-ap-form-section')
				{
					$('#oadh-pop-ap-grid').trigger('reloadGrid');
					cleanJournals('oadh-pop-ap-');
				}
				else
				{

				}
			});

			$('#oadh-pop-ap-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#oadh-pop-ap-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-pop-ap-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#oadh-pop-ap-detail-btn-export-xls').click(function()
			{
				$('#oadh-pop-ap-back-detail-gridXlsButton').click();
			});

			$('#oadh-pop-ap-detail-btn-export-csv').click(function()
			{
				$('#oadh-pop-ap-back-detail-gridCsvButton').click();
			});

			$('#oadh-pop-ap-btn-export-xls').click(function()
			{
				if($('#oadh-pop-ap-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-pop-ap-gridXlsButton').click();
				}
			});

			$('#oadh-pop-ap-btn-export-csv').click(function()
			{
				if($('#oadh-pop-ap-journals-section').attr('data-target-id') == '')
				{
					$('#oadh-pop-ap-gridCsvButton').click();
				}
			});

			$('#oadh-pop-ap-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-pop-ap-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-pop-ap-grid').isRowSelected())
					{
						$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-pop-ap-modal-delete-file').modal('show');
			});

			$('#oadh-pop-ap-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-pop-ap-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/population/population-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-pop-ap-btn-toolbar', false);
						$('#oadh-pop-ap-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-pop-ap-modal-delete-prod').click();
							$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-pop-ap-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#oadh-pop-ap-grid').trigger('reloadGrid');
						$('#oadh-pop-ap-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#oadh-pop-ap-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-pop-ap-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-pop-ap-grid').isRowSelected())
					{
						$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-pop-ap-modal-delete-prod').modal('show');
			});

			$('#oadh-pop-ap-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#oadh-pop-ap-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/population/population-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-pop-ap-btn-toolbar', false);
						$('#oadh-pop-ap-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-pop-ap-modal-delete-prod').click();
							$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#oadh-pop-ap-prod-data').trigger('reloadGrid');
							$('#oadh-pop-ap-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-pop-ap-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-pop-ap-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#oadh-pop-ap-journals-section').attr('data-target-id') == '')
				{
					if(!$('#oadh-pop-ap-grid').isRowSelected())
					{
						$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#oadh-pop-ap-modal-generate').modal('show');
			});

			$('#oadh-pop-ap-btn-modal-generate').click(function()
			{
				var rowData = $('#oadh-pop-ap-grid').getRowData($('#oadh-pop-ap-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/population/population-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-pop-ap-btn-toolbar', false);
						$('#oadh-pop-ap-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#oadh-pop-ap-btn-refresh').click();
							$("#oadh-pop-ap-btn-group-2").disabledButtonGroup();
							$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#oadh-pop-ap-btn-refresh').click();
							$("#oadh-pop-ap-btn-group-2").disabledButtonGroup();
							$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#oadh-pop-ap-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#oadh-pop-ap-btn-process').click(function()
			{

				if(!$('#oadh-pop-ap-grid').isRowSelected())
				{
					$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-pop-ap-grid').getRowData($('#oadh-pop-ap-grid').jqGrid('getGridParam', 'selrow'));

				$('#oadh-pop-ap-mp-form').jqMgVal('clearContextualClasses');
				$('#oadh-pop-ap-id').val(rowData.file_id);
				$('#oadh-pop-ap-name').val(rowData.file_name);
				$('#oadh-pop-ap-system-route').val(rowData.file_system_route);

				$('#oadh-pop-ap-mp-modal').modal('show');
			});

			$('#oadh-pop-ap-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#oadh-pop-ap-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#oadh-pop-ap-mp-form').formToObject('oadh-pop-ap-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/population/population-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'oadh-pop-ap-btn-toolbar', false);
						$('#oadh-pop-ap-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#oadh-pop-ap-btn-refresh').click();
							$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#oadh-pop-ap-temp-data, #oadh-pop-ap-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#oadh-pop-ap-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#oadh-pop-ap-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#oadh-pop-ap-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#oadh-pop-ap-edit-action').isEmpty())
			{
				showButtonHelper('oadh-pop-ap-btn-close', 'oadh-pop-ap-btn-group-2', $('#oadh-pop-ap-edit-action').attr('data-content'));
			}

			$('#oadh-pop-ap-btn-delete-helper').click(function()
		  {
				showButtonHelper('oadh-pop-ap-btn-close', 'oadh-pop-ap-btn-group-2', $('#oadh-pop-ap-delete-action').attr('data-content'));
		  });

			if(!$('#oadh-pop-ap-delete-action').isEmpty())
			{
				showButtonHelper('oadh-pop-ap-btn-close', 'oadh-pop-ap-btn-group-2', $('#oadh-pop-ap-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-pop-ap-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-pop-ap-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'oadh-pop-ap-btn-upload', 'class' => 'btn btn-default oadh-pop-ap-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-pop-ap-btn-refresh', 'class' => 'btn btn-default oadh-pop-ap-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="oadh-pop-ap-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'oadh-pop-ap-btn-process', 'class' => 'btn btn-default oadh-pop-ap-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'oadh-pop-ap-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'oadh-pop-ap-btn-delete-prod', 'class' => 'btn btn-default oadh-pop-ap-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'oadh-pop-ap-btn-delete-file', 'class' => 'btn btn-default oadh-pop-ap-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='oadh-pop-ap-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='oadh-pop-ap-grid'>
				{!!
				GridRender::setGridId("oadh-pop-ap-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'oadhPopApOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'oadhPopApOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-pop-ap-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#oadh-pop-ap-temp-data-tab" aria-controls="oadh-pop-ap-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#oadh-pop-ap-processed-data-tab" aria-controls="oadh-pop-ap-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="oadh-pop-ap-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-pop-ap-front-temp-data'>
      				{!!
      				GridRender::setGridId('oadh-pop-ap-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/population/population-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_pop_ap_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_pop_ap_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_pop_ap_year', 'align' => 'center', 'width' => '20', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'age', 'name' => 'oadh_pop_ap_age', 'align' => 'center', 'width' => '20', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.population'), 'index' => 'population', 'name' => 'oadh_pop_ap_population', 'align' => 'center', 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'status', 'name' => 'oadh_pop_ap_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="oadh-pop-ap-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='oadh-pop-ap-front-processed-grid'>
              {!!
              GridRender::setGridId('oadh-pop-ap-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/population/population-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'id', 'name' => 'oadh_pop_ap_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'file_id', 'name' => 'oadh_pop_ap_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'year', 'name' => 'oadh_pop_ap_year', 'align' => 'center', 'width' => '20', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'age', 'name' => 'oadh_pop_ap_age', 'align' => 'center', 'width' => '20', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.population'), 'index' => 'population', 'name' => 'oadh_pop_ap_population', 'align' => 'center', 'hidden' => false, 'width' => '90'))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='oadh-pop-ap-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='oadh-pop-ap-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'oadh-pop-ap-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-ap-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-pop-ap-header-rows-number', 1 , array('id' => 'oadh-pop-ap-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('oadh-pop-ap-id', null, array('id' => 'oadh-pop-ap-id')) !!}
											{!! Form::hidden('oadh-pop-ap-name', null, array('id' => 'oadh-pop-ap-name')) !!}
											{!! Form::hidden('oadh-pop-ap-system-route', null, array('id' => 'oadh-pop-ap-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-ap-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('oadh-pop-ap-last-row-number', null, array('id' => 'oadh-pop-ap-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-ap-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('oadh-pop-ap-mp-date-format', null, array('id' => 'oadh-pop-ap-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-ap-age', Lang::get('decima-oadh::back-end-column.age'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-ap-age', 'A' , array('id' => 'oadh-pop-ap-age', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-ap-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-ap-year', 'B' , array('id' => 'oadh-pop-ap-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-ap-population', Lang::get('decima-oadh::back-end-column.population'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-ap-population', 'C' , array('id' => 'oadh-pop-ap-population', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('oadh-pop-ap-status', Lang::get('decima-oadh::back-end-general.status'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('oadh-pop-ap-status', 'D' , array('id' => 'oadh-pop-ap-status', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="oadh-pop-ap-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='oadh-pop-ap-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-pop-ap-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-pop-ap-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-pop-ap-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-pop-ap-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-pop-ap-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='oadh-pop-ap-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-pop-ap-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="oadh-pop-ap-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-pop-ap-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
