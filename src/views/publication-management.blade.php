@extends('layouts.base')

@section('container')
{!! Form::hidden('oadh-pm-new-action', null, array('id' => 'oadh-pm-new-action')) !!}
{!! Form::hidden('oadh-pm-edit-action', null, array('id' => 'oadh-pm-edit-action', 'data-content' => Lang::get('decima-oadh::publication-management.editHelpText'))) !!}
{!! Form::hidden('oadh-pm-remove-action', null, array('id' => 'oadh-pm-remove-action', 'data-content' => Lang::get('decima-oadh::publication-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'oadh-pm-btn-edit-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'oadh-pm-btn-delete-helper', 'class' => 'hidden')) !!}
<style></style>

<script type='text/javascript'>

	//For grids with multiselect enabled
	// function oadhPmOnSelectRowEvent(id)
	// {
	// 	var selRowIds = $('#oadh-pm-grid').jqGrid('getGridParam', 'selarrrow'), id;
	//
	// 	if(selRowIds.length == 0)
	// 	{
	// 		$('#oadh-pm-btn-group-2').disabledButtonGroup();
	// 		cleanJournals('oadh-pm-');
	// 		// cleanFiles('oadh-pm-')
	// 	}
	// 	else if(selRowIds.length == 1)
	// 	{
	// 		$('#oadh-pm-btn-group-2').enableButtonGroup();
	//
	// 		id = $('#oadh-pm-grid').getSelectedRowId('oadh_pm_id');
	//
	// 		if($('#oadh-pm-journals').attr('data-journalized-id') != id)
	// 		{
	// 			cleanJournals('oadh-pm-');
	// 			// getElementFiles('oadh-pm-', id);
	// 			getAppJournals('oadh-pm-','firstPage', id);
	// 		}
	//
	// 	}
	// 	else if(selRowIds.length > 1)
	// 	{
	// 		$('#oadh-pm-btn-group-2').disabledButtonGroup();
	// 		$('#oadh-pm-btn-delete').removeAttr('disabled');
	// 		cleanJournals('oadh-pm-');
	// 		// cleanFiles('oadh-pm-')
	// 	}
	// }


	//For grids with multiselect disabled
	function oadhPmOnSelectRowEvent()
	{
		var id = $('#oadh-pm-grid').getSelectedRowId('oadh_pm_id');

		if($('#oadh-pm-grid-section').attr('data-id') != id)
		{
			$('#oadh-pm-grid-section').attr('data-id', id);
			getAppJournals('oadh-pm-', 'firstPage', id);
			//getElementFiles('oadh-pm-', id);
		}

		$('#oadh-pm-btn-group-2').enableButtonGroup();
	}


	$(document).ready(function()
	{
		$('.oadh-pm-btn-tooltip').tooltip();

		$('#oadh-pm-form').jqMgVal('addFormFieldsValidations');

		$('#oadh-pm-grid-section').on('hidden.bs.collapse', function ()
		{
			$($('#oadh-pm-journals-section').attr('data-target-id')).collapse('show');
		});

		$('#oadh-pm-form-section').on('shown.bs.collapse', function ()
		{
			// $('#oadh-pm-').focus();
		});

		$('#oadh-pm-').focusout(function()
		{
			$('#oadh-pm-btn-save').focus();
		});

		$('#oadh-pm-form-section').on('hidden.bs.collapse', function ()
		{
			$('#oadh-pm-grid-section').collapse('show');
		});

		$('#oadh-pm-btn-new').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			$('#oadh-pm-btn-toolbar').disabledButtonGroup();

			$('#oadh-pm-btn-group-3').enableButtonGroup();

			$('#oadh-pm-form-new-title').removeClass('hidden');

			$('#oadh-pm-journals-section').attr('data-target-id', '#oadh-pm-form-section');

			$('#oadh-pm-grid-section').collapse('hide');

			$('.decima-erp-tooltip').tooltip('hide');
		});

		$('#oadh-pm-btn-refresh').click(function()
		{
			$('.decima-erp-tooltip').tooltip('hide');

			$('#oadh-pm-btn-toolbar').disabledButtonGroup();

			$('#oadh-pm-btn-group-1').enableButtonGroup();

			if($('#oadh-pm-journals-section').attr('data-target-id') == '' || $('#oadh-pm-journals-section').attr('data-target-id') == '#oadh-pm-form-section')
			{
				$('#oadh-pm-grid').trigger('reloadGrid');

				$('#oadh-pm-grid-section').attr('data-id', '');

				cleanJournals('oadh-pm-');
				// cleanFiles('oadh-pm-');
			}
			else
			{

			}
		});

		$('#oadh-pm-btn-export-xls').click(function()
		{
			if($('#oadh-pm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-pm-gridXlsButton').click();
			}
			else
			{

			}
		});

		$('#oadh-pm-btn-export-csv').click(function()
		{
			if($('#oadh-pm-journals-section').attr('data-target-id') == '')
			{
				$('#oadh-pm-gridCsvButton').click();
			}
			else
			{

			}
		});

		$('#oadh-pm-btn-edit').click(function()
		{
			var rowData;

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-pm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-pm-grid').isRowSelected())
				{
					$('#oadh-pm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				$('#oadh-pm-btn-toolbar').disabledButtonGroup();

				$('#oadh-pm-btn-group-3').enableButtonGroup();

				$('#oadh-pm-form-edit-title').removeClass('hidden');

				rowData = $('#oadh-pm-grid').getRowData($('#oadh-pm-grid').jqGrid('getGridParam', 'selrow'));

				populateFormFields(rowData);

				$('#oadh-pm-type-label').setAutocompleteLabel(rowData.oadh_pm_type);
				$('#oadh-pm-lang-label').setAutocompleteLabel(rowData.oadh_pm_lang);

				if (rowData.oadh_pm_tags.length > 0)
				{
					var ids = rowData.oadh_pm_tags.split(',');
					$.each(ids, function (index, token)
					{
						$('#oadh-pm-tags').tokenfield('createToken', token);
					});
				}

				$('#oadh-pm-form-edit-title').removeClass('hidden');

				$('#oadh-pm-journals-section').attr('data-target-id', '#oadh-pm-form-section');

				$('#oadh-pm-grid-section').collapse('hide');
			}
			else
			{

			}
		});

		$('#oadh-pm-btn-delete').click(function()
		{
			var rowData;

			if($(this).hasAttr('disabled'))
			{
				return;
			}

			if($('#oadh-pm-journals-section').attr('data-target-id') == '')
			{
				if(!$('#oadh-pm-grid').isRowSelected())
				{
					$('#oadh-pm-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#oadh-pm-grid').getRowData($('#oadh-pm-grid').jqGrid('getGridParam', 'selrow'));

				// $('#oadh-pm-delete-message').html($('#oadh-pm-delete-message').attr('data-default-label').replace(':name', rowData.oadh_pm_name));
			}
			else
			{

			}

			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-pm-modal-delete').modal('show');
		});

		$('#oadh-pm-btn-modal-delete').click(function()
		{
			var id, url;

			if($('#oadh-pm-journals-section').attr('data-target-id') == '')
			{
				url = $('#oadh-pm-form').attr('action') + '/delete';
				//For grids with multiselect enabled
				//id = $('#oadh-pm-grid').getSelectedRowsIdCell('oadh_pm_id');
				//For grids with multiselect disabled
				id = $('#oadh-pm-grid').getSelectedRowId('oadh_pm_id');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify({'_token':$('#app-token').val(), 'id':id}),
				dataType : 'json',
				url:  url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-pm-btn-toolbar', false);
					$('#oadh-pm-modal-delete').modal('hide');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						$('#oadh-pm-btn-refresh').click();
						$('#oadh-pm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
					}
					else if(json.info)
					{
						$('#oadh-pm-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 12000);
					}

					$('#oadh-pm-modal-delete').modal('hide');
					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-pm-btn-save').click(function()
		{
			var url = $('#oadh-pm-form').attr('action'), action = 'new';

			$('.decima-erp-tooltip').tooltip('hide');

			if($('#oadh-pm-journals-section').attr('data-target-id') == '#oadh-pm-form-section')
			{
				if(!$('#oadh-pm-form').jqMgVal('isFormValid'))
				{
					return;
				}

				if($('#oadh-pm-id').isEmpty())
				{
					url = url + '/create';
				}
				else
				{
					url = url + '/update';
					action = 'edit';
				}

				data = $('#oadh-pm-form').formToObject('oadh-pm-');
			}
			else
			{

			}

			$.ajax(
			{
				type: 'POST',
				data: JSON.stringify(data),
				dataType : 'json',
				url: url,
				error: function (jqXHR, textStatus, errorThrown)
				{
					handleServerExceptions(jqXHR, 'oadh-pm-form');
				},
				beforeSend:function()
				{
					$('#app-loader').removeClass('hidden');
					disabledAll();
				},
				success:function(json)
				{
					if(json.success)
					{
						if($('#oadh-pm-journals-section').attr('data-target-id') == '#oadh-pm-form-section')
						{
							$('#oadh-pm-btn-close').click();
							$('#oadh-pm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
						}
						else
						{
							// $('#oadh-pm-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 6000);
							// $('#oadh-pm-form').showAlertAsFirstChild('alert-success', json.success, 6000)
						}
					}
					else if(json.info)
					{
						if($('#oadh-pm-journals-section').attr('data-target-id') == '#oadh-pm-form-section')
						{
							$('#oadh-pm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
						else
						{
							// $('#oadh-pm-form').showAlertAsFirstChild('alert-info', json.info, 12000);
						}
					}

					$('#app-loader').addClass('hidden');
					enableAll();
					$('.decima-erp-tooltip').tooltip('hide');
				}
			});
		});

		$('#oadh-pm-btn-close').click(function()
		{
			if($(this).hasAttr('disabled'))
			{
				return;
			}

			// oadh-pm-form-section
			if($('#oadh-pm-journals-section').attr('data-target-id') == '#oadh-pm-form-section')
			{
				$('#oadh-pm-form-new-title').addClass('hidden');
				$('#oadh-pm-form-edit-title').addClass('hidden');
				$('#oadh-pm-btn-refresh').click();
				$('#oadh-pm-form').jqMgVal('clearForm');
				$('#oadh-pm-form-section').collapse('hide');
				$('#oadh-pm-form').clearTags();

			}
			else
			{

			}

			$('#oadh-pm-btn-group-1').enableButtonGroup();
			$('#oadh-pm-btn-group-3').disabledButtonGroup();
			$('.decima-erp-tooltip').tooltip('hide');
			$('#oadh-pm-journals-section').attr('data-target-id', '')
		});

		$('#oadh-pm-file-uploader-modal').on('hidden.bs.modal', function (e)
		{
			dataFiles = $.parseJSON($("#oadh-pm-file-uploader-modal").attr('data-files'));

			if(dataFiles.length == 2)
			{
				$('#oadh-pm-image-url').val(dataFiles[1]['url']);
			}
			else if(dataFiles.length == 1)
			{
				$('#oadh-pm-image-url').val(dataFiles[0]['url']);
			}
		});

		$('#oadh-pm-btn-edit-helper').click(function()
	  {
			showButtonHelper('oadh-pm-btn-close', 'oadh-pm-btn-group-2', $('#oadh-pm-edit-action').attr('data-content'));
	  });

		$('#oadh-pm-btn-delete-helper').click(function()
	  {
			showButtonHelper('oadh-pm-btn-close', 'oadh-pm-btn-group-2', $('#oadh-pm-delete-action').attr('data-content'));
	  });

		if(!$('#oadh-pm-new-action').isEmpty())
		{
			$('#oadh-pm-btn-new').click();
		}

		if(!$('#oadh-pm-edit-action').isEmpty())
		{
			showButtonHelper('oadh-pm-btn-close', 'oadh-pm-btn-group-2', $('#oadh-pm-edit-action').attr('data-content'));
		}

		if(!$('#oadh-pm-delete-action').isEmpty())
		{
			showButtonHelper('oadh-pm-btn-close', 'oadh-pm-btn-group-2', $('#oadh-pm-delete-action').attr('data-content'));
		}

		setTimeout(function(){
			$('#oadh-pm-tags').tokenfield(
			{
				showAutocompleteOnFocus: true,
				beautify:false
			});
		});
	});
</script>

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="oadh-pm-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="oadh-pm-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-plus"></i> ' . Lang::get('toolbar.new'), array('id' => 'oadh-pm-btn-new', 'class' => 'btn btn-default oadh-pm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-oadh::publication-management.new'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'oadh-pm-btn-refresh', 'class' => 'btn btn-default oadh-pm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
				<div class="btn-group">
					{!! Form::button('<i class="fa fa-share-square-o"></i> ' . Lang::get('toolbar.export') . ' <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
					<ul class="dropdown-menu">
         		<li><a id='oadh-pm-btn-export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> {{ Lang::get('decima-accounting::journal-management.standardSpreadsheet') . ' (' . Lang::get('form.spreadsheet') . ')' }}</a></li>
         		<!-- <li><a id='oadh-pm-btn-export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li> -->
       		</ul>
				</div>
			</div>
			<div id="oadh-pm-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-edit"></i> ' . Lang::get('toolbar.edit'), array('id' => 'oadh-pm-btn-edit', 'class' => 'btn btn-default oadh-pm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::publication-management.edit'))) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('toolbar.delete'), array('id' => 'oadh-pm-btn-delete', 'class' => 'btn btn-default oadh-pm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::publication-management.delete'))) !!}
				{{-- Form::button('<i class="fa fa-upload"></i> ', array('id' => 'oadh-pm-btn-upload', 'class' => 'btn btn-default oadh-pm-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-folder' => Lang::get('decima-oadh::publication-management.folder'), 'data-original-title' => Lang::get('toolbar.uploadLongText'))) --}}
				{{-- Form::button('<i class="fa fa-files-o"></i> ', array('id' => 'oadh-pm-btn-show-files', 'class' => 'btn btn-default oadh-pm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showFilesLongText'))) --}}
				{{-- Form::button('<i class="fa fa-history"></i> ', array('id' => 'oadh-pm-btn-show-history', 'class' => 'btn btn-default oadh-pm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.showHistoryLongText'))) --}}
			</div>
			<div id="oadh-pm-btn-group-3" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-save"></i> ' . Lang::get('toolbar.save'), array('id' => 'oadh-pm-btn-save', 'class' => 'btn btn-default oadh-pm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('decima-oadh::publication-management.save'))) !!}
				{!! Form::button('<i class="fa fa-undo"></i> ' . Lang::get('toolbar.close'), array('id' => 'oadh-pm-btn-close', 'class' => 'btn btn-default oadh-pm-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => Lang::get('toolbar.closeLongText'))) !!}
			</div>
		</div>
		<div id='oadh-pm-grid-section' class='app-grid collapse in' data-id='' data-app-grid-id='oadh-pm-grid'>
			{!!
			GridRender::setGridId('oadh-pm-grid')
				->enablefilterToolbar(false, false)
				->hideXlsExporter()
  			->hideCsvExporter()
	    	->setGridOption('url',URL::to('ucaoadh/cms/publication-management/grid-data'))
	    	->setGridOption('caption', Lang::get('decima-oadh::publication-management.gridTitle'))
				->setGridOption('filename', Lang::get('decima-oadh::publication-management.gridTitle'))
				->setGridOption('height', 'auto')
				->setGridOption('multiselect', false)
				->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
	    	->setGridOption('postData',array('_token' => Session::token()))
				->setGridEvent('onSelectRow', 'oadhPmOnSelectRowEvent')
	    	->addColumn(array('index' => 'p.id', 'name' => 'oadh_pm_id', 'hidden' => true))
				->addColumn(array('label' => Lang::get('decima-oadh::publication-management.type'), 'index' => 'p.type', 'name' => 'oadh_pm_type', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::publication-management.typeGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select'))
				->addColumn(array('label' => Lang::get('form.date'), 'index' => 'p.date' ,'name' => 'oadh_pm_date', 'formatter' => 'date', 'align' => 'center'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.title'), 'index' => 'p.title' ,'name' => 'oadh_pm_title'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.description'), 'index' => 'p.descripcion' ,'name' => 'oadh_pm_description'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.author'), 'index' => 'p.author' ,'name' => 'oadh_pm_author'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.tags'), 'index' => 'p.tags' ,'name' => 'oadh_pm_tags'))
				->addColumn(array('label' => Lang::get('decima-oadh::publication-management.lang'), 'index' => 'p.lang' ,'name' => 'oadh_pm_lang', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::publication-management.langGridText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.imageUrl'), 'index' => 'p.image_url' ,'name' => 'oadh_pm_image_url'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.downloadUrl'), 'index' => 'p.download_url' ,'name' => 'oadh_pm_download_url'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.summaryUrl'), 'index' => 'p.summary_url' ,'name' => 'oadh_pm_summary_url'))
	    	->addColumn(array('label' => Lang::get('decima-oadh::publication-management.infographicUrl'), 'index' => 'p.infographic_url' ,'name' => 'oadh_pm_infographic_url'))
				->addColumn(array('label' => Lang::get('decima-oadh::publication-management.isHighlightedGrid'), 'index' => 'p.is_highlighted' ,'name' => 'oadh_pm_is_highlighted', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('form.booleanText')), 'align' => 'center' , 'stype' => 'select', 'width' => 70))
	    	->renderGrid();
			!!}
		</div>
	</div>
</div>
<div id='oadh-pm-form-section' class="row collapse">
	<div class="col-lg-12 col-md-12">
		<div class="form-container">
			{!! Form::open(array('id' => 'oadh-pm-form', 'url' => URL::to('ucaoadh/cms/publication-management'), 'role'  =>  'form', 'onsubmit' => 'return false;')) !!}
				<legend id="oadh-pm-form-new-title" class="hidden">{{ Lang::get('decima-oadh::publication-management.formNewTitle') }}</legend>
				<legend id="oadh-pm-form-edit-title" class="hidden">{{ Lang::get('decima-oadh::publication-management.formEditTitle') }}</legend>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-pm-type', Lang::get('decima-oadh::publication-management.type'), array('class' => 'control-label ')) !!}
							{!! Form::autocomplete('oadh-pm-type-label', array(array('label'=>'Boletín', 'value'=>'B'), array('label'=>'Memorias', 'value'=>'V'), array('label'=>'Informe anual', 'value'=>'F'), array('label'=>'Investigación', 'value'=>'I'), array('label'=>'Resumen', 'value'=>'R')), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-pm-type-label', 'oadh-pm-type', null, null) !!}
							{!! Form::hidden('oadh-pm-type', null, array('id'=> 'oadh-pm-type')) !!}
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-pm-date', Lang::get('form.date'), array('class' => 'control-label')) !!}
							{!! Form::date('oadh-pm-date', array('class' => 'form-control', 'data-mg-required' => '')) !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-pm-title', Lang::get('decima-oadh::publication-management.title'), array('class' => 'control-label')) !!}
							{!! Form::text('oadh-pm-title', null , array('id' => 'oadh-pm-title', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							{!! Form::hidden('oadh-pm-id', null, array('id' => 'oadh-pm-id')) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-pm-description', Lang::get('decima-oadh::publication-management.description'), array('class' => 'control-label')) !!}
							{!! Form::textareacustom('oadh-pm-description', 2, 377, array('id' => 'oadh-pm-description', 'class' => 'form-control', 'data-mg-required' => '')) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-pm-author', Lang::get('decima-oadh::publication-management.author'), array('class' => 'control-label')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-user"></i>
								</span>
								{!! Form::text('oadh-pm-author', null , array('id' => 'oadh-pm-author', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-pm-tags', Lang::get('decima-oadh::publication-management.tags'), array('class' => 'control-label')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-tags"></i>
								</span>
								{!! Form::text('oadh-pm-tags', null , array('id' => 'oadh-pm-tags', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group mg-hm clearfix">
							{!! Form::label('oadh-pm-lang', Lang::get('decima-oadh::publication-management.lang'), array('class' => 'control-label ')) !!}
							{!! Form::autocomplete('oadh-pm-lang-label', array(array('label'=>Lang::get('decima-oadh::publication-management.es'), 'value'=>'es'), array('label'=>Lang::get('decima-oadh::publication-management.en'), 'value'=>'en')), array('class' => 'form-control', 'data-mg-required' => '', 'data-id' => ''), 'oadh-pm-lang-label', 'oadh-pm-lang', null, null) !!}
							{!! Form::hidden('oadh-pm-lang', null, array('id'=> 'oadh-pm-lang')) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-pm-image-url', Lang::get('decima-oadh::publication-management.imageUrl'), array('class' => 'control-label')) !!}
							{!! Form::imageuploader('oadh-pm-image-url', array('id' => 'oadh-pm-image-url', 'class' => 'form-control','data-mg-required' => ''), $prefix, "Publicaciones", 760, '""', '[760]') !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-pm-download-url', Lang::get('decima-oadh::publication-management.downloadUrl'), array('class' => 'control-label')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-link"></i>
								</span>
								{!! Form::text('oadh-pm-download-url', null , array('id' => 'oadh-pm-download-url', 'class' => 'form-control', 'data-mg-required' => '')) !!}
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-pm-summary-url', Lang::get('decima-oadh::publication-management.summaryUrl'), array('class' => 'control-label')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-link"></i>
								</span>
								{!! Form::text('oadh-pm-summary-url', null , array('id' => 'oadh-pm-summary-url', 'class' => 'form-control')) !!}
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group mg-hm">
							{!! Form::label('oadh-pm-infographic-url', Lang::get('decima-oadh::publication-management.infographicUrl'), array('class' => 'control-label')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-link"></i>
								</span>
								{!! Form::text('oadh-pm-infographic-url', null , array('id' => 'oadh-pm-infographic-url', 'class' => 'form-control')) !!}
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group checkbox">
							<label class="control-label">
								{!! Form::checkbox('oadh-pm-is-highlighted', 'S',false, array('id' => 'oadh-pm-is-highlighted')) !!}
								{{ Lang::get('decima-oadh::publication-management.isHighlighted') }}
							</label>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@include('decima-file::file-uploader')
<div id='oadh-pm-journals-section' class="row collapse in section-block" data-target-id="">
	{!! Form::journals('oadh-pm-', $appInfo['id']) !!}
</div>
<div id='oadh-pm-modal-delete' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm oadh-pm-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p id="oadh-pm-delete-message">{{ Lang::get('form.deleteMessageConfirmation') }}</p>
				<!-- <p id="oadh-pm-delete-message" data-default-label="{{ Lang::get('decima-oadh::publication-management.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="oadh-pm-btn-modal-delete" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@parent
@stop
