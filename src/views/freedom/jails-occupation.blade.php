@extends('layouts.base')

@section('container')
{!! Form::hidden('free-jo-new-action', null, array('id' => 'free-jo-new-action')) !!}
{!! Form::hidden('free-jo-edit-action', null, array('id' => 'free-jo-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('free-jo-remove-action', null, array('id' => 'free-jo-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'free-jo-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'free-jo-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_free-jo-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeJoOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#free-jo-grid').getSelectedRowId('file_id'), rowData = $('#free-jo-grid').getRowData($('#free-jo-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#free-jo-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('free-jo-', 'firstPage', id);

				$('#free-jo-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'jo.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#free-jo-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'jo.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#free-jo-btn-group-2').enableButtonGroup();

		}

		function freeCoDetailOnSelectRowEvent()
		{
			var selRowIds = $('#free-jo-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#free-jo-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#free-jo-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#free-jo-detail-btn-group-2').disabledButtonGroup();
				$('#free-jo-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeJoOnLoadCompleteEvent()
		{
			$('#free-jo-temp-data').jqGrid('clearGridData');
			$('#free-jo-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.free-jo-btn-tooltip').tooltip();

			$('#free-jo-mp-form').jqMgVal('addFormFieldsValidations');

			$('#free-jo-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#free-jo-form-section').on('hidden.bs.collapse', function ()
			{
				$('#free-jo-grid-section').collapse('show');

				$('#free-jo-journals-section').collapse('show');

				$('#free-jo-filters').show();
			});

			$('#free-jo-form-section').on('shown.bs.collapse', function ()
			{
				// $('#free-jo-').focus();
			});

			$('#free-jo-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#free-jo-btn-refresh').click();
	    });

			$('#free-jo-').focusout(function()
			{
				$('#free-jo-btn-save').focus();
			});

			$('#free-jo-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.free-jo-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#free-jo-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#free-jo-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('free-jo-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('free-jo-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('free-jo-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#free-jo-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-jo-btn-toolbar').disabledButtonGroup();
				$('#free-jo-btn-group-1').enableButtonGroup();

				if($('#free-jo-journals-section').attr('data-target-id') == '' || $('#free-jo-journals-section').attr('data-target-id') == '#free-jo-form-section')
				{
					$('#free-jo-grid').trigger('reloadGrid');
					cleanJournals('free-jo-');
				}
				else
				{

				}
			});

			$('#free-jo-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#free-jo-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#free-jo-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#free-jo-detail-btn-export-xls').click(function()
			{
				$('#free-jo-back-detail-gridXlsButton').click();
			});

			$('#free-jo-detail-btn-export-csv').click(function()
			{
				$('#free-jo-back-detail-gridCsvButton').click();
			});

			$('#free-jo-btn-export-xls').click(function()
			{
				if($('#free-jo-journals-section').attr('data-target-id') == '')
				{
					$('#free-jo-gridXlsButton').click();
				}
			});

			$('#free-jo-btn-export-csv').click(function()
			{
				if($('#free-jo-journals-section').attr('data-target-id') == '')
				{
					$('#free-jo-gridCsvButton').click();
				}
			});

			$('#free-jo-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-jo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-jo-grid').isRowSelected())
					{
						$('#free-jo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-jo-modal-delete-file').modal('show');
			});

			$('#free-jo-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-jo-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-jo-btn-toolbar', false);
						$('#free-jo-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-jo-modal-delete-prod').click();
							$('#free-jo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-jo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-jo-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#free-jo-grid').trigger('reloadGrid');
						$('#free-jo-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#free-jo-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-jo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-jo-grid').isRowSelected())
					{
						$('#free-jo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-jo-modal-delete-prod').modal('show');
			});

			$('#free-jo-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-jo-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-jo-btn-toolbar', false);
						$('#free-jo-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-jo-modal-delete-prod').click();
							$('#free-jo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#free-jo-prod-data').trigger('reloadGrid');
							$('#free-jo-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-jo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-jo-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-jo-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-jo-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-jo-grid').isRowSelected())
					{
						$('#free-jo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-jo-modal-generate').modal('show');
			});

			$('#free-jo-btn-modal-generate').click(function()
			{
				var rowData = $('#free-jo-grid').getRowData($('#free-jo-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-jo-btn-toolbar', false);
						$('#free-jo-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-jo-btn-refresh').click();
							$("#free-jo-btn-group-2").disabledButtonGroup();
							$('#free-jo-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-jo-btn-refresh').click();
							$("#free-jo-btn-group-2").disabledButtonGroup();
							$('#free-jo-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-jo-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-jo-btn-process').click(function()
			{

				if(!$('#free-jo-grid').isRowSelected())
				{
					$('#free-jo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#free-jo-grid').getRowData($('#free-jo-grid').jqGrid('getGridParam', 'selrow'));

				$('#free-jo-mp-form').jqMgVal('clearContextualClasses');
				$('#free-jo-id').val(rowData.file_id);
				$('#free-jo-name').val(rowData.file_name);
				$('#free-jo-system-route').val(rowData.file_system_route);

				$('#free-jo-mp-modal').modal('show');
			});

			$('#free-jo-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#free-jo-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#free-jo-mp-form').formToObject('free-jo-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-jo-btn-toolbar', false);
						$('#free-jo-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#free-jo-btn-refresh').click();
							$('#free-jo-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#free-jo-temp-data, #free-jo-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'jo.file_id','op':'eq','data':'" + $('#free-jo-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-jo-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#free-jo-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#free-jo-edit-action').isEmpty())
			{
				showButtonHelper('free-jo-btn-close', 'free-jo-btn-group-2', $('#free-jo-edit-action').attr('data-content'));
			}

			$('#free-jo-btn-delete-helper').click(function()
		  {
				showButtonHelper('free-jo-btn-close', 'free-jo-btn-group-2', $('#free-jo-delete-action').attr('data-content'));
		  });

			if(!$('#free-jo-delete-action').isEmpty())
			{
				showButtonHelper('free-jo-btn-close', 'free-jo-btn-group-2', $('#free-jo-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="free-jo-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="free-jo-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'free-jo-btn-upload', 'class' => 'btn btn-default free-jo-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'free-jo-btn-refresh', 'class' => 'btn btn-default free-jo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="free-jo-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'free-jo-btn-process', 'class' => 'btn btn-default free-jo-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'free-jo-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'free-jo-btn-delete-prod', 'class' => 'btn btn-default free-jo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'free-jo-btn-delete-file', 'class' => 'btn btn-default free-jo-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='free-jo-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='free-jo-grid'>
				{!!
				GridRender::setGridId("free-jo-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeJoOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeJoOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#free-jo-temp-data-tab" aria-controls="free-jo-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#free-jo-processed-data-tab" aria-controls="free-jo-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="free-jo-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-jo-front-temp-data'>
      				{!!
      				GridRender::setGridId('free-jo-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'jo.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'jo.id', 'name' => 'oadh_jo_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'jo.file_id', 'name' => 'oadh_jo_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'jo.department', 'name' => 'oadh_jo_departament', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.station'), 'index' => 'jo.station', 'name' => 'oadh_jo_station', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.unit'), 'index' => 'jo.unit', 'name' => 'oadh_jo_unit', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'jo.year', 'name' => 'oadh_jo_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.capacity'), 'index' => 'jo.capacity', 'name' => 'oadh_jo_capacity', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.manPopulation'), 'index' => 'jo.man_population', 'name' => 'oadh_jo_man_population', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.womenPopulation'), 'index' => 'jo.women_population', 'name' => 'oadh_jo_women_population', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'jo.status', 'name' => 'oadh_jo_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="free-jo-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-jo-front-processed-grid'>
              {!!
              GridRender::setGridId('free-jo-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'jo.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'jo.id', 'name' => 'oadh_jo_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'jo.file_id', 'name' => 'oadh_jo_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'jo.department', 'name' => 'oadh_jo_departament', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.station'), 'index' => 'jo.station', 'name' => 'oadh_jo_station', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.unit'), 'index' => 'jo.unit', 'name' => 'oadh_jo_unit', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'jo.year', 'name' => 'oadh_jo_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.capacity'), 'index' => 'jo.capacity', 'name' => 'oadh_jo_capacity', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.manPopulation'), 'index' => 'jo.man_population', 'name' => 'oadh_jo_man_population', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.womenPopulation'), 'index' => 'jo.women_population', 'name' => 'oadh_jo_women_population', 'align' => 'center', 'width' => 15, 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='free-jo-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='free-jo-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'free-jo-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-jo-header-rows-number', 1 , array('id' => 'free-jo-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('free-jo-id', null, array('id' => 'free-jo-id')) !!}
											{!! Form::hidden('free-jo-name', null, array('id' => 'free-jo-name')) !!}
											{!! Form::hidden('free-jo-system-route', null, array('id' => 'free-jo-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-jo-last-row-number', null, array('id' => 'free-jo-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('free-jo-mp-date-format', null, array('id' => 'free-jo-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-departament', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-jo-departament', 'A' , array('id' => 'free-jo-departament', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-station', Lang::get('decima-oadh::back-end-column.station'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-jo-station', 'B' , array('id' => 'free-jo-station', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-unit', Lang::get('decima-oadh::back-end-column.unit'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-jo-unit', 'C' , array('id' => 'free-jo-unit', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-jo-year', 'D' , array('id' => 'free-jo-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-capacity', Lang::get('decima-oadh::back-end-column.capacity'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-jo-capacity', 'E' , array('id' => 'free-jo-capacity', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-man-population', Lang::get('decima-oadh::back-end-column.manPopulation'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-jo-man-population', 'F' , array('id' => 'free-jo-man-population', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-jo-women-population', Lang::get('decima-oadh::back-end-column.womenPopulation'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-jo-women-population', 'G' , array('id' => 'free-jo-women-population', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="free-jo-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='free-jo-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-jo-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-jo-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-jo-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-jo-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-jo-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-jo-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-jo-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="free-jo-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-jo-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
