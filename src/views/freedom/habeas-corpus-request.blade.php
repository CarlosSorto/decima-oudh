@extends('layouts.base')

@section('container')
{!! Form::hidden('free-hcr-new-action', null, array('id' => 'free-hcr-new-action')) !!}
{!! Form::hidden('free-hcr-edit-action', null, array('id' => 'free-hcr-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('free-hcr-remove-action', null, array('id' => 'free-hcr-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'free-hcr-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'free-hcr-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_free-hcr-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeHcrOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#free-hcr-grid').getSelectedRowId('file_id'), rowData = $('#free-hcr-grid').getRowData($('#free-hcr-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#free-hcr-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('free-hcr-', 'firstPage', id);

				$('#free-hcr-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'hcr.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#free-hcr-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'hcr.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#free-hcr-btn-group-2').enableButtonGroup();

		}

		function freeHcrDetailOnSelectRowEvent()
		{
			var selRowIds = $('#free-hcr-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#free-hcr-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#free-hcr-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#free-hcr-detail-btn-group-2').disabledButtonGroup();
				$('#free-hcr-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeHcrOnLoadCompleteEvent()
		{
			$('#free-hcr-temp-data').jqGrid('clearGridData');
			$('#free-hcr-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.free-hcr-btn-tooltip').tooltip();

			$('#free-hcr-mp-form').jqMgVal('addFormFieldsValidations');

			$('#free-hcr-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#free-hcr-form-section').on('hidden.bs.collapse', function ()
			{
				$('#free-hcr-grid-section').collapse('show');

				$('#free-hcr-journals-section').collapse('show');

				$('#free-hcr-filters').show();
			});

			$('#free-hcr-form-section').on('shown.bs.collapse', function ()
			{
				// $('#free-hcr-').focus();
			});

			$('#free-hcr-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#free-hcr-btn-refresh').click();
	    });

			$('#free-hcr-').focusout(function()
			{
				$('#free-hcr-btn-save').focus();
			});

			$('#free-hcr-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.free-hcr-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#free-hcr-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#free-hcr-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('free-hcr-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('free-hcr-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('free-hcr-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#free-hcr-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-hcr-btn-toolbar').disabledButtonGroup();
				$('#free-hcr-btn-group-1').enableButtonGroup();

				if($('#free-hcr-journals-section').attr('data-target-id') == '' || $('#free-hcr-journals-section').attr('data-target-id') == '#free-hcr-form-section')
				{
					$('#free-hcr-grid').trigger('reloadGrid');
					cleanJournals('free-hcr-');
				}
				else
				{

				}
			});

			$('#free-hcr-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#free-hcr-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#free-hcr-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#free-hcr-detail-btn-export-xls').click(function()
			{
				$('#free-hcr-back-detail-gridXlsButton').click();
			});

			$('#free-hcr-detail-btn-export-csv').click(function()
			{
				$('#free-hcr-back-detail-gridCsvButton').click();
			});

			$('#free-hcr-btn-export-xls').click(function()
			{
				if($('#free-hcr-journals-section').attr('data-target-id') == '')
				{
					$('#free-hcr-gridXlsButton').click();
				}
			});

			$('#free-hcr-btn-export-csv').click(function()
			{
				if($('#free-hcr-journals-section').attr('data-target-id') == '')
				{
					$('#free-hcr-gridCsvButton').click();
				}
			});

			$('#free-hcr-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-hcr-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-hcr-grid').isRowSelected())
					{
						$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-hcr-modal-delete-file').modal('show');
			});

			$('#free-hcr-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-hcr-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-hcr-btn-toolbar', false);
						$('#free-hcr-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-hcr-modal-delete-prod').click();
							$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-hcr-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#free-hcr-grid').trigger('reloadGrid');
						$('#free-hcr-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#free-hcr-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-hcr-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-hcr-grid').isRowSelected())
					{
						$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-hcr-modal-delete-prod').modal('show');
			});

			$('#free-hcr-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-hcr-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-hcr-btn-toolbar', false);
						$('#free-hcr-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-hcr-modal-delete-prod').click();
							$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#free-hcr-prod-data').trigger('reloadGrid');
							$('#free-hcr-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-hcr-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-hcr-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-hcr-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-hcr-grid').isRowSelected())
					{
						$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-hcr-modal-generate').modal('show');
			});

			$('#free-hcr-btn-modal-generate').click(function()
			{
				var rowData = $('#free-hcr-grid').getRowData($('#free-hcr-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-hcr-btn-toolbar', false);
						$('#free-hcr-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-hcr-btn-refresh').click();
							$("#free-hcr-btn-group-2").disabledButtonGroup();
							$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-hcr-btn-refresh').click();
							$("#free-hcr-btn-group-2").disabledButtonGroup();
							$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-hcr-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-hcr-btn-process').click(function()
			{

				if(!$('#free-hcr-grid').isRowSelected())
				{
					$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#free-hcr-grid').getRowData($('#free-hcr-grid').jqGrid('getGridParam', 'selrow'));

				$('#free-hcr-mp-form').jqMgVal('clearContextualClasses');
				$('#free-hcr-id').val(rowData.file_id);
				$('#free-hcr-name').val(rowData.file_name);
				$('#free-hcr-system-route').val(rowData.file_system_route);

				$('#free-hcr-mp-modal').modal('show');
			});

			$('#free-hcr-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#free-hcr-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#free-hcr-mp-form').formToObject('free-hcr-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-hcr-btn-toolbar', false);
						$('#free-hcr-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#free-hcr-btn-refresh').click();
							$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#free-hcr-temp-data, #free-hcr-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'hcr.file_id','op':'eq','data':'" + $('#free-hcr-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-hcr-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#free-hcr-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#free-hcr-edit-action').isEmpty())
			{
				showButtonHelper('free-hcr-btn-close', 'free-hcr-btn-group-2', $('#free-hcr-edit-action').attr('data-content'));
			}

			$('#free-hcr-btn-delete-helper').click(function()
		  {
				showButtonHelper('free-hcr-btn-close', 'free-hcr-btn-group-2', $('#free-hcr-delete-action').attr('data-content'));
		  });

			if(!$('#free-hcr-delete-action').isEmpty())
			{
				showButtonHelper('free-hcr-btn-close', 'free-hcr-btn-group-2', $('#free-hcr-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="free-hcr-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="free-hcr-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'free-hcr-btn-upload', 'class' => 'btn btn-default free-hcr-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'free-hcr-btn-refresh', 'class' => 'btn btn-default free-hcr-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="free-hcr-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'free-hcr-btn-process', 'class' => 'btn btn-default free-hcr-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'free-hcr-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'free-hcr-btn-delete-prod', 'class' => 'btn btn-default free-hcr-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'free-hcr-btn-delete-file', 'class' => 'btn btn-default free-hcr-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='free-hcr-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='free-hcr-grid'>
				{!!
				GridRender::setGridId("free-hcr-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeHcrOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeHcrOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#free-hcr-temp-data-tab" aria-controls="free-hcr-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#free-hcr-processed-data-tab" aria-controls="free-hcr-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="free-hcr-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-hcr-front-temp-data'>
      				{!!
      				GridRender::setGridId('free-hcr-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'hcr.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'hcr.id', 'name' => 'oadh_hcr_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'hcr.file_id', 'name' => 'oadh_hcr_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'hcr.year', 'name' => 'oadh_hcr_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'hcr.sex', 'name' => 'oadh_hcr_sex', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.court'), 'index' => 'hcr.court', 'name' => 'oadh_hcr_court', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'hcr.status', 'name' => 'oadh_hcr_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="free-hcr-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-hcr-front-processed-grid'>
              {!!
              GridRender::setGridId('free-hcr-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'hcr.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'hcr.id', 'name' => 'oadh_hcr_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'hcr.file_id', 'name' => 'oadh_hcr_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'hcr.year', 'name' => 'oadh_hcr_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'hcr.sex', 'name' => 'oadh_hcr_sex', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.court'), 'index' => 'hcr.court', 'name' => 'oadh_hcr_court', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '100'))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='free-hcr-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='free-hcr-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'free-hcr-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-hcr-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-hcr-header-rows-number', 1 , array('id' => 'free-hcr-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('free-hcr-id', null, array('id' => 'free-hcr-id')) !!}
											{!! Form::hidden('free-hcr-name', null, array('id' => 'free-hcr-name')) !!}
											{!! Form::hidden('free-hcr-system-route', null, array('id' => 'free-hcr-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-hcr-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-hcr-last-row-number', null, array('id' => 'free-hcr-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-hcr-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('free-hcr-mp-date-format', null, array('id' => 'free-hcr-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-hcr-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-hcr-year', 'A' , array('id' => 'free-hcr-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-hcr-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-hcr-sex', 'B' , array('id' => 'free-hcr-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-hcr-court', Lang::get('decima-oadh::back-end-column.court'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-hcr-court', 'C' , array('id' => 'free-hcr-court', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-hcr-status', Lang::get('decima-oadh::back-end-general.status'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-hcr-status', 'D' , array('id' => 'free-hcr-status', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="free-hcr-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='free-hcr-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-hcr-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-hcr-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-hcr-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-hcr-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFileMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-hcr-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-hcr-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-hcr-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="free-hcr-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-hcr-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
