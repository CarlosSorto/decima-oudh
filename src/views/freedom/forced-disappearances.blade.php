@extends('layouts.base')

@section('container')
{!! Form::hidden('free-fd-new-action', null, array('id' => 'free-fd-new-action')) !!}
{!! Form::hidden('free-fd-edit-action', null, array('id' => 'free-fd-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('free-fd-remove-action', null, array('id' => 'free-fd-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'free-fd-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'free-fd-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_free-fd-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeFdOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#free-fd-grid').getSelectedRowId('file_id'), rowData = $('#free-fd-grid').getRowData($('#free-fd-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#free-fd-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('free-fd-', 'firstPage', id);

				$('#free-fd-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'fd.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#free-fd-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'fd.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#free-fd-btn-group-2').enableButtonGroup();

		}

		function freeCoDetailOnSelectRowEvent()
		{
			var selRowIds = $('#free-fd-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#free-fd-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#free-fd-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#free-fd-detail-btn-group-2').disabledButtonGroup();
				$('#free-fd-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeFdOnLoadCompleteEvent()
		{
			$('#free-fd-temp-data').jqGrid('clearGridData');
			$('#free-fd-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.free-fd-btn-tooltip').tooltip();

			$('#free-fd-mp-form').jqMgVal('addFormFieldsValidations');

			$('#free-fd-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#free-fd-form-section').on('hidden.bs.collapse', function ()
			{
				$('#free-fd-grid-section').collapse('show');

				$('#free-fd-journals-section').collapse('show');

				$('#free-fd-filters').show();
			});

			$('#free-fd-form-section').on('shown.bs.collapse', function ()
			{
				// $('#free-fd-').focus();
			});

			$('#free-fd-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#free-fd-btn-refresh').click();
	    });

			$('#free-fd-').focusout(function()
			{
				$('#free-fd-btn-save').focus();
			});

			$('#free-fd-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.free-fd-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#free-fd-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#free-fd-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('free-fd-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('free-fd-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('free-fd-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#free-fd-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-fd-btn-toolbar').disabledButtonGroup();
				$('#free-fd-btn-group-1').enableButtonGroup();

				if($('#free-fd-journals-section').attr('data-target-id') == '' || $('#free-fd-journals-section').attr('data-target-id') == '#free-fd-form-section')
				{
					$('#free-fd-grid').trigger('reloadGrid');
					cleanJournals('free-fd-');
				}
				else
				{

				}
			});

			$('#free-fd-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#free-fd-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#free-fd-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#free-fd-detail-btn-export-xls').click(function()
			{
				$('#free-fd-back-detail-gridXlsButton').click();
			});

			$('#free-fd-detail-btn-export-csv').click(function()
			{
				$('#free-fd-back-detail-gridCsvButton').click();
			});

			$('#free-fd-btn-export-xls').click(function()
			{
				if($('#free-fd-journals-section').attr('data-target-id') == '')
				{
					$('#free-fd-gridXlsButton').click();
				}
			});

			$('#free-fd-btn-export-csv').click(function()
			{
				if($('#free-fd-journals-section').attr('data-target-id') == '')
				{
					$('#free-fd-gridCsvButton').click();
				}
			});

			$('#free-fd-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-fd-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-fd-grid').isRowSelected())
					{
						$('#free-fd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-fd-modal-delete-file').modal('show');
			});

			$('#free-fd-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-fd-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-fd-btn-toolbar', false);
						$('#free-fd-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-fd-modal-delete-prod').click();
							$('#free-fd-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-fd-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-fd-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#free-fd-grid').trigger('reloadGrid');
						$('#free-fd-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#free-fd-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-fd-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-fd-grid').isRowSelected())
					{
						$('#free-fd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-fd-modal-delete-prod').modal('show');
			});

			$('#free-fd-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-fd-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-fd-btn-toolbar', false);
						$('#free-fd-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-fd-modal-delete-prod').click();
							$('#free-fd-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#free-fd-prod-data').trigger('reloadGrid');
							$('#free-fd-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-fd-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-fd-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-fd-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-fd-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-fd-grid').isRowSelected())
					{
						$('#free-fd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-fd-modal-generate').modal('show');
			});

			$('#free-fd-btn-modal-generate').click(function()
			{
				var rowData = $('#free-fd-grid').getRowData($('#free-fd-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-fd-btn-toolbar', false);
						$('#free-fd-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-fd-btn-refresh').click();
							$("#free-fd-btn-group-2").disabledButtonGroup();
							$('#free-fd-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-fd-btn-refresh').click();
							$("#free-fd-btn-group-2").disabledButtonGroup();
							$('#free-fd-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-fd-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-fd-btn-process').click(function()
			{

				if(!$('#free-fd-grid').isRowSelected())
				{
					$('#free-fd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#free-fd-grid').getRowData($('#free-fd-grid').jqGrid('getGridParam', 'selrow'));

				$('#free-fd-mp-form').jqMgVal('clearContextualClasses');
				$('#free-fd-id').val(rowData.file_id);
				$('#free-fd-name').val(rowData.file_name);
				$('#free-fd-system-route').val(rowData.file_system_route);

				$('#free-fd-mp-modal').modal('show');
			});

			$('#free-fd-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#free-fd-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#free-fd-mp-form').formToObject('free-fd-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-fd-btn-toolbar', false);
						$('#free-fd-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#free-fd-btn-refresh').click();
							$('#free-fd-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#free-fd-temp-data, #free-fd-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'fd.file_id','op':'eq','data':'" + $('#free-fd-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-fd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#free-fd-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#free-fd-edit-action').isEmpty())
			{
				showButtonHelper('free-fd-btn-close', 'free-fd-btn-group-2', $('#free-fd-edit-action').attr('data-content'));
			}

			$('#free-fd-btn-delete-helper').click(function()
		  {
				showButtonHelper('free-fd-btn-close', 'free-fd-btn-group-2', $('#free-fd-delete-action').attr('data-content'));
		  });

			if(!$('#free-fd-delete-action').isEmpty())
			{
				showButtonHelper('free-fd-btn-close', 'free-fd-btn-group-2', $('#free-fd-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="free-fd-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="free-fd-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'free-fd-btn-upload', 'class' => 'btn btn-default free-fd-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'free-fd-btn-refresh', 'class' => 'btn btn-default free-fd-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="free-fd-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'free-fd-btn-process', 'class' => 'btn btn-default free-fd-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'free-fd-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'free-fd-btn-delete-prod', 'class' => 'btn btn-default free-fd-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'free-fd-btn-delete-file', 'class' => 'btn btn-default free-fd-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='free-fd-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='free-fd-grid'>
				{!!
				GridRender::setGridId("free-fd-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeFdOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeFdOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#free-fd-temp-data-tab" aria-controls="free-fd-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#free-fd-processed-data-tab" aria-controls="free-fd-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="free-fd-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-fd-front-temp-data'>
      				{!!
      				GridRender::setGridId('free-fd-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'fd.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'fd.id', 'name' => 'oadh_fd_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'fd.file_id', 'name' => 'oadh_fd_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'fd.year', 'name' => 'oadh_fd_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'fd.gender', 'name' => 'oadh_fd_gender', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'fd.department', 'name' => 'oadh_fd_departament', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'fd.municipality', 'name' => 'oadh_fd_municipality', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.profession'), 'index' => 'fd.profession', 'name' => 'oadh_fd_profession', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'fd.crime', 'name' => 'oadh_fd_crime', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'fd.status', 'name' => 'oadh_fd_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="free-fd-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-fd-front-processed-grid'>
              {!!
              GridRender::setGridId('free-fd-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'fd.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'fd.id', 'name' => 'oadh_fd_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'fd.file_id', 'name' => 'oadh_fd_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'fd.year', 'name' => 'oadh_fd_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'fd.gender', 'name' => 'oadh_fd_gender', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'fd.department', 'name' => 'oadh_fd_departament', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'fd.municipality', 'name' => 'oadh_fd_municipality', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.profession'), 'index' => 'fd.profession', 'name' => 'oadh_fd_profession', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.crime'), 'index' => 'fd.crime', 'name' => 'oadh_fd_crime', 'align' => 'center', 'width' => 15, 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='free-fd-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='free-fd-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'free-fd-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-fd-header-rows-number', 1 , array('id' => 'free-fd-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('free-fd-id', null, array('id' => 'free-fd-id')) !!}
											{!! Form::hidden('free-fd-name', null, array('id' => 'free-fd-name')) !!}
											{!! Form::hidden('free-fd-system-route', null, array('id' => 'free-fd-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-fd-last-row-number', null, array('id' => 'free-fd-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('free-fd-mp-date-format', null, array('id' => 'free-fd-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-fd-year', 'A' , array('id' => 'free-fd-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-gender', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-fd-gender', 'B' , array('id' => 'free-fd-gender', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-departament', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-fd-departament', 'C' , array('id' => 'free-fd-departament', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-fd-municipality', 'D' , array('id' => 'free-fd-municipality', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-profession', Lang::get('decima-oadh::back-end-column.profession'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-fd-profession', 'E' , array('id' => 'free-fd-profession', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-fd-crime', Lang::get('decima-oadh::back-end-column.crime'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-fd-crime', 'F' , array('id' => 'free-fd-crime', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="free-fd-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='free-fd-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-fd-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-fd-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-fd-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-fd-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-fd-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-fd-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-fd-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="free-fd-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-fd-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
