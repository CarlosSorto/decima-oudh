@extends('layouts.base')

@section('container')
{!! Form::hidden('free-cd-new-action', null, array('id' => 'free-cd-new-action')) !!}
{!! Form::hidden('free-cd-edit-action', null, array('id' => 'free-cd-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('free-cd-remove-action', null, array('id' => 'free-cd-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'free-cd-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'free-cd-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_free-cd-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeCdOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#free-cd-grid').getSelectedRowId('file_id'), rowData = $('#free-cd-grid').getRowData($('#free-cd-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#free-cd-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppCdurnals('free-cd-', 'firstPage', id);

				$('#free-cd-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'cd.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#free-cd-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'cd.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#free-cd-btn-group-2').enableButtonGroup();

		}

		function freeCoDetailOnSelectRowEvent()
		{
			var selRowIds = $('#free-cd-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#free-cd-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#free-cd-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#free-cd-detail-btn-group-2').disabledButtonGroup();
				$('#free-cd-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeCdOnLoadCompleteEvent()
		{
			$('#free-cd-temp-data').jqGrid('clearGridData');
			$('#free-cd-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.free-cd-btn-tooltip').tooltip();

			$('#free-cd-mp-form').jqMgVal('addFormFieldsValidations');

			$('#free-cd-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#free-cd-form-section').on('hidden.bs.collapse', function ()
			{
				$('#free-cd-grid-section').collapse('show');

				$('#free-cd-journals-section').collapse('show');

				$('#free-cd-filters').show();
			});

			$('#free-cd-form-section').on('shown.bs.collapse', function ()
			{
				// $('#free-cd-').focus();
			});

			$('#free-cd-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#free-cd-btn-refresh').click();
	    });

			$('#free-cd-').focusout(function()
			{
				$('#free-cd-btn-save').focus();
			});

			$('#free-cd-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.free-cd-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#free-cd-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#free-cd-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('free-cd-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('free-cd-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('free-cd-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#free-cd-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-cd-btn-toolbar').disabledButtonGroup();
				$('#free-cd-btn-group-1').enableButtonGroup();

				if($('#free-cd-journals-section').attr('data-target-id') == '' || $('#free-cd-journals-section').attr('data-target-id') == '#free-cd-form-section')
				{
					$('#free-cd-grid').trigger('reloadGrid');
					cleanJournals('free-cd-');
				}
				else
				{

				}
			});

			$('#free-cd-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#free-cd-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#free-cd-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#free-cd-detail-btn-export-xls').click(function()
			{
				$('#free-cd-back-detail-gridXlsButton').click();
			});

			$('#free-cd-detail-btn-export-csv').click(function()
			{
				$('#free-cd-back-detail-gridCsvButton').click();
			});

			$('#free-cd-btn-export-xls').click(function()
			{
				if($('#free-cd-journals-section').attr('data-target-id') == '')
				{
					$('#free-cd-gridXlsButton').click();
				}
			});

			$('#free-cd-btn-export-csv').click(function()
			{
				if($('#free-cd-journals-section').attr('data-target-id') == '')
				{
					$('#free-cd-gridCsvButton').click();
				}
			});

			$('#free-cd-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-cd-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-cd-grid').isRowSelected())
					{
						$('#free-cd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-cd-modal-delete-file').modal('show');
			});

			$('#free-cd-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-cd-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-cd-btn-toolbar', false);
						$('#free-cd-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-cd-modal-delete-prod').click();
							$('#free-cd-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-cd-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-cd-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#free-cd-grid').trigger('reloadGrid');
						$('#free-cd-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#free-cd-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-cd-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-cd-grid').isRowSelected())
					{
						$('#free-cd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-cd-modal-delete-prod').modal('show');
			});

			$('#free-cd-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-cd-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-cd-btn-toolbar', false);
						$('#free-cd-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-cd-modal-delete-prod').click();
							$('#free-cd-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#free-cd-prod-data').trigger('reloadGrid');
							$('#free-cd-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-cd-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-cd-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-cd-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-cd-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-cd-grid').isRowSelected())
					{
						$('#free-cd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-cd-modal-generate').modal('show');
			});

			$('#free-cd-btn-modal-generate').click(function()
			{
				var rowData = $('#free-cd-grid').getRowData($('#free-cd-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-cd-btn-toolbar', false);
						$('#free-cd-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-cd-btn-refresh').click();
							$("#free-cd-btn-group-2").disabledButtonGroup();
							$('#free-cd-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-cd-btn-refresh').click();
							$("#free-cd-btn-group-2").disabledButtonGroup();
							$('#free-cd-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-cd-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-cd-btn-process').click(function()
			{

				if(!$('#free-cd-grid').isRowSelected())
				{
					$('#free-cd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#free-cd-grid').getRowData($('#free-cd-grid').jqGrid('getGridParam', 'selrow'));

				$('#free-cd-mp-form').jqMgVal('clearContextualClasses');
				$('#free-cd-id').val(rowData.file_id);
				$('#free-cd-name').val(rowData.file_name);
				$('#free-cd-system-route').val(rowData.file_system_route);

				$('#free-cd-mp-modal').modal('show');
			});

			$('#free-cd-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#free-cd-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#free-cd-mp-form').formToObject('free-cd-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-cd-btn-toolbar', false);
						$('#free-cd-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#free-cd-btn-refresh').click();
							$('#free-cd-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#free-cd-temp-data, #free-cd-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'cd.file_id','op':'eq','data':'" + $('#free-cd-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-cd-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#free-cd-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#free-cd-edit-action').isEmpty())
			{
				showButtonHelper('free-cd-btn-close', 'free-cd-btn-group-2', $('#free-cd-edit-action').attr('data-content'));
			}

			$('#free-cd-btn-delete-helper').click(function()
		  {
				showButtonHelper('free-cd-btn-close', 'free-cd-btn-group-2', $('#free-cd-delete-action').attr('data-content'));
		  });

			if(!$('#free-cd-delete-action').isEmpty())
			{
				showButtonHelper('free-cd-btn-close', 'free-cd-btn-group-2', $('#free-cd-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="free-cd-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="free-cd-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'free-cd-btn-upload', 'class' => 'btn btn-default free-cd-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'free-cd-btn-refresh', 'class' => 'btn btn-default free-cd-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="free-cd-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'free-cd-btn-process', 'class' => 'btn btn-default free-cd-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'free-cd-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'free-cd-btn-delete-prod', 'class' => 'btn btn-default free-cd-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'free-cd-btn-delete-file', 'class' => 'btn btn-default free-cd-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='free-cd-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='free-cd-grid'>
				{!!
				GridRender::setGridId("free-cd-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeCdOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeCdOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#free-cd-temp-data-tab" aria-controls="free-cd-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#free-cd-processed-data-tab" aria-controls="free-cd-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="free-cd-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-cd-front-temp-data'>
      				{!!
      				GridRender::setGridId('free-cd-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'cd.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'cd.id', 'name' => 'oadh_cd_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'cd.file_id', 'name' => 'oadh_cd_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.chronicDisease'), 'index' => 'cd.chronic_disease', 'name' => 'oadh_cd_chronic_disease', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'cd.sex', 'name' => 'oadh_cd_sex', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'cd.age', 'name' => 'oadh_cd_age', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.jail'), 'index' => 'cd.jail', 'name' => 'oadh_cd_jail', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'cd.year', 'name' => 'oadh_cd_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'cd.status', 'name' => 'oadh_cd_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="free-cd-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-cd-front-processed-grid'>
              {!!
              GridRender::setGridId('free-cd-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'cd.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'cd.id', 'name' => 'oadh_cd_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'cd.file_id', 'name' => 'oadh_cd_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.chronicDisease'), 'index' => 'cd.chronic_disease', 'name' => 'oadh_cd_chronic_disease', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'cd.sex', 'name' => 'oadh_cd_sex', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'cd.age', 'name' => 'oadh_cd_age', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.jail'), 'index' => 'cd.jail', 'name' => 'oadh_cd_jail', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'cd.year', 'name' => 'oadh_cd_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='free-cd-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='free-cd-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'free-cd-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-cd-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-cd-header-rows-number', 1 , array('id' => 'free-cd-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('free-cd-id', null, array('id' => 'free-cd-id')) !!}
											{!! Form::hidden('free-cd-name', null, array('id' => 'free-cd-name')) !!}
											{!! Form::hidden('free-cd-system-route', null, array('id' => 'free-cd-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-cd-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-cd-last-row-number', null, array('id' => 'free-cd-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-cd-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('free-cd-mp-date-format', null, array('id' => 'free-cd-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-cd-chronic-disease', Lang::get('decima-oadh::back-end-column.chronicDisease'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-cd-chronic-disease', 'A' , array('id' => 'free-cd-chronic-disease', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-cd-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-cd-sex', 'B' , array('id' => 'free-cd-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-cd-age', Lang::get('decima-oadh::back-end-column.age'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-cd-age', 'C' , array('id' => 'free-cd-age', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-cd-jail', Lang::get('decima-oadh::back-end-column.jail'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-cd-jail', 'D' , array('id' => 'free-cd-jail', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-cd-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-cd-year', 'E' , array('id' => 'free-cd-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="free-cd-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='free-cd-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-cd-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-cd-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-cd-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-cd-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-cd-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-cd-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-cd-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="free-cd-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-cd-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
