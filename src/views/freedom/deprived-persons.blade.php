@extends('layouts.base')

@section('container')
{!! Form::hidden('free-dp-new-action', null, array('id' => 'free-dp-new-action')) !!}
{!! Form::hidden('free-dp-edit-action', null, array('id' => 'free-dp-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('free-dp-remove-action', null, array('id' => 'free-dp-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'free-dp-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'free-dp-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_free-dp-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeDpOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#free-dp-grid').getSelectedRowId('file_id'), rowData = $('#free-dp-grid').getRowData($('#free-dp-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#free-dp-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppDpurnals('free-dp-', 'firstPage', id);

				$('#free-dp-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'dp.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#free-dp-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'dp.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#free-dp-btn-group-2').enableButtonGroup();

		}

		function freeCoDetailOnSelectRowEvent()
		{
			var selRowIds = $('#free-dp-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#free-dp-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#free-dp-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#free-dp-detail-btn-group-2').disabledButtonGroup();
				$('#free-dp-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeDpOnLoadCompleteEvent()
		{
			$('#free-dp-temp-data').jqGrid('clearGridData');
			$('#free-dp-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.free-dp-btn-tooltip').tooltip();

			$('#free-dp-mp-form').jqMgVal('addFormFieldsValidations');

			$('#free-dp-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#free-dp-form-section').on('hidden.bs.collapse', function ()
			{
				$('#free-dp-grid-section').collapse('show');

				$('#free-dp-journals-section').collapse('show');

				$('#free-dp-filters').show();
			});

			$('#free-dp-form-section').on('shown.bs.collapse', function ()
			{
				// $('#free-dp-').focus();
			});

			$('#free-dp-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#free-dp-btn-refresh').click();
	    });

			$('#free-dp-').focusout(function()
			{
				$('#free-dp-btn-save').focus();
			});

			$('#free-dp-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.free-dp-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#free-dp-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#free-dp-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('free-dp-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('free-dp-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('free-dp-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#free-dp-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-dp-btn-toolbar').disabledButtonGroup();
				$('#free-dp-btn-group-1').enableButtonGroup();

				if($('#free-dp-journals-section').attr('data-target-id') == '' || $('#free-dp-journals-section').attr('data-target-id') == '#free-dp-form-section')
				{
					$('#free-dp-grid').trigger('reloadGrid');
					cleanJournals('free-dp-');
				}
				else
				{

				}
			});

			$('#free-dp-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#free-dp-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#free-dp-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#free-dp-detail-btn-export-xls').click(function()
			{
				$('#free-dp-back-detail-gridXlsButton').click();
			});

			$('#free-dp-detail-btn-export-csv').click(function()
			{
				$('#free-dp-back-detail-gridCsvButton').click();
			});

			$('#free-dp-btn-export-xls').click(function()
			{
				if($('#free-dp-journals-section').attr('data-target-id') == '')
				{
					$('#free-dp-gridXlsButton').click();
				}
			});

			$('#free-dp-btn-export-csv').click(function()
			{
				if($('#free-dp-journals-section').attr('data-target-id') == '')
				{
					$('#free-dp-gridCsvButton').click();
				}
			});

			$('#free-dp-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-dp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-dp-grid').isRowSelected())
					{
						$('#free-dp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-dp-modal-delete-file').modal('show');
			});

			$('#free-dp-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-dp-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-dp-btn-toolbar', false);
						$('#free-dp-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-dp-modal-delete-prod').click();
							$('#free-dp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-dp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-dp-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#free-dp-grid').trigger('reloadGrid');
						$('#free-dp-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#free-dp-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-dp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-dp-grid').isRowSelected())
					{
						$('#free-dp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-dp-modal-delete-prod').modal('show');
			});

			$('#free-dp-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-dp-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-dp-btn-toolbar', false);
						$('#free-dp-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-dp-modal-delete-prod').click();
							$('#free-dp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#free-dp-prod-data').trigger('reloadGrid');
							$('#free-dp-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-dp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-dp-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-dp-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-dp-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-dp-grid').isRowSelected())
					{
						$('#free-dp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-dp-modal-generate').modal('show');
			});

			$('#free-dp-btn-modal-generate').click(function()
			{
				var rowData = $('#free-dp-grid').getRowData($('#free-dp-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-dp-btn-toolbar', false);
						$('#free-dp-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-dp-btn-refresh').click();
							$("#free-dp-btn-group-2").disabledButtonGroup();
							$('#free-dp-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-dp-btn-refresh').click();
							$("#free-dp-btn-group-2").disabledButtonGroup();
							$('#free-dp-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-dp-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-dp-btn-process').click(function()
			{

				if(!$('#free-dp-grid').isRowSelected())
				{
					$('#free-dp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#free-dp-grid').getRowData($('#free-dp-grid').jqGrid('getGridParam', 'selrow'));

				$('#free-dp-mp-form').jqMgVal('clearContextualClasses');
				$('#free-dp-id').val(rowData.file_id);
				$('#free-dp-name').val(rowData.file_name);
				$('#free-dp-system-route').val(rowData.file_system_route);

				$('#free-dp-mp-modal').modal('show');
			});

			$('#free-dp-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#free-dp-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#free-dp-mp-form').formToObject('free-dp-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-dp-btn-toolbar', false);
						$('#free-dp-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#free-dp-btn-refresh').click();
							$('#free-dp-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#free-dp-temp-data, #free-dp-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'dp.file_id','op':'eq','data':'" + $('#free-dp-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-dp-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#free-dp-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#free-dp-edit-action').isEmpty())
			{
				showButtonHelper('free-dp-btn-close', 'free-dp-btn-group-2', $('#free-dp-edit-action').attr('data-content'));
			}

			$('#free-dp-btn-delete-helper').click(function()
		  {
				showButtonHelper('free-dp-btn-close', 'free-dp-btn-group-2', $('#free-dp-delete-action').attr('data-content'));
		  });

			if(!$('#free-dp-delete-action').isEmpty())
			{
				showButtonHelper('free-dp-btn-close', 'free-dp-btn-group-2', $('#free-dp-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="free-dp-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="free-dp-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'free-dp-btn-upload', 'class' => 'btn btn-default free-dp-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'free-dp-btn-refresh', 'class' => 'btn btn-default free-dp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="free-dp-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'free-dp-btn-process', 'class' => 'btn btn-default free-dp-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'free-dp-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'free-dp-btn-delete-prod', 'class' => 'btn btn-default free-dp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'free-dp-btn-delete-file', 'class' => 'btn btn-default free-dp-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='free-dp-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='free-dp-grid'>
				{!!
				GridRender::setGridId("free-dp-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeDpOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeDpOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#free-dp-temp-data-tab" aria-controls="free-dp-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#free-dp-processed-data-tab" aria-controls="free-dp-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="free-dp-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-dp-front-temp-data'>
      				{!!
      				GridRender::setGridId('free-dp-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'dp.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'dp.id', 'name' => 'oadh_dp_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'dp.file_id', 'name' => 'oadh_dp_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'dp.year', 'name' => 'oadh_dp_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.humanRight'), 'index' => 'dp.right_to', 'name' => 'oadh_dp_right_to', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.violatedFact'), 'index' => 'dp.violated_fact', 'name' => 'oadh_dp_violated_fact', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'dp.sex', 'name' => 'oadh_dp_sex', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'dp.age', 'name' => 'oadh_dp_age', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'dp.department', 'name' => 'oadh_dp_departament', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'dp.municipality', 'name' => 'oadh_dp_municipality', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'dp.status', 'name' => 'oadh_dp_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="free-dp-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-dp-front-processed-grid'>
              {!!
              GridRender::setGridId('free-dp-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'dp.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'dp.id', 'name' => 'oadh_dp_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'dp.file_id', 'name' => 'oadh_dp_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'dp.year', 'name' => 'oadh_dp_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::human-right-management.humanRight'), 'index' => 'dp.right_to', 'name' => 'oadh_dp_right_to', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::media-monitoring.violatedFact'), 'index' => 'dp.violated_fact', 'name' => 'oadh_dp_violated_fact', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.sex'), 'index' => 'dp.sex', 'name' => 'oadh_dp_sex', 'align' => 'center', 'width' => '15'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.age'), 'index' => 'dp.age', 'name' => 'oadh_dp_age', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'dp.department', 'name' => 'oadh_dp_departament', 'align' => 'center', 'width' => 15, 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'dp.municipality', 'name' => 'oadh_dp_municipality', 'align' => 'center', 'width' => 15, 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='free-dp-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='free-dp-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'free-dp-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-dp-header-rows-number', 1 , array('id' => 'free-dp-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('free-dp-id', null, array('id' => 'free-dp-id')) !!}
											{!! Form::hidden('free-dp-name', null, array('id' => 'free-dp-name')) !!}
											{!! Form::hidden('free-dp-system-route', null, array('id' => 'free-dp-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-dp-last-row-number', null, array('id' => 'free-dp-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('free-dp-mp-date-format', null, array('id' => 'free-dp-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-dp-year', 'A' , array('id' => 'free-dp-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-right_to', Lang::get('decima-oadh::human-right-management.humanRight'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-dp-right_to', 'B' , array('id' => 'free-dp-right_to', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-violated-fact', Lang::get('decima-oadh::media-monitoring.violatedFact'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-dp-violated-fact', 'C' , array('id' => 'free-dp-violated-fact', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-sex', Lang::get('decima-oadh::back-end-column.sex'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-dp-sex', 'D' , array('id' => 'free-dp-sex', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-age', Lang::get('decima-oadh::back-end-column.age'), array('class' => 'control-label control-label-hidden', 'style' => 'margin-bottom: 25px;')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-dp-age', 'E' , array('id' => 'free-dp-age', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-departament', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-dp-departament', 'F' , array('id' => 'free-dp-departament', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-dp-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-dp-municipality', 'G' , array('id' => 'free-dp-municipality', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="free-dp-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='free-dp-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-dp-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-dp-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-dp-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-dp-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-dp-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-dp-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-dp-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="free-dp-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-dp-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
