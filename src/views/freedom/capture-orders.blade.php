@extends('layouts.base')

@section('container')
{!! Form::hidden('free-co-new-action', null, array('id' => 'free-co-new-action')) !!}
{!! Form::hidden('free-co-edit-action', null, array('id' => 'free-co-edit-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::hidden('free-co-remove-action', null, array('id' => 'free-co-remove-action', 'data-content' => Lang::get('decima-inventory::requisition-management.editHelpText'))) !!}
{!! Form::button('', array('id' => 'free-co-btn-delete-helper', 'class' => 'hidden')) !!}
{!! Form::button('', array('id' => 'free-co-btn-void-helper', 'class' => 'hidden')) !!}
<style>
	#gbox_free-co-grid .ui-th-column-header {
		text-align: left;
		padding-left: 20px;
	}
</style>
<script type="text/javascript">

		function freeCoOnSelectRowEvent()
		{
			// var id = 1;
			var id = $('#free-co-grid').getSelectedRowId('file_id'), rowData = $('#free-co-grid').getRowData($('#free-co-grid').jqGrid('getGridParam', 'selrow'));

			// if($('#free-co-journals').attr('data-journalized-id') != id)
			// {
			// 	getAppJournals('free-co-', 'firstPage', id);

				$('#free-co-temp-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'co.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
				$('#free-co-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'co.file_id','op':'eq','data':" + id + "}]}"}}).trigger('reloadGrid');
			// }

			$('#free-co-btn-group-2').enableButtonGroup();

		}

		function freeCoDetailOnSelectRowEvent()
		{
			var selRowIds = $('#free-co-back-detail-grid').jqGrid('getGridParam', 'selarrrow');

			if(selRowIds.length == 0)
			{
				$('#free-co-detail-btn-group-2').disabledButtonGroup();
			}
			else if(selRowIds.length == 1)
			{
				$('#free-co-detail-btn-group-2').enableButtonGroup();
			}
			else if(selRowIds.length > 1)
			{
				$('#free-co-detail-btn-group-2').disabledButtonGroup();
				$('#free-co-detail-btn-delete').removeAttr('disabled');
			}
		}

		function freeCoOnLoadCompleteEvent()
		{
			$('#free-co-temp-data').jqGrid('clearGridData');
			$('#free-co-prod-data').jqGrid('clearGridData');
		}


		$(document).ready(function()
		{
			$('.free-co-btn-tooltip').tooltip();

			$('#free-co-mp-form').jqMgVal('addFormFieldsValidations');

			$('#free-co-journals-section').on('hidden.bs.collapse', function ()
			{
				$($(this).attr('data-target-id')).collapse('show');
			});

			$('#free-co-form-section').on('hidden.bs.collapse', function ()
			{
				$('#free-co-grid-section').collapse('show');

				$('#free-co-journals-section').collapse('show');

				$('#free-co-filters').show();
			});

			$('#free-co-form-section').on('shown.bs.collapse', function ()
			{
				// $('#free-co-').focus();
			});

			$('#free-co-file-uploader-modal').on('hidden.bs.modal', function (e)
			{
	      $('#free-co-btn-refresh').click();
	    });

			$('#free-co-').focusout(function()
			{
				$('#free-co-btn-save').focus();
			});

			$('#free-co-btn-upload').click(function()
			{
				var rowData, rowId;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				$('.free-co-btn-tooltip').tooltip('hide');
				//parentFileId = Obtener el id de la fila seleccionada del grid
				//Validar si la fila seleccionada es una registro de tipo carpeta
				//Sino lo es, mostrar mensaje (tipo info) utilizando esta funcion:
				//$('#free-co-folder-validation-message').removeClass('hidden');
				// parentFileId = $('#free-co-grid').getSelectedRowId('inv_mi_id');
				// openUploader(prefix, systemReferenceId, parentFolder, allowedFileTypes, minWidth, sameWidthAsHeigth, sizes, maxFileCount, isPublic, parentFileId)
				// openUploader('free-co-', '', '', ['spreadsheet'], '', false, [], 0, false, 2873);
				//openUploader('free-co-', '', '', ['spreadsheet'], '', false, [], 0, false, 54);
				openUploader('free-co-', '', '', ['spreadsheet'], '', false, [], 0, false, {{ Config::get('folders.' . $appInfo['id']) }});
			});

			$('#free-co-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-co-btn-toolbar').disabledButtonGroup();
				$('#free-co-btn-group-1').enableButtonGroup();

				if($('#free-co-journals-section').attr('data-target-id') == '' || $('#free-co-journals-section').attr('data-target-id') == '#free-co-form-section')
				{
					$('#free-co-grid').trigger('reloadGrid');
					cleanJournals('free-co-');
				}
				else
				{

				}
			});

			$('#free-co-detail-btn-refresh').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				$('#free-co-back-detail-grid').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'file_id','op':'eq','data':'" + $('#free-co-detail-master-id').val() + "'}]}"}}).trigger('reloadGrid');
			});

			$('#free-co-detail-btn-export-xls').click(function()
			{
				$('#free-co-back-detail-gridXlsButton').click();
			});

			$('#free-co-detail-btn-export-csv').click(function()
			{
				$('#free-co-back-detail-gridCsvButton').click();
			});

			$('#free-co-btn-export-xls').click(function()
			{
				if($('#free-co-journals-section').attr('data-target-id') == '')
				{
					$('#free-co-gridXlsButton').click();
				}
			});

			$('#free-co-btn-export-csv').click(function()
			{
				if($('#free-co-journals-section').attr('data-target-id') == '')
				{
					$('#free-co-gridCsvButton').click();
				}
			});

			$('#free-co-btn-delete-file').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-co-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-co-grid').isRowSelected())
					{
						$('#free-co-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-co-modal-delete-file').modal('show');
			});

			$('#free-co-btn-modal-delete-file').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-co-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-file',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-co-btn-toolbar', false);
						$('#free-co-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-co-modal-delete-prod').click();
							$('#free-co-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-co-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-co-modal-delete-file').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
						$('#free-co-grid').trigger('reloadGrid');
						$('#free-co-prod-data').trigger('reloadGrid');
					}
				});
			});

			$('#free-co-btn-delete-prod').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-co-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-co-grid').isRowSelected())
					{
						$('#free-co-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-co-modal-delete-prod').modal('show');
			});

			$('#free-co-btn-modal-delete-prod').click(function()
			{
				var id, url;

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': $('#free-co-grid').getSelectedRowId('file_id')
						}
					),
					dataType: 'json',
					url: $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-delete-from-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-co-btn-toolbar', false);
						$('#free-co-modal-delete').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-co-modal-delete-prod').click();
							$('#free-co-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);

							$('#free-co-prod-data').trigger('reloadGrid');
							$('#free-co-grid').trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-co-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-co-modal-delete-prod').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-co-btn-generate').click(function()
			{
				var rowData;

				if($(this).hasAttr('disabled'))
				{
					return;
				}

				if($('#free-co-journals-section').attr('data-target-id') == '')
				{
					if(!$('#free-co-grid').isRowSelected())
					{
						$('#free-co-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
						return;
					}

				}
				else
				{

				}

				$('.decima-erp-tooltip').tooltip('hide');
				$('#free-co-modal-generate').modal('show');
			});

			$('#free-co-btn-modal-generate').click(function()
			{
				var rowData = $('#free-co-grid').getRowData($('#free-co-grid').jqGrid('getGridParam', 'selrow'));

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(
						{
							'_token':$('#app-token').val(),
							'file_id': rowData['file_id']
						}
					),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-copy-to-production',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-co-btn-toolbar', false);
						$('#free-co-modal-generate').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							$('#free-co-btn-refresh').click();
							$("#free-co-btn-group-2").disabledButtonGroup();
							$('#free-co-btn-toolbar').showAlertAfterElement('alert-success alert-custom',json.success, 5000);
						}

						if(json.info)
						{
							$('#free-co-btn-refresh').click();
							$("#free-co-btn-group-2").disabledButtonGroup();
							$('#free-co-btn-toolbar').showAlertAfterElement('alert-info alert-custom',json.info, 5000);
						}

						$('#free-co-modal-generate').modal('hide');

						$('#app-loader').addClass('hidden');
						enableAll();

						$('.decima-erp-tooltip').tooltip('hide');
					}
				});
			});

			$('#free-co-btn-process').click(function()
			{

				if(!$('#free-co-grid').isRowSelected())
				{
					$('#free-co-btn-toolbar').showAlertAfterElement('alert-info alert-custom', lang.invalidSelection, 5000);
					return;
				}

				rowData = $('#free-co-grid').getRowData($('#free-co-grid').jqGrid('getGridParam', 'selrow'));

				$('#free-co-mp-form').jqMgVal('clearContextualClasses');
				$('#free-co-id').val(rowData.file_id);
				$('#free-co-name').val(rowData.file_name);
				$('#free-co-system-route').val(rowData.file_system_route);

				$('#free-co-mp-modal').modal('show');
			});

			$('#free-co-mp-btn-process').click(function()
			{
				$('.decima-erp-tooltip').tooltip('hide');

				if(!$('#free-co-mp-form').jqMgVal('isFormValid'))
				{
					return;
				}

				//Cambio parametrizacion
				//Agrego un campo de columns para hacerlo mas dinamico
				let data = $('#free-co-mp-form').formToObject('free-co-');
				data['columns'] = ['year', 'court', 'sex'];
				data['view'] = 'HabeasCorpusRequest';

				$.ajax(
				{
					type: 'POST',
					data: JSON.stringify(data),
					dataType : 'json',
					url:  $('#app-url').val() + '/ucaoadh/freedom/freedom-uploader/{{ $appInfo['id'] }}-process',
					error: function (jqXHR, textStatus, errorThrown)
					{
						handleServerExceptions(jqXHR, 'free-co-btn-toolbar', false);
						$('#free-co-mp-modal').modal('hide');
					},
					beforeSend:function()
					{
						$('#app-loader').removeClass('hidden');
						disabledAll();
					},
					success:function(json)
					{
						if(json.success)
						{
							// $('#free-co-btn-refresh').click();
							$('#free-co-btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.success, 5000);
							$('#free-co-temp-data, #free-co-prod-data').jqGrid('setGridParam', {'postData':{"filters":"{'groupOp':'AND','rules':[{'field':'co.file_id','op':'eq','data':'" + $('#free-co-grid').getSelectedRowId('file_id') + "'}]}"}}).trigger('reloadGrid');
						}

						if(json.info)
						{
							$('#free-co-btn-toolbar').showAlertAfterElement('alert-info alert-custom', json.info, 15000);
						}

						$('#app-loader').addClass('hidden');
						enableAll();

						$('#free-co-mp-modal').modal('hide');
					}
				});
			});

			if(!$('#free-co-edit-action').isEmpty())
			{
				showButtonHelper('free-co-btn-close', 'free-co-btn-group-2', $('#free-co-edit-action').attr('data-content'));
			}

			$('#free-co-btn-delete-helper').click(function()
		  {
				showButtonHelper('free-co-btn-close', 'free-co-btn-group-2', $('#free-co-delete-action').attr('data-content'));
		  });

			if(!$('#free-co-delete-action').isEmpty())
			{
				showButtonHelper('free-co-btn-close', 'free-co-btn-group-2', $('#free-co-delete-action').attr('data-content'));
			}
		});
</script>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div id="free-co-btn-toolbar" class="section-header btn-toolbar" role="toolbar">
			<div id="free-co-btn-group-1" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-upload"></i> ' . Lang::get('decima-file::file-management.upload'), array('id' => 'free-co-btn-upload', 'class' => 'btn btn-default free-co-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('decima-file::file-management.uploadLongText'))) !!}
				{!! Form::button('<i class="fa fa-refresh"></i> ' . Lang::get('toolbar.refresh'), array('id' => 'free-co-btn-refresh', 'class' => 'btn btn-default free-co-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => Lang::get('toolbar.refreshLongText'))) !!}
			</div>
			<div id="free-co-btn-group-2" class="btn-group btn-group-app-toolbar">
				{!! Form::button('<i class="fa fa-cogs"></i> ' . Lang::get('toolbar.process'), array('id' => 'free-co-btn-process', 'class' => 'btn btn-default free-co-btn-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-copy"></i> ' . Lang::get('decima-oadh::back-end-general.copyToProd'), array('id' => 'free-co-btn-generate', 'class' => 'btn btn-default', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFromProd'), array('id' => 'free-co-btn-delete-prod', 'class' => 'btn btn-default free-co-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
				{!! Form::button('<i class="fa fa-minus"></i> ' . Lang::get('decima-oadh::back-end-general.deleteFile'), array('id' => 'free-co-btn-delete-file', 'class' => 'btn btn-default free-co-btn-tooltip decima-erp-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '')) !!}
			</div>
		</div>
		<div id='free-co-grid-section' class='collapse in'>
			<div class='app-grid' data-app-grid-id='free-co-grid'>
				{!!
				GridRender::setGridId("free-co-grid")
					->enablefilterToolbar(false, false)
					->hideXlsExporter()
	  			->hideCsvExporter()
					->setGridOption('rowNum', 5)
		    	->setGridOption('url',URL::to('/ucaoadh/file'))
		    	->setGridOption('caption', Lang::get('decima-file::file-management.gridTitle'))
					//->setGridOption('postData', array('_token' => Session::token()))
		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'f.parent_file_id','op':'eq','data':'" . Config::get('folders.' . $appInfo['id']) ."'}]}"))
					->setGridEvent('loadComplete', 'freeCoOnLoadCompleteEvent')
					->setGridEvent('onSelectRow', 'freeCoOnSelectRowEvent')
	    		->setGridOption('multiselect', false)
	    		->addColumn(array('index' => 'f.id', 'name' => 'file_id', 'hidden' => true))
					->addColumn(array('index' => 'f.system_route', 'name' => 'file_system_route', 'hidden' => true))
					->addColumn(array('label' => '', 'index' => 'f.icon_html' ,'name' => 'file_icon_html' , 'width' => '5'))
	    		->addColumn(array('label' => Lang::get('decima-file::file-management.name'), 'index' => 'f.name' ,'name' => 'file_name'))
					->addColumn(array('label' => '', 'index' => 'f.url_html' ,'name' => 'file_url_html', 'width' => '10', 'align' => 'center'))
		    	->renderGrid();
				!!}
			</div>
      <div id="oadh-filters-body" class="app-grid" style='padding-top: 10px'>
        <ul class="nav nav-tabs" role="tablist" style="margin-top:0px;">
          <li role="presentation" class="active"><a href="#free-co-temp-data-tab" aria-controls="free-co-temp-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.tempData') }}</a></li>
          <li role="presentation" ><a href="#free-co-processed-data-tab" aria-controls="free-co-processed-data-tab" role="tab" data-toggle="tab">{{ Lang::get('decima-oadh::back-end-general.prodData') }}</a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="free-co-temp-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-co-front-temp-data'>
      				{!!
      				GridRender::setGridId('free-co-temp-data')
      					->hideXlsExporter()
      	  			->hideCsvExporter()
      					->setGridOption('height', 'auto')
      					->setGridOption('multiselect', false)
      					->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
      					->setGridOption('rowNum', 5)
      		    	->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-temp-grid-data'))
      		    	->setGridOption('caption', Lang::get('decima-oadh::back-end-general.tempData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'co.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'co.id', 'name' => 'oadh_co_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'co.file_id', 'name' => 'oadh_co_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'co.department', 'name' => 'oadh_co_departament', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'co.municipality', 'name' => 'oadh_co_municipality', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'co.year', 'name' => 'oadh_co_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.status'), 'index' => 'co.status', 'name' => 'oadh_co_status', 'formatter' => 'select', 'editoptions' => array('value' => Lang::get('decima-oadh::back-end-column.statusGridText')), 'align' => 'center', 'hidden' => false, 'stype' => 'select', 'align' => 'center', 'width' => '20'))
      		    	->renderGrid();
      				!!}
      			</div>
          </div>
          <div role="tabpanel" class="tab-pane" id="free-co-processed-data-tab">
            <div class='app-grid app-grid-without-toolbar section-block' data-app-grid-id='free-co-front-processed-grid'>
              {!!
              GridRender::setGridId('free-co-prod-data')
                ->hideXlsExporter()
                ->hideCsvExporter()
                ->setGridOption('height', 'auto')
                ->setGridOption('multiselect', false)
                ->setGridOption('rowList', array(5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 100, 250, 500, 750, 1000, 2500, 5000))
                ->setGridOption('rowNum', 5)
                ->setGridOption('url', URL::to('/ucaoadh/freedom/freedom-uploader/' . $appInfo['id'] . '-prod-grid-data'))
								->setGridOption('caption', Lang::get('decima-oadh::back-end-general.prodData'))
      		    	->setGridOption('postData', array('_token' => Session::token(), 'filters'=>"{'groupOp':'AND','rules':[{'field':'co.file_id','op':'eq','data':'-1'}]}"))
      					->setGridOption('footerrow',false)
      		    	->addColumn(array('index' => 'co.id', 'name' => 'oadh_co_id', 'hidden' => true))
      		    	->addColumn(array('index' => 'co.file_id', 'name' => 'oadh_co_file_id', 'hidden' => true))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.department'), 'index' => 'co.department', 'name' => 'oadh_co_departament', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.municipality'), 'index' => 'co.municipality', 'name' => 'oadh_co_municipality', 'align' => 'center', 'width' => 80, 'hidden' => false, 'width' => '90'))
								->addColumn(array('label' => Lang::get('decima-oadh::back-end-column.year'), 'index' => 'co.year', 'name' => 'oadh_co_year', 'align' => 'center', 'width' => '10', 'hidden' => false))
                ->renderGrid();
              !!}
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<div id='free-co-journals-section' class="row collapse in section-block" data-target-id="" data-sale-id="">

</div>
<div id='free-co-mp-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog">
    <div class="modal-content">
			<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ Lang::get('decima-oadh::back-end-general.process') }}</h4>
      </div>
			<div class="modal-body clearfix">
				{!! Form::open(array('id' => 'free-co-mp-form', 'role' => 'form', 'onsubmit' => 'return false;')) !!}
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.general') }}</legend>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-co-mp-header-rows-number', Lang::get('decima-oadh::back-end-general.headerRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-co-header-rows-number', 1 , array('id' => 'free-co-header-rows-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
											{!! Form::hidden('free-co-id', null, array('id' => 'free-co-id')) !!}
											{!! Form::hidden('free-co-name', null, array('id' => 'free-co-name')) !!}
											{!! Form::hidden('free-co-system-route', null, array('id' => 'free-co-system-route')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-co-last-row-number', Lang::get('decima-oadh::back-end-general.lastRowsNumber'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon">#</span>
											{!! Form::text('free-co-last-row-number', null, array('id' => 'free-co-last-row-number', 'class' => 'form-control', 'data-mg-validator' => 'positiveInteger', 'data-mg-required' => '')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<!-- <div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-co-mp-date-format', Lang::get('decima-oadh::back-end-general.dateFormat'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
											{!! Form::text('free-co-mp-date-format', null, array('id' => 'free-co-mp-date-format', 'class' => 'form-control', 'data-mg-required' => '', 'maxlength' => '5')) !!}
										</div>
									</div> -->
								</div>
							</div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<legend>{{ Lang::get('decima-oadh::back-end-general.processLegend') }}</legend>
						</div>
		 			</div>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-co-departament', Lang::get('decima-oadh::back-end-column.department'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-co-departament', 'A' , array('id' => 'free-co-departament', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-co-municipality', Lang::get('decima-oadh::back-end-column.municipality'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-co-municipality', 'B' , array('id' => 'free-co-municipality', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-co-year', Lang::get('decima-oadh::back-end-column.year'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-co-year', 'C' , array('id' => 'free-co-year', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div>
								<!-- <div class="col-md-2" style="padding-right: 5px;">
									<div class="form-group mg-hm help-block-hidden">
										{!! Form::label('free-co-status', Lang::get('decima-oadh::back-end-general.status'), array('class' => 'control-label control-label-hidden')) !!}
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-sort-alpha-asc"></i></span>
											{!! Form::text('free-co-status', 'D' , array('id' => 'free-co-status', 'class' => 'form-control', 'maxlength' => '1')) !!}
										</div>
									</div>
								</div> -->
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> {{ Lang::get('toolbar.close') }}</button>
				<button id="free-co-mp-btn-process" type="button" class="btn btn-primary"><i class="fa fa fa-check"></i> {{ Lang::get('toolbar.process') }}</button>
			</div>
  	</div>
	</div>
</div>

<div id='free-co-modal-delete-prod' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-co-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				 <!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-co-btn-modal-delete-prod" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-co-modal-delete-file' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-co-btn-delete">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				<!-- <p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}"></p> -->
				<p  data-default-label="{{ Lang::get('decima-file::file-management.deleteFileMessageConfirmation') }}">
					{{ Lang::get('decima-oadh::back-end-general.deleteFromProdMessageConfirmation') }}
				</p>
				<!-- <p  data-default-label="{{ Lang::get('module::app.deleteMessageConfirmation') }}"></p> -->
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-co-btn-modal-delete-file" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>

<div id='free-co-modal-generate' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm free-co-btn-generate">
    <div class="modal-content">
			<div class="modal-body" style="padding: 20px 20px 0px 20px;">
				 <p id="free-co-generate-message">
					 {{ Lang::get('decima-oadh::back-end-general.copyToProductionConfirmationMessage') }}
				 </p>
      </div>
			<div class="modal-footer" style="text-align:center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('form.no') }}</button>
				<button id="free-co-btn-modal-generate" type="button" class="btn btn-primary">{{ Lang::get('form.yes') }}</button>
			</div>
    </div>
  </div>
</div>
@include('decima-file::file-uploader')
@parent
@stop
