<?php
/**
 * @file
 * Journals configuration config file.
 *
 * All DecimaOadh code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(

	/*
 	|--------------------------------------------------------------------------
	| Initial DecimaOadh Setup Page
	|--------------------------------------------------------------------------
	|
	*/
	'initial-oadh-setup' => array('journalizedType' => array('OADH_Settings'), 'recordsPerPage' => 4),

	/*
 	|--------------------------------------------------------------------------
	|
	|--------------------------------------------------------------------------
	|
	*/
	'media-monitoring' => array('journalizedType' => array('OADH_News'), 'recordsPerPage' => 4),
	'digital-media-management' => array('journalizedType' => array('OADH_Digital_Media'), 'recordsPerPage' => 4),
	'human-right-management' => array('journalizedType' => array('OADH_Human_Right'), 'recordsPerPage' => 4),
	'habeas-corpus-request' => array('journalizedType' => array('OADH_FREE_Habeas_Corpus_Request'), 'recordsPerPage' => 4),
	'publication-management' => array('journalizedType' => array('OADH_CMS_Publication'), 'recordsPerPage' => 4),
	'activity-management' => array('journalizedType' => array('OADH_CMS_Activity'), 'recordsPerPage' => 4),
	'multimedia-management' => array('journalizedType' => array('OADH_CMS_Multimedia'), 'recordsPerPage' => 4),
	'institutions-management' => array('journalizedType' => array('OADH_Institution'), 'recordsPerPage' => 4),
	'right-management' => array('journalizedType' => array('OADH_Right'), 'recordsPerPage' => 4),
	'recommendations-management' => array('journalizedType' => array('OADH_Recommendation'), 'recordsPerPage' => 4),
	// 'recommendations-import' => array('journalizedType' => array('OADH_R'), 'recordsPerPage' => 4),
);
