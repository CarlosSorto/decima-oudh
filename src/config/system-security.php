<?php
/**
 * @file
 * System security configuration.
 *
 * All DecimaERP code is copyright by the original authors and released under the GNU Aferro General Public License version 3 (AGPLv3) or later.
 * See COPYRIGHT and LICENSE.
 */

return array(

	/*
	|--------------------------------------------------------------------------
	| System logo public path
	|--------------------------------------------------------------------------
	|
	*/

	// 'system_logo_public_path' => '',
	'system_logo_public_path' => 'oadh/logos/logo-primario-siglas-v1.png',

	/*
	|--------------------------------------------------------------------------
	| System logo login visibility
	|--------------------------------------------------------------------------
	|
	*/

	'system_logo_login_is_visible' => true,

	/*
	|--------------------------------------------------------------------------
	| System logo topabar visibility
	|--------------------------------------------------------------------------
	|
	*/

	'system_logo_topbar_is_visible' => true,

	/*
	|--------------------------------------------------------------------------
	| System title
	|--------------------------------------------------------------------------
	|
	*/

	'system_title' => 'DecimaERP',

	/*
	|--------------------------------------------------------------------------
	| System title login visibility
	|--------------------------------------------------------------------------
	|
	*/

	'system_title_login_is_visible' => false,

	/*
	|--------------------------------------------------------------------------
	| System title topabar visibility
	|--------------------------------------------------------------------------
	|
	*/

	'system_title_topbar_is_visible' => false,

	/*
	|--------------------------------------------------------------------------
	| System logo public path
	|--------------------------------------------------------------------------
	|
	*/

	'powered_by' => '',

	/*
	|--------------------------------------------------------------------------
	| System name
	|--------------------------------------------------------------------------
	|
	*/

	'system_name' => 'DecimaERP v2.0',

	/*
	|--------------------------------------------------------------------------
	| System icon
	|--------------------------------------------------------------------------
	|
	*/

	'system_icon' => 'fa fa-area-chart',

	/*
	|--------------------------------------------------------------------------
	| Reply to email
	|--------------------------------------------------------------------------
	|
	*/

	'reply_to_email' => 'no-reply@decimaerp.com',

	/*
	|--------------------------------------------------------------------------
	| Reply to name
	|--------------------------------------------------------------------------
	|
	*/

	'reply_to_name' => 'DecimaERP',

	/*
	|--------------------------------------------------------------------------
	| Brand URL
	|--------------------------------------------------------------------------
	|
	*/

	'brand_url' => 'http://www.decimaerp.com',

	/*
	|--------------------------------------------------------------------------
	| Root user id array
	|--------------------------------------------------------------------------
	|
	*/

	'root_users_id' => array(1),

	/*
	|--------------------------------------------------------------------------
	| Root default email
	|--------------------------------------------------------------------------
	|
	*/

	'root_default_email' => 'root@decimaerp.com',

	/*
	|--------------------------------------------------------------------------
	| Root default password
	|--------------------------------------------------------------------------
	|
	*/

	'root_default_password' => 'root',

	/*
	|--------------------------------------------------------------------------
	| Apps that can only be access
	|--------------------------------------------------------------------------
	|
	| Apps that can only be access my admin users
	|
	*/

	'admin_apps_id' => array(2),

	/*
	|--------------------------------------------------------------------------
	| Permissions that can only be access
	|--------------------------------------------------------------------------
	|
	| Permissions that can only be used my admin users
	|
	*/

	'admin_permissions_id' => array(5),

	/*
	|--------------------------------------------------------------------------
	| Notify user by email when added to an organization
	|--------------------------------------------------------------------------
	|
	*/
	'email_organization_user' => env('EMAIL_ORGANIZATION_USER', true),

	/*
	|--------------------------------------------------------------------------
	| Support email
	|--------------------------------------------------------------------------
	|
	*/
	'support_email' => env('SUPPORT_EMAIL', 'no-reply@decimaerp.com'),

	/*
	|--------------------------------------------------------------------------
	| Support email
	|--------------------------------------------------------------------------
	|
	*/
	'support_name' => env('SUPPORT_NAME', 'Support DecimaERP'),

	/*
	|--------------------------------------------------------------------------
	| Custom menu (disabled by default)
	|--------------------------------------------------------------------------
	|
	*/
	'custom_menu' => env('CUSTOM_MENU', true),
	// 'custom_menu' => env('CUSTOM_MENU', false),

	/*
	|--------------------------------------------------------------------------
	| Additional Javascript scripts
	|--------------------------------------------------------------------------
	|
	*/
	'additional_js' => explode(',', env('ADDITIONAL_JS', '')),

	/*
	|--------------------------------------------------------------------------
	| Additional CSS stylesheet
	|--------------------------------------------------------------------------
	|
	*/
	'additional_css' => explode(',', env('ADDITIONAL_CSS', '')),

	/*
	|--------------------------------------------------------------------------
	| Additional CDN Javascript scripts
	|--------------------------------------------------------------------------
	|
	*/
	// 'additional_cdn_js' => explode(',', env('ADDITIONAL_CDN_JS', '
	// 	https://cdnjs.cloudflare.com/ajax/libs/jSignature/2.1.2/jSignature.min.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.0.6/jquery.mmenu.all.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/fullcalendar-scheduler/1.9.4/scheduler.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/es-us.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/js/jquery.imgareaselect.min.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.3/tinymce.min.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.3/jquery.tinymce.min.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.3/plugins/table/plugin.min.js,
	// 	https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.3/plugins/code/plugin.min.js
	// ')),
	'additional_cdn_js' => explode(',', env('ADDITIONAL_CDN_JS', '
		https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.2.2/mmenu.js,
		https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/js/jquery.imgareaselect.min.js
	')),
	// 'additional_cdn_js' => explode(',', env('ADDITIONAL_CDN_JS', '')),

	/*
	|--------------------------------------------------------------------------
	| Additional CDN CSS stylesheet
	|--------------------------------------------------------------------------
	|
	*/
	// 'additional_cdn_css' => explode(',', env('ADDITIONAL_CDN_CSS', '
	// 	https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.0.6/jquery.mmenu.all.css,
	// 	https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css,
	// 	https://cdnjs.cloudflare.com/ajax/libs/fullcalendar-scheduler/1.9.4/scheduler.css,
	// 	https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/css/imgareaselect-animated.css
	// ')),
	'additional_cdn_css' => explode(',', env('ADDITIONAL_CDN_CSS', '
		https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.2.2/mmenu.css,
		https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/css/imgareaselect-animated.css
	')),
	// 'additional_cdn_css' => explode(',', env('ADDITIONAL_CDN_CSS', '')),

	/*
	|--------------------------------------------------------------------------
	| Use CDN jasvascript libraries (requieres an internet conection)
	|--------------------------------------------------------------------------
	|
	*/
	'cdnjs' => env('CDNJS', true),

	/*
	|--------------------------------------------------------------------------
	| CAS authentication (disabled by default)
	|--------------------------------------------------------------------------
	|
	*/

	'cas' => env('CAS', false),

	/*
	|--------------------------------------------------------------------------
	| CAS domain organization
	|--------------------------------------------------------------------------
	|
	*/

	'cas_domain' => env('CAS_DOMAIN', ''),

	/*
	|--------------------------------------------------------------------------
	| API key
	|--------------------------------------------------------------------------
	|
	*/

	'api_key' => '00000',

	/*
	|--------------------------------------------------------------------------
	| API tags
	|--------------------------------------------------------------------------
	|
	*/

	'api_tags' => array(
		'0001' => 'hdfskjhfkjsdhfkjhsdfhf',
	),

	/*
	|--------------------------------------------------------------------------
	| Demo token
	|--------------------------------------------------------------------------
	|
	*/

	'demo_api_token' => 'kMAWkpEoXWkb9qo1SCD5jX3eHKFvXX',

	/*
	|--------------------------------------------------------------------------
	| Demo token
	|--------------------------------------------------------------------------
	|
	*/

	'demo_api_user' => array(
		'id' => 1,
		'timezone' => 'America/El_Salvador',
		'organization_id' => 32,
		'organization_name' => 'Decima.la Demo',
		'database_connection_name' => 'demodecimala',
		'article_images_folder_id' => 3,
		'main_gallery_images_folder_id' => 94,
		'default_category_id' => 1,
		'default_article_type_id' => 1,
		'default_increase_movement_type_id' => 1,
		'default_decrease_movement_type_id' => 2,
		'default_warehouse_id' => 1,
	),

	/*
	|--------------------------------------------------------------------------
	| Firebase Database URL
	|--------------------------------------------------------------------------
	|
	*/

	'firebase_database_url' => '',/*
	
	|--------------------------------------------------------------------------
	| Firebase Database Key
	|--------------------------------------------------------------------------
	|
	*/

	'firebase_database_key' => '',

	/*
	|--------------------------------------------------------------------------
	| Custom email settings
	|--------------------------------------------------------------------------
	|
	*/

	'custom_mail' => array(
		
	),

);
