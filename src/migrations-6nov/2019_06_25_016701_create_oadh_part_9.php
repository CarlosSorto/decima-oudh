<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart9 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('OADH_CMS_Download', function (Blueprint $table)
      {
        $table->increments('id');
        $table->datetime('datetime');
        $table->unsignedInteger('publication_id')->nullable();
        $table->foreign('publication_id')->references('id')->on('OADH_CMS_Publication');
        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_CMS_Download');
    }
}
