<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('OADH_INT_Crimes_Victims_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("sex")->nullable();
        $table->string("age_range");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Crimes_Victims', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("sex")->nullable();
        $table->string("age_range");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Crimes_Accused_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime")->nullable();
        $table->string("category");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Crimes_Accused', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime")->nullable();
        $table->string("category");
        $table->string("department");
        $table->string("municipality")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Working_Officials_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("category");
        $table->string("sex")->nullable();
        $table->string("department");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Working_Officials', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("category");
        $table->string("sex")->nullable();
        $table->string("department");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Gender_Trafficking_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("sex")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Gender_Trafficking', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("sex")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Type_Trafficking_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Type_Trafficking', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Age_Trafficking_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("age_range");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Age_Trafficking', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("age_range");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Movement_Freedom_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->string("age_range");
        $table->string("sex")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Movement_Freedom', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->string("age_range");
        $table->string("sex")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Age_Women_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("age_range");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Age_Women', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("age_range");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Type_Women_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
      Schema::create('OADH_INT_Type_Women', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_State_Women_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_State_Women', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("department");
        $table->string("municipality")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Type_Sexual_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Type_Sexual', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("department");
        $table->string("municipality")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Gender_Sexual_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("sex")->nullable();
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Gender_Sexual', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("sex")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Age_Sexual_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("age_range");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Age_Sexual', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("crime");
        $table->string("age_range");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_State_Injuries_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        // $table->integer('record')->nullable();
        $table->integer("year");
        $table->string("department");
        $table->string("municipality")->nullable();
        // $table->double("total");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_State_Injuries', function (Blueprint $table)
      {
        $table->increments('id');
        // $table->integer('record')->nullable();
        $table->integer("year");
        $table->string("department");
        $table->string("municipality")->nullable();
        // $table->double("total");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Age_Injuries_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("sex")->nullable();
        $table->string("age_range");
        $table->string("crime");
        // $table->double("total");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Age_Injuries', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("sex")->nullable();
        $table->string("age_range");
        $table->string("crime");
        // $table->double("total");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Schedule_Injuries_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("weapon_type");
        // $table->string("aggressor_type");
        // $table->string("time");
        // $table->double("total");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Schedule_Injuries', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        $table->string("weapon_type");
        // $table->string("aggressor_type");
        // $table->string("time");
        // $table->double("total");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Agent_Investigation_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("code");
        $table->integer("year");//falta
        $table->string("crime");
        $table->string("category");
        $table->string("sex")->nullable();
        $table->string("place");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->string("state");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Agent_Investigation', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("code");
        $table->integer("year");//falta
        $table->string("crime");
        $table->string("category");
        $table->string("sex")->nullable();
        $table->string("place");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->string("state");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Integrity_Violation_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("year");
        // $table->date("date");
        $table->string("sex")->nullable();
        $table->integer("age");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->string("right_to");
        $table->string("violated_fact");
        $table->string("institutions");
        $table->string("dependencies");
        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_INT_Integrity_Violation', function (Blueprint $table)
      {
        $table->increments('id');
        // $table->date("date");
        $table->integer("year");
        // $table->date("date");
        $table->string("sex")->nullable();
        $table->integer("age");
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->string("right_to");
        $table->string("violated_fact");
        $table->string("institutions");
        $table->string("dependencies");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_INT_Integrity_Violation');
      Schema::dropIfExists('OADH_INT_Integrity_Violation_Temp');

      Schema::dropIfExists('OADH_INT_Agent_Investigation');
      Schema::dropIfExists('OADH_INT_Agent_Investigation_Temp');

      Schema::dropIfExists('OADH_INT_Schedule_Injuries');
      Schema::dropIfExists('OADH_INT_Schedule_Injuries_Temp');

      Schema::dropIfExists('OADH_INT_Age_Injuries');
      Schema::dropIfExists('OADH_INT_Age_Injuries_Temp');

      Schema::dropIfExists('OADH_INT_State_Injuries');
      Schema::dropIfExists('OADH_INT_State_Injuries_Temp');

      Schema::dropIfExists('OADH_INT_Age_Sexual');
      Schema::dropIfExists('OADH_INT_Age_Sexual_Temp');

      Schema::dropIfExists('OADH_INT_Gender_Sexual');
      Schema::dropIfExists('OADH_INT_Gender_Sexual_Temp');

      Schema::dropIfExists('OADH_INT_Type_Sexual');
      Schema::dropIfExists('OADH_INT_Type_Sexual_Temp');

      Schema::dropIfExists('OADH_INT_State_Women');
      Schema::dropIfExists('OADH_INT_State_Women_Temp');

      Schema::dropIfExists('OADH_INT_Type_Women');
      Schema::dropIfExists('OADH_INT_Type_Women_Temp');

      Schema::dropIfExists('OADH_INT_Age_Women');
      Schema::dropIfExists('OADH_INT_Age_Women_Temp');

      Schema::dropIfExists('OADH_INT_Movement_Freedom');
      Schema::dropIfExists('OADH_INT_Movement_Freedom_Temp');

      Schema::dropIfExists('OADH_INT_Age_Trafficking');
      Schema::dropIfExists('OADH_INT_Age_Trafficking_Temp');

      Schema::dropIfExists('OADH_INT_Type_Trafficking');
      Schema::dropIfExists('OADH_INT_Type_Trafficking_Temp');

      Schema::dropIfExists('OADH_INT_Gender_Trafficking');
      Schema::dropIfExists('OADH_INT_Gender_Trafficking_Temp');

      Schema::dropIfExists('OADH_INT_Working_Officials');
      Schema::dropIfExists('OADH_INT_Working_Officials_Temp');

      Schema::dropIfExists('OADH_INT_Crimes_Accused');
      Schema::dropIfExists('OADH_INT_Crimes_Accused_Temp');

      Schema::dropIfExists('OADH_INT_Crimes_Victims');
      Schema::dropIfExists('OADH_INT_Crimes_Victims_Temp');
    }
}
