<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('OADH_LIFE_Officers_Investigated_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('record')->nullable();;//EXPEDIENTE nullable
        $table->string('investigation_type');//TIPO DE INVESTIGACION
        $table->integer('year');//AÑO
        $table->string('crime');//DELITO
        $table->string('category')->nullable();//CATEGORIA
        $table->string('position')->nullable();//CARGO QUE OCUPA EL MIEMBRO POLICIAL
        $table->string('officer_sex')->nullable();//SEXO
        $table->string('location_place')->nullable();//LUGAR EN EL QUE ESTA DESTACADO
        $table->string('location_department',60)->nullable();// nullable DEPARTAMENTO
        $table->string('location_municipality',60)->nullable(); //MUNICIPIO
        $table->date('date')->nullable();//nullable Fecha
        $table->string('act_place')->nullable();// )->nullable(); LUGAR DEL HECHO
        $table->string('act_department', 60)->nullable();// )->nullable();DEPARTAMENTO
        $table->string('act_municipality',60)->nullable();//MUNICIPIO
        $table->string('weapon_type')->nullable();// )->nullable();TIPO DE ARMA UTILIZADA
        $table->tinyInteger('victim_quantity')->nullable();// )->nullable();CANTIDAD DE VICTIMAS
        $table->string('victim_sex')->nullable();// )->nullable();SEXO
        $table->string('criminal_justice_process', 60)->nullable();// )->nullable();ESTADO DEL PROCESO PENAL

        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_LIFE_Officers_Investigated', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('record')->nullable();;//EXPEDIENTE nullable
        $table->string('investigation_type');//TIPO DE INVESTIGACION
        $table->integer('year');//AÑO
        $table->string('crime');//DELITO
        $table->string('category')->nullable();//CATEGORIA
        $table->string('position')->nullable();//CARGO QUE OCUPA EL MIEMBRO POLICIAL
        $table->string('officer_sex')->nullable();//SEXO
        $table->string('location_place')->nullable();//LUGAR EN EL QUE ESTA DESTACADO
        $table->string('location_department',60)->nullable();// nullable DEPARTAMENTO
        $table->string('location_municipality',60)->nullable(); //MUNICIPIO
        $table->date('date')->nullable();//nullable Fecha
        $table->string('act_place')->nullable();// )->nullable(); LUGAR DEL HECHO
        $table->string('act_department', 60)->nullable();// )->nullable();DEPARTAMENTO
        $table->string('act_municipality',60)->nullable();//MUNICIPIO
        $table->string('weapon_type')->nullable();// )->nullable();TIPO DE ARMA UTILIZADA
        $table->tinyInteger('victim_quantity')->nullable();// )->nullable();CANTIDAD DE VICTIMAS
        $table->string('victim_sex')->nullable();// )->nullable();SEXO
        $table->string('criminal_justice_process', 60)->nullable();// )->nullable();ESTADO DEL PROCESO PENAL

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      //Del Archivo Homicidios - BD.xlsx
      Schema::create('OADH_LIFE_Homicides_Rates_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer('record')->nullable(); //numero de registro
        $table->integer('year'); //anio de la agresion
        $table->string('month',25); //mes de la agresion
        $table->integer('day'); //dia de la agresion
        $table->time('time_aggression')->nullable(); //hora de la agresion
        $table->integer('age')->nulleable(); //edad
        $table->string('sex',10); //sexo
        $table->string('occupation',60); //ocupacion
        $table->string('municipality',60); //municipio
        $table->string('department',25); //departamento
        $table->string('weapon_type',60); //tipo de arma

        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
       });

       Schema::create('OADH_LIFE_Homicides_Rates', function (Blueprint $table)
       {
        $table->increments('id');
        $table->integer('record'); //numero de registro
        $table->integer('year'); //anio de la agresion
        $table->string('month',25); //mes de la agresion
        $table->integer('day'); //dia de la agresion
        $table->time('time_aggression')->nullable(); //hora de la agresion
        $table->integer('age')->nulleable(); //edad
        $table->string('sex',10); //sexo
        $table->string('occupation',60); //ocupacion
        $table->string('municipality',60); //municipio
        $table->string('department',25); //departamento
        $table->string('weapon_type',60);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_LIFE_Officers_Investigated_Temp');
      Schema::dropIfExists('OADH_LIFE_Officers_Investigated');
      Schema::dropIfExists('OADH_LIFE_Homicides_Rates_Temp');
      Schema::dropIfExists('OADH_LIFE_Homicides_Rates');

    }
}
