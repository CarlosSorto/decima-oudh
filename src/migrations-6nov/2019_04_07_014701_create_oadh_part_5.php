<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('OADH_POP_Age_Population_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("age");
        $table->integer("year");
        $table->integer("population");

        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_POP_Age_Population', function (Blueprint $table)
      {
        $table->increments('id');
        $table->integer("age");
        $table->integer("year");
        $table->integer("population");

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_POP_Territory_Population_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->integer("year");
        $table->integer("population");
        $table->string("sex")->nullable();

        $table->char("status", 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_POP_Territory_Population', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string("department");
        $table->string("municipality")->nullable();
        $table->integer("year");
        $table->integer("population");
        $table->string("sex")->nullable();

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_POP_Age_Population');
      Schema::dropIfExists('OADH_POP_Age_Population_Temp');
      Schema::dropIfExists('OADH_POP_Territory_Population');
      Schema::dropIfExists('OADH_POP_Territory_Population_Temp');

    }
}
