<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOadhPart7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Del Archivo Agresiones Ilegitimas.xlsx hoja Agresiones Ilegitimas
      Schema::create('OADH_LIFE_Aggression_Weapon_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('department',25); //departamento
        $table->string('municipality',60); //municipio
        $table->string('day',15); //dia
        $table->integer('day_number'); //dia en formato numerico
        $table->integer('month'); //mes
        $table->integer('year'); //anio
        $table->date('date'); //fecha
        $table->time('time')->nullable(); //hora
        $table->string('gang',60); //pandilla o delincuente comun
        $table->string('division',60)->nullable(); //division a la que pertenece
        $table->integer('aggressions'); //agresiones ilegitimas C/AF
        $table->integer('deceased'); //fallecidos

        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_LIFE_Aggression_Weapon', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('department',25); //departamento
        $table->string('municipality',60); //municipio
        $table->string('day',15); //dia
        $table->integer('day_number'); //dia en formato numerico
        $table->integer('month'); //mes
        $table->integer('year'); //anio
        $table->date('date'); //fecha
        $table->time('time')->nullable(); //hora
        $table->string('gang',60); //pandilla o delincuente comun
        $table->string('division',60)->nullable(); //division a la que pertenece
        $table->integer('aggressions'); //agresiones ilegitimas C/AF
        $table->integer('deceased'); //fallecidos


        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      //Del Archivo Agresiones Ilegitimas.xlsx hoja AgresionesIlegitimas(PolFalle)
      Schema::create('OADH_LIFE_Aggression_Death_Temp', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('department',25); //departamento
        $table->string('municipality',60); //municipio
        $table->string('day',15); //dia
        $table->integer('day_number'); //dia en formato numerico
        $table->integer('month'); //mes
        $table->integer('year'); //anio
        $table->date('date'); //fecha
        $table->time('time')->nullable(); //hora
        $table->string('gang',60); //pandilla o delincuente comun
        $table->string('division',60)->nullable(); //division a la que pertenece
        $table->integer('deceased_policemen'); //policias fallecidos
        $table->integer('aggressions'); //agresiones ilegitimas

        $table->char('status', 1);

        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

      Schema::create('OADH_LIFE_Aggression_Death', function (Blueprint $table)
      {
        $table->increments('id');
        $table->string('department',25); //departamento
        $table->string('municipality',60); //municipio
        $table->string('day',15); //dia
        $table->integer('day_number'); //dia en formato numerico
        $table->integer('month'); //mes
        $table->integer('year'); //anio
        $table->date('date'); //fecha
        $table->time('time')->nullable(); //hora
        $table->string('gang',60); //pandilla o delincuente comun
        $table->string('division',60)->nullable(); //division a la que pertenece
        $table->integer('deceased_policemen'); //policias fallecidos
        $table->integer('aggressions'); //agresiones ilegitimas


        $table->unsignedInteger('file_id')->nullable();
        $table->foreign('file_id')->references('id')->on('FILE_File');

        $table->unsignedInteger('organization_id')->index();

        $table->timestamps();
        $table->softDeletes();
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists('OADH_LIFE_Aggression_Weapon');
      Schema::dropIfExists('OADH_LIFE_Aggression_Weapon_Temp');
      Schema::dropIfExists('OADH_LIFE_Aggression_Death');
      Schema::dropIfExists('OADH_LIFE_Aggression_Death_Temp');

    }
}
