-- Cambios para crear carpetas
ALTER TABLE public."FILE_File"
    ALTER COLUMN is_public DROP NOT NULL;
ALTER TABLE public."FILE_File"
    ALTER COLUMN key TYPE character varying (255) COLLATE pg_catalog."default";
ALTER TABLE public."FILE_File"
    ALTER COLUMN key DROP NOT NULL;
ALTER TABLE public."FILE_File"
    ALTER COLUMN icon TYPE character varying (20) COLLATE pg_catalog."default";
ALTER TABLE public."FILE_File"
    ALTER COLUMN icon DROP NOT NULL;
ALTER TABLE public."FILE_File"
    ALTER COLUMN icon_html TYPE character varying (255) COLLATE pg_catalog."default";
ALTER TABLE public."FILE_File"
    ALTER COLUMN icon_html DROP NOT NULL;
ALTER TABLE public."FILE_File"
    ALTER COLUMN parent_file_id DROP NOT NULL;
ALTER TABLE public."FILE_File"
    ALTER COLUMN system_reference_type TYPE character varying (40) COLLATE pg_catalog."default";
ALTER TABLE public."FILE_File"
    ALTER COLUMN system_reference_type DROP NOT NULL;
ALTER TABLE public."FILE_File"
    ALTER COLUMN system_reference_id DROP NOT NULL;

ALTER TABLE public."OADH_People"
    ADD COLUMN sexual_diversity varchar;
