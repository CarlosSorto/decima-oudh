--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2019-04-29 15:54:09 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16386)
-- Name: FILE_File; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."FILE_File" (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    type character(1) NOT NULL,
    system_type character varying(100),
    system_route character varying(255),
    url text,
    url_html text,
    is_public boolean,
    key character varying(255),
    icon character varying(20),
    icon_html character varying(255),
    width double precision,
    height double precision,
    parent_file_id integer,
    system_reference_type character varying(40),
    system_reference_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."FILE_File" OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16392)
-- Name: FILE_File_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."FILE_File_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."FILE_File_id_seq" OWNER TO postgres;

--
-- TOC entry 3559 (class 0 OID 0)
-- Dependencies: 197
-- Name: FILE_File_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."FILE_File_id_seq" OWNED BY public."FILE_File".id;


--
-- TOC entry 249 (class 1259 OID 26294)
-- Name: OADH_FREE_Forced_Disappearances_Temps; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Forced_Disappearances_Temps" (
    id integer NOT NULL,
    year integer NOT NULL,
    gender character(1) NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    profession character varying(255) NOT NULL,
    crime character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Forced_Disappearances_Temps" OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 26292)
-- Name: OADH_FREE_Forced_Disappearance_Temps_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Forced_Disappearance_Temps_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Forced_Disappearance_Temps_id_seq" OWNER TO postgres;

--
-- TOC entry 3560 (class 0 OID 0)
-- Dependencies: 248
-- Name: OADH_FREE_Forced_Disappearance_Temps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Forced_Disappearance_Temps_id_seq" OWNED BY public."OADH_FREE_Forced_Disappearances_Temps".id;


--
-- TOC entry 198 (class 1259 OID 16394)
-- Name: ORG_Organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ORG_Organization" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    street1 character varying(255),
    street2 character varying(255),
    city_name character varying(255),
    state_name character varying(255),
    zip_code character varying(20),
    web_site character varying(255),
    phone_number character varying(60),
    fax character varying(60),
    email character varying(255),
    tax_id character varying(255),
    company_registration character varying(255),
    commercial_trade character varying(255),
    logo_url text,
    cost_price_precision smallint DEFAULT '2'::smallint NOT NULL,
    discount_precision smallint DEFAULT '2'::smallint NOT NULL,
    database_connection_name character varying(60) NOT NULL,
    api_token character varying(60),
    sale_point_quantity smallint DEFAULT '1'::smallint NOT NULL,
    country_id integer NOT NULL,
    currency_id integer,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."ORG_Organization" OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16403)
-- Name: ORG_Organization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."ORG_Organization_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ORG_Organization_id_seq" OWNER TO postgres;

--
-- TOC entry 3561 (class 0 OID 0)
-- Dependencies: 199
-- Name: ORG_Organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."ORG_Organization_id_seq" OWNED BY public."ORG_Organization".id;


--
-- TOC entry 200 (class 1259 OID 16405)
-- Name: SEC_Failed_Jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Failed_Jobs" (
    id integer NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    failed_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public."SEC_Failed_Jobs" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16411)
-- Name: SEC_Failed_Jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Failed_Jobs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Failed_Jobs_id_seq" OWNER TO postgres;

--
-- TOC entry 3562 (class 0 OID 0)
-- Dependencies: 201
-- Name: SEC_Failed_Jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Failed_Jobs_id_seq" OWNED BY public."SEC_Failed_Jobs".id;


--
-- TOC entry 202 (class 1259 OID 16413)
-- Name: SEC_File; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_File" (
    id integer NOT NULL,
    file_id integer NOT NULL,
    key character varying(255) NOT NULL,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_File" OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16416)
-- Name: SEC_File_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_File_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_File_id_seq" OWNER TO postgres;

--
-- TOC entry 3563 (class 0 OID 0)
-- Dependencies: 203
-- Name: SEC_File_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_File_id_seq" OWNED BY public."SEC_File".id;


--
-- TOC entry 204 (class 1259 OID 16418)
-- Name: SEC_Journal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Journal" (
    id integer NOT NULL,
    journalized_id integer NOT NULL,
    journalized_type character varying(30) NOT NULL,
    user_id integer NOT NULL,
    organization_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Journal" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16421)
-- Name: SEC_Journal_Detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Journal_Detail" (
    id integer NOT NULL,
    field character varying(60),
    field_lang_key character varying(100),
    note character varying(255),
    old_value text,
    new_value text,
    journal_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Journal_Detail" OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16427)
-- Name: SEC_Journal_Detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Journal_Detail_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Journal_Detail_id_seq" OWNER TO postgres;

--
-- TOC entry 3564 (class 0 OID 0)
-- Dependencies: 206
-- Name: SEC_Journal_Detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Journal_Detail_id_seq" OWNED BY public."SEC_Journal_Detail".id;


--
-- TOC entry 207 (class 1259 OID 16429)
-- Name: SEC_Journal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Journal_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Journal_id_seq" OWNER TO postgres;

--
-- TOC entry 3565 (class 0 OID 0)
-- Dependencies: 207
-- Name: SEC_Journal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Journal_id_seq" OWNED BY public."SEC_Journal".id;


--
-- TOC entry 208 (class 1259 OID 16431)
-- Name: SEC_Menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Menu" (
    id integer NOT NULL,
    name character varying(300) NOT NULL,
    lang_key character varying(100),
    url character varying(300),
    action_button_id character varying(60) DEFAULT ''::character varying NOT NULL,
    action_lang_key character varying(100),
    icon character varying(60),
    parent_id integer,
    created_by integer NOT NULL,
    module_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Menu" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16438)
-- Name: SEC_Menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Menu_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Menu_id_seq" OWNER TO postgres;

--
-- TOC entry 3566 (class 0 OID 0)
-- Dependencies: 209
-- Name: SEC_Menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Menu_id_seq" OWNED BY public."SEC_Menu".id;


--
-- TOC entry 210 (class 1259 OID 16440)
-- Name: SEC_Module; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Module" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    lang_key character varying(100),
    icon character varying(60),
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Module" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16443)
-- Name: SEC_Module_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Module_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Module_id_seq" OWNER TO postgres;

--
-- TOC entry 3567 (class 0 OID 0)
-- Dependencies: 211
-- Name: SEC_Module_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Module_id_seq" OWNED BY public."SEC_Module".id;


--
-- TOC entry 212 (class 1259 OID 16445)
-- Name: SEC_Password_Reminders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Password_Reminders" (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public."SEC_Password_Reminders" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16451)
-- Name: SEC_Permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Permission" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    key character varying(60) NOT NULL,
    lang_key character varying(100),
    url character varying(300),
    alias_url character varying(300),
    action_button_id character varying(60),
    action_lang_key character varying(100),
    icon character varying(60),
    shortcut_icon character varying(60),
    is_only_shortcut boolean DEFAULT false NOT NULL,
    is_dashboard_shortcut_visible boolean DEFAULT false NOT NULL,
    hidden boolean DEFAULT false NOT NULL,
    menu_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Permission" OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16460)
-- Name: SEC_Permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Permission_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Permission_id_seq" OWNER TO postgres;

--
-- TOC entry 3568 (class 0 OID 0)
-- Dependencies: 214
-- Name: SEC_Permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Permission_id_seq" OWNED BY public."SEC_Permission".id;


--
-- TOC entry 215 (class 1259 OID 16462)
-- Name: SEC_Role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Role" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    lang_key character varying(100),
    description character varying(255),
    organization_id integer,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Role" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16465)
-- Name: SEC_Role_Menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Role_Menu" (
    id integer NOT NULL,
    role_id integer NOT NULL,
    menu_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Role_Menu" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16468)
-- Name: SEC_Role_Menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Role_Menu_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Role_Menu_id_seq" OWNER TO postgres;

--
-- TOC entry 3569 (class 0 OID 0)
-- Dependencies: 217
-- Name: SEC_Role_Menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Role_Menu_id_seq" OWNED BY public."SEC_Role_Menu".id;


--
-- TOC entry 218 (class 1259 OID 16470)
-- Name: SEC_Role_Permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Role_Permission" (
    id integer NOT NULL,
    role_id integer NOT NULL,
    permission_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Role_Permission" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16473)
-- Name: SEC_Role_Permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Role_Permission_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Role_Permission_id_seq" OWNER TO postgres;

--
-- TOC entry 3570 (class 0 OID 0)
-- Dependencies: 219
-- Name: SEC_Role_Permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Role_Permission_id_seq" OWNED BY public."SEC_Role_Permission".id;


--
-- TOC entry 220 (class 1259 OID 16475)
-- Name: SEC_Role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Role_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Role_id_seq" OWNER TO postgres;

--
-- TOC entry 3571 (class 0 OID 0)
-- Dependencies: 220
-- Name: SEC_Role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Role_id_seq" OWNED BY public."SEC_Role".id;


--
-- TOC entry 221 (class 1259 OID 16477)
-- Name: SEC_User; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User" (
    id integer NOT NULL,
    firstname character varying(60) NOT NULL,
    lastname character varying(60) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    is_active boolean NOT NULL,
    is_admin boolean NOT NULL,
    timezone character varying(60),
    activation_code character varying(255),
    activated_at timestamp(0) without time zone,
    last_login timestamp(0) without time zone,
    popovers_shown boolean DEFAULT false NOT NULL,
    multiple_organization_popover_shown boolean DEFAULT false NOT NULL,
    remember_token character varying(100),
    created_by integer,
    default_organization integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16485)
-- Name: SEC_User_Menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User_Menu" (
    id integer NOT NULL,
    is_assigned boolean NOT NULL,
    user_id integer NOT NULL,
    menu_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User_Menu" OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16488)
-- Name: SEC_User_Menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_Menu_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_Menu_id_seq" OWNER TO postgres;

--
-- TOC entry 3572 (class 0 OID 0)
-- Dependencies: 223
-- Name: SEC_User_Menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_Menu_id_seq" OWNED BY public."SEC_User_Menu".id;


--
-- TOC entry 224 (class 1259 OID 16490)
-- Name: SEC_User_Organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User_Organization" (
    id integer NOT NULL,
    user_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User_Organization" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16493)
-- Name: SEC_User_Organization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_Organization_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_Organization_id_seq" OWNER TO postgres;

--
-- TOC entry 3573 (class 0 OID 0)
-- Dependencies: 225
-- Name: SEC_User_Organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_Organization_id_seq" OWNED BY public."SEC_User_Organization".id;


--
-- TOC entry 226 (class 1259 OID 16495)
-- Name: SEC_User_Permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User_Permission" (
    id integer NOT NULL,
    is_assigned boolean NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User_Permission" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16498)
-- Name: SEC_User_Permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_Permission_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_Permission_id_seq" OWNER TO postgres;

--
-- TOC entry 3574 (class 0 OID 0)
-- Dependencies: 227
-- Name: SEC_User_Permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_Permission_id_seq" OWNED BY public."SEC_User_Permission".id;


--
-- TOC entry 228 (class 1259 OID 16500)
-- Name: SEC_User_Role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User_Role" (
    id integer NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User_Role" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16503)
-- Name: SEC_User_Role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_Role_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_Role_id_seq" OWNER TO postgres;

--
-- TOC entry 3575 (class 0 OID 0)
-- Dependencies: 229
-- Name: SEC_User_Role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_Role_id_seq" OWNED BY public."SEC_User_Role".id;


--
-- TOC entry 230 (class 1259 OID 16505)
-- Name: SEC_User_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_id_seq" OWNER TO postgres;

--
-- TOC entry 3576 (class 0 OID 0)
-- Dependencies: 230
-- Name: SEC_User_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_id_seq" OWNED BY public."SEC_User".id;


--
-- TOC entry 231 (class 1259 OID 16507)
-- Name: SYS_Account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Account" (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    parent_key character varying(255),
    name character varying(255) NOT NULL,
    balance_type character(1) NOT NULL,
    account_type_key character(1) NOT NULL,
    is_group boolean DEFAULT false NOT NULL,
    is_general_ledger_account boolean DEFAULT false NOT NULL,
    account_chart_type_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Account" OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 16515)
-- Name: SYS_Account_Chart_Type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Account_Chart_Type" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    url character varying(255) NOT NULL,
    lang_key character varying(100),
    country_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Account_Chart_Type" OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16518)
-- Name: SYS_Account_Chart_Type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Account_Chart_Type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Account_Chart_Type_id_seq" OWNER TO postgres;

--
-- TOC entry 3577 (class 0 OID 0)
-- Dependencies: 233
-- Name: SYS_Account_Chart_Type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Account_Chart_Type_id_seq" OWNED BY public."SYS_Account_Chart_Type".id;


--
-- TOC entry 234 (class 1259 OID 16520)
-- Name: SYS_Account_Type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Account_Type" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    lang_key character varying(100),
    key character(1) NOT NULL,
    pl_bs_category character(1) NOT NULL,
    deferral_method character(1) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Account_Type" OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 16523)
-- Name: SYS_Account_Type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Account_Type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Account_Type_id_seq" OWNER TO postgres;

--
-- TOC entry 3578 (class 0 OID 0)
-- Dependencies: 235
-- Name: SYS_Account_Type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Account_Type_id_seq" OWNED BY public."SYS_Account_Type".id;


--
-- TOC entry 236 (class 1259 OID 16525)
-- Name: SYS_Account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Account_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Account_id_seq" OWNER TO postgres;

--
-- TOC entry 3579 (class 0 OID 0)
-- Dependencies: 236
-- Name: SYS_Account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Account_id_seq" OWNED BY public."SYS_Account".id;


--
-- TOC entry 237 (class 1259 OID 16527)
-- Name: SYS_Country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Country" (
    id integer NOT NULL,
    iso_code character varying(3) NOT NULL,
    name character varying(60) NOT NULL,
    region_name character varying(60) NOT NULL,
    region_lang_key character varying(100) NOT NULL,
    tax_id_name character varying(255),
    tax_id_abbreviation character varying(255),
    registration_number_name character varying(255),
    registration_number_abbreviation character varying(255),
    single_identity_document_number_name character varying(255),
    single_identity_document_number_abbreviation character varying(255),
    social_security_number_name character varying(255),
    social_security_number_abbreviation character varying(255),
    single_previsional_number_name character varying(255),
    single_previsional_number_abbreviation character varying(255),
    currency_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Country" OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 16533)
-- Name: SYS_Country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Country_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Country_id_seq" OWNER TO postgres;

--
-- TOC entry 3580 (class 0 OID 0)
-- Dependencies: 238
-- Name: SYS_Country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Country_id_seq" OWNED BY public."SYS_Country".id;


--
-- TOC entry 239 (class 1259 OID 16535)
-- Name: SYS_Currency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Currency" (
    id integer NOT NULL,
    iso_code character varying(3) NOT NULL,
    symbol character varying(10) NOT NULL,
    name character varying(60) NOT NULL,
    standard_precision smallint NOT NULL,
    costing_precision smallint NOT NULL,
    price_precision smallint NOT NULL,
    currency_symbol_at_the_right boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Currency" OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 16538)
-- Name: SYS_Currency_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Currency_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Currency_id_seq" OWNER TO postgres;

--
-- TOC entry 3581 (class 0 OID 0)
-- Dependencies: 240
-- Name: SYS_Currency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Currency_id_seq" OWNED BY public."SYS_Currency".id;


--
-- TOC entry 241 (class 1259 OID 16540)
-- Name: SYS_Region; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Region" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    region_code character varying(4),
    region_lang_key character varying(100),
    country_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Region" OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 16543)
-- Name: SYS_Region_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Region_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Region_id_seq" OWNER TO postgres;

--
-- TOC entry 3582 (class 0 OID 0)
-- Dependencies: 242
-- Name: SYS_Region_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Region_id_seq" OWNED BY public."SYS_Region".id;


--
-- TOC entry 243 (class 1259 OID 16545)
-- Name: SYS_Voucher_Type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Voucher_Type" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    lang_key character varying(100),
    key character(1),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Voucher_Type" OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 16548)
-- Name: SYS_Voucher_Type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Voucher_Type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Voucher_Type_id_seq" OWNER TO postgres;

--
-- TOC entry 3583 (class 0 OID 0)
-- Dependencies: 244
-- Name: SYS_Voucher_Type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Voucher_Type_id_seq" OWNED BY public."SYS_Voucher_Type".id;


--
-- TOC entry 245 (class 1259 OID 16550)
-- Name: cache; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cache (
    key character varying(255) NOT NULL,
    value text NOT NULL,
    expiration integer NOT NULL
);


ALTER TABLE public.cache OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 16556)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 16559)
-- Name: sessions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sessions (
    id character varying(255) NOT NULL,
    user_id integer,
    ip_address character varying(45),
    user_agent text,
    payload text NOT NULL,
    last_activity integer NOT NULL
);


ALTER TABLE public.sessions OWNER TO postgres;

--
-- TOC entry 3210 (class 2604 OID 16565)
-- Name: FILE_File id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."FILE_File" ALTER COLUMN id SET DEFAULT nextval('public."FILE_File_id_seq"'::regclass);


--
-- TOC entry 3245 (class 2604 OID 26297)
-- Name: OADH_FREE_Forced_Disappearances_Temps id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearances_Temps" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Forced_Disappearance_Temps_id_seq"'::regclass);


--
-- TOC entry 3214 (class 2604 OID 16566)
-- Name: ORG_Organization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization" ALTER COLUMN id SET DEFAULT nextval('public."ORG_Organization_id_seq"'::regclass);


--
-- TOC entry 3215 (class 2604 OID 16567)
-- Name: SEC_Failed_Jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Failed_Jobs" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Failed_Jobs_id_seq"'::regclass);


--
-- TOC entry 3216 (class 2604 OID 16568)
-- Name: SEC_File id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_File" ALTER COLUMN id SET DEFAULT nextval('public."SEC_File_id_seq"'::regclass);


--
-- TOC entry 3217 (class 2604 OID 16569)
-- Name: SEC_Journal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Journal_id_seq"'::regclass);


--
-- TOC entry 3218 (class 2604 OID 16570)
-- Name: SEC_Journal_Detail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal_Detail" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Journal_Detail_id_seq"'::regclass);


--
-- TOC entry 3220 (class 2604 OID 16571)
-- Name: SEC_Menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Menu_id_seq"'::regclass);


--
-- TOC entry 3221 (class 2604 OID 16572)
-- Name: SEC_Module id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Module" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Module_id_seq"'::regclass);


--
-- TOC entry 3225 (class 2604 OID 16573)
-- Name: SEC_Permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Permission" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Permission_id_seq"'::regclass);


--
-- TOC entry 3226 (class 2604 OID 16574)
-- Name: SEC_Role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Role_id_seq"'::regclass);


--
-- TOC entry 3227 (class 2604 OID 16575)
-- Name: SEC_Role_Menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Role_Menu_id_seq"'::regclass);


--
-- TOC entry 3228 (class 2604 OID 16576)
-- Name: SEC_Role_Permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Role_Permission_id_seq"'::regclass);


--
-- TOC entry 3231 (class 2604 OID 16577)
-- Name: SEC_User id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_id_seq"'::regclass);


--
-- TOC entry 3232 (class 2604 OID 16578)
-- Name: SEC_User_Menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_Menu_id_seq"'::regclass);


--
-- TOC entry 3233 (class 2604 OID 16579)
-- Name: SEC_User_Organization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_Organization_id_seq"'::regclass);


--
-- TOC entry 3234 (class 2604 OID 16580)
-- Name: SEC_User_Permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_Permission_id_seq"'::regclass);


--
-- TOC entry 3235 (class 2604 OID 16581)
-- Name: SEC_User_Role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_Role_id_seq"'::regclass);


--
-- TOC entry 3238 (class 2604 OID 16582)
-- Name: SYS_Account id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Account_id_seq"'::regclass);


--
-- TOC entry 3239 (class 2604 OID 16583)
-- Name: SYS_Account_Chart_Type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Chart_Type" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Account_Chart_Type_id_seq"'::regclass);


--
-- TOC entry 3240 (class 2604 OID 16584)
-- Name: SYS_Account_Type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Type" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Account_Type_id_seq"'::regclass);


--
-- TOC entry 3241 (class 2604 OID 16585)
-- Name: SYS_Country id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Country" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Country_id_seq"'::regclass);


--
-- TOC entry 3242 (class 2604 OID 16586)
-- Name: SYS_Currency id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Currency" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Currency_id_seq"'::regclass);


--
-- TOC entry 3243 (class 2604 OID 16587)
-- Name: SYS_Region id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Region" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Region_id_seq"'::regclass);


--
-- TOC entry 3244 (class 2604 OID 16588)
-- Name: SYS_Voucher_Type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Voucher_Type" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Voucher_Type_id_seq"'::regclass);


--
-- TOC entry 3500 (class 0 OID 16386)
-- Dependencies: 196
-- Data for Name: FILE_File; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."FILE_File" (id, name, type, system_type, system_route, url, url_html, is_public, key, icon, icon_html, width, height, parent_file_id, system_reference_type, system_reference_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	Ejemplos	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-03-24 18:50:23	2019-03-24 18:50:23	\N
7	estadocuenta6840010153.xls	A	spreadsheet	organizations/1/estadocuenta6840010153.xls	http://localhost:8000/files/file-manager/serve/private/$2y$10$Bmil3IKMZFdK0DpY1fgFeNwSJeYbGpbGDO9e8AfGIT8SWR3yYquu	<a href="http://localhost:8000/files/file-manager/serve/private/$2y$10$Bmil3IKMZFdK0DpY1fgFeNwSJeYbGpbGDO9e8AfGIT8SWR3yYquu" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size: 2em;color:#1E448B;"></i></a>	f	$2y$10$Bmil3IKMZFdK0DpY1fgFeNwSJeYbGpbGDO9e8AfGIT8SWR3yYquu	fa fa-file-excel-o	<i class="fa fa-file-excel-o" aria-hidden="true" style="font-size: 2em;color:#9FCC23;"></i>	\N	\N	1	\N	\N	1	2019-03-24 18:51:38	2019-03-24 18:51:38	\N
8	habeas_corpus01.xlsx	A	spreadsheet	organizations/1/habeas_corpus01.xlsx	http://localhost:8000/files/file-manager/serve/private/$2y$10$f1ud9QuojiT3G7T5HveGOVvvWZJoC41Z26VWlQnL7NTijj.kPE.	<a href="http://localhost:8000/files/file-manager/serve/private/$2y$10$f1ud9QuojiT3G7T5HveGOVvvWZJoC41Z26VWlQnL7NTijj.kPE." target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size: 2em;color:#1E448B;"></i></a>	f	$2y$10$f1ud9QuojiT3G7T5HveGOVvvWZJoC41Z26VWlQnL7NTijj.kPE.	fa fa-file-excel-o	<i class="fa fa-file-excel-o" aria-hidden="true" style="font-size: 2em;color:#9FCC23;"></i>	\N	\N	8	\N	\N	1	2019-03-29 11:58:20	2019-03-29 11:58:20	\N
15	test	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-16 02:33:38	2019-04-16 02:38:04	\N
16	Habeas Corpus	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-16 02:38:11	2019-04-16 02:38:11	\N
18	habeas_corpus02.xlsx	A	spreadsheet	organizations/1/habeas_corpus02.xlsx	http://localhost:8000/files/file-manager/serve/private/$2y$10$VEHDrOWxYwEKJBYMJww4YOyqAtgJbY6jDDiPsq1RHfgx0lUdaSoC	<a href="http://localhost:8000/files/file-manager/serve/private/$2y$10$VEHDrOWxYwEKJBYMJww4YOyqAtgJbY6jDDiPsq1RHfgx0lUdaSoC" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size: 2em;color:#1E448B;"></i></a>	f	$2y$10$VEHDrOWxYwEKJBYMJww4YOyqAtgJbY6jDDiPsq1RHfgx0lUdaSoC	fa fa-file-excel-o	<i class="fa fa-file-excel-o" aria-hidden="true" style="font-size: 2em;color:#9FCC23;"></i>	\N	\N	16	\N	\N	1	2019-04-25 11:10:52	2019-04-25 11:10:52	\N
39	Capture Orders	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:31:48	2019-04-25 11:31:48	\N
20	Test	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:21:45	2019-04-25 11:21:45	\N
34	02___Personas_detenidas_e_internadas_provisionalmente.xlsx	A	spreadsheet	organizations/1/02___Personas_detenidas_e_internadas_provisionalmente.xlsx	http://localhost:8000/files/file-manager/serve/private/$2y$10$jRJEVHuJVWtfWQCI5lqhOcA2MkZxU6sJMIl46EfDplAjYxbkxsJK	<a href="http://localhost:8000/files/file-manager/serve/private/$2y$10$jRJEVHuJVWtfWQCI5lqhOcA2MkZxU6sJMIl46EfDplAjYxbkxsJK" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size: 2em;color:#1E448B;"></i></a>	f	$2y$10$jRJEVHuJVWtfWQCI5lqhOcA2MkZxU6sJMIl46EfDplAjYxbkxsJK	fa fa-file-excel-o	<i class="fa fa-file-excel-o" aria-hidden="true" style="font-size: 2em;color:#9FCC23;"></i>	\N	\N	21	\N	\N	1	2019-04-25 11:33:12	2019-04-25 11:33:12	\N
21	People Detained	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:20:18	2019-04-25 11:22:15	\N
22	Criminal Cases Trials	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:25:11	2019-04-25 11:25:11	\N
23	Mass Trials	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:25:50	2019-04-25 11:25:50	\N
24	Procedural Fraud	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:26:27	2019-04-25 11:26:27	\N
25	Disapparences Victims	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:28:46	2019-04-25 11:28:46	\N
26	Accused Liberty Deprivation	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:29:25	2019-04-25 11:29:25	\N
27	Victim Liberty Deprivation	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:29:46	2019-04-25 11:29:46	\N
28	Forced Disappearances	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:30:15	2019-04-25 11:30:15	\N
29	Jails Occupation	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:30:27	2019-04-25 11:30:27	\N
30	Deprived persons	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:30:56	2019-04-25 11:30:56	\N
31	Acute Disease	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:31:12	2019-04-25 11:31:12	\N
32	Chronic Diseases	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-04-25 11:31:29	2019-04-25 11:31:29	\N
\.


--
-- TOC entry 3553 (class 0 OID 26294)
-- Dependencies: 249
-- Data for Name: OADH_FREE_Forced_Disappearances_Temps; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Forced_Disappearances_Temps" (id, year, gender, departament, municipality, profession, crime, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3502 (class 0 OID 16394)
-- Dependencies: 198
-- Data for Name: ORG_Organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."ORG_Organization" (id, name, street1, street2, city_name, state_name, zip_code, web_site, phone_number, fax, email, tax_id, company_registration, commercial_trade, logo_url, cost_price_precision, discount_precision, database_connection_name, api_token, sale_point_quantity, country_id, currency_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	OADH	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2	default	\N	1	202	160	1	2018-12-13 04:00:02	2018-12-13 04:00:02	\N
\.


--
-- TOC entry 3504 (class 0 OID 16405)
-- Dependencies: 200
-- Data for Name: SEC_Failed_Jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Failed_Jobs" (id, connection, queue, payload, failed_at) FROM stdin;
\.


--
-- TOC entry 3506 (class 0 OID 16413)
-- Dependencies: 202
-- Data for Name: SEC_File; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_File" (id, file_id, key, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	7	$2y$10$Bmil3IKMZFdK0DpY1fgFeNwSJeYbGpbGDO9e8AfGIT8SWR3yYquu	1	2019-03-24 18:51:38	2019-03-24 18:51:38	\N
2	8	$2y$10$f1ud9QuojiT3G7T5HveGOVvvWZJoC41Z26VWlQnL7NTijj.kPE.	1	2019-03-29 11:58:20	2019-03-29 11:58:20	\N
3	9	$2y$10$HyKOcTTlnUMMzzZuQGSn0u0wci8AQmILebSnYwYiRKVBBblM.EXa	1	2019-04-02 12:02:17	2019-04-02 12:02:17	\N
4	10	$2y$10$ojjJQ3cJgHQT6N3eyJ5hzu4ye8wnsPqOP6aykO1HAlOL85iYHo0.	1	2019-04-02 12:07:41	2019-04-02 12:07:41	\N
5	11	$2y$10$miIl5DLdyiWzY9tblW9XOD8yqXtg1888rTpuAC05nOFmWhDAd8C	1	2019-04-02 14:28:03	2019-04-02 14:28:03	\N
6	12	$2y$10$v38WuuvjhO70OdSbx9IZ9uP3f4zAZV1TlbZMnopcbqkTVrHNJnNda	1	2019-04-02 17:08:09	2019-04-02 17:08:09	\N
7	13	$2y$10$uFklKSsV0XVErzqcnrF2tOW9nQRMWmas6lKtINkCSC12UztMG	1	2019-04-07 21:32:21	2019-04-07 21:32:21	\N
8	14	$2y$10$cD2shU9DIcU.PvPzBB4zIe0kWauyWU4v30uLMnBww0zLHrNDb3bHi	1	2019-04-08 21:40:54	2019-04-08 21:40:54	\N
9	17	$2y$10$oRlvp3pnfpY.x9Qpaue4z.W5d26Vwk.iqV6VKeLm6Mip5gq3YcCa	1	2019-04-16 03:57:37	2019-04-16 03:57:37	\N
10	18	$2y$10$VEHDrOWxYwEKJBYMJww4YOyqAtgJbY6jDDiPsq1RHfgx0lUdaSoC	1	2019-04-25 11:10:52	2019-04-25 11:10:52	\N
11	34	$2y$10$jRJEVHuJVWtfWQCI5lqhOcA2MkZxU6sJMIl46EfDplAjYxbkxsJK	1	2019-04-25 11:33:12	2019-04-25 11:33:12	\N
\.


--
-- TOC entry 3508 (class 0 OID 16418)
-- Dependencies: 204
-- Data for Name: SEC_Journal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Journal" (id, journalized_id, journalized_type, user_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	1	SEC_User	1	\N	2018-12-13 03:30:36	2018-12-13 03:30:36	\N
2	1	SEC_User	1	\N	2018-12-13 03:33:13	2018-12-13 03:33:13	\N
3	1	SEC_User	1	\N	2018-12-13 03:35:55	2018-12-13 03:35:55	\N
4	1	SEC_User	1	\N	2018-12-13 03:37:19	2018-12-13 03:37:19	\N
5	1	ORG_Organization	1	\N	2018-12-13 04:00:04	2018-12-13 04:00:04	\N
6	1	SEC_User	1	1	2018-12-13 04:00:04	2018-12-13 04:00:04	\N
7	1	SEC_User	1	1	2018-12-13 04:39:01	2018-12-13 04:39:01	\N
8	1	SEC_User	1	1	2018-12-13 05:12:56	2018-12-13 05:12:56	\N
9	1	SEC_User	1	1	2018-12-13 05:13:01	2018-12-13 05:13:01	\N
10	1	SEC_User	1	1	2018-12-13 05:15:05	2018-12-13 05:15:05	\N
11	1	SEC_User	1	1	2018-12-13 05:15:15	2018-12-13 05:15:15	\N
12	1	SEC_User	1	1	2018-12-14 02:15:30	2018-12-14 02:15:30	\N
13	1	SEC_User	1	1	2018-12-14 02:15:31	2018-12-14 02:15:31	\N
14	2	OADH_News	1	1	2019-01-13 00:24:14	2019-01-13 00:24:14	\N
15	2	OADH_News	1	1	2019-01-13 00:30:42	2019-01-13 00:30:42	\N
16	2	OADH_News	1	1	2019-01-13 00:39:34	2019-01-13 00:39:34	\N
17	2	OADH_News	1	1	2019-01-13 00:40:21	2019-01-13 00:40:21	\N
18	2	OADH_News	1	1	2019-01-13 00:41:00	2019-01-13 00:41:00	\N
19	2	OADH_News	1	1	2019-01-13 00:41:23	2019-01-13 00:41:23	\N
20	2	OADH_News	1	1	2019-01-13 00:42:42	2019-01-13 00:42:42	\N
21	2	OADH_News	1	1	2019-01-13 00:42:53	2019-01-13 00:42:53	\N
22	1	OADH_News	1	1	2019-01-15 18:51:33	2019-01-15 18:51:33	\N
23	1	OADH_News	1	1	2019-01-16 00:36:27	2019-01-16 00:36:27	\N
24	1	OADH_News	1	1	2019-01-16 00:39:21	2019-01-16 00:39:21	\N
25	1	OADH_News	1	1	2019-01-16 00:39:34	2019-01-16 00:39:34	\N
26	1	OADH_News	1	1	2019-01-16 00:39:44	2019-01-16 00:39:44	\N
27	1	OADH_News	1	1	2019-01-16 00:40:12	2019-01-16 00:40:12	\N
28	1	OADH_News	1	1	2019-01-16 00:40:26	2019-01-16 00:40:26	\N
29	1	OADH_News	1	1	2019-01-16 02:37:23	2019-01-16 02:37:23	\N
30	1	OADH_News	1	1	2019-01-16 02:43:36	2019-01-16 02:43:36	\N
31	1	OADH_News	1	1	2019-01-16 02:47:01	2019-01-16 02:47:01	\N
32	1	OADH_News	1	1	2019-01-16 02:47:55	2019-01-16 02:47:55	\N
33	1	OADH_News	1	1	2019-01-16 02:48:05	2019-01-16 02:48:05	\N
34	1	OADH_News	1	1	2019-01-16 02:51:46	2019-01-16 02:51:46	\N
35	1	OADH_News	1	1	2019-01-16 02:52:42	2019-01-16 02:52:42	\N
36	1	OADH_News	1	1	2019-01-16 02:57:34	2019-01-16 02:57:34	\N
37	1	OADH_News	1	1	2019-01-16 03:00:09	2019-01-16 03:00:09	\N
38	1	OADH_News	1	1	2019-01-16 03:13:48	2019-01-16 03:13:48	\N
39	1	OADH_News	1	1	2019-01-16 03:13:59	2019-01-16 03:13:59	\N
40	1	OADH_News	1	1	2019-01-16 03:14:53	2019-01-16 03:14:53	\N
41	1	OADH_News	1	1	2019-01-16 03:15:00	2019-01-16 03:15:00	\N
42	1	OADH_News	1	1	2019-01-16 03:27:30	2019-01-16 03:27:30	\N
43	1	OADH_News	1	1	2019-01-16 03:30:58	2019-01-16 03:30:58	\N
44	1	OADH_News	1	1	2019-01-16 03:31:07	2019-01-16 03:31:07	\N
45	1	OADH_News	1	1	2019-01-16 03:31:13	2019-01-16 03:31:13	\N
46	1	OADH_News	1	1	2019-01-16 03:31:19	2019-01-16 03:31:19	\N
47	2	OADH_News	1	1	2019-01-16 03:33:33	2019-01-16 03:33:33	\N
48	3	OADH_News	1	1	2019-01-16 03:37:48	2019-01-16 03:37:48	\N
49	4	OADH_News	1	1	2019-01-16 03:43:05	2019-01-16 03:43:05	\N
50	4	OADH_News	1	1	2019-01-16 03:43:17	2019-01-16 03:43:17	\N
51	4	OADH_News	1	1	2019-01-16 03:43:36	2019-01-16 03:43:36	\N
52	4	OADH_News	1	1	2019-01-16 03:43:51	2019-01-16 03:43:51	\N
53	4	OADH_News	1	1	2019-01-16 03:45:50	2019-01-16 03:45:50	\N
54	4	OADH_News	1	1	2019-01-16 03:46:13	2019-01-16 03:46:13	\N
55	4	OADH_News	1	1	2019-01-16 03:46:21	2019-01-16 03:46:21	\N
56	5	OADH_News	1	1	2019-01-16 18:52:26	2019-01-16 18:52:26	\N
57	5	OADH_News	1	1	2019-01-16 18:55:31	2019-01-16 18:55:31	\N
58	5	OADH_News	1	1	2019-01-16 18:56:12	2019-01-16 18:56:12	\N
59	5	OADH_News	1	1	2019-01-16 18:56:29	2019-01-16 18:56:29	\N
60	5	OADH_News	1	1	2019-01-16 18:56:33	2019-01-16 18:56:33	\N
61	5	OADH_News	1	1	2019-01-16 18:56:44	2019-01-16 18:56:44	\N
62	5	OADH_News	1	1	2019-01-16 18:56:54	2019-01-16 18:56:54	\N
63	5	OADH_News	1	1	2019-01-16 18:56:58	2019-01-16 18:56:58	\N
64	1	OADH_News	1	1	2019-02-13 03:42:42	2019-02-13 03:42:42	\N
65	1	OADH_News	1	1	2019-02-13 03:43:06	2019-02-13 03:43:06	\N
66	1	OADH_News	1	1	2019-02-13 03:43:25	2019-02-13 03:43:25	\N
67	1	OADH_News	1	1	2019-02-15 14:28:43	2019-02-15 14:28:43	\N
68	1	OADH_News	1	1	2019-02-15 14:29:16	2019-02-15 14:29:16	\N
69	1	SEC_User	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
70	5	FILE_File	1	1	2019-03-24 18:50:23	2019-03-24 18:50:23	\N
71	7	FILE_File	1	1	2019-03-24 18:51:38	2019-03-24 18:51:38	\N
72	8	FILE_File	1	1	2019-03-29 11:58:20	2019-03-29 11:58:20	\N
73	9	FILE_File	1	1	2019-04-02 12:02:17	2019-04-02 12:02:17	\N
74	9	FILE_File	1	1	2019-04-02 12:06:57	2019-04-02 12:06:57	\N
75	10	FILE_File	1	1	2019-04-02 12:07:41	2019-04-02 12:07:41	\N
76	10	FILE_File	1	1	2019-04-02 14:27:47	2019-04-02 14:27:47	\N
77	11	FILE_File	1	1	2019-04-02 14:28:03	2019-04-02 14:28:03	\N
78	12	FILE_File	1	1	2019-04-02 17:08:09	2019-04-02 17:08:09	\N
79	12	FILE_File	1	1	2019-04-02 17:15:52	2019-04-02 17:15:52	\N
80	11	FILE_File	1	1	2019-04-05 00:20:15	2019-04-05 00:20:15	\N
81	13	FILE_File	1	1	2019-04-07 21:32:21	2019-04-07 21:32:21	\N
82	13	FILE_File	1	1	2019-04-07 21:48:38	2019-04-07 21:48:38	\N
83	14	FILE_File	1	1	2019-04-08 21:40:54	2019-04-08 21:40:54	\N
84	14	FILE_File	1	1	2019-04-08 21:44:23	2019-04-08 21:44:23	\N
85	1	OADH_News	1	1	2019-04-11 13:54:25	2019-04-11 13:54:25	\N
86	1	OADH_News	1	1	2019-04-11 14:02:28	2019-04-11 14:02:28	\N
87	1	OADH_News	1	1	2019-04-11 14:02:36	2019-04-11 14:02:36	\N
89	15	FILE_File	1	1	2019-04-16 02:33:38	2019-04-16 02:33:38	\N
90	15	FILE_File	1	1	2019-04-16 02:38:04	2019-04-16 02:38:04	\N
91	16	FILE_File	1	1	2019-04-16 02:38:11	2019-04-16 02:38:11	\N
92	17	FILE_File	1	1	2019-04-16 03:57:37	2019-04-16 03:57:37	\N
93	17	FILE_File	1	1	2019-04-16 03:58:24	2019-04-16 03:58:24	\N
94	18	FILE_File	1	1	2019-04-25 11:10:52	2019-04-25 11:10:52	\N
95	19	FILE_File	1	1	2019-04-25 11:20:18	2019-04-25 11:20:18	\N
96	20	FILE_File	1	1	2019-04-25 11:21:45	2019-04-25 11:21:45	\N
97	21	FILE_File	1	1	2019-04-25 11:22:15	2019-04-25 11:22:15	\N
98	22	FILE_File	1	1	2019-04-25 11:25:11	2019-04-25 11:25:11	\N
99	23	FILE_File	1	1	2019-04-25 11:25:50	2019-04-25 11:25:50	\N
100	24	FILE_File	1	1	2019-04-25 11:26:27	2019-04-25 11:26:27	\N
101	25	FILE_File	1	1	2019-04-25 11:28:46	2019-04-25 11:28:46	\N
102	26	FILE_File	1	1	2019-04-25 11:29:25	2019-04-25 11:29:25	\N
103	27	FILE_File	1	1	2019-04-25 11:29:46	2019-04-25 11:29:46	\N
104	28	FILE_File	1	1	2019-04-25 11:30:15	2019-04-25 11:30:15	\N
105	29	FILE_File	1	1	2019-04-25 11:30:27	2019-04-25 11:30:27	\N
106	30	FILE_File	1	1	2019-04-25 11:30:56	2019-04-25 11:30:56	\N
107	31	FILE_File	1	1	2019-04-25 11:31:12	2019-04-25 11:31:12	\N
108	32	FILE_File	1	1	2019-04-25 11:31:29	2019-04-25 11:31:29	\N
109	33	FILE_File	1	1	2019-04-25 11:31:48	2019-04-25 11:31:48	\N
110	34	FILE_File	1	1	2019-04-25 11:33:12	2019-04-25 11:33:12	\N
\.


--
-- TOC entry 3509 (class 0 OID 16421)
-- Dependencies: 205
-- Data for Name: SEC_Journal_Detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Journal_Detail" (id, field, field_lang_key, note, old_value, new_value, journal_id, created_at, updated_at, deleted_at) FROM stdin;
1	Correo electrónico	security/user-management.email	\N	root@decimaerp.com	oadh@decimaerp.com	1	2018-12-13 03:30:36	2018-12-13 03:30:36	\N
2	Nombre	security/user-management.firstname	\N	root	oadh	1	2018-12-13 03:30:36	2018-12-13 03:30:36	\N
3	Apellido	security/user-management.lastname	\N	root	oadh	1	2018-12-13 03:30:37	2018-12-13 03:30:37	\N
4	Zona horaria	security/user-management.timezone	\N	\N	America/El_Salvador	1	2018-12-13 03:30:37	2018-12-13 03:30:37	\N
5	Contraseña	security/user-management.password	\N	**********	**********	1	2018-12-13 03:30:37	2018-12-13 03:30:37	\N
6	Correo electrónico	security/user-management.email	\N	oadh@decimaerp.com	root@decimaerp.com	2	2018-12-13 03:33:13	2018-12-13 03:33:13	\N
7	Nombre	security/user-management.firstname	\N	oadh	root	2	2018-12-13 03:33:14	2018-12-13 03:33:14	\N
8	Apellido	security/user-management.lastname	\N	oadh	root	2	2018-12-13 03:33:14	2018-12-13 03:33:14	\N
9	Contraseña	security/user-management.password	\N	**********	**********	2	2018-12-13 03:33:14	2018-12-13 03:33:14	\N
10	Correo electrónico	security/user-management.email	\N	root@decimaerp.com	hello@decimaerp.com	3	2018-12-13 03:35:55	2018-12-13 03:35:55	\N
11	Contraseña	security/user-management.password	\N	**********	**********	3	2018-12-13 03:35:55	2018-12-13 03:35:55	\N
12	Correo electrónico	security/user-management.email	\N	hello@decimaerp.com	oadh@decimaerp.com	4	2018-12-13 03:37:19	2018-12-13 03:37:19	\N
13	Nombre	security/user-management.firstname	\N	root	oadh	4	2018-12-13 03:37:20	2018-12-13 03:37:20	\N
14	Apellido	security/user-management.lastname	\N	root	oadh	4	2018-12-13 03:37:20	2018-12-13 03:37:20	\N
15	Contraseña	security/user-management.password	\N	**********	**********	4	2018-12-13 03:37:20	2018-12-13 03:37:20	\N
16	\N	\N	Se agregó la organización "OADH" en el sistema.	\N	\N	5	2018-12-13 04:00:04	2018-12-13 04:00:04	\N
17	\N	security/user-management.defaultOrganization	\N	\N	OADH	6	2018-12-13 04:00:05	2018-12-13 04:00:05	\N
18	\N	\N	Se asignó la aplicación "Configuración inicial" a oadh@decimaerp.com.	\N	\N	7	2018-12-13 04:39:02	2018-12-13 04:39:02	\N
19	\N	\N	Se asignó la aplicación "Gestión de derechos humanos" a oadh@decimaerp.com.	\N	\N	7	2018-12-13 04:39:02	2018-12-13 04:39:02	\N
20	\N	\N	Se asignó la aplicación "Gestión de medios" a oadh@decimaerp.com.	\N	\N	7	2018-12-13 04:39:03	2018-12-13 04:39:03	\N
21	\N	\N	Se asignó la aplicación "Monitoreo de medios" a oadh@decimaerp.com.	\N	\N	7	2018-12-13 04:39:03	2018-12-13 04:39:03	\N
22	\N	\N	Se desasignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	8	2018-12-13 05:12:56	2018-12-13 05:12:56	\N
23	\N	\N	Se asignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	9	2018-12-13 05:13:02	2018-12-13 05:13:02	\N
24	\N	\N	Se desasignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	10	2018-12-13 05:15:06	2018-12-13 05:15:06	\N
25	\N	\N	Se asignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	11	2018-12-13 05:15:15	2018-12-13 05:15:15	\N
26	\N	\N	Se desasignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	12	2018-12-14 02:15:30	2018-12-14 02:15:30	\N
27	\N	\N	Se asignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	13	2018-12-14 02:15:31	2018-12-14 02:15:31	\N
28	\N	\N	Se agregó el registro ":title"	\N	\N	14	2019-01-13 00:24:14	2019-01-13 00:24:14	\N
29	\N	\N	Se agregó el registro ":title"	\N	\N	15	2019-01-13 00:30:42	2019-01-13 00:30:42	\N
30	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	16	2019-01-13 00:39:34	2019-01-13 00:39:34	\N
31	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	17	2019-01-13 00:40:21	2019-01-13 00:40:21	\N
32	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	18	2019-01-13 00:41:00	2019-01-13 00:41:00	\N
33	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	19	2019-01-13 00:41:23	2019-01-13 00:41:23	\N
34	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	20	2019-01-13 00:42:42	2019-01-13 00:42:42	\N
35	Link de noticia en facebook	decima-oadh::media-monitoring.facebookLink	\N	\N	dd	21	2019-01-13 00:42:53	2019-01-13 00:42:53	\N
36	Link de screenshot	decima-oadh::media-monitoring.screenshotLink	\N	\N	dd	21	2019-01-13 00:42:53	2019-01-13 00:42:53	\N
37	\N	\N	Se agregó el registro ":title"	\N	\N	22	2019-01-15 18:51:33	2019-01-15 18:51:33	\N
38	\N	\N	Se agregó el registro ":title"	\N	\N	23	2019-01-16 00:36:27	2019-01-16 00:36:27	\N
39	\N	\N	Se agregó el registro ":title"	\N	\N	24	2019-01-16 00:39:21	2019-01-16 00:39:21	\N
40	\N	\N	Se agregó el registro ":title"	\N	\N	25	2019-01-16 00:39:34	2019-01-16 00:39:34	\N
41	\N	\N	Se agregó el registro ":title"	\N	\N	26	2019-01-16 00:39:45	2019-01-16 00:39:45	\N
42	\N	\N	Se agregó el registro ":title"	\N	\N	27	2019-01-16 00:40:12	2019-01-16 00:40:12	\N
43	\N	\N	Se agregó el registro ":title"	\N	\N	28	2019-01-16 00:40:26	2019-01-16 00:40:26	\N
44	\N	\N	Se agregó el registro ":title"	\N	\N	29	2019-01-16 02:37:23	2019-01-16 02:37:23	\N
45	decima-oadh::media-monitoring.subtype	decima-oadh::media-monitoring.subtype	\N	P	S	30	2019-01-16 02:43:36	2019-01-16 02:43:36	\N
46	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	\N	1	31	2019-01-16 02:47:01	2019-01-16 02:47:01	\N
47	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	1	\N	32	2019-01-16 02:47:55	2019-01-16 02:47:55	\N
48	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	\N	0	32	2019-01-16 02:47:55	2019-01-16 02:47:55	\N
49	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	0	1	33	2019-01-16 02:48:05	2019-01-16 02:48:05	\N
50	Tipo de victima	decima-oadh::media-monitoring.type	\N	V	S	34	2019-01-16 02:51:46	2019-01-16 02:51:46	\N
51	\N	\N	Se agregó el registro ":title"	\N	\N	35	2019-01-16 02:52:42	2019-01-16 02:52:42	\N
52	Tipo de victima	decima-oadh::media-monitoring.type	\N	V	S	36	2019-01-16 02:57:34	2019-01-16 02:57:34	\N
53	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	\N	1	37	2019-01-16 03:00:09	2019-01-16 03:00:09	\N
54	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	1	0	37	2019-01-16 03:00:09	2019-01-16 03:00:09	\N
55	decima-oadh::media-monitoring.subtype	decima-oadh::media-monitoring.subtype	\N	S	P	38	2019-01-16 03:13:48	2019-01-16 03:13:48	\N
56	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	1	0	39	2019-01-16 03:13:59	2019-01-16 03:13:59	\N
57	decima-oadh::media-monitoring.subtype	decima-oadh::media-monitoring.subtype	\N	P	S	40	2019-01-16 03:14:53	2019-01-16 03:14:53	\N
58	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	\N	1	40	2019-01-16 03:14:53	2019-01-16 03:14:53	\N
59	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	1	0	41	2019-01-16 03:15:00	2019-01-16 03:15:00	\N
60	\N	\N	Se agregó el registro ":title"	\N	\N	42	2019-01-16 03:27:30	2019-01-16 03:27:30	\N
61	Relación con la victima	decima-oadh::media-monitoring.victimRelation	\N	0	1	43	2019-01-16 03:30:58	2019-01-16 03:30:58	\N
62	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	1	\N	43	2019-01-16 03:30:58	2019-01-16 03:30:58	\N
63	Relación con la victima	decima-oadh::media-monitoring.victimRelation	\N	1	0	44	2019-01-16 03:31:07	2019-01-16 03:31:07	\N
64	decima-oadh::media-monitoring.subtype	decima-oadh::media-monitoring.subtype	\N	P	S	45	2019-01-16 03:31:13	2019-01-16 03:31:13	\N
65	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	\N	1	45	2019-01-16 03:31:13	2019-01-16 03:31:13	\N
66	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	1	0	46	2019-01-16 03:31:19	2019-01-16 03:31:19	\N
67	\N	\N	Se agregó el registro ":title"	\N	\N	47	2019-01-16 03:33:33	2019-01-16 03:33:33	\N
68	\N	\N	Se agregó el registro ":title"	\N	\N	48	2019-01-16 03:37:48	2019-01-16 03:37:48	\N
69	\N	\N	Se agregó el registro ":title"	\N	\N	49	2019-01-16 03:43:05	2019-01-16 03:43:05	\N
70	\N	\N	Se agregó el registro ":title"	\N	\N	50	2019-01-16 03:43:18	2019-01-16 03:43:18	\N
71	\N	\N	Se agregó el registro ":title"	\N	\N	51	2019-01-16 03:43:36	2019-01-16 03:43:36	\N
72	\N	\N	Se agregó el registro ":title"	\N	\N	52	2019-01-16 03:43:51	2019-01-16 03:43:51	\N
73	\N	\N	Se agregó el registro ":title"	\N	\N	53	2019-01-16 03:45:50	2019-01-16 03:45:50	\N
74	\N	\N	Se agregó el registro ":title"	\N	\N	54	2019-01-16 03:46:13	2019-01-16 03:46:13	\N
75	Link de noticia en facebook	decima-oadh::media-monitoring.facebookLink	\N	\N	ddd	55	2019-01-16 03:46:21	2019-01-16 03:46:21	\N
76	Link de screenshot	decima-oadh::media-monitoring.screenshotLink	\N	\N	dd	55	2019-01-16 03:46:21	2019-01-16 03:46:21	\N
77	\N	\N	Se agregó el registro ":title"	\N	\N	56	2019-01-16 18:52:26	2019-01-16 18:52:26	\N
78	N° de página	decima-oadh::media-monitoring.page	\N	5	1000	57	2019-01-16 18:55:31	2019-01-16 18:55:31	\N
79	\N	\N	Se agregó el registro ":title"	\N	\N	58	2019-01-16 18:56:12	2019-01-16 18:56:12	\N
80	\N	\N	Se agregó el registro ":title"	\N	\N	59	2019-01-16 18:56:29	2019-01-16 18:56:29	\N
81	\N	\N	Se agregó el registro ":title"	\N	\N	60	2019-01-16 18:56:33	2019-01-16 18:56:33	\N
82	\N	\N	Se agregó el registro ":title"	\N	\N	61	2019-01-16 18:56:44	2019-01-16 18:56:44	\N
83	\N	\N	Se agregó el registro ":title"	\N	\N	62	2019-01-16 18:56:54	2019-01-16 18:56:54	\N
84	\N	\N	Se agregó el registro ":title"	\N	\N	63	2019-01-16 18:56:58	2019-01-16 18:56:58	\N
85	\N	\N	Se agregó la noticia "Prueba"	\N	\N	64	2019-02-13 03:42:42	2019-02-13 03:42:42	\N
86	\N	\N	decima-oadh::media-monitoring.addedRightJournal	\N	\N	65	2019-02-13 03:43:06	2019-02-13 03:43:06	\N
87	\N	\N	Se agregó la noticia ":name"	\N	\N	67	2019-02-15 14:28:43	2019-02-15 14:28:43	\N
88	\N	\N	Se agregó la noticia ":name"	\N	\N	68	2019-02-15 14:29:16	2019-02-15 14:29:16	\N
89	\N	\N	Se asignó la aplicación "Solicitudes de habeas corpus" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
90	\N	\N	Se asignó la aplicación "Personas detenidas e internadas provisionalmente" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
91	\N	\N	Se asignó la aplicación "Cantidad de Juicios en Materia Penal" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
92	\N	\N	Se asignó la aplicación "Personas detenidas y procesadas en masa" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
93	\N	\N	Se asignó la aplicación "Imputados por los delitos de fraude procesal y simulación de delitos" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
94	\N	\N	Se asignó la aplicación "Detenciones basadas en órdenes de captura" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
95	\N	\N	Se asignó la aplicación "Víctimas de desaparición" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
96	\N	\N	Se asignó la aplicación "Imputados por el delito de privación de libertad" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
97	\N	\N	Se asignó la aplicación "Víctimas de privación de libertad" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
98	\N	\N	Se asignó la aplicación "Imputados por Desaparición Forzada" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
99	\N	\N	Se asignó la aplicación "Ocupación en bartolinas policiales" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
100	\N	\N	Se asignó la aplicación "Derechos violentados a privados de libertad" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
101	\N	\N	Se asignó la aplicación "Enfermedades agudas más comunes en centros penales" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
102	\N	\N	Se asignó la aplicación "Enfermedades crónicas más comunes en centros penales" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
103	\N	\N	Se asignó la aplicación "Víctimas ingresadas por medio de denuncias ante los delitos de de lesiones, amenazas, disparo de arma de fuego, extorsión y tortura" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
104	\N	\N	Se asignó la aplicación "Imputados de profesión PNC y miembros de FAES ante el delito de lesiones, disparo de arma de fuego, extorsión y tortura" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
105	\N	\N	Se asignó la aplicación "Agentes que continuaron laborando durante fases de investigaciones abiertas" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
106	\N	\N	Se asignó la aplicación "Según sexo - Casos reportados por el delito de trata y tráfico de personas" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
107	\N	\N	Se asignó la aplicación "Según tipo de delito - Casos reportados por el delito de trata y tráfico de personas" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
108	\N	\N	Se asignó la aplicación "Según rango de edad - Casos reportados por el delito de trata y tráfico de personas" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
109	\N	\N	Se asignó la aplicación "Víctimas ingresadas por el delito de limitación ilegal a la libertad de circulación" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
110	\N	\N	Se asignó la aplicación "Según rango de edad - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
111	\N	\N	Se asignó la aplicación "Según tipo de delito - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
112	\N	\N	Se asignó la aplicación "Según depto. y mpio. - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
113	\N	\N	Se asignó la aplicación "Según tipo, depto. y mpio. - Denuncias interpuestas por delitos atentatorios contra la libertad sexual" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
114	\N	\N	Se asignó la aplicación "Según sexo - Denuncias interpuestas por delitos atentatorios contra la libertad sexual" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
115	\N	\N	Se asignó la aplicación "Según rango de edad - Denuncias interpuestas por delitos atentatorios contra la libertad sexual" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
116	\N	\N	Se asignó la aplicación "Según depto. y mpio. - Agresiones registradas por el delito de lesiones" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
117	\N	\N	Se asignó la aplicación "Según sexo y rango de edad - Agresiones registradas por el delito de lesiones" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
118	\N	\N	Se asignó la aplicación "Según rango de horario y tipo de víct. - Agresiones registradas por el delito de lesiones" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
119	\N	\N	Se asignó la aplicación "Investigaciones de agentes PNC por delitos relativo al abuso de la fuerza y uso ilegal de armas de fuego" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
120	\N	\N	Se asignó la aplicación "Denuncias reportadas en la PDDH por la vulneración del derecho a la integridad personal" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
121	\N	\N	Se agregó la carpeta "Ejemplos " al sistema	\N	\N	70	2019-03-24 18:50:23	2019-03-24 18:50:23	\N
122	\N	\N	Se agregó el archivo "estadocuenta6840010153.xls " al sistema	\N	\N	71	2019-03-24 18:51:38	2019-03-24 18:51:38	\N
123	\N	\N	Se agregó el archivo "habeas_corpus01.xlsx " al sistema	\N	\N	72	2019-03-29 11:58:20	2019-03-29 11:58:20	\N
124	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	73	2019-04-02 12:02:17	2019-04-02 12:02:17	\N
125	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	74	2019-04-02 12:06:57	2019-04-02 12:06:57	\N
126	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	75	2019-04-02 12:07:41	2019-04-02 12:07:41	\N
127	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	76	2019-04-02 14:27:47	2019-04-02 14:27:47	\N
128	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	77	2019-04-02 14:28:03	2019-04-02 14:28:03	\N
129	\N	\N	Se agregó el archivo "habeas_corpus03.xlsx " al sistema	\N	\N	78	2019-04-02 17:08:09	2019-04-02 17:08:09	\N
130	\N	\N	Se eliminó el archivo "habeas_corpus03.xlsx" seleccionado en el sistema.	\N	\N	79	2019-04-02 17:15:52	2019-04-02 17:15:52	\N
131	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	80	2019-04-05 00:20:16	2019-04-05 00:20:16	\N
132	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	81	2019-04-07 21:32:21	2019-04-07 21:32:21	\N
133	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	82	2019-04-07 21:48:38	2019-04-07 21:48:38	\N
134	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	83	2019-04-08 21:40:54	2019-04-08 21:40:54	\N
135	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	84	2019-04-08 21:44:23	2019-04-08 21:44:23	\N
136	\N	\N	decima-oadh::media-monitoring.addedRightJournal	\N	\N	85	2019-04-11 13:54:25	2019-04-11 13:54:25	\N
137	\N	\N	Se agregó la noticia ":name"	\N	\N	86	2019-04-11 14:02:28	2019-04-11 14:02:28	\N
138	\N	\N	decima-oadh::media-monitoring.deletedDetailJournal	\N	\N	87	2019-04-11 14:02:36	2019-04-11 14:02:36	\N
140	\N	\N	Se agregó la carpeta "Habeas Corpus " al sistema	\N	\N	89	2019-04-16 02:33:38	2019-04-16 02:33:38	\N
141	Nombre	decima-file::file-management.name	\N	Habeas Corpus	test	90	2019-04-16 02:38:04	2019-04-16 02:38:04	\N
142	\N	\N	Se agregó la carpeta "Habeas Corpus " al sistema	\N	\N	91	2019-04-16 02:38:11	2019-04-16 02:38:11	\N
143	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	92	2019-04-16 03:57:37	2019-04-16 03:57:37	\N
144	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	93	2019-04-16 03:58:24	2019-04-16 03:58:24	\N
145	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	94	2019-04-25 11:10:52	2019-04-25 11:10:52	\N
146	\N	\N	Se agregó la carpeta "Personas Detenidas " al sistema	\N	\N	95	2019-04-25 11:20:18	2019-04-25 11:20:18	\N
147	\N	\N	Se agregó la carpeta "Criminal Cases Trials " al sistema	\N	\N	96	2019-04-25 11:21:45	2019-04-25 11:21:45	\N
148	Nombre	decima-file::file-management.name	\N	Personas Detenidas	Test	97	2019-04-25 11:22:15	2019-04-25 11:22:15	\N
149	\N	\N	Se agregó la carpeta "Mass Trials " al sistema	\N	\N	98	2019-04-25 11:25:11	2019-04-25 11:25:11	\N
150	\N	\N	Se agregó la carpeta "Procedural Fraud " al sistema	\N	\N	99	2019-04-25 11:25:50	2019-04-25 11:25:50	\N
151	\N	\N	Se agregó la carpeta "Disapparences Victims " al sistema	\N	\N	100	2019-04-25 11:26:27	2019-04-25 11:26:27	\N
152	\N	\N	Se agregó la carpeta "Disapparences Victims " al sistema	\N	\N	101	2019-04-25 11:28:46	2019-04-25 11:28:46	\N
153	\N	\N	Se agregó la carpeta "Accused Liberty Deprivation " al sistema	\N	\N	102	2019-04-25 11:29:25	2019-04-25 11:29:25	\N
154	\N	\N	Se agregó la carpeta "Victim Liberty Deprivation " al sistema	\N	\N	103	2019-04-25 11:29:46	2019-04-25 11:29:46	\N
155	\N	\N	Se agregó la carpeta "Forced Disappearances " al sistema	\N	\N	104	2019-04-25 11:30:15	2019-04-25 11:30:15	\N
156	\N	\N	Se agregó la carpeta "Jails Occupation " al sistema	\N	\N	105	2019-04-25 11:30:27	2019-04-25 11:30:27	\N
157	\N	\N	Se agregó la carpeta "Deprived persons " al sistema	\N	\N	106	2019-04-25 11:30:56	2019-04-25 11:30:56	\N
158	\N	\N	Se agregó la carpeta "Acute Disease " al sistema	\N	\N	107	2019-04-25 11:31:12	2019-04-25 11:31:12	\N
159	\N	\N	Se agregó la carpeta "Chronic Diseases " al sistema	\N	\N	108	2019-04-25 11:31:29	2019-04-25 11:31:29	\N
160	\N	\N	Se agregó la carpeta "Capture Orders " al sistema	\N	\N	109	2019-04-25 11:31:48	2019-04-25 11:31:48	\N
161	\N	\N	Se agregó el archivo "02___Personas_detenidas_e_internadas_provisionalmente.xlsx " al sistema	\N	\N	110	2019-04-25 11:33:12	2019-04-25 11:33:12	\N
\.


--
-- TOC entry 3512 (class 0 OID 16431)
-- Dependencies: 208
-- Data for Name: SEC_Menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Menu" (id, name, lang_key, url, action_button_id, action_lang_key, icon, parent_id, created_by, module_id, created_at, updated_at, deleted_at) FROM stdin;
1	Organization	security/menu.organization	\N		\N	fa fa-building-o	\N	1	1	2018-12-13 03:18:43	2018-12-13 03:18:43	\N
2	Organization Management	security/menu.organizationManagment	/general-setup/organization/organization-management	om-btn-close	security/menu.organizationManagmentAction	fa fa-wrench	1	1	1	2018-12-13 03:18:43	2018-12-13 03:18:43	\N
3	Security	security/menu.security	\N		\N	fa fa-lock	\N	1	1	2018-12-13 03:18:43	2018-12-13 03:18:43	\N
4	User Management	security/menu.userManagment	/general-setup/security/user-management	um-btn-close	security/menu.userManagmentAction	fa fa-wrench	3	1	1	2018-12-13 03:18:43	2018-12-13 03:18:43	\N
5	File Manager	decima-file::menu.fileManager	/files/file-manager	fi-fm-btn-close	decima-file::menu.fileManagerAction	fa fa-files-o	\N	1	2	2018-12-13 03:20:27	2018-12-13 03:20:27	\N
6	Setup (OADH)	decima-oadh::menu.setup	\N		\N	fa fa-gear	\N	1	3	2018-12-13 04:38:19	2018-12-13 04:38:19	\N
10	Transaction (OADH)	decima-oadh::menu.transactions	\N		\N	fa fa-exchange	\N	1	3	2018-12-13 04:38:20	2018-12-13 04:38:20	\N
7	Configuración inicial	decima-oadh::menu.initialSetup	/ucaoadh/setup/initial-setup	oadh-is-btn-close	decima-oadh::menu.initialSetupAction	fa fa-gear	6	1	3	2018-12-13 04:38:19	2018-12-13 04:38:19	\N
8	Gestión de derechos humanos	decima-oadh::menu.humanRightManagement	/ucaoadh/setup/human-right-management	oadh-hrm-btn-close	decima-oadh::menu.humanRightManagementAction	fa fa-gear	6	1	3	2018-12-13 04:38:20	2018-12-13 04:38:20	\N
9	Gestión de medios	decima-oadh::menu.mediaManagement	/ucaoadh/setup/digital-media-management	rh-emp-btn-close	decima-oadh::menu.mediaManagementAction	fa fa-gear	6	1	3	2018-12-13 04:38:20	2018-12-13 04:38:20	\N
12	Derecho a la libertad	decima-oadh::menu.rightToFreedom	\N		\N	fa fa-database	\N	1	3	\N	\N	\N
13	Derecho a la integridad personal	decima-oadh::menu.rightToPersonalIntegrity	\N		\N	fa fa-database	\N	1	3	\N	\N	\N
14	Derecho a la vida	decima-oadh::menu.rightToLife	\N		\N	fa fa-database	\N	1	3	\N	\N	\N
19	Solicitudes de habeas corpus	decima-oadh::menu.habeasCorpusRequest	/ucaoadh/freedom/habeas-corpus-request	oadh-free-hcr-btn-close	decima-oadh::menu.habeasCorpusRequestAction	fa fa-upload	12	1	3	\N	\N	\N
20	Personas detenidas e internadas provisionalmente	decima-oadh::menu.peopleDetained	/ucaoadh/freedom/people-detained	oadh-free-pd-btn-close	decima-oadh::menu.peopleDetainedAction	fa fa-upload	12	1	3	\N	\N	\N
21	Cantidad de Juicios en Materia Penal	decima-oadh::menu.criminalCases	/ucaoadh/freedom/criminal-cases-trials	oadh-free-cct-btn-close	decima-oadh::menu.criminalCasesAction	fa fa-upload	12	1	3	\N	\N	\N
22	Personas detenidas y procesadas en masa	decima-oadh::menu.massTrials	/ucaoadh/freedom/mass-trials	oadh-free-mt-btn-close	decima-oadh::menu.massTrialsAction	fa fa-upload	12	1	3	\N	\N	\N
23	Imputados por los delitos de fraude procesal y simulación de delitos	decima-oadh::menu.proceduralFraud	/ucaoadh/freedom/procedural-fraud	oadh-free-pf-btn-close	decima-oadh::menu.proceduralFraudAction	fa fa-upload	12	1	3	\N	\N	\N
24	Detenciones basadas en órdenes de captura	decima-oadh::menu.captureOrders	/ucaoadh/freedom/capture-orders	oadh-free-co-btn-close	decima-oadh::menu.proceduralFraudAction	fa fa-upload	12	1	3	\N	\N	\N
25	Víctimas de desaparición	decima-oadh::menu.disapparencesVictims	/ucaoadh/freedom/disapparences-victims	oadh-free-dv-btn-close	decima-oadh::menu.disapparencesVictimsAction	fa fa-upload	12	1	3	\N	\N	\N
26	Imputados por el delito de privación de libertad	decima-oadh::menu.accusedlibertyDeprivation	/ucaoadh/freedom/accused-liberty-deprivation	oadh-free-llp-btn-close	decima-oadh::menu.accusedLibertyDeprivationAction	fa fa-upload	12	1	3	\N	\N	\N
27	Víctimas de privación de libertad	decima-oadh::menu.victimLibertyDeprivation	/ucaoadh/freedom/victim-liberty-deprivation	oadh-free-vlp-btn-close	decima-oadh::menu.victimLibertyDeprivationAction	fa fa-upload	12	1	3	\N	\N	\N
28	Imputados por Desaparición Forzada	decima-oadh::menu.forcedDisappearances	/ucaoadh/freedom/forced-disappearances	oadh-free-fd-btn-close	decima-oadh::menu.libertyDeprivationAction	fa fa-upload	12	1	3	\N	\N	\N
29	Ocupación en bartolinas policiales	decima-oadh::menu.jailsOccupation	/ucaoadh/freedom/jails-occupation	oadh-free-fd-btn-close	decima-oadh::menu.jailsOccupationAction	fa fa-upload	12	1	3	\N	\N	\N
30	Derechos violentados a privados de libertad	decima-oadh::menu.deprivedPersons	/ucaoadh/freedom/deprived-persons	oadh-free-dp-btn-close	decima-oadh::menu.deprivedPersonsAction	fa fa-upload	12	1	3	\N	\N	\N
31	Enfermedades agudas más comunes en centros penales	decima-oadh::menu.acuteDiseases	/ucaoadh/freedom/acute-disease	oadh-free-ad-btn-close	decima-oadh::menu.acuteIllnessAction	fa fa-upload	12	1	3	\N	\N	\N
11	Monitoreo de medios	decima-oadh::menu.mediaMonitoring	/ucaoadh/transactions/media-monitoring	oadh-mmo-btn-close	decima-oadh::menu.mediaMonitoringAction	fa fa-eye	10	1	3	2018-12-13 04:38:21	2018-12-13 04:38:21	\N
32	Enfermedades crónicas más comunes en centros penales	decima-oadh::menu.chronicDiseases	/ucaoadh/freedom/chronic-diseases	oadh-free-cd-btn-close	decima-oadh::menu.chronicDiseasesAction	fa fa-upload	12	1	3	\N	\N	\N
33	Víctimas ingresadas por medio de denuncias ante los delitos de de lesiones, amenazas, disparo de arma de fuego, extorsión y tortura	decima-oadh::menu.crimesVictims	/ucaoadh/integrity/crimes-victims	oadh-inte-cv-btn-close	decima-oadh::menu.crimesVictimsAction	fa fa-upload	13	1	3	\N	\N	\N
34	Imputados de profesión PNC y miembros de FAES ante el delito de lesiones, disparo de arma de fuego, extorsión y tortura	decima-oadh::menu.crimesAccused	/ucaoadh/integrity/crimes-accused	oadh-inte-ca-btn-close	decima-oadh::menu.crimesAccusedAction	fa fa-upload	13	1	3	\N	\N	\N
35	Agentes que continuaron laborando durante fases de investigaciones abiertas	decima-oadh::menu.workingOfficials	/ucaoadh/integrity/working-officials	oadh-inte-wo-btn-close	decima-oadh::menu.workingOfficialsAction	fa fa-upload	13	1	3	\N	\N	\N
36	Según sexo - Casos reportados por el delito de trata y tráfico de personas	decima-oadh::menu.genderTrafficking	/ucaoadh/integrity/gender-trafficking	oadh-inte-gt-btn-close	decima-oadh::menu.genderTraffickingAction	fa fa-upload	13	1	3	\N	\N	\N
37	Según tipo de delito - Casos reportados por el delito de trata y tráfico de personas	decima-oadh::menu.typeTrafficking	/ucaoadh/integrity/type-trafficking	oadh-inte-tt-btn-close	decima-oadh::menu.typeTraffickingAction	fa fa-upload	13	1	3	\N	\N	\N
38	Según rango de edad - Casos reportados por el delito de trata y tráfico de personas	decima-oadh::menu.ageTrafficking	/ucaoadh/integrity/age-trafficking	oadh-inte-at-btn-close	decima-oadh::menu.ageTraffickingAction	fa fa-upload	13	1	3	\N	\N	\N
39	Víctimas ingresadas por el delito de limitación ilegal a la libertad de circulación	decima-oadh::menu.movementFreedom	/ucaoadh/integrity/movement-freedom	oadh-inte-mf-btn-close	decima-oadh::menu.movementFreedomAction	fa fa-upload	13	1	3	\N	\N	\N
40	Según rango de edad - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres	decima-oadh::menu.ageWomen	/ucaoadh/integrity/age-women	oadh-inte-aw-btn-close	decima-oadh::menu.ageWomenAction	fa fa-upload	13	1	3	\N	\N	\N
41	Según tipo de delito - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres	decima-oadh::menu.typeWomen	/ucaoadh/integrity/type-women	oadh-inte-aw-btn-close	decima-oadh::menu.typeWomenAction	fa fa-upload	13	1	3	\N	\N	\N
42	Según depto. y mpio. - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres	decima-oadh::menu.StateWomen	/ucaoadh/integrity/state-women	oadh-inte-aw-btn-close	decima-oadh::menu.stateWomenAction	fa fa-upload	13	1	3	\N	\N	\N
43	Según tipo, depto. y mpio. - Denuncias interpuestas por delitos atentatorios contra la libertad sexual	decima-oadh::menu.typeSexual	/ucaoadh/integrity/type-sexual	oadh-inte-ts-btn-close	decima-oadh::menu.typeSexualAction	fa fa-upload	13	1	3	\N	\N	\N
44	Según sexo - Denuncias interpuestas por delitos atentatorios contra la libertad sexual	decima-oadh::menu.genderSexual	/ucaoadh/integrity/gender-sexual	oadh-inte-gs-btn-close	decima-oadh::menu.genderSexualAction	fa fa-upload	13	1	3	\N	\N	\N
45	Según rango de edad - Denuncias interpuestas por delitos atentatorios contra la libertad sexual	decima-oadh::menu.ageSexual	/ucaoadh/integrity/age-sexual	oadh-inte-as-btn-close	decima-oadh::menu.ageSexualAction	fa fa-upload	13	1	3	\N	\N	\N
46	Según depto. y mpio. - Agresiones registradas por el delito de lesiones	decima-oadh::menu.stateInjuries	/ucaoadh/integrity/state-injuries	oadh-inte-si-btn-close	decima-oadh::menu.stateInjuriesAction	fa fa-upload	13	1	3	\N	\N	\N
47	Según sexo y rango de edad - Agresiones registradas por el delito de lesiones	decima-oadh::menu.ageInjuries	/ucaoadh/integrity/age-injuries	oadh-inte-si-btn-close	decima-oadh::menu.ageInjuriesAction	fa fa-upload	13	1	3	\N	\N	\N
48	Según rango de horario y tipo de víct. - Agresiones registradas por el delito de lesiones	decima-oadh::menu.scheduleInjuries	/ucaoadh/integrity/schedule-injuries	oadh-inte-si-btn-close	decima-oadh::menu.scheduleInjuriesAction	fa fa-upload	13	1	3	\N	\N	\N
49	Investigaciones de agentes PNC por delitos relativo al abuso de la fuerza y uso ilegal de armas de fuego	decima-oadh::menu.agentInvestigation	/ucaoadh/integrity/agent-investigation	oadh-inte-ai-btn-close	decima-oadh::menu.agentInvestigationAction	fa fa-upload	13	1	3	\N	\N	\N
50	Denuncias reportadas en la PDDH por la vulneración del derecho a la integridad personal	decima-oadh::menu.integrityViolation	/ucaoadh/integrity/integrity-violation	oadh-inte-iv-btn-close	decima-oadh::menu.integrityViolationAction	fa fa-upload	13	1	3	\N	\N	\N
\.


--
-- TOC entry 3514 (class 0 OID 16440)
-- Dependencies: 210
-- Data for Name: SEC_Module; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Module" (id, name, lang_key, icon, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	General Setup	security/module.generalSetup	fa fa-cogs	1	2018-12-13 03:18:42	2018-12-13 03:18:42	\N
2	Files	decima-file::menu.module	fa fa-folder-open	1	2018-12-13 03:20:26	2018-12-13 03:20:26	\N
3	OADH	decima-oadh::menu.module	fa fa-search	1	2018-12-13 04:38:17	2018-12-13 04:38:17	\N
\.


--
-- TOC entry 3516 (class 0 OID 16445)
-- Dependencies: 212
-- Data for Name: SEC_Password_Reminders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Password_Reminders" (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 3517 (class 0 OID 16451)
-- Dependencies: 213
-- Data for Name: SEC_Permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Permission" (id, name, key, lang_key, url, alias_url, action_button_id, action_lang_key, icon, shortcut_icon, is_only_shortcut, is_dashboard_shortcut_visible, hidden, menu_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	Setup a new organization	newOrganization	security/permission.newOrganization	/general-setup/organization/organization-management/new	/general-setup/organization/organization-management	om-btn-new	organization/organization-management.new	fa fa-plus	\N	t	f	f	2	1	2018-12-13 03:18:44	2018-12-13 03:18:44	\N
2	Edit organization	editOrganization	organization/organization-management.edit	/general-setup/organization/organization-management/edit	/general-setup/organization/organization-management	om-btn-edit-helper	organization/organization-management.edit	\N	\N	t	f	t	2	1	2018-12-13 03:18:44	2018-12-13 03:18:44	\N
3	Remove organization	removeOrganization	organization/organization-management.delete	/general-setup/organization/organization-management/remove	/general-setup/organization/organization-management	om-btn-remove-helper	organization/organization-management.delete	\N	\N	t	f	t	2	1	2018-12-13 03:18:45	2018-12-13 03:18:45	\N
4	New user	newUser	security/permission.newUser	/general-setup/security/user-management/new	/general-setup/security/user-management	um-btn-new	security/user-management.new	fa fa-plus	\N	t	f	f	4	1	2018-12-13 03:18:45	2018-12-13 03:18:45	\N
5	New administrator user	newAdminUser	security/permission.newAdminUser	/general-setup/security/user-management/new-admin	/general-setup/security/user-management	um-btn-new-admin	security/user-management.newAdminLongText	fa fa-plus	\N	f	f	f	4	1	2018-12-13 03:18:45	2018-12-13 03:18:45	\N
6	Remove user	removeUser	security/user-management.deleteLongText	/general-setup/security/user-management/remove-user	/general-setup/security/user-management	um-btn-remove-helper	security/user-management.deleteLongText	\N	\N	t	f	t	4	1	2018-12-13 03:18:46	2018-12-13 03:18:46	\N
7	Assign role	assignRole	security/permission.assignRole	/general-setup/security/user-management/assign-role	/general-setup/security/user-management	um-btn-assign-helper	security/permission.assignRole	\N	\N	t	f	t	4	1	2018-12-13 03:18:46	2018-12-13 03:18:46	\N
8	Unassign role	unassignRole	security/permission.unassignRole	/general-setup/security/user-management/unassign-role	/general-setup/security/user-management	um-btn-unassign-helper	security/permission.unassignRole	\N	\N	t	f	t	4	1	2018-12-13 03:18:46	2018-12-13 03:18:46	\N
9	Upload File	uploadFile	decima-file::menu.uploadFile	/files/file-manager/upload-file	/files/file-manager	fi-fm-btn-upload-file	decima-file::menu.uploadFileAction	fa fa-cloud-upload	\N	t	f	f	5	1	2018-12-13 03:20:28	2018-12-13 03:20:28	\N
10	New Folder	newFolder	decima-file::menu.newFolder	/files/file-manager/new-folder	/files/file-manager	fi-fm-btn-new-folder	decima-file::menu.newFolderAction	fa fa-folder	\N	t	f	f	5	1	2018-12-13 03:20:28	2018-12-13 03:20:28	\N
11	Delete File	deleteFile	decima-file::menu.deleteFile	/files/file-manager/delete-file	/files/file-manager	fi-fm-btn-delete-file-helper	decima-file::menu.deleteFileAction	\N	\N	t	f	t	5	1	2018-12-13 03:20:28	2018-12-13 03:20:28	\N
12	Delete Folder	deleteFolder	decima-file::menu.deleteFolder	/files/file-manager/delete-folder	/files/file-manager	fi-fm-btn-delete-folder-helper	decima-file::menu.deleteFolderAction	\N	\N	t	f	t	5	1	2018-12-13 03:20:29	2018-12-13 03:20:29	\N
\.


--
-- TOC entry 3519 (class 0 OID 16462)
-- Dependencies: 215
-- Data for Name: SEC_Role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Role" (id, name, lang_key, description, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3520 (class 0 OID 16465)
-- Dependencies: 216
-- Data for Name: SEC_Role_Menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Role_Menu" (id, role_id, menu_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3522 (class 0 OID 16470)
-- Dependencies: 218
-- Data for Name: SEC_Role_Permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Role_Permission" (id, role_id, permission_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3525 (class 0 OID 16477)
-- Dependencies: 221
-- Data for Name: SEC_User; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User" (id, firstname, lastname, email, password, is_active, is_admin, timezone, activation_code, activated_at, last_login, popovers_shown, multiple_organization_popover_shown, remember_token, created_by, default_organization, created_at, updated_at, deleted_at) FROM stdin;
1	oadh	oadh	oadh@decimaerp.com	$2y$10$4QJVrf/BGIN11JeMNEGqF.U3RE6Ar/UKlEEal/IqQUEyAKMrgGYEG	t	t	America/El_Salvador	\N	\N	\N	t	f	F0oUyJ6qCLHxpmUTYgqIKDRXsNp20HOW3w8nlltxLKYrkQ8WAPXLGurvujFI	\N	1	2018-12-13 03:18:42	2019-04-16 02:31:50	\N
\.


--
-- TOC entry 3526 (class 0 OID 16485)
-- Dependencies: 222
-- Data for Name: SEC_User_Menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User_Menu" (id, is_assigned, user_id, menu_id, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	t	1	2	1	1	2018-12-13 04:00:03	2018-12-13 04:00:03	\N
3	t	1	5	1	1	2018-12-13 04:00:03	2018-12-13 04:00:03	\N
4	t	1	11	1	1	2018-12-13 04:39:00	2018-12-13 04:39:00	\N
5	t	1	9	1	1	2018-12-13 04:39:01	2018-12-13 04:39:01	\N
6	t	1	8	1	1	2018-12-13 04:39:01	2018-12-13 04:39:01	\N
7	t	1	7	1	1	2018-12-13 04:39:01	2018-12-13 04:39:01	\N
13	t	1	4	1	1	2018-12-14 02:15:31	2018-12-14 02:15:31	\N
14	t	1	19	1	1	2019-03-17 17:53:18	2019-03-17 17:53:18	\N
15	t	1	20	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
16	t	1	21	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
17	t	1	22	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
18	t	1	23	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
19	t	1	24	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
20	t	1	25	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
21	t	1	26	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
22	t	1	27	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
23	t	1	28	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
24	t	1	29	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
25	t	1	30	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
26	t	1	31	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
27	t	1	32	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
28	t	1	33	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
29	t	1	34	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
30	t	1	35	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
31	t	1	36	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
32	t	1	37	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
33	t	1	38	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
34	t	1	39	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
35	t	1	40	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
36	t	1	41	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
37	t	1	42	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
38	t	1	43	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
39	t	1	44	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
40	t	1	45	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
41	t	1	46	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
42	t	1	47	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
43	t	1	48	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
44	t	1	49	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
45	t	1	50	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
\.


--
-- TOC entry 3528 (class 0 OID 16490)
-- Dependencies: 224
-- Data for Name: SEC_User_Organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User_Organization" (id, user_id, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	1	1	1	2018-12-13 04:00:02	2018-12-13 04:00:02	\N
\.


--
-- TOC entry 3530 (class 0 OID 16495)
-- Dependencies: 226
-- Data for Name: SEC_User_Permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User_Permission" (id, is_assigned, user_id, permission_id, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	t	1	5	1	1	2018-12-13 04:00:03	2018-12-13 04:00:03	\N
\.


--
-- TOC entry 3532 (class 0 OID 16500)
-- Dependencies: 228
-- Data for Name: SEC_User_Role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User_Role" (id, user_id, role_id, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3535 (class 0 OID 16507)
-- Dependencies: 231
-- Data for Name: SYS_Account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Account" (id, key, parent_key, name, balance_type, account_type_key, is_group, is_general_ledger_account, account_chart_type_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3536 (class 0 OID 16515)
-- Dependencies: 232
-- Data for Name: SYS_Account_Chart_Type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Account_Chart_Type" (id, name, url, lang_key, country_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3538 (class 0 OID 16520)
-- Dependencies: 234
-- Data for Name: SYS_Account_Type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Account_Type" (id, name, lang_key, key, pl_bs_category, deferral_method, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3541 (class 0 OID 16527)
-- Dependencies: 237
-- Data for Name: SYS_Country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Country" (id, iso_code, name, region_name, region_lang_key, tax_id_name, tax_id_abbreviation, registration_number_name, registration_number_abbreviation, single_identity_document_number_name, single_identity_document_number_abbreviation, social_security_number_name, social_security_number_abbreviation, single_previsional_number_name, single_previsional_number_abbreviation, currency_id, created_at, updated_at, deleted_at) FROM stdin;
1	AND	Andorra	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:16:46	2018-12-13 03:16:46	\N
2	ARE	United Arab Emirates	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2018-12-13 03:16:46	2018-12-13 03:16:46	\N
3	AFG	Afghanistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2018-12-13 03:16:46	2018-12-13 03:16:46	\N
4	ATG	Antigua And Barbuda	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:16:47	2018-12-13 03:16:47	\N
5	AIA	Anguilla	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:16:47	2018-12-13 03:16:47	\N
6	ALB	Albania	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	2018-12-13 03:16:47	2018-12-13 03:16:47	\N
7	ARM	Armenia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	4	2018-12-13 03:16:48	2018-12-13 03:16:48	\N
8	ANT	Netherlands Antilles	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	2018-12-13 03:16:48	2018-12-13 03:16:48	\N
9	AGO	Angola	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	6	2018-12-13 03:16:48	2018-12-13 03:16:48	\N
10	ATA	Antarctica	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:16:48	2018-12-13 03:16:48	\N
11	ARG	Argentina	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	7	2018-12-13 03:16:49	2018-12-13 03:16:49	\N
12	ASM	American Samoa	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:16:49	2018-12-13 03:16:49	\N
13	AUT	Austria	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:16:49	2018-12-13 03:16:49	\N
14	AUS	Australia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:16:50	2018-12-13 03:16:50	\N
15	ABW	Aruba	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	10	2018-12-13 03:16:50	2018-12-13 03:16:50	\N
16	AZE	Azerbaijan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	2018-12-13 03:16:50	2018-12-13 03:16:50	\N
17	BIH	Bosnia And Herzegovina	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	12	2018-12-13 03:16:50	2018-12-13 03:16:50	\N
18	BRB	Barbados	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	13	2018-12-13 03:16:51	2018-12-13 03:16:51	\N
19	BGD	Bangladesh	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	14	2018-12-13 03:16:51	2018-12-13 03:16:51	\N
20	BEL	Belgium	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:16:51	2018-12-13 03:16:51	\N
21	BFA	Burkina Faso	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:16:52	2018-12-13 03:16:52	\N
22	BGR	Bulgaria	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	16	2018-12-13 03:16:52	2018-12-13 03:16:52	\N
23	BHR	Bahrain	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	18	2018-12-13 03:16:52	2018-12-13 03:16:52	\N
24	BDI	Burundi	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	19	2018-12-13 03:16:52	2018-12-13 03:16:52	\N
25	BEN	Benin	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:16:53	2018-12-13 03:16:53	\N
26	BMU	Bermuda	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	20	2018-12-13 03:16:53	2018-12-13 03:16:53	\N
27	BRN	Brunei Darussalam	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	21	2018-12-13 03:16:53	2018-12-13 03:16:53	\N
28	BOL	Bolivia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	22	2018-12-13 03:16:54	2018-12-13 03:16:54	\N
29	BRA	Brazil	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	23	2018-12-13 03:16:54	2018-12-13 03:16:54	\N
30	BHS	Bahamas	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	24	2018-12-13 03:16:54	2018-12-13 03:16:54	\N
31	BTN	Bhutan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	72	2018-12-13 03:16:54	2018-12-13 03:16:54	\N
32	BVT	Bouvet Island	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:16:55	2018-12-13 03:16:55	\N
33	BWA	Botswana	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	26	2018-12-13 03:16:55	2018-12-13 03:16:55	\N
34	BLR	Belarus	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	27	2018-12-13 03:16:55	2018-12-13 03:16:55	\N
35	BLZ	Belize	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	28	2018-12-13 03:16:55	2018-12-13 03:16:55	\N
36	CAN	Canada	Province	system/country.province	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	29	2018-12-13 03:16:56	2018-12-13 03:16:56	\N
37	CCK	Cocos (Keeling) Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:16:56	2018-12-13 03:16:56	\N
38	COD	Congo The Democratic Republic Of The	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	30	2018-12-13 03:16:56	2018-12-13 03:16:56	\N
39	CAF	Central African Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:16:57	2018-12-13 03:16:57	\N
40	COG	Congo	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:16:57	2018-12-13 03:16:57	\N
41	CHE	Switzerland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	31	2018-12-13 03:16:57	2018-12-13 03:16:57	\N
42	CIV	Cote D'Ivoire	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:16:57	2018-12-13 03:16:57	\N
43	COK	Cook Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	116	2018-12-13 03:16:58	2018-12-13 03:16:58	\N
44	CHL	Chile	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	32	2018-12-13 03:16:58	2018-12-13 03:16:58	\N
45	CMR	Cameroon	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:16:58	2018-12-13 03:16:58	\N
46	CHN	China	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	33	2018-12-13 03:16:58	2018-12-13 03:16:58	\N
47	COL	Colombia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	34	2018-12-13 03:16:59	2018-12-13 03:16:59	\N
48	CRI	Costa Rica	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	35	2018-12-13 03:16:59	2018-12-13 03:16:59	\N
49	CUB	Cuba	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	36	2018-12-13 03:16:59	2018-12-13 03:16:59	\N
50	CPV	Cape Verde	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	37	2018-12-13 03:17:00	2018-12-13 03:17:00	\N
51	CXR	Christmas Island	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:00	2018-12-13 03:17:00	\N
52	CYP	Cyprus	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	38	2018-12-13 03:17:00	2018-12-13 03:17:00	\N
53	CZE	Czech Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	39	2018-12-13 03:17:00	2018-12-13 03:17:00	\N
54	DEU	Germany	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:01	2018-12-13 03:17:01	\N
55	DJI	Djibouti	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	41	2018-12-13 03:17:01	2018-12-13 03:17:01	\N
56	DNK	Denmark	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	42	2018-12-13 03:17:01	2018-12-13 03:17:01	\N
57	DMA	Dominica	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:01	2018-12-13 03:17:01	\N
58	DOM	Dominican Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	43	2018-12-13 03:17:02	2018-12-13 03:17:02	\N
59	DZA	Algeria	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	44	2018-12-13 03:17:02	2018-12-13 03:17:02	\N
60	ECU	Ecuador	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:02	2018-12-13 03:17:02	\N
61	EST	Estonia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	45	2018-12-13 03:17:03	2018-12-13 03:17:03	\N
62	EGY	Egypt	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	46	2018-12-13 03:17:03	2018-12-13 03:17:03	\N
63	ESH	Western Sahara	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:03	2018-12-13 03:17:03	\N
64	ERI	Eritrea	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	47	2018-12-13 03:17:03	2018-12-13 03:17:03	\N
65	ESP	Spain	Provincia	system/country.provincia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:04	2018-12-13 03:17:04	\N
66	ETH	Ethiopia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	49	2018-12-13 03:17:04	2018-12-13 03:17:04	\N
67	FIN	Finland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:04	2018-12-13 03:17:04	\N
68	FJI	Fiji	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	52	2018-12-13 03:17:04	2018-12-13 03:17:04	\N
69	FLK	Falkland Islands (Malvinas)	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	53	2018-12-13 03:17:05	2018-12-13 03:17:05	\N
70	FSM	Micronesia Federated States Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:05	2018-12-13 03:17:05	\N
71	FRO	Faroe Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	42	2018-12-13 03:17:05	2018-12-13 03:17:05	\N
72	FRA	France	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:06	2018-12-13 03:17:06	\N
73	GAB	Gabon	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:17:06	2018-12-13 03:17:06	\N
74	GBR	United Kingdom	County	system/country.county	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	55	2018-12-13 03:17:06	2018-12-13 03:17:06	\N
75	GRD	Grenada	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:06	2018-12-13 03:17:06	\N
76	GEO	Georgia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	56	2018-12-13 03:17:07	2018-12-13 03:17:07	\N
77	GUF	French Guiana	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:07	2018-12-13 03:17:07	\N
78	GHA	Ghana	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	57	2018-12-13 03:17:07	2018-12-13 03:17:07	\N
79	GIB	Gibraltar	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	58	2018-12-13 03:17:07	2018-12-13 03:17:07	\N
80	GRL	Greenland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	42	2018-12-13 03:17:08	2018-12-13 03:17:08	\N
81	GMB	Gambia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	59	2018-12-13 03:17:08	2018-12-13 03:17:08	\N
82	GIN	Guinea	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	60	2018-12-13 03:17:08	2018-12-13 03:17:08	\N
83	GLP	Guadeloupe	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:09	2018-12-13 03:17:09	\N
84	GNQ	Equatorial Guinea	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:17:09	2018-12-13 03:17:09	\N
85	GRC	Greece	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:09	2018-12-13 03:17:09	\N
86	SGS	South Georgia And The South Sandwich Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:10	2018-12-13 03:17:10	\N
87	GTM	Guatemala	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	61	2018-12-13 03:17:10	2018-12-13 03:17:10	\N
88	GUM	Guam	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:10	2018-12-13 03:17:10	\N
89	GNB	Guinea-Bissau	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	62	2018-12-13 03:17:10	2018-12-13 03:17:10	\N
90	GUY	Guyana	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	63	2018-12-13 03:17:11	2018-12-13 03:17:11	\N
91	HKG	Hong Kong	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	64	2018-12-13 03:17:11	2018-12-13 03:17:11	\N
92	HMD	Heard Island And Mcdonald Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:11	2018-12-13 03:17:11	\N
93	HND	Honduras	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	65	2018-12-13 03:17:11	2018-12-13 03:17:11	\N
94	HRV	Croatia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	66	2018-12-13 03:17:12	2018-12-13 03:17:12	\N
95	HTI	Haiti	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	67	2018-12-13 03:17:12	2018-12-13 03:17:12	\N
96	HUN	Hungary	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	68	2018-12-13 03:17:12	2018-12-13 03:17:12	\N
97	IDN	Indonesia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	69	2018-12-13 03:17:13	2018-12-13 03:17:13	\N
98	IRL	Ireland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:13	2018-12-13 03:17:13	\N
99	ISR	Israel	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71	2018-12-13 03:17:13	2018-12-13 03:17:13	\N
100	IND	India	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	72	2018-12-13 03:17:13	2018-12-13 03:17:13	\N
101	IOT	British Indian Ocean Territory	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:14	2018-12-13 03:17:14	\N
102	IRQ	Iraq	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	73	2018-12-13 03:17:14	2018-12-13 03:17:14	\N
103	IRN	Iran Islamic Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	74	2018-12-13 03:17:14	2018-12-13 03:17:14	\N
104	ISL	Iceland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	75	2018-12-13 03:17:15	2018-12-13 03:17:15	\N
105	ITA	Italy	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:15	2018-12-13 03:17:15	\N
106	JAM	Jamaica	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	77	2018-12-13 03:17:15	2018-12-13 03:17:15	\N
107	JOR	Jordan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	78	2018-12-13 03:17:15	2018-12-13 03:17:15	\N
108	JPN	Japan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	79	2018-12-13 03:17:16	2018-12-13 03:17:16	\N
109	KEN	Kenya	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	80	2018-12-13 03:17:16	2018-12-13 03:17:16	\N
110	KGZ	Kyrgyzstan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	81	2018-12-13 03:17:16	2018-12-13 03:17:16	\N
111	KHM	Cambodia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	82	2018-12-13 03:17:16	2018-12-13 03:17:16	\N
112	KIR	Kiribati	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:17	2018-12-13 03:17:17	\N
113	COM	Comoros	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	83	2018-12-13 03:17:17	2018-12-13 03:17:17	\N
114	KNA	Saint Kitts And Nevis	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:17	2018-12-13 03:17:17	\N
115	PRK	Korea Democratic People's Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	84	2018-12-13 03:17:18	2018-12-13 03:17:18	\N
116	KOR	Korea Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	85	2018-12-13 03:17:18	2018-12-13 03:17:18	\N
117	KWT	Kuwait	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	86	2018-12-13 03:17:18	2018-12-13 03:17:18	\N
118	CYM	Cayman Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	87	2018-12-13 03:17:18	2018-12-13 03:17:18	\N
119	KAZ	Kazakhstan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	88	2018-12-13 03:17:19	2018-12-13 03:17:19	\N
120	LAO	Lao People's Democratic Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	89	2018-12-13 03:17:19	2018-12-13 03:17:19	\N
121	LBN	Lebanon	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	90	2018-12-13 03:17:19	2018-12-13 03:17:19	\N
122	LCA	Saint Lucia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:19	2018-12-13 03:17:19	\N
123	LIE	Liechtenstein	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	31	2018-12-13 03:17:20	2018-12-13 03:17:20	\N
124	LKA	Sri Lanka	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	91	2018-12-13 03:17:20	2018-12-13 03:17:20	\N
125	LBR	Liberia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	92	2018-12-13 03:17:20	2018-12-13 03:17:20	\N
126	LSO	Lesotho	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	173	2018-12-13 03:17:21	2018-12-13 03:17:21	\N
127	LTU	Lithuania	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	93	2018-12-13 03:17:21	2018-12-13 03:17:21	\N
128	LUX	Luxembourg	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:21	2018-12-13 03:17:21	\N
129	LVA	Latvia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	94	2018-12-13 03:17:21	2018-12-13 03:17:21	\N
130	LBY	Libyan Arab Jamahiriya	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	95	2018-12-13 03:17:22	2018-12-13 03:17:22	\N
131	MAR	Morocco	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	96	2018-12-13 03:17:22	2018-12-13 03:17:22	\N
132	MCO	Monaco	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:22	2018-12-13 03:17:22	\N
133	MDA	Moldova	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	97	2018-12-13 03:17:22	2018-12-13 03:17:22	\N
134	MNE	Montenegro	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:23	2018-12-13 03:17:23	\N
135	MDG	Madagascar	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	98	2018-12-13 03:17:23	2018-12-13 03:17:23	\N
136	MHL	Marshall Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:23	2018-12-13 03:17:23	\N
137	MKD	Macedonia, The Former Yugoslav Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	99	2018-12-13 03:17:24	2018-12-13 03:17:24	\N
138	MLI	Mali	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:17:24	2018-12-13 03:17:24	\N
139	MMR	Myanmar	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100	2018-12-13 03:17:24	2018-12-13 03:17:24	\N
140	MNG	Mongolia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	101	2018-12-13 03:17:24	2018-12-13 03:17:24	\N
141	MAC	Macao	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	102	2018-12-13 03:17:25	2018-12-13 03:17:25	\N
142	MNP	Northern Mariana Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:25	2018-12-13 03:17:25	\N
143	MTQ	Martinique	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:25	2018-12-13 03:17:25	\N
144	MRT	Mauritania	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	103	2018-12-13 03:17:26	2018-12-13 03:17:26	\N
145	MSR	Montserrat	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:26	2018-12-13 03:17:26	\N
146	MLT	Malta	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	104	2018-12-13 03:17:26	2018-12-13 03:17:26	\N
147	MUS	Mauritius	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	105	2018-12-13 03:17:26	2018-12-13 03:17:26	\N
148	MDV	Maldives	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	106	2018-12-13 03:17:27	2018-12-13 03:17:27	\N
149	MWI	Malawi	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	107	2018-12-13 03:17:27	2018-12-13 03:17:27	\N
150	MEX	Mexico	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	108	2018-12-13 03:17:27	2018-12-13 03:17:27	\N
151	MYS	Malaysia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	109	2018-12-13 03:17:27	2018-12-13 03:17:27	\N
152	MOZ	Mozambique	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	110	2018-12-13 03:17:28	2018-12-13 03:17:28	\N
153	NAM	Namibia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	173	2018-12-13 03:17:28	2018-12-13 03:17:28	\N
154	NCL	New Caledonia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	170	2018-12-13 03:17:28	2018-12-13 03:17:28	\N
155	NER	Niger	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:17:29	2018-12-13 03:17:29	\N
156	NFK	Norfolk Island	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:29	2018-12-13 03:17:29	\N
157	NGA	Nigeria	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	111	2018-12-13 03:17:29	2018-12-13 03:17:29	\N
158	NIC	Nicaragua	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	112	2018-12-13 03:17:29	2018-12-13 03:17:29	\N
159	NLD	Netherlands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:30	2018-12-13 03:17:30	\N
160	NOR	Norway	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	114	2018-12-13 03:17:30	2018-12-13 03:17:30	\N
161	NPL	Nepal	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	115	2018-12-13 03:17:30	2018-12-13 03:17:30	\N
162	NRU	Nauru	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:30	2018-12-13 03:17:30	\N
163	NIU	Niue	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	116	2018-12-13 03:17:31	2018-12-13 03:17:31	\N
164	NZL	New Zealand	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	116	2018-12-13 03:17:31	2018-12-13 03:17:31	\N
165	OMN	Oman	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	117	2018-12-13 03:17:31	2018-12-13 03:17:31	\N
166	PAN	Panama	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	118	2018-12-13 03:17:32	2018-12-13 03:17:32	\N
167	PER	Peru	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	119	2018-12-13 03:17:32	2018-12-13 03:17:32	\N
168	PYF	French Polynesia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	170	2018-12-13 03:17:32	2018-12-13 03:17:32	\N
169	PNG	Papua New Guinea	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	120	2018-12-13 03:17:32	2018-12-13 03:17:32	\N
170	PHL	Philippines	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	121	2018-12-13 03:17:33	2018-12-13 03:17:33	\N
171	PAK	Pakistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	122	2018-12-13 03:17:33	2018-12-13 03:17:33	\N
172	POL	Poland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	123	2018-12-13 03:17:33	2018-12-13 03:17:33	\N
173	SPM	Saint Pierre And Miquelon	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:33	2018-12-13 03:17:33	\N
174	PCN	Pitcairn	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:34	2018-12-13 03:17:34	\N
175	PRI	Puerto Rico	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:34	2018-12-13 03:17:34	\N
176	PSE	Palestinian Territory Occupied	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:34	2018-12-13 03:17:34	\N
177	PRT	Portugal	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:35	2018-12-13 03:17:35	\N
178	PLW	Palau	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:35	2018-12-13 03:17:35	\N
179	PRY	Paraguay	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	125	2018-12-13 03:17:35	2018-12-13 03:17:35	\N
180	QAT	Qatar	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	126	2018-12-13 03:17:35	2018-12-13 03:17:35	\N
181	REU	Réunion	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:36	2018-12-13 03:17:36	\N
182	ROU	Romania	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	127	2018-12-13 03:17:36	2018-12-13 03:17:36	\N
183	SRB	Serbia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	129	2018-12-13 03:17:36	2018-12-13 03:17:36	\N
184	RUS	Russian Federation	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	130	2018-12-13 03:17:37	2018-12-13 03:17:37	\N
185	RWA	Rwanda	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	131	2018-12-13 03:17:37	2018-12-13 03:17:37	\N
186	SAU	Saudi Arabia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	132	2018-12-13 03:17:37	2018-12-13 03:17:37	\N
187	SLB	Solomon Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	133	2018-12-13 03:17:38	2018-12-13 03:17:38	\N
188	SYC	Seychelles	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	134	2018-12-13 03:17:38	2018-12-13 03:17:38	\N
189	SDN	Sudan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	135	2018-12-13 03:17:38	2018-12-13 03:17:38	\N
190	SWE	Sweden	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	136	2018-12-13 03:17:38	2018-12-13 03:17:38	\N
191	SGP	Singapore	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	138	2018-12-13 03:17:39	2018-12-13 03:17:39	\N
192	SHN	Saint Helena	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	139	2018-12-13 03:17:39	2018-12-13 03:17:39	\N
193	SVN	Slovenia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:39	2018-12-13 03:17:39	\N
194	SJM	Svalbard And Jan Mayen	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:39	2018-12-13 03:17:39	\N
195	SVK	Slovakia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	140	2018-12-13 03:17:40	2018-12-13 03:17:40	\N
196	SLE	Sierra Leone	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	141	2018-12-13 03:17:40	2018-12-13 03:17:40	\N
197	SMR	San Marino	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:40	2018-12-13 03:17:40	\N
198	SEN	Senegal	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:17:41	2018-12-13 03:17:41	\N
199	SOM	Somalia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	142	2018-12-13 03:17:41	2018-12-13 03:17:41	\N
200	SUR	Suriname	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	143	2018-12-13 03:17:41	2018-12-13 03:17:41	\N
201	STP	Sao Tome And Principe	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	144	2018-12-13 03:17:41	2018-12-13 03:17:41	\N
202	SLV	El Salvador	State	system/country.department	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:42	2018-12-13 03:17:42	\N
203	SYR	Syrian Arab Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	146	2018-12-13 03:17:42	2018-12-13 03:17:42	\N
204	SWZ	Swaziland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	147	2018-12-13 03:17:42	2018-12-13 03:17:42	\N
205	TCA	Turks And Caicos Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:42	2018-12-13 03:17:42	\N
206	TCD	Chad	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:17:43	2018-12-13 03:17:43	\N
207	ATF	French Southern Territories	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:43	2018-12-13 03:17:43	\N
208	TGO	Togo	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:17:43	2018-12-13 03:17:43	\N
209	THA	Thailand	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	148	2018-12-13 03:17:44	2018-12-13 03:17:44	\N
210	TJK	Tajikistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	149	2018-12-13 03:17:44	2018-12-13 03:17:44	\N
211	TKL	Tokelau	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	116	2018-12-13 03:17:44	2018-12-13 03:17:44	\N
212	TLS	Timor-Leste	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:44	2018-12-13 03:17:44	\N
213	TKM	Turkmenistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	150	2018-12-13 03:17:45	2018-12-13 03:17:45	\N
214	TUN	Tunisia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	151	2018-12-13 03:17:45	2018-12-13 03:17:45	\N
215	TON	Tonga	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	152	2018-12-13 03:17:45	2018-12-13 03:17:45	\N
216	TUR	Turkey	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	154	2018-12-13 03:17:46	2018-12-13 03:17:46	\N
217	TTO	Trinidad And Tobago	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	155	2018-12-13 03:17:46	2018-12-13 03:17:46	\N
218	TUV	Tuvalu	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:46	2018-12-13 03:17:46	\N
219	TWN	Taiwan, Province Of China	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	156	2018-12-13 03:17:46	2018-12-13 03:17:46	\N
220	TZA	Tanzania United Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	157	2018-12-13 03:17:47	2018-12-13 03:17:47	\N
221	UKR	Ukraine	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	158	2018-12-13 03:17:47	2018-12-13 03:17:47	\N
222	UGA	Uganda	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	159	2018-12-13 03:17:47	2018-12-13 03:17:47	\N
223	UMI	United States Minor Outlying Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:47	2018-12-13 03:17:47	\N
224	USA	United States	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:48	2018-12-13 03:17:48	\N
225	URY	Uruguay	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	161	2018-12-13 03:17:48	2018-12-13 03:17:48	\N
226	UZB	Uzbekistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	162	2018-12-13 03:17:48	2018-12-13 03:17:48	\N
227	VAT	Holy See (Vatican City State)	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:49	2018-12-13 03:17:49	\N
228	VCT	Saint Vincent And The Grenadines	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:49	2018-12-13 03:17:49	\N
229	VEN	Venezuela	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	163	2018-12-13 03:17:49	2018-12-13 03:17:49	\N
230	VGB	Virgin Islands British	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:49	2018-12-13 03:17:49	\N
231	VIR	Virgin Islands U.S.	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:50	2018-12-13 03:17:50	\N
232	VNM	Viet Nam	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	164	2018-12-13 03:17:50	2018-12-13 03:17:50	\N
233	VUT	Vanuatu	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	165	2018-12-13 03:17:50	2018-12-13 03:17:50	\N
234	WLF	Wallis And Futuna	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	170	2018-12-13 03:17:50	2018-12-13 03:17:50	\N
235	WSM	Samoa	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	166	2018-12-13 03:17:51	2018-12-13 03:17:51	\N
236	YEM	Yemen	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	171	2018-12-13 03:17:51	2018-12-13 03:17:51	\N
237	MYT	Mayotte	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:51	2018-12-13 03:17:51	\N
238	ZAF	South Africa	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	173	2018-12-13 03:17:52	2018-12-13 03:17:52	\N
239	ZMB	Zambia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	174	2018-12-13 03:17:52	2018-12-13 03:17:52	\N
240	ZWE	Zimbabwe	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	175	2018-12-13 03:17:52	2018-12-13 03:17:52	\N
\.


--
-- TOC entry 3543 (class 0 OID 16535)
-- Dependencies: 239
-- Data for Name: SYS_Currency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Currency" (id, iso_code, symbol, name, standard_precision, costing_precision, price_precision, currency_symbol_at_the_right, created_at, updated_at, deleted_at) FROM stdin;
2	AFA	Af	Afghani	2	2	2	f	2018-12-13 03:15:58	2018-12-13 03:15:58	\N
3	ALL	L	Lek	2	2	2	f	2018-12-13 03:15:58	2018-12-13 03:15:58	\N
4	AMD	դր.	Armenian Dram	2	2	2	f	2018-12-13 03:15:58	2018-12-13 03:15:58	\N
5	ANG	NAf.	Netherlands Antillian Guilder	2	2	2	f	2018-12-13 03:15:59	2018-12-13 03:15:59	\N
6	AOA	Kz	Kwanza	2	2	2	f	2018-12-13 03:15:59	2018-12-13 03:15:59	\N
7	ARS	$	Argentine Peso	2	2	2	f	2018-12-13 03:15:59	2018-12-13 03:15:59	\N
8	ATS	Sch	Austrian Schilling	2	2	2	f	2018-12-13 03:15:59	2018-12-13 03:15:59	\N
9	AUD	$	Australian Dollar	2	2	2	f	2018-12-13 03:16:00	2018-12-13 03:16:00	\N
10	AWG	ƒ	Aruban Guilder	2	2	2	f	2018-12-13 03:16:00	2018-12-13 03:16:00	\N
11	AZM	m	Azerbaijanian Manat	2	2	2	f	2018-12-13 03:16:00	2018-12-13 03:16:00	\N
12	BAM	KM	Convertible Marks	2	2	2	f	2018-12-13 03:16:00	2018-12-13 03:16:00	\N
13	BBD	Bds$	Barbados Dollar	2	2	2	f	2018-12-13 03:16:01	2018-12-13 03:16:01	\N
14	BDT	৳	Taka	2	2	2	f	2018-12-13 03:16:01	2018-12-13 03:16:01	\N
15	BEF	BFr	Belgian Franc	0	0	0	f	2018-12-13 03:16:01	2018-12-13 03:16:01	\N
16	BGL	Lv	Lev	2	2	2	f	2018-12-13 03:16:02	2018-12-13 03:16:02	\N
17	BGN	лв	Bulgarian Lev	2	2	2	f	2018-12-13 03:16:02	2018-12-13 03:16:02	\N
18	BHD	ب.د	Bahraini Dinar	3	5	5	f	2018-12-13 03:16:02	2018-12-13 03:16:02	\N
19	BIF	FBu	Burundi Franc	0	2	2	f	2018-12-13 03:16:02	2018-12-13 03:16:02	\N
20	BMD	Bd$	Bermudian Dollar	2	2	2	f	2018-12-13 03:16:03	2018-12-13 03:16:03	\N
21	BND	B$	Brunei Dollar	2	2	2	f	2018-12-13 03:16:03	2018-12-13 03:16:03	\N
22	BOB	Bs.	Boliviano	2	2	2	f	2018-12-13 03:16:03	2018-12-13 03:16:03	\N
23	BRL	R$	Brazilian Real	2	2	2	f	2018-12-13 03:16:04	2018-12-13 03:16:04	\N
24	BSD	B$	Bahamian Dollar	2	2	2	f	2018-12-13 03:16:04	2018-12-13 03:16:04	\N
25	BSF	Bs F	Venezuelan BolÃ­var Fuerte	2	2	2	f	2018-12-13 03:16:04	2018-12-13 03:16:04	\N
26	BWP	P	Pula	2	2	2	f	2018-12-13 03:16:04	2018-12-13 03:16:04	\N
27	BYR	BR	Belarussian Ruble	0	2	2	f	2018-12-13 03:16:05	2018-12-13 03:16:05	\N
28	BZD	BZ$	Belize Dollar	2	2	2	f	2018-12-13 03:16:05	2018-12-13 03:16:05	\N
29	CAD	C$	Canadian Dollar	2	2	2	f	2018-12-13 03:16:05	2018-12-13 03:16:05	\N
30	CDF	Fr	Franc Congolais	2	2	2	f	2018-12-13 03:16:05	2018-12-13 03:16:05	\N
31	CHF	SwF	Swiss Franc	2	2	2	f	2018-12-13 03:16:06	2018-12-13 03:16:06	\N
32	CLP	Ch$	Chilean Peso	0	2	2	f	2018-12-13 03:16:06	2018-12-13 03:16:06	\N
33	CNY	¥	Yuan Renminbi	2	2	2	f	2018-12-13 03:16:06	2018-12-13 03:16:06	\N
34	COP	Col$	Colombian Peso	2	2	2	f	2018-12-13 03:16:07	2018-12-13 03:16:07	\N
35	CRC	₡	Costa Rican Colon	2	2	2	f	2018-12-13 03:16:07	2018-12-13 03:16:07	\N
36	CUP	Cu$	Cuban Peso	2	2	2	f	2018-12-13 03:16:07	2018-12-13 03:16:07	\N
37	CVE	C.V.Esc.	Cape Verde Escudo	2	2	2	f	2018-12-13 03:16:07	2018-12-13 03:16:07	\N
38	CYP	£C	Cyprus Pound	2	2	2	f	2018-12-13 03:16:08	2018-12-13 03:16:08	\N
39	CZK	Kč	Czech Koruna	2	2	2	f	2018-12-13 03:16:08	2018-12-13 03:16:08	\N
40	DEM	DM	Deutsche Mark	2	2	2	f	2018-12-13 03:16:08	2018-12-13 03:16:08	\N
41	DJF	DF	Djibouti Franc	0	2	2	f	2018-12-13 03:16:09	2018-12-13 03:16:09	\N
42	DKK	Dkr	Danish Krone	2	2	2	f	2018-12-13 03:16:09	2018-12-13 03:16:09	\N
43	DOP	RD$	Dominican Peso	2	2	2	f	2018-12-13 03:16:09	2018-12-13 03:16:09	\N
44	DZD	د.ج	Algerian Dinar	2	2	2	f	2018-12-13 03:16:09	2018-12-13 03:16:09	\N
45	EEK	KR	Kroon	2	2	2	f	2018-12-13 03:16:10	2018-12-13 03:16:10	\N
46	EGP	£E	Egyptian Pound	2	2	2	f	2018-12-13 03:16:10	2018-12-13 03:16:10	\N
47	ERN	Nfk	Nakfa	2	2	2	f	2018-12-13 03:16:10	2018-12-13 03:16:10	\N
48	ESP	Pts	Spanish Peseta	0	0	0	f	2018-12-13 03:16:10	2018-12-13 03:16:10	\N
49	ETB	Br	Ethiopian Birr	2	2	2	f	2018-12-13 03:16:11	2018-12-13 03:16:11	\N
50	EUR	€	Euro	2	2	2	t	2018-12-13 03:16:11	2018-12-13 03:16:11	\N
51	FIM	FM	Finish Mark	2	2	2	f	2018-12-13 03:16:11	2018-12-13 03:16:11	\N
52	FJD	F$	Fiji Dollar	2	2	2	f	2018-12-13 03:16:12	2018-12-13 03:16:12	\N
53	FKP	£F	Falkland Islands Pound	2	2	2	f	2018-12-13 03:16:12	2018-12-13 03:16:12	\N
54	FRF	Fr	French Franc	2	2	2	f	2018-12-13 03:16:12	2018-12-13 03:16:12	\N
55	GBP	£	British Pound	2	2	2	f	2018-12-13 03:16:12	2018-12-13 03:16:12	\N
56	GEL	ლ	Lari	2	2	2	f	2018-12-13 03:16:13	2018-12-13 03:16:13	\N
57	GHC	¢	Cedi	2	2	2	f	2018-12-13 03:16:13	2018-12-13 03:16:13	\N
58	GIP	£G	Gibraltar Pound	2	2	2	f	2018-12-13 03:16:13	2018-12-13 03:16:13	\N
59	GMD	D	Dalasi	2	2	2	f	2018-12-13 03:16:14	2018-12-13 03:16:14	\N
60	GNF	Fr	Guinea Franc	0	2	2	f	2018-12-13 03:16:14	2018-12-13 03:16:14	\N
61	GTQ	Q	Quetzal	2	2	2	f	2018-12-13 03:16:14	2018-12-13 03:16:14	\N
62	GWP	$	Guinea-Bissau Peso	2	2	2	f	2018-12-13 03:16:14	2018-12-13 03:16:14	\N
63	GYD	G$	Guyana Dollar	2	2	2	f	2018-12-13 03:16:15	2018-12-13 03:16:15	\N
64	HKD	HK$	Hong Kong Dollar	2	2	2	f	2018-12-13 03:16:15	2018-12-13 03:16:15	\N
65	HNL	L	Lempira	2	2	2	f	2018-12-13 03:16:15	2018-12-13 03:16:15	\N
66	HRK	kn	Croatian Kuna	2	2	2	f	2018-12-13 03:16:15	2018-12-13 03:16:15	\N
67	HTG	G	Gourde	2	2	2	f	2018-12-13 03:16:16	2018-12-13 03:16:16	\N
68	HUF	Ft	Forint	2	2	2	f	2018-12-13 03:16:16	2018-12-13 03:16:16	\N
69	IDR	Rp	Rupiah	2	2	2	f	2018-12-13 03:16:16	2018-12-13 03:16:16	\N
70	IEP	I£	Irish Pound	2	2	2	f	2018-12-13 03:16:17	2018-12-13 03:16:17	\N
71	ILS	₪	New Israeli Sheqel	2	2	2	f	2018-12-13 03:16:17	2018-12-13 03:16:17	\N
72	INR	Rs	Indian Rupee	2	2	2	f	2018-12-13 03:16:17	2018-12-13 03:16:17	\N
73	IQD	ع.د	Iraqi Dinar	3	5	5	f	2018-12-13 03:16:17	2018-12-13 03:16:17	\N
74	IRR	﷼	Iranian Rial	2	2	2	f	2018-12-13 03:16:18	2018-12-13 03:16:18	\N
75	ISK	IKr	Iceland Krona	2	2	2	f	2018-12-13 03:16:18	2018-12-13 03:16:18	\N
76	ITL	L	Italian Lira	0	0	0	f	2018-12-13 03:16:18	2018-12-13 03:16:18	\N
77	JMD	J$	Jamaican Dollar	2	2	2	f	2018-12-13 03:16:19	2018-12-13 03:16:19	\N
78	JOD	د.ا	Jordanian Dinar	3	5	5	f	2018-12-13 03:16:19	2018-12-13 03:16:19	\N
79	JPY	¥	Japanese Yen	0	0	0	f	2018-12-13 03:16:19	2018-12-13 03:16:19	\N
80	KES	K Sh	Kenyan Shilling	2	2	2	f	2018-12-13 03:16:19	2018-12-13 03:16:19	\N
81	KGS	som	Som	2	2	2	f	2018-12-13 03:16:20	2018-12-13 03:16:20	\N
82	KHR	CR	Riel	2	2	2	f	2018-12-13 03:16:20	2018-12-13 03:16:20	\N
83	KMF	CF	Comoro Franc	0	2	2	f	2018-12-13 03:16:20	2018-12-13 03:16:20	\N
84	KPW	₩	North Korean Won	2	2	2	f	2018-12-13 03:16:20	2018-12-13 03:16:20	\N
85	KRW	W	Won	0	2	2	f	2018-12-13 03:16:21	2018-12-13 03:16:21	\N
86	KWD	د.ك	Kuwaiti Dinar	3	5	5	f	2018-12-13 03:16:21	2018-12-13 03:16:21	\N
87	KYD	CI$	Cayman Islands Dollar	2	2	2	f	2018-12-13 03:16:21	2018-12-13 03:16:21	\N
88	KZT	〒	Tenge	2	2	2	f	2018-12-13 03:16:22	2018-12-13 03:16:22	\N
89	LAK	₭	Kip	2	2	2	f	2018-12-13 03:16:22	2018-12-13 03:16:22	\N
90	LBP	ل.ل	Lebanese Pound	2	2	2	f	2018-12-13 03:16:22	2018-12-13 03:16:22	\N
91	LKR	SLRs	Sri Lanka Rupee	2	2	2	f	2018-12-13 03:16:22	2018-12-13 03:16:22	\N
92	LRD	$	Liberian Dollar	2	2	2	f	2018-12-13 03:16:23	2018-12-13 03:16:23	\N
93	LTL	Lt	Lithuanian Litas	2	2	2	f	2018-12-13 03:16:23	2018-12-13 03:16:23	\N
94	LVL	ل.د	Latvian Lats	2	2	2	f	2018-12-13 03:16:23	2018-12-13 03:16:23	\N
95	LYD	LD	Libyan Dinar	3	5	5	f	2018-12-13 03:16:23	2018-12-13 03:16:23	\N
96	MAD	د.م.	Moroccan Dirham	2	2	2	f	2018-12-13 03:16:24	2018-12-13 03:16:24	\N
97	MDL	L	Moldovan Leu	2	2	2	f	2018-12-13 03:16:24	2018-12-13 03:16:24	\N
98	MGF	FMG	Malagasy Franc	0	2	2	f	2018-12-13 03:16:24	2018-12-13 03:16:24	\N
99	MKD	ден	Denar	2	2	2	f	2018-12-13 03:16:25	2018-12-13 03:16:25	\N
100	MMK	K	Kyat	2	2	2	f	2018-12-13 03:16:25	2018-12-13 03:16:25	\N
101	MNT	Tug	Tugrik	2	2	2	f	2018-12-13 03:16:25	2018-12-13 03:16:25	\N
102	MOP	P	Pataca	2	2	2	f	2018-12-13 03:16:25	2018-12-13 03:16:25	\N
103	MRO	UM	Ouguiya	2	2	2	f	2018-12-13 03:16:26	2018-12-13 03:16:26	\N
104	MTL	Lm	Maltese Lira	2	2	2	f	2018-12-13 03:16:26	2018-12-13 03:16:26	\N
105	MUR	Mau Rs	Mauritius Rupee	2	2	2	f	2018-12-13 03:16:26	2018-12-13 03:16:26	\N
106	MVR	Rf	Rufiyaa	2	2	2	f	2018-12-13 03:16:26	2018-12-13 03:16:26	\N
107	MWK	MK	Kwacha	2	2	2	f	2018-12-13 03:16:27	2018-12-13 03:16:27	\N
108	MXN	$	Mexican Peso	2	2	2	f	2018-12-13 03:16:27	2018-12-13 03:16:27	\N
109	MYR	RM	Malaysian Ringgit	2	2	2	f	2018-12-13 03:16:27	2018-12-13 03:16:27	\N
110	MZN	MTn	Metical	2	2	2	f	2018-12-13 03:16:28	2018-12-13 03:16:28	\N
111	NGN	₦	Naira	2	2	2	f	2018-12-13 03:16:28	2018-12-13 03:16:28	\N
112	NIO	C$	Cordoba Oro	2	2	2	f	2018-12-13 03:16:28	2018-12-13 03:16:28	\N
113	NLF	Fl	Dutch Guilder	2	2	2	f	2018-12-13 03:16:28	2018-12-13 03:16:28	\N
114	NOK	NKr	Norwegian Krone	2	2	2	f	2018-12-13 03:16:29	2018-12-13 03:16:29	\N
115	NPR	NRs	Nepalese Rupee	2	2	2	f	2018-12-13 03:16:29	2018-12-13 03:16:29	\N
116	NZD	$	New Zealand Dollar	2	2	2	f	2018-12-13 03:16:29	2018-12-13 03:16:29	\N
117	OMR	ر.ع.	Rial Omani	3	5	5	f	2018-12-13 03:16:30	2018-12-13 03:16:30	\N
118	PAB	B/.	Balboa	2	2	2	f	2018-12-13 03:16:30	2018-12-13 03:16:30	\N
119	PEN	S/.	Nuevo Sol	2	2	2	f	2018-12-13 03:16:30	2018-12-13 03:16:30	\N
120	PGK	K	Kina	2	2	2	f	2018-12-13 03:16:30	2018-12-13 03:16:30	\N
121	PHP	₱	Philippine Peso	2	2	2	f	2018-12-13 03:16:31	2018-12-13 03:16:31	\N
122	PKR	Rs	Pakistan Rupee	2	2	2	f	2018-12-13 03:16:31	2018-12-13 03:16:31	\N
123	PLN	zł	Zloty	2	2	2	f	2018-12-13 03:16:31	2018-12-13 03:16:31	\N
124	PTE	Es	Portugese Escudo	0	0	0	f	2018-12-13 03:16:31	2018-12-13 03:16:31	\N
125	PYG	₲	Guarani	0	2	2	f	2018-12-13 03:16:32	2018-12-13 03:16:32	\N
126	QAR	ر.ق	Qatari Rial	2	2	2	f	2018-12-13 03:16:32	2018-12-13 03:16:32	\N
127	ROL	L	Leu	2	2	2	f	2018-12-13 03:16:32	2018-12-13 03:16:32	\N
128	RON	L	Romanian Leu	2	2	2	f	2018-12-13 03:16:33	2018-12-13 03:16:33	\N
129	RSD	дин	Serbian Dinar	2	2	2	f	2018-12-13 03:16:33	2018-12-13 03:16:33	\N
130	RUB	руб.	Russian Ruble	2	2	2	f	2018-12-13 03:16:33	2018-12-13 03:16:33	\N
131	RWF	RF	Rwanda Franc	0	2	2	f	2018-12-13 03:16:33	2018-12-13 03:16:33	\N
132	SAR	ر.س	Saudi Riyal	2	2	2	f	2018-12-13 03:16:34	2018-12-13 03:16:34	\N
133	SBD	SI$	Solomon Islands Dollar	2	2	2	f	2018-12-13 03:16:34	2018-12-13 03:16:34	\N
134	SCR	SR	Seychelles Rupee	2	2	2	f	2018-12-13 03:16:34	2018-12-13 03:16:34	\N
135	SDG	£	Sudanese Pound	2	2	2	f	2018-12-13 03:16:34	2018-12-13 03:16:34	\N
136	SEK	kr	Swedish Krona	2	2	2	f	2018-12-13 03:16:35	2018-12-13 03:16:35	\N
137	SFR	SFr	Swiss Franc	2	2	2	f	2018-12-13 03:16:35	2018-12-13 03:16:35	\N
138	SGD	S$	Singapore Dollar	2	2	2	f	2018-12-13 03:16:35	2018-12-13 03:16:35	\N
139	SHP	£S	Saint Helena Pound	2	2	2	f	2018-12-13 03:16:36	2018-12-13 03:16:36	\N
140	SKK	Sk	Slovak Koruna	2	2	2	f	2018-12-13 03:16:36	2018-12-13 03:16:36	\N
141	SLL	Le	Leone	2	2	2	f	2018-12-13 03:16:36	2018-12-13 03:16:36	\N
142	SOS	So. Sh.	Somali Shilling	2	2	2	f	2018-12-13 03:16:36	2018-12-13 03:16:36	\N
143	SRD	$	Suriname Dollar	2	2	2	f	2018-12-13 03:16:37	2018-12-13 03:16:37	\N
144	STD	Db	Dobra	2	2	2	f	2018-12-13 03:16:37	2018-12-13 03:16:37	\N
145	SVC	¢	El Salvador Colon	2	2	2	f	2018-12-13 03:16:37	2018-12-13 03:16:37	\N
146	SYP	£S	Syrian Pound	2	2	2	f	2018-12-13 03:16:37	2018-12-13 03:16:37	\N
147	SZL	L	Lilangeni	2	2	2	f	2018-12-13 03:16:38	2018-12-13 03:16:38	\N
148	THB	฿	Baht	2	2	2	f	2018-12-13 03:16:38	2018-12-13 03:16:38	\N
149	TJS	SM	Somoni	2	2	2	f	2018-12-13 03:16:38	2018-12-13 03:16:38	\N
150	TMM	m	Manat	2	2	2	f	2018-12-13 03:16:39	2018-12-13 03:16:39	\N
151	TND	د.ت	Tunisian Dinar	3	5	5	f	2018-12-13 03:16:39	2018-12-13 03:16:39	\N
152	TOP	T$	Pa’anga	2	2	2	f	2018-12-13 03:16:39	2018-12-13 03:16:39	\N
153	TPE	$	Timor Escudo	0	2	2	f	2018-12-13 03:16:39	2018-12-13 03:16:39	\N
154	TRY	YTL	Turkish Lira	0	2	2	f	2018-12-13 03:16:40	2018-12-13 03:16:40	\N
155	TTD	TT$	Trinidad and Tobago Dollar	2	2	2	f	2018-12-13 03:16:40	2018-12-13 03:16:40	\N
156	TWD	NT$	New Taiwan Dollar	2	2	2	f	2018-12-13 03:16:40	2018-12-13 03:16:40	\N
157	TZS	TSh	Tanzanian Shilling	2	2	2	f	2018-12-13 03:16:40	2018-12-13 03:16:40	\N
158	UAH	₴	Hryvnia	2	2	2	f	2018-12-13 03:16:41	2018-12-13 03:16:41	\N
159	UGX	USh	Uganda Shilling	2	2	2	f	2018-12-13 03:16:41	2018-12-13 03:16:41	\N
160	USD	$	US Dollar	2	2	2	f	2018-12-13 03:16:41	2018-12-13 03:16:41	\N
161	UYU	$U	Peso Uruguayo	2	2	2	f	2018-12-13 03:16:42	2018-12-13 03:16:42	\N
162	UZS	som	Uzbekistan Sum	2	2	2	f	2018-12-13 03:16:42	2018-12-13 03:16:42	\N
163	VEB	Bs	Bolivar	2	2	2	f	2018-12-13 03:16:42	2018-12-13 03:16:42	\N
164	VND	₫	Dong	2	2	2	f	2018-12-13 03:16:42	2018-12-13 03:16:42	\N
165	VUV	VT	Vatu	0	2	2	f	2018-12-13 03:16:43	2018-12-13 03:16:43	\N
166	WST	WS$	Tala	2	2	2	f	2018-12-13 03:16:43	2018-12-13 03:16:43	\N
167	XAF	CFAF	CFA Franc BEAC	0	2	2	f	2018-12-13 03:16:43	2018-12-13 03:16:43	\N
168	XCD	EC$	East Caribbean Dollar	2	2	2	f	2018-12-13 03:16:43	2018-12-13 03:16:43	\N
169	XOF	CFAF	CFA Franc BCEAO	0	2	2	f	2018-12-13 03:16:44	2018-12-13 03:16:44	\N
170	XPF	CFPF	CFP Franc	0	2	2	f	2018-12-13 03:16:44	2018-12-13 03:16:44	\N
171	YER	﷼	Yemeni Rial	2	2	2	f	2018-12-13 03:16:44	2018-12-13 03:16:44	\N
172	YUM	Din	Yugoslavian Dinar	2	2	2	f	2018-12-13 03:16:45	2018-12-13 03:16:45	\N
173	ZAR	R	Rand	2	2	2	f	2018-12-13 03:16:45	2018-12-13 03:16:45	\N
174	ZMK	ZK	Kwacha	2	2	2	f	2018-12-13 03:16:45	2018-12-13 03:16:45	\N
175	ZWD	Z$	Zimbabwe Dollar	2	2	2	f	2018-12-13 03:16:45	2018-12-13 03:16:45	\N
1	AED	د.إ	UAE Dirham	2	2	2	f	2018-12-13 03:15:57	2018-12-13 03:15:57	\N
\.


--
-- TOC entry 3545 (class 0 OID 16540)
-- Dependencies: 241
-- Data for Name: SYS_Region; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Region" (id, name, region_code, region_lang_key, country_id, created_at, updated_at, deleted_at) FROM stdin;
1	Alberta		\N	36	2018-12-13 03:17:53	2018-12-13 03:17:53	\N
2	British Columbia		\N	36	2018-12-13 03:17:53	2018-12-13 03:17:53	\N
3	Manitoba		\N	36	2018-12-13 03:17:53	2018-12-13 03:17:53	\N
4	New Brunswick		\N	36	2018-12-13 03:17:53	2018-12-13 03:17:53	\N
5	Newfoundland and Labrador		\N	36	2018-12-13 03:17:54	2018-12-13 03:17:54	\N
6	Nova Scotia		\N	36	2018-12-13 03:17:54	2018-12-13 03:17:54	\N
7	Northwest Territories		\N	36	2018-12-13 03:17:54	2018-12-13 03:17:54	\N
8	Nunavut		\N	36	2018-12-13 03:17:55	2018-12-13 03:17:55	\N
9	Ontario		\N	36	2018-12-13 03:17:55	2018-12-13 03:17:55	\N
10	Prince Edward Island		\N	36	2018-12-13 03:17:55	2018-12-13 03:17:55	\N
11	Québec		\N	36	2018-12-13 03:17:55	2018-12-13 03:17:55	\N
12	Saskatchewan		\N	36	2018-12-13 03:17:56	2018-12-13 03:17:56	\N
13	Yukon		\N	36	2018-12-13 03:17:56	2018-12-13 03:17:56	\N
14	Avon		\N	74	2018-12-13 03:17:56	2018-12-13 03:17:56	\N
15	Befordshire		\N	74	2018-12-13 03:17:57	2018-12-13 03:17:57	\N
16	Buckinghamshire		\N	74	2018-12-13 03:17:57	2018-12-13 03:17:57	\N
17	Cambridgeshire		\N	74	2018-12-13 03:17:57	2018-12-13 03:17:57	\N
18	Cheshire		\N	74	2018-12-13 03:17:57	2018-12-13 03:17:57	\N
19	Cleveland		\N	74	2018-12-13 03:17:58	2018-12-13 03:17:58	\N
20	Cornwall		\N	74	2018-12-13 03:17:58	2018-12-13 03:17:58	\N
21	Cumbria		\N	74	2018-12-13 03:17:58	2018-12-13 03:17:58	\N
22	Derbyshire		\N	74	2018-12-13 03:17:58	2018-12-13 03:17:58	\N
23	Devon		\N	74	2018-12-13 03:17:59	2018-12-13 03:17:59	\N
24	Dorset		\N	74	2018-12-13 03:17:59	2018-12-13 03:17:59	\N
25	Durham		\N	74	2018-12-13 03:17:59	2018-12-13 03:17:59	\N
26	East Sussex		\N	74	2018-12-13 03:18:00	2018-12-13 03:18:00	\N
27	Essex		\N	74	2018-12-13 03:18:00	2018-12-13 03:18:00	\N
28	Gloucestershire		\N	74	2018-12-13 03:18:00	2018-12-13 03:18:00	\N
29	Greater London		\N	74	2018-12-13 03:18:00	2018-12-13 03:18:00	\N
30	Greater Manchester		\N	74	2018-12-13 03:18:01	2018-12-13 03:18:01	\N
31	Hampshire		\N	74	2018-12-13 03:18:01	2018-12-13 03:18:01	\N
32	Herefordshire		\N	74	2018-12-13 03:18:01	2018-12-13 03:18:01	\N
33	Hertfordshire		\N	74	2018-12-13 03:18:02	2018-12-13 03:18:02	\N
34	Humberside		\N	74	2018-12-13 03:18:02	2018-12-13 03:18:02	\N
35	Isle of Wight		\N	74	2018-12-13 03:18:02	2018-12-13 03:18:02	\N
36	Kent		\N	74	2018-12-13 03:18:02	2018-12-13 03:18:02	\N
37	Lancashire		\N	74	2018-12-13 03:18:03	2018-12-13 03:18:03	\N
38	Lincolnshire		\N	74	2018-12-13 03:18:03	2018-12-13 03:18:03	\N
39	Merseyside		\N	74	2018-12-13 03:18:03	2018-12-13 03:18:03	\N
40	Norfolk		\N	74	2018-12-13 03:18:03	2018-12-13 03:18:03	\N
41	Northamtonshire		\N	74	2018-12-13 03:18:04	2018-12-13 03:18:04	\N
42	Northumberland		\N	74	2018-12-13 03:18:04	2018-12-13 03:18:04	\N
43	North Yorkshire		\N	74	2018-12-13 03:18:04	2018-12-13 03:18:04	\N
44	Nottinghamshire		\N	74	2018-12-13 03:18:05	2018-12-13 03:18:05	\N
45	Oxfordshire		\N	74	2018-12-13 03:18:05	2018-12-13 03:18:05	\N
46	Shropshire		\N	74	2018-12-13 03:18:05	2018-12-13 03:18:05	\N
47	Somerset		\N	74	2018-12-13 03:18:06	2018-12-13 03:18:06	\N
48	South Yorkshire		\N	74	2018-12-13 03:18:06	2018-12-13 03:18:06	\N
49	Staffordshire		\N	74	2018-12-13 03:18:06	2018-12-13 03:18:06	\N
50	Suffolk		\N	74	2018-12-13 03:18:06	2018-12-13 03:18:06	\N
51	Surrey		\N	74	2018-12-13 03:18:07	2018-12-13 03:18:07	\N
52	Tyne and Wear		\N	74	2018-12-13 03:18:07	2018-12-13 03:18:07	\N
53	Warwickshire		\N	74	2018-12-13 03:18:07	2018-12-13 03:18:07	\N
54	West Midlands		\N	74	2018-12-13 03:18:08	2018-12-13 03:18:08	\N
55	West Sussex		\N	74	2018-12-13 03:18:08	2018-12-13 03:18:08	\N
56	West Yorkshire		\N	74	2018-12-13 03:18:08	2018-12-13 03:18:08	\N
57	Wiltshire		\N	74	2018-12-13 03:18:08	2018-12-13 03:18:08	\N
58	Worcestershire		\N	74	2018-12-13 03:18:09	2018-12-13 03:18:09	\N
59	Borders		\N	74	2018-12-13 03:18:09	2018-12-13 03:18:09	\N
60	Central (Scotland)		\N	74	2018-12-13 03:18:09	2018-12-13 03:18:09	\N
61	Dumfries and Galloway		\N	74	2018-12-13 03:18:09	2018-12-13 03:18:09	\N
62	Fife		\N	74	2018-12-13 03:18:10	2018-12-13 03:18:10	\N
63	Grampian		\N	74	2018-12-13 03:18:10	2018-12-13 03:18:10	\N
64	Highland		\N	74	2018-12-13 03:18:10	2018-12-13 03:18:10	\N
65	Lothian		\N	74	2018-12-13 03:18:11	2018-12-13 03:18:11	\N
66	Orkney		\N	74	2018-12-13 03:18:11	2018-12-13 03:18:11	\N
67	Shetland		\N	74	2018-12-13 03:18:11	2018-12-13 03:18:11	\N
68	Strathclyde		\N	74	2018-12-13 03:18:11	2018-12-13 03:18:11	\N
69	Tayside		\N	74	2018-12-13 03:18:12	2018-12-13 03:18:12	\N
70	Western Isles		\N	74	2018-12-13 03:18:12	2018-12-13 03:18:12	\N
71	Clwyd		\N	74	2018-12-13 03:18:12	2018-12-13 03:18:12	\N
72	Dyfed		\N	74	2018-12-13 03:18:12	2018-12-13 03:18:12	\N
73	Gwent		\N	74	2018-12-13 03:18:13	2018-12-13 03:18:13	\N
74	Gwynedd		\N	74	2018-12-13 03:18:13	2018-12-13 03:18:13	\N
75	Mid Glamorgan		\N	74	2018-12-13 03:18:13	2018-12-13 03:18:13	\N
76	Powys		\N	74	2018-12-13 03:18:14	2018-12-13 03:18:14	\N
77	South Glamorgan		\N	74	2018-12-13 03:18:14	2018-12-13 03:18:14	\N
78	West Glamorgan		\N	74	2018-12-13 03:18:14	2018-12-13 03:18:14	\N
79	Antrim (NI)		\N	74	2018-12-13 03:18:14	2018-12-13 03:18:14	\N
80	Ards (NI)		\N	74	2018-12-13 03:18:15	2018-12-13 03:18:15	\N
81	Armagh (NI)		\N	74	2018-12-13 03:18:15	2018-12-13 03:18:15	\N
82	Ballymena (NI)		\N	74	2018-12-13 03:18:15	2018-12-13 03:18:15	\N
83	Ballymoney (NI)		\N	74	2018-12-13 03:18:16	2018-12-13 03:18:16	\N
84	Banbridge (NI)		\N	74	2018-12-13 03:18:16	2018-12-13 03:18:16	\N
85	Belfast (NI)		\N	74	2018-12-13 03:18:16	2018-12-13 03:18:16	\N
86	Carrickfergus (NI)		\N	74	2018-12-13 03:18:16	2018-12-13 03:18:16	\N
87	Castlereagh (NI)		\N	74	2018-12-13 03:18:17	2018-12-13 03:18:17	\N
88	Coleraine (NI)		\N	74	2018-12-13 03:18:17	2018-12-13 03:18:17	\N
89	Cookstown (NI)		\N	74	2018-12-13 03:18:18	2018-12-13 03:18:18	\N
90	Craigavon (NI)		\N	74	2018-12-13 03:18:18	2018-12-13 03:18:18	\N
91	Down (NI)		\N	74	2018-12-13 03:18:18	2018-12-13 03:18:18	\N
92	Dungannon (NI)		\N	74	2018-12-13 03:18:18	2018-12-13 03:18:18	\N
93	Fermanagh (NI)		\N	74	2018-12-13 03:18:19	2018-12-13 03:18:19	\N
94	Larne (NI)		\N	74	2018-12-13 03:18:19	2018-12-13 03:18:19	\N
95	Limavady (NI)		\N	74	2018-12-13 03:18:19	2018-12-13 03:18:19	\N
96	Lisburn (NI)		\N	74	2018-12-13 03:18:20	2018-12-13 03:18:20	\N
97	Londonderry (NI)		\N	74	2018-12-13 03:18:20	2018-12-13 03:18:20	\N
98	Magherafelt (NI)		\N	74	2018-12-13 03:18:20	2018-12-13 03:18:20	\N
99	Moyle (NI)		\N	74	2018-12-13 03:18:20	2018-12-13 03:18:20	\N
100	Newry and Mourne (NI)		\N	74	2018-12-13 03:18:21	2018-12-13 03:18:21	\N
101	Newtonabbey (NI)		\N	74	2018-12-13 03:18:21	2018-12-13 03:18:21	\N
102	North Down (NI)		\N	74	2018-12-13 03:18:21	2018-12-13 03:18:21	\N
103	Omagh (NI)		\N	74	2018-12-13 03:18:21	2018-12-13 03:18:21	\N
104	Strabane (NI)		\N	74	2018-12-13 03:18:22	2018-12-13 03:18:22	\N
105	Alderney (CI)		\N	74	2018-12-13 03:18:22	2018-12-13 03:18:22	\N
106	Guernsey (CI)		\N	74	2018-12-13 03:18:22	2018-12-13 03:18:22	\N
107	Jersey (CI)		\N	74	2018-12-13 03:18:23	2018-12-13 03:18:23	\N
108	Isle of Man		\N	74	2018-12-13 03:18:23	2018-12-13 03:18:23	\N
109	Connecticut		\N	224	2018-12-13 03:18:23	2018-12-13 03:18:23	\N
110	California		\N	224	2018-12-13 03:18:23	2018-12-13 03:18:23	\N
111	Massachusetts		\N	224	2018-12-13 03:18:24	2018-12-13 03:18:24	\N
112	Maine		\N	224	2018-12-13 03:18:24	2018-12-13 03:18:24	\N
113	New Hampshire		\N	224	2018-12-13 03:18:24	2018-12-13 03:18:24	\N
114	Rhode Island		\N	224	2018-12-13 03:18:24	2018-12-13 03:18:24	\N
115	New York		\N	224	2018-12-13 03:18:25	2018-12-13 03:18:25	\N
116	New Jersey		\N	224	2018-12-13 03:18:25	2018-12-13 03:18:25	\N
117	Pennsylvania		\N	224	2018-12-13 03:18:25	2018-12-13 03:18:25	\N
118	Virginia		\N	224	2018-12-13 03:18:26	2018-12-13 03:18:26	\N
119	West Virginia		\N	224	2018-12-13 03:18:26	2018-12-13 03:18:26	\N
120	Florida		\N	224	2018-12-13 03:18:26	2018-12-13 03:18:26	\N
121	Alabama		\N	224	2018-12-13 03:18:26	2018-12-13 03:18:26	\N
122	Deleware		\N	224	2018-12-13 03:18:27	2018-12-13 03:18:27	\N
123	Washington		\N	224	2018-12-13 03:18:27	2018-12-13 03:18:27	\N
124	Alaska		\N	224	2018-12-13 03:18:28	2018-12-13 03:18:28	\N
125	Tennessee		\N	224	2018-12-13 03:18:28	2018-12-13 03:18:28	\N
126	Georgia		\N	224	2018-12-13 03:18:28	2018-12-13 03:18:28	\N
127	North Dakota		\N	224	2018-12-13 03:18:29	2018-12-13 03:18:29	\N
128	South Dakato		\N	224	2018-12-13 03:18:29	2018-12-13 03:18:29	\N
129	Michigan		\N	224	2018-12-13 03:18:29	2018-12-13 03:18:29	\N
130	Iowa		\N	224	2018-12-13 03:18:29	2018-12-13 03:18:29	\N
131	Indiana		\N	224	2018-12-13 03:18:30	2018-12-13 03:18:30	\N
132	Ohio		\N	224	2018-12-13 03:18:30	2018-12-13 03:18:30	\N
133	Illinois		\N	224	2018-12-13 03:18:30	2018-12-13 03:18:30	\N
134	South Carolina		\N	224	2018-12-13 03:18:31	2018-12-13 03:18:31	\N
135	North Carolina		\N	224	2018-12-13 03:18:31	2018-12-13 03:18:31	\N
136	Kentucky		\N	224	2018-12-13 03:18:31	2018-12-13 03:18:31	\N
137	Lousiana		\N	224	2018-12-13 03:18:31	2018-12-13 03:18:31	\N
138	Texas		\N	224	2018-12-13 03:18:32	2018-12-13 03:18:32	\N
139	Oklahoma		\N	224	2018-12-13 03:18:32	2018-12-13 03:18:32	\N
140	Mississippi		\N	224	2018-12-13 03:18:32	2018-12-13 03:18:32	\N
141	Montana		\N	224	2018-12-13 03:18:32	2018-12-13 03:18:32	\N
142	Missouri		\N	224	2018-12-13 03:18:33	2018-12-13 03:18:33	\N
143	Kansas		\N	224	2018-12-13 03:18:33	2018-12-13 03:18:33	\N
144	Nebraska		\N	224	2018-12-13 03:18:33	2018-12-13 03:18:33	\N
145	Wyoming		\N	224	2018-12-13 03:18:34	2018-12-13 03:18:34	\N
146	New Mexico		\N	224	2018-12-13 03:18:34	2018-12-13 03:18:34	\N
147	Oregon		\N	224	2018-12-13 03:18:34	2018-12-13 03:18:34	\N
148	Arkansas		\N	224	2018-12-13 03:18:34	2018-12-13 03:18:34	\N
149	Arizona		\N	224	2018-12-13 03:18:35	2018-12-13 03:18:35	\N
150	Utah		\N	224	2018-12-13 03:18:35	2018-12-13 03:18:35	\N
151	Idaho		\N	224	2018-12-13 03:18:35	2018-12-13 03:18:35	\N
152	Nevada		\N	224	2018-12-13 03:18:35	2018-12-13 03:18:35	\N
153	Colorado		\N	224	2018-12-13 03:18:36	2018-12-13 03:18:36	\N
154	Hawaii		\N	224	2018-12-13 03:18:36	2018-12-13 03:18:36	\N
155	Minnesota		\N	224	2018-12-13 03:18:36	2018-12-13 03:18:36	\N
156	Wisconsin		\N	224	2018-12-13 03:18:37	2018-12-13 03:18:37	\N
157	Maryland		\N	224	2018-12-13 03:18:37	2018-12-13 03:18:37	\N
158	Vermont		\N	224	2018-12-13 03:18:37	2018-12-13 03:18:37	\N
159	Ahuachapán		\N	202	2018-12-13 03:18:37	2018-12-13 03:18:37	\N
160	Cabañas		\N	202	2018-12-13 03:18:38	2018-12-13 03:18:38	\N
161	Chalatenango		\N	202	2018-12-13 03:18:38	2018-12-13 03:18:38	\N
162	Cuscatlán		\N	202	2018-12-13 03:18:38	2018-12-13 03:18:38	\N
163	La Libertad		\N	202	2018-12-13 03:18:38	2018-12-13 03:18:38	\N
164	La Paz		\N	202	2018-12-13 03:18:39	2018-12-13 03:18:39	\N
165	La Unión		\N	202	2018-12-13 03:18:39	2018-12-13 03:18:39	\N
166	Morazán		\N	202	2018-12-13 03:18:39	2018-12-13 03:18:39	\N
167	Santa Ana		\N	202	2018-12-13 03:18:40	2018-12-13 03:18:40	\N
168	San Miguel		\N	202	2018-12-13 03:18:40	2018-12-13 03:18:40	\N
169	San Salvador		\N	202	2018-12-13 03:18:40	2018-12-13 03:18:40	\N
170	San Vicente		\N	202	2018-12-13 03:18:40	2018-12-13 03:18:40	\N
171	Sonsonate		\N	202	2018-12-13 03:18:41	2018-12-13 03:18:41	\N
172	Usulután		\N	202	2018-12-13 03:18:41	2018-12-13 03:18:41	\N
\.


--
-- TOC entry 3547 (class 0 OID 16545)
-- Dependencies: 243
-- Data for Name: SYS_Voucher_Type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Voucher_Type" (id, name, lang_key, key, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 3549 (class 0 OID 16550)
-- Dependencies: 245
-- Data for Name: cache; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cache (key, value, expiration) FROM stdin;
\.


--
-- TOC entry 3550 (class 0 OID 16556)
-- Dependencies: 246
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (migration, batch) FROM stdin;
2014_02_28_042556_create_system_tables	1
2014_02_28_050447_create_organization_tables	1
2014_02_28_050455_create_security_tables	1
2015_01_25_182642_create_acct_system_tables	1
2016_09_21_201625_create_file_table	1
2017_01_11_023422_create_sessions_table	1
2017_01_17_173255_create_security_file_table	1
2017_02_01_042031_create_cache_table	1
\.


--
-- TOC entry 3551 (class 0 OID 16559)
-- Dependencies: 247
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sessions (id, user_id, ip_address, user_agent, payload, last_activity) FROM stdin;
\.


--
-- TOC entry 3584 (class 0 OID 0)
-- Dependencies: 197
-- Name: FILE_File_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."FILE_File_id_seq"', 34, true);


--
-- TOC entry 3585 (class 0 OID 0)
-- Dependencies: 248
-- Name: OADH_FREE_Forced_Disappearance_Temps_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Forced_Disappearance_Temps_id_seq"', 1, false);


--
-- TOC entry 3586 (class 0 OID 0)
-- Dependencies: 199
-- Name: ORG_Organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."ORG_Organization_id_seq"', 1, true);


--
-- TOC entry 3587 (class 0 OID 0)
-- Dependencies: 201
-- Name: SEC_Failed_Jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Failed_Jobs_id_seq"', 1, false);


--
-- TOC entry 3588 (class 0 OID 0)
-- Dependencies: 203
-- Name: SEC_File_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_File_id_seq"', 11, true);


--
-- TOC entry 3589 (class 0 OID 0)
-- Dependencies: 206
-- Name: SEC_Journal_Detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Journal_Detail_id_seq"', 161, true);


--
-- TOC entry 3590 (class 0 OID 0)
-- Dependencies: 207
-- Name: SEC_Journal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Journal_id_seq"', 110, true);


--
-- TOC entry 3591 (class 0 OID 0)
-- Dependencies: 209
-- Name: SEC_Menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Menu_id_seq"', 50, true);


--
-- TOC entry 3592 (class 0 OID 0)
-- Dependencies: 211
-- Name: SEC_Module_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Module_id_seq"', 3, true);


--
-- TOC entry 3593 (class 0 OID 0)
-- Dependencies: 214
-- Name: SEC_Permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Permission_id_seq"', 12, true);


--
-- TOC entry 3594 (class 0 OID 0)
-- Dependencies: 217
-- Name: SEC_Role_Menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Role_Menu_id_seq"', 1, false);


--
-- TOC entry 3595 (class 0 OID 0)
-- Dependencies: 219
-- Name: SEC_Role_Permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Role_Permission_id_seq"', 1, false);


--
-- TOC entry 3596 (class 0 OID 0)
-- Dependencies: 220
-- Name: SEC_Role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Role_id_seq"', 1, false);


--
-- TOC entry 3597 (class 0 OID 0)
-- Dependencies: 223
-- Name: SEC_User_Menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_Menu_id_seq"', 45, true);


--
-- TOC entry 3598 (class 0 OID 0)
-- Dependencies: 225
-- Name: SEC_User_Organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_Organization_id_seq"', 1, true);


--
-- TOC entry 3599 (class 0 OID 0)
-- Dependencies: 227
-- Name: SEC_User_Permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_Permission_id_seq"', 1, true);


--
-- TOC entry 3600 (class 0 OID 0)
-- Dependencies: 229
-- Name: SEC_User_Role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_Role_id_seq"', 1, false);


--
-- TOC entry 3601 (class 0 OID 0)
-- Dependencies: 230
-- Name: SEC_User_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_id_seq"', 1, true);


--
-- TOC entry 3602 (class 0 OID 0)
-- Dependencies: 233
-- Name: SYS_Account_Chart_Type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Account_Chart_Type_id_seq"', 1, false);


--
-- TOC entry 3603 (class 0 OID 0)
-- Dependencies: 235
-- Name: SYS_Account_Type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Account_Type_id_seq"', 1, false);


--
-- TOC entry 3604 (class 0 OID 0)
-- Dependencies: 236
-- Name: SYS_Account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Account_id_seq"', 1, false);


--
-- TOC entry 3605 (class 0 OID 0)
-- Dependencies: 238
-- Name: SYS_Country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Country_id_seq"', 240, true);


--
-- TOC entry 3606 (class 0 OID 0)
-- Dependencies: 240
-- Name: SYS_Currency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Currency_id_seq"', 175, true);


--
-- TOC entry 3607 (class 0 OID 0)
-- Dependencies: 242
-- Name: SYS_Region_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Region_id_seq"', 172, true);


--
-- TOC entry 3608 (class 0 OID 0)
-- Dependencies: 244
-- Name: SYS_Voucher_Type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Voucher_Type_id_seq"', 1, false);


--
-- TOC entry 3247 (class 2606 OID 16590)
-- Name: FILE_File FILE_File_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."FILE_File"
    ADD CONSTRAINT "FILE_File_pkey" PRIMARY KEY (id);


--
-- TOC entry 3334 (class 2606 OID 26302)
-- Name: OADH_FREE_Forced_Disappearances_Temps OADH_FREE_Forced_Disappearance_Temps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearances_Temps"
    ADD CONSTRAINT "OADH_FREE_Forced_Disappearance_Temps_pkey" PRIMARY KEY (id);


--
-- TOC entry 3249 (class 2606 OID 16592)
-- Name: ORG_Organization ORG_Organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization"
    ADD CONSTRAINT "ORG_Organization_pkey" PRIMARY KEY (id);


--
-- TOC entry 3254 (class 2606 OID 16594)
-- Name: SEC_Failed_Jobs SEC_Failed_Jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Failed_Jobs"
    ADD CONSTRAINT "SEC_Failed_Jobs_pkey" PRIMARY KEY (id);


--
-- TOC entry 3256 (class 2606 OID 16596)
-- Name: SEC_File SEC_File_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_File"
    ADD CONSTRAINT "SEC_File_pkey" PRIMARY KEY (id);


--
-- TOC entry 3266 (class 2606 OID 16598)
-- Name: SEC_Journal_Detail SEC_Journal_Detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal_Detail"
    ADD CONSTRAINT "SEC_Journal_Detail_pkey" PRIMARY KEY (id);


--
-- TOC entry 3260 (class 2606 OID 16600)
-- Name: SEC_Journal SEC_Journal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal"
    ADD CONSTRAINT "SEC_Journal_pkey" PRIMARY KEY (id);


--
-- TOC entry 3269 (class 2606 OID 16602)
-- Name: SEC_Menu SEC_Menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu"
    ADD CONSTRAINT "SEC_Menu_pkey" PRIMARY KEY (id);


--
-- TOC entry 3274 (class 2606 OID 16604)
-- Name: SEC_Module SEC_Module_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Module"
    ADD CONSTRAINT "SEC_Module_pkey" PRIMARY KEY (id);


--
-- TOC entry 3278 (class 2606 OID 16606)
-- Name: SEC_Permission SEC_Permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Permission"
    ADD CONSTRAINT "SEC_Permission_pkey" PRIMARY KEY (id);


--
-- TOC entry 3283 (class 2606 OID 16608)
-- Name: SEC_Role_Menu SEC_Role_Menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu"
    ADD CONSTRAINT "SEC_Role_Menu_pkey" PRIMARY KEY (id);


--
-- TOC entry 3287 (class 2606 OID 16610)
-- Name: SEC_Role_Permission SEC_Role_Permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission"
    ADD CONSTRAINT "SEC_Role_Permission_pkey" PRIMARY KEY (id);


--
-- TOC entry 3280 (class 2606 OID 16612)
-- Name: SEC_Role SEC_Role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role"
    ADD CONSTRAINT "SEC_Role_pkey" PRIMARY KEY (id);


--
-- TOC entry 3295 (class 2606 OID 16614)
-- Name: SEC_User_Menu SEC_User_Menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT "SEC_User_Menu_pkey" PRIMARY KEY (id);


--
-- TOC entry 3301 (class 2606 OID 16616)
-- Name: SEC_User_Organization SEC_User_Organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization"
    ADD CONSTRAINT "SEC_User_Organization_pkey" PRIMARY KEY (id);


--
-- TOC entry 3305 (class 2606 OID 16618)
-- Name: SEC_User_Permission SEC_User_Permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT "SEC_User_Permission_pkey" PRIMARY KEY (id);


--
-- TOC entry 3311 (class 2606 OID 16620)
-- Name: SEC_User_Role SEC_User_Role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT "SEC_User_Role_pkey" PRIMARY KEY (id);


--
-- TOC entry 3291 (class 2606 OID 16622)
-- Name: SEC_User SEC_User_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User"
    ADD CONSTRAINT "SEC_User_pkey" PRIMARY KEY (id);


--
-- TOC entry 3318 (class 2606 OID 16624)
-- Name: SYS_Account_Chart_Type SYS_Account_Chart_Type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Chart_Type"
    ADD CONSTRAINT "SYS_Account_Chart_Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3320 (class 2606 OID 16626)
-- Name: SYS_Account_Type SYS_Account_Type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Type"
    ADD CONSTRAINT "SYS_Account_Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3316 (class 2606 OID 16628)
-- Name: SYS_Account SYS_Account_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account"
    ADD CONSTRAINT "SYS_Account_pkey" PRIMARY KEY (id);


--
-- TOC entry 3322 (class 2606 OID 16630)
-- Name: SYS_Country SYS_Country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Country"
    ADD CONSTRAINT "SYS_Country_pkey" PRIMARY KEY (id);


--
-- TOC entry 3324 (class 2606 OID 16632)
-- Name: SYS_Currency SYS_Currency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Currency"
    ADD CONSTRAINT "SYS_Currency_pkey" PRIMARY KEY (id);


--
-- TOC entry 3326 (class 2606 OID 16634)
-- Name: SYS_Region SYS_Region_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Region"
    ADD CONSTRAINT "SYS_Region_pkey" PRIMARY KEY (id);


--
-- TOC entry 3328 (class 2606 OID 16636)
-- Name: SYS_Voucher_Type SYS_Voucher_Type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Voucher_Type"
    ADD CONSTRAINT "SYS_Voucher_Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3330 (class 2606 OID 16638)
-- Name: cache cache_key_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cache
    ADD CONSTRAINT cache_key_unique UNIQUE (key);


--
-- TOC entry 3332 (class 2606 OID 16640)
-- Name: sessions sessions_id_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_id_unique UNIQUE (id);


--
-- TOC entry 3335 (class 1259 OID 26308)
-- Name: oadh_free_forced_disappearance_temps_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_forced_disappearance_temps_organization_id_index ON public."OADH_FREE_Forced_Disappearances_Temps" USING btree (organization_id);


--
-- TOC entry 3250 (class 1259 OID 16641)
-- Name: org_organization_cost_price_precision_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX org_organization_cost_price_precision_index ON public."ORG_Organization" USING btree (cost_price_precision);


--
-- TOC entry 3251 (class 1259 OID 16642)
-- Name: org_organization_discount_precision_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX org_organization_discount_precision_index ON public."ORG_Organization" USING btree (discount_precision);


--
-- TOC entry 3252 (class 1259 OID 16643)
-- Name: org_organization_sale_point_quantity_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX org_organization_sale_point_quantity_index ON public."ORG_Organization" USING btree (sale_point_quantity);


--
-- TOC entry 3257 (class 1259 OID 16644)
-- Name: sec_file_file_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_file_file_id_index ON public."SEC_File" USING btree (file_id);


--
-- TOC entry 3258 (class 1259 OID 16645)
-- Name: sec_file_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_file_organization_id_index ON public."SEC_File" USING btree (organization_id);


--
-- TOC entry 3261 (class 1259 OID 16646)
-- Name: sec_journal_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_created_at_index ON public."SEC_Journal" USING btree (created_at);


--
-- TOC entry 3267 (class 1259 OID 16647)
-- Name: sec_journal_detail_journal_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_detail_journal_id_index ON public."SEC_Journal_Detail" USING btree (journal_id);


--
-- TOC entry 3262 (class 1259 OID 16648)
-- Name: sec_journal_journalized_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_journalized_id_index ON public."SEC_Journal" USING btree (journalized_id);


--
-- TOC entry 3263 (class 1259 OID 16649)
-- Name: sec_journal_journalized_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_journalized_type_index ON public."SEC_Journal" USING btree (journalized_type);


--
-- TOC entry 3264 (class 1259 OID 16650)
-- Name: sec_journal_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_user_id_index ON public."SEC_Journal" USING btree (user_id);


--
-- TOC entry 3270 (class 1259 OID 16651)
-- Name: sec_menu_module_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_menu_module_id_index ON public."SEC_Menu" USING btree (module_id);


--
-- TOC entry 3271 (class 1259 OID 16652)
-- Name: sec_menu_parent_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_menu_parent_id_index ON public."SEC_Menu" USING btree (parent_id);


--
-- TOC entry 3272 (class 1259 OID 17787)
-- Name: sec_menu_url_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_menu_url_index ON public."SEC_Menu" USING btree (url);


--
-- TOC entry 3275 (class 1259 OID 16654)
-- Name: sec_password_reminders_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_password_reminders_email_index ON public."SEC_Password_Reminders" USING btree (email);


--
-- TOC entry 3276 (class 1259 OID 16655)
-- Name: sec_password_reminders_token_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_password_reminders_token_index ON public."SEC_Password_Reminders" USING btree (token);


--
-- TOC entry 3284 (class 1259 OID 16656)
-- Name: sec_role_menu_menu_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_menu_menu_id_index ON public."SEC_Role_Menu" USING btree (menu_id);


--
-- TOC entry 3285 (class 1259 OID 16657)
-- Name: sec_role_menu_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_menu_role_id_index ON public."SEC_Role_Menu" USING btree (role_id);


--
-- TOC entry 3281 (class 1259 OID 16658)
-- Name: sec_role_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_organization_id_index ON public."SEC_Role" USING btree (organization_id);


--
-- TOC entry 3288 (class 1259 OID 16659)
-- Name: sec_role_permission_permission_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_permission_permission_id_index ON public."SEC_Role_Permission" USING btree (permission_id);


--
-- TOC entry 3289 (class 1259 OID 16660)
-- Name: sec_role_permission_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_permission_role_id_index ON public."SEC_Role_Permission" USING btree (role_id);


--
-- TOC entry 3292 (class 1259 OID 16661)
-- Name: sec_user_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_email_index ON public."SEC_User" USING btree (email);


--
-- TOC entry 3293 (class 1259 OID 16662)
-- Name: sec_user_is_active_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_is_active_index ON public."SEC_User" USING btree (is_active);


--
-- TOC entry 3296 (class 1259 OID 16663)
-- Name: sec_user_menu_is_assigned_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_menu_is_assigned_index ON public."SEC_User_Menu" USING btree (is_assigned);


--
-- TOC entry 3297 (class 1259 OID 16664)
-- Name: sec_user_menu_menu_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_menu_menu_id_index ON public."SEC_User_Menu" USING btree (menu_id);


--
-- TOC entry 3298 (class 1259 OID 16665)
-- Name: sec_user_menu_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_menu_organization_id_index ON public."SEC_User_Menu" USING btree (organization_id);


--
-- TOC entry 3299 (class 1259 OID 16666)
-- Name: sec_user_menu_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_menu_user_id_index ON public."SEC_User_Menu" USING btree (user_id);


--
-- TOC entry 3302 (class 1259 OID 16667)
-- Name: sec_user_organization_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_organization_organization_id_index ON public."SEC_User_Organization" USING btree (organization_id);


--
-- TOC entry 3303 (class 1259 OID 16668)
-- Name: sec_user_organization_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_organization_user_id_index ON public."SEC_User_Organization" USING btree (user_id);


--
-- TOC entry 3306 (class 1259 OID 16669)
-- Name: sec_user_permission_is_assigned_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_permission_is_assigned_index ON public."SEC_User_Permission" USING btree (is_assigned);


--
-- TOC entry 3307 (class 1259 OID 16670)
-- Name: sec_user_permission_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_permission_organization_id_index ON public."SEC_User_Permission" USING btree (organization_id);


--
-- TOC entry 3308 (class 1259 OID 16671)
-- Name: sec_user_permission_permission_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_permission_permission_id_index ON public."SEC_User_Permission" USING btree (permission_id);


--
-- TOC entry 3309 (class 1259 OID 16672)
-- Name: sec_user_permission_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_permission_user_id_index ON public."SEC_User_Permission" USING btree (user_id);


--
-- TOC entry 3312 (class 1259 OID 16673)
-- Name: sec_user_role_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_role_organization_id_index ON public."SEC_User_Role" USING btree (organization_id);


--
-- TOC entry 3313 (class 1259 OID 16674)
-- Name: sec_user_role_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_role_role_id_index ON public."SEC_User_Role" USING btree (role_id);


--
-- TOC entry 3314 (class 1259 OID 16675)
-- Name: sec_user_role_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_role_user_id_index ON public."SEC_User_Role" USING btree (user_id);


--
-- TOC entry 3336 (class 2606 OID 16676)
-- Name: FILE_File file_file_parent_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."FILE_File"
    ADD CONSTRAINT file_file_parent_file_id_foreign FOREIGN KEY (parent_file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 3378 (class 2606 OID 26303)
-- Name: OADH_FREE_Forced_Disappearances_Temps oadh_free_forced_disappearance_temps_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearances_Temps"
    ADD CONSTRAINT oadh_free_forced_disappearance_temps_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 3337 (class 2606 OID 16681)
-- Name: ORG_Organization org_organization_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization"
    ADD CONSTRAINT org_organization_country_id_foreign FOREIGN KEY (country_id) REFERENCES public."SYS_Country"(id);


--
-- TOC entry 3338 (class 2606 OID 16686)
-- Name: ORG_Organization org_organization_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization"
    ADD CONSTRAINT org_organization_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3339 (class 2606 OID 16691)
-- Name: ORG_Organization org_organization_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization"
    ADD CONSTRAINT org_organization_currency_id_foreign FOREIGN KEY (currency_id) REFERENCES public."SYS_Currency"(id);


--
-- TOC entry 3342 (class 2606 OID 16696)
-- Name: SEC_Journal_Detail sec_journal_detail_journal_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal_Detail"
    ADD CONSTRAINT sec_journal_detail_journal_id_foreign FOREIGN KEY (journal_id) REFERENCES public."SEC_Journal"(id);


--
-- TOC entry 3340 (class 2606 OID 16701)
-- Name: SEC_Journal sec_journal_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal"
    ADD CONSTRAINT sec_journal_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 3341 (class 2606 OID 16706)
-- Name: SEC_Journal sec_journal_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal"
    ADD CONSTRAINT sec_journal_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3343 (class 2606 OID 16711)
-- Name: SEC_Menu sec_menu_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu"
    ADD CONSTRAINT sec_menu_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3344 (class 2606 OID 16716)
-- Name: SEC_Menu sec_menu_module_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu"
    ADD CONSTRAINT sec_menu_module_id_foreign FOREIGN KEY (module_id) REFERENCES public."SEC_Module"(id);


--
-- TOC entry 3345 (class 2606 OID 16721)
-- Name: SEC_Menu sec_menu_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu"
    ADD CONSTRAINT sec_menu_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public."SEC_Menu"(id);


--
-- TOC entry 3346 (class 2606 OID 16726)
-- Name: SEC_Module sec_module_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Module"
    ADD CONSTRAINT sec_module_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3347 (class 2606 OID 16731)
-- Name: SEC_Permission sec_permission_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Permission"
    ADD CONSTRAINT sec_permission_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3348 (class 2606 OID 16736)
-- Name: SEC_Permission sec_permission_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Permission"
    ADD CONSTRAINT sec_permission_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public."SEC_Menu"(id);


--
-- TOC entry 3349 (class 2606 OID 16741)
-- Name: SEC_Role sec_role_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role"
    ADD CONSTRAINT sec_role_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3351 (class 2606 OID 16746)
-- Name: SEC_Role_Menu sec_role_menu_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu"
    ADD CONSTRAINT sec_role_menu_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3352 (class 2606 OID 16751)
-- Name: SEC_Role_Menu sec_role_menu_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu"
    ADD CONSTRAINT sec_role_menu_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public."SEC_Menu"(id);


--
-- TOC entry 3353 (class 2606 OID 16756)
-- Name: SEC_Role_Menu sec_role_menu_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu"
    ADD CONSTRAINT sec_role_menu_role_id_foreign FOREIGN KEY (role_id) REFERENCES public."SEC_Role"(id);


--
-- TOC entry 3350 (class 2606 OID 16761)
-- Name: SEC_Role sec_role_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role"
    ADD CONSTRAINT sec_role_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 3354 (class 2606 OID 16766)
-- Name: SEC_Role_Permission sec_role_permission_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission"
    ADD CONSTRAINT sec_role_permission_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3355 (class 2606 OID 16771)
-- Name: SEC_Role_Permission sec_role_permission_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission"
    ADD CONSTRAINT sec_role_permission_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public."SEC_Permission"(id);


--
-- TOC entry 3356 (class 2606 OID 16776)
-- Name: SEC_Role_Permission sec_role_permission_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission"
    ADD CONSTRAINT sec_role_permission_role_id_foreign FOREIGN KEY (role_id) REFERENCES public."SEC_Role"(id);


--
-- TOC entry 3357 (class 2606 OID 16781)
-- Name: SEC_User sec_user_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User"
    ADD CONSTRAINT sec_user_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3358 (class 2606 OID 16786)
-- Name: SEC_User sec_user_default_organization_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User"
    ADD CONSTRAINT sec_user_default_organization_foreign FOREIGN KEY (default_organization) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 3359 (class 2606 OID 16791)
-- Name: SEC_User_Menu sec_user_menu_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT sec_user_menu_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3360 (class 2606 OID 16796)
-- Name: SEC_User_Menu sec_user_menu_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT sec_user_menu_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public."SEC_Menu"(id);


--
-- TOC entry 3361 (class 2606 OID 16801)
-- Name: SEC_User_Menu sec_user_menu_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT sec_user_menu_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 3362 (class 2606 OID 16806)
-- Name: SEC_User_Menu sec_user_menu_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT sec_user_menu_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3363 (class 2606 OID 16811)
-- Name: SEC_User_Organization sec_user_organization_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization"
    ADD CONSTRAINT sec_user_organization_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3364 (class 2606 OID 16816)
-- Name: SEC_User_Organization sec_user_organization_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization"
    ADD CONSTRAINT sec_user_organization_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 3365 (class 2606 OID 16821)
-- Name: SEC_User_Organization sec_user_organization_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization"
    ADD CONSTRAINT sec_user_organization_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3366 (class 2606 OID 16826)
-- Name: SEC_User_Permission sec_user_permission_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT sec_user_permission_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3367 (class 2606 OID 16831)
-- Name: SEC_User_Permission sec_user_permission_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT sec_user_permission_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 3368 (class 2606 OID 16836)
-- Name: SEC_User_Permission sec_user_permission_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT sec_user_permission_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public."SEC_Permission"(id);


--
-- TOC entry 3369 (class 2606 OID 16841)
-- Name: SEC_User_Permission sec_user_permission_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT sec_user_permission_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3370 (class 2606 OID 16846)
-- Name: SEC_User_Role sec_user_role_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT sec_user_role_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3371 (class 2606 OID 16851)
-- Name: SEC_User_Role sec_user_role_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT sec_user_role_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 3372 (class 2606 OID 16856)
-- Name: SEC_User_Role sec_user_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT sec_user_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public."SEC_Role"(id);


--
-- TOC entry 3373 (class 2606 OID 16861)
-- Name: SEC_User_Role sec_user_role_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT sec_user_role_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 3374 (class 2606 OID 16866)
-- Name: SYS_Account sys_account_account_chart_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account"
    ADD CONSTRAINT sys_account_account_chart_type_id_foreign FOREIGN KEY (account_chart_type_id) REFERENCES public."SYS_Account_Chart_Type"(id);


--
-- TOC entry 3375 (class 2606 OID 16871)
-- Name: SYS_Account_Chart_Type sys_account_chart_type_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Chart_Type"
    ADD CONSTRAINT sys_account_chart_type_country_id_foreign FOREIGN KEY (country_id) REFERENCES public."SYS_Country"(id);


--
-- TOC entry 3376 (class 2606 OID 16876)
-- Name: SYS_Country sys_country_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Country"
    ADD CONSTRAINT sys_country_currency_id_foreign FOREIGN KEY (currency_id) REFERENCES public."SYS_Currency"(id);


--
-- TOC entry 3377 (class 2606 OID 16881)
-- Name: SYS_Region sys_region_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Region"
    ADD CONSTRAINT sys_region_country_id_foreign FOREIGN KEY (country_id) REFERENCES public."SYS_Country"(id);


-- Completed on 2019-04-29 15:54:10 CST

--
-- PostgreSQL database dump complete
--

