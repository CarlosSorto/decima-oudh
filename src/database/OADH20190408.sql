--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2019-04-08 14:47:40 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16386)
-- Name: FILE_File; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."FILE_File" (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    type character(1) NOT NULL,
    system_type character varying(100),
    system_route character varying(255),
    url text,
    url_html text,
    is_public boolean,
    key character varying(255),
    icon character varying(20),
    icon_html character varying(255),
    width double precision,
    height double precision,
    parent_file_id integer,
    system_reference_type character varying(40),
    system_reference_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."FILE_File" OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16392)
-- Name: FILE_File_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."FILE_File_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."FILE_File_id_seq" OWNER TO postgres;

--
-- TOC entry 4618 (class 0 OID 0)
-- Dependencies: 197
-- Name: FILE_File_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."FILE_File_id_seq" OWNED BY public."FILE_File".id;


--
-- TOC entry 259 (class 1259 OID 17708)
-- Name: OADH_Context; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_Context" (
    id integer NOT NULL,
    department character varying(60),
    municipality character varying(60),
    neighborhood character varying(60),
    organization_id integer NOT NULL,
    news_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_Context" OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 17706)
-- Name: OADH_Context_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_Context_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_Context_id_seq" OWNER TO postgres;

--
-- TOC entry 4619 (class 0 OID 0)
-- Dependencies: 258
-- Name: OADH_Context_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_Context_id_seq" OWNED BY public."OADH_Context".id;


--
-- TOC entry 251 (class 1259 OID 17638)
-- Name: OADH_Digital_Media; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_Digital_Media" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    type character(1) NOT NULL,
    abbreviation character(5),
    parent_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_Digital_Media" OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 17636)
-- Name: OADH_Digital_Media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_Digital_Media_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_Digital_Media_id_seq" OWNER TO postgres;

--
-- TOC entry 4620 (class 0 OID 0)
-- Dependencies: 250
-- Name: OADH_Digital_Media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_Digital_Media_id_seq" OWNED BY public."OADH_Digital_Media".id;


--
-- TOC entry 299 (class 1259 OID 26243)
-- Name: OADH_FREE_Accused_Liberty_Deprivation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Accused_Liberty_Deprivation" (
    id integer NOT NULL,
    year integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    profession character varying(255) NOT NULL,
    gender character(1) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Accused_Liberty_Deprivation" OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 26226)
-- Name: OADH_FREE_Accused_Liberty_Deprivation_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Accused_Liberty_Deprivation_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    profession character varying(255) NOT NULL,
    gender character(1) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Accused_Liberty_Deprivation_Temp" OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 26224)
-- Name: OADH_FREE_Accused_Liberty_Deprivation_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Accused_Liberty_Deprivation_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Accused_Liberty_Deprivation_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4621 (class 0 OID 0)
-- Dependencies: 296
-- Name: OADH_FREE_Accused_Liberty_Deprivation_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Accused_Liberty_Deprivation_Temp_id_seq" OWNED BY public."OADH_FREE_Accused_Liberty_Deprivation_Temp".id;


--
-- TOC entry 298 (class 1259 OID 26241)
-- Name: OADH_FREE_Accused_Liberty_Deprivation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Accused_Liberty_Deprivation_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Accused_Liberty_Deprivation_id_seq" OWNER TO postgres;

--
-- TOC entry 4622 (class 0 OID 0)
-- Dependencies: 298
-- Name: OADH_FREE_Accused_Liberty_Deprivation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Accused_Liberty_Deprivation_id_seq" OWNED BY public."OADH_FREE_Accused_Liberty_Deprivation".id;


--
-- TOC entry 315 (class 1259 OID 26379)
-- Name: OADH_FREE_Acute_Diseases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Acute_Diseases" (
    id integer NOT NULL,
    year integer NOT NULL,
    disease character varying(255) NOT NULL,
    quantity integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Acute_Diseases" OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 26362)
-- Name: OADH_FREE_Acute_Diseases_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Acute_Diseases_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    "right" character varying(255) NOT NULL,
    "violated_Fact" character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    age integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Acute_Diseases_Temp" OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 26360)
-- Name: OADH_FREE_Acute_Diseases_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Acute_Diseases_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Acute_Diseases_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4623 (class 0 OID 0)
-- Dependencies: 312
-- Name: OADH_FREE_Acute_Diseases_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Acute_Diseases_Temp_id_seq" OWNED BY public."OADH_FREE_Acute_Diseases_Temp".id;


--
-- TOC entry 314 (class 1259 OID 26377)
-- Name: OADH_FREE_Acute_Diseases_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Acute_Diseases_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Acute_Diseases_id_seq" OWNER TO postgres;

--
-- TOC entry 4624 (class 0 OID 0)
-- Dependencies: 314
-- Name: OADH_FREE_Acute_Diseases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Acute_Diseases_id_seq" OWNED BY public."OADH_FREE_Acute_Diseases".id;


--
-- TOC entry 291 (class 1259 OID 26175)
-- Name: OADH_FREE_Capture_Orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Capture_Orders" (
    id integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Capture_Orders" OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 26158)
-- Name: OADH_FREE_Capture_Orders_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Capture_Orders_Temp" (
    id integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Capture_Orders_Temp" OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 26156)
-- Name: OADH_FREE_Capture_Orders_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Capture_Orders_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Capture_Orders_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4625 (class 0 OID 0)
-- Dependencies: 288
-- Name: OADH_FREE_Capture_Orders_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Capture_Orders_Temp_id_seq" OWNED BY public."OADH_FREE_Capture_Orders_Temp".id;


--
-- TOC entry 290 (class 1259 OID 26173)
-- Name: OADH_FREE_Capture_Orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Capture_Orders_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Capture_Orders_id_seq" OWNER TO postgres;

--
-- TOC entry 4626 (class 0 OID 0)
-- Dependencies: 290
-- Name: OADH_FREE_Capture_Orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Capture_Orders_id_seq" OWNED BY public."OADH_FREE_Capture_Orders".id;


--
-- TOC entry 319 (class 1259 OID 26410)
-- Name: OADH_FREE_Chronic_Diseases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Chronic_Diseases" (
    id integer NOT NULL,
    chronic_disease character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    age integer NOT NULL,
    jail character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Chronic_Diseases" OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 26393)
-- Name: OADH_FREE_Chronic_Diseases_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Chronic_Diseases_Temp" (
    id integer NOT NULL,
    chronic_disease character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    age integer NOT NULL,
    jail character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Chronic_Diseases_Temp" OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 26391)
-- Name: OADH_FREE_Chronic_Diseases_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Chronic_Diseases_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Chronic_Diseases_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4627 (class 0 OID 0)
-- Dependencies: 316
-- Name: OADH_FREE_Chronic_Diseases_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Chronic_Diseases_Temp_id_seq" OWNED BY public."OADH_FREE_Chronic_Diseases_Temp".id;


--
-- TOC entry 318 (class 1259 OID 26408)
-- Name: OADH_FREE_Chronic_Diseases_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Chronic_Diseases_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Chronic_Diseases_id_seq" OWNER TO postgres;

--
-- TOC entry 4628 (class 0 OID 0)
-- Dependencies: 318
-- Name: OADH_FREE_Chronic_Diseases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Chronic_Diseases_id_seq" OWNED BY public."OADH_FREE_Chronic_Diseases".id;


--
-- TOC entry 279 (class 1259 OID 26079)
-- Name: OADH_FREE_Criminal_Cases_Trials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Criminal_Cases_Trials" (
    id integer NOT NULL,
    year integer NOT NULL,
    phase character varying(255) NOT NULL,
    court character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Criminal_Cases_Trials" OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 26062)
-- Name: OADH_FREE_Criminal_Cases_Trials_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Criminal_Cases_Trials_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    phase character varying(255) NOT NULL,
    court character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Criminal_Cases_Trials_Temp" OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 26060)
-- Name: OADH_FREE_Criminal_Cases_Trials_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Criminal_Cases_Trials_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Criminal_Cases_Trials_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4629 (class 0 OID 0)
-- Dependencies: 276
-- Name: OADH_FREE_Criminal_Cases_Trials_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Criminal_Cases_Trials_Temp_id_seq" OWNED BY public."OADH_FREE_Criminal_Cases_Trials_Temp".id;


--
-- TOC entry 278 (class 1259 OID 26077)
-- Name: OADH_FREE_Criminal_Cases_Trials_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Criminal_Cases_Trials_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Criminal_Cases_Trials_id_seq" OWNER TO postgres;

--
-- TOC entry 4630 (class 0 OID 0)
-- Dependencies: 278
-- Name: OADH_FREE_Criminal_Cases_Trials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Criminal_Cases_Trials_id_seq" OWNED BY public."OADH_FREE_Criminal_Cases_Trials".id;


--
-- TOC entry 311 (class 1259 OID 26345)
-- Name: OADH_FREE_Deprived_Persons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Deprived_Persons" (
    id integer NOT NULL,
    year integer NOT NULL,
    "right" character varying(255) NOT NULL,
    "violated_Fact" character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    age integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Deprived_Persons" OWNER TO postgres;

--
-- TOC entry 309 (class 1259 OID 26328)
-- Name: OADH_FREE_Deprived_Persons_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Deprived_Persons_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    "right" character varying(255) NOT NULL,
    "violated_Fact" character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    age integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Deprived_Persons_Temp" OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 26326)
-- Name: OADH_FREE_Deprived_Persons_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Deprived_Persons_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Deprived_Persons_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4631 (class 0 OID 0)
-- Dependencies: 308
-- Name: OADH_FREE_Deprived_Persons_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Deprived_Persons_Temp_id_seq" OWNED BY public."OADH_FREE_Deprived_Persons_Temp".id;


--
-- TOC entry 310 (class 1259 OID 26343)
-- Name: OADH_FREE_Deprived_Persons_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Deprived_Persons_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Deprived_Persons_id_seq" OWNER TO postgres;

--
-- TOC entry 4632 (class 0 OID 0)
-- Dependencies: 310
-- Name: OADH_FREE_Deprived_Persons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Deprived_Persons_id_seq" OWNED BY public."OADH_FREE_Deprived_Persons".id;


--
-- TOC entry 295 (class 1259 OID 26209)
-- Name: OADH_FREE_Disappearances_Victims; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Disappearances_Victims" (
    id integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Disappearances_Victims" OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 26192)
-- Name: OADH_FREE_Disappearances_Victims_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Disappearances_Victims_Temp" (
    id integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Disappearances_Victims_Temp" OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 26190)
-- Name: OADH_FREE_Disappearances_Victims_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Disappearances_Victims_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Disappearances_Victims_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4633 (class 0 OID 0)
-- Dependencies: 292
-- Name: OADH_FREE_Disappearances_Victims_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Disappearances_Victims_Temp_id_seq" OWNED BY public."OADH_FREE_Disappearances_Victims_Temp".id;


--
-- TOC entry 294 (class 1259 OID 26207)
-- Name: OADH_FREE_Disappearances_Victims_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Disappearances_Victims_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Disappearances_Victims_id_seq" OWNER TO postgres;

--
-- TOC entry 4634 (class 0 OID 0)
-- Dependencies: 294
-- Name: OADH_FREE_Disappearances_Victims_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Disappearances_Victims_id_seq" OWNED BY public."OADH_FREE_Disappearances_Victims".id;


--
-- TOC entry 305 (class 1259 OID 26294)
-- Name: OADH_FREE_Forced_Disappearance_Temps; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Forced_Disappearance_Temps" (
    id integer NOT NULL,
    year integer NOT NULL,
    gender character(1) NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    profession character varying(255) NOT NULL,
    crime character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Forced_Disappearance_Temps" OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 26292)
-- Name: OADH_FREE_Forced_Disappearance_Temps_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Forced_Disappearance_Temps_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Forced_Disappearance_Temps_id_seq" OWNER TO postgres;

--
-- TOC entry 4635 (class 0 OID 0)
-- Dependencies: 304
-- Name: OADH_FREE_Forced_Disappearance_Temps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Forced_Disappearance_Temps_id_seq" OWNED BY public."OADH_FREE_Forced_Disappearance_Temps".id;


--
-- TOC entry 307 (class 1259 OID 26311)
-- Name: OADH_FREE_Forced_Disappearances; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Forced_Disappearances" (
    id integer NOT NULL,
    year integer NOT NULL,
    gender character(1) NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    profession character varying(255) NOT NULL,
    crime character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Forced_Disappearances" OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 26309)
-- Name: OADH_FREE_Forced_Disappearances_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Forced_Disappearances_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Forced_Disappearances_id_seq" OWNER TO postgres;

--
-- TOC entry 4636 (class 0 OID 0)
-- Dependencies: 306
-- Name: OADH_FREE_Forced_Disappearances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Forced_Disappearances_id_seq" OWNED BY public."OADH_FREE_Forced_Disappearances".id;


--
-- TOC entry 271 (class 1259 OID 26014)
-- Name: OADH_FREE_Habeas_Corpus_Request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Habeas_Corpus_Request" (
    id integer NOT NULL,
    year integer NOT NULL,
    court character varying(255) NOT NULL,
    status character(1),
    file_id integer,
    organization_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    sex character varying(1)
);


ALTER TABLE public."OADH_FREE_Habeas_Corpus_Request" OWNER TO postgres;

--
-- TOC entry 269 (class 1259 OID 26000)
-- Name: OADH_FREE_Habeas_Corpus_Request_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Habeas_Corpus_Request_Temp" (
    id integer NOT NULL,
    court character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    sex character varying DEFAULT 1
);


ALTER TABLE public."OADH_FREE_Habeas_Corpus_Request_Temp" OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 25998)
-- Name: OADH_FREE_Habeas_Corpus_Request_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Habeas_Corpus_Request_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Habeas_Corpus_Request_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4637 (class 0 OID 0)
-- Dependencies: 268
-- Name: OADH_FREE_Habeas_Corpus_Request_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Habeas_Corpus_Request_Temp_id_seq" OWNED BY public."OADH_FREE_Habeas_Corpus_Request_Temp".id;


--
-- TOC entry 270 (class 1259 OID 26012)
-- Name: OADH_FREE_Habeas_Corpus_Request_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Habeas_Corpus_Request_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Habeas_Corpus_Request_id_seq" OWNER TO postgres;

--
-- TOC entry 4638 (class 0 OID 0)
-- Dependencies: 270
-- Name: OADH_FREE_Habeas_Corpus_Request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Habeas_Corpus_Request_id_seq" OWNED BY public."OADH_FREE_Habeas_Corpus_Request".id;


--
-- TOC entry 323 (class 1259 OID 26444)
-- Name: OADH_FREE_Jails_Occupation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Jails_Occupation" (
    id integer NOT NULL,
    departament character varying(255) NOT NULL,
    station character varying(255) NOT NULL,
    unit character varying(255) NOT NULL,
    year integer NOT NULL,
    capacity integer NOT NULL,
    man_population integer NOT NULL,
    women_population integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Jails_Occupation" OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 26427)
-- Name: OADH_FREE_Jails_Occupation_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Jails_Occupation_Temp" (
    id integer NOT NULL,
    departament character varying(255) NOT NULL,
    station character varying(255) NOT NULL,
    unit character varying(255) NOT NULL,
    year integer NOT NULL,
    capacity integer NOT NULL,
    man_population integer NOT NULL,
    women_population integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Jails_Occupation_Temp" OWNER TO postgres;

--
-- TOC entry 320 (class 1259 OID 26425)
-- Name: OADH_FREE_Jails_Occupation_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Jails_Occupation_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Jails_Occupation_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4639 (class 0 OID 0)
-- Dependencies: 320
-- Name: OADH_FREE_Jails_Occupation_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Jails_Occupation_Temp_id_seq" OWNED BY public."OADH_FREE_Jails_Occupation_Temp".id;


--
-- TOC entry 322 (class 1259 OID 26442)
-- Name: OADH_FREE_Jails_Occupation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Jails_Occupation_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Jails_Occupation_id_seq" OWNER TO postgres;

--
-- TOC entry 4640 (class 0 OID 0)
-- Dependencies: 322
-- Name: OADH_FREE_Jails_Occupation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Jails_Occupation_id_seq" OWNED BY public."OADH_FREE_Jails_Occupation".id;


--
-- TOC entry 283 (class 1259 OID 26110)
-- Name: OADH_FREE_Mass_Trials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Mass_Trials" (
    id integer NOT NULL,
    year integer NOT NULL,
    sex character(1) NOT NULL,
    court character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Mass_Trials" OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 26096)
-- Name: OADH_FREE_Mass_Trials_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Mass_Trials_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    sex character(1) NOT NULL,
    court character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Mass_Trials_Temp" OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 26094)
-- Name: OADH_FREE_Mass_Trials_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Mass_Trials_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Mass_Trials_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4641 (class 0 OID 0)
-- Dependencies: 280
-- Name: OADH_FREE_Mass_Trials_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Mass_Trials_Temp_id_seq" OWNED BY public."OADH_FREE_Mass_Trials_Temp".id;


--
-- TOC entry 282 (class 1259 OID 26108)
-- Name: OADH_FREE_Mass_Trials_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Mass_Trials_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Mass_Trials_id_seq" OWNER TO postgres;

--
-- TOC entry 4642 (class 0 OID 0)
-- Dependencies: 282
-- Name: OADH_FREE_Mass_Trials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Mass_Trials_id_seq" OWNED BY public."OADH_FREE_Mass_Trials".id;


--
-- TOC entry 275 (class 1259 OID 26045)
-- Name: OADH_FREE_People_Detained; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_People_Detained" (
    id integer NOT NULL,
    state character varying(255) NOT NULL,
    year integer NOT NULL,
    departament character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_People_Detained" OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 26028)
-- Name: OADH_FREE_People_Detained_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_People_Detained_Temp" (
    id integer NOT NULL,
    state character varying(255) NOT NULL,
    year integer NOT NULL,
    departament character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_People_Detained_Temp" OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 26026)
-- Name: OADH_FREE_People_Detained_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_People_Detained_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_People_Detained_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4643 (class 0 OID 0)
-- Dependencies: 272
-- Name: OADH_FREE_People_Detained_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_People_Detained_Temp_id_seq" OWNED BY public."OADH_FREE_People_Detained_Temp".id;


--
-- TOC entry 274 (class 1259 OID 26043)
-- Name: OADH_FREE_People_Detained_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_People_Detained_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_People_Detained_id_seq" OWNER TO postgres;

--
-- TOC entry 4644 (class 0 OID 0)
-- Dependencies: 274
-- Name: OADH_FREE_People_Detained_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_People_Detained_id_seq" OWNED BY public."OADH_FREE_People_Detained".id;


--
-- TOC entry 287 (class 1259 OID 26141)
-- Name: OADH_FREE_Procedural_Fraud; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Procedural_Fraud" (
    id integer NOT NULL,
    gender character(1) NOT NULL,
    crime character varying(255) NOT NULL,
    year integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Procedural_Fraud" OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 26124)
-- Name: OADH_FREE_Procedural_Fraud_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Procedural_Fraud_Temp" (
    id integer NOT NULL,
    gender character(1) NOT NULL,
    crime character varying(255) NOT NULL,
    year integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Procedural_Fraud_Temp" OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 26122)
-- Name: OADH_FREE_Procedural_Fraud_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Procedural_Fraud_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Procedural_Fraud_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4645 (class 0 OID 0)
-- Dependencies: 284
-- Name: OADH_FREE_Procedural_Fraud_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Procedural_Fraud_Temp_id_seq" OWNED BY public."OADH_FREE_Procedural_Fraud_Temp".id;


--
-- TOC entry 286 (class 1259 OID 26139)
-- Name: OADH_FREE_Procedural_Fraud_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Procedural_Fraud_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Procedural_Fraud_id_seq" OWNER TO postgres;

--
-- TOC entry 4646 (class 0 OID 0)
-- Dependencies: 286
-- Name: OADH_FREE_Procedural_Fraud_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Procedural_Fraud_id_seq" OWNED BY public."OADH_FREE_Procedural_Fraud".id;


--
-- TOC entry 303 (class 1259 OID 26277)
-- Name: OADH_FREE_Victims_Liberty_Deprivation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Victims_Liberty_Deprivation" (
    id integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Victims_Liberty_Deprivation" OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 26260)
-- Name: OADH_FREE_Victims_Liberty_Deprivation_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_FREE_Victims_Liberty_Deprivation_Temp" (
    id integer NOT NULL,
    departament character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    year integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_FREE_Victims_Liberty_Deprivation_Temp" OWNER TO postgres;

--
-- TOC entry 300 (class 1259 OID 26258)
-- Name: OADH_FREE_Victims_Liberty_Deprivation_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Victims_Liberty_Deprivation_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Victims_Liberty_Deprivation_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4647 (class 0 OID 0)
-- Dependencies: 300
-- Name: OADH_FREE_Victims_Liberty_Deprivation_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Victims_Liberty_Deprivation_Temp_id_seq" OWNED BY public."OADH_FREE_Victims_Liberty_Deprivation_Temp".id;


--
-- TOC entry 302 (class 1259 OID 26275)
-- Name: OADH_FREE_Victims_Liberty_Deprivation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_FREE_Victims_Liberty_Deprivation_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_FREE_Victims_Liberty_Deprivation_id_seq" OWNER TO postgres;

--
-- TOC entry 4648 (class 0 OID 0)
-- Dependencies: 302
-- Name: OADH_FREE_Victims_Liberty_Deprivation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_FREE_Victims_Liberty_Deprivation_id_seq" OWNED BY public."OADH_FREE_Victims_Liberty_Deprivation".id;


--
-- TOC entry 261 (class 1259 OID 17722)
-- Name: OADH_Human_Right; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_Human_Right" (
    id integer NOT NULL,
    name text NOT NULL,
    type character(1),
    parent_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_Human_Right" OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 17720)
-- Name: OADH_Human_Right_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_Human_Right_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_Human_Right_id_seq" OWNER TO postgres;

--
-- TOC entry 4649 (class 0 OID 0)
-- Dependencies: 260
-- Name: OADH_Human_Right_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_Human_Right_id_seq" OWNED BY public."OADH_Human_Right".id;


--
-- TOC entry 383 (class 1259 OID 26915)
-- Name: OADH_INT_Age_Injuries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Age_Injuries" (
    id integer NOT NULL,
    year integer NOT NULL,
    sex character(1) NOT NULL,
    age_range character varying(255) NOT NULL,
    total double precision NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Age_Injuries" OWNER TO postgres;

--
-- TOC entry 381 (class 1259 OID 26901)
-- Name: OADH_INT_Age_Injuries_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Age_Injuries_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    sex character(1) NOT NULL,
    age_range character varying(255) NOT NULL,
    total double precision NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Age_Injuries_Temp" OWNER TO postgres;

--
-- TOC entry 380 (class 1259 OID 26899)
-- Name: OADH_INT_Age_Injuries_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Age_Injuries_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Age_Injuries_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4650 (class 0 OID 0)
-- Dependencies: 380
-- Name: OADH_INT_Age_Injuries_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Age_Injuries_Temp_id_seq" OWNED BY public."OADH_INT_Age_Injuries_Temp".id;


--
-- TOC entry 382 (class 1259 OID 26913)
-- Name: OADH_INT_Age_Injuries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Age_Injuries_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Age_Injuries_id_seq" OWNER TO postgres;

--
-- TOC entry 4651 (class 0 OID 0)
-- Dependencies: 382
-- Name: OADH_INT_Age_Injuries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Age_Injuries_id_seq" OWNED BY public."OADH_INT_Age_Injuries".id;


--
-- TOC entry 375 (class 1259 OID 26850)
-- Name: OADH_INT_Age_Sexual; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Age_Sexual" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    age_range character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Age_Sexual" OWNER TO postgres;

--
-- TOC entry 373 (class 1259 OID 26833)
-- Name: OADH_INT_Age_Sexual_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Age_Sexual_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    age_range character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Age_Sexual_Temp" OWNER TO postgres;

--
-- TOC entry 372 (class 1259 OID 26831)
-- Name: OADH_INT_Age_Sexual_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Age_Sexual_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Age_Sexual_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4652 (class 0 OID 0)
-- Dependencies: 372
-- Name: OADH_INT_Age_Sexual_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Age_Sexual_Temp_id_seq" OWNED BY public."OADH_INT_Age_Sexual_Temp".id;


--
-- TOC entry 374 (class 1259 OID 26848)
-- Name: OADH_INT_Age_Sexual_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Age_Sexual_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Age_Sexual_id_seq" OWNER TO postgres;

--
-- TOC entry 4653 (class 0 OID 0)
-- Dependencies: 374
-- Name: OADH_INT_Age_Sexual_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Age_Sexual_id_seq" OWNED BY public."OADH_INT_Age_Sexual".id;


--
-- TOC entry 347 (class 1259 OID 26633)
-- Name: OADH_INT_Age_Trafficking; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Age_Trafficking" (
    id integer NOT NULL,
    year integer NOT NULL,
    age_range character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Age_Trafficking" OWNER TO postgres;

--
-- TOC entry 345 (class 1259 OID 26619)
-- Name: OADH_INT_Age_Trafficking_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Age_Trafficking_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    age_range character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Age_Trafficking_Temp" OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 26617)
-- Name: OADH_INT_Age_Trafficking_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Age_Trafficking_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Age_Trafficking_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4654 (class 0 OID 0)
-- Dependencies: 344
-- Name: OADH_INT_Age_Trafficking_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Age_Trafficking_Temp_id_seq" OWNED BY public."OADH_INT_Age_Trafficking_Temp".id;


--
-- TOC entry 346 (class 1259 OID 26631)
-- Name: OADH_INT_Age_Trafficking_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Age_Trafficking_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Age_Trafficking_id_seq" OWNER TO postgres;

--
-- TOC entry 4655 (class 0 OID 0)
-- Dependencies: 346
-- Name: OADH_INT_Age_Trafficking_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Age_Trafficking_id_seq" OWNED BY public."OADH_INT_Age_Trafficking".id;


--
-- TOC entry 355 (class 1259 OID 26695)
-- Name: OADH_INT_Age_Women; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Age_Women" (
    id integer NOT NULL,
    year integer NOT NULL,
    age_range character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Age_Women" OWNER TO postgres;

--
-- TOC entry 353 (class 1259 OID 26681)
-- Name: OADH_INT_Age_Women_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Age_Women_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    age_range character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Age_Women_Temp" OWNER TO postgres;

--
-- TOC entry 352 (class 1259 OID 26679)
-- Name: OADH_INT_Age_Women_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Age_Women_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Age_Women_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4656 (class 0 OID 0)
-- Dependencies: 352
-- Name: OADH_INT_Age_Women_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Age_Women_Temp_id_seq" OWNED BY public."OADH_INT_Age_Women_Temp".id;


--
-- TOC entry 354 (class 1259 OID 26693)
-- Name: OADH_INT_Age_Women_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Age_Women_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Age_Women_id_seq" OWNER TO postgres;

--
-- TOC entry 4657 (class 0 OID 0)
-- Dependencies: 354
-- Name: OADH_INT_Age_Women_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Age_Women_id_seq" OWNED BY public."OADH_INT_Age_Women".id;


--
-- TOC entry 391 (class 1259 OID 26980)
-- Name: OADH_INT_Agent_Investigation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Agent_Investigation" (
    id integer NOT NULL,
    code character varying(255) NOT NULL,
    crime character varying(255) NOT NULL,
    category character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    place character varying(255) NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    state character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Agent_Investigation" OWNER TO postgres;

--
-- TOC entry 389 (class 1259 OID 26963)
-- Name: OADH_INT_Agent_Investigation_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Agent_Investigation_Temp" (
    id integer NOT NULL,
    code character varying(255) NOT NULL,
    crime character varying(255) NOT NULL,
    category character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    place character varying(255) NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    state character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Agent_Investigation_Temp" OWNER TO postgres;

--
-- TOC entry 388 (class 1259 OID 26961)
-- Name: OADH_INT_Agent_Investigation_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Agent_Investigation_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Agent_Investigation_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4658 (class 0 OID 0)
-- Dependencies: 388
-- Name: OADH_INT_Agent_Investigation_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Agent_Investigation_Temp_id_seq" OWNED BY public."OADH_INT_Agent_Investigation_Temp".id;


--
-- TOC entry 390 (class 1259 OID 26978)
-- Name: OADH_INT_Agent_Investigation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Agent_Investigation_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Agent_Investigation_id_seq" OWNER TO postgres;

--
-- TOC entry 4659 (class 0 OID 0)
-- Dependencies: 390
-- Name: OADH_INT_Agent_Investigation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Agent_Investigation_id_seq" OWNED BY public."OADH_INT_Agent_Investigation".id;


--
-- TOC entry 331 (class 1259 OID 26512)
-- Name: OADH_INT_Crimes_Accused; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Crimes_Accused" (
    id integer NOT NULL,
    year integer NOT NULL,
    category character varying(255) NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Crimes_Accused" OWNER TO postgres;

--
-- TOC entry 329 (class 1259 OID 26495)
-- Name: OADH_INT_Crimes_Accused_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Crimes_Accused_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    category character varying(255) NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Crimes_Accused_Temp" OWNER TO postgres;

--
-- TOC entry 328 (class 1259 OID 26493)
-- Name: OADH_INT_Crimes_Accused_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Crimes_Accused_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Crimes_Accused_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4660 (class 0 OID 0)
-- Dependencies: 328
-- Name: OADH_INT_Crimes_Accused_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Crimes_Accused_Temp_id_seq" OWNED BY public."OADH_INT_Crimes_Accused_Temp".id;


--
-- TOC entry 330 (class 1259 OID 26510)
-- Name: OADH_INT_Crimes_Accused_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Crimes_Accused_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Crimes_Accused_id_seq" OWNER TO postgres;

--
-- TOC entry 4661 (class 0 OID 0)
-- Dependencies: 330
-- Name: OADH_INT_Crimes_Accused_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Crimes_Accused_id_seq" OWNED BY public."OADH_INT_Crimes_Accused".id;


--
-- TOC entry 327 (class 1259 OID 26478)
-- Name: OADH_INT_Crimes_Victims; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Crimes_Victims" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    age_range character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Crimes_Victims" OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 26461)
-- Name: OADH_INT_Crimes_Victims_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Crimes_Victims_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    age_range character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Crimes_Victims_Temp" OWNER TO postgres;

--
-- TOC entry 324 (class 1259 OID 26459)
-- Name: OADH_INT_Crimes_Victims_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Crimes_Victims_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Crimes_Victims_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4662 (class 0 OID 0)
-- Dependencies: 324
-- Name: OADH_INT_Crimes_Victims_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Crimes_Victims_Temp_id_seq" OWNED BY public."OADH_INT_Crimes_Victims_Temp".id;


--
-- TOC entry 326 (class 1259 OID 26476)
-- Name: OADH_INT_Crimes_Victims_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Crimes_Victims_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Crimes_Victims_id_seq" OWNER TO postgres;

--
-- TOC entry 4663 (class 0 OID 0)
-- Dependencies: 326
-- Name: OADH_INT_Crimes_Victims_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Crimes_Victims_id_seq" OWNED BY public."OADH_INT_Crimes_Victims".id;


--
-- TOC entry 371 (class 1259 OID 26819)
-- Name: OADH_INT_Gender_Sexual; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Gender_Sexual" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Gender_Sexual" OWNER TO postgres;

--
-- TOC entry 369 (class 1259 OID 26805)
-- Name: OADH_INT_Gender_Sexual_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Gender_Sexual_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Gender_Sexual_Temp" OWNER TO postgres;

--
-- TOC entry 368 (class 1259 OID 26803)
-- Name: OADH_INT_Gender_Sexual_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Gender_Sexual_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Gender_Sexual_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4664 (class 0 OID 0)
-- Dependencies: 368
-- Name: OADH_INT_Gender_Sexual_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Gender_Sexual_Temp_id_seq" OWNED BY public."OADH_INT_Gender_Sexual_Temp".id;


--
-- TOC entry 370 (class 1259 OID 26817)
-- Name: OADH_INT_Gender_Sexual_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Gender_Sexual_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Gender_Sexual_id_seq" OWNER TO postgres;

--
-- TOC entry 4665 (class 0 OID 0)
-- Dependencies: 370
-- Name: OADH_INT_Gender_Sexual_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Gender_Sexual_id_seq" OWNED BY public."OADH_INT_Gender_Sexual".id;


--
-- TOC entry 339 (class 1259 OID 26577)
-- Name: OADH_INT_Gender_Trafficking; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Gender_Trafficking" (
    id integer NOT NULL,
    year integer NOT NULL,
    sex character(1) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Gender_Trafficking" OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 26563)
-- Name: OADH_INT_Gender_Trafficking_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Gender_Trafficking_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    sex character(1) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Gender_Trafficking_Temp" OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 26561)
-- Name: OADH_INT_Gender_Trafficking_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Gender_Trafficking_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Gender_Trafficking_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4666 (class 0 OID 0)
-- Dependencies: 336
-- Name: OADH_INT_Gender_Trafficking_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Gender_Trafficking_Temp_id_seq" OWNED BY public."OADH_INT_Gender_Trafficking_Temp".id;


--
-- TOC entry 338 (class 1259 OID 26575)
-- Name: OADH_INT_Gender_Trafficking_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Gender_Trafficking_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Gender_Trafficking_id_seq" OWNER TO postgres;

--
-- TOC entry 4667 (class 0 OID 0)
-- Dependencies: 338
-- Name: OADH_INT_Gender_Trafficking_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Gender_Trafficking_id_seq" OWNED BY public."OADH_INT_Gender_Trafficking".id;


--
-- TOC entry 395 (class 1259 OID 27014)
-- Name: OADH_INT_Integrity_Violation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Integrity_Violation" (
    id integer NOT NULL,
    date date NOT NULL,
    sex character(1) NOT NULL,
    age integer NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    "right" character varying(255) NOT NULL,
    vilated_fact character varying(255) NOT NULL,
    institutions character varying(255) NOT NULL,
    dependencies character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Integrity_Violation" OWNER TO postgres;

--
-- TOC entry 393 (class 1259 OID 26997)
-- Name: OADH_INT_Integrity_Violation_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Integrity_Violation_Temp" (
    id integer NOT NULL,
    date date NOT NULL,
    sex character(1) NOT NULL,
    age integer NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    "right" character varying(255) NOT NULL,
    vilated_fact character varying(255) NOT NULL,
    institutions character varying(255) NOT NULL,
    dependencies character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Integrity_Violation_Temp" OWNER TO postgres;

--
-- TOC entry 392 (class 1259 OID 26995)
-- Name: OADH_INT_Integrity_Violation_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Integrity_Violation_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Integrity_Violation_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4668 (class 0 OID 0)
-- Dependencies: 392
-- Name: OADH_INT_Integrity_Violation_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Integrity_Violation_Temp_id_seq" OWNED BY public."OADH_INT_Integrity_Violation_Temp".id;


--
-- TOC entry 394 (class 1259 OID 27012)
-- Name: OADH_INT_Integrity_Violation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Integrity_Violation_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Integrity_Violation_id_seq" OWNER TO postgres;

--
-- TOC entry 4669 (class 0 OID 0)
-- Dependencies: 394
-- Name: OADH_INT_Integrity_Violation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Integrity_Violation_id_seq" OWNED BY public."OADH_INT_Integrity_Violation".id;


--
-- TOC entry 351 (class 1259 OID 26664)
-- Name: OADH_INT_Movement_Freedom; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Movement_Freedom" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    age_range character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Movement_Freedom" OWNER TO postgres;

--
-- TOC entry 349 (class 1259 OID 26647)
-- Name: OADH_INT_Movement_Freedom_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Movement_Freedom_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    age_range character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Movement_Freedom_Temp" OWNER TO postgres;

--
-- TOC entry 348 (class 1259 OID 26645)
-- Name: OADH_INT_Movement_Freedom_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Movement_Freedom_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Movement_Freedom_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4670 (class 0 OID 0)
-- Dependencies: 348
-- Name: OADH_INT_Movement_Freedom_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Movement_Freedom_Temp_id_seq" OWNED BY public."OADH_INT_Movement_Freedom_Temp".id;


--
-- TOC entry 350 (class 1259 OID 26662)
-- Name: OADH_INT_Movement_Freedom_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Movement_Freedom_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Movement_Freedom_id_seq" OWNER TO postgres;

--
-- TOC entry 4671 (class 0 OID 0)
-- Dependencies: 350
-- Name: OADH_INT_Movement_Freedom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Movement_Freedom_id_seq" OWNED BY public."OADH_INT_Movement_Freedom".id;


--
-- TOC entry 387 (class 1259 OID 26946)
-- Name: OADH_INT_Schedule_Injuries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Schedule_Injuries" (
    id integer NOT NULL,
    year integer NOT NULL,
    aggressor_type character varying(255) NOT NULL,
    "time" character varying(255) NOT NULL,
    total double precision NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Schedule_Injuries" OWNER TO postgres;

--
-- TOC entry 385 (class 1259 OID 26929)
-- Name: OADH_INT_Schedule_Injuries_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Schedule_Injuries_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    aggressor_type character varying(255) NOT NULL,
    "time" character varying(255) NOT NULL,
    total double precision NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Schedule_Injuries_Temp" OWNER TO postgres;

--
-- TOC entry 384 (class 1259 OID 26927)
-- Name: OADH_INT_Schedule_Injuries_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Schedule_Injuries_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Schedule_Injuries_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4672 (class 0 OID 0)
-- Dependencies: 384
-- Name: OADH_INT_Schedule_Injuries_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Schedule_Injuries_Temp_id_seq" OWNED BY public."OADH_INT_Schedule_Injuries_Temp".id;


--
-- TOC entry 386 (class 1259 OID 26944)
-- Name: OADH_INT_Schedule_Injuries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Schedule_Injuries_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Schedule_Injuries_id_seq" OWNER TO postgres;

--
-- TOC entry 4673 (class 0 OID 0)
-- Dependencies: 386
-- Name: OADH_INT_Schedule_Injuries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Schedule_Injuries_id_seq" OWNED BY public."OADH_INT_Schedule_Injuries".id;


--
-- TOC entry 379 (class 1259 OID 26884)
-- Name: OADH_INT_State_Injuries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_State_Injuries" (
    id integer NOT NULL,
    year integer NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    total double precision NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_State_Injuries" OWNER TO postgres;

--
-- TOC entry 377 (class 1259 OID 26867)
-- Name: OADH_INT_State_Injuries_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_State_Injuries_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    total double precision NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_State_Injuries_Temp" OWNER TO postgres;

--
-- TOC entry 376 (class 1259 OID 26865)
-- Name: OADH_INT_State_Injuries_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_State_Injuries_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_State_Injuries_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4674 (class 0 OID 0)
-- Dependencies: 376
-- Name: OADH_INT_State_Injuries_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_State_Injuries_Temp_id_seq" OWNED BY public."OADH_INT_State_Injuries_Temp".id;


--
-- TOC entry 378 (class 1259 OID 26882)
-- Name: OADH_INT_State_Injuries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_State_Injuries_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_State_Injuries_id_seq" OWNER TO postgres;

--
-- TOC entry 4675 (class 0 OID 0)
-- Dependencies: 378
-- Name: OADH_INT_State_Injuries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_State_Injuries_id_seq" OWNED BY public."OADH_INT_State_Injuries".id;


--
-- TOC entry 363 (class 1259 OID 26754)
-- Name: OADH_INT_State_Women; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_State_Women" (
    id integer NOT NULL,
    year integer NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_State_Women" OWNER TO postgres;

--
-- TOC entry 361 (class 1259 OID 26737)
-- Name: OADH_INT_State_Women_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_State_Women_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_State_Women_Temp" OWNER TO postgres;

--
-- TOC entry 360 (class 1259 OID 26735)
-- Name: OADH_INT_State_Women_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_State_Women_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_State_Women_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4676 (class 0 OID 0)
-- Dependencies: 360
-- Name: OADH_INT_State_Women_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_State_Women_Temp_id_seq" OWNED BY public."OADH_INT_State_Women_Temp".id;


--
-- TOC entry 362 (class 1259 OID 26752)
-- Name: OADH_INT_State_Women_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_State_Women_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_State_Women_id_seq" OWNER TO postgres;

--
-- TOC entry 4677 (class 0 OID 0)
-- Dependencies: 362
-- Name: OADH_INT_State_Women_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_State_Women_id_seq" OWNED BY public."OADH_INT_State_Women".id;


--
-- TOC entry 367 (class 1259 OID 26788)
-- Name: OADH_INT_Type_Sexual; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Type_Sexual" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Type_Sexual" OWNER TO postgres;

--
-- TOC entry 365 (class 1259 OID 26771)
-- Name: OADH_INT_Type_Sexual_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Type_Sexual_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Type_Sexual_Temp" OWNER TO postgres;

--
-- TOC entry 364 (class 1259 OID 26769)
-- Name: OADH_INT_Type_Sexual_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Type_Sexual_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Type_Sexual_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4678 (class 0 OID 0)
-- Dependencies: 364
-- Name: OADH_INT_Type_Sexual_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Type_Sexual_Temp_id_seq" OWNED BY public."OADH_INT_Type_Sexual_Temp".id;


--
-- TOC entry 366 (class 1259 OID 26786)
-- Name: OADH_INT_Type_Sexual_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Type_Sexual_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Type_Sexual_id_seq" OWNER TO postgres;

--
-- TOC entry 4679 (class 0 OID 0)
-- Dependencies: 366
-- Name: OADH_INT_Type_Sexual_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Type_Sexual_id_seq" OWNED BY public."OADH_INT_Type_Sexual".id;


--
-- TOC entry 343 (class 1259 OID 26605)
-- Name: OADH_INT_Type_Trafficking; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Type_Trafficking" (
    id integer NOT NULL,
    crime character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Type_Trafficking" OWNER TO postgres;

--
-- TOC entry 341 (class 1259 OID 26591)
-- Name: OADH_INT_Type_Trafficking_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Type_Trafficking_Temp" (
    id integer NOT NULL,
    crime character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Type_Trafficking_Temp" OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 26589)
-- Name: OADH_INT_Type_Trafficking_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Type_Trafficking_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Type_Trafficking_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4680 (class 0 OID 0)
-- Dependencies: 340
-- Name: OADH_INT_Type_Trafficking_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Type_Trafficking_Temp_id_seq" OWNED BY public."OADH_INT_Type_Trafficking_Temp".id;


--
-- TOC entry 342 (class 1259 OID 26603)
-- Name: OADH_INT_Type_Trafficking_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Type_Trafficking_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Type_Trafficking_id_seq" OWNER TO postgres;

--
-- TOC entry 4681 (class 0 OID 0)
-- Dependencies: 342
-- Name: OADH_INT_Type_Trafficking_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Type_Trafficking_id_seq" OWNED BY public."OADH_INT_Type_Trafficking".id;


--
-- TOC entry 359 (class 1259 OID 26723)
-- Name: OADH_INT_Type_Women; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Type_Women" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Type_Women" OWNER TO postgres;

--
-- TOC entry 357 (class 1259 OID 26709)
-- Name: OADH_INT_Type_Women_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Type_Women_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Type_Women_Temp" OWNER TO postgres;

--
-- TOC entry 356 (class 1259 OID 26707)
-- Name: OADH_INT_Type_Women_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Type_Women_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Type_Women_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4682 (class 0 OID 0)
-- Dependencies: 356
-- Name: OADH_INT_Type_Women_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Type_Women_Temp_id_seq" OWNED BY public."OADH_INT_Type_Women_Temp".id;


--
-- TOC entry 358 (class 1259 OID 26721)
-- Name: OADH_INT_Type_Women_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Type_Women_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Type_Women_id_seq" OWNER TO postgres;

--
-- TOC entry 4683 (class 0 OID 0)
-- Dependencies: 358
-- Name: OADH_INT_Type_Women_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Type_Women_id_seq" OWNED BY public."OADH_INT_Type_Women".id;


--
-- TOC entry 335 (class 1259 OID 26546)
-- Name: OADH_INT_Working_Officials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Working_Officials" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    category character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    department character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Working_Officials" OWNER TO postgres;

--
-- TOC entry 333 (class 1259 OID 26529)
-- Name: OADH_INT_Working_Officials_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_INT_Working_Officials_Temp" (
    id integer NOT NULL,
    year integer NOT NULL,
    crime character varying(255) NOT NULL,
    category character varying(255) NOT NULL,
    sex character(1) NOT NULL,
    department character varying(255) NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_INT_Working_Officials_Temp" OWNER TO postgres;

--
-- TOC entry 332 (class 1259 OID 26527)
-- Name: OADH_INT_Working_Officials_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Working_Officials_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Working_Officials_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4684 (class 0 OID 0)
-- Dependencies: 332
-- Name: OADH_INT_Working_Officials_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Working_Officials_Temp_id_seq" OWNED BY public."OADH_INT_Working_Officials_Temp".id;


--
-- TOC entry 334 (class 1259 OID 26544)
-- Name: OADH_INT_Working_Officials_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_INT_Working_Officials_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_INT_Working_Officials_id_seq" OWNER TO postgres;

--
-- TOC entry 4685 (class 0 OID 0)
-- Dependencies: 334
-- Name: OADH_INT_Working_Officials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_INT_Working_Officials_id_seq" OWNED BY public."OADH_INT_Working_Officials".id;


--
-- TOC entry 253 (class 1259 OID 17652)
-- Name: OADH_News; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_News" (
    id integer NOT NULL,
    date date NOT NULL,
    title character varying(60) NOT NULL,
    code character varying(20) NOT NULL,
    page integer,
    "time" time(0) without time zone,
    journalistic_genre character varying(150) NOT NULL,
    summary text,
    new_link character varying(60),
    note_locations text NOT NULL,
    twitter_link character varying(150),
    facebook_link character varying(150),
    screenshot_link character varying(150),
    organization_id integer NOT NULL,
    digital_media_id integer NOT NULL,
    section_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_News" OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 17739)
-- Name: OADH_News_Human_Rights; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_News_Human_Rights" (
    id integer NOT NULL,
    population_affected character varying(150),
    qualification smallint NOT NULL,
    justification character varying(50) NOT NULL,
    tracing_type_id integer NOT NULL,
    right_id integer NOT NULL,
    topic_id integer,
    violated_fact_id integer,
    news_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_News_Human_Rights" OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 17737)
-- Name: OADH_News_Human_Rights_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_News_Human_Rights_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_News_Human_Rights_id_seq" OWNER TO postgres;

--
-- TOC entry 4686 (class 0 OID 0)
-- Dependencies: 262
-- Name: OADH_News_Human_Rights_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_News_Human_Rights_id_seq" OWNED BY public."OADH_News_Human_Rights".id;


--
-- TOC entry 267 (class 1259 OID 25981)
-- Name: OADH_News_News; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_News_News" (
    id integer NOT NULL,
    related_news_id integer NOT NULL,
    news_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_News_News" OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 25979)
-- Name: OADH_News_News_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_News_News_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_News_News_id_seq" OWNER TO postgres;

--
-- TOC entry 4687 (class 0 OID 0)
-- Dependencies: 266
-- Name: OADH_News_News_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_News_News_id_seq" OWNED BY public."OADH_News_News".id;


--
-- TOC entry 252 (class 1259 OID 17650)
-- Name: OADH_News_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_News_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_News_id_seq" OWNER TO postgres;

--
-- TOC entry 4688 (class 0 OID 0)
-- Dependencies: 252
-- Name: OADH_News_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_News_id_seq" OWNED BY public."OADH_News".id;


--
-- TOC entry 399 (class 1259 OID 34187)
-- Name: OADH_POP_Age_Population; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_POP_Age_Population" (
    id integer NOT NULL,
    age integer NOT NULL,
    year integer NOT NULL,
    population integer NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_POP_Age_Population" OWNER TO postgres;

--
-- TOC entry 397 (class 1259 OID 34173)
-- Name: OADH_POP_Age_Population_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_POP_Age_Population_Temp" (
    id integer NOT NULL,
    age integer NOT NULL,
    year integer NOT NULL,
    population integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_POP_Age_Population_Temp" OWNER TO postgres;

--
-- TOC entry 396 (class 1259 OID 34171)
-- Name: OADH_POP_Age_Population_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_POP_Age_Population_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_POP_Age_Population_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4689 (class 0 OID 0)
-- Dependencies: 396
-- Name: OADH_POP_Age_Population_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_POP_Age_Population_Temp_id_seq" OWNED BY public."OADH_POP_Age_Population_Temp".id;


--
-- TOC entry 398 (class 1259 OID 34185)
-- Name: OADH_POP_Age_Population_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_POP_Age_Population_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_POP_Age_Population_id_seq" OWNER TO postgres;

--
-- TOC entry 4690 (class 0 OID 0)
-- Dependencies: 398
-- Name: OADH_POP_Age_Population_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_POP_Age_Population_id_seq" OWNED BY public."OADH_POP_Age_Population".id;


--
-- TOC entry 403 (class 1259 OID 34218)
-- Name: OADH_POP_Territory_Population; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_POP_Territory_Population" (
    id integer NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    year integer NOT NULL,
    population integer NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_POP_Territory_Population" OWNER TO postgres;

--
-- TOC entry 401 (class 1259 OID 34201)
-- Name: OADH_POP_Territory_Population_Temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_POP_Territory_Population_Temp" (
    id integer NOT NULL,
    department character varying(255) NOT NULL,
    municipality character varying(255) NOT NULL,
    year integer NOT NULL,
    population integer NOT NULL,
    status character(1) NOT NULL,
    file_id integer,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_POP_Territory_Population_Temp" OWNER TO postgres;

--
-- TOC entry 400 (class 1259 OID 34199)
-- Name: OADH_POP_Territory_Population_Temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_POP_Territory_Population_Temp_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_POP_Territory_Population_Temp_id_seq" OWNER TO postgres;

--
-- TOC entry 4691 (class 0 OID 0)
-- Dependencies: 400
-- Name: OADH_POP_Territory_Population_Temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_POP_Territory_Population_Temp_id_seq" OWNED BY public."OADH_POP_Territory_Population_Temp".id;


--
-- TOC entry 402 (class 1259 OID 34216)
-- Name: OADH_POP_Territory_Population_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_POP_Territory_Population_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_POP_Territory_Population_id_seq" OWNER TO postgres;

--
-- TOC entry 4692 (class 0 OID 0)
-- Dependencies: 402
-- Name: OADH_POP_Territory_Population_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_POP_Territory_Population_id_seq" OWNED BY public."OADH_POP_Territory_Population".id;


--
-- TOC entry 265 (class 1259 OID 17773)
-- Name: OADH_People; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_People" (
    id integer NOT NULL,
    type character(1) NOT NULL,
    subtype character(1) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    age integer,
    profesion character varying(40) NOT NULL,
    gender character(1),
    victimizer_relation boolean,
    main_victim_relation boolean,
    victim_relation boolean,
    organization_id integer NOT NULL,
    news_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_People" OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 17771)
-- Name: OADH_People_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_People_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_People_id_seq" OWNER TO postgres;

--
-- TOC entry 4693 (class 0 OID 0)
-- Dependencies: 264
-- Name: OADH_People_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_People_id_seq" OWNED BY public."OADH_People".id;


--
-- TOC entry 257 (class 1259 OID 17691)
-- Name: OADH_Producedure; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_Producedure" (
    id integer NOT NULL,
    place character varying(60),
    weapon character varying(255),
    hypothesis_fact text,
    context text,
    accident_type character varying(60),
    summary text,
    organization_id integer NOT NULL,
    news_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."OADH_Producedure" OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 17689)
-- Name: OADH_Producedure_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_Producedure_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_Producedure_id_seq" OWNER TO postgres;

--
-- TOC entry 4694 (class 0 OID 0)
-- Dependencies: 256
-- Name: OADH_Producedure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_Producedure_id_seq" OWNED BY public."OADH_Producedure".id;


--
-- TOC entry 249 (class 1259 OID 17626)
-- Name: OADH_Settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_Settings" (
    id integer NOT NULL,
    population_affected text NOT NULL,
    note_locations text NOT NULL,
    journalistic_genre text NOT NULL,
    is_configured boolean,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    sources text,
    sexual_diversities text,
    guns_types text
);


ALTER TABLE public."OADH_Settings" OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 17624)
-- Name: OADH_Settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_Settings_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_Settings_id_seq" OWNER TO postgres;

--
-- TOC entry 4695 (class 0 OID 0)
-- Dependencies: 248
-- Name: OADH_Settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_Settings_id_seq" OWNED BY public."OADH_Settings".id;


--
-- TOC entry 255 (class 1259 OID 17674)
-- Name: OADH_Source; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."OADH_Source" (
    id integer NOT NULL,
    source text NOT NULL,
    organization_id integer NOT NULL,
    news_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    instance character varying(255),
    responsable character varying(255)
);


ALTER TABLE public."OADH_Source" OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 17672)
-- Name: OADH_Source_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."OADH_Source_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."OADH_Source_id_seq" OWNER TO postgres;

--
-- TOC entry 4696 (class 0 OID 0)
-- Dependencies: 254
-- Name: OADH_Source_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."OADH_Source_id_seq" OWNED BY public."OADH_Source".id;


--
-- TOC entry 198 (class 1259 OID 16394)
-- Name: ORG_Organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."ORG_Organization" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    street1 character varying(255),
    street2 character varying(255),
    city_name character varying(255),
    state_name character varying(255),
    zip_code character varying(20),
    web_site character varying(255),
    phone_number character varying(60),
    fax character varying(60),
    email character varying(255),
    tax_id character varying(255),
    company_registration character varying(255),
    commercial_trade character varying(255),
    logo_url text,
    cost_price_precision smallint DEFAULT '2'::smallint NOT NULL,
    discount_precision smallint DEFAULT '2'::smallint NOT NULL,
    database_connection_name character varying(60) NOT NULL,
    api_token character varying(60),
    sale_point_quantity smallint DEFAULT '1'::smallint NOT NULL,
    country_id integer NOT NULL,
    currency_id integer,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."ORG_Organization" OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16403)
-- Name: ORG_Organization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."ORG_Organization_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ORG_Organization_id_seq" OWNER TO postgres;

--
-- TOC entry 4697 (class 0 OID 0)
-- Dependencies: 199
-- Name: ORG_Organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."ORG_Organization_id_seq" OWNED BY public."ORG_Organization".id;


--
-- TOC entry 200 (class 1259 OID 16405)
-- Name: SEC_Failed_Jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Failed_Jobs" (
    id integer NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    failed_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public."SEC_Failed_Jobs" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16411)
-- Name: SEC_Failed_Jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Failed_Jobs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Failed_Jobs_id_seq" OWNER TO postgres;

--
-- TOC entry 4698 (class 0 OID 0)
-- Dependencies: 201
-- Name: SEC_Failed_Jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Failed_Jobs_id_seq" OWNED BY public."SEC_Failed_Jobs".id;


--
-- TOC entry 202 (class 1259 OID 16413)
-- Name: SEC_File; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_File" (
    id integer NOT NULL,
    file_id integer NOT NULL,
    key character varying(255) NOT NULL,
    organization_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_File" OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16416)
-- Name: SEC_File_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_File_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_File_id_seq" OWNER TO postgres;

--
-- TOC entry 4699 (class 0 OID 0)
-- Dependencies: 203
-- Name: SEC_File_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_File_id_seq" OWNED BY public."SEC_File".id;


--
-- TOC entry 204 (class 1259 OID 16418)
-- Name: SEC_Journal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Journal" (
    id integer NOT NULL,
    journalized_id integer NOT NULL,
    journalized_type character varying(30) NOT NULL,
    user_id integer NOT NULL,
    organization_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Journal" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16421)
-- Name: SEC_Journal_Detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Journal_Detail" (
    id integer NOT NULL,
    field character varying(60),
    field_lang_key character varying(100),
    note character varying(255),
    old_value text,
    new_value text,
    journal_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Journal_Detail" OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16427)
-- Name: SEC_Journal_Detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Journal_Detail_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Journal_Detail_id_seq" OWNER TO postgres;

--
-- TOC entry 4700 (class 0 OID 0)
-- Dependencies: 206
-- Name: SEC_Journal_Detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Journal_Detail_id_seq" OWNED BY public."SEC_Journal_Detail".id;


--
-- TOC entry 207 (class 1259 OID 16429)
-- Name: SEC_Journal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Journal_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Journal_id_seq" OWNER TO postgres;

--
-- TOC entry 4701 (class 0 OID 0)
-- Dependencies: 207
-- Name: SEC_Journal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Journal_id_seq" OWNED BY public."SEC_Journal".id;


--
-- TOC entry 208 (class 1259 OID 16431)
-- Name: SEC_Menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Menu" (
    id integer NOT NULL,
    name character varying(300) NOT NULL,
    lang_key character varying(100),
    url character varying(300),
    action_button_id character varying(60) DEFAULT ''::character varying NOT NULL,
    action_lang_key character varying(100),
    icon character varying(60),
    parent_id integer,
    created_by integer NOT NULL,
    module_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Menu" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16438)
-- Name: SEC_Menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Menu_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Menu_id_seq" OWNER TO postgres;

--
-- TOC entry 4702 (class 0 OID 0)
-- Dependencies: 209
-- Name: SEC_Menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Menu_id_seq" OWNED BY public."SEC_Menu".id;


--
-- TOC entry 210 (class 1259 OID 16440)
-- Name: SEC_Module; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Module" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    lang_key character varying(100),
    icon character varying(60),
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Module" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16443)
-- Name: SEC_Module_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Module_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Module_id_seq" OWNER TO postgres;

--
-- TOC entry 4703 (class 0 OID 0)
-- Dependencies: 211
-- Name: SEC_Module_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Module_id_seq" OWNED BY public."SEC_Module".id;


--
-- TOC entry 212 (class 1259 OID 16445)
-- Name: SEC_Password_Reminders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Password_Reminders" (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public."SEC_Password_Reminders" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16451)
-- Name: SEC_Permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Permission" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    key character varying(60) NOT NULL,
    lang_key character varying(100),
    url character varying(300),
    alias_url character varying(300),
    action_button_id character varying(60),
    action_lang_key character varying(100),
    icon character varying(60),
    shortcut_icon character varying(60),
    is_only_shortcut boolean DEFAULT false NOT NULL,
    is_dashboard_shortcut_visible boolean DEFAULT false NOT NULL,
    hidden boolean DEFAULT false NOT NULL,
    menu_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Permission" OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16460)
-- Name: SEC_Permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Permission_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Permission_id_seq" OWNER TO postgres;

--
-- TOC entry 4704 (class 0 OID 0)
-- Dependencies: 214
-- Name: SEC_Permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Permission_id_seq" OWNED BY public."SEC_Permission".id;


--
-- TOC entry 215 (class 1259 OID 16462)
-- Name: SEC_Role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Role" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    lang_key character varying(100),
    description character varying(255),
    organization_id integer,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Role" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16465)
-- Name: SEC_Role_Menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Role_Menu" (
    id integer NOT NULL,
    role_id integer NOT NULL,
    menu_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Role_Menu" OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16468)
-- Name: SEC_Role_Menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Role_Menu_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Role_Menu_id_seq" OWNER TO postgres;

--
-- TOC entry 4705 (class 0 OID 0)
-- Dependencies: 217
-- Name: SEC_Role_Menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Role_Menu_id_seq" OWNED BY public."SEC_Role_Menu".id;


--
-- TOC entry 218 (class 1259 OID 16470)
-- Name: SEC_Role_Permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_Role_Permission" (
    id integer NOT NULL,
    role_id integer NOT NULL,
    permission_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_Role_Permission" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16473)
-- Name: SEC_Role_Permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Role_Permission_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Role_Permission_id_seq" OWNER TO postgres;

--
-- TOC entry 4706 (class 0 OID 0)
-- Dependencies: 219
-- Name: SEC_Role_Permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Role_Permission_id_seq" OWNED BY public."SEC_Role_Permission".id;


--
-- TOC entry 220 (class 1259 OID 16475)
-- Name: SEC_Role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_Role_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_Role_id_seq" OWNER TO postgres;

--
-- TOC entry 4707 (class 0 OID 0)
-- Dependencies: 220
-- Name: SEC_Role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_Role_id_seq" OWNED BY public."SEC_Role".id;


--
-- TOC entry 221 (class 1259 OID 16477)
-- Name: SEC_User; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User" (
    id integer NOT NULL,
    firstname character varying(60) NOT NULL,
    lastname character varying(60) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    is_active boolean NOT NULL,
    is_admin boolean NOT NULL,
    timezone character varying(60),
    activation_code character varying(255),
    activated_at timestamp(0) without time zone,
    last_login timestamp(0) without time zone,
    popovers_shown boolean DEFAULT false NOT NULL,
    multiple_organization_popover_shown boolean DEFAULT false NOT NULL,
    remember_token character varying(100),
    created_by integer,
    default_organization integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User" OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16485)
-- Name: SEC_User_Menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User_Menu" (
    id integer NOT NULL,
    is_assigned boolean NOT NULL,
    user_id integer NOT NULL,
    menu_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User_Menu" OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16488)
-- Name: SEC_User_Menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_Menu_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_Menu_id_seq" OWNER TO postgres;

--
-- TOC entry 4708 (class 0 OID 0)
-- Dependencies: 223
-- Name: SEC_User_Menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_Menu_id_seq" OWNED BY public."SEC_User_Menu".id;


--
-- TOC entry 224 (class 1259 OID 16490)
-- Name: SEC_User_Organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User_Organization" (
    id integer NOT NULL,
    user_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User_Organization" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16493)
-- Name: SEC_User_Organization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_Organization_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_Organization_id_seq" OWNER TO postgres;

--
-- TOC entry 4709 (class 0 OID 0)
-- Dependencies: 225
-- Name: SEC_User_Organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_Organization_id_seq" OWNED BY public."SEC_User_Organization".id;


--
-- TOC entry 226 (class 1259 OID 16495)
-- Name: SEC_User_Permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User_Permission" (
    id integer NOT NULL,
    is_assigned boolean NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User_Permission" OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16498)
-- Name: SEC_User_Permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_Permission_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_Permission_id_seq" OWNER TO postgres;

--
-- TOC entry 4710 (class 0 OID 0)
-- Dependencies: 227
-- Name: SEC_User_Permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_Permission_id_seq" OWNED BY public."SEC_User_Permission".id;


--
-- TOC entry 228 (class 1259 OID 16500)
-- Name: SEC_User_Role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SEC_User_Role" (
    id integer NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    organization_id integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SEC_User_Role" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16503)
-- Name: SEC_User_Role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_Role_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_Role_id_seq" OWNER TO postgres;

--
-- TOC entry 4711 (class 0 OID 0)
-- Dependencies: 229
-- Name: SEC_User_Role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_Role_id_seq" OWNED BY public."SEC_User_Role".id;


--
-- TOC entry 230 (class 1259 OID 16505)
-- Name: SEC_User_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SEC_User_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SEC_User_id_seq" OWNER TO postgres;

--
-- TOC entry 4712 (class 0 OID 0)
-- Dependencies: 230
-- Name: SEC_User_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SEC_User_id_seq" OWNED BY public."SEC_User".id;


--
-- TOC entry 231 (class 1259 OID 16507)
-- Name: SYS_Account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Account" (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    parent_key character varying(255),
    name character varying(255) NOT NULL,
    balance_type character(1) NOT NULL,
    account_type_key character(1) NOT NULL,
    is_group boolean DEFAULT false NOT NULL,
    is_general_ledger_account boolean DEFAULT false NOT NULL,
    account_chart_type_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Account" OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 16515)
-- Name: SYS_Account_Chart_Type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Account_Chart_Type" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    url character varying(255) NOT NULL,
    lang_key character varying(100),
    country_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Account_Chart_Type" OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16518)
-- Name: SYS_Account_Chart_Type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Account_Chart_Type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Account_Chart_Type_id_seq" OWNER TO postgres;

--
-- TOC entry 4713 (class 0 OID 0)
-- Dependencies: 233
-- Name: SYS_Account_Chart_Type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Account_Chart_Type_id_seq" OWNED BY public."SYS_Account_Chart_Type".id;


--
-- TOC entry 234 (class 1259 OID 16520)
-- Name: SYS_Account_Type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Account_Type" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    lang_key character varying(100),
    key character(1) NOT NULL,
    pl_bs_category character(1) NOT NULL,
    deferral_method character(1) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Account_Type" OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 16523)
-- Name: SYS_Account_Type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Account_Type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Account_Type_id_seq" OWNER TO postgres;

--
-- TOC entry 4714 (class 0 OID 0)
-- Dependencies: 235
-- Name: SYS_Account_Type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Account_Type_id_seq" OWNED BY public."SYS_Account_Type".id;


--
-- TOC entry 236 (class 1259 OID 16525)
-- Name: SYS_Account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Account_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Account_id_seq" OWNER TO postgres;

--
-- TOC entry 4715 (class 0 OID 0)
-- Dependencies: 236
-- Name: SYS_Account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Account_id_seq" OWNED BY public."SYS_Account".id;


--
-- TOC entry 237 (class 1259 OID 16527)
-- Name: SYS_Country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Country" (
    id integer NOT NULL,
    iso_code character varying(3) NOT NULL,
    name character varying(60) NOT NULL,
    region_name character varying(60) NOT NULL,
    region_lang_key character varying(100) NOT NULL,
    tax_id_name character varying(255),
    tax_id_abbreviation character varying(255),
    registration_number_name character varying(255),
    registration_number_abbreviation character varying(255),
    single_identity_document_number_name character varying(255),
    single_identity_document_number_abbreviation character varying(255),
    social_security_number_name character varying(255),
    social_security_number_abbreviation character varying(255),
    single_previsional_number_name character varying(255),
    single_previsional_number_abbreviation character varying(255),
    currency_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Country" OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 16533)
-- Name: SYS_Country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Country_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Country_id_seq" OWNER TO postgres;

--
-- TOC entry 4716 (class 0 OID 0)
-- Dependencies: 238
-- Name: SYS_Country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Country_id_seq" OWNED BY public."SYS_Country".id;


--
-- TOC entry 239 (class 1259 OID 16535)
-- Name: SYS_Currency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Currency" (
    id integer NOT NULL,
    iso_code character varying(3) NOT NULL,
    symbol character varying(10) NOT NULL,
    name character varying(60) NOT NULL,
    standard_precision smallint NOT NULL,
    costing_precision smallint NOT NULL,
    price_precision smallint NOT NULL,
    currency_symbol_at_the_right boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Currency" OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 16538)
-- Name: SYS_Currency_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Currency_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Currency_id_seq" OWNER TO postgres;

--
-- TOC entry 4717 (class 0 OID 0)
-- Dependencies: 240
-- Name: SYS_Currency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Currency_id_seq" OWNED BY public."SYS_Currency".id;


--
-- TOC entry 241 (class 1259 OID 16540)
-- Name: SYS_Region; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Region" (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    region_code character varying(4),
    region_lang_key character varying(100),
    country_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Region" OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 16543)
-- Name: SYS_Region_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Region_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Region_id_seq" OWNER TO postgres;

--
-- TOC entry 4718 (class 0 OID 0)
-- Dependencies: 242
-- Name: SYS_Region_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Region_id_seq" OWNED BY public."SYS_Region".id;


--
-- TOC entry 243 (class 1259 OID 16545)
-- Name: SYS_Voucher_Type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SYS_Voucher_Type" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    lang_key character varying(100),
    key character(1),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public."SYS_Voucher_Type" OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 16548)
-- Name: SYS_Voucher_Type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."SYS_Voucher_Type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."SYS_Voucher_Type_id_seq" OWNER TO postgres;

--
-- TOC entry 4719 (class 0 OID 0)
-- Dependencies: 244
-- Name: SYS_Voucher_Type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."SYS_Voucher_Type_id_seq" OWNED BY public."SYS_Voucher_Type".id;


--
-- TOC entry 245 (class 1259 OID 16550)
-- Name: cache; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cache (
    key character varying(255) NOT NULL,
    value text NOT NULL,
    expiration integer NOT NULL
);


ALTER TABLE public.cache OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 16556)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 16559)
-- Name: sessions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sessions (
    id character varying(255) NOT NULL,
    user_id integer,
    ip_address character varying(45),
    user_agent text,
    payload text NOT NULL,
    last_activity integer NOT NULL
);


ALTER TABLE public.sessions OWNER TO postgres;

--
-- TOC entry 3724 (class 2604 OID 16565)
-- Name: FILE_File id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."FILE_File" ALTER COLUMN id SET DEFAULT nextval('public."FILE_File_id_seq"'::regclass);


--
-- TOC entry 3764 (class 2604 OID 17711)
-- Name: OADH_Context id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Context" ALTER COLUMN id SET DEFAULT nextval('public."OADH_Context_id_seq"'::regclass);


--
-- TOC entry 3760 (class 2604 OID 17641)
-- Name: OADH_Digital_Media id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Digital_Media" ALTER COLUMN id SET DEFAULT nextval('public."OADH_Digital_Media_id_seq"'::regclass);


--
-- TOC entry 3785 (class 2604 OID 26246)
-- Name: OADH_FREE_Accused_Liberty_Deprivation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Accused_Liberty_Deprivation" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Accused_Liberty_Deprivation_id_seq"'::regclass);


--
-- TOC entry 3784 (class 2604 OID 26229)
-- Name: OADH_FREE_Accused_Liberty_Deprivation_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Accused_Liberty_Deprivation_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Accused_Liberty_Deprivation_Temp_id_seq"'::regclass);


--
-- TOC entry 3793 (class 2604 OID 26382)
-- Name: OADH_FREE_Acute_Diseases id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Acute_Diseases" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Acute_Diseases_id_seq"'::regclass);


--
-- TOC entry 3792 (class 2604 OID 26365)
-- Name: OADH_FREE_Acute_Diseases_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Acute_Diseases_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Acute_Diseases_Temp_id_seq"'::regclass);


--
-- TOC entry 3781 (class 2604 OID 26178)
-- Name: OADH_FREE_Capture_Orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Capture_Orders" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Capture_Orders_id_seq"'::regclass);


--
-- TOC entry 3780 (class 2604 OID 26161)
-- Name: OADH_FREE_Capture_Orders_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Capture_Orders_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Capture_Orders_Temp_id_seq"'::regclass);


--
-- TOC entry 3795 (class 2604 OID 26413)
-- Name: OADH_FREE_Chronic_Diseases id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Chronic_Diseases" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Chronic_Diseases_id_seq"'::regclass);


--
-- TOC entry 3794 (class 2604 OID 26396)
-- Name: OADH_FREE_Chronic_Diseases_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Chronic_Diseases_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Chronic_Diseases_Temp_id_seq"'::regclass);


--
-- TOC entry 3775 (class 2604 OID 26082)
-- Name: OADH_FREE_Criminal_Cases_Trials id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Criminal_Cases_Trials" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Criminal_Cases_Trials_id_seq"'::regclass);


--
-- TOC entry 3774 (class 2604 OID 26065)
-- Name: OADH_FREE_Criminal_Cases_Trials_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Criminal_Cases_Trials_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Criminal_Cases_Trials_Temp_id_seq"'::regclass);


--
-- TOC entry 3791 (class 2604 OID 26348)
-- Name: OADH_FREE_Deprived_Persons id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Deprived_Persons" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Deprived_Persons_id_seq"'::regclass);


--
-- TOC entry 3790 (class 2604 OID 26331)
-- Name: OADH_FREE_Deprived_Persons_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Deprived_Persons_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Deprived_Persons_Temp_id_seq"'::regclass);


--
-- TOC entry 3783 (class 2604 OID 26212)
-- Name: OADH_FREE_Disappearances_Victims id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Disappearances_Victims" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Disappearances_Victims_id_seq"'::regclass);


--
-- TOC entry 3782 (class 2604 OID 26195)
-- Name: OADH_FREE_Disappearances_Victims_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Disappearances_Victims_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Disappearances_Victims_Temp_id_seq"'::regclass);


--
-- TOC entry 3788 (class 2604 OID 26297)
-- Name: OADH_FREE_Forced_Disappearance_Temps id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearance_Temps" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Forced_Disappearance_Temps_id_seq"'::regclass);


--
-- TOC entry 3789 (class 2604 OID 26314)
-- Name: OADH_FREE_Forced_Disappearances id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearances" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Forced_Disappearances_id_seq"'::regclass);


--
-- TOC entry 3771 (class 2604 OID 26017)
-- Name: OADH_FREE_Habeas_Corpus_Request id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Habeas_Corpus_Request" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Habeas_Corpus_Request_id_seq"'::regclass);


--
-- TOC entry 3769 (class 2604 OID 26003)
-- Name: OADH_FREE_Habeas_Corpus_Request_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Habeas_Corpus_Request_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Habeas_Corpus_Request_Temp_id_seq"'::regclass);


--
-- TOC entry 3797 (class 2604 OID 26447)
-- Name: OADH_FREE_Jails_Occupation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Jails_Occupation" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Jails_Occupation_id_seq"'::regclass);


--
-- TOC entry 3796 (class 2604 OID 26430)
-- Name: OADH_FREE_Jails_Occupation_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Jails_Occupation_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Jails_Occupation_Temp_id_seq"'::regclass);


--
-- TOC entry 3777 (class 2604 OID 26113)
-- Name: OADH_FREE_Mass_Trials id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Mass_Trials" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Mass_Trials_id_seq"'::regclass);


--
-- TOC entry 3776 (class 2604 OID 26099)
-- Name: OADH_FREE_Mass_Trials_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Mass_Trials_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Mass_Trials_Temp_id_seq"'::regclass);


--
-- TOC entry 3773 (class 2604 OID 26048)
-- Name: OADH_FREE_People_Detained id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_People_Detained" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_People_Detained_id_seq"'::regclass);


--
-- TOC entry 3772 (class 2604 OID 26031)
-- Name: OADH_FREE_People_Detained_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_People_Detained_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_People_Detained_Temp_id_seq"'::regclass);


--
-- TOC entry 3779 (class 2604 OID 26144)
-- Name: OADH_FREE_Procedural_Fraud id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Procedural_Fraud" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Procedural_Fraud_id_seq"'::regclass);


--
-- TOC entry 3778 (class 2604 OID 26127)
-- Name: OADH_FREE_Procedural_Fraud_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Procedural_Fraud_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Procedural_Fraud_Temp_id_seq"'::regclass);


--
-- TOC entry 3787 (class 2604 OID 26280)
-- Name: OADH_FREE_Victims_Liberty_Deprivation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Victims_Liberty_Deprivation" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Victims_Liberty_Deprivation_id_seq"'::regclass);


--
-- TOC entry 3786 (class 2604 OID 26263)
-- Name: OADH_FREE_Victims_Liberty_Deprivation_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Victims_Liberty_Deprivation_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_FREE_Victims_Liberty_Deprivation_Temp_id_seq"'::regclass);


--
-- TOC entry 3765 (class 2604 OID 17725)
-- Name: OADH_Human_Right id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Human_Right" ALTER COLUMN id SET DEFAULT nextval('public."OADH_Human_Right_id_seq"'::regclass);


--
-- TOC entry 3827 (class 2604 OID 26918)
-- Name: OADH_INT_Age_Injuries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Injuries" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Age_Injuries_id_seq"'::regclass);


--
-- TOC entry 3826 (class 2604 OID 26904)
-- Name: OADH_INT_Age_Injuries_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Injuries_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Age_Injuries_Temp_id_seq"'::regclass);


--
-- TOC entry 3823 (class 2604 OID 26853)
-- Name: OADH_INT_Age_Sexual id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Sexual" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Age_Sexual_id_seq"'::regclass);


--
-- TOC entry 3822 (class 2604 OID 26836)
-- Name: OADH_INT_Age_Sexual_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Sexual_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Age_Sexual_Temp_id_seq"'::regclass);


--
-- TOC entry 3809 (class 2604 OID 26636)
-- Name: OADH_INT_Age_Trafficking id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Trafficking" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Age_Trafficking_id_seq"'::regclass);


--
-- TOC entry 3808 (class 2604 OID 26622)
-- Name: OADH_INT_Age_Trafficking_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Trafficking_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Age_Trafficking_Temp_id_seq"'::regclass);


--
-- TOC entry 3813 (class 2604 OID 26698)
-- Name: OADH_INT_Age_Women id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Women" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Age_Women_id_seq"'::regclass);


--
-- TOC entry 3812 (class 2604 OID 26684)
-- Name: OADH_INT_Age_Women_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Women_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Age_Women_Temp_id_seq"'::regclass);


--
-- TOC entry 3831 (class 2604 OID 26983)
-- Name: OADH_INT_Agent_Investigation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Agent_Investigation" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Agent_Investigation_id_seq"'::regclass);


--
-- TOC entry 3830 (class 2604 OID 26966)
-- Name: OADH_INT_Agent_Investigation_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Agent_Investigation_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Agent_Investigation_Temp_id_seq"'::regclass);


--
-- TOC entry 3801 (class 2604 OID 26515)
-- Name: OADH_INT_Crimes_Accused id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Accused" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Crimes_Accused_id_seq"'::regclass);


--
-- TOC entry 3800 (class 2604 OID 26498)
-- Name: OADH_INT_Crimes_Accused_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Accused_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Crimes_Accused_Temp_id_seq"'::regclass);


--
-- TOC entry 3799 (class 2604 OID 26481)
-- Name: OADH_INT_Crimes_Victims id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Victims" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Crimes_Victims_id_seq"'::regclass);


--
-- TOC entry 3798 (class 2604 OID 26464)
-- Name: OADH_INT_Crimes_Victims_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Victims_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Crimes_Victims_Temp_id_seq"'::regclass);


--
-- TOC entry 3821 (class 2604 OID 26822)
-- Name: OADH_INT_Gender_Sexual id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Sexual" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Gender_Sexual_id_seq"'::regclass);


--
-- TOC entry 3820 (class 2604 OID 26808)
-- Name: OADH_INT_Gender_Sexual_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Sexual_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Gender_Sexual_Temp_id_seq"'::regclass);


--
-- TOC entry 3805 (class 2604 OID 26580)
-- Name: OADH_INT_Gender_Trafficking id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Trafficking" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Gender_Trafficking_id_seq"'::regclass);


--
-- TOC entry 3804 (class 2604 OID 26566)
-- Name: OADH_INT_Gender_Trafficking_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Trafficking_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Gender_Trafficking_Temp_id_seq"'::regclass);


--
-- TOC entry 3833 (class 2604 OID 27017)
-- Name: OADH_INT_Integrity_Violation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Integrity_Violation" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Integrity_Violation_id_seq"'::regclass);


--
-- TOC entry 3832 (class 2604 OID 27000)
-- Name: OADH_INT_Integrity_Violation_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Integrity_Violation_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Integrity_Violation_Temp_id_seq"'::regclass);


--
-- TOC entry 3811 (class 2604 OID 26667)
-- Name: OADH_INT_Movement_Freedom id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Movement_Freedom" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Movement_Freedom_id_seq"'::regclass);


--
-- TOC entry 3810 (class 2604 OID 26650)
-- Name: OADH_INT_Movement_Freedom_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Movement_Freedom_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Movement_Freedom_Temp_id_seq"'::regclass);


--
-- TOC entry 3829 (class 2604 OID 26949)
-- Name: OADH_INT_Schedule_Injuries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Schedule_Injuries" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Schedule_Injuries_id_seq"'::regclass);


--
-- TOC entry 3828 (class 2604 OID 26932)
-- Name: OADH_INT_Schedule_Injuries_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Schedule_Injuries_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Schedule_Injuries_Temp_id_seq"'::regclass);


--
-- TOC entry 3825 (class 2604 OID 26887)
-- Name: OADH_INT_State_Injuries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Injuries" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_State_Injuries_id_seq"'::regclass);


--
-- TOC entry 3824 (class 2604 OID 26870)
-- Name: OADH_INT_State_Injuries_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Injuries_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_State_Injuries_Temp_id_seq"'::regclass);


--
-- TOC entry 3817 (class 2604 OID 26757)
-- Name: OADH_INT_State_Women id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Women" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_State_Women_id_seq"'::regclass);


--
-- TOC entry 3816 (class 2604 OID 26740)
-- Name: OADH_INT_State_Women_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Women_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_State_Women_Temp_id_seq"'::regclass);


--
-- TOC entry 3819 (class 2604 OID 26791)
-- Name: OADH_INT_Type_Sexual id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Sexual" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Type_Sexual_id_seq"'::regclass);


--
-- TOC entry 3818 (class 2604 OID 26774)
-- Name: OADH_INT_Type_Sexual_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Sexual_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Type_Sexual_Temp_id_seq"'::regclass);


--
-- TOC entry 3807 (class 2604 OID 26608)
-- Name: OADH_INT_Type_Trafficking id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Trafficking" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Type_Trafficking_id_seq"'::regclass);


--
-- TOC entry 3806 (class 2604 OID 26594)
-- Name: OADH_INT_Type_Trafficking_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Trafficking_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Type_Trafficking_Temp_id_seq"'::regclass);


--
-- TOC entry 3815 (class 2604 OID 26726)
-- Name: OADH_INT_Type_Women id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Women" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Type_Women_id_seq"'::regclass);


--
-- TOC entry 3814 (class 2604 OID 26712)
-- Name: OADH_INT_Type_Women_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Women_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Type_Women_Temp_id_seq"'::regclass);


--
-- TOC entry 3803 (class 2604 OID 26549)
-- Name: OADH_INT_Working_Officials id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Working_Officials" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Working_Officials_id_seq"'::regclass);


--
-- TOC entry 3802 (class 2604 OID 26532)
-- Name: OADH_INT_Working_Officials_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Working_Officials_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_INT_Working_Officials_Temp_id_seq"'::regclass);


--
-- TOC entry 3761 (class 2604 OID 17655)
-- Name: OADH_News id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News" ALTER COLUMN id SET DEFAULT nextval('public."OADH_News_id_seq"'::regclass);


--
-- TOC entry 3766 (class 2604 OID 17742)
-- Name: OADH_News_Human_Rights id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_Human_Rights" ALTER COLUMN id SET DEFAULT nextval('public."OADH_News_Human_Rights_id_seq"'::regclass);


--
-- TOC entry 3768 (class 2604 OID 25984)
-- Name: OADH_News_News id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_News" ALTER COLUMN id SET DEFAULT nextval('public."OADH_News_News_id_seq"'::regclass);


--
-- TOC entry 3835 (class 2604 OID 34190)
-- Name: OADH_POP_Age_Population id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Age_Population" ALTER COLUMN id SET DEFAULT nextval('public."OADH_POP_Age_Population_id_seq"'::regclass);


--
-- TOC entry 3834 (class 2604 OID 34176)
-- Name: OADH_POP_Age_Population_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Age_Population_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_POP_Age_Population_Temp_id_seq"'::regclass);


--
-- TOC entry 3837 (class 2604 OID 34221)
-- Name: OADH_POP_Territory_Population id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Territory_Population" ALTER COLUMN id SET DEFAULT nextval('public."OADH_POP_Territory_Population_id_seq"'::regclass);


--
-- TOC entry 3836 (class 2604 OID 34204)
-- Name: OADH_POP_Territory_Population_Temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Territory_Population_Temp" ALTER COLUMN id SET DEFAULT nextval('public."OADH_POP_Territory_Population_Temp_id_seq"'::regclass);


--
-- TOC entry 3767 (class 2604 OID 17776)
-- Name: OADH_People id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_People" ALTER COLUMN id SET DEFAULT nextval('public."OADH_People_id_seq"'::regclass);


--
-- TOC entry 3763 (class 2604 OID 17694)
-- Name: OADH_Producedure id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Producedure" ALTER COLUMN id SET DEFAULT nextval('public."OADH_Producedure_id_seq"'::regclass);


--
-- TOC entry 3759 (class 2604 OID 17629)
-- Name: OADH_Settings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Settings" ALTER COLUMN id SET DEFAULT nextval('public."OADH_Settings_id_seq"'::regclass);


--
-- TOC entry 3762 (class 2604 OID 17677)
-- Name: OADH_Source id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Source" ALTER COLUMN id SET DEFAULT nextval('public."OADH_Source_id_seq"'::regclass);


--
-- TOC entry 3728 (class 2604 OID 16566)
-- Name: ORG_Organization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization" ALTER COLUMN id SET DEFAULT nextval('public."ORG_Organization_id_seq"'::regclass);


--
-- TOC entry 3729 (class 2604 OID 16567)
-- Name: SEC_Failed_Jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Failed_Jobs" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Failed_Jobs_id_seq"'::regclass);


--
-- TOC entry 3730 (class 2604 OID 16568)
-- Name: SEC_File id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_File" ALTER COLUMN id SET DEFAULT nextval('public."SEC_File_id_seq"'::regclass);


--
-- TOC entry 3731 (class 2604 OID 16569)
-- Name: SEC_Journal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Journal_id_seq"'::regclass);


--
-- TOC entry 3732 (class 2604 OID 16570)
-- Name: SEC_Journal_Detail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal_Detail" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Journal_Detail_id_seq"'::regclass);


--
-- TOC entry 3734 (class 2604 OID 16571)
-- Name: SEC_Menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Menu_id_seq"'::regclass);


--
-- TOC entry 3735 (class 2604 OID 16572)
-- Name: SEC_Module id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Module" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Module_id_seq"'::regclass);


--
-- TOC entry 3739 (class 2604 OID 16573)
-- Name: SEC_Permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Permission" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Permission_id_seq"'::regclass);


--
-- TOC entry 3740 (class 2604 OID 16574)
-- Name: SEC_Role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Role_id_seq"'::regclass);


--
-- TOC entry 3741 (class 2604 OID 16575)
-- Name: SEC_Role_Menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Role_Menu_id_seq"'::regclass);


--
-- TOC entry 3742 (class 2604 OID 16576)
-- Name: SEC_Role_Permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission" ALTER COLUMN id SET DEFAULT nextval('public."SEC_Role_Permission_id_seq"'::regclass);


--
-- TOC entry 3745 (class 2604 OID 16577)
-- Name: SEC_User id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_id_seq"'::regclass);


--
-- TOC entry 3746 (class 2604 OID 16578)
-- Name: SEC_User_Menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_Menu_id_seq"'::regclass);


--
-- TOC entry 3747 (class 2604 OID 16579)
-- Name: SEC_User_Organization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_Organization_id_seq"'::regclass);


--
-- TOC entry 3748 (class 2604 OID 16580)
-- Name: SEC_User_Permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_Permission_id_seq"'::regclass);


--
-- TOC entry 3749 (class 2604 OID 16581)
-- Name: SEC_User_Role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role" ALTER COLUMN id SET DEFAULT nextval('public."SEC_User_Role_id_seq"'::regclass);


--
-- TOC entry 3752 (class 2604 OID 16582)
-- Name: SYS_Account id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Account_id_seq"'::regclass);


--
-- TOC entry 3753 (class 2604 OID 16583)
-- Name: SYS_Account_Chart_Type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Chart_Type" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Account_Chart_Type_id_seq"'::regclass);


--
-- TOC entry 3754 (class 2604 OID 16584)
-- Name: SYS_Account_Type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Type" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Account_Type_id_seq"'::regclass);


--
-- TOC entry 3755 (class 2604 OID 16585)
-- Name: SYS_Country id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Country" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Country_id_seq"'::regclass);


--
-- TOC entry 3756 (class 2604 OID 16586)
-- Name: SYS_Currency id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Currency" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Currency_id_seq"'::regclass);


--
-- TOC entry 3757 (class 2604 OID 16587)
-- Name: SYS_Region id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Region" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Region_id_seq"'::regclass);


--
-- TOC entry 3758 (class 2604 OID 16588)
-- Name: SYS_Voucher_Type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Voucher_Type" ALTER COLUMN id SET DEFAULT nextval('public."SYS_Voucher_Type_id_seq"'::regclass);


--
-- TOC entry 4405 (class 0 OID 16386)
-- Dependencies: 196
-- Data for Name: FILE_File; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."FILE_File" (id, name, type, system_type, system_route, url, url_html, is_public, key, icon, icon_html, width, height, parent_file_id, system_reference_type, system_reference_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	Ejemplos	C	\N	\N	\N	\N	\N	\N	fa fa-folder-o	<i class="fa fa-folder-o" aria-hidden="true" style="font-size: 2em;color:#416099;"></i>	\N	\N	\N	\N	\N	1	2019-03-24 18:50:23	2019-03-24 18:50:23	\N
7	estadocuenta6840010153.xls	A	spreadsheet	organizations/1/estadocuenta6840010153.xls	http://localhost:8000/files/file-manager/serve/private/$2y$10$Bmil3IKMZFdK0DpY1fgFeNwSJeYbGpbGDO9e8AfGIT8SWR3yYquu	<a href="http://localhost:8000/files/file-manager/serve/private/$2y$10$Bmil3IKMZFdK0DpY1fgFeNwSJeYbGpbGDO9e8AfGIT8SWR3yYquu" target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size: 2em;color:#1E448B;"></i></a>	f	$2y$10$Bmil3IKMZFdK0DpY1fgFeNwSJeYbGpbGDO9e8AfGIT8SWR3yYquu	fa fa-file-excel-o	<i class="fa fa-file-excel-o" aria-hidden="true" style="font-size: 2em;color:#9FCC23;"></i>	\N	\N	1	\N	\N	1	2019-03-24 18:51:38	2019-03-24 18:51:38	\N
8	habeas_corpus01.xlsx	A	spreadsheet	organizations/1/habeas_corpus01.xlsx	http://localhost:8000/files/file-manager/serve/private/$2y$10$f1ud9QuojiT3G7T5HveGOVvvWZJoC41Z26VWlQnL7NTijj.kPE.	<a href="http://localhost:8000/files/file-manager/serve/private/$2y$10$f1ud9QuojiT3G7T5HveGOVvvWZJoC41Z26VWlQnL7NTijj.kPE." target="_blank"><i class="fa fa-external-link" aria-hidden="true" style="font-size: 2em;color:#1E448B;"></i></a>	f	$2y$10$f1ud9QuojiT3G7T5HveGOVvvWZJoC41Z26VWlQnL7NTijj.kPE.	fa fa-file-excel-o	<i class="fa fa-file-excel-o" aria-hidden="true" style="font-size: 2em;color:#9FCC23;"></i>	\N	\N	8	\N	\N	1	2019-03-29 11:58:20	2019-03-29 11:58:20	\N
\.


--
-- TOC entry 4468 (class 0 OID 17708)
-- Dependencies: 259
-- Data for Name: OADH_Context; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_Context" (id, department, municipality, neighborhood, organization_id, news_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4460 (class 0 OID 17638)
-- Dependencies: 251
-- Data for Name: OADH_Digital_Media; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_Digital_Media" (id, name, type, abbreviation, parent_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	El Diario de Hoy	M	EDH  	\N	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
2	En Portada	S	\N	1	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
3	Nacional	S	\N	1	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
4	Internacional	S	\N	1	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
5	Opinión y editoriales	S	\N	1	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
6	Negocios	S	\N	1	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
7	Comunidades	S	\N	1	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
8	Trends	S	\N	1	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
9	EDH Deportes	S	\N	1	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
10	La Prensa Gráfica	M	LGP  	\N	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
11	Tema del día	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
12	Nación	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
13	Política	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
14	Judicial	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
15	Social	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
16	Fotográfica	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
17	Mundo	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
18	Opinión	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
19	Economía	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
20	Departamentos	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
21	Zona Oriente	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
22	Zona Occidente	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
23	Zona Paracentral	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
24	Plus	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
25	LGP Deportes	S	\N	10	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
26	Diario Co-latino	M	D-COL	\N	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
27	Nacionales	S	\N	26	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
28	Internacionales	S	\N	26	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
29	Sociedad	S	\N	26	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
30	Suplemento deportivo	S	\N	26	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
31	Opinión	S	\N	26	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
32	Letras y arte cuarto creciente	S	\N	26	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
33	Cartelera	S	\N	26	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
34	Economía	S	\N	26	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
35	Diario El Mundo	M	EM   	\N	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
36	Politica	S	\N	35	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
37	En foco	S	\N	35	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
38	Nacional	S	\N	35	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
39	Opinión	S	\N	35	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
40	Economía	S	\N	35	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
41	Internacional	S	\N	35	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
42	Deportes	S	\N	35	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
43	Entretenimiento	S	\N	35	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
44	El Faro	M	EF   	\N	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
45	Revista Factum	M	RF   	\N	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
46	ContraPunto	M	CP   	\N	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
47	Transparencia Activa	M	TA   	\N	1	2019-02-01 04:35:04	2019-02-01 04:35:04	\N
\.


--
-- TOC entry 4508 (class 0 OID 26243)
-- Dependencies: 299
-- Data for Name: OADH_FREE_Accused_Liberty_Deprivation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Accused_Liberty_Deprivation" (id, year, departament, municipality, profession, gender, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4506 (class 0 OID 26226)
-- Dependencies: 297
-- Data for Name: OADH_FREE_Accused_Liberty_Deprivation_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Accused_Liberty_Deprivation_Temp" (id, year, departament, municipality, profession, gender, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4524 (class 0 OID 26379)
-- Dependencies: 315
-- Data for Name: OADH_FREE_Acute_Diseases; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Acute_Diseases" (id, year, disease, quantity, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4522 (class 0 OID 26362)
-- Dependencies: 313
-- Data for Name: OADH_FREE_Acute_Diseases_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Acute_Diseases_Temp" (id, year, "right", "violated_Fact", sex, age, departament, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4500 (class 0 OID 26175)
-- Dependencies: 291
-- Data for Name: OADH_FREE_Capture_Orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Capture_Orders" (id, departament, municipality, year, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4498 (class 0 OID 26158)
-- Dependencies: 289
-- Data for Name: OADH_FREE_Capture_Orders_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Capture_Orders_Temp" (id, departament, municipality, year, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4528 (class 0 OID 26410)
-- Dependencies: 319
-- Data for Name: OADH_FREE_Chronic_Diseases; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Chronic_Diseases" (id, chronic_disease, sex, age, jail, year, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4526 (class 0 OID 26393)
-- Dependencies: 317
-- Data for Name: OADH_FREE_Chronic_Diseases_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Chronic_Diseases_Temp" (id, chronic_disease, sex, age, jail, year, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4488 (class 0 OID 26079)
-- Dependencies: 279
-- Data for Name: OADH_FREE_Criminal_Cases_Trials; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Criminal_Cases_Trials" (id, year, phase, court, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4486 (class 0 OID 26062)
-- Dependencies: 277
-- Data for Name: OADH_FREE_Criminal_Cases_Trials_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Criminal_Cases_Trials_Temp" (id, year, phase, court, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4520 (class 0 OID 26345)
-- Dependencies: 311
-- Data for Name: OADH_FREE_Deprived_Persons; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Deprived_Persons" (id, year, "right", "violated_Fact", sex, age, departament, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4518 (class 0 OID 26328)
-- Dependencies: 309
-- Data for Name: OADH_FREE_Deprived_Persons_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Deprived_Persons_Temp" (id, year, "right", "violated_Fact", sex, age, departament, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4504 (class 0 OID 26209)
-- Dependencies: 295
-- Data for Name: OADH_FREE_Disappearances_Victims; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Disappearances_Victims" (id, departament, municipality, year, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4502 (class 0 OID 26192)
-- Dependencies: 293
-- Data for Name: OADH_FREE_Disappearances_Victims_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Disappearances_Victims_Temp" (id, departament, municipality, year, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4514 (class 0 OID 26294)
-- Dependencies: 305
-- Data for Name: OADH_FREE_Forced_Disappearance_Temps; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Forced_Disappearance_Temps" (id, year, gender, departament, municipality, profession, crime, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4516 (class 0 OID 26311)
-- Dependencies: 307
-- Data for Name: OADH_FREE_Forced_Disappearances; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Forced_Disappearances" (id, year, gender, departament, municipality, profession, crime, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4480 (class 0 OID 26014)
-- Dependencies: 271
-- Data for Name: OADH_FREE_Habeas_Corpus_Request; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Habeas_Corpus_Request" (id, year, court, status, file_id, organization_id, created_at, updated_at, deleted_at, sex) FROM stdin;
5167	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5168	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5169	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5170	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5171	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5172	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5173	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5174	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5175	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5176	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5177	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5178	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5179	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5180	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5181	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5182	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5183	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5184	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5185	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5186	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5187	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5188	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5189	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	M
5190	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5191	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5192	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5193	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5194	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5195	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5196	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5197	2016	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5198	2016	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5199	2016	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5200	2016	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5201	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5202	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5203	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5204	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5205	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5206	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5207	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5208	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5209	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	M
5210	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5211	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5212	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5213	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5214	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5215	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5216	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	M
5217	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5218	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5219	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5220	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5221	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5222	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5223	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5224	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5225	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5226	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5227	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5228	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5229	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5230	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5231	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5232	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5233	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5234	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5235	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5236	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5237	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5238	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5239	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5240	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5241	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5242	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5243	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5244	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5245	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5246	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5247	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5248	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5249	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5250	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5251	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5252	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5253	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5254	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5255	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5256	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5257	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5258	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5259	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5260	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5261	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5262	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5263	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5264	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5265	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5266	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5267	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5268	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5269	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5270	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5271	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5272	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5273	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5274	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5275	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5276	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5277	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5278	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5279	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5280	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5281	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5282	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5283	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5284	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5285	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5286	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5287	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5288	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5289	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5290	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5291	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5292	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5293	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5294	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5295	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5296	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5297	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5298	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5299	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5300	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5301	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5302	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5303	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5304	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5305	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5306	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5307	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5308	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5309	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5310	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5311	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5312	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5313	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5314	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5315	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5316	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5317	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5318	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5319	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5320	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5321	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5322	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5323	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5324	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5325	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5326	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5327	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5328	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5329	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5330	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5331	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5332	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5333	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5334	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5335	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5336	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5337	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5338	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5339	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5340	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5341	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5342	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5343	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5344	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5345	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5346	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5347	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5348	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5349	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5350	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5351	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5352	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5353	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5354	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5355	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5356	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5357	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5358	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5359	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5360	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5361	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5362	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5363	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5364	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5365	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5366	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5367	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5368	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5369	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5370	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5371	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5372	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5373	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5374	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5375	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5376	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5377	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5378	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5379	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5380	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5381	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5382	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5383	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5384	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5385	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5386	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5387	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5388	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5389	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5390	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5391	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5392	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5393	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5394	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5395	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5396	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5397	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5398	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5399	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5400	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5401	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5402	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5403	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5404	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5405	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5406	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5407	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5408	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5409	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5410	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5411	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5412	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5413	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5414	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5415	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5416	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5417	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5418	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5419	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5420	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5421	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5422	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5423	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5424	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5425	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5426	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5427	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5428	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5429	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5430	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5431	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5432	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5433	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5434	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5435	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5436	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5437	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5438	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5439	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5440	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5441	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5442	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5443	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5444	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5445	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5446	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5447	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5448	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5449	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5450	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5451	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5452	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5453	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5454	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5455	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5456	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5457	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5458	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5459	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5460	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5461	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5462	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5463	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5464	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5465	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5466	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5467	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5468	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5469	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5470	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5471	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5472	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5473	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5474	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5475	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5476	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5477	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5478	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5479	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5480	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5481	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5482	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5483	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5484	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5485	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5486	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5487	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5488	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5489	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5490	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5491	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5492	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5493	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5494	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5495	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5496	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5497	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5498	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5499	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5500	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5501	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5502	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5503	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5504	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5505	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5506	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5507	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5508	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5509	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5510	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5511	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	M
5512	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5513	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5514	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5515	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5516	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5517	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5518	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5519	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5520	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5521	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5522	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5523	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5524	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5525	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5526	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5527	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5528	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5529	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5530	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5531	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5532	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5533	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	M
5534	2017	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5535	2017	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5536	2017	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5537	2017	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	M
5538	2018	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5539	2018	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
5540	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5541	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5542	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5543	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5544	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5545	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5546	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5547	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	M
5548	2014	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5549	2014	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5550	2015	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5551	2015	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5552	2015	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5553	2015	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5554	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5555	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5556	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5557	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5558	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5559	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5560	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5561	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5562	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5563	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5564	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5565	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5566	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5567	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5568	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5569	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5570	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5571	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5572	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5573	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5574	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5575	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5576	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5577	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5578	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5579	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5580	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5581	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5582	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5583	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5584	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5585	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5586	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5587	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5588	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5589	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5590	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5591	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5592	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5593	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
5594	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	M
5595	2013	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
5596	2013	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
5597	2014	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
5598	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5599	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5600	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5601	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5602	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5603	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5604	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5605	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5606	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5607	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5608	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5609	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5610	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5611	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5612	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5613	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5614	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
5615	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
5616	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5617	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5618	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5619	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5620	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5621	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5622	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5623	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5624	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5625	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5626	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5627	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5628	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5629	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5630	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5631	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
5632	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
5633	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
5634	2018	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5635	2018	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
5636	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5637	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5638	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5639	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5640	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5641	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5642	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5643	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5644	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5645	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5646	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5647	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5648	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5649	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5650	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5651	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5652	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5653	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5654	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5655	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5656	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5657	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5658	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5659	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5660	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5661	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5662	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5663	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5664	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5665	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5666	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5667	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5668	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5669	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5670	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5671	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5672	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5673	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5674	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5675	2016	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5676	2016	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5677	2016	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5678	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5679	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5680	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5681	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5682	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5683	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5684	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5685	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5686	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5687	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5688	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5689	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5690	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5691	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5692	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5693	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5694	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5695	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5696	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5697	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5698	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5699	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5700	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5701	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5702	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5703	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5704	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5705	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5706	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5707	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5708	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5709	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5710	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5711	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5712	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5713	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5714	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5715	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5716	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5717	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5718	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5719	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5720	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5721	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5722	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5723	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5724	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5725	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5726	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5727	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5728	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5729	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5730	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5731	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5732	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5733	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5734	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5735	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5736	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5737	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5738	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5739	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5740	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5741	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5742	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5743	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5744	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5745	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5746	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5747	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5748	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5749	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5750	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5751	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5752	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5753	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5754	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5755	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5756	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5757	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5758	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5759	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5760	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5761	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5762	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5763	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5764	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5765	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5766	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5767	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5768	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5769	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5770	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5771	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5772	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5773	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5774	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5775	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5776	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5777	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5778	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5779	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5780	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5781	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5782	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5783	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5784	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5785	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5786	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5787	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5788	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5789	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5790	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5791	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5792	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5793	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5794	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5795	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5796	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5797	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5798	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5799	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
5800	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5801	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5802	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5803	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5804	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5805	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5806	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5807	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5808	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5809	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5810	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5811	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5812	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5813	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5814	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5815	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5816	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5817	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5818	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5819	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
5820	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5821	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5822	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5823	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5824	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5825	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5826	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5827	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5828	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5829	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
5830	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5831	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5832	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5833	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5834	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5835	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
5836	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
5837	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
5838	2013	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	\N	8	1	\N	\N	\N	H
5839	2013	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	\N	8	1	\N	\N	\N	H
5840	2013	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	\N	8	1	\N	\N	\N	H
5841	2013	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	\N	8	1	\N	\N	\N	H
5842	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5843	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5844	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5845	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5846	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5847	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5848	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5849	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5850	2013	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5851	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5852	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5853	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5854	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5855	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5856	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5857	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5858	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5859	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5860	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5861	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5862	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5863	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5864	2014	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	M
5865	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5866	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5867	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5868	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5869	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5870	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5871	2015	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5872	2016	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5873	2016	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5874	2016	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5875	2016	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5876	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5877	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5878	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5879	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5880	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5881	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5882	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5883	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5884	2017	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	M
5885	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5886	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5887	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5888	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5889	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5890	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	H
5891	2018	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	\N	8	1	\N	\N	\N	M
5892	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5893	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5894	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5895	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5896	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5897	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5898	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5899	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5900	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5901	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5902	2013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5903	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5904	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5905	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5906	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5907	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5908	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5909	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5910	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5911	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5912	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5913	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5914	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5915	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5916	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5917	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5918	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5919	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5920	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5921	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5922	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5923	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5924	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5925	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5926	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5927	2014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5928	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5929	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5930	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5931	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5932	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5933	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5934	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5935	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5936	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5937	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5938	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5939	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5940	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5941	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5942	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5943	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5944	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5945	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5946	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5947	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5948	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5949	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5950	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5951	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5952	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5953	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5954	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5955	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5956	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5957	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5958	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5959	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5960	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5961	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5962	2015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
5963	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5964	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5965	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5966	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5967	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5968	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5969	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5970	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5971	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5972	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5973	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5974	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5975	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5976	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5977	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5978	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5979	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5980	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5981	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5982	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5983	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5984	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5985	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5986	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5987	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5988	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5989	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5990	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5991	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5992	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5993	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5994	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5995	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5996	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5997	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5998	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
5999	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6000	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6001	2016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6002	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6003	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6004	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6005	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6006	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6007	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6008	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6009	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6010	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6011	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6012	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6013	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6014	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6015	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6016	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6017	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6018	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6019	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6020	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6021	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6022	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6023	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6024	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6025	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6026	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6027	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6028	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6029	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6030	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6031	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6032	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6033	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6034	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6035	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6036	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6037	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6038	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6039	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6040	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6041	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6042	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6043	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6044	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6045	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6046	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6047	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6048	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6049	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6050	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6051	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6052	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6053	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6054	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6055	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6056	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6057	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6058	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6059	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6060	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6061	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6062	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6063	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6064	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6065	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6066	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6067	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6068	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6069	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6070	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6071	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6072	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6073	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6074	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6075	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6076	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6077	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6078	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6079	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6080	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6081	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6082	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6083	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6084	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6085	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6086	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6087	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6088	2017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6089	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6090	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6091	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6092	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6093	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6094	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6095	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6096	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6097	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6098	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6099	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6100	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6101	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6102	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6103	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6104	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6105	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6106	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6107	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6108	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6109	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6110	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6111	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6112	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6113	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6114	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6115	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6116	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6117	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6118	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6119	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6120	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6121	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6122	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6123	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6124	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6125	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6126	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6127	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6128	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6129	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6130	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6131	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6132	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6133	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6134	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6135	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6136	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6137	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6138	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6139	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6140	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6141	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6142	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6143	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6144	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6145	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6146	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6147	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6148	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6149	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6150	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6151	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6152	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6153	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6154	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6155	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6156	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6157	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6158	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6159	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6160	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6161	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6162	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6163	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6164	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6165	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6166	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6167	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6168	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6169	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6170	2018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6171	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6172	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6173	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6174	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6175	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6176	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6177	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6178	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6179	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6180	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6181	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6182	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6183	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6184	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6185	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6186	2013	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	M
6187	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6188	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6189	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6190	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6191	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6192	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6193	2014	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6194	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6195	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6196	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6197	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6198	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6199	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6200	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6201	2015	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6202	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6203	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6204	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6205	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6206	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6207	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6208	2016	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	M
6209	2017	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6210	2017	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6211	2017	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6212	2017	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	M
6213	2018	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6214	2018	Cámara de la Segunda Sección del Centro de Cojutepeque	\N	8	1	\N	\N	\N	H
6215	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6216	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6217	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6218	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6219	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6220	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6221	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6222	2013	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	M
6223	2014	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6224	2014	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6225	2015	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6226	2015	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6227	2015	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6228	2015	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6229	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6230	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6231	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6232	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6233	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6234	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6235	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6236	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6237	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6238	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6239	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6240	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6241	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6242	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6243	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6244	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6245	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6246	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6247	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6248	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6249	2016	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6250	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6251	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6252	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6253	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6254	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6255	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6256	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6257	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6258	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6259	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6260	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6261	2017	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6262	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6263	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6264	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6265	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6266	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6267	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6268	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	H
6269	2018	Cámara de la Tercera Sección de Occidente de Ahuachapán	\N	8	1	\N	\N	\N	M
6270	2013	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
6271	2013	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
6272	2014	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
6273	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6274	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6275	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6276	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6277	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6278	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6279	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6280	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6281	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6282	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6283	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6284	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6285	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6286	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6287	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6288	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6289	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
6290	2015	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
6291	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6292	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6293	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6294	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6295	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6296	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6297	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6298	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6299	2016	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6300	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6301	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6302	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6303	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6304	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6305	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6306	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
6307	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
6308	2017	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	M
6309	2018	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6310	2018	Cámara de la Segunda Sección de Oriente de Usulután	\N	8	1	\N	\N	\N	H
6311	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6312	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6313	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6314	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6315	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6316	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6317	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6318	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6319	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6320	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6321	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6322	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6323	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6324	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6325	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6326	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6327	2013	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6328	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6329	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6330	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6331	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6332	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6333	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6334	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6335	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6336	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6337	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6338	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6339	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6340	2014	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	M
6341	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6342	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6343	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6344	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6345	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6346	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6347	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6348	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6349	2015	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6350	2016	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6351	2016	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6352	2016	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6353	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6354	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6355	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6356	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6357	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6358	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6359	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6360	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6361	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6362	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6363	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6364	2017	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6365	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6366	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6367	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6368	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6369	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6370	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6371	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6372	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6373	2018	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	\N	8	1	\N	\N	\N	H
6374	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6375	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6376	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6377	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6378	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6379	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6380	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6381	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6382	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6383	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6384	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6385	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6386	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6387	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6388	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6389	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6390	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6391	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6392	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6393	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6394	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6395	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6396	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6397	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6398	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6399	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6400	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6401	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6402	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6403	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6404	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6405	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6406	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6407	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6408	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6409	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6410	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6411	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6412	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6413	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6414	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6415	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6416	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6417	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6418	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6419	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6420	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6421	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6422	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6423	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6424	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6425	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6426	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6427	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6428	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6429	2013	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6430	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6431	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6432	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6433	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6434	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6435	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6436	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6437	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6438	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6439	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6440	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6441	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6442	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6443	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6444	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6445	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6446	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6447	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6448	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6449	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6450	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6451	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6452	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6453	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6454	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6455	2014	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6456	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6457	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6458	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6459	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6460	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6461	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6462	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6463	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6464	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6465	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6466	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6467	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6468	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6469	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6470	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6471	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6472	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6473	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6474	2015	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
6475	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6476	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6477	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6478	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6479	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6480	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6481	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6482	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6483	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6484	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6485	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6486	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6487	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6488	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6489	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6490	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6491	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6492	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6493	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6494	2016	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
6495	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6496	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6497	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6498	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6499	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6500	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6501	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6502	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6503	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6504	2017	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
6505	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6506	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6507	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6508	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6509	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6510	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	H
6511	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
6512	2018	Cámara de la Tercera Sección del Centro de San Vicente	\N	8	1	\N	\N	\N	M
6513	2013	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	\N	8	1	\N	\N	\N	H
6514	2013	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	\N	8	1	\N	\N	\N	H
6515	2013	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	\N	8	1	\N	\N	\N	H
6516	2013	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	\N	8	1	\N	\N	\N	H
\.


--
-- TOC entry 4478 (class 0 OID 26000)
-- Dependencies: 269
-- Data for Name: OADH_FREE_Habeas_Corpus_Request_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Habeas_Corpus_Request_Temp" (id, court, year, status, file_id, organization_id, created_at, updated_at, deleted_at, sex) FROM stdin;
2701	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2702	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2703	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2704	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2705	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2706	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2707	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2708	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2709	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2013	T	8	1	\N	\N	\N	H
2710	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2711	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2712	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2713	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2714	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2715	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2716	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2717	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2718	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2719	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2720	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2721	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2722	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	H
2723	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2014	T	8	1	\N	\N	\N	M
2724	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2015	T	8	1	\N	\N	\N	H
2725	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2015	T	8	1	\N	\N	\N	H
2726	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2015	T	8	1	\N	\N	\N	H
2727	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2015	T	8	1	\N	\N	\N	H
2728	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2015	T	8	1	\N	\N	\N	H
2729	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2015	T	8	1	\N	\N	\N	H
2730	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2015	T	8	1	\N	\N	\N	H
2731	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2016	T	8	1	\N	\N	\N	H
2732	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2016	T	8	1	\N	\N	\N	H
2733	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2016	T	8	1	\N	\N	\N	H
2734	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2016	T	8	1	\N	\N	\N	H
2735	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	H
2736	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	H
2737	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	H
2738	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	H
2739	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	H
2740	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	H
2741	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	H
2742	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	H
2743	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2017	T	8	1	\N	\N	\N	M
2744	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2018	T	8	1	\N	\N	\N	H
2745	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2018	T	8	1	\N	\N	\N	H
2746	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2018	T	8	1	\N	\N	\N	H
2747	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2018	T	8	1	\N	\N	\N	H
2748	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2018	T	8	1	\N	\N	\N	H
2749	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2018	T	8	1	\N	\N	\N	H
2750	Cámara de lo Penal  de la Cuarta Sección Centro de Santa Tecla	2018	T	8	1	\N	\N	\N	M
2751	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2752	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2753	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2754	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2755	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2756	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2757	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2758	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2759	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
2760	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	M
2761	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	M
2762	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2763	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2764	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2765	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2766	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2767	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2768	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2769	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2770	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2771	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2772	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2773	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2774	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2775	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2776	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2777	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2778	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2779	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2780	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2781	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2782	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
2783	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	M
2784	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	M
2785	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	M
2786	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	M
2787	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2788	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2789	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2790	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2791	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2792	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2793	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2794	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2795	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2796	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2797	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2798	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2799	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2800	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2801	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2802	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2803	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2804	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2805	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2806	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2807	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2808	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2809	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2810	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2811	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2812	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2813	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2814	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2815	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2816	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2817	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2818	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2819	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
2820	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	M
2821	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	M
2822	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2823	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2824	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2825	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2826	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2827	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2828	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2829	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2830	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2831	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2832	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2833	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2834	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2835	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2836	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2837	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2838	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2839	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2840	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2841	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2842	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2843	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2844	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2845	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2846	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2847	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2848	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2849	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2850	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2851	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2852	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2853	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2854	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2855	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2856	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2857	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
2858	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	M
2859	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	M
2860	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	M
2861	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2862	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2863	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2864	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2865	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2866	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2867	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2868	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2869	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2870	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2871	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2872	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2873	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2874	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2875	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2876	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2877	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2878	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2879	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2880	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2881	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2882	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2883	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2884	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2885	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2886	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2887	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2888	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2889	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2890	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2891	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2892	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2893	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2894	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2895	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2896	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2897	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2898	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2899	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2900	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2901	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2902	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2903	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2904	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2905	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2906	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2907	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2908	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2909	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2910	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2911	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2912	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2913	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2914	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2915	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2916	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2917	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2918	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2919	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2920	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2921	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2922	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2923	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2924	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2925	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2926	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2927	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2928	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2929	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2930	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2931	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2932	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2933	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2934	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2935	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2936	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2937	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2938	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2939	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2940	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
2941	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	M
2942	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	M
2943	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	M
2944	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	M
2945	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	M
2946	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	M
2947	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	M
2948	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2949	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2950	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2951	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2952	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2953	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2954	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2955	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2956	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2957	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2958	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2959	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2960	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2961	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2962	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2963	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2964	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2965	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2966	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2967	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2968	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2969	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2970	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2971	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2972	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2973	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2974	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2975	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2976	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2977	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2978	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2979	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2980	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2981	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2982	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2983	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2984	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2985	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2986	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2987	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2988	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2989	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2990	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2991	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2992	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2993	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2994	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2995	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2996	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2997	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2998	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
2999	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3000	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3001	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3002	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3003	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3004	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3005	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3006	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3007	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3008	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3009	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3010	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3011	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3012	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3013	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3014	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3015	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3016	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3017	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3018	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3019	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3020	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3021	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3022	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3023	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3024	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3025	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3026	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3027	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3028	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3029	Cámara de lo Penal de la Primera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	M
3030	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3031	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3032	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3033	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3034	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3035	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3036	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3037	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3038	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3039	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3040	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3041	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3042	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3043	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3044	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	H
3045	Cámara de la Segunda Sección del Centro de Cojutepeque	2013	T	8	1	\N	\N	\N	M
3046	Cámara de la Segunda Sección del Centro de Cojutepeque	2014	T	8	1	\N	\N	\N	H
3047	Cámara de la Segunda Sección del Centro de Cojutepeque	2014	T	8	1	\N	\N	\N	H
3048	Cámara de la Segunda Sección del Centro de Cojutepeque	2014	T	8	1	\N	\N	\N	H
3049	Cámara de la Segunda Sección del Centro de Cojutepeque	2014	T	8	1	\N	\N	\N	H
3050	Cámara de la Segunda Sección del Centro de Cojutepeque	2014	T	8	1	\N	\N	\N	H
3051	Cámara de la Segunda Sección del Centro de Cojutepeque	2014	T	8	1	\N	\N	\N	H
3052	Cámara de la Segunda Sección del Centro de Cojutepeque	2014	T	8	1	\N	\N	\N	H
3053	Cámara de la Segunda Sección del Centro de Cojutepeque	2015	T	8	1	\N	\N	\N	H
3054	Cámara de la Segunda Sección del Centro de Cojutepeque	2015	T	8	1	\N	\N	\N	H
3055	Cámara de la Segunda Sección del Centro de Cojutepeque	2015	T	8	1	\N	\N	\N	H
3056	Cámara de la Segunda Sección del Centro de Cojutepeque	2015	T	8	1	\N	\N	\N	H
3057	Cámara de la Segunda Sección del Centro de Cojutepeque	2015	T	8	1	\N	\N	\N	H
3058	Cámara de la Segunda Sección del Centro de Cojutepeque	2015	T	8	1	\N	\N	\N	H
3059	Cámara de la Segunda Sección del Centro de Cojutepeque	2015	T	8	1	\N	\N	\N	H
3060	Cámara de la Segunda Sección del Centro de Cojutepeque	2015	T	8	1	\N	\N	\N	H
3061	Cámara de la Segunda Sección del Centro de Cojutepeque	2016	T	8	1	\N	\N	\N	H
3062	Cámara de la Segunda Sección del Centro de Cojutepeque	2016	T	8	1	\N	\N	\N	H
3063	Cámara de la Segunda Sección del Centro de Cojutepeque	2016	T	8	1	\N	\N	\N	H
3064	Cámara de la Segunda Sección del Centro de Cojutepeque	2016	T	8	1	\N	\N	\N	H
3065	Cámara de la Segunda Sección del Centro de Cojutepeque	2016	T	8	1	\N	\N	\N	H
3066	Cámara de la Segunda Sección del Centro de Cojutepeque	2016	T	8	1	\N	\N	\N	H
3067	Cámara de la Segunda Sección del Centro de Cojutepeque	2016	T	8	1	\N	\N	\N	M
3068	Cámara de la Segunda Sección del Centro de Cojutepeque	2017	T	8	1	\N	\N	\N	H
3069	Cámara de la Segunda Sección del Centro de Cojutepeque	2017	T	8	1	\N	\N	\N	H
3070	Cámara de la Segunda Sección del Centro de Cojutepeque	2017	T	8	1	\N	\N	\N	H
3071	Cámara de la Segunda Sección del Centro de Cojutepeque	2017	T	8	1	\N	\N	\N	M
3072	Cámara de la Segunda Sección del Centro de Cojutepeque	2018	T	8	1	\N	\N	\N	H
3073	Cámara de la Segunda Sección del Centro de Cojutepeque	2018	T	8	1	\N	\N	\N	H
3074	Cámara de la Tercera Sección de Occidente de Ahuachapán	2013	T	8	1	\N	\N	\N	H
3075	Cámara de la Tercera Sección de Occidente de Ahuachapán	2013	T	8	1	\N	\N	\N	H
3076	Cámara de la Tercera Sección de Occidente de Ahuachapán	2013	T	8	1	\N	\N	\N	H
3077	Cámara de la Tercera Sección de Occidente de Ahuachapán	2013	T	8	1	\N	\N	\N	H
3078	Cámara de la Tercera Sección de Occidente de Ahuachapán	2013	T	8	1	\N	\N	\N	H
3079	Cámara de la Tercera Sección de Occidente de Ahuachapán	2013	T	8	1	\N	\N	\N	H
3080	Cámara de la Tercera Sección de Occidente de Ahuachapán	2013	T	8	1	\N	\N	\N	H
3081	Cámara de la Tercera Sección de Occidente de Ahuachapán	2013	T	8	1	\N	\N	\N	M
3082	Cámara de la Tercera Sección de Occidente de Ahuachapán	2014	T	8	1	\N	\N	\N	H
3083	Cámara de la Tercera Sección de Occidente de Ahuachapán	2014	T	8	1	\N	\N	\N	H
3084	Cámara de la Tercera Sección de Occidente de Ahuachapán	2015	T	8	1	\N	\N	\N	H
3085	Cámara de la Tercera Sección de Occidente de Ahuachapán	2015	T	8	1	\N	\N	\N	H
3086	Cámara de la Tercera Sección de Occidente de Ahuachapán	2015	T	8	1	\N	\N	\N	H
3087	Cámara de la Tercera Sección de Occidente de Ahuachapán	2015	T	8	1	\N	\N	\N	H
3088	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3089	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3090	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3091	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3092	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3093	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3094	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3095	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3096	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3097	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3098	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3099	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3100	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3101	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3102	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3103	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3104	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3105	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3106	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3107	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3108	Cámara de la Tercera Sección de Occidente de Ahuachapán	2016	T	8	1	\N	\N	\N	H
3109	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3110	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3111	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3112	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3113	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3114	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3115	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3116	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3117	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3118	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3119	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3120	Cámara de la Tercera Sección de Occidente de Ahuachapán	2017	T	8	1	\N	\N	\N	H
3121	Cámara de la Tercera Sección de Occidente de Ahuachapán	2018	T	8	1	\N	\N	\N	H
3122	Cámara de la Tercera Sección de Occidente de Ahuachapán	2018	T	8	1	\N	\N	\N	H
3123	Cámara de la Tercera Sección de Occidente de Ahuachapán	2018	T	8	1	\N	\N	\N	H
3124	Cámara de la Tercera Sección de Occidente de Ahuachapán	2018	T	8	1	\N	\N	\N	H
3125	Cámara de la Tercera Sección de Occidente de Ahuachapán	2018	T	8	1	\N	\N	\N	H
3126	Cámara de la Tercera Sección de Occidente de Ahuachapán	2018	T	8	1	\N	\N	\N	H
3127	Cámara de la Tercera Sección de Occidente de Ahuachapán	2018	T	8	1	\N	\N	\N	H
3128	Cámara de la Tercera Sección de Occidente de Ahuachapán	2018	T	8	1	\N	\N	\N	M
3129	Cámara de la Segunda Sección de Oriente de Usulután	2013	T	8	1	\N	\N	\N	M
3130	Cámara de la Segunda Sección de Oriente de Usulután	2013	T	8	1	\N	\N	\N	M
3131	Cámara de la Segunda Sección de Oriente de Usulután	2014	T	8	1	\N	\N	\N	M
3132	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3133	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3134	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3135	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3136	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3137	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3138	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3139	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3140	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3141	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3142	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3143	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3144	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3145	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3146	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3147	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	H
3148	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	M
3149	Cámara de la Segunda Sección de Oriente de Usulután	2015	T	8	1	\N	\N	\N	M
3150	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3151	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3152	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3153	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3154	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3155	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3156	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3157	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3158	Cámara de la Segunda Sección de Oriente de Usulután	2016	T	8	1	\N	\N	\N	H
3159	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	H
3160	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	H
3161	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	H
3162	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	H
3163	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	H
3164	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	H
3165	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	M
3166	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	M
3167	Cámara de la Segunda Sección de Oriente de Usulután	2017	T	8	1	\N	\N	\N	M
3168	Cámara de la Segunda Sección de Oriente de Usulután	2018	T	8	1	\N	\N	\N	H
3169	Cámara de la Segunda Sección de Oriente de Usulután	2018	T	8	1	\N	\N	\N	H
3170	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3171	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3172	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3173	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3174	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3175	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3176	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3177	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3178	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3179	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3180	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3181	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3182	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	H
3183	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	M
3184	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	M
3185	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	M
3186	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2013	T	8	1	\N	\N	\N	M
3187	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3188	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3189	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3190	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3191	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3192	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3193	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3194	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3195	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3196	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3197	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3198	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	H
3199	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2014	T	8	1	\N	\N	\N	M
3200	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3201	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3202	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3203	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3204	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3205	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3206	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3207	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3208	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2015	T	8	1	\N	\N	\N	H
3209	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
3210	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
3211	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2016	T	8	1	\N	\N	\N	H
3212	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3213	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3214	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3215	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3216	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3217	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3218	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3219	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3220	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3221	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3222	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3223	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2017	T	8	1	\N	\N	\N	H
3224	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3225	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3226	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3227	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3228	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3229	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3230	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3231	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3232	Cámara de Segunda Instancia de la Tercera Sección de Oriente de San Miguel	2018	T	8	1	\N	\N	\N	H
3233	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3234	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3235	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3236	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3237	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3238	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3239	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3240	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3241	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3242	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3243	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3244	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3245	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3246	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3247	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3248	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3249	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3250	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3251	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3252	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3253	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3254	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3255	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3256	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3257	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3258	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3259	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3260	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3261	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3262	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3263	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3264	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3265	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3266	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3267	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3268	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3269	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3270	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3271	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3272	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3273	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3274	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3275	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3276	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3277	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3278	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3279	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3280	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3281	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3282	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3283	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3284	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3285	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3286	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3287	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3288	Cámara de la Tercera Sección del Centro de San Vicente	2013	T	8	1	\N	\N	\N	H
3289	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3290	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3291	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3292	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3293	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3294	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3295	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3296	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3297	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3298	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3299	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3300	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3301	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3302	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3303	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3304	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3305	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3306	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3307	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3308	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3309	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3310	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3311	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3312	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3313	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3314	Cámara de la Tercera Sección del Centro de San Vicente	2014	T	8	1	\N	\N	\N	H
3315	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3316	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3317	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3318	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3319	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3320	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3321	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3322	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3323	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3324	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3325	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3326	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3327	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3328	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3329	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3330	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3331	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3332	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	H
3333	Cámara de la Tercera Sección del Centro de San Vicente	2015	T	8	1	\N	\N	\N	M
3334	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3335	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3336	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3337	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3338	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3339	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3340	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3341	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3342	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3343	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3344	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3345	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3346	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3347	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3348	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3349	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3350	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3351	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3352	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	H
3353	Cámara de la Tercera Sección del Centro de San Vicente	2016	T	8	1	\N	\N	\N	M
3354	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3355	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3356	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3357	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3358	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3359	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3360	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3361	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3362	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	H
3363	Cámara de la Tercera Sección del Centro de San Vicente	2017	T	8	1	\N	\N	\N	M
3364	Cámara de la Tercera Sección del Centro de San Vicente	2018	T	8	1	\N	\N	\N	H
3365	Cámara de la Tercera Sección del Centro de San Vicente	2018	T	8	1	\N	\N	\N	H
3366	Cámara de la Tercera Sección del Centro de San Vicente	2018	T	8	1	\N	\N	\N	H
3367	Cámara de la Tercera Sección del Centro de San Vicente	2018	T	8	1	\N	\N	\N	H
3368	Cámara de la Tercera Sección del Centro de San Vicente	2018	T	8	1	\N	\N	\N	H
3369	Cámara de la Tercera Sección del Centro de San Vicente	2018	T	8	1	\N	\N	\N	H
3370	Cámara de la Tercera Sección del Centro de San Vicente	2018	T	8	1	\N	\N	\N	M
3371	Cámara de la Tercera Sección del Centro de San Vicente	2018	T	8	1	\N	\N	\N	M
3372	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	2013	T	8	1	\N	\N	\N	H
3373	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	2013	T	8	1	\N	\N	\N	H
3374	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	2013	T	8	1	\N	\N	\N	H
3375	Cámara de lo Penal de la Primera Sección de Occidente de Santa Ana	2013	T	8	1	\N	\N	\N	H
\.


--
-- TOC entry 4532 (class 0 OID 26444)
-- Dependencies: 323
-- Data for Name: OADH_FREE_Jails_Occupation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Jails_Occupation" (id, departament, station, unit, year, capacity, man_population, women_population, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4530 (class 0 OID 26427)
-- Dependencies: 321
-- Data for Name: OADH_FREE_Jails_Occupation_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Jails_Occupation_Temp" (id, departament, station, unit, year, capacity, man_population, women_population, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4492 (class 0 OID 26110)
-- Dependencies: 283
-- Data for Name: OADH_FREE_Mass_Trials; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Mass_Trials" (id, year, sex, court, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4490 (class 0 OID 26096)
-- Dependencies: 281
-- Data for Name: OADH_FREE_Mass_Trials_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Mass_Trials_Temp" (id, year, sex, court, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4484 (class 0 OID 26045)
-- Dependencies: 275
-- Data for Name: OADH_FREE_People_Detained; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_People_Detained" (id, state, year, departament, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4482 (class 0 OID 26028)
-- Dependencies: 273
-- Data for Name: OADH_FREE_People_Detained_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_People_Detained_Temp" (id, state, year, departament, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4496 (class 0 OID 26141)
-- Dependencies: 287
-- Data for Name: OADH_FREE_Procedural_Fraud; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Procedural_Fraud" (id, gender, crime, year, departament, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4494 (class 0 OID 26124)
-- Dependencies: 285
-- Data for Name: OADH_FREE_Procedural_Fraud_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Procedural_Fraud_Temp" (id, gender, crime, year, departament, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4512 (class 0 OID 26277)
-- Dependencies: 303
-- Data for Name: OADH_FREE_Victims_Liberty_Deprivation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Victims_Liberty_Deprivation" (id, departament, municipality, year, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4510 (class 0 OID 26260)
-- Dependencies: 301
-- Data for Name: OADH_FREE_Victims_Liberty_Deprivation_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_FREE_Victims_Liberty_Deprivation_Temp" (id, departament, municipality, year, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4470 (class 0 OID 17722)
-- Dependencies: 261
-- Data for Name: OADH_Human_Right; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_Human_Right" (id, name, type, parent_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	La efectividad de una vida segura y sin amenazas	\N	\N	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
2	Derecho a la vida	\N	1	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
3	Homicidios	T	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
4	Feminicidios	T	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
5	Crímenes de odio	T	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
6	Ejecuciones sumarias	T	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
7	Torturas	T	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
8	Accidente de tránsito	T	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
9	Tráfico de armas de fuego	T	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
10	Muerte arbitraria o extralegal consumada	H	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
11	Muerte arbitraria o extralegal fallida	H	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
12	Amenazas de muerte	H	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
13	Desapariciones forzadas o involuntarias	H	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
14	Accidentes de tránsito	H	2	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
15	Derecho a la integridad física y moral	\N	1	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
16	Hacinamiento carcelario	T	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
17	Amenazas	T	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
18	Maltrato físico y psicológico 	T	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
19	Programas de reinserción	T	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
20	Tráfico de personas	T	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
21	Uso desproporcionado de la fuerza	T	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
22	Tortura	H	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
23	Tratos o penas crueles, inhumanas o degradantes	H	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
24	Malos tratos	H	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
25	Uso desproporcionado de la fuerza	H	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
26	Trato inhumano a los detenidos	H	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
27	Violencia sexual	H	15	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
28	Derecho a la seguridad física y jurídica	\N	1	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
29	Extorsión	T	28	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
30	Tortura	T	28	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
31	Exclusión	T	28	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
32	Inequidad	T	28	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
33	Acoso por defender derechos	T	28	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
34	Actuaciones de las instituciones del Estado ante la vulnerabilidad de los defensores de derechos humanos	T	28	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
35	Inclusión	T	28	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
36	Derecho a la libertad personal	\N	1	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
37	Detenciones ilegales y arbitrarias	T	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
38	Detenciones masivas	T	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
39	Garantías para un juicio justo	T	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
40	Garantías de los derechos de los detenidos	T	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
41	Detención arbitraria	H	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
42	Detención por falta de policía	H	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
43	Falta de garantías procesales	H	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
44	Detención por hechos no tipificados en la ley	H	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
45	Reclutamiento arbitrario	H	36	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
46	El acceso a la justicia	\N	\N	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
47	Derecho a la justicia pronta y cumplida	\N	46	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
48	Actuaciones del ministerio público y de la PGR	T	47	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
49	Funcionamiento del sistema de justicia	T	47	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
50	Corrupción	T	47	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
51	Denegación de justicia ordinaria	H	47	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
52	Denegación de justicia constitucional (hábeas corpus, amparo, recurso de inconstitucionalidad)	H	47	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
53	Retardación de justicia	H	47	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
54	Derecho a la integridad de la persona detenida	\N	46	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
55	Sistema penitenciario	T	54	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
56	Enfermedades penitenciarias	T	54	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
57	Programas de reinserción a los privados de libertad	T	54	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
58	La existencia de un medio ambiente de calidad	\N	\N	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
59	Derecho al medio ambiente	\N	58	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
60	Conservación y protección del agua	T	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
61	Acceso al agua potable	T	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
62	Tala de árboles	T	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
63	Desechos sólidos 	T	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
64	Minería	T	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
65	Erosión	T	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
66	Contaminación atmosférica	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
67	Contaminación sónica (ruidos y vibraciones)	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
68	Contaminación por gases, humos, partículas, suspendidas y olores	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
69	Contaminación electromagnética	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
70	Contaminación por agroquímicos y plaguicidas	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
71	Contaminación de aguas	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
72	Contaminación aguas superficiales	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
73	Contaminación ríos y arroyos	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
74	Contaminación quebradas	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
75	Contaminación lagos y lagunas	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
76	Contaminación del suelo	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
77	Contaminación del suelo por agroquímicos y pesticidas	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
78	Contaminación del suelo por desechos peligrosos	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
79	Vertidos de desechos sólidos	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
80	Uso no sostenible de recursos naturales	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
81	Modificación del territorio	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
82	Carencia negligente de instrucción, planes y programas encaminadas a la mitigación y reducción de los efectos negativos de desastres	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
83	Transporte, depósitos, almacenamiento, procesamiento y uso de sustancias peligrosas sin tomar las medidas precautorias requeridas	H	59	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
84	El acceso a la educación en los niveles inicial, parvularia, básica y media	\N	\N	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
85	Derecho a la educación	\N	84	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
86	Sistema educativo	T	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
87	Programas de alimentación	T	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
88	Alfabetización 	T	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
89	Deserción escolar	T	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
90	Discriminación educacional	H	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
91	Denegación del derecho a la educación parvularia, básica y especial gratuita	H	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
92	Establecimiento de colegiaturas y pagos obligatorios ilegales	H	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
93	Cierre ilegal o arbitrario de centros de enseñanza e instrucción públicos	H	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
94	Retiro y reducción ilícita de suministros a centros educativos estatales y falta de condiciones mínimas de infraestructura en sus instalaciones	H	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
95	Obstaculización ilegal del proceso de enseñanza- aprendizaje en los centros educativos	H	85	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
96	El acceso al trabajo	\N	\N	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
97	Derecho al trabajo	\N	96	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
98	Condiciones laborales	T	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
99	Empleo	T	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
100	Subempleo	T	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
101		H	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
102	Tolerancia estatal ante despidos ilegales en centros de trabajo públicos y privado	H	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
103	Denegación arbitraria de los derechos o prestaciones laborales reconocidas por la ley	H	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
104	Discriminación laboral	H	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
105	Actos ilegales o arbitrarios atentatorios contra la estabilidad laboral de trabajadores públicos o privados	H	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
106	Inobservancia de las medidas de seguridad e higiene obligatorias en los centros de trabajo	H	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
107	Incumplimiento de los contratos colectivos de trabajo	H	97	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
108	Derecho al salario y las prestaciones sociales	\N	96	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
109	Salario mínimo	T	108	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
110	Escalafón salarial	T	108	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
111	Trabajo en el hogar	T	108	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
112	Derecho a la sindicalización	\N	96	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
113	Irrespeto a fuero sindical	T	112	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
114	Amenazas	T	112	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
115	Maltrato	T	112	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
116	Acoso	T	112	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
117	Denegación o cancelación arbitraria de la persona jurídica, a sindicatos o asociaciones de trabajadores	H	112	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
118	Obstrucción ilegal de la actividad sindical o de asociación laboral	H	112	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
119	Actos ilícitos que afectan los derechos de dirigentes laborales	H	112	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
120	Perturbaciones del derecho de organización y filiación sindical o gremial	H	112	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
121	La experiencia de redes de protección social	\N	\N	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
122	Derecho a la seguridad social	\N	121	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
123	Trabajadoras domésticas	T	122	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
124	Desprotección ilegal o arbitraria en materia de seguridad social	H	122	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
125	Denegación ilícita de los beneficios o prestaciones de seguridad social por parte de instituciones estatales encargadas de la materia	H	122	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
126	Derecho a la jubilación y pensión	\N	121	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
127	 Sistema de pensiones	T	126	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
128	Derecho a la vivienda del propietario y no propietario	\N	121	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
129	 Sistema de pensiones	T	128	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
130	Derecho a la vivienda del propietario y no propietario	\N	121	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
131	 Desalojos	T	130	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
132	Discriminación en la participación activa en el diseño de las políticas nacionales	H	130	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
133	Exclusión o restricción para la obtención de créditos bancarios por motivos de estado familiar	H	130	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
134	Discriminación en la tenencia y propiedad de la tierra y en los procesos de reparto de tierras	H	130	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
135	Discriminación en la prestación de servicios básicos de transporte	H	130	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
136	La cultura del diálogo, equidad, tolerancia e inclusión	\N	\N	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
137	Derecho a la reparación de las víctimas, especialmente de los grupos más vulnerables	\N	136	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
138	Casos de violaciones de derechos humanos durante el conflicto armado	T	137	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
139	Memoria histórica	T	137	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
140	Violaciones de derechos humanos a mujeres	T	137	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
141	Colectivos LGTBI	T	137	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
142	Derecho a la igualdad y trato sin discriminación	\N	136	1	2019-02-01 04:35:11	2019-02-01 04:35:11	\N
\.


--
-- TOC entry 4592 (class 0 OID 26915)
-- Dependencies: 383
-- Data for Name: OADH_INT_Age_Injuries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Age_Injuries" (id, year, sex, age_range, total, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4590 (class 0 OID 26901)
-- Dependencies: 381
-- Data for Name: OADH_INT_Age_Injuries_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Age_Injuries_Temp" (id, year, sex, age_range, total, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4584 (class 0 OID 26850)
-- Dependencies: 375
-- Data for Name: OADH_INT_Age_Sexual; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Age_Sexual" (id, year, crime, age_range, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4582 (class 0 OID 26833)
-- Dependencies: 373
-- Data for Name: OADH_INT_Age_Sexual_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Age_Sexual_Temp" (id, year, crime, age_range, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4556 (class 0 OID 26633)
-- Dependencies: 347
-- Data for Name: OADH_INT_Age_Trafficking; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Age_Trafficking" (id, year, age_range, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4554 (class 0 OID 26619)
-- Dependencies: 345
-- Data for Name: OADH_INT_Age_Trafficking_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Age_Trafficking_Temp" (id, year, age_range, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4564 (class 0 OID 26695)
-- Dependencies: 355
-- Data for Name: OADH_INT_Age_Women; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Age_Women" (id, year, age_range, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4562 (class 0 OID 26681)
-- Dependencies: 353
-- Data for Name: OADH_INT_Age_Women_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Age_Women_Temp" (id, year, age_range, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4600 (class 0 OID 26980)
-- Dependencies: 391
-- Data for Name: OADH_INT_Agent_Investigation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Agent_Investigation" (id, code, crime, category, sex, place, department, municipality, state, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4598 (class 0 OID 26963)
-- Dependencies: 389
-- Data for Name: OADH_INT_Agent_Investigation_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Agent_Investigation_Temp" (id, code, crime, category, sex, place, department, municipality, state, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4540 (class 0 OID 26512)
-- Dependencies: 331
-- Data for Name: OADH_INT_Crimes_Accused; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Crimes_Accused" (id, year, category, department, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4538 (class 0 OID 26495)
-- Dependencies: 329
-- Data for Name: OADH_INT_Crimes_Accused_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Crimes_Accused_Temp" (id, year, category, department, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4536 (class 0 OID 26478)
-- Dependencies: 327
-- Data for Name: OADH_INT_Crimes_Victims; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Crimes_Victims" (id, year, crime, sex, age_range, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4534 (class 0 OID 26461)
-- Dependencies: 325
-- Data for Name: OADH_INT_Crimes_Victims_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Crimes_Victims_Temp" (id, year, crime, sex, age_range, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4580 (class 0 OID 26819)
-- Dependencies: 371
-- Data for Name: OADH_INT_Gender_Sexual; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Gender_Sexual" (id, year, crime, sex, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4578 (class 0 OID 26805)
-- Dependencies: 369
-- Data for Name: OADH_INT_Gender_Sexual_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Gender_Sexual_Temp" (id, year, crime, sex, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4548 (class 0 OID 26577)
-- Dependencies: 339
-- Data for Name: OADH_INT_Gender_Trafficking; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Gender_Trafficking" (id, year, sex, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4546 (class 0 OID 26563)
-- Dependencies: 337
-- Data for Name: OADH_INT_Gender_Trafficking_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Gender_Trafficking_Temp" (id, year, sex, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4604 (class 0 OID 27014)
-- Dependencies: 395
-- Data for Name: OADH_INT_Integrity_Violation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Integrity_Violation" (id, date, sex, age, department, municipality, "right", vilated_fact, institutions, dependencies, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4602 (class 0 OID 26997)
-- Dependencies: 393
-- Data for Name: OADH_INT_Integrity_Violation_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Integrity_Violation_Temp" (id, date, sex, age, department, municipality, "right", vilated_fact, institutions, dependencies, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4560 (class 0 OID 26664)
-- Dependencies: 351
-- Data for Name: OADH_INT_Movement_Freedom; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Movement_Freedom" (id, year, crime, department, municipality, age_range, sex, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4558 (class 0 OID 26647)
-- Dependencies: 349
-- Data for Name: OADH_INT_Movement_Freedom_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Movement_Freedom_Temp" (id, year, crime, department, municipality, age_range, sex, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4596 (class 0 OID 26946)
-- Dependencies: 387
-- Data for Name: OADH_INT_Schedule_Injuries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Schedule_Injuries" (id, year, aggressor_type, "time", total, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4594 (class 0 OID 26929)
-- Dependencies: 385
-- Data for Name: OADH_INT_Schedule_Injuries_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Schedule_Injuries_Temp" (id, year, aggressor_type, "time", total, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4588 (class 0 OID 26884)
-- Dependencies: 379
-- Data for Name: OADH_INT_State_Injuries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_State_Injuries" (id, year, department, municipality, total, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4586 (class 0 OID 26867)
-- Dependencies: 377
-- Data for Name: OADH_INT_State_Injuries_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_State_Injuries_Temp" (id, year, department, municipality, total, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4572 (class 0 OID 26754)
-- Dependencies: 363
-- Data for Name: OADH_INT_State_Women; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_State_Women" (id, year, department, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4570 (class 0 OID 26737)
-- Dependencies: 361
-- Data for Name: OADH_INT_State_Women_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_State_Women_Temp" (id, year, department, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4576 (class 0 OID 26788)
-- Dependencies: 367
-- Data for Name: OADH_INT_Type_Sexual; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Type_Sexual" (id, year, crime, department, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4574 (class 0 OID 26771)
-- Dependencies: 365
-- Data for Name: OADH_INT_Type_Sexual_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Type_Sexual_Temp" (id, year, crime, department, municipality, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4552 (class 0 OID 26605)
-- Dependencies: 343
-- Data for Name: OADH_INT_Type_Trafficking; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Type_Trafficking" (id, crime, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4550 (class 0 OID 26591)
-- Dependencies: 341
-- Data for Name: OADH_INT_Type_Trafficking_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Type_Trafficking_Temp" (id, crime, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4568 (class 0 OID 26723)
-- Dependencies: 359
-- Data for Name: OADH_INT_Type_Women; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Type_Women" (id, year, crime, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4566 (class 0 OID 26709)
-- Dependencies: 357
-- Data for Name: OADH_INT_Type_Women_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Type_Women_Temp" (id, year, crime, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4544 (class 0 OID 26546)
-- Dependencies: 335
-- Data for Name: OADH_INT_Working_Officials; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Working_Officials" (id, year, crime, category, sex, department, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4542 (class 0 OID 26529)
-- Dependencies: 333
-- Data for Name: OADH_INT_Working_Officials_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_INT_Working_Officials_Temp" (id, year, crime, category, sex, department, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4462 (class 0 OID 17652)
-- Dependencies: 253
-- Data for Name: OADH_News; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_News" (id, date, title, code, page, "time", journalistic_genre, summary, new_link, note_locations, twitter_link, facebook_link, screenshot_link, organization_id, digital_media_id, section_id, created_at, updated_at, deleted_at) FROM stdin;
1	2019-02-12	Prueba	EDH120220191	10	10:40:00	Noticia	cddad	dd	Página frontal	\N	\N	\N	1	1	2	2019-02-13 03:42:42	2019-02-13 03:42:42	\N
\.


--
-- TOC entry 4472 (class 0 OID 17739)
-- Dependencies: 263
-- Data for Name: OADH_News_Human_Rights; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_News_Human_Rights" (id, population_affected, qualification, justification, tracing_type_id, right_id, topic_id, violated_fact_id, news_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	\N	0	ddd	1	15	16	22	1	1	2019-02-13 03:43:06	2019-02-13 03:43:25	\N
\.


--
-- TOC entry 4476 (class 0 OID 25981)
-- Dependencies: 267
-- Data for Name: OADH_News_News; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_News_News" (id, related_news_id, news_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4608 (class 0 OID 34187)
-- Dependencies: 399
-- Data for Name: OADH_POP_Age_Population; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_POP_Age_Population" (id, age, year, population, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4606 (class 0 OID 34173)
-- Dependencies: 397
-- Data for Name: OADH_POP_Age_Population_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_POP_Age_Population_Temp" (id, age, year, population, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4612 (class 0 OID 34218)
-- Dependencies: 403
-- Data for Name: OADH_POP_Territory_Population; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_POP_Territory_Population" (id, department, municipality, year, population, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4610 (class 0 OID 34201)
-- Dependencies: 401
-- Data for Name: OADH_POP_Territory_Population_Temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_POP_Territory_Population_Temp" (id, department, municipality, year, population, status, file_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4474 (class 0 OID 17773)
-- Dependencies: 265
-- Data for Name: OADH_People; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_People" (id, type, subtype, first_name, last_name, age, profesion, gender, victimizer_relation, main_victim_relation, victim_relation, organization_id, news_id, created_at, updated_at, deleted_at) FROM stdin;
1	V	P	mario	gallegos	10	Informatico	H	t	f	\N	1	1	2019-02-15 14:28:43	2019-02-15 14:28:43	\N
2	V	P	richard	gallegos	10	industrial	H	f	\N	\N	1	1	2019-02-15 14:29:16	2019-02-15 14:29:16	\N
\.


--
-- TOC entry 4466 (class 0 OID 17691)
-- Dependencies: 257
-- Data for Name: OADH_Producedure; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_Producedure" (id, place, weapon, hypothesis_fact, context, accident_type, summary, organization_id, news_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4458 (class 0 OID 17626)
-- Dependencies: 249
-- Data for Name: OADH_Settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_Settings" (id, population_affected, note_locations, journalistic_genre, is_configured, organization_id, created_at, updated_at, deleted_at, sources, sexual_diversities, guns_types) FROM stdin;
1	Las niñas,Los niños,Los adolescentes,Los jóvenes,Las mujeres,Los hombres,Los adultos,Los adultos mayores,Colectivos LGTBI,Grupos étnicos (indígenas- población afrodescendiente- etc.),Personas con discapacidad,Víctimas del conflicto armado	Página frontal,Página interna,Páginas trasera	Noticia,Reportaje,Crónica,Entrevista,Editorial,Artículo de opinión	\N	1	2019-02-01 04:34:57	2019-02-01 04:34:57	\N	\N	\N	\N
\.


--
-- TOC entry 4464 (class 0 OID 17674)
-- Dependencies: 255
-- Data for Name: OADH_Source; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."OADH_Source" (id, source, organization_id, news_id, created_at, updated_at, deleted_at, instance, responsable) FROM stdin;
\.


--
-- TOC entry 4407 (class 0 OID 16394)
-- Dependencies: 198
-- Data for Name: ORG_Organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."ORG_Organization" (id, name, street1, street2, city_name, state_name, zip_code, web_site, phone_number, fax, email, tax_id, company_registration, commercial_trade, logo_url, cost_price_precision, discount_precision, database_connection_name, api_token, sale_point_quantity, country_id, currency_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	OADH	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2	default	\N	1	202	160	1	2018-12-13 04:00:02	2018-12-13 04:00:02	\N
\.


--
-- TOC entry 4409 (class 0 OID 16405)
-- Dependencies: 200
-- Data for Name: SEC_Failed_Jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Failed_Jobs" (id, connection, queue, payload, failed_at) FROM stdin;
\.


--
-- TOC entry 4411 (class 0 OID 16413)
-- Dependencies: 202
-- Data for Name: SEC_File; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_File" (id, file_id, key, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	7	$2y$10$Bmil3IKMZFdK0DpY1fgFeNwSJeYbGpbGDO9e8AfGIT8SWR3yYquu	1	2019-03-24 18:51:38	2019-03-24 18:51:38	\N
2	8	$2y$10$f1ud9QuojiT3G7T5HveGOVvvWZJoC41Z26VWlQnL7NTijj.kPE.	1	2019-03-29 11:58:20	2019-03-29 11:58:20	\N
3	9	$2y$10$HyKOcTTlnUMMzzZuQGSn0u0wci8AQmILebSnYwYiRKVBBblM.EXa	1	2019-04-02 12:02:17	2019-04-02 12:02:17	\N
4	10	$2y$10$ojjJQ3cJgHQT6N3eyJ5hzu4ye8wnsPqOP6aykO1HAlOL85iYHo0.	1	2019-04-02 12:07:41	2019-04-02 12:07:41	\N
5	11	$2y$10$miIl5DLdyiWzY9tblW9XOD8yqXtg1888rTpuAC05nOFmWhDAd8C	1	2019-04-02 14:28:03	2019-04-02 14:28:03	\N
6	12	$2y$10$v38WuuvjhO70OdSbx9IZ9uP3f4zAZV1TlbZMnopcbqkTVrHNJnNda	1	2019-04-02 17:08:09	2019-04-02 17:08:09	\N
7	13	$2y$10$uFklKSsV0XVErzqcnrF2tOW9nQRMWmas6lKtINkCSC12UztMG	1	2019-04-07 21:32:21	2019-04-07 21:32:21	\N
\.


--
-- TOC entry 4413 (class 0 OID 16418)
-- Dependencies: 204
-- Data for Name: SEC_Journal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Journal" (id, journalized_id, journalized_type, user_id, organization_id, created_at, updated_at, deleted_at) FROM stdin;
1	1	SEC_User	1	\N	2018-12-13 03:30:36	2018-12-13 03:30:36	\N
2	1	SEC_User	1	\N	2018-12-13 03:33:13	2018-12-13 03:33:13	\N
3	1	SEC_User	1	\N	2018-12-13 03:35:55	2018-12-13 03:35:55	\N
4	1	SEC_User	1	\N	2018-12-13 03:37:19	2018-12-13 03:37:19	\N
5	1	ORG_Organization	1	\N	2018-12-13 04:00:04	2018-12-13 04:00:04	\N
6	1	SEC_User	1	1	2018-12-13 04:00:04	2018-12-13 04:00:04	\N
7	1	SEC_User	1	1	2018-12-13 04:39:01	2018-12-13 04:39:01	\N
8	1	SEC_User	1	1	2018-12-13 05:12:56	2018-12-13 05:12:56	\N
9	1	SEC_User	1	1	2018-12-13 05:13:01	2018-12-13 05:13:01	\N
10	1	SEC_User	1	1	2018-12-13 05:15:05	2018-12-13 05:15:05	\N
11	1	SEC_User	1	1	2018-12-13 05:15:15	2018-12-13 05:15:15	\N
12	1	SEC_User	1	1	2018-12-14 02:15:30	2018-12-14 02:15:30	\N
13	1	SEC_User	1	1	2018-12-14 02:15:31	2018-12-14 02:15:31	\N
14	2	OADH_News	1	1	2019-01-13 00:24:14	2019-01-13 00:24:14	\N
15	2	OADH_News	1	1	2019-01-13 00:30:42	2019-01-13 00:30:42	\N
16	2	OADH_News	1	1	2019-01-13 00:39:34	2019-01-13 00:39:34	\N
17	2	OADH_News	1	1	2019-01-13 00:40:21	2019-01-13 00:40:21	\N
18	2	OADH_News	1	1	2019-01-13 00:41:00	2019-01-13 00:41:00	\N
19	2	OADH_News	1	1	2019-01-13 00:41:23	2019-01-13 00:41:23	\N
20	2	OADH_News	1	1	2019-01-13 00:42:42	2019-01-13 00:42:42	\N
21	2	OADH_News	1	1	2019-01-13 00:42:53	2019-01-13 00:42:53	\N
22	1	OADH_News	1	1	2019-01-15 18:51:33	2019-01-15 18:51:33	\N
23	1	OADH_News	1	1	2019-01-16 00:36:27	2019-01-16 00:36:27	\N
24	1	OADH_News	1	1	2019-01-16 00:39:21	2019-01-16 00:39:21	\N
25	1	OADH_News	1	1	2019-01-16 00:39:34	2019-01-16 00:39:34	\N
26	1	OADH_News	1	1	2019-01-16 00:39:44	2019-01-16 00:39:44	\N
27	1	OADH_News	1	1	2019-01-16 00:40:12	2019-01-16 00:40:12	\N
28	1	OADH_News	1	1	2019-01-16 00:40:26	2019-01-16 00:40:26	\N
29	1	OADH_News	1	1	2019-01-16 02:37:23	2019-01-16 02:37:23	\N
30	1	OADH_News	1	1	2019-01-16 02:43:36	2019-01-16 02:43:36	\N
31	1	OADH_News	1	1	2019-01-16 02:47:01	2019-01-16 02:47:01	\N
32	1	OADH_News	1	1	2019-01-16 02:47:55	2019-01-16 02:47:55	\N
33	1	OADH_News	1	1	2019-01-16 02:48:05	2019-01-16 02:48:05	\N
34	1	OADH_News	1	1	2019-01-16 02:51:46	2019-01-16 02:51:46	\N
35	1	OADH_News	1	1	2019-01-16 02:52:42	2019-01-16 02:52:42	\N
36	1	OADH_News	1	1	2019-01-16 02:57:34	2019-01-16 02:57:34	\N
37	1	OADH_News	1	1	2019-01-16 03:00:09	2019-01-16 03:00:09	\N
38	1	OADH_News	1	1	2019-01-16 03:13:48	2019-01-16 03:13:48	\N
39	1	OADH_News	1	1	2019-01-16 03:13:59	2019-01-16 03:13:59	\N
40	1	OADH_News	1	1	2019-01-16 03:14:53	2019-01-16 03:14:53	\N
41	1	OADH_News	1	1	2019-01-16 03:15:00	2019-01-16 03:15:00	\N
42	1	OADH_News	1	1	2019-01-16 03:27:30	2019-01-16 03:27:30	\N
43	1	OADH_News	1	1	2019-01-16 03:30:58	2019-01-16 03:30:58	\N
44	1	OADH_News	1	1	2019-01-16 03:31:07	2019-01-16 03:31:07	\N
45	1	OADH_News	1	1	2019-01-16 03:31:13	2019-01-16 03:31:13	\N
46	1	OADH_News	1	1	2019-01-16 03:31:19	2019-01-16 03:31:19	\N
47	2	OADH_News	1	1	2019-01-16 03:33:33	2019-01-16 03:33:33	\N
48	3	OADH_News	1	1	2019-01-16 03:37:48	2019-01-16 03:37:48	\N
49	4	OADH_News	1	1	2019-01-16 03:43:05	2019-01-16 03:43:05	\N
50	4	OADH_News	1	1	2019-01-16 03:43:17	2019-01-16 03:43:17	\N
51	4	OADH_News	1	1	2019-01-16 03:43:36	2019-01-16 03:43:36	\N
52	4	OADH_News	1	1	2019-01-16 03:43:51	2019-01-16 03:43:51	\N
53	4	OADH_News	1	1	2019-01-16 03:45:50	2019-01-16 03:45:50	\N
54	4	OADH_News	1	1	2019-01-16 03:46:13	2019-01-16 03:46:13	\N
55	4	OADH_News	1	1	2019-01-16 03:46:21	2019-01-16 03:46:21	\N
56	5	OADH_News	1	1	2019-01-16 18:52:26	2019-01-16 18:52:26	\N
57	5	OADH_News	1	1	2019-01-16 18:55:31	2019-01-16 18:55:31	\N
58	5	OADH_News	1	1	2019-01-16 18:56:12	2019-01-16 18:56:12	\N
59	5	OADH_News	1	1	2019-01-16 18:56:29	2019-01-16 18:56:29	\N
60	5	OADH_News	1	1	2019-01-16 18:56:33	2019-01-16 18:56:33	\N
61	5	OADH_News	1	1	2019-01-16 18:56:44	2019-01-16 18:56:44	\N
62	5	OADH_News	1	1	2019-01-16 18:56:54	2019-01-16 18:56:54	\N
63	5	OADH_News	1	1	2019-01-16 18:56:58	2019-01-16 18:56:58	\N
64	1	OADH_News	1	1	2019-02-13 03:42:42	2019-02-13 03:42:42	\N
65	1	OADH_News	1	1	2019-02-13 03:43:06	2019-02-13 03:43:06	\N
66	1	OADH_News	1	1	2019-02-13 03:43:25	2019-02-13 03:43:25	\N
67	1	OADH_News	1	1	2019-02-15 14:28:43	2019-02-15 14:28:43	\N
68	1	OADH_News	1	1	2019-02-15 14:29:16	2019-02-15 14:29:16	\N
69	1	SEC_User	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
70	5	FILE_File	1	1	2019-03-24 18:50:23	2019-03-24 18:50:23	\N
71	7	FILE_File	1	1	2019-03-24 18:51:38	2019-03-24 18:51:38	\N
72	8	FILE_File	1	1	2019-03-29 11:58:20	2019-03-29 11:58:20	\N
73	9	FILE_File	1	1	2019-04-02 12:02:17	2019-04-02 12:02:17	\N
74	9	FILE_File	1	1	2019-04-02 12:06:57	2019-04-02 12:06:57	\N
75	10	FILE_File	1	1	2019-04-02 12:07:41	2019-04-02 12:07:41	\N
76	10	FILE_File	1	1	2019-04-02 14:27:47	2019-04-02 14:27:47	\N
77	11	FILE_File	1	1	2019-04-02 14:28:03	2019-04-02 14:28:03	\N
78	12	FILE_File	1	1	2019-04-02 17:08:09	2019-04-02 17:08:09	\N
79	12	FILE_File	1	1	2019-04-02 17:15:52	2019-04-02 17:15:52	\N
80	11	FILE_File	1	1	2019-04-05 00:20:15	2019-04-05 00:20:15	\N
81	13	FILE_File	1	1	2019-04-07 21:32:21	2019-04-07 21:32:21	\N
82	13	FILE_File	1	1	2019-04-07 21:48:38	2019-04-07 21:48:38	\N
\.


--
-- TOC entry 4414 (class 0 OID 16421)
-- Dependencies: 205
-- Data for Name: SEC_Journal_Detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Journal_Detail" (id, field, field_lang_key, note, old_value, new_value, journal_id, created_at, updated_at, deleted_at) FROM stdin;
1	Correo electrónico	security/user-management.email	\N	root@decimaerp.com	oadh@decimaerp.com	1	2018-12-13 03:30:36	2018-12-13 03:30:36	\N
2	Nombre	security/user-management.firstname	\N	root	oadh	1	2018-12-13 03:30:36	2018-12-13 03:30:36	\N
3	Apellido	security/user-management.lastname	\N	root	oadh	1	2018-12-13 03:30:37	2018-12-13 03:30:37	\N
4	Zona horaria	security/user-management.timezone	\N	\N	America/El_Salvador	1	2018-12-13 03:30:37	2018-12-13 03:30:37	\N
5	Contraseña	security/user-management.password	\N	**********	**********	1	2018-12-13 03:30:37	2018-12-13 03:30:37	\N
6	Correo electrónico	security/user-management.email	\N	oadh@decimaerp.com	root@decimaerp.com	2	2018-12-13 03:33:13	2018-12-13 03:33:13	\N
7	Nombre	security/user-management.firstname	\N	oadh	root	2	2018-12-13 03:33:14	2018-12-13 03:33:14	\N
8	Apellido	security/user-management.lastname	\N	oadh	root	2	2018-12-13 03:33:14	2018-12-13 03:33:14	\N
9	Contraseña	security/user-management.password	\N	**********	**********	2	2018-12-13 03:33:14	2018-12-13 03:33:14	\N
10	Correo electrónico	security/user-management.email	\N	root@decimaerp.com	hello@decimaerp.com	3	2018-12-13 03:35:55	2018-12-13 03:35:55	\N
11	Contraseña	security/user-management.password	\N	**********	**********	3	2018-12-13 03:35:55	2018-12-13 03:35:55	\N
12	Correo electrónico	security/user-management.email	\N	hello@decimaerp.com	oadh@decimaerp.com	4	2018-12-13 03:37:19	2018-12-13 03:37:19	\N
13	Nombre	security/user-management.firstname	\N	root	oadh	4	2018-12-13 03:37:20	2018-12-13 03:37:20	\N
14	Apellido	security/user-management.lastname	\N	root	oadh	4	2018-12-13 03:37:20	2018-12-13 03:37:20	\N
15	Contraseña	security/user-management.password	\N	**********	**********	4	2018-12-13 03:37:20	2018-12-13 03:37:20	\N
16	\N	\N	Se agregó la organización "OADH" en el sistema.	\N	\N	5	2018-12-13 04:00:04	2018-12-13 04:00:04	\N
17	\N	security/user-management.defaultOrganization	\N	\N	OADH	6	2018-12-13 04:00:05	2018-12-13 04:00:05	\N
18	\N	\N	Se asignó la aplicación "Configuración inicial" a oadh@decimaerp.com.	\N	\N	7	2018-12-13 04:39:02	2018-12-13 04:39:02	\N
19	\N	\N	Se asignó la aplicación "Gestión de derechos humanos" a oadh@decimaerp.com.	\N	\N	7	2018-12-13 04:39:02	2018-12-13 04:39:02	\N
20	\N	\N	Se asignó la aplicación "Gestión de medios" a oadh@decimaerp.com.	\N	\N	7	2018-12-13 04:39:03	2018-12-13 04:39:03	\N
21	\N	\N	Se asignó la aplicación "Monitoreo de medios" a oadh@decimaerp.com.	\N	\N	7	2018-12-13 04:39:03	2018-12-13 04:39:03	\N
22	\N	\N	Se desasignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	8	2018-12-13 05:12:56	2018-12-13 05:12:56	\N
23	\N	\N	Se asignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	9	2018-12-13 05:13:02	2018-12-13 05:13:02	\N
24	\N	\N	Se desasignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	10	2018-12-13 05:15:06	2018-12-13 05:15:06	\N
25	\N	\N	Se asignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	11	2018-12-13 05:15:15	2018-12-13 05:15:15	\N
26	\N	\N	Se desasignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	12	2018-12-14 02:15:30	2018-12-14 02:15:30	\N
27	\N	\N	Se asignó la aplicación "Gestión de usuarios" a oadh@decimaerp.com.	\N	\N	13	2018-12-14 02:15:31	2018-12-14 02:15:31	\N
28	\N	\N	Se agregó el registro ":title"	\N	\N	14	2019-01-13 00:24:14	2019-01-13 00:24:14	\N
29	\N	\N	Se agregó el registro ":title"	\N	\N	15	2019-01-13 00:30:42	2019-01-13 00:30:42	\N
30	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	16	2019-01-13 00:39:34	2019-01-13 00:39:34	\N
31	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	17	2019-01-13 00:40:21	2019-01-13 00:40:21	\N
32	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	18	2019-01-13 00:41:00	2019-01-13 00:41:00	\N
33	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	19	2019-01-13 00:41:23	2019-01-13 00:41:23	\N
34	\N	\N	decima-oadh::media-monitoring.addedDetailJournal	\N	\N	20	2019-01-13 00:42:42	2019-01-13 00:42:42	\N
35	Link de noticia en facebook	decima-oadh::media-monitoring.facebookLink	\N	\N	dd	21	2019-01-13 00:42:53	2019-01-13 00:42:53	\N
36	Link de screenshot	decima-oadh::media-monitoring.screenshotLink	\N	\N	dd	21	2019-01-13 00:42:53	2019-01-13 00:42:53	\N
37	\N	\N	Se agregó el registro ":title"	\N	\N	22	2019-01-15 18:51:33	2019-01-15 18:51:33	\N
38	\N	\N	Se agregó el registro ":title"	\N	\N	23	2019-01-16 00:36:27	2019-01-16 00:36:27	\N
39	\N	\N	Se agregó el registro ":title"	\N	\N	24	2019-01-16 00:39:21	2019-01-16 00:39:21	\N
40	\N	\N	Se agregó el registro ":title"	\N	\N	25	2019-01-16 00:39:34	2019-01-16 00:39:34	\N
41	\N	\N	Se agregó el registro ":title"	\N	\N	26	2019-01-16 00:39:45	2019-01-16 00:39:45	\N
42	\N	\N	Se agregó el registro ":title"	\N	\N	27	2019-01-16 00:40:12	2019-01-16 00:40:12	\N
43	\N	\N	Se agregó el registro ":title"	\N	\N	28	2019-01-16 00:40:26	2019-01-16 00:40:26	\N
44	\N	\N	Se agregó el registro ":title"	\N	\N	29	2019-01-16 02:37:23	2019-01-16 02:37:23	\N
45	decima-oadh::media-monitoring.subtype	decima-oadh::media-monitoring.subtype	\N	P	S	30	2019-01-16 02:43:36	2019-01-16 02:43:36	\N
46	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	\N	1	31	2019-01-16 02:47:01	2019-01-16 02:47:01	\N
47	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	1	\N	32	2019-01-16 02:47:55	2019-01-16 02:47:55	\N
48	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	\N	0	32	2019-01-16 02:47:55	2019-01-16 02:47:55	\N
49	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	0	1	33	2019-01-16 02:48:05	2019-01-16 02:48:05	\N
50	Tipo de victima	decima-oadh::media-monitoring.type	\N	V	S	34	2019-01-16 02:51:46	2019-01-16 02:51:46	\N
51	\N	\N	Se agregó el registro ":title"	\N	\N	35	2019-01-16 02:52:42	2019-01-16 02:52:42	\N
52	Tipo de victima	decima-oadh::media-monitoring.type	\N	V	S	36	2019-01-16 02:57:34	2019-01-16 02:57:34	\N
53	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	\N	1	37	2019-01-16 03:00:09	2019-01-16 03:00:09	\N
54	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	1	0	37	2019-01-16 03:00:09	2019-01-16 03:00:09	\N
55	decima-oadh::media-monitoring.subtype	decima-oadh::media-monitoring.subtype	\N	S	P	38	2019-01-16 03:13:48	2019-01-16 03:13:48	\N
56	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	1	0	39	2019-01-16 03:13:59	2019-01-16 03:13:59	\N
57	decima-oadh::media-monitoring.subtype	decima-oadh::media-monitoring.subtype	\N	P	S	40	2019-01-16 03:14:53	2019-01-16 03:14:53	\N
58	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	\N	1	40	2019-01-16 03:14:53	2019-01-16 03:14:53	\N
59	decima-oadh::media-monitoring.mainVictimRelation	decima-oadh::media-monitoring.mainVictimRelation	\N	1	0	41	2019-01-16 03:15:00	2019-01-16 03:15:00	\N
60	\N	\N	Se agregó el registro ":title"	\N	\N	42	2019-01-16 03:27:30	2019-01-16 03:27:30	\N
61	Relación con la victima	decima-oadh::media-monitoring.victimRelation	\N	0	1	43	2019-01-16 03:30:58	2019-01-16 03:30:58	\N
62	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	1	\N	43	2019-01-16 03:30:58	2019-01-16 03:30:58	\N
63	Relación con la victima	decima-oadh::media-monitoring.victimRelation	\N	1	0	44	2019-01-16 03:31:07	2019-01-16 03:31:07	\N
64	decima-oadh::media-monitoring.subtype	decima-oadh::media-monitoring.subtype	\N	P	S	45	2019-01-16 03:31:13	2019-01-16 03:31:13	\N
65	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	\N	1	45	2019-01-16 03:31:13	2019-01-16 03:31:13	\N
66	Relación con victimario	decima-oadh::media-monitoring.victimizerRelation	\N	1	0	46	2019-01-16 03:31:19	2019-01-16 03:31:19	\N
67	\N	\N	Se agregó el registro ":title"	\N	\N	47	2019-01-16 03:33:33	2019-01-16 03:33:33	\N
68	\N	\N	Se agregó el registro ":title"	\N	\N	48	2019-01-16 03:37:48	2019-01-16 03:37:48	\N
69	\N	\N	Se agregó el registro ":title"	\N	\N	49	2019-01-16 03:43:05	2019-01-16 03:43:05	\N
70	\N	\N	Se agregó el registro ":title"	\N	\N	50	2019-01-16 03:43:18	2019-01-16 03:43:18	\N
71	\N	\N	Se agregó el registro ":title"	\N	\N	51	2019-01-16 03:43:36	2019-01-16 03:43:36	\N
72	\N	\N	Se agregó el registro ":title"	\N	\N	52	2019-01-16 03:43:51	2019-01-16 03:43:51	\N
73	\N	\N	Se agregó el registro ":title"	\N	\N	53	2019-01-16 03:45:50	2019-01-16 03:45:50	\N
74	\N	\N	Se agregó el registro ":title"	\N	\N	54	2019-01-16 03:46:13	2019-01-16 03:46:13	\N
75	Link de noticia en facebook	decima-oadh::media-monitoring.facebookLink	\N	\N	ddd	55	2019-01-16 03:46:21	2019-01-16 03:46:21	\N
76	Link de screenshot	decima-oadh::media-monitoring.screenshotLink	\N	\N	dd	55	2019-01-16 03:46:21	2019-01-16 03:46:21	\N
77	\N	\N	Se agregó el registro ":title"	\N	\N	56	2019-01-16 18:52:26	2019-01-16 18:52:26	\N
78	N° de página	decima-oadh::media-monitoring.page	\N	5	1000	57	2019-01-16 18:55:31	2019-01-16 18:55:31	\N
79	\N	\N	Se agregó el registro ":title"	\N	\N	58	2019-01-16 18:56:12	2019-01-16 18:56:12	\N
80	\N	\N	Se agregó el registro ":title"	\N	\N	59	2019-01-16 18:56:29	2019-01-16 18:56:29	\N
81	\N	\N	Se agregó el registro ":title"	\N	\N	60	2019-01-16 18:56:33	2019-01-16 18:56:33	\N
82	\N	\N	Se agregó el registro ":title"	\N	\N	61	2019-01-16 18:56:44	2019-01-16 18:56:44	\N
83	\N	\N	Se agregó el registro ":title"	\N	\N	62	2019-01-16 18:56:54	2019-01-16 18:56:54	\N
84	\N	\N	Se agregó el registro ":title"	\N	\N	63	2019-01-16 18:56:58	2019-01-16 18:56:58	\N
85	\N	\N	Se agregó la noticia "Prueba"	\N	\N	64	2019-02-13 03:42:42	2019-02-13 03:42:42	\N
86	\N	\N	decima-oadh::media-monitoring.addedRightJournal	\N	\N	65	2019-02-13 03:43:06	2019-02-13 03:43:06	\N
87	\N	\N	Se agregó la noticia ":name"	\N	\N	67	2019-02-15 14:28:43	2019-02-15 14:28:43	\N
88	\N	\N	Se agregó la noticia ":name"	\N	\N	68	2019-02-15 14:29:16	2019-02-15 14:29:16	\N
89	\N	\N	Se asignó la aplicación "Solicitudes de habeas corpus" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
90	\N	\N	Se asignó la aplicación "Personas detenidas e internadas provisionalmente" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
91	\N	\N	Se asignó la aplicación "Cantidad de Juicios en Materia Penal" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
92	\N	\N	Se asignó la aplicación "Personas detenidas y procesadas en masa" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
93	\N	\N	Se asignó la aplicación "Imputados por los delitos de fraude procesal y simulación de delitos" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
94	\N	\N	Se asignó la aplicación "Detenciones basadas en órdenes de captura" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
95	\N	\N	Se asignó la aplicación "Víctimas de desaparición" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
96	\N	\N	Se asignó la aplicación "Imputados por el delito de privación de libertad" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
97	\N	\N	Se asignó la aplicación "Víctimas de privación de libertad" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
98	\N	\N	Se asignó la aplicación "Imputados por Desaparición Forzada" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
99	\N	\N	Se asignó la aplicación "Ocupación en bartolinas policiales" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
100	\N	\N	Se asignó la aplicación "Derechos violentados a privados de libertad" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
101	\N	\N	Se asignó la aplicación "Enfermedades agudas más comunes en centros penales" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
102	\N	\N	Se asignó la aplicación "Enfermedades crónicas más comunes en centros penales" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
103	\N	\N	Se asignó la aplicación "Víctimas ingresadas por medio de denuncias ante los delitos de de lesiones, amenazas, disparo de arma de fuego, extorsión y tortura" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
104	\N	\N	Se asignó la aplicación "Imputados de profesión PNC y miembros de FAES ante el delito de lesiones, disparo de arma de fuego, extorsión y tortura" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
105	\N	\N	Se asignó la aplicación "Agentes que continuaron laborando durante fases de investigaciones abiertas" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
106	\N	\N	Se asignó la aplicación "Según sexo - Casos reportados por el delito de trata y tráfico de personas" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
107	\N	\N	Se asignó la aplicación "Según tipo de delito - Casos reportados por el delito de trata y tráfico de personas" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
108	\N	\N	Se asignó la aplicación "Según rango de edad - Casos reportados por el delito de trata y tráfico de personas" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
109	\N	\N	Se asignó la aplicación "Víctimas ingresadas por el delito de limitación ilegal a la libertad de circulación" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
110	\N	\N	Se asignó la aplicación "Según rango de edad - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
111	\N	\N	Se asignó la aplicación "Según tipo de delito - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
112	\N	\N	Se asignó la aplicación "Según depto. y mpio. - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
113	\N	\N	Se asignó la aplicación "Según tipo, depto. y mpio. - Denuncias interpuestas por delitos atentatorios contra la libertad sexual" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
114	\N	\N	Se asignó la aplicación "Según sexo - Denuncias interpuestas por delitos atentatorios contra la libertad sexual" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
115	\N	\N	Se asignó la aplicación "Según rango de edad - Denuncias interpuestas por delitos atentatorios contra la libertad sexual" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
116	\N	\N	Se asignó la aplicación "Según depto. y mpio. - Agresiones registradas por el delito de lesiones" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
117	\N	\N	Se asignó la aplicación "Según sexo y rango de edad - Agresiones registradas por el delito de lesiones" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
118	\N	\N	Se asignó la aplicación "Según rango de horario y tipo de víct. - Agresiones registradas por el delito de lesiones" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
119	\N	\N	Se asignó la aplicación "Investigaciones de agentes PNC por delitos relativo al abuso de la fuerza y uso ilegal de armas de fuego" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
120	\N	\N	Se asignó la aplicación "Denuncias reportadas en la PDDH por la vulneración del derecho a la integridad personal" a oadh@decimaerp.com.	\N	\N	69	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
121	\N	\N	Se agregó la carpeta "Ejemplos " al sistema	\N	\N	70	2019-03-24 18:50:23	2019-03-24 18:50:23	\N
122	\N	\N	Se agregó el archivo "estadocuenta6840010153.xls " al sistema	\N	\N	71	2019-03-24 18:51:38	2019-03-24 18:51:38	\N
123	\N	\N	Se agregó el archivo "habeas_corpus01.xlsx " al sistema	\N	\N	72	2019-03-29 11:58:20	2019-03-29 11:58:20	\N
124	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	73	2019-04-02 12:02:17	2019-04-02 12:02:17	\N
125	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	74	2019-04-02 12:06:57	2019-04-02 12:06:57	\N
126	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	75	2019-04-02 12:07:41	2019-04-02 12:07:41	\N
127	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	76	2019-04-02 14:27:47	2019-04-02 14:27:47	\N
128	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	77	2019-04-02 14:28:03	2019-04-02 14:28:03	\N
129	\N	\N	Se agregó el archivo "habeas_corpus03.xlsx " al sistema	\N	\N	78	2019-04-02 17:08:09	2019-04-02 17:08:09	\N
130	\N	\N	Se eliminó el archivo "habeas_corpus03.xlsx" seleccionado en el sistema.	\N	\N	79	2019-04-02 17:15:52	2019-04-02 17:15:52	\N
131	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	80	2019-04-05 00:20:16	2019-04-05 00:20:16	\N
132	\N	\N	Se agregó el archivo "habeas_corpus02.xlsx " al sistema	\N	\N	81	2019-04-07 21:32:21	2019-04-07 21:32:21	\N
133	\N	\N	Se eliminó el archivo "habeas_corpus02.xlsx" seleccionado en el sistema.	\N	\N	82	2019-04-07 21:48:38	2019-04-07 21:48:38	\N
\.


--
-- TOC entry 4417 (class 0 OID 16431)
-- Dependencies: 208
-- Data for Name: SEC_Menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Menu" (id, name, lang_key, url, action_button_id, action_lang_key, icon, parent_id, created_by, module_id, created_at, updated_at, deleted_at) FROM stdin;
1	Organization	security/menu.organization	\N		\N	fa fa-building-o	\N	1	1	2018-12-13 03:18:43	2018-12-13 03:18:43	\N
2	Organization Management	security/menu.organizationManagment	/general-setup/organization/organization-management	om-btn-close	security/menu.organizationManagmentAction	fa fa-wrench	1	1	1	2018-12-13 03:18:43	2018-12-13 03:18:43	\N
3	Security	security/menu.security	\N		\N	fa fa-lock	\N	1	1	2018-12-13 03:18:43	2018-12-13 03:18:43	\N
4	User Management	security/menu.userManagment	/general-setup/security/user-management	um-btn-close	security/menu.userManagmentAction	fa fa-wrench	3	1	1	2018-12-13 03:18:43	2018-12-13 03:18:43	\N
5	File Manager	decima-file::menu.fileManager	/files/file-manager	fi-fm-btn-close	decima-file::menu.fileManagerAction	fa fa-files-o	\N	1	2	2018-12-13 03:20:27	2018-12-13 03:20:27	\N
6	Setup (OADH)	decima-oadh::menu.setup	\N		\N	fa fa-gear	\N	1	3	2018-12-13 04:38:19	2018-12-13 04:38:19	\N
10	Transaction (OADH)	decima-oadh::menu.transactions	\N		\N	fa fa-exchange	\N	1	3	2018-12-13 04:38:20	2018-12-13 04:38:20	\N
7	Configuración inicial	decima-oadh::menu.initialSetup	/ucaoadh/setup/initial-setup	oadh-is-btn-close	decima-oadh::menu.initialSetupAction	fa fa-gear	6	1	3	2018-12-13 04:38:19	2018-12-13 04:38:19	\N
8	Gestión de derechos humanos	decima-oadh::menu.humanRightManagement	/ucaoadh/setup/human-right-management	oadh-hrm-btn-close	decima-oadh::menu.humanRightManagementAction	fa fa-gear	6	1	3	2018-12-13 04:38:20	2018-12-13 04:38:20	\N
11	Monitoreo de medios	decima-oadh::menu.mediaMonitoring	/ucaoadh/transactions/media-monitoring	oadh-mmo-btn-close	decima-oadh::menu.mediaMonitoringAction	fa fa-eye	10	1	3	2018-12-13 04:38:21	2018-12-13 04:38:21	\N
9	Gestión de medios	decima-oadh::menu.mediaManagement	/ucaoadh/setup/digital-media-management	rh-emp-btn-close	decima-oadh::menu.mediaManagementAction	fa fa-gear	6	1	3	2018-12-13 04:38:20	2018-12-13 04:38:20	\N
12	Derecho a la libertad	decima-oadh::menu.rightToFreedom	\N		\N	fa fa-database	\N	1	3	\N	\N	\N
13	Derecho a la integridad personal	decima-oadh::menu.rightToPersonalIntegrity	\N		\N	fa fa-database	\N	1	3	\N	\N	\N
14	Derecho a la vida	decima-oadh::menu.rightToLife	\N		\N	fa fa-database	\N	1	3	\N	\N	\N
19	Solicitudes de habeas corpus	decima-oadh::menu.habeasCorpusRequest	/ucaoadh/freedom/habeas-corpus-request	oadh-free-hcr-btn-close	decima-oadh::menu.habeasCorpusRequestAction	fa fa-upload	12	1	3	\N	\N	\N
20	Personas detenidas e internadas provisionalmente	decima-oadh::menu.peopleDetained	/ucaoadh/freedom/people-detained	oadh-free-pd-btn-close	decima-oadh::menu.peopleDetainedAction	fa fa-upload	12	1	3	\N	\N	\N
21	Cantidad de Juicios en Materia Penal	decima-oadh::menu.criminalCases	/ucaoadh/freedom/criminal-cases-trials	oadh-free-cct-btn-close	decima-oadh::menu.criminalCasesAction	fa fa-upload	12	1	3	\N	\N	\N
22	Personas detenidas y procesadas en masa	decima-oadh::menu.massTrials	/ucaoadh/freedom/mass-trials	oadh-free-mt-btn-close	decima-oadh::menu.massTrialsAction	fa fa-upload	12	1	3	\N	\N	\N
23	Imputados por los delitos de fraude procesal y simulación de delitos	decima-oadh::menu.proceduralFraud	/ucaoadh/freedom/procedural-fraud	oadh-free-pf-btn-close	decima-oadh::menu.proceduralFraudAction	fa fa-upload	12	1	3	\N	\N	\N
24	Detenciones basadas en órdenes de captura	decima-oadh::menu.captureOrders	/ucaoadh/freedom/capture-orders	oadh-free-co-btn-close	decima-oadh::menu.proceduralFraudAction	fa fa-upload	12	1	3	\N	\N	\N
25	Víctimas de desaparición	decima-oadh::menu.disapparencesVictims	/ucaoadh/freedom/disapparences-victims	oadh-free-dv-btn-close	decima-oadh::menu.disapparencesVictimsAction	fa fa-upload	12	1	3	\N	\N	\N
26	Imputados por el delito de privación de libertad	decima-oadh::menu.accusedlibertyDeprivation	/ucaoadh/freedom/accused-liberty-deprivation	oadh-free-llp-btn-close	decima-oadh::menu.accusedLibertyDeprivationAction	fa fa-upload	12	1	3	\N	\N	\N
27	Víctimas de privación de libertad	decima-oadh::menu.victimLibertyDeprivation	/ucaoadh/freedom/victim-liberty-deprivation	oadh-free-vlp-btn-close	decima-oadh::menu.victimLibertyDeprivationAction	fa fa-upload	12	1	3	\N	\N	\N
28	Imputados por Desaparición Forzada	decima-oadh::menu.forcedDisappearances	/ucaoadh/freedom/forced-disappearances	oadh-free-fd-btn-close	decima-oadh::menu.libertyDeprivationAction	fa fa-upload	12	1	3	\N	\N	\N
29	Ocupación en bartolinas policiales	decima-oadh::menu.jailsOccupation	/ucaoadh/freedom/jails-occupation	oadh-free-fd-btn-close	decima-oadh::menu.jailsOccupationAction	fa fa-upload	12	1	3	\N	\N	\N
30	Derechos violentados a privados de libertad	decima-oadh::menu.deprivedPersons	/ucaoadh/freedom/deprived-persons	oadh-free-dp-btn-close	decima-oadh::menu.deprivedPersonsAction	fa fa-upload	12	1	3	\N	\N	\N
31	Enfermedades agudas más comunes en centros penales	decima-oadh::menu.acuteDiseases	/ucaoadh/freedom/acute-disease	oadh-free-ad-btn-close	decima-oadh::menu.acuteIllnessAction	fa fa-upload	12	1	3	\N	\N	\N
32	Enfermedades crónicas más comunes en centros penales	decima-oadh::menu.chronicDiseases	/ucaoadh/freedom/chronic-diseases	oadh-free-cd-btn-close	decima-oadh::menu.chronicDiseasesAction	fa fa-upload	12	1	3	\N	\N	\N
33	Víctimas ingresadas por medio de denuncias ante los delitos de de lesiones, amenazas, disparo de arma de fuego, extorsión y tortura	decima-oadh::menu.crimesVictims	/ucaoadh/integrity/crimes-victims	oadh-inte-cv-btn-close	decima-oadh::menu.crimesVictimsAction	fa fa-upload	13	1	3	\N	\N	\N
34	Imputados de profesión PNC y miembros de FAES ante el delito de lesiones, disparo de arma de fuego, extorsión y tortura	decima-oadh::menu.crimesAccused	/ucaoadh/integrity/crimes-accused	oadh-inte-ca-btn-close	decima-oadh::menu.crimesAccusedAction	fa fa-upload	13	1	3	\N	\N	\N
35	Agentes que continuaron laborando durante fases de investigaciones abiertas	decima-oadh::menu.workingOfficials	/ucaoadh/integrity/working-officials	oadh-inte-wo-btn-close	decima-oadh::menu.workingOfficialsAction	fa fa-upload	13	1	3	\N	\N	\N
36	Según sexo - Casos reportados por el delito de trata y tráfico de personas	decima-oadh::menu.genderTrafficking	/ucaoadh/integrity/gender-trafficking	oadh-inte-gt-btn-close	decima-oadh::menu.genderTraffickingAction	fa fa-upload	13	1	3	\N	\N	\N
37	Según tipo de delito - Casos reportados por el delito de trata y tráfico de personas	decima-oadh::menu.typeTrafficking	/ucaoadh/integrity/type-trafficking	oadh-inte-tt-btn-close	decima-oadh::menu.typeTraffickingAction	fa fa-upload	13	1	3	\N	\N	\N
38	Según rango de edad - Casos reportados por el delito de trata y tráfico de personas	decima-oadh::menu.ageTrafficking	/ucaoadh/integrity/age-trafficking	oadh-inte-at-btn-close	decima-oadh::menu.ageTraffickingAction	fa fa-upload	13	1	3	\N	\N	\N
39	Víctimas ingresadas por el delito de limitación ilegal a la libertad de circulación	decima-oadh::menu.movementFreedom	/ucaoadh/integrity/movement-freedom	oadh-inte-mf-btn-close	decima-oadh::menu.movementFreedomAction	fa fa-upload	13	1	3	\N	\N	\N
40	Según rango de edad - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres	decima-oadh::menu.ageWomen	/ucaoadh/integrity/age-women	oadh-inte-aw-btn-close	decima-oadh::menu.ageWomenAction	fa fa-upload	13	1	3	\N	\N	\N
41	Según tipo de delito - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres	decima-oadh::menu.typeWomen	/ucaoadh/integrity/type-women	oadh-inte-aw-btn-close	decima-oadh::menu.typeWomenAction	fa fa-upload	13	1	3	\N	\N	\N
42	Según depto. y mpio. - Víctimas ingresadas en la Unidad de Atención Especializada para las mujeres	decima-oadh::menu.StateWomen	/ucaoadh/integrity/state-women	oadh-inte-aw-btn-close	decima-oadh::menu.stateWomenAction	fa fa-upload	13	1	3	\N	\N	\N
43	Según tipo, depto. y mpio. - Denuncias interpuestas por delitos atentatorios contra la libertad sexual	decima-oadh::menu.typeSexual	/ucaoadh/integrity/type-sexual	oadh-inte-ts-btn-close	decima-oadh::menu.typeSexualAction	fa fa-upload	13	1	3	\N	\N	\N
44	Según sexo - Denuncias interpuestas por delitos atentatorios contra la libertad sexual	decima-oadh::menu.genderSexual	/ucaoadh/integrity/gender-sexual	oadh-inte-gs-btn-close	decima-oadh::menu.genderSexualAction	fa fa-upload	13	1	3	\N	\N	\N
45	Según rango de edad - Denuncias interpuestas por delitos atentatorios contra la libertad sexual	decima-oadh::menu.ageSexual	/ucaoadh/integrity/age-sexual	oadh-inte-as-btn-close	decima-oadh::menu.ageSexualAction	fa fa-upload	13	1	3	\N	\N	\N
46	Según depto. y mpio. - Agresiones registradas por el delito de lesiones	decima-oadh::menu.stateInjuries	/ucaoadh/integrity/state-injuries	oadh-inte-si-btn-close	decima-oadh::menu.stateInjuriesAction	fa fa-upload	13	1	3	\N	\N	\N
47	Según sexo y rango de edad - Agresiones registradas por el delito de lesiones	decima-oadh::menu.ageInjuries	/ucaoadh/integrity/age-injuries	oadh-inte-si-btn-close	decima-oadh::menu.ageInjuriesAction	fa fa-upload	13	1	3	\N	\N	\N
48	Según rango de horario y tipo de víct. - Agresiones registradas por el delito de lesiones	decima-oadh::menu.scheduleInjuries	/ucaoadh/integrity/schedule-injuries	oadh-inte-si-btn-close	decima-oadh::menu.scheduleInjuriesAction	fa fa-upload	13	1	3	\N	\N	\N
49	Investigaciones de agentes PNC por delitos relativo al abuso de la fuerza y uso ilegal de armas de fuego	decima-oadh::menu.agentInvestigation	/ucaoadh/integrity/agent-investigation	oadh-inte-ai-btn-close	decima-oadh::menu.agentInvestigationAction	fa fa-upload	13	1	3	\N	\N	\N
50	Denuncias reportadas en la PDDH por la vulneración del derecho a la integridad personal	decima-oadh::menu.integrityViolation	/ucaoadh/integrity/integrity-violation	oadh-inte-iv-btn-close	decima-oadh::menu.integrityViolationAction	fa fa-upload	13	1	3	\N	\N	\N
\.


--
-- TOC entry 4419 (class 0 OID 16440)
-- Dependencies: 210
-- Data for Name: SEC_Module; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Module" (id, name, lang_key, icon, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	General Setup	security/module.generalSetup	fa fa-cogs	1	2018-12-13 03:18:42	2018-12-13 03:18:42	\N
2	Files	decima-file::menu.module	fa fa-folder-open	1	2018-12-13 03:20:26	2018-12-13 03:20:26	\N
3	OADH	decima-oadh::menu.module	fa fa-search	1	2018-12-13 04:38:17	2018-12-13 04:38:17	\N
\.


--
-- TOC entry 4421 (class 0 OID 16445)
-- Dependencies: 212
-- Data for Name: SEC_Password_Reminders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Password_Reminders" (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 4422 (class 0 OID 16451)
-- Dependencies: 213
-- Data for Name: SEC_Permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Permission" (id, name, key, lang_key, url, alias_url, action_button_id, action_lang_key, icon, shortcut_icon, is_only_shortcut, is_dashboard_shortcut_visible, hidden, menu_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	Setup a new organization	newOrganization	security/permission.newOrganization	/general-setup/organization/organization-management/new	/general-setup/organization/organization-management	om-btn-new	organization/organization-management.new	fa fa-plus	\N	t	f	f	2	1	2018-12-13 03:18:44	2018-12-13 03:18:44	\N
2	Edit organization	editOrganization	organization/organization-management.edit	/general-setup/organization/organization-management/edit	/general-setup/organization/organization-management	om-btn-edit-helper	organization/organization-management.edit	\N	\N	t	f	t	2	1	2018-12-13 03:18:44	2018-12-13 03:18:44	\N
3	Remove organization	removeOrganization	organization/organization-management.delete	/general-setup/organization/organization-management/remove	/general-setup/organization/organization-management	om-btn-remove-helper	organization/organization-management.delete	\N	\N	t	f	t	2	1	2018-12-13 03:18:45	2018-12-13 03:18:45	\N
4	New user	newUser	security/permission.newUser	/general-setup/security/user-management/new	/general-setup/security/user-management	um-btn-new	security/user-management.new	fa fa-plus	\N	t	f	f	4	1	2018-12-13 03:18:45	2018-12-13 03:18:45	\N
5	New administrator user	newAdminUser	security/permission.newAdminUser	/general-setup/security/user-management/new-admin	/general-setup/security/user-management	um-btn-new-admin	security/user-management.newAdminLongText	fa fa-plus	\N	f	f	f	4	1	2018-12-13 03:18:45	2018-12-13 03:18:45	\N
6	Remove user	removeUser	security/user-management.deleteLongText	/general-setup/security/user-management/remove-user	/general-setup/security/user-management	um-btn-remove-helper	security/user-management.deleteLongText	\N	\N	t	f	t	4	1	2018-12-13 03:18:46	2018-12-13 03:18:46	\N
7	Assign role	assignRole	security/permission.assignRole	/general-setup/security/user-management/assign-role	/general-setup/security/user-management	um-btn-assign-helper	security/permission.assignRole	\N	\N	t	f	t	4	1	2018-12-13 03:18:46	2018-12-13 03:18:46	\N
8	Unassign role	unassignRole	security/permission.unassignRole	/general-setup/security/user-management/unassign-role	/general-setup/security/user-management	um-btn-unassign-helper	security/permission.unassignRole	\N	\N	t	f	t	4	1	2018-12-13 03:18:46	2018-12-13 03:18:46	\N
9	Upload File	uploadFile	decima-file::menu.uploadFile	/files/file-manager/upload-file	/files/file-manager	fi-fm-btn-upload-file	decima-file::menu.uploadFileAction	fa fa-cloud-upload	\N	t	f	f	5	1	2018-12-13 03:20:28	2018-12-13 03:20:28	\N
10	New Folder	newFolder	decima-file::menu.newFolder	/files/file-manager/new-folder	/files/file-manager	fi-fm-btn-new-folder	decima-file::menu.newFolderAction	fa fa-folder	\N	t	f	f	5	1	2018-12-13 03:20:28	2018-12-13 03:20:28	\N
11	Delete File	deleteFile	decima-file::menu.deleteFile	/files/file-manager/delete-file	/files/file-manager	fi-fm-btn-delete-file-helper	decima-file::menu.deleteFileAction	\N	\N	t	f	t	5	1	2018-12-13 03:20:28	2018-12-13 03:20:28	\N
12	Delete Folder	deleteFolder	decima-file::menu.deleteFolder	/files/file-manager/delete-folder	/files/file-manager	fi-fm-btn-delete-folder-helper	decima-file::menu.deleteFolderAction	\N	\N	t	f	t	5	1	2018-12-13 03:20:29	2018-12-13 03:20:29	\N
\.


--
-- TOC entry 4424 (class 0 OID 16462)
-- Dependencies: 215
-- Data for Name: SEC_Role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Role" (id, name, lang_key, description, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4425 (class 0 OID 16465)
-- Dependencies: 216
-- Data for Name: SEC_Role_Menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Role_Menu" (id, role_id, menu_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4427 (class 0 OID 16470)
-- Dependencies: 218
-- Data for Name: SEC_Role_Permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_Role_Permission" (id, role_id, permission_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4430 (class 0 OID 16477)
-- Dependencies: 221
-- Data for Name: SEC_User; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User" (id, firstname, lastname, email, password, is_active, is_admin, timezone, activation_code, activated_at, last_login, popovers_shown, multiple_organization_popover_shown, remember_token, created_by, default_organization, created_at, updated_at, deleted_at) FROM stdin;
1	oadh	oadh	oadh@decimaerp.com	$2y$10$4QJVrf/BGIN11JeMNEGqF.U3RE6Ar/UKlEEal/IqQUEyAKMrgGYEG	t	t	America/El_Salvador	\N	\N	\N	t	f	93INSstlnjQALOMsabYOhPKSLUpwOSYyIsXt5gHbVMJArwPdfeybPjI2q4Xq	\N	1	2018-12-13 03:18:42	2019-03-24 18:36:02	\N
\.


--
-- TOC entry 4431 (class 0 OID 16485)
-- Dependencies: 222
-- Data for Name: SEC_User_Menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User_Menu" (id, is_assigned, user_id, menu_id, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	t	1	2	1	1	2018-12-13 04:00:03	2018-12-13 04:00:03	\N
3	t	1	5	1	1	2018-12-13 04:00:03	2018-12-13 04:00:03	\N
4	t	1	11	1	1	2018-12-13 04:39:00	2018-12-13 04:39:00	\N
5	t	1	9	1	1	2018-12-13 04:39:01	2018-12-13 04:39:01	\N
6	t	1	8	1	1	2018-12-13 04:39:01	2018-12-13 04:39:01	\N
7	t	1	7	1	1	2018-12-13 04:39:01	2018-12-13 04:39:01	\N
13	t	1	4	1	1	2018-12-14 02:15:31	2018-12-14 02:15:31	\N
14	t	1	19	1	1	2019-03-17 17:53:18	2019-03-17 17:53:18	\N
15	t	1	20	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
16	t	1	21	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
17	t	1	22	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
18	t	1	23	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
19	t	1	24	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
20	t	1	25	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
21	t	1	26	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
22	t	1	27	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
23	t	1	28	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
24	t	1	29	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
25	t	1	30	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
26	t	1	31	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
27	t	1	32	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
28	t	1	33	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
29	t	1	34	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
30	t	1	35	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
31	t	1	36	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
32	t	1	37	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
33	t	1	38	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
34	t	1	39	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
35	t	1	40	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
36	t	1	41	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
37	t	1	42	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
38	t	1	43	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
39	t	1	44	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
40	t	1	45	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
41	t	1	46	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
42	t	1	47	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
43	t	1	48	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
44	t	1	49	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
45	t	1	50	1	1	2019-03-17 17:53:19	2019-03-17 17:53:19	\N
\.


--
-- TOC entry 4433 (class 0 OID 16490)
-- Dependencies: 224
-- Data for Name: SEC_User_Organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User_Organization" (id, user_id, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	1	1	1	2018-12-13 04:00:02	2018-12-13 04:00:02	\N
\.


--
-- TOC entry 4435 (class 0 OID 16495)
-- Dependencies: 226
-- Data for Name: SEC_User_Permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User_Permission" (id, is_assigned, user_id, permission_id, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
1	t	1	5	1	1	2018-12-13 04:00:03	2018-12-13 04:00:03	\N
\.


--
-- TOC entry 4437 (class 0 OID 16500)
-- Dependencies: 228
-- Data for Name: SEC_User_Role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SEC_User_Role" (id, user_id, role_id, organization_id, created_by, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4440 (class 0 OID 16507)
-- Dependencies: 231
-- Data for Name: SYS_Account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Account" (id, key, parent_key, name, balance_type, account_type_key, is_group, is_general_ledger_account, account_chart_type_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4441 (class 0 OID 16515)
-- Dependencies: 232
-- Data for Name: SYS_Account_Chart_Type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Account_Chart_Type" (id, name, url, lang_key, country_id, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4443 (class 0 OID 16520)
-- Dependencies: 234
-- Data for Name: SYS_Account_Type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Account_Type" (id, name, lang_key, key, pl_bs_category, deferral_method, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4446 (class 0 OID 16527)
-- Dependencies: 237
-- Data for Name: SYS_Country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Country" (id, iso_code, name, region_name, region_lang_key, tax_id_name, tax_id_abbreviation, registration_number_name, registration_number_abbreviation, single_identity_document_number_name, single_identity_document_number_abbreviation, social_security_number_name, social_security_number_abbreviation, single_previsional_number_name, single_previsional_number_abbreviation, currency_id, created_at, updated_at, deleted_at) FROM stdin;
1	AND	Andorra	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:16:46	2018-12-13 03:16:46	\N
2	ARE	United Arab Emirates	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	2018-12-13 03:16:46	2018-12-13 03:16:46	\N
3	AFG	Afghanistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2	2018-12-13 03:16:46	2018-12-13 03:16:46	\N
4	ATG	Antigua And Barbuda	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:16:47	2018-12-13 03:16:47	\N
5	AIA	Anguilla	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:16:47	2018-12-13 03:16:47	\N
6	ALB	Albania	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3	2018-12-13 03:16:47	2018-12-13 03:16:47	\N
7	ARM	Armenia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	4	2018-12-13 03:16:48	2018-12-13 03:16:48	\N
8	ANT	Netherlands Antilles	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	5	2018-12-13 03:16:48	2018-12-13 03:16:48	\N
9	AGO	Angola	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	6	2018-12-13 03:16:48	2018-12-13 03:16:48	\N
10	ATA	Antarctica	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:16:48	2018-12-13 03:16:48	\N
11	ARG	Argentina	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	7	2018-12-13 03:16:49	2018-12-13 03:16:49	\N
12	ASM	American Samoa	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:16:49	2018-12-13 03:16:49	\N
13	AUT	Austria	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:16:49	2018-12-13 03:16:49	\N
14	AUS	Australia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:16:50	2018-12-13 03:16:50	\N
15	ABW	Aruba	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	10	2018-12-13 03:16:50	2018-12-13 03:16:50	\N
16	AZE	Azerbaijan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	11	2018-12-13 03:16:50	2018-12-13 03:16:50	\N
17	BIH	Bosnia And Herzegovina	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	12	2018-12-13 03:16:50	2018-12-13 03:16:50	\N
18	BRB	Barbados	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	13	2018-12-13 03:16:51	2018-12-13 03:16:51	\N
19	BGD	Bangladesh	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	14	2018-12-13 03:16:51	2018-12-13 03:16:51	\N
20	BEL	Belgium	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:16:51	2018-12-13 03:16:51	\N
21	BFA	Burkina Faso	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:16:52	2018-12-13 03:16:52	\N
22	BGR	Bulgaria	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	16	2018-12-13 03:16:52	2018-12-13 03:16:52	\N
23	BHR	Bahrain	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	18	2018-12-13 03:16:52	2018-12-13 03:16:52	\N
24	BDI	Burundi	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	19	2018-12-13 03:16:52	2018-12-13 03:16:52	\N
25	BEN	Benin	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:16:53	2018-12-13 03:16:53	\N
26	BMU	Bermuda	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	20	2018-12-13 03:16:53	2018-12-13 03:16:53	\N
27	BRN	Brunei Darussalam	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	21	2018-12-13 03:16:53	2018-12-13 03:16:53	\N
28	BOL	Bolivia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	22	2018-12-13 03:16:54	2018-12-13 03:16:54	\N
29	BRA	Brazil	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	23	2018-12-13 03:16:54	2018-12-13 03:16:54	\N
30	BHS	Bahamas	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	24	2018-12-13 03:16:54	2018-12-13 03:16:54	\N
31	BTN	Bhutan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	72	2018-12-13 03:16:54	2018-12-13 03:16:54	\N
32	BVT	Bouvet Island	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:16:55	2018-12-13 03:16:55	\N
33	BWA	Botswana	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	26	2018-12-13 03:16:55	2018-12-13 03:16:55	\N
34	BLR	Belarus	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	27	2018-12-13 03:16:55	2018-12-13 03:16:55	\N
35	BLZ	Belize	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	28	2018-12-13 03:16:55	2018-12-13 03:16:55	\N
36	CAN	Canada	Province	system/country.province	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	29	2018-12-13 03:16:56	2018-12-13 03:16:56	\N
37	CCK	Cocos (Keeling) Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:16:56	2018-12-13 03:16:56	\N
38	COD	Congo The Democratic Republic Of The	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	30	2018-12-13 03:16:56	2018-12-13 03:16:56	\N
39	CAF	Central African Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:16:57	2018-12-13 03:16:57	\N
40	COG	Congo	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:16:57	2018-12-13 03:16:57	\N
41	CHE	Switzerland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	31	2018-12-13 03:16:57	2018-12-13 03:16:57	\N
42	CIV	Cote D'Ivoire	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:16:57	2018-12-13 03:16:57	\N
43	COK	Cook Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	116	2018-12-13 03:16:58	2018-12-13 03:16:58	\N
44	CHL	Chile	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	32	2018-12-13 03:16:58	2018-12-13 03:16:58	\N
45	CMR	Cameroon	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:16:58	2018-12-13 03:16:58	\N
46	CHN	China	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	33	2018-12-13 03:16:58	2018-12-13 03:16:58	\N
47	COL	Colombia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	34	2018-12-13 03:16:59	2018-12-13 03:16:59	\N
48	CRI	Costa Rica	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	35	2018-12-13 03:16:59	2018-12-13 03:16:59	\N
49	CUB	Cuba	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	36	2018-12-13 03:16:59	2018-12-13 03:16:59	\N
50	CPV	Cape Verde	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	37	2018-12-13 03:17:00	2018-12-13 03:17:00	\N
51	CXR	Christmas Island	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:00	2018-12-13 03:17:00	\N
52	CYP	Cyprus	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	38	2018-12-13 03:17:00	2018-12-13 03:17:00	\N
53	CZE	Czech Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	39	2018-12-13 03:17:00	2018-12-13 03:17:00	\N
54	DEU	Germany	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:01	2018-12-13 03:17:01	\N
55	DJI	Djibouti	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	41	2018-12-13 03:17:01	2018-12-13 03:17:01	\N
56	DNK	Denmark	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	42	2018-12-13 03:17:01	2018-12-13 03:17:01	\N
57	DMA	Dominica	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:01	2018-12-13 03:17:01	\N
58	DOM	Dominican Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	43	2018-12-13 03:17:02	2018-12-13 03:17:02	\N
59	DZA	Algeria	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	44	2018-12-13 03:17:02	2018-12-13 03:17:02	\N
60	ECU	Ecuador	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:02	2018-12-13 03:17:02	\N
61	EST	Estonia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	45	2018-12-13 03:17:03	2018-12-13 03:17:03	\N
62	EGY	Egypt	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	46	2018-12-13 03:17:03	2018-12-13 03:17:03	\N
63	ESH	Western Sahara	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:03	2018-12-13 03:17:03	\N
64	ERI	Eritrea	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	47	2018-12-13 03:17:03	2018-12-13 03:17:03	\N
65	ESP	Spain	Provincia	system/country.provincia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:04	2018-12-13 03:17:04	\N
66	ETH	Ethiopia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	49	2018-12-13 03:17:04	2018-12-13 03:17:04	\N
67	FIN	Finland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:04	2018-12-13 03:17:04	\N
68	FJI	Fiji	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	52	2018-12-13 03:17:04	2018-12-13 03:17:04	\N
69	FLK	Falkland Islands (Malvinas)	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	53	2018-12-13 03:17:05	2018-12-13 03:17:05	\N
70	FSM	Micronesia Federated States Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:05	2018-12-13 03:17:05	\N
71	FRO	Faroe Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	42	2018-12-13 03:17:05	2018-12-13 03:17:05	\N
72	FRA	France	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:06	2018-12-13 03:17:06	\N
73	GAB	Gabon	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:17:06	2018-12-13 03:17:06	\N
74	GBR	United Kingdom	County	system/country.county	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	55	2018-12-13 03:17:06	2018-12-13 03:17:06	\N
75	GRD	Grenada	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:06	2018-12-13 03:17:06	\N
76	GEO	Georgia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	56	2018-12-13 03:17:07	2018-12-13 03:17:07	\N
77	GUF	French Guiana	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:07	2018-12-13 03:17:07	\N
78	GHA	Ghana	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	57	2018-12-13 03:17:07	2018-12-13 03:17:07	\N
79	GIB	Gibraltar	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	58	2018-12-13 03:17:07	2018-12-13 03:17:07	\N
80	GRL	Greenland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	42	2018-12-13 03:17:08	2018-12-13 03:17:08	\N
81	GMB	Gambia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	59	2018-12-13 03:17:08	2018-12-13 03:17:08	\N
82	GIN	Guinea	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	60	2018-12-13 03:17:08	2018-12-13 03:17:08	\N
83	GLP	Guadeloupe	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:09	2018-12-13 03:17:09	\N
84	GNQ	Equatorial Guinea	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:17:09	2018-12-13 03:17:09	\N
85	GRC	Greece	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:09	2018-12-13 03:17:09	\N
86	SGS	South Georgia And The South Sandwich Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:10	2018-12-13 03:17:10	\N
87	GTM	Guatemala	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	61	2018-12-13 03:17:10	2018-12-13 03:17:10	\N
88	GUM	Guam	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:10	2018-12-13 03:17:10	\N
89	GNB	Guinea-Bissau	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	62	2018-12-13 03:17:10	2018-12-13 03:17:10	\N
90	GUY	Guyana	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	63	2018-12-13 03:17:11	2018-12-13 03:17:11	\N
91	HKG	Hong Kong	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	64	2018-12-13 03:17:11	2018-12-13 03:17:11	\N
92	HMD	Heard Island And Mcdonald Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:11	2018-12-13 03:17:11	\N
93	HND	Honduras	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	65	2018-12-13 03:17:11	2018-12-13 03:17:11	\N
94	HRV	Croatia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	66	2018-12-13 03:17:12	2018-12-13 03:17:12	\N
95	HTI	Haiti	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	67	2018-12-13 03:17:12	2018-12-13 03:17:12	\N
96	HUN	Hungary	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	68	2018-12-13 03:17:12	2018-12-13 03:17:12	\N
97	IDN	Indonesia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	69	2018-12-13 03:17:13	2018-12-13 03:17:13	\N
98	IRL	Ireland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:13	2018-12-13 03:17:13	\N
99	ISR	Israel	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71	2018-12-13 03:17:13	2018-12-13 03:17:13	\N
100	IND	India	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	72	2018-12-13 03:17:13	2018-12-13 03:17:13	\N
101	IOT	British Indian Ocean Territory	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:14	2018-12-13 03:17:14	\N
102	IRQ	Iraq	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	73	2018-12-13 03:17:14	2018-12-13 03:17:14	\N
103	IRN	Iran Islamic Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	74	2018-12-13 03:17:14	2018-12-13 03:17:14	\N
104	ISL	Iceland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	75	2018-12-13 03:17:15	2018-12-13 03:17:15	\N
105	ITA	Italy	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:15	2018-12-13 03:17:15	\N
106	JAM	Jamaica	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	77	2018-12-13 03:17:15	2018-12-13 03:17:15	\N
107	JOR	Jordan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	78	2018-12-13 03:17:15	2018-12-13 03:17:15	\N
108	JPN	Japan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	79	2018-12-13 03:17:16	2018-12-13 03:17:16	\N
109	KEN	Kenya	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	80	2018-12-13 03:17:16	2018-12-13 03:17:16	\N
110	KGZ	Kyrgyzstan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	81	2018-12-13 03:17:16	2018-12-13 03:17:16	\N
111	KHM	Cambodia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	82	2018-12-13 03:17:16	2018-12-13 03:17:16	\N
112	KIR	Kiribati	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:17	2018-12-13 03:17:17	\N
113	COM	Comoros	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	83	2018-12-13 03:17:17	2018-12-13 03:17:17	\N
114	KNA	Saint Kitts And Nevis	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:17	2018-12-13 03:17:17	\N
115	PRK	Korea Democratic People's Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	84	2018-12-13 03:17:18	2018-12-13 03:17:18	\N
116	KOR	Korea Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	85	2018-12-13 03:17:18	2018-12-13 03:17:18	\N
117	KWT	Kuwait	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	86	2018-12-13 03:17:18	2018-12-13 03:17:18	\N
118	CYM	Cayman Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	87	2018-12-13 03:17:18	2018-12-13 03:17:18	\N
119	KAZ	Kazakhstan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	88	2018-12-13 03:17:19	2018-12-13 03:17:19	\N
120	LAO	Lao People's Democratic Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	89	2018-12-13 03:17:19	2018-12-13 03:17:19	\N
121	LBN	Lebanon	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	90	2018-12-13 03:17:19	2018-12-13 03:17:19	\N
122	LCA	Saint Lucia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:19	2018-12-13 03:17:19	\N
123	LIE	Liechtenstein	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	31	2018-12-13 03:17:20	2018-12-13 03:17:20	\N
124	LKA	Sri Lanka	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	91	2018-12-13 03:17:20	2018-12-13 03:17:20	\N
125	LBR	Liberia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	92	2018-12-13 03:17:20	2018-12-13 03:17:20	\N
126	LSO	Lesotho	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	173	2018-12-13 03:17:21	2018-12-13 03:17:21	\N
127	LTU	Lithuania	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	93	2018-12-13 03:17:21	2018-12-13 03:17:21	\N
128	LUX	Luxembourg	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:21	2018-12-13 03:17:21	\N
129	LVA	Latvia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	94	2018-12-13 03:17:21	2018-12-13 03:17:21	\N
130	LBY	Libyan Arab Jamahiriya	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	95	2018-12-13 03:17:22	2018-12-13 03:17:22	\N
131	MAR	Morocco	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	96	2018-12-13 03:17:22	2018-12-13 03:17:22	\N
132	MCO	Monaco	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:22	2018-12-13 03:17:22	\N
133	MDA	Moldova	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	97	2018-12-13 03:17:22	2018-12-13 03:17:22	\N
134	MNE	Montenegro	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:23	2018-12-13 03:17:23	\N
135	MDG	Madagascar	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	98	2018-12-13 03:17:23	2018-12-13 03:17:23	\N
136	MHL	Marshall Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:23	2018-12-13 03:17:23	\N
137	MKD	Macedonia, The Former Yugoslav Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	99	2018-12-13 03:17:24	2018-12-13 03:17:24	\N
138	MLI	Mali	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:17:24	2018-12-13 03:17:24	\N
139	MMR	Myanmar	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100	2018-12-13 03:17:24	2018-12-13 03:17:24	\N
140	MNG	Mongolia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	101	2018-12-13 03:17:24	2018-12-13 03:17:24	\N
141	MAC	Macao	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	102	2018-12-13 03:17:25	2018-12-13 03:17:25	\N
142	MNP	Northern Mariana Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:25	2018-12-13 03:17:25	\N
143	MTQ	Martinique	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:25	2018-12-13 03:17:25	\N
144	MRT	Mauritania	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	103	2018-12-13 03:17:26	2018-12-13 03:17:26	\N
145	MSR	Montserrat	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:26	2018-12-13 03:17:26	\N
146	MLT	Malta	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	104	2018-12-13 03:17:26	2018-12-13 03:17:26	\N
147	MUS	Mauritius	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	105	2018-12-13 03:17:26	2018-12-13 03:17:26	\N
148	MDV	Maldives	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	106	2018-12-13 03:17:27	2018-12-13 03:17:27	\N
149	MWI	Malawi	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	107	2018-12-13 03:17:27	2018-12-13 03:17:27	\N
150	MEX	Mexico	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	108	2018-12-13 03:17:27	2018-12-13 03:17:27	\N
151	MYS	Malaysia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	109	2018-12-13 03:17:27	2018-12-13 03:17:27	\N
152	MOZ	Mozambique	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	110	2018-12-13 03:17:28	2018-12-13 03:17:28	\N
153	NAM	Namibia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	173	2018-12-13 03:17:28	2018-12-13 03:17:28	\N
154	NCL	New Caledonia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	170	2018-12-13 03:17:28	2018-12-13 03:17:28	\N
155	NER	Niger	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:17:29	2018-12-13 03:17:29	\N
156	NFK	Norfolk Island	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:29	2018-12-13 03:17:29	\N
157	NGA	Nigeria	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	111	2018-12-13 03:17:29	2018-12-13 03:17:29	\N
158	NIC	Nicaragua	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	112	2018-12-13 03:17:29	2018-12-13 03:17:29	\N
159	NLD	Netherlands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:30	2018-12-13 03:17:30	\N
160	NOR	Norway	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	114	2018-12-13 03:17:30	2018-12-13 03:17:30	\N
161	NPL	Nepal	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	115	2018-12-13 03:17:30	2018-12-13 03:17:30	\N
162	NRU	Nauru	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:30	2018-12-13 03:17:30	\N
163	NIU	Niue	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	116	2018-12-13 03:17:31	2018-12-13 03:17:31	\N
164	NZL	New Zealand	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	116	2018-12-13 03:17:31	2018-12-13 03:17:31	\N
165	OMN	Oman	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	117	2018-12-13 03:17:31	2018-12-13 03:17:31	\N
166	PAN	Panama	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	118	2018-12-13 03:17:32	2018-12-13 03:17:32	\N
167	PER	Peru	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	119	2018-12-13 03:17:32	2018-12-13 03:17:32	\N
168	PYF	French Polynesia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	170	2018-12-13 03:17:32	2018-12-13 03:17:32	\N
169	PNG	Papua New Guinea	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	120	2018-12-13 03:17:32	2018-12-13 03:17:32	\N
170	PHL	Philippines	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	121	2018-12-13 03:17:33	2018-12-13 03:17:33	\N
171	PAK	Pakistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	122	2018-12-13 03:17:33	2018-12-13 03:17:33	\N
172	POL	Poland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	123	2018-12-13 03:17:33	2018-12-13 03:17:33	\N
173	SPM	Saint Pierre And Miquelon	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:33	2018-12-13 03:17:33	\N
174	PCN	Pitcairn	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:34	2018-12-13 03:17:34	\N
175	PRI	Puerto Rico	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:34	2018-12-13 03:17:34	\N
176	PSE	Palestinian Territory Occupied	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:34	2018-12-13 03:17:34	\N
177	PRT	Portugal	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:35	2018-12-13 03:17:35	\N
178	PLW	Palau	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:35	2018-12-13 03:17:35	\N
179	PRY	Paraguay	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	125	2018-12-13 03:17:35	2018-12-13 03:17:35	\N
180	QAT	Qatar	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	126	2018-12-13 03:17:35	2018-12-13 03:17:35	\N
181	REU	Réunion	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:36	2018-12-13 03:17:36	\N
182	ROU	Romania	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	127	2018-12-13 03:17:36	2018-12-13 03:17:36	\N
183	SRB	Serbia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	129	2018-12-13 03:17:36	2018-12-13 03:17:36	\N
184	RUS	Russian Federation	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	130	2018-12-13 03:17:37	2018-12-13 03:17:37	\N
185	RWA	Rwanda	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	131	2018-12-13 03:17:37	2018-12-13 03:17:37	\N
186	SAU	Saudi Arabia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	132	2018-12-13 03:17:37	2018-12-13 03:17:37	\N
187	SLB	Solomon Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	133	2018-12-13 03:17:38	2018-12-13 03:17:38	\N
188	SYC	Seychelles	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	134	2018-12-13 03:17:38	2018-12-13 03:17:38	\N
189	SDN	Sudan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	135	2018-12-13 03:17:38	2018-12-13 03:17:38	\N
190	SWE	Sweden	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	136	2018-12-13 03:17:38	2018-12-13 03:17:38	\N
191	SGP	Singapore	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	138	2018-12-13 03:17:39	2018-12-13 03:17:39	\N
192	SHN	Saint Helena	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	139	2018-12-13 03:17:39	2018-12-13 03:17:39	\N
193	SVN	Slovenia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:39	2018-12-13 03:17:39	\N
194	SJM	Svalbard And Jan Mayen	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:39	2018-12-13 03:17:39	\N
195	SVK	Slovakia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	140	2018-12-13 03:17:40	2018-12-13 03:17:40	\N
196	SLE	Sierra Leone	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	141	2018-12-13 03:17:40	2018-12-13 03:17:40	\N
197	SMR	San Marino	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:40	2018-12-13 03:17:40	\N
198	SEN	Senegal	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:17:41	2018-12-13 03:17:41	\N
199	SOM	Somalia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	142	2018-12-13 03:17:41	2018-12-13 03:17:41	\N
200	SUR	Suriname	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	143	2018-12-13 03:17:41	2018-12-13 03:17:41	\N
201	STP	Sao Tome And Principe	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	144	2018-12-13 03:17:41	2018-12-13 03:17:41	\N
202	SLV	El Salvador	State	system/country.department	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:42	2018-12-13 03:17:42	\N
203	SYR	Syrian Arab Republic	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	146	2018-12-13 03:17:42	2018-12-13 03:17:42	\N
204	SWZ	Swaziland	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	147	2018-12-13 03:17:42	2018-12-13 03:17:42	\N
205	TCA	Turks And Caicos Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:42	2018-12-13 03:17:42	\N
206	TCD	Chad	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	167	2018-12-13 03:17:43	2018-12-13 03:17:43	\N
207	ATF	French Southern Territories	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:43	2018-12-13 03:17:43	\N
208	TGO	Togo	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	169	2018-12-13 03:17:43	2018-12-13 03:17:43	\N
209	THA	Thailand	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	148	2018-12-13 03:17:44	2018-12-13 03:17:44	\N
210	TJK	Tajikistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	149	2018-12-13 03:17:44	2018-12-13 03:17:44	\N
211	TKL	Tokelau	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	116	2018-12-13 03:17:44	2018-12-13 03:17:44	\N
212	TLS	Timor-Leste	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:44	2018-12-13 03:17:44	\N
213	TKM	Turkmenistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	150	2018-12-13 03:17:45	2018-12-13 03:17:45	\N
214	TUN	Tunisia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	151	2018-12-13 03:17:45	2018-12-13 03:17:45	\N
215	TON	Tonga	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	152	2018-12-13 03:17:45	2018-12-13 03:17:45	\N
216	TUR	Turkey	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	154	2018-12-13 03:17:46	2018-12-13 03:17:46	\N
217	TTO	Trinidad And Tobago	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	155	2018-12-13 03:17:46	2018-12-13 03:17:46	\N
218	TUV	Tuvalu	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	9	2018-12-13 03:17:46	2018-12-13 03:17:46	\N
219	TWN	Taiwan, Province Of China	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	156	2018-12-13 03:17:46	2018-12-13 03:17:46	\N
220	TZA	Tanzania United Republic Of	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	157	2018-12-13 03:17:47	2018-12-13 03:17:47	\N
221	UKR	Ukraine	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	158	2018-12-13 03:17:47	2018-12-13 03:17:47	\N
222	UGA	Uganda	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	159	2018-12-13 03:17:47	2018-12-13 03:17:47	\N
223	UMI	United States Minor Outlying Islands	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2018-12-13 03:17:47	2018-12-13 03:17:47	\N
224	USA	United States	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:48	2018-12-13 03:17:48	\N
225	URY	Uruguay	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	161	2018-12-13 03:17:48	2018-12-13 03:17:48	\N
226	UZB	Uzbekistan	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	162	2018-12-13 03:17:48	2018-12-13 03:17:48	\N
227	VAT	Holy See (Vatican City State)	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:49	2018-12-13 03:17:49	\N
228	VCT	Saint Vincent And The Grenadines	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	168	2018-12-13 03:17:49	2018-12-13 03:17:49	\N
229	VEN	Venezuela	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	163	2018-12-13 03:17:49	2018-12-13 03:17:49	\N
230	VGB	Virgin Islands British	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:49	2018-12-13 03:17:49	\N
231	VIR	Virgin Islands U.S.	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	160	2018-12-13 03:17:50	2018-12-13 03:17:50	\N
232	VNM	Viet Nam	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	164	2018-12-13 03:17:50	2018-12-13 03:17:50	\N
233	VUT	Vanuatu	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	165	2018-12-13 03:17:50	2018-12-13 03:17:50	\N
234	WLF	Wallis And Futuna	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	170	2018-12-13 03:17:50	2018-12-13 03:17:50	\N
235	WSM	Samoa	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	166	2018-12-13 03:17:51	2018-12-13 03:17:51	\N
236	YEM	Yemen	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	171	2018-12-13 03:17:51	2018-12-13 03:17:51	\N
237	MYT	Mayotte	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	50	2018-12-13 03:17:51	2018-12-13 03:17:51	\N
238	ZAF	South Africa	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	173	2018-12-13 03:17:52	2018-12-13 03:17:52	\N
239	ZMB	Zambia	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	174	2018-12-13 03:17:52	2018-12-13 03:17:52	\N
240	ZWE	Zimbabwe	State	system/country.state	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	175	2018-12-13 03:17:52	2018-12-13 03:17:52	\N
\.


--
-- TOC entry 4448 (class 0 OID 16535)
-- Dependencies: 239
-- Data for Name: SYS_Currency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Currency" (id, iso_code, symbol, name, standard_precision, costing_precision, price_precision, currency_symbol_at_the_right, created_at, updated_at, deleted_at) FROM stdin;
2	AFA	Af	Afghani	2	2	2	f	2018-12-13 03:15:58	2018-12-13 03:15:58	\N
3	ALL	L	Lek	2	2	2	f	2018-12-13 03:15:58	2018-12-13 03:15:58	\N
4	AMD	դր.	Armenian Dram	2	2	2	f	2018-12-13 03:15:58	2018-12-13 03:15:58	\N
5	ANG	NAf.	Netherlands Antillian Guilder	2	2	2	f	2018-12-13 03:15:59	2018-12-13 03:15:59	\N
6	AOA	Kz	Kwanza	2	2	2	f	2018-12-13 03:15:59	2018-12-13 03:15:59	\N
7	ARS	$	Argentine Peso	2	2	2	f	2018-12-13 03:15:59	2018-12-13 03:15:59	\N
8	ATS	Sch	Austrian Schilling	2	2	2	f	2018-12-13 03:15:59	2018-12-13 03:15:59	\N
9	AUD	$	Australian Dollar	2	2	2	f	2018-12-13 03:16:00	2018-12-13 03:16:00	\N
10	AWG	ƒ	Aruban Guilder	2	2	2	f	2018-12-13 03:16:00	2018-12-13 03:16:00	\N
11	AZM	m	Azerbaijanian Manat	2	2	2	f	2018-12-13 03:16:00	2018-12-13 03:16:00	\N
12	BAM	KM	Convertible Marks	2	2	2	f	2018-12-13 03:16:00	2018-12-13 03:16:00	\N
13	BBD	Bds$	Barbados Dollar	2	2	2	f	2018-12-13 03:16:01	2018-12-13 03:16:01	\N
14	BDT	৳	Taka	2	2	2	f	2018-12-13 03:16:01	2018-12-13 03:16:01	\N
15	BEF	BFr	Belgian Franc	0	0	0	f	2018-12-13 03:16:01	2018-12-13 03:16:01	\N
16	BGL	Lv	Lev	2	2	2	f	2018-12-13 03:16:02	2018-12-13 03:16:02	\N
17	BGN	лв	Bulgarian Lev	2	2	2	f	2018-12-13 03:16:02	2018-12-13 03:16:02	\N
18	BHD	ب.د	Bahraini Dinar	3	5	5	f	2018-12-13 03:16:02	2018-12-13 03:16:02	\N
19	BIF	FBu	Burundi Franc	0	2	2	f	2018-12-13 03:16:02	2018-12-13 03:16:02	\N
20	BMD	Bd$	Bermudian Dollar	2	2	2	f	2018-12-13 03:16:03	2018-12-13 03:16:03	\N
21	BND	B$	Brunei Dollar	2	2	2	f	2018-12-13 03:16:03	2018-12-13 03:16:03	\N
22	BOB	Bs.	Boliviano	2	2	2	f	2018-12-13 03:16:03	2018-12-13 03:16:03	\N
23	BRL	R$	Brazilian Real	2	2	2	f	2018-12-13 03:16:04	2018-12-13 03:16:04	\N
24	BSD	B$	Bahamian Dollar	2	2	2	f	2018-12-13 03:16:04	2018-12-13 03:16:04	\N
25	BSF	Bs F	Venezuelan BolÃ­var Fuerte	2	2	2	f	2018-12-13 03:16:04	2018-12-13 03:16:04	\N
26	BWP	P	Pula	2	2	2	f	2018-12-13 03:16:04	2018-12-13 03:16:04	\N
27	BYR	BR	Belarussian Ruble	0	2	2	f	2018-12-13 03:16:05	2018-12-13 03:16:05	\N
28	BZD	BZ$	Belize Dollar	2	2	2	f	2018-12-13 03:16:05	2018-12-13 03:16:05	\N
29	CAD	C$	Canadian Dollar	2	2	2	f	2018-12-13 03:16:05	2018-12-13 03:16:05	\N
30	CDF	Fr	Franc Congolais	2	2	2	f	2018-12-13 03:16:05	2018-12-13 03:16:05	\N
31	CHF	SwF	Swiss Franc	2	2	2	f	2018-12-13 03:16:06	2018-12-13 03:16:06	\N
32	CLP	Ch$	Chilean Peso	0	2	2	f	2018-12-13 03:16:06	2018-12-13 03:16:06	\N
33	CNY	¥	Yuan Renminbi	2	2	2	f	2018-12-13 03:16:06	2018-12-13 03:16:06	\N
34	COP	Col$	Colombian Peso	2	2	2	f	2018-12-13 03:16:07	2018-12-13 03:16:07	\N
35	CRC	₡	Costa Rican Colon	2	2	2	f	2018-12-13 03:16:07	2018-12-13 03:16:07	\N
36	CUP	Cu$	Cuban Peso	2	2	2	f	2018-12-13 03:16:07	2018-12-13 03:16:07	\N
37	CVE	C.V.Esc.	Cape Verde Escudo	2	2	2	f	2018-12-13 03:16:07	2018-12-13 03:16:07	\N
38	CYP	£C	Cyprus Pound	2	2	2	f	2018-12-13 03:16:08	2018-12-13 03:16:08	\N
39	CZK	Kč	Czech Koruna	2	2	2	f	2018-12-13 03:16:08	2018-12-13 03:16:08	\N
40	DEM	DM	Deutsche Mark	2	2	2	f	2018-12-13 03:16:08	2018-12-13 03:16:08	\N
41	DJF	DF	Djibouti Franc	0	2	2	f	2018-12-13 03:16:09	2018-12-13 03:16:09	\N
42	DKK	Dkr	Danish Krone	2	2	2	f	2018-12-13 03:16:09	2018-12-13 03:16:09	\N
43	DOP	RD$	Dominican Peso	2	2	2	f	2018-12-13 03:16:09	2018-12-13 03:16:09	\N
44	DZD	د.ج	Algerian Dinar	2	2	2	f	2018-12-13 03:16:09	2018-12-13 03:16:09	\N
45	EEK	KR	Kroon	2	2	2	f	2018-12-13 03:16:10	2018-12-13 03:16:10	\N
46	EGP	£E	Egyptian Pound	2	2	2	f	2018-12-13 03:16:10	2018-12-13 03:16:10	\N
47	ERN	Nfk	Nakfa	2	2	2	f	2018-12-13 03:16:10	2018-12-13 03:16:10	\N
48	ESP	Pts	Spanish Peseta	0	0	0	f	2018-12-13 03:16:10	2018-12-13 03:16:10	\N
49	ETB	Br	Ethiopian Birr	2	2	2	f	2018-12-13 03:16:11	2018-12-13 03:16:11	\N
50	EUR	€	Euro	2	2	2	t	2018-12-13 03:16:11	2018-12-13 03:16:11	\N
51	FIM	FM	Finish Mark	2	2	2	f	2018-12-13 03:16:11	2018-12-13 03:16:11	\N
52	FJD	F$	Fiji Dollar	2	2	2	f	2018-12-13 03:16:12	2018-12-13 03:16:12	\N
53	FKP	£F	Falkland Islands Pound	2	2	2	f	2018-12-13 03:16:12	2018-12-13 03:16:12	\N
54	FRF	Fr	French Franc	2	2	2	f	2018-12-13 03:16:12	2018-12-13 03:16:12	\N
55	GBP	£	British Pound	2	2	2	f	2018-12-13 03:16:12	2018-12-13 03:16:12	\N
56	GEL	ლ	Lari	2	2	2	f	2018-12-13 03:16:13	2018-12-13 03:16:13	\N
57	GHC	¢	Cedi	2	2	2	f	2018-12-13 03:16:13	2018-12-13 03:16:13	\N
58	GIP	£G	Gibraltar Pound	2	2	2	f	2018-12-13 03:16:13	2018-12-13 03:16:13	\N
59	GMD	D	Dalasi	2	2	2	f	2018-12-13 03:16:14	2018-12-13 03:16:14	\N
60	GNF	Fr	Guinea Franc	0	2	2	f	2018-12-13 03:16:14	2018-12-13 03:16:14	\N
61	GTQ	Q	Quetzal	2	2	2	f	2018-12-13 03:16:14	2018-12-13 03:16:14	\N
62	GWP	$	Guinea-Bissau Peso	2	2	2	f	2018-12-13 03:16:14	2018-12-13 03:16:14	\N
63	GYD	G$	Guyana Dollar	2	2	2	f	2018-12-13 03:16:15	2018-12-13 03:16:15	\N
64	HKD	HK$	Hong Kong Dollar	2	2	2	f	2018-12-13 03:16:15	2018-12-13 03:16:15	\N
65	HNL	L	Lempira	2	2	2	f	2018-12-13 03:16:15	2018-12-13 03:16:15	\N
66	HRK	kn	Croatian Kuna	2	2	2	f	2018-12-13 03:16:15	2018-12-13 03:16:15	\N
67	HTG	G	Gourde	2	2	2	f	2018-12-13 03:16:16	2018-12-13 03:16:16	\N
68	HUF	Ft	Forint	2	2	2	f	2018-12-13 03:16:16	2018-12-13 03:16:16	\N
69	IDR	Rp	Rupiah	2	2	2	f	2018-12-13 03:16:16	2018-12-13 03:16:16	\N
70	IEP	I£	Irish Pound	2	2	2	f	2018-12-13 03:16:17	2018-12-13 03:16:17	\N
71	ILS	₪	New Israeli Sheqel	2	2	2	f	2018-12-13 03:16:17	2018-12-13 03:16:17	\N
72	INR	Rs	Indian Rupee	2	2	2	f	2018-12-13 03:16:17	2018-12-13 03:16:17	\N
73	IQD	ع.د	Iraqi Dinar	3	5	5	f	2018-12-13 03:16:17	2018-12-13 03:16:17	\N
74	IRR	﷼	Iranian Rial	2	2	2	f	2018-12-13 03:16:18	2018-12-13 03:16:18	\N
75	ISK	IKr	Iceland Krona	2	2	2	f	2018-12-13 03:16:18	2018-12-13 03:16:18	\N
76	ITL	L	Italian Lira	0	0	0	f	2018-12-13 03:16:18	2018-12-13 03:16:18	\N
77	JMD	J$	Jamaican Dollar	2	2	2	f	2018-12-13 03:16:19	2018-12-13 03:16:19	\N
78	JOD	د.ا	Jordanian Dinar	3	5	5	f	2018-12-13 03:16:19	2018-12-13 03:16:19	\N
79	JPY	¥	Japanese Yen	0	0	0	f	2018-12-13 03:16:19	2018-12-13 03:16:19	\N
80	KES	K Sh	Kenyan Shilling	2	2	2	f	2018-12-13 03:16:19	2018-12-13 03:16:19	\N
81	KGS	som	Som	2	2	2	f	2018-12-13 03:16:20	2018-12-13 03:16:20	\N
82	KHR	CR	Riel	2	2	2	f	2018-12-13 03:16:20	2018-12-13 03:16:20	\N
83	KMF	CF	Comoro Franc	0	2	2	f	2018-12-13 03:16:20	2018-12-13 03:16:20	\N
84	KPW	₩	North Korean Won	2	2	2	f	2018-12-13 03:16:20	2018-12-13 03:16:20	\N
85	KRW	W	Won	0	2	2	f	2018-12-13 03:16:21	2018-12-13 03:16:21	\N
86	KWD	د.ك	Kuwaiti Dinar	3	5	5	f	2018-12-13 03:16:21	2018-12-13 03:16:21	\N
87	KYD	CI$	Cayman Islands Dollar	2	2	2	f	2018-12-13 03:16:21	2018-12-13 03:16:21	\N
88	KZT	〒	Tenge	2	2	2	f	2018-12-13 03:16:22	2018-12-13 03:16:22	\N
89	LAK	₭	Kip	2	2	2	f	2018-12-13 03:16:22	2018-12-13 03:16:22	\N
90	LBP	ل.ل	Lebanese Pound	2	2	2	f	2018-12-13 03:16:22	2018-12-13 03:16:22	\N
91	LKR	SLRs	Sri Lanka Rupee	2	2	2	f	2018-12-13 03:16:22	2018-12-13 03:16:22	\N
92	LRD	$	Liberian Dollar	2	2	2	f	2018-12-13 03:16:23	2018-12-13 03:16:23	\N
93	LTL	Lt	Lithuanian Litas	2	2	2	f	2018-12-13 03:16:23	2018-12-13 03:16:23	\N
94	LVL	ل.د	Latvian Lats	2	2	2	f	2018-12-13 03:16:23	2018-12-13 03:16:23	\N
95	LYD	LD	Libyan Dinar	3	5	5	f	2018-12-13 03:16:23	2018-12-13 03:16:23	\N
96	MAD	د.م.	Moroccan Dirham	2	2	2	f	2018-12-13 03:16:24	2018-12-13 03:16:24	\N
97	MDL	L	Moldovan Leu	2	2	2	f	2018-12-13 03:16:24	2018-12-13 03:16:24	\N
98	MGF	FMG	Malagasy Franc	0	2	2	f	2018-12-13 03:16:24	2018-12-13 03:16:24	\N
99	MKD	ден	Denar	2	2	2	f	2018-12-13 03:16:25	2018-12-13 03:16:25	\N
100	MMK	K	Kyat	2	2	2	f	2018-12-13 03:16:25	2018-12-13 03:16:25	\N
101	MNT	Tug	Tugrik	2	2	2	f	2018-12-13 03:16:25	2018-12-13 03:16:25	\N
102	MOP	P	Pataca	2	2	2	f	2018-12-13 03:16:25	2018-12-13 03:16:25	\N
103	MRO	UM	Ouguiya	2	2	2	f	2018-12-13 03:16:26	2018-12-13 03:16:26	\N
104	MTL	Lm	Maltese Lira	2	2	2	f	2018-12-13 03:16:26	2018-12-13 03:16:26	\N
105	MUR	Mau Rs	Mauritius Rupee	2	2	2	f	2018-12-13 03:16:26	2018-12-13 03:16:26	\N
106	MVR	Rf	Rufiyaa	2	2	2	f	2018-12-13 03:16:26	2018-12-13 03:16:26	\N
107	MWK	MK	Kwacha	2	2	2	f	2018-12-13 03:16:27	2018-12-13 03:16:27	\N
108	MXN	$	Mexican Peso	2	2	2	f	2018-12-13 03:16:27	2018-12-13 03:16:27	\N
109	MYR	RM	Malaysian Ringgit	2	2	2	f	2018-12-13 03:16:27	2018-12-13 03:16:27	\N
110	MZN	MTn	Metical	2	2	2	f	2018-12-13 03:16:28	2018-12-13 03:16:28	\N
111	NGN	₦	Naira	2	2	2	f	2018-12-13 03:16:28	2018-12-13 03:16:28	\N
112	NIO	C$	Cordoba Oro	2	2	2	f	2018-12-13 03:16:28	2018-12-13 03:16:28	\N
113	NLF	Fl	Dutch Guilder	2	2	2	f	2018-12-13 03:16:28	2018-12-13 03:16:28	\N
114	NOK	NKr	Norwegian Krone	2	2	2	f	2018-12-13 03:16:29	2018-12-13 03:16:29	\N
115	NPR	NRs	Nepalese Rupee	2	2	2	f	2018-12-13 03:16:29	2018-12-13 03:16:29	\N
116	NZD	$	New Zealand Dollar	2	2	2	f	2018-12-13 03:16:29	2018-12-13 03:16:29	\N
117	OMR	ر.ع.	Rial Omani	3	5	5	f	2018-12-13 03:16:30	2018-12-13 03:16:30	\N
118	PAB	B/.	Balboa	2	2	2	f	2018-12-13 03:16:30	2018-12-13 03:16:30	\N
119	PEN	S/.	Nuevo Sol	2	2	2	f	2018-12-13 03:16:30	2018-12-13 03:16:30	\N
120	PGK	K	Kina	2	2	2	f	2018-12-13 03:16:30	2018-12-13 03:16:30	\N
121	PHP	₱	Philippine Peso	2	2	2	f	2018-12-13 03:16:31	2018-12-13 03:16:31	\N
122	PKR	Rs	Pakistan Rupee	2	2	2	f	2018-12-13 03:16:31	2018-12-13 03:16:31	\N
123	PLN	zł	Zloty	2	2	2	f	2018-12-13 03:16:31	2018-12-13 03:16:31	\N
124	PTE	Es	Portugese Escudo	0	0	0	f	2018-12-13 03:16:31	2018-12-13 03:16:31	\N
125	PYG	₲	Guarani	0	2	2	f	2018-12-13 03:16:32	2018-12-13 03:16:32	\N
126	QAR	ر.ق	Qatari Rial	2	2	2	f	2018-12-13 03:16:32	2018-12-13 03:16:32	\N
127	ROL	L	Leu	2	2	2	f	2018-12-13 03:16:32	2018-12-13 03:16:32	\N
128	RON	L	Romanian Leu	2	2	2	f	2018-12-13 03:16:33	2018-12-13 03:16:33	\N
129	RSD	дин	Serbian Dinar	2	2	2	f	2018-12-13 03:16:33	2018-12-13 03:16:33	\N
130	RUB	руб.	Russian Ruble	2	2	2	f	2018-12-13 03:16:33	2018-12-13 03:16:33	\N
131	RWF	RF	Rwanda Franc	0	2	2	f	2018-12-13 03:16:33	2018-12-13 03:16:33	\N
132	SAR	ر.س	Saudi Riyal	2	2	2	f	2018-12-13 03:16:34	2018-12-13 03:16:34	\N
133	SBD	SI$	Solomon Islands Dollar	2	2	2	f	2018-12-13 03:16:34	2018-12-13 03:16:34	\N
134	SCR	SR	Seychelles Rupee	2	2	2	f	2018-12-13 03:16:34	2018-12-13 03:16:34	\N
135	SDG	£	Sudanese Pound	2	2	2	f	2018-12-13 03:16:34	2018-12-13 03:16:34	\N
136	SEK	kr	Swedish Krona	2	2	2	f	2018-12-13 03:16:35	2018-12-13 03:16:35	\N
137	SFR	SFr	Swiss Franc	2	2	2	f	2018-12-13 03:16:35	2018-12-13 03:16:35	\N
138	SGD	S$	Singapore Dollar	2	2	2	f	2018-12-13 03:16:35	2018-12-13 03:16:35	\N
139	SHP	£S	Saint Helena Pound	2	2	2	f	2018-12-13 03:16:36	2018-12-13 03:16:36	\N
140	SKK	Sk	Slovak Koruna	2	2	2	f	2018-12-13 03:16:36	2018-12-13 03:16:36	\N
141	SLL	Le	Leone	2	2	2	f	2018-12-13 03:16:36	2018-12-13 03:16:36	\N
142	SOS	So. Sh.	Somali Shilling	2	2	2	f	2018-12-13 03:16:36	2018-12-13 03:16:36	\N
143	SRD	$	Suriname Dollar	2	2	2	f	2018-12-13 03:16:37	2018-12-13 03:16:37	\N
144	STD	Db	Dobra	2	2	2	f	2018-12-13 03:16:37	2018-12-13 03:16:37	\N
145	SVC	¢	El Salvador Colon	2	2	2	f	2018-12-13 03:16:37	2018-12-13 03:16:37	\N
146	SYP	£S	Syrian Pound	2	2	2	f	2018-12-13 03:16:37	2018-12-13 03:16:37	\N
147	SZL	L	Lilangeni	2	2	2	f	2018-12-13 03:16:38	2018-12-13 03:16:38	\N
148	THB	฿	Baht	2	2	2	f	2018-12-13 03:16:38	2018-12-13 03:16:38	\N
149	TJS	SM	Somoni	2	2	2	f	2018-12-13 03:16:38	2018-12-13 03:16:38	\N
150	TMM	m	Manat	2	2	2	f	2018-12-13 03:16:39	2018-12-13 03:16:39	\N
151	TND	د.ت	Tunisian Dinar	3	5	5	f	2018-12-13 03:16:39	2018-12-13 03:16:39	\N
152	TOP	T$	Pa’anga	2	2	2	f	2018-12-13 03:16:39	2018-12-13 03:16:39	\N
153	TPE	$	Timor Escudo	0	2	2	f	2018-12-13 03:16:39	2018-12-13 03:16:39	\N
154	TRY	YTL	Turkish Lira	0	2	2	f	2018-12-13 03:16:40	2018-12-13 03:16:40	\N
155	TTD	TT$	Trinidad and Tobago Dollar	2	2	2	f	2018-12-13 03:16:40	2018-12-13 03:16:40	\N
156	TWD	NT$	New Taiwan Dollar	2	2	2	f	2018-12-13 03:16:40	2018-12-13 03:16:40	\N
157	TZS	TSh	Tanzanian Shilling	2	2	2	f	2018-12-13 03:16:40	2018-12-13 03:16:40	\N
158	UAH	₴	Hryvnia	2	2	2	f	2018-12-13 03:16:41	2018-12-13 03:16:41	\N
159	UGX	USh	Uganda Shilling	2	2	2	f	2018-12-13 03:16:41	2018-12-13 03:16:41	\N
160	USD	$	US Dollar	2	2	2	f	2018-12-13 03:16:41	2018-12-13 03:16:41	\N
161	UYU	$U	Peso Uruguayo	2	2	2	f	2018-12-13 03:16:42	2018-12-13 03:16:42	\N
162	UZS	som	Uzbekistan Sum	2	2	2	f	2018-12-13 03:16:42	2018-12-13 03:16:42	\N
163	VEB	Bs	Bolivar	2	2	2	f	2018-12-13 03:16:42	2018-12-13 03:16:42	\N
164	VND	₫	Dong	2	2	2	f	2018-12-13 03:16:42	2018-12-13 03:16:42	\N
165	VUV	VT	Vatu	0	2	2	f	2018-12-13 03:16:43	2018-12-13 03:16:43	\N
166	WST	WS$	Tala	2	2	2	f	2018-12-13 03:16:43	2018-12-13 03:16:43	\N
167	XAF	CFAF	CFA Franc BEAC	0	2	2	f	2018-12-13 03:16:43	2018-12-13 03:16:43	\N
168	XCD	EC$	East Caribbean Dollar	2	2	2	f	2018-12-13 03:16:43	2018-12-13 03:16:43	\N
169	XOF	CFAF	CFA Franc BCEAO	0	2	2	f	2018-12-13 03:16:44	2018-12-13 03:16:44	\N
170	XPF	CFPF	CFP Franc	0	2	2	f	2018-12-13 03:16:44	2018-12-13 03:16:44	\N
171	YER	﷼	Yemeni Rial	2	2	2	f	2018-12-13 03:16:44	2018-12-13 03:16:44	\N
172	YUM	Din	Yugoslavian Dinar	2	2	2	f	2018-12-13 03:16:45	2018-12-13 03:16:45	\N
173	ZAR	R	Rand	2	2	2	f	2018-12-13 03:16:45	2018-12-13 03:16:45	\N
174	ZMK	ZK	Kwacha	2	2	2	f	2018-12-13 03:16:45	2018-12-13 03:16:45	\N
175	ZWD	Z$	Zimbabwe Dollar	2	2	2	f	2018-12-13 03:16:45	2018-12-13 03:16:45	\N
1	AED	د.إ	UAE Dirham	2	2	2	f	2018-12-13 03:15:57	2018-12-13 03:15:57	\N
\.


--
-- TOC entry 4450 (class 0 OID 16540)
-- Dependencies: 241
-- Data for Name: SYS_Region; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Region" (id, name, region_code, region_lang_key, country_id, created_at, updated_at, deleted_at) FROM stdin;
1	Alberta		\N	36	2018-12-13 03:17:53	2018-12-13 03:17:53	\N
2	British Columbia		\N	36	2018-12-13 03:17:53	2018-12-13 03:17:53	\N
3	Manitoba		\N	36	2018-12-13 03:17:53	2018-12-13 03:17:53	\N
4	New Brunswick		\N	36	2018-12-13 03:17:53	2018-12-13 03:17:53	\N
5	Newfoundland and Labrador		\N	36	2018-12-13 03:17:54	2018-12-13 03:17:54	\N
6	Nova Scotia		\N	36	2018-12-13 03:17:54	2018-12-13 03:17:54	\N
7	Northwest Territories		\N	36	2018-12-13 03:17:54	2018-12-13 03:17:54	\N
8	Nunavut		\N	36	2018-12-13 03:17:55	2018-12-13 03:17:55	\N
9	Ontario		\N	36	2018-12-13 03:17:55	2018-12-13 03:17:55	\N
10	Prince Edward Island		\N	36	2018-12-13 03:17:55	2018-12-13 03:17:55	\N
11	Québec		\N	36	2018-12-13 03:17:55	2018-12-13 03:17:55	\N
12	Saskatchewan		\N	36	2018-12-13 03:17:56	2018-12-13 03:17:56	\N
13	Yukon		\N	36	2018-12-13 03:17:56	2018-12-13 03:17:56	\N
14	Avon		\N	74	2018-12-13 03:17:56	2018-12-13 03:17:56	\N
15	Befordshire		\N	74	2018-12-13 03:17:57	2018-12-13 03:17:57	\N
16	Buckinghamshire		\N	74	2018-12-13 03:17:57	2018-12-13 03:17:57	\N
17	Cambridgeshire		\N	74	2018-12-13 03:17:57	2018-12-13 03:17:57	\N
18	Cheshire		\N	74	2018-12-13 03:17:57	2018-12-13 03:17:57	\N
19	Cleveland		\N	74	2018-12-13 03:17:58	2018-12-13 03:17:58	\N
20	Cornwall		\N	74	2018-12-13 03:17:58	2018-12-13 03:17:58	\N
21	Cumbria		\N	74	2018-12-13 03:17:58	2018-12-13 03:17:58	\N
22	Derbyshire		\N	74	2018-12-13 03:17:58	2018-12-13 03:17:58	\N
23	Devon		\N	74	2018-12-13 03:17:59	2018-12-13 03:17:59	\N
24	Dorset		\N	74	2018-12-13 03:17:59	2018-12-13 03:17:59	\N
25	Durham		\N	74	2018-12-13 03:17:59	2018-12-13 03:17:59	\N
26	East Sussex		\N	74	2018-12-13 03:18:00	2018-12-13 03:18:00	\N
27	Essex		\N	74	2018-12-13 03:18:00	2018-12-13 03:18:00	\N
28	Gloucestershire		\N	74	2018-12-13 03:18:00	2018-12-13 03:18:00	\N
29	Greater London		\N	74	2018-12-13 03:18:00	2018-12-13 03:18:00	\N
30	Greater Manchester		\N	74	2018-12-13 03:18:01	2018-12-13 03:18:01	\N
31	Hampshire		\N	74	2018-12-13 03:18:01	2018-12-13 03:18:01	\N
32	Herefordshire		\N	74	2018-12-13 03:18:01	2018-12-13 03:18:01	\N
33	Hertfordshire		\N	74	2018-12-13 03:18:02	2018-12-13 03:18:02	\N
34	Humberside		\N	74	2018-12-13 03:18:02	2018-12-13 03:18:02	\N
35	Isle of Wight		\N	74	2018-12-13 03:18:02	2018-12-13 03:18:02	\N
36	Kent		\N	74	2018-12-13 03:18:02	2018-12-13 03:18:02	\N
37	Lancashire		\N	74	2018-12-13 03:18:03	2018-12-13 03:18:03	\N
38	Lincolnshire		\N	74	2018-12-13 03:18:03	2018-12-13 03:18:03	\N
39	Merseyside		\N	74	2018-12-13 03:18:03	2018-12-13 03:18:03	\N
40	Norfolk		\N	74	2018-12-13 03:18:03	2018-12-13 03:18:03	\N
41	Northamtonshire		\N	74	2018-12-13 03:18:04	2018-12-13 03:18:04	\N
42	Northumberland		\N	74	2018-12-13 03:18:04	2018-12-13 03:18:04	\N
43	North Yorkshire		\N	74	2018-12-13 03:18:04	2018-12-13 03:18:04	\N
44	Nottinghamshire		\N	74	2018-12-13 03:18:05	2018-12-13 03:18:05	\N
45	Oxfordshire		\N	74	2018-12-13 03:18:05	2018-12-13 03:18:05	\N
46	Shropshire		\N	74	2018-12-13 03:18:05	2018-12-13 03:18:05	\N
47	Somerset		\N	74	2018-12-13 03:18:06	2018-12-13 03:18:06	\N
48	South Yorkshire		\N	74	2018-12-13 03:18:06	2018-12-13 03:18:06	\N
49	Staffordshire		\N	74	2018-12-13 03:18:06	2018-12-13 03:18:06	\N
50	Suffolk		\N	74	2018-12-13 03:18:06	2018-12-13 03:18:06	\N
51	Surrey		\N	74	2018-12-13 03:18:07	2018-12-13 03:18:07	\N
52	Tyne and Wear		\N	74	2018-12-13 03:18:07	2018-12-13 03:18:07	\N
53	Warwickshire		\N	74	2018-12-13 03:18:07	2018-12-13 03:18:07	\N
54	West Midlands		\N	74	2018-12-13 03:18:08	2018-12-13 03:18:08	\N
55	West Sussex		\N	74	2018-12-13 03:18:08	2018-12-13 03:18:08	\N
56	West Yorkshire		\N	74	2018-12-13 03:18:08	2018-12-13 03:18:08	\N
57	Wiltshire		\N	74	2018-12-13 03:18:08	2018-12-13 03:18:08	\N
58	Worcestershire		\N	74	2018-12-13 03:18:09	2018-12-13 03:18:09	\N
59	Borders		\N	74	2018-12-13 03:18:09	2018-12-13 03:18:09	\N
60	Central (Scotland)		\N	74	2018-12-13 03:18:09	2018-12-13 03:18:09	\N
61	Dumfries and Galloway		\N	74	2018-12-13 03:18:09	2018-12-13 03:18:09	\N
62	Fife		\N	74	2018-12-13 03:18:10	2018-12-13 03:18:10	\N
63	Grampian		\N	74	2018-12-13 03:18:10	2018-12-13 03:18:10	\N
64	Highland		\N	74	2018-12-13 03:18:10	2018-12-13 03:18:10	\N
65	Lothian		\N	74	2018-12-13 03:18:11	2018-12-13 03:18:11	\N
66	Orkney		\N	74	2018-12-13 03:18:11	2018-12-13 03:18:11	\N
67	Shetland		\N	74	2018-12-13 03:18:11	2018-12-13 03:18:11	\N
68	Strathclyde		\N	74	2018-12-13 03:18:11	2018-12-13 03:18:11	\N
69	Tayside		\N	74	2018-12-13 03:18:12	2018-12-13 03:18:12	\N
70	Western Isles		\N	74	2018-12-13 03:18:12	2018-12-13 03:18:12	\N
71	Clwyd		\N	74	2018-12-13 03:18:12	2018-12-13 03:18:12	\N
72	Dyfed		\N	74	2018-12-13 03:18:12	2018-12-13 03:18:12	\N
73	Gwent		\N	74	2018-12-13 03:18:13	2018-12-13 03:18:13	\N
74	Gwynedd		\N	74	2018-12-13 03:18:13	2018-12-13 03:18:13	\N
75	Mid Glamorgan		\N	74	2018-12-13 03:18:13	2018-12-13 03:18:13	\N
76	Powys		\N	74	2018-12-13 03:18:14	2018-12-13 03:18:14	\N
77	South Glamorgan		\N	74	2018-12-13 03:18:14	2018-12-13 03:18:14	\N
78	West Glamorgan		\N	74	2018-12-13 03:18:14	2018-12-13 03:18:14	\N
79	Antrim (NI)		\N	74	2018-12-13 03:18:14	2018-12-13 03:18:14	\N
80	Ards (NI)		\N	74	2018-12-13 03:18:15	2018-12-13 03:18:15	\N
81	Armagh (NI)		\N	74	2018-12-13 03:18:15	2018-12-13 03:18:15	\N
82	Ballymena (NI)		\N	74	2018-12-13 03:18:15	2018-12-13 03:18:15	\N
83	Ballymoney (NI)		\N	74	2018-12-13 03:18:16	2018-12-13 03:18:16	\N
84	Banbridge (NI)		\N	74	2018-12-13 03:18:16	2018-12-13 03:18:16	\N
85	Belfast (NI)		\N	74	2018-12-13 03:18:16	2018-12-13 03:18:16	\N
86	Carrickfergus (NI)		\N	74	2018-12-13 03:18:16	2018-12-13 03:18:16	\N
87	Castlereagh (NI)		\N	74	2018-12-13 03:18:17	2018-12-13 03:18:17	\N
88	Coleraine (NI)		\N	74	2018-12-13 03:18:17	2018-12-13 03:18:17	\N
89	Cookstown (NI)		\N	74	2018-12-13 03:18:18	2018-12-13 03:18:18	\N
90	Craigavon (NI)		\N	74	2018-12-13 03:18:18	2018-12-13 03:18:18	\N
91	Down (NI)		\N	74	2018-12-13 03:18:18	2018-12-13 03:18:18	\N
92	Dungannon (NI)		\N	74	2018-12-13 03:18:18	2018-12-13 03:18:18	\N
93	Fermanagh (NI)		\N	74	2018-12-13 03:18:19	2018-12-13 03:18:19	\N
94	Larne (NI)		\N	74	2018-12-13 03:18:19	2018-12-13 03:18:19	\N
95	Limavady (NI)		\N	74	2018-12-13 03:18:19	2018-12-13 03:18:19	\N
96	Lisburn (NI)		\N	74	2018-12-13 03:18:20	2018-12-13 03:18:20	\N
97	Londonderry (NI)		\N	74	2018-12-13 03:18:20	2018-12-13 03:18:20	\N
98	Magherafelt (NI)		\N	74	2018-12-13 03:18:20	2018-12-13 03:18:20	\N
99	Moyle (NI)		\N	74	2018-12-13 03:18:20	2018-12-13 03:18:20	\N
100	Newry and Mourne (NI)		\N	74	2018-12-13 03:18:21	2018-12-13 03:18:21	\N
101	Newtonabbey (NI)		\N	74	2018-12-13 03:18:21	2018-12-13 03:18:21	\N
102	North Down (NI)		\N	74	2018-12-13 03:18:21	2018-12-13 03:18:21	\N
103	Omagh (NI)		\N	74	2018-12-13 03:18:21	2018-12-13 03:18:21	\N
104	Strabane (NI)		\N	74	2018-12-13 03:18:22	2018-12-13 03:18:22	\N
105	Alderney (CI)		\N	74	2018-12-13 03:18:22	2018-12-13 03:18:22	\N
106	Guernsey (CI)		\N	74	2018-12-13 03:18:22	2018-12-13 03:18:22	\N
107	Jersey (CI)		\N	74	2018-12-13 03:18:23	2018-12-13 03:18:23	\N
108	Isle of Man		\N	74	2018-12-13 03:18:23	2018-12-13 03:18:23	\N
109	Connecticut		\N	224	2018-12-13 03:18:23	2018-12-13 03:18:23	\N
110	California		\N	224	2018-12-13 03:18:23	2018-12-13 03:18:23	\N
111	Massachusetts		\N	224	2018-12-13 03:18:24	2018-12-13 03:18:24	\N
112	Maine		\N	224	2018-12-13 03:18:24	2018-12-13 03:18:24	\N
113	New Hampshire		\N	224	2018-12-13 03:18:24	2018-12-13 03:18:24	\N
114	Rhode Island		\N	224	2018-12-13 03:18:24	2018-12-13 03:18:24	\N
115	New York		\N	224	2018-12-13 03:18:25	2018-12-13 03:18:25	\N
116	New Jersey		\N	224	2018-12-13 03:18:25	2018-12-13 03:18:25	\N
117	Pennsylvania		\N	224	2018-12-13 03:18:25	2018-12-13 03:18:25	\N
118	Virginia		\N	224	2018-12-13 03:18:26	2018-12-13 03:18:26	\N
119	West Virginia		\N	224	2018-12-13 03:18:26	2018-12-13 03:18:26	\N
120	Florida		\N	224	2018-12-13 03:18:26	2018-12-13 03:18:26	\N
121	Alabama		\N	224	2018-12-13 03:18:26	2018-12-13 03:18:26	\N
122	Deleware		\N	224	2018-12-13 03:18:27	2018-12-13 03:18:27	\N
123	Washington		\N	224	2018-12-13 03:18:27	2018-12-13 03:18:27	\N
124	Alaska		\N	224	2018-12-13 03:18:28	2018-12-13 03:18:28	\N
125	Tennessee		\N	224	2018-12-13 03:18:28	2018-12-13 03:18:28	\N
126	Georgia		\N	224	2018-12-13 03:18:28	2018-12-13 03:18:28	\N
127	North Dakota		\N	224	2018-12-13 03:18:29	2018-12-13 03:18:29	\N
128	South Dakato		\N	224	2018-12-13 03:18:29	2018-12-13 03:18:29	\N
129	Michigan		\N	224	2018-12-13 03:18:29	2018-12-13 03:18:29	\N
130	Iowa		\N	224	2018-12-13 03:18:29	2018-12-13 03:18:29	\N
131	Indiana		\N	224	2018-12-13 03:18:30	2018-12-13 03:18:30	\N
132	Ohio		\N	224	2018-12-13 03:18:30	2018-12-13 03:18:30	\N
133	Illinois		\N	224	2018-12-13 03:18:30	2018-12-13 03:18:30	\N
134	South Carolina		\N	224	2018-12-13 03:18:31	2018-12-13 03:18:31	\N
135	North Carolina		\N	224	2018-12-13 03:18:31	2018-12-13 03:18:31	\N
136	Kentucky		\N	224	2018-12-13 03:18:31	2018-12-13 03:18:31	\N
137	Lousiana		\N	224	2018-12-13 03:18:31	2018-12-13 03:18:31	\N
138	Texas		\N	224	2018-12-13 03:18:32	2018-12-13 03:18:32	\N
139	Oklahoma		\N	224	2018-12-13 03:18:32	2018-12-13 03:18:32	\N
140	Mississippi		\N	224	2018-12-13 03:18:32	2018-12-13 03:18:32	\N
141	Montana		\N	224	2018-12-13 03:18:32	2018-12-13 03:18:32	\N
142	Missouri		\N	224	2018-12-13 03:18:33	2018-12-13 03:18:33	\N
143	Kansas		\N	224	2018-12-13 03:18:33	2018-12-13 03:18:33	\N
144	Nebraska		\N	224	2018-12-13 03:18:33	2018-12-13 03:18:33	\N
145	Wyoming		\N	224	2018-12-13 03:18:34	2018-12-13 03:18:34	\N
146	New Mexico		\N	224	2018-12-13 03:18:34	2018-12-13 03:18:34	\N
147	Oregon		\N	224	2018-12-13 03:18:34	2018-12-13 03:18:34	\N
148	Arkansas		\N	224	2018-12-13 03:18:34	2018-12-13 03:18:34	\N
149	Arizona		\N	224	2018-12-13 03:18:35	2018-12-13 03:18:35	\N
150	Utah		\N	224	2018-12-13 03:18:35	2018-12-13 03:18:35	\N
151	Idaho		\N	224	2018-12-13 03:18:35	2018-12-13 03:18:35	\N
152	Nevada		\N	224	2018-12-13 03:18:35	2018-12-13 03:18:35	\N
153	Colorado		\N	224	2018-12-13 03:18:36	2018-12-13 03:18:36	\N
154	Hawaii		\N	224	2018-12-13 03:18:36	2018-12-13 03:18:36	\N
155	Minnesota		\N	224	2018-12-13 03:18:36	2018-12-13 03:18:36	\N
156	Wisconsin		\N	224	2018-12-13 03:18:37	2018-12-13 03:18:37	\N
157	Maryland		\N	224	2018-12-13 03:18:37	2018-12-13 03:18:37	\N
158	Vermont		\N	224	2018-12-13 03:18:37	2018-12-13 03:18:37	\N
159	Ahuachapán		\N	202	2018-12-13 03:18:37	2018-12-13 03:18:37	\N
160	Cabañas		\N	202	2018-12-13 03:18:38	2018-12-13 03:18:38	\N
161	Chalatenango		\N	202	2018-12-13 03:18:38	2018-12-13 03:18:38	\N
162	Cuscatlán		\N	202	2018-12-13 03:18:38	2018-12-13 03:18:38	\N
163	La Libertad		\N	202	2018-12-13 03:18:38	2018-12-13 03:18:38	\N
164	La Paz		\N	202	2018-12-13 03:18:39	2018-12-13 03:18:39	\N
165	La Unión		\N	202	2018-12-13 03:18:39	2018-12-13 03:18:39	\N
166	Morazán		\N	202	2018-12-13 03:18:39	2018-12-13 03:18:39	\N
167	Santa Ana		\N	202	2018-12-13 03:18:40	2018-12-13 03:18:40	\N
168	San Miguel		\N	202	2018-12-13 03:18:40	2018-12-13 03:18:40	\N
169	San Salvador		\N	202	2018-12-13 03:18:40	2018-12-13 03:18:40	\N
170	San Vicente		\N	202	2018-12-13 03:18:40	2018-12-13 03:18:40	\N
171	Sonsonate		\N	202	2018-12-13 03:18:41	2018-12-13 03:18:41	\N
172	Usulután		\N	202	2018-12-13 03:18:41	2018-12-13 03:18:41	\N
\.


--
-- TOC entry 4452 (class 0 OID 16545)
-- Dependencies: 243
-- Data for Name: SYS_Voucher_Type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SYS_Voucher_Type" (id, name, lang_key, key, created_at, updated_at, deleted_at) FROM stdin;
\.


--
-- TOC entry 4454 (class 0 OID 16550)
-- Dependencies: 245
-- Data for Name: cache; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cache (key, value, expiration) FROM stdin;
\.


--
-- TOC entry 4455 (class 0 OID 16556)
-- Dependencies: 246
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (migration, batch) FROM stdin;
2014_02_28_042556_create_system_tables	1
2014_02_28_050447_create_organization_tables	1
2014_02_28_050455_create_security_tables	1
2015_01_25_182642_create_acct_system_tables	1
2016_09_21_201625_create_file_table	1
2017_01_11_023422_create_sessions_table	1
2017_01_17_173255_create_security_file_table	1
2017_02_01_042031_create_cache_table	1
2018_12_11_014701_create_oadh_part_1	2
2019_02_26_014701_create_oadh_part_2	3
2019_03_13_014701_create_oadh_part_3	4
2019_03_18_014701_create_oadh_part_4	5
2019_04_07_014701_create_oadh_part_5	6
\.


--
-- TOC entry 4456 (class 0 OID 16559)
-- Dependencies: 247
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sessions (id, user_id, ip_address, user_agent, payload, last_activity) FROM stdin;
\.


--
-- TOC entry 4720 (class 0 OID 0)
-- Dependencies: 197
-- Name: FILE_File_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."FILE_File_id_seq"', 13, true);


--
-- TOC entry 4721 (class 0 OID 0)
-- Dependencies: 258
-- Name: OADH_Context_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_Context_id_seq"', 1, false);


--
-- TOC entry 4722 (class 0 OID 0)
-- Dependencies: 250
-- Name: OADH_Digital_Media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_Digital_Media_id_seq"', 47, true);


--
-- TOC entry 4723 (class 0 OID 0)
-- Dependencies: 296
-- Name: OADH_FREE_Accused_Liberty_Deprivation_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Accused_Liberty_Deprivation_Temp_id_seq"', 1, false);


--
-- TOC entry 4724 (class 0 OID 0)
-- Dependencies: 298
-- Name: OADH_FREE_Accused_Liberty_Deprivation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Accused_Liberty_Deprivation_id_seq"', 1, false);


--
-- TOC entry 4725 (class 0 OID 0)
-- Dependencies: 312
-- Name: OADH_FREE_Acute_Diseases_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Acute_Diseases_Temp_id_seq"', 1, false);


--
-- TOC entry 4726 (class 0 OID 0)
-- Dependencies: 314
-- Name: OADH_FREE_Acute_Diseases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Acute_Diseases_id_seq"', 1, false);


--
-- TOC entry 4727 (class 0 OID 0)
-- Dependencies: 288
-- Name: OADH_FREE_Capture_Orders_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Capture_Orders_Temp_id_seq"', 1, false);


--
-- TOC entry 4728 (class 0 OID 0)
-- Dependencies: 290
-- Name: OADH_FREE_Capture_Orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Capture_Orders_id_seq"', 1, false);


--
-- TOC entry 4729 (class 0 OID 0)
-- Dependencies: 316
-- Name: OADH_FREE_Chronic_Diseases_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Chronic_Diseases_Temp_id_seq"', 1, false);


--
-- TOC entry 4730 (class 0 OID 0)
-- Dependencies: 318
-- Name: OADH_FREE_Chronic_Diseases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Chronic_Diseases_id_seq"', 1, false);


--
-- TOC entry 4731 (class 0 OID 0)
-- Dependencies: 276
-- Name: OADH_FREE_Criminal_Cases_Trials_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Criminal_Cases_Trials_Temp_id_seq"', 1, false);


--
-- TOC entry 4732 (class 0 OID 0)
-- Dependencies: 278
-- Name: OADH_FREE_Criminal_Cases_Trials_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Criminal_Cases_Trials_id_seq"', 1, false);


--
-- TOC entry 4733 (class 0 OID 0)
-- Dependencies: 308
-- Name: OADH_FREE_Deprived_Persons_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Deprived_Persons_Temp_id_seq"', 1, false);


--
-- TOC entry 4734 (class 0 OID 0)
-- Dependencies: 310
-- Name: OADH_FREE_Deprived_Persons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Deprived_Persons_id_seq"', 1, false);


--
-- TOC entry 4735 (class 0 OID 0)
-- Dependencies: 292
-- Name: OADH_FREE_Disappearances_Victims_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Disappearances_Victims_Temp_id_seq"', 1, false);


--
-- TOC entry 4736 (class 0 OID 0)
-- Dependencies: 294
-- Name: OADH_FREE_Disappearances_Victims_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Disappearances_Victims_id_seq"', 1, false);


--
-- TOC entry 4737 (class 0 OID 0)
-- Dependencies: 304
-- Name: OADH_FREE_Forced_Disappearance_Temps_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Forced_Disappearance_Temps_id_seq"', 1, false);


--
-- TOC entry 4738 (class 0 OID 0)
-- Dependencies: 306
-- Name: OADH_FREE_Forced_Disappearances_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Forced_Disappearances_id_seq"', 1, false);


--
-- TOC entry 4739 (class 0 OID 0)
-- Dependencies: 268
-- Name: OADH_FREE_Habeas_Corpus_Request_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Habeas_Corpus_Request_Temp_id_seq"', 6480, true);


--
-- TOC entry 4740 (class 0 OID 0)
-- Dependencies: 270
-- Name: OADH_FREE_Habeas_Corpus_Request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Habeas_Corpus_Request_id_seq"', 11340, true);


--
-- TOC entry 4741 (class 0 OID 0)
-- Dependencies: 320
-- Name: OADH_FREE_Jails_Occupation_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Jails_Occupation_Temp_id_seq"', 1, false);


--
-- TOC entry 4742 (class 0 OID 0)
-- Dependencies: 322
-- Name: OADH_FREE_Jails_Occupation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Jails_Occupation_id_seq"', 1, false);


--
-- TOC entry 4743 (class 0 OID 0)
-- Dependencies: 280
-- Name: OADH_FREE_Mass_Trials_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Mass_Trials_Temp_id_seq"', 1, false);


--
-- TOC entry 4744 (class 0 OID 0)
-- Dependencies: 282
-- Name: OADH_FREE_Mass_Trials_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Mass_Trials_id_seq"', 1, false);


--
-- TOC entry 4745 (class 0 OID 0)
-- Dependencies: 272
-- Name: OADH_FREE_People_Detained_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_People_Detained_Temp_id_seq"', 1, false);


--
-- TOC entry 4746 (class 0 OID 0)
-- Dependencies: 274
-- Name: OADH_FREE_People_Detained_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_People_Detained_id_seq"', 1, false);


--
-- TOC entry 4747 (class 0 OID 0)
-- Dependencies: 284
-- Name: OADH_FREE_Procedural_Fraud_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Procedural_Fraud_Temp_id_seq"', 1, false);


--
-- TOC entry 4748 (class 0 OID 0)
-- Dependencies: 286
-- Name: OADH_FREE_Procedural_Fraud_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Procedural_Fraud_id_seq"', 1, false);


--
-- TOC entry 4749 (class 0 OID 0)
-- Dependencies: 300
-- Name: OADH_FREE_Victims_Liberty_Deprivation_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Victims_Liberty_Deprivation_Temp_id_seq"', 1, false);


--
-- TOC entry 4750 (class 0 OID 0)
-- Dependencies: 302
-- Name: OADH_FREE_Victims_Liberty_Deprivation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_FREE_Victims_Liberty_Deprivation_id_seq"', 1, false);


--
-- TOC entry 4751 (class 0 OID 0)
-- Dependencies: 260
-- Name: OADH_Human_Right_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_Human_Right_id_seq"', 142, true);


--
-- TOC entry 4752 (class 0 OID 0)
-- Dependencies: 380
-- Name: OADH_INT_Age_Injuries_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Age_Injuries_Temp_id_seq"', 1, false);


--
-- TOC entry 4753 (class 0 OID 0)
-- Dependencies: 382
-- Name: OADH_INT_Age_Injuries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Age_Injuries_id_seq"', 1, false);


--
-- TOC entry 4754 (class 0 OID 0)
-- Dependencies: 372
-- Name: OADH_INT_Age_Sexual_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Age_Sexual_Temp_id_seq"', 1, false);


--
-- TOC entry 4755 (class 0 OID 0)
-- Dependencies: 374
-- Name: OADH_INT_Age_Sexual_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Age_Sexual_id_seq"', 1, false);


--
-- TOC entry 4756 (class 0 OID 0)
-- Dependencies: 344
-- Name: OADH_INT_Age_Trafficking_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Age_Trafficking_Temp_id_seq"', 1, false);


--
-- TOC entry 4757 (class 0 OID 0)
-- Dependencies: 346
-- Name: OADH_INT_Age_Trafficking_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Age_Trafficking_id_seq"', 1, false);


--
-- TOC entry 4758 (class 0 OID 0)
-- Dependencies: 352
-- Name: OADH_INT_Age_Women_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Age_Women_Temp_id_seq"', 1, false);


--
-- TOC entry 4759 (class 0 OID 0)
-- Dependencies: 354
-- Name: OADH_INT_Age_Women_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Age_Women_id_seq"', 1, false);


--
-- TOC entry 4760 (class 0 OID 0)
-- Dependencies: 388
-- Name: OADH_INT_Agent_Investigation_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Agent_Investigation_Temp_id_seq"', 1, false);


--
-- TOC entry 4761 (class 0 OID 0)
-- Dependencies: 390
-- Name: OADH_INT_Agent_Investigation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Agent_Investigation_id_seq"', 1, false);


--
-- TOC entry 4762 (class 0 OID 0)
-- Dependencies: 328
-- Name: OADH_INT_Crimes_Accused_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Crimes_Accused_Temp_id_seq"', 1, false);


--
-- TOC entry 4763 (class 0 OID 0)
-- Dependencies: 330
-- Name: OADH_INT_Crimes_Accused_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Crimes_Accused_id_seq"', 1, false);


--
-- TOC entry 4764 (class 0 OID 0)
-- Dependencies: 324
-- Name: OADH_INT_Crimes_Victims_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Crimes_Victims_Temp_id_seq"', 1, false);


--
-- TOC entry 4765 (class 0 OID 0)
-- Dependencies: 326
-- Name: OADH_INT_Crimes_Victims_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Crimes_Victims_id_seq"', 1, false);


--
-- TOC entry 4766 (class 0 OID 0)
-- Dependencies: 368
-- Name: OADH_INT_Gender_Sexual_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Gender_Sexual_Temp_id_seq"', 1, false);


--
-- TOC entry 4767 (class 0 OID 0)
-- Dependencies: 370
-- Name: OADH_INT_Gender_Sexual_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Gender_Sexual_id_seq"', 1, false);


--
-- TOC entry 4768 (class 0 OID 0)
-- Dependencies: 336
-- Name: OADH_INT_Gender_Trafficking_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Gender_Trafficking_Temp_id_seq"', 1, false);


--
-- TOC entry 4769 (class 0 OID 0)
-- Dependencies: 338
-- Name: OADH_INT_Gender_Trafficking_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Gender_Trafficking_id_seq"', 1, false);


--
-- TOC entry 4770 (class 0 OID 0)
-- Dependencies: 392
-- Name: OADH_INT_Integrity_Violation_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Integrity_Violation_Temp_id_seq"', 1, false);


--
-- TOC entry 4771 (class 0 OID 0)
-- Dependencies: 394
-- Name: OADH_INT_Integrity_Violation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Integrity_Violation_id_seq"', 1, false);


--
-- TOC entry 4772 (class 0 OID 0)
-- Dependencies: 348
-- Name: OADH_INT_Movement_Freedom_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Movement_Freedom_Temp_id_seq"', 1, false);


--
-- TOC entry 4773 (class 0 OID 0)
-- Dependencies: 350
-- Name: OADH_INT_Movement_Freedom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Movement_Freedom_id_seq"', 1, false);


--
-- TOC entry 4774 (class 0 OID 0)
-- Dependencies: 384
-- Name: OADH_INT_Schedule_Injuries_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Schedule_Injuries_Temp_id_seq"', 1, false);


--
-- TOC entry 4775 (class 0 OID 0)
-- Dependencies: 386
-- Name: OADH_INT_Schedule_Injuries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Schedule_Injuries_id_seq"', 1, false);


--
-- TOC entry 4776 (class 0 OID 0)
-- Dependencies: 376
-- Name: OADH_INT_State_Injuries_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_State_Injuries_Temp_id_seq"', 1, false);


--
-- TOC entry 4777 (class 0 OID 0)
-- Dependencies: 378
-- Name: OADH_INT_State_Injuries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_State_Injuries_id_seq"', 1, false);


--
-- TOC entry 4778 (class 0 OID 0)
-- Dependencies: 360
-- Name: OADH_INT_State_Women_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_State_Women_Temp_id_seq"', 1, false);


--
-- TOC entry 4779 (class 0 OID 0)
-- Dependencies: 362
-- Name: OADH_INT_State_Women_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_State_Women_id_seq"', 1, false);


--
-- TOC entry 4780 (class 0 OID 0)
-- Dependencies: 364
-- Name: OADH_INT_Type_Sexual_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Type_Sexual_Temp_id_seq"', 1, false);


--
-- TOC entry 4781 (class 0 OID 0)
-- Dependencies: 366
-- Name: OADH_INT_Type_Sexual_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Type_Sexual_id_seq"', 1, false);


--
-- TOC entry 4782 (class 0 OID 0)
-- Dependencies: 340
-- Name: OADH_INT_Type_Trafficking_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Type_Trafficking_Temp_id_seq"', 1, false);


--
-- TOC entry 4783 (class 0 OID 0)
-- Dependencies: 342
-- Name: OADH_INT_Type_Trafficking_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Type_Trafficking_id_seq"', 1, false);


--
-- TOC entry 4784 (class 0 OID 0)
-- Dependencies: 356
-- Name: OADH_INT_Type_Women_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Type_Women_Temp_id_seq"', 1, false);


--
-- TOC entry 4785 (class 0 OID 0)
-- Dependencies: 358
-- Name: OADH_INT_Type_Women_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Type_Women_id_seq"', 1, false);


--
-- TOC entry 4786 (class 0 OID 0)
-- Dependencies: 332
-- Name: OADH_INT_Working_Officials_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Working_Officials_Temp_id_seq"', 1, false);


--
-- TOC entry 4787 (class 0 OID 0)
-- Dependencies: 334
-- Name: OADH_INT_Working_Officials_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_INT_Working_Officials_id_seq"', 1, false);


--
-- TOC entry 4788 (class 0 OID 0)
-- Dependencies: 262
-- Name: OADH_News_Human_Rights_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_News_Human_Rights_id_seq"', 1, true);


--
-- TOC entry 4789 (class 0 OID 0)
-- Dependencies: 266
-- Name: OADH_News_News_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_News_News_id_seq"', 1, false);


--
-- TOC entry 4790 (class 0 OID 0)
-- Dependencies: 252
-- Name: OADH_News_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_News_id_seq"', 1, true);


--
-- TOC entry 4791 (class 0 OID 0)
-- Dependencies: 396
-- Name: OADH_POP_Age_Population_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_POP_Age_Population_Temp_id_seq"', 1, false);


--
-- TOC entry 4792 (class 0 OID 0)
-- Dependencies: 398
-- Name: OADH_POP_Age_Population_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_POP_Age_Population_id_seq"', 1, false);


--
-- TOC entry 4793 (class 0 OID 0)
-- Dependencies: 400
-- Name: OADH_POP_Territory_Population_Temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_POP_Territory_Population_Temp_id_seq"', 1, false);


--
-- TOC entry 4794 (class 0 OID 0)
-- Dependencies: 402
-- Name: OADH_POP_Territory_Population_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_POP_Territory_Population_id_seq"', 1, false);


--
-- TOC entry 4795 (class 0 OID 0)
-- Dependencies: 264
-- Name: OADH_People_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_People_id_seq"', 2, true);


--
-- TOC entry 4796 (class 0 OID 0)
-- Dependencies: 256
-- Name: OADH_Producedure_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_Producedure_id_seq"', 1, false);


--
-- TOC entry 4797 (class 0 OID 0)
-- Dependencies: 248
-- Name: OADH_Settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_Settings_id_seq"', 1, true);


--
-- TOC entry 4798 (class 0 OID 0)
-- Dependencies: 254
-- Name: OADH_Source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."OADH_Source_id_seq"', 1, false);


--
-- TOC entry 4799 (class 0 OID 0)
-- Dependencies: 199
-- Name: ORG_Organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."ORG_Organization_id_seq"', 1, true);


--
-- TOC entry 4800 (class 0 OID 0)
-- Dependencies: 201
-- Name: SEC_Failed_Jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Failed_Jobs_id_seq"', 1, false);


--
-- TOC entry 4801 (class 0 OID 0)
-- Dependencies: 203
-- Name: SEC_File_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_File_id_seq"', 7, true);


--
-- TOC entry 4802 (class 0 OID 0)
-- Dependencies: 206
-- Name: SEC_Journal_Detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Journal_Detail_id_seq"', 133, true);


--
-- TOC entry 4803 (class 0 OID 0)
-- Dependencies: 207
-- Name: SEC_Journal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Journal_id_seq"', 82, true);


--
-- TOC entry 4804 (class 0 OID 0)
-- Dependencies: 209
-- Name: SEC_Menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Menu_id_seq"', 50, true);


--
-- TOC entry 4805 (class 0 OID 0)
-- Dependencies: 211
-- Name: SEC_Module_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Module_id_seq"', 3, true);


--
-- TOC entry 4806 (class 0 OID 0)
-- Dependencies: 214
-- Name: SEC_Permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Permission_id_seq"', 12, true);


--
-- TOC entry 4807 (class 0 OID 0)
-- Dependencies: 217
-- Name: SEC_Role_Menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Role_Menu_id_seq"', 1, false);


--
-- TOC entry 4808 (class 0 OID 0)
-- Dependencies: 219
-- Name: SEC_Role_Permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Role_Permission_id_seq"', 1, false);


--
-- TOC entry 4809 (class 0 OID 0)
-- Dependencies: 220
-- Name: SEC_Role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_Role_id_seq"', 1, false);


--
-- TOC entry 4810 (class 0 OID 0)
-- Dependencies: 223
-- Name: SEC_User_Menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_Menu_id_seq"', 45, true);


--
-- TOC entry 4811 (class 0 OID 0)
-- Dependencies: 225
-- Name: SEC_User_Organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_Organization_id_seq"', 1, true);


--
-- TOC entry 4812 (class 0 OID 0)
-- Dependencies: 227
-- Name: SEC_User_Permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_Permission_id_seq"', 1, true);


--
-- TOC entry 4813 (class 0 OID 0)
-- Dependencies: 229
-- Name: SEC_User_Role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_Role_id_seq"', 1, false);


--
-- TOC entry 4814 (class 0 OID 0)
-- Dependencies: 230
-- Name: SEC_User_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SEC_User_id_seq"', 1, true);


--
-- TOC entry 4815 (class 0 OID 0)
-- Dependencies: 233
-- Name: SYS_Account_Chart_Type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Account_Chart_Type_id_seq"', 1, false);


--
-- TOC entry 4816 (class 0 OID 0)
-- Dependencies: 235
-- Name: SYS_Account_Type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Account_Type_id_seq"', 1, false);


--
-- TOC entry 4817 (class 0 OID 0)
-- Dependencies: 236
-- Name: SYS_Account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Account_id_seq"', 1, false);


--
-- TOC entry 4818 (class 0 OID 0)
-- Dependencies: 238
-- Name: SYS_Country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Country_id_seq"', 240, true);


--
-- TOC entry 4819 (class 0 OID 0)
-- Dependencies: 240
-- Name: SYS_Currency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Currency_id_seq"', 175, true);


--
-- TOC entry 4820 (class 0 OID 0)
-- Dependencies: 242
-- Name: SYS_Region_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Region_id_seq"', 172, true);


--
-- TOC entry 4821 (class 0 OID 0)
-- Dependencies: 244
-- Name: SYS_Voucher_Type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."SYS_Voucher_Type_id_seq"', 1, false);


--
-- TOC entry 3839 (class 2606 OID 16590)
-- Name: FILE_File FILE_File_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."FILE_File"
    ADD CONSTRAINT "FILE_File_pkey" PRIMARY KEY (id);


--
-- TOC entry 3941 (class 2606 OID 17713)
-- Name: OADH_Context OADH_Context_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Context"
    ADD CONSTRAINT "OADH_Context_pkey" PRIMARY KEY (id);


--
-- TOC entry 3929 (class 2606 OID 17643)
-- Name: OADH_Digital_Media OADH_Digital_Media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Digital_Media"
    ADD CONSTRAINT "OADH_Digital_Media_pkey" PRIMARY KEY (id);


--
-- TOC entry 3998 (class 2606 OID 26234)
-- Name: OADH_FREE_Accused_Liberty_Deprivation_Temp OADH_FREE_Accused_Liberty_Deprivation_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Accused_Liberty_Deprivation_Temp"
    ADD CONSTRAINT "OADH_FREE_Accused_Liberty_Deprivation_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4001 (class 2606 OID 26251)
-- Name: OADH_FREE_Accused_Liberty_Deprivation OADH_FREE_Accused_Liberty_Deprivation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Accused_Liberty_Deprivation"
    ADD CONSTRAINT "OADH_FREE_Accused_Liberty_Deprivation_pkey" PRIMARY KEY (id);


--
-- TOC entry 4022 (class 2606 OID 26370)
-- Name: OADH_FREE_Acute_Diseases_Temp OADH_FREE_Acute_Diseases_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Acute_Diseases_Temp"
    ADD CONSTRAINT "OADH_FREE_Acute_Diseases_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4025 (class 2606 OID 26384)
-- Name: OADH_FREE_Acute_Diseases OADH_FREE_Acute_Diseases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Acute_Diseases"
    ADD CONSTRAINT "OADH_FREE_Acute_Diseases_pkey" PRIMARY KEY (id);


--
-- TOC entry 3986 (class 2606 OID 26166)
-- Name: OADH_FREE_Capture_Orders_Temp OADH_FREE_Capture_Orders_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Capture_Orders_Temp"
    ADD CONSTRAINT "OADH_FREE_Capture_Orders_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 3989 (class 2606 OID 26183)
-- Name: OADH_FREE_Capture_Orders OADH_FREE_Capture_Orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Capture_Orders"
    ADD CONSTRAINT "OADH_FREE_Capture_Orders_pkey" PRIMARY KEY (id);


--
-- TOC entry 4028 (class 2606 OID 26401)
-- Name: OADH_FREE_Chronic_Diseases_Temp OADH_FREE_Chronic_Diseases_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Chronic_Diseases_Temp"
    ADD CONSTRAINT "OADH_FREE_Chronic_Diseases_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4031 (class 2606 OID 26418)
-- Name: OADH_FREE_Chronic_Diseases OADH_FREE_Chronic_Diseases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Chronic_Diseases"
    ADD CONSTRAINT "OADH_FREE_Chronic_Diseases_pkey" PRIMARY KEY (id);


--
-- TOC entry 3968 (class 2606 OID 26070)
-- Name: OADH_FREE_Criminal_Cases_Trials_Temp OADH_FREE_Criminal_Cases_Trials_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Criminal_Cases_Trials_Temp"
    ADD CONSTRAINT "OADH_FREE_Criminal_Cases_Trials_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 3971 (class 2606 OID 26087)
-- Name: OADH_FREE_Criminal_Cases_Trials OADH_FREE_Criminal_Cases_Trials_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Criminal_Cases_Trials"
    ADD CONSTRAINT "OADH_FREE_Criminal_Cases_Trials_pkey" PRIMARY KEY (id);


--
-- TOC entry 4016 (class 2606 OID 26336)
-- Name: OADH_FREE_Deprived_Persons_Temp OADH_FREE_Deprived_Persons_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Deprived_Persons_Temp"
    ADD CONSTRAINT "OADH_FREE_Deprived_Persons_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4019 (class 2606 OID 26353)
-- Name: OADH_FREE_Deprived_Persons OADH_FREE_Deprived_Persons_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Deprived_Persons"
    ADD CONSTRAINT "OADH_FREE_Deprived_Persons_pkey" PRIMARY KEY (id);


--
-- TOC entry 3992 (class 2606 OID 26200)
-- Name: OADH_FREE_Disappearances_Victims_Temp OADH_FREE_Disappearances_Victims_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Disappearances_Victims_Temp"
    ADD CONSTRAINT "OADH_FREE_Disappearances_Victims_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 3995 (class 2606 OID 26217)
-- Name: OADH_FREE_Disappearances_Victims OADH_FREE_Disappearances_Victims_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Disappearances_Victims"
    ADD CONSTRAINT "OADH_FREE_Disappearances_Victims_pkey" PRIMARY KEY (id);


--
-- TOC entry 4010 (class 2606 OID 26302)
-- Name: OADH_FREE_Forced_Disappearance_Temps OADH_FREE_Forced_Disappearance_Temps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearance_Temps"
    ADD CONSTRAINT "OADH_FREE_Forced_Disappearance_Temps_pkey" PRIMARY KEY (id);


--
-- TOC entry 4013 (class 2606 OID 26319)
-- Name: OADH_FREE_Forced_Disappearances OADH_FREE_Forced_Disappearances_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearances"
    ADD CONSTRAINT "OADH_FREE_Forced_Disappearances_pkey" PRIMARY KEY (id);


--
-- TOC entry 3956 (class 2606 OID 26005)
-- Name: OADH_FREE_Habeas_Corpus_Request_Temp OADH_FREE_Habeas_Corpus_Request_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Habeas_Corpus_Request_Temp"
    ADD CONSTRAINT "OADH_FREE_Habeas_Corpus_Request_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 3959 (class 2606 OID 26019)
-- Name: OADH_FREE_Habeas_Corpus_Request OADH_FREE_Habeas_Corpus_Request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Habeas_Corpus_Request"
    ADD CONSTRAINT "OADH_FREE_Habeas_Corpus_Request_pkey" PRIMARY KEY (id);


--
-- TOC entry 4034 (class 2606 OID 26435)
-- Name: OADH_FREE_Jails_Occupation_Temp OADH_FREE_Jails_Occupation_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Jails_Occupation_Temp"
    ADD CONSTRAINT "OADH_FREE_Jails_Occupation_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4037 (class 2606 OID 26452)
-- Name: OADH_FREE_Jails_Occupation OADH_FREE_Jails_Occupation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Jails_Occupation"
    ADD CONSTRAINT "OADH_FREE_Jails_Occupation_pkey" PRIMARY KEY (id);


--
-- TOC entry 3974 (class 2606 OID 26101)
-- Name: OADH_FREE_Mass_Trials_Temp OADH_FREE_Mass_Trials_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Mass_Trials_Temp"
    ADD CONSTRAINT "OADH_FREE_Mass_Trials_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 3977 (class 2606 OID 26115)
-- Name: OADH_FREE_Mass_Trials OADH_FREE_Mass_Trials_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Mass_Trials"
    ADD CONSTRAINT "OADH_FREE_Mass_Trials_pkey" PRIMARY KEY (id);


--
-- TOC entry 3962 (class 2606 OID 26036)
-- Name: OADH_FREE_People_Detained_Temp OADH_FREE_People_Detained_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_People_Detained_Temp"
    ADD CONSTRAINT "OADH_FREE_People_Detained_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 3965 (class 2606 OID 26053)
-- Name: OADH_FREE_People_Detained OADH_FREE_People_Detained_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_People_Detained"
    ADD CONSTRAINT "OADH_FREE_People_Detained_pkey" PRIMARY KEY (id);


--
-- TOC entry 3980 (class 2606 OID 26132)
-- Name: OADH_FREE_Procedural_Fraud_Temp OADH_FREE_Procedural_Fraud_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Procedural_Fraud_Temp"
    ADD CONSTRAINT "OADH_FREE_Procedural_Fraud_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 3983 (class 2606 OID 26149)
-- Name: OADH_FREE_Procedural_Fraud OADH_FREE_Procedural_Fraud_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Procedural_Fraud"
    ADD CONSTRAINT "OADH_FREE_Procedural_Fraud_pkey" PRIMARY KEY (id);


--
-- TOC entry 4004 (class 2606 OID 26268)
-- Name: OADH_FREE_Victims_Liberty_Deprivation_Temp OADH_FREE_Victims_Liberty_Deprivation_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Victims_Liberty_Deprivation_Temp"
    ADD CONSTRAINT "OADH_FREE_Victims_Liberty_Deprivation_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4007 (class 2606 OID 26285)
-- Name: OADH_FREE_Victims_Liberty_Deprivation OADH_FREE_Victims_Liberty_Deprivation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Victims_Liberty_Deprivation"
    ADD CONSTRAINT "OADH_FREE_Victims_Liberty_Deprivation_pkey" PRIMARY KEY (id);


--
-- TOC entry 3944 (class 2606 OID 17730)
-- Name: OADH_Human_Right OADH_Human_Right_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Human_Right"
    ADD CONSTRAINT "OADH_Human_Right_pkey" PRIMARY KEY (id);


--
-- TOC entry 4124 (class 2606 OID 26906)
-- Name: OADH_INT_Age_Injuries_Temp OADH_INT_Age_Injuries_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Injuries_Temp"
    ADD CONSTRAINT "OADH_INT_Age_Injuries_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4127 (class 2606 OID 26920)
-- Name: OADH_INT_Age_Injuries OADH_INT_Age_Injuries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Injuries"
    ADD CONSTRAINT "OADH_INT_Age_Injuries_pkey" PRIMARY KEY (id);


--
-- TOC entry 4112 (class 2606 OID 26841)
-- Name: OADH_INT_Age_Sexual_Temp OADH_INT_Age_Sexual_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Sexual_Temp"
    ADD CONSTRAINT "OADH_INT_Age_Sexual_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4115 (class 2606 OID 26858)
-- Name: OADH_INT_Age_Sexual OADH_INT_Age_Sexual_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Sexual"
    ADD CONSTRAINT "OADH_INT_Age_Sexual_pkey" PRIMARY KEY (id);


--
-- TOC entry 4070 (class 2606 OID 26624)
-- Name: OADH_INT_Age_Trafficking_Temp OADH_INT_Age_Trafficking_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Trafficking_Temp"
    ADD CONSTRAINT "OADH_INT_Age_Trafficking_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4073 (class 2606 OID 26638)
-- Name: OADH_INT_Age_Trafficking OADH_INT_Age_Trafficking_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Trafficking"
    ADD CONSTRAINT "OADH_INT_Age_Trafficking_pkey" PRIMARY KEY (id);


--
-- TOC entry 4082 (class 2606 OID 26686)
-- Name: OADH_INT_Age_Women_Temp OADH_INT_Age_Women_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Women_Temp"
    ADD CONSTRAINT "OADH_INT_Age_Women_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4085 (class 2606 OID 26700)
-- Name: OADH_INT_Age_Women OADH_INT_Age_Women_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Women"
    ADD CONSTRAINT "OADH_INT_Age_Women_pkey" PRIMARY KEY (id);


--
-- TOC entry 4136 (class 2606 OID 26971)
-- Name: OADH_INT_Agent_Investigation_Temp OADH_INT_Agent_Investigation_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Agent_Investigation_Temp"
    ADD CONSTRAINT "OADH_INT_Agent_Investigation_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4139 (class 2606 OID 26988)
-- Name: OADH_INT_Agent_Investigation OADH_INT_Agent_Investigation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Agent_Investigation"
    ADD CONSTRAINT "OADH_INT_Agent_Investigation_pkey" PRIMARY KEY (id);


--
-- TOC entry 4046 (class 2606 OID 26503)
-- Name: OADH_INT_Crimes_Accused_Temp OADH_INT_Crimes_Accused_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Accused_Temp"
    ADD CONSTRAINT "OADH_INT_Crimes_Accused_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4049 (class 2606 OID 26520)
-- Name: OADH_INT_Crimes_Accused OADH_INT_Crimes_Accused_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Accused"
    ADD CONSTRAINT "OADH_INT_Crimes_Accused_pkey" PRIMARY KEY (id);


--
-- TOC entry 4040 (class 2606 OID 26469)
-- Name: OADH_INT_Crimes_Victims_Temp OADH_INT_Crimes_Victims_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Victims_Temp"
    ADD CONSTRAINT "OADH_INT_Crimes_Victims_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4043 (class 2606 OID 26486)
-- Name: OADH_INT_Crimes_Victims OADH_INT_Crimes_Victims_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Victims"
    ADD CONSTRAINT "OADH_INT_Crimes_Victims_pkey" PRIMARY KEY (id);


--
-- TOC entry 4106 (class 2606 OID 26810)
-- Name: OADH_INT_Gender_Sexual_Temp OADH_INT_Gender_Sexual_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Sexual_Temp"
    ADD CONSTRAINT "OADH_INT_Gender_Sexual_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4109 (class 2606 OID 26824)
-- Name: OADH_INT_Gender_Sexual OADH_INT_Gender_Sexual_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Sexual"
    ADD CONSTRAINT "OADH_INT_Gender_Sexual_pkey" PRIMARY KEY (id);


--
-- TOC entry 4058 (class 2606 OID 26568)
-- Name: OADH_INT_Gender_Trafficking_Temp OADH_INT_Gender_Trafficking_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Trafficking_Temp"
    ADD CONSTRAINT "OADH_INT_Gender_Trafficking_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4061 (class 2606 OID 26582)
-- Name: OADH_INT_Gender_Trafficking OADH_INT_Gender_Trafficking_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Trafficking"
    ADD CONSTRAINT "OADH_INT_Gender_Trafficking_pkey" PRIMARY KEY (id);


--
-- TOC entry 4142 (class 2606 OID 27005)
-- Name: OADH_INT_Integrity_Violation_Temp OADH_INT_Integrity_Violation_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Integrity_Violation_Temp"
    ADD CONSTRAINT "OADH_INT_Integrity_Violation_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4145 (class 2606 OID 27022)
-- Name: OADH_INT_Integrity_Violation OADH_INT_Integrity_Violation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Integrity_Violation"
    ADD CONSTRAINT "OADH_INT_Integrity_Violation_pkey" PRIMARY KEY (id);


--
-- TOC entry 4076 (class 2606 OID 26655)
-- Name: OADH_INT_Movement_Freedom_Temp OADH_INT_Movement_Freedom_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Movement_Freedom_Temp"
    ADD CONSTRAINT "OADH_INT_Movement_Freedom_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4079 (class 2606 OID 26672)
-- Name: OADH_INT_Movement_Freedom OADH_INT_Movement_Freedom_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Movement_Freedom"
    ADD CONSTRAINT "OADH_INT_Movement_Freedom_pkey" PRIMARY KEY (id);


--
-- TOC entry 4130 (class 2606 OID 26937)
-- Name: OADH_INT_Schedule_Injuries_Temp OADH_INT_Schedule_Injuries_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Schedule_Injuries_Temp"
    ADD CONSTRAINT "OADH_INT_Schedule_Injuries_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4133 (class 2606 OID 26954)
-- Name: OADH_INT_Schedule_Injuries OADH_INT_Schedule_Injuries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Schedule_Injuries"
    ADD CONSTRAINT "OADH_INT_Schedule_Injuries_pkey" PRIMARY KEY (id);


--
-- TOC entry 4118 (class 2606 OID 26875)
-- Name: OADH_INT_State_Injuries_Temp OADH_INT_State_Injuries_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Injuries_Temp"
    ADD CONSTRAINT "OADH_INT_State_Injuries_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4121 (class 2606 OID 26892)
-- Name: OADH_INT_State_Injuries OADH_INT_State_Injuries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Injuries"
    ADD CONSTRAINT "OADH_INT_State_Injuries_pkey" PRIMARY KEY (id);


--
-- TOC entry 4094 (class 2606 OID 26745)
-- Name: OADH_INT_State_Women_Temp OADH_INT_State_Women_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Women_Temp"
    ADD CONSTRAINT "OADH_INT_State_Women_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4097 (class 2606 OID 26762)
-- Name: OADH_INT_State_Women OADH_INT_State_Women_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Women"
    ADD CONSTRAINT "OADH_INT_State_Women_pkey" PRIMARY KEY (id);


--
-- TOC entry 4100 (class 2606 OID 26779)
-- Name: OADH_INT_Type_Sexual_Temp OADH_INT_Type_Sexual_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Sexual_Temp"
    ADD CONSTRAINT "OADH_INT_Type_Sexual_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4103 (class 2606 OID 26796)
-- Name: OADH_INT_Type_Sexual OADH_INT_Type_Sexual_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Sexual"
    ADD CONSTRAINT "OADH_INT_Type_Sexual_pkey" PRIMARY KEY (id);


--
-- TOC entry 4064 (class 2606 OID 26596)
-- Name: OADH_INT_Type_Trafficking_Temp OADH_INT_Type_Trafficking_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Trafficking_Temp"
    ADD CONSTRAINT "OADH_INT_Type_Trafficking_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4067 (class 2606 OID 26610)
-- Name: OADH_INT_Type_Trafficking OADH_INT_Type_Trafficking_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Trafficking"
    ADD CONSTRAINT "OADH_INT_Type_Trafficking_pkey" PRIMARY KEY (id);


--
-- TOC entry 4088 (class 2606 OID 26714)
-- Name: OADH_INT_Type_Women_Temp OADH_INT_Type_Women_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Women_Temp"
    ADD CONSTRAINT "OADH_INT_Type_Women_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4091 (class 2606 OID 26728)
-- Name: OADH_INT_Type_Women OADH_INT_Type_Women_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Women"
    ADD CONSTRAINT "OADH_INT_Type_Women_pkey" PRIMARY KEY (id);


--
-- TOC entry 4052 (class 2606 OID 26537)
-- Name: OADH_INT_Working_Officials_Temp OADH_INT_Working_Officials_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Working_Officials_Temp"
    ADD CONSTRAINT "OADH_INT_Working_Officials_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4055 (class 2606 OID 26554)
-- Name: OADH_INT_Working_Officials OADH_INT_Working_Officials_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Working_Officials"
    ADD CONSTRAINT "OADH_INT_Working_Officials_pkey" PRIMARY KEY (id);


--
-- TOC entry 3947 (class 2606 OID 17744)
-- Name: OADH_News_Human_Rights OADH_News_Human_Rights_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_Human_Rights"
    ADD CONSTRAINT "OADH_News_Human_Rights_pkey" PRIMARY KEY (id);


--
-- TOC entry 3953 (class 2606 OID 25986)
-- Name: OADH_News_News OADH_News_News_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_News"
    ADD CONSTRAINT "OADH_News_News_pkey" PRIMARY KEY (id);


--
-- TOC entry 3932 (class 2606 OID 17660)
-- Name: OADH_News OADH_News_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News"
    ADD CONSTRAINT "OADH_News_pkey" PRIMARY KEY (id);


--
-- TOC entry 4148 (class 2606 OID 34178)
-- Name: OADH_POP_Age_Population_Temp OADH_POP_Age_Population_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Age_Population_Temp"
    ADD CONSTRAINT "OADH_POP_Age_Population_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4151 (class 2606 OID 34192)
-- Name: OADH_POP_Age_Population OADH_POP_Age_Population_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Age_Population"
    ADD CONSTRAINT "OADH_POP_Age_Population_pkey" PRIMARY KEY (id);


--
-- TOC entry 4154 (class 2606 OID 34209)
-- Name: OADH_POP_Territory_Population_Temp OADH_POP_Territory_Population_Temp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Territory_Population_Temp"
    ADD CONSTRAINT "OADH_POP_Territory_Population_Temp_pkey" PRIMARY KEY (id);


--
-- TOC entry 4157 (class 2606 OID 34226)
-- Name: OADH_POP_Territory_Population OADH_POP_Territory_Population_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Territory_Population"
    ADD CONSTRAINT "OADH_POP_Territory_Population_pkey" PRIMARY KEY (id);


--
-- TOC entry 3950 (class 2606 OID 17778)
-- Name: OADH_People OADH_People_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_People"
    ADD CONSTRAINT "OADH_People_pkey" PRIMARY KEY (id);


--
-- TOC entry 3938 (class 2606 OID 17699)
-- Name: OADH_Producedure OADH_Producedure_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Producedure"
    ADD CONSTRAINT "OADH_Producedure_pkey" PRIMARY KEY (id);


--
-- TOC entry 3926 (class 2606 OID 17634)
-- Name: OADH_Settings OADH_Settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Settings"
    ADD CONSTRAINT "OADH_Settings_pkey" PRIMARY KEY (id);


--
-- TOC entry 3935 (class 2606 OID 17682)
-- Name: OADH_Source OADH_Source_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Source"
    ADD CONSTRAINT "OADH_Source_pkey" PRIMARY KEY (id);


--
-- TOC entry 3841 (class 2606 OID 16592)
-- Name: ORG_Organization ORG_Organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization"
    ADD CONSTRAINT "ORG_Organization_pkey" PRIMARY KEY (id);


--
-- TOC entry 3846 (class 2606 OID 16594)
-- Name: SEC_Failed_Jobs SEC_Failed_Jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Failed_Jobs"
    ADD CONSTRAINT "SEC_Failed_Jobs_pkey" PRIMARY KEY (id);


--
-- TOC entry 3848 (class 2606 OID 16596)
-- Name: SEC_File SEC_File_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_File"
    ADD CONSTRAINT "SEC_File_pkey" PRIMARY KEY (id);


--
-- TOC entry 3858 (class 2606 OID 16598)
-- Name: SEC_Journal_Detail SEC_Journal_Detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal_Detail"
    ADD CONSTRAINT "SEC_Journal_Detail_pkey" PRIMARY KEY (id);


--
-- TOC entry 3852 (class 2606 OID 16600)
-- Name: SEC_Journal SEC_Journal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal"
    ADD CONSTRAINT "SEC_Journal_pkey" PRIMARY KEY (id);


--
-- TOC entry 3861 (class 2606 OID 16602)
-- Name: SEC_Menu SEC_Menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu"
    ADD CONSTRAINT "SEC_Menu_pkey" PRIMARY KEY (id);


--
-- TOC entry 3866 (class 2606 OID 16604)
-- Name: SEC_Module SEC_Module_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Module"
    ADD CONSTRAINT "SEC_Module_pkey" PRIMARY KEY (id);


--
-- TOC entry 3870 (class 2606 OID 16606)
-- Name: SEC_Permission SEC_Permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Permission"
    ADD CONSTRAINT "SEC_Permission_pkey" PRIMARY KEY (id);


--
-- TOC entry 3875 (class 2606 OID 16608)
-- Name: SEC_Role_Menu SEC_Role_Menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu"
    ADD CONSTRAINT "SEC_Role_Menu_pkey" PRIMARY KEY (id);


--
-- TOC entry 3879 (class 2606 OID 16610)
-- Name: SEC_Role_Permission SEC_Role_Permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission"
    ADD CONSTRAINT "SEC_Role_Permission_pkey" PRIMARY KEY (id);


--
-- TOC entry 3872 (class 2606 OID 16612)
-- Name: SEC_Role SEC_Role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role"
    ADD CONSTRAINT "SEC_Role_pkey" PRIMARY KEY (id);


--
-- TOC entry 3887 (class 2606 OID 16614)
-- Name: SEC_User_Menu SEC_User_Menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT "SEC_User_Menu_pkey" PRIMARY KEY (id);


--
-- TOC entry 3893 (class 2606 OID 16616)
-- Name: SEC_User_Organization SEC_User_Organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization"
    ADD CONSTRAINT "SEC_User_Organization_pkey" PRIMARY KEY (id);


--
-- TOC entry 3897 (class 2606 OID 16618)
-- Name: SEC_User_Permission SEC_User_Permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT "SEC_User_Permission_pkey" PRIMARY KEY (id);


--
-- TOC entry 3903 (class 2606 OID 16620)
-- Name: SEC_User_Role SEC_User_Role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT "SEC_User_Role_pkey" PRIMARY KEY (id);


--
-- TOC entry 3883 (class 2606 OID 16622)
-- Name: SEC_User SEC_User_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User"
    ADD CONSTRAINT "SEC_User_pkey" PRIMARY KEY (id);


--
-- TOC entry 3910 (class 2606 OID 16624)
-- Name: SYS_Account_Chart_Type SYS_Account_Chart_Type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Chart_Type"
    ADD CONSTRAINT "SYS_Account_Chart_Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3912 (class 2606 OID 16626)
-- Name: SYS_Account_Type SYS_Account_Type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Type"
    ADD CONSTRAINT "SYS_Account_Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3908 (class 2606 OID 16628)
-- Name: SYS_Account SYS_Account_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account"
    ADD CONSTRAINT "SYS_Account_pkey" PRIMARY KEY (id);


--
-- TOC entry 3914 (class 2606 OID 16630)
-- Name: SYS_Country SYS_Country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Country"
    ADD CONSTRAINT "SYS_Country_pkey" PRIMARY KEY (id);


--
-- TOC entry 3916 (class 2606 OID 16632)
-- Name: SYS_Currency SYS_Currency_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Currency"
    ADD CONSTRAINT "SYS_Currency_pkey" PRIMARY KEY (id);


--
-- TOC entry 3918 (class 2606 OID 16634)
-- Name: SYS_Region SYS_Region_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Region"
    ADD CONSTRAINT "SYS_Region_pkey" PRIMARY KEY (id);


--
-- TOC entry 3920 (class 2606 OID 16636)
-- Name: SYS_Voucher_Type SYS_Voucher_Type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Voucher_Type"
    ADD CONSTRAINT "SYS_Voucher_Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3922 (class 2606 OID 16638)
-- Name: cache cache_key_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cache
    ADD CONSTRAINT cache_key_unique UNIQUE (key);


--
-- TOC entry 3924 (class 2606 OID 16640)
-- Name: sessions sessions_id_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_id_unique UNIQUE (id);


--
-- TOC entry 3942 (class 1259 OID 17719)
-- Name: oadh_context_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_context_organization_id_index ON public."OADH_Context" USING btree (organization_id);


--
-- TOC entry 3930 (class 1259 OID 17644)
-- Name: oadh_digital_media_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_digital_media_organization_id_index ON public."OADH_Digital_Media" USING btree (organization_id);


--
-- TOC entry 4002 (class 1259 OID 26257)
-- Name: oadh_free_accused_liberty_deprivation_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_accused_liberty_deprivation_organization_id_index ON public."OADH_FREE_Accused_Liberty_Deprivation" USING btree (organization_id);


--
-- TOC entry 3999 (class 1259 OID 26240)
-- Name: oadh_free_accused_liberty_deprivation_temp_organization_id_inde; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_accused_liberty_deprivation_temp_organization_id_inde ON public."OADH_FREE_Accused_Liberty_Deprivation_Temp" USING btree (organization_id);


--
-- TOC entry 4026 (class 1259 OID 26390)
-- Name: oadh_free_acute_diseases_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_acute_diseases_organization_id_index ON public."OADH_FREE_Acute_Diseases" USING btree (organization_id);


--
-- TOC entry 4023 (class 1259 OID 26376)
-- Name: oadh_free_acute_diseases_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_acute_diseases_temp_organization_id_index ON public."OADH_FREE_Acute_Diseases_Temp" USING btree (organization_id);


--
-- TOC entry 3990 (class 1259 OID 26189)
-- Name: oadh_free_capture_orders_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_capture_orders_organization_id_index ON public."OADH_FREE_Capture_Orders" USING btree (organization_id);


--
-- TOC entry 3987 (class 1259 OID 26172)
-- Name: oadh_free_capture_orders_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_capture_orders_temp_organization_id_index ON public."OADH_FREE_Capture_Orders_Temp" USING btree (organization_id);


--
-- TOC entry 4032 (class 1259 OID 26424)
-- Name: oadh_free_chronic_diseases_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_chronic_diseases_organization_id_index ON public."OADH_FREE_Chronic_Diseases" USING btree (organization_id);


--
-- TOC entry 4029 (class 1259 OID 26407)
-- Name: oadh_free_chronic_diseases_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_chronic_diseases_temp_organization_id_index ON public."OADH_FREE_Chronic_Diseases_Temp" USING btree (organization_id);


--
-- TOC entry 3972 (class 1259 OID 26093)
-- Name: oadh_free_criminal_cases_trials_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_criminal_cases_trials_organization_id_index ON public."OADH_FREE_Criminal_Cases_Trials" USING btree (organization_id);


--
-- TOC entry 3969 (class 1259 OID 26076)
-- Name: oadh_free_criminal_cases_trials_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_criminal_cases_trials_temp_organization_id_index ON public."OADH_FREE_Criminal_Cases_Trials_Temp" USING btree (organization_id);


--
-- TOC entry 4020 (class 1259 OID 26359)
-- Name: oadh_free_deprived_persons_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_deprived_persons_organization_id_index ON public."OADH_FREE_Deprived_Persons" USING btree (organization_id);


--
-- TOC entry 4017 (class 1259 OID 26342)
-- Name: oadh_free_deprived_persons_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_deprived_persons_temp_organization_id_index ON public."OADH_FREE_Deprived_Persons_Temp" USING btree (organization_id);


--
-- TOC entry 3996 (class 1259 OID 26223)
-- Name: oadh_free_disappearances_victims_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_disappearances_victims_organization_id_index ON public."OADH_FREE_Disappearances_Victims" USING btree (organization_id);


--
-- TOC entry 3993 (class 1259 OID 26206)
-- Name: oadh_free_disappearances_victims_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_disappearances_victims_temp_organization_id_index ON public."OADH_FREE_Disappearances_Victims_Temp" USING btree (organization_id);


--
-- TOC entry 4011 (class 1259 OID 26308)
-- Name: oadh_free_forced_disappearance_temps_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_forced_disappearance_temps_organization_id_index ON public."OADH_FREE_Forced_Disappearance_Temps" USING btree (organization_id);


--
-- TOC entry 4014 (class 1259 OID 26325)
-- Name: oadh_free_forced_disappearances_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_forced_disappearances_organization_id_index ON public."OADH_FREE_Forced_Disappearances" USING btree (organization_id);


--
-- TOC entry 3960 (class 1259 OID 26025)
-- Name: oadh_free_habeas_corpus_request_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_habeas_corpus_request_organization_id_index ON public."OADH_FREE_Habeas_Corpus_Request" USING btree (organization_id);


--
-- TOC entry 3957 (class 1259 OID 26011)
-- Name: oadh_free_habeas_corpus_request_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_habeas_corpus_request_temp_organization_id_index ON public."OADH_FREE_Habeas_Corpus_Request_Temp" USING btree (organization_id);


--
-- TOC entry 4038 (class 1259 OID 26458)
-- Name: oadh_free_jails_occupation_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_jails_occupation_organization_id_index ON public."OADH_FREE_Jails_Occupation" USING btree (organization_id);


--
-- TOC entry 4035 (class 1259 OID 26441)
-- Name: oadh_free_jails_occupation_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_jails_occupation_temp_organization_id_index ON public."OADH_FREE_Jails_Occupation_Temp" USING btree (organization_id);


--
-- TOC entry 3978 (class 1259 OID 26121)
-- Name: oadh_free_mass_trials_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_mass_trials_organization_id_index ON public."OADH_FREE_Mass_Trials" USING btree (organization_id);


--
-- TOC entry 3975 (class 1259 OID 26107)
-- Name: oadh_free_mass_trials_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_mass_trials_temp_organization_id_index ON public."OADH_FREE_Mass_Trials_Temp" USING btree (organization_id);


--
-- TOC entry 3966 (class 1259 OID 26059)
-- Name: oadh_free_people_detained_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_people_detained_organization_id_index ON public."OADH_FREE_People_Detained" USING btree (organization_id);


--
-- TOC entry 3963 (class 1259 OID 26042)
-- Name: oadh_free_people_detained_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_people_detained_temp_organization_id_index ON public."OADH_FREE_People_Detained_Temp" USING btree (organization_id);


--
-- TOC entry 3984 (class 1259 OID 26155)
-- Name: oadh_free_procedural_fraud_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_procedural_fraud_organization_id_index ON public."OADH_FREE_Procedural_Fraud" USING btree (organization_id);


--
-- TOC entry 3981 (class 1259 OID 26138)
-- Name: oadh_free_procedural_fraud_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_procedural_fraud_temp_organization_id_index ON public."OADH_FREE_Procedural_Fraud_Temp" USING btree (organization_id);


--
-- TOC entry 4008 (class 1259 OID 26291)
-- Name: oadh_free_victims_liberty_deprivation_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_victims_liberty_deprivation_organization_id_index ON public."OADH_FREE_Victims_Liberty_Deprivation" USING btree (organization_id);


--
-- TOC entry 4005 (class 1259 OID 26274)
-- Name: oadh_free_victims_liberty_deprivation_temp_organization_id_inde; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_free_victims_liberty_deprivation_temp_organization_id_inde ON public."OADH_FREE_Victims_Liberty_Deprivation_Temp" USING btree (organization_id);


--
-- TOC entry 3945 (class 1259 OID 17731)
-- Name: oadh_human_right_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_human_right_organization_id_index ON public."OADH_Human_Right" USING btree (organization_id);


--
-- TOC entry 4128 (class 1259 OID 26926)
-- Name: oadh_int_age_injuries_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_age_injuries_organization_id_index ON public."OADH_INT_Age_Injuries" USING btree (organization_id);


--
-- TOC entry 4125 (class 1259 OID 26912)
-- Name: oadh_int_age_injuries_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_age_injuries_temp_organization_id_index ON public."OADH_INT_Age_Injuries_Temp" USING btree (organization_id);


--
-- TOC entry 4116 (class 1259 OID 26864)
-- Name: oadh_int_age_sexual_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_age_sexual_organization_id_index ON public."OADH_INT_Age_Sexual" USING btree (organization_id);


--
-- TOC entry 4113 (class 1259 OID 26847)
-- Name: oadh_int_age_sexual_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_age_sexual_temp_organization_id_index ON public."OADH_INT_Age_Sexual_Temp" USING btree (organization_id);


--
-- TOC entry 4074 (class 1259 OID 26644)
-- Name: oadh_int_age_trafficking_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_age_trafficking_organization_id_index ON public."OADH_INT_Age_Trafficking" USING btree (organization_id);


--
-- TOC entry 4071 (class 1259 OID 26630)
-- Name: oadh_int_age_trafficking_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_age_trafficking_temp_organization_id_index ON public."OADH_INT_Age_Trafficking_Temp" USING btree (organization_id);


--
-- TOC entry 4086 (class 1259 OID 26706)
-- Name: oadh_int_age_women_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_age_women_organization_id_index ON public."OADH_INT_Age_Women" USING btree (organization_id);


--
-- TOC entry 4083 (class 1259 OID 26692)
-- Name: oadh_int_age_women_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_age_women_temp_organization_id_index ON public."OADH_INT_Age_Women_Temp" USING btree (organization_id);


--
-- TOC entry 4140 (class 1259 OID 26994)
-- Name: oadh_int_agent_investigation_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_agent_investigation_organization_id_index ON public."OADH_INT_Agent_Investigation" USING btree (organization_id);


--
-- TOC entry 4137 (class 1259 OID 26977)
-- Name: oadh_int_agent_investigation_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_agent_investigation_temp_organization_id_index ON public."OADH_INT_Agent_Investigation_Temp" USING btree (organization_id);


--
-- TOC entry 4050 (class 1259 OID 26526)
-- Name: oadh_int_crimes_accused_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_crimes_accused_organization_id_index ON public."OADH_INT_Crimes_Accused" USING btree (organization_id);


--
-- TOC entry 4047 (class 1259 OID 26509)
-- Name: oadh_int_crimes_accused_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_crimes_accused_temp_organization_id_index ON public."OADH_INT_Crimes_Accused_Temp" USING btree (organization_id);


--
-- TOC entry 4044 (class 1259 OID 26492)
-- Name: oadh_int_crimes_victims_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_crimes_victims_organization_id_index ON public."OADH_INT_Crimes_Victims" USING btree (organization_id);


--
-- TOC entry 4041 (class 1259 OID 26475)
-- Name: oadh_int_crimes_victims_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_crimes_victims_temp_organization_id_index ON public."OADH_INT_Crimes_Victims_Temp" USING btree (organization_id);


--
-- TOC entry 4110 (class 1259 OID 26830)
-- Name: oadh_int_gender_sexual_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_gender_sexual_organization_id_index ON public."OADH_INT_Gender_Sexual" USING btree (organization_id);


--
-- TOC entry 4107 (class 1259 OID 26816)
-- Name: oadh_int_gender_sexual_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_gender_sexual_temp_organization_id_index ON public."OADH_INT_Gender_Sexual_Temp" USING btree (organization_id);


--
-- TOC entry 4062 (class 1259 OID 26588)
-- Name: oadh_int_gender_trafficking_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_gender_trafficking_organization_id_index ON public."OADH_INT_Gender_Trafficking" USING btree (organization_id);


--
-- TOC entry 4059 (class 1259 OID 26574)
-- Name: oadh_int_gender_trafficking_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_gender_trafficking_temp_organization_id_index ON public."OADH_INT_Gender_Trafficking_Temp" USING btree (organization_id);


--
-- TOC entry 4146 (class 1259 OID 27028)
-- Name: oadh_int_integrity_violation_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_integrity_violation_organization_id_index ON public."OADH_INT_Integrity_Violation" USING btree (organization_id);


--
-- TOC entry 4143 (class 1259 OID 27011)
-- Name: oadh_int_integrity_violation_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_integrity_violation_temp_organization_id_index ON public."OADH_INT_Integrity_Violation_Temp" USING btree (organization_id);


--
-- TOC entry 4080 (class 1259 OID 26678)
-- Name: oadh_int_movement_freedom_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_movement_freedom_organization_id_index ON public."OADH_INT_Movement_Freedom" USING btree (organization_id);


--
-- TOC entry 4077 (class 1259 OID 26661)
-- Name: oadh_int_movement_freedom_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_movement_freedom_temp_organization_id_index ON public."OADH_INT_Movement_Freedom_Temp" USING btree (organization_id);


--
-- TOC entry 4134 (class 1259 OID 26960)
-- Name: oadh_int_schedule_injuries_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_schedule_injuries_organization_id_index ON public."OADH_INT_Schedule_Injuries" USING btree (organization_id);


--
-- TOC entry 4131 (class 1259 OID 26943)
-- Name: oadh_int_schedule_injuries_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_schedule_injuries_temp_organization_id_index ON public."OADH_INT_Schedule_Injuries_Temp" USING btree (organization_id);


--
-- TOC entry 4122 (class 1259 OID 26898)
-- Name: oadh_int_state_injuries_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_state_injuries_organization_id_index ON public."OADH_INT_State_Injuries" USING btree (organization_id);


--
-- TOC entry 4119 (class 1259 OID 26881)
-- Name: oadh_int_state_injuries_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_state_injuries_temp_organization_id_index ON public."OADH_INT_State_Injuries_Temp" USING btree (organization_id);


--
-- TOC entry 4098 (class 1259 OID 26768)
-- Name: oadh_int_state_women_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_state_women_organization_id_index ON public."OADH_INT_State_Women" USING btree (organization_id);


--
-- TOC entry 4095 (class 1259 OID 26751)
-- Name: oadh_int_state_women_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_state_women_temp_organization_id_index ON public."OADH_INT_State_Women_Temp" USING btree (organization_id);


--
-- TOC entry 4104 (class 1259 OID 26802)
-- Name: oadh_int_type_sexual_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_type_sexual_organization_id_index ON public."OADH_INT_Type_Sexual" USING btree (organization_id);


--
-- TOC entry 4101 (class 1259 OID 26785)
-- Name: oadh_int_type_sexual_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_type_sexual_temp_organization_id_index ON public."OADH_INT_Type_Sexual_Temp" USING btree (organization_id);


--
-- TOC entry 4068 (class 1259 OID 26616)
-- Name: oadh_int_type_trafficking_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_type_trafficking_organization_id_index ON public."OADH_INT_Type_Trafficking" USING btree (organization_id);


--
-- TOC entry 4065 (class 1259 OID 26602)
-- Name: oadh_int_type_trafficking_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_type_trafficking_temp_organization_id_index ON public."OADH_INT_Type_Trafficking_Temp" USING btree (organization_id);


--
-- TOC entry 4092 (class 1259 OID 26734)
-- Name: oadh_int_type_women_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_type_women_organization_id_index ON public."OADH_INT_Type_Women" USING btree (organization_id);


--
-- TOC entry 4089 (class 1259 OID 26720)
-- Name: oadh_int_type_women_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_type_women_temp_organization_id_index ON public."OADH_INT_Type_Women_Temp" USING btree (organization_id);


--
-- TOC entry 4056 (class 1259 OID 26560)
-- Name: oadh_int_working_officials_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_working_officials_organization_id_index ON public."OADH_INT_Working_Officials" USING btree (organization_id);


--
-- TOC entry 4053 (class 1259 OID 26543)
-- Name: oadh_int_working_officials_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_int_working_officials_temp_organization_id_index ON public."OADH_INT_Working_Officials_Temp" USING btree (organization_id);


--
-- TOC entry 3948 (class 1259 OID 17770)
-- Name: oadh_news_human_rights_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_news_human_rights_organization_id_index ON public."OADH_News_Human_Rights" USING btree (organization_id);


--
-- TOC entry 3954 (class 1259 OID 25997)
-- Name: oadh_news_news_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_news_news_organization_id_index ON public."OADH_News_News" USING btree (organization_id);


--
-- TOC entry 3933 (class 1259 OID 17671)
-- Name: oadh_news_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_news_organization_id_index ON public."OADH_News" USING btree (organization_id);


--
-- TOC entry 3951 (class 1259 OID 17784)
-- Name: oadh_people_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_people_organization_id_index ON public."OADH_People" USING btree (organization_id);


--
-- TOC entry 4152 (class 1259 OID 34198)
-- Name: oadh_pop_age_population_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_pop_age_population_organization_id_index ON public."OADH_POP_Age_Population" USING btree (organization_id);


--
-- TOC entry 4149 (class 1259 OID 34184)
-- Name: oadh_pop_age_population_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_pop_age_population_temp_organization_id_index ON public."OADH_POP_Age_Population_Temp" USING btree (organization_id);


--
-- TOC entry 4158 (class 1259 OID 34232)
-- Name: oadh_pop_territory_population_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_pop_territory_population_organization_id_index ON public."OADH_POP_Territory_Population" USING btree (organization_id);


--
-- TOC entry 4155 (class 1259 OID 34215)
-- Name: oadh_pop_territory_population_temp_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_pop_territory_population_temp_organization_id_index ON public."OADH_POP_Territory_Population_Temp" USING btree (organization_id);


--
-- TOC entry 3939 (class 1259 OID 17705)
-- Name: oadh_producedure_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_producedure_organization_id_index ON public."OADH_Producedure" USING btree (organization_id);


--
-- TOC entry 3927 (class 1259 OID 17635)
-- Name: oadh_settings_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_settings_organization_id_index ON public."OADH_Settings" USING btree (organization_id);


--
-- TOC entry 3936 (class 1259 OID 17688)
-- Name: oadh_source_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oadh_source_organization_id_index ON public."OADH_Source" USING btree (organization_id);


--
-- TOC entry 3842 (class 1259 OID 16641)
-- Name: org_organization_cost_price_precision_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX org_organization_cost_price_precision_index ON public."ORG_Organization" USING btree (cost_price_precision);


--
-- TOC entry 3843 (class 1259 OID 16642)
-- Name: org_organization_discount_precision_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX org_organization_discount_precision_index ON public."ORG_Organization" USING btree (discount_precision);


--
-- TOC entry 3844 (class 1259 OID 16643)
-- Name: org_organization_sale_point_quantity_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX org_organization_sale_point_quantity_index ON public."ORG_Organization" USING btree (sale_point_quantity);


--
-- TOC entry 3849 (class 1259 OID 16644)
-- Name: sec_file_file_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_file_file_id_index ON public."SEC_File" USING btree (file_id);


--
-- TOC entry 3850 (class 1259 OID 16645)
-- Name: sec_file_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_file_organization_id_index ON public."SEC_File" USING btree (organization_id);


--
-- TOC entry 3853 (class 1259 OID 16646)
-- Name: sec_journal_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_created_at_index ON public."SEC_Journal" USING btree (created_at);


--
-- TOC entry 3859 (class 1259 OID 16647)
-- Name: sec_journal_detail_journal_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_detail_journal_id_index ON public."SEC_Journal_Detail" USING btree (journal_id);


--
-- TOC entry 3854 (class 1259 OID 16648)
-- Name: sec_journal_journalized_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_journalized_id_index ON public."SEC_Journal" USING btree (journalized_id);


--
-- TOC entry 3855 (class 1259 OID 16649)
-- Name: sec_journal_journalized_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_journalized_type_index ON public."SEC_Journal" USING btree (journalized_type);


--
-- TOC entry 3856 (class 1259 OID 16650)
-- Name: sec_journal_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_journal_user_id_index ON public."SEC_Journal" USING btree (user_id);


--
-- TOC entry 3862 (class 1259 OID 16651)
-- Name: sec_menu_module_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_menu_module_id_index ON public."SEC_Menu" USING btree (module_id);


--
-- TOC entry 3863 (class 1259 OID 16652)
-- Name: sec_menu_parent_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_menu_parent_id_index ON public."SEC_Menu" USING btree (parent_id);


--
-- TOC entry 3864 (class 1259 OID 17787)
-- Name: sec_menu_url_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_menu_url_index ON public."SEC_Menu" USING btree (url);


--
-- TOC entry 3867 (class 1259 OID 16654)
-- Name: sec_password_reminders_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_password_reminders_email_index ON public."SEC_Password_Reminders" USING btree (email);


--
-- TOC entry 3868 (class 1259 OID 16655)
-- Name: sec_password_reminders_token_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_password_reminders_token_index ON public."SEC_Password_Reminders" USING btree (token);


--
-- TOC entry 3876 (class 1259 OID 16656)
-- Name: sec_role_menu_menu_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_menu_menu_id_index ON public."SEC_Role_Menu" USING btree (menu_id);


--
-- TOC entry 3877 (class 1259 OID 16657)
-- Name: sec_role_menu_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_menu_role_id_index ON public."SEC_Role_Menu" USING btree (role_id);


--
-- TOC entry 3873 (class 1259 OID 16658)
-- Name: sec_role_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_organization_id_index ON public."SEC_Role" USING btree (organization_id);


--
-- TOC entry 3880 (class 1259 OID 16659)
-- Name: sec_role_permission_permission_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_permission_permission_id_index ON public."SEC_Role_Permission" USING btree (permission_id);


--
-- TOC entry 3881 (class 1259 OID 16660)
-- Name: sec_role_permission_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_role_permission_role_id_index ON public."SEC_Role_Permission" USING btree (role_id);


--
-- TOC entry 3884 (class 1259 OID 16661)
-- Name: sec_user_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_email_index ON public."SEC_User" USING btree (email);


--
-- TOC entry 3885 (class 1259 OID 16662)
-- Name: sec_user_is_active_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_is_active_index ON public."SEC_User" USING btree (is_active);


--
-- TOC entry 3888 (class 1259 OID 16663)
-- Name: sec_user_menu_is_assigned_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_menu_is_assigned_index ON public."SEC_User_Menu" USING btree (is_assigned);


--
-- TOC entry 3889 (class 1259 OID 16664)
-- Name: sec_user_menu_menu_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_menu_menu_id_index ON public."SEC_User_Menu" USING btree (menu_id);


--
-- TOC entry 3890 (class 1259 OID 16665)
-- Name: sec_user_menu_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_menu_organization_id_index ON public."SEC_User_Menu" USING btree (organization_id);


--
-- TOC entry 3891 (class 1259 OID 16666)
-- Name: sec_user_menu_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_menu_user_id_index ON public."SEC_User_Menu" USING btree (user_id);


--
-- TOC entry 3894 (class 1259 OID 16667)
-- Name: sec_user_organization_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_organization_organization_id_index ON public."SEC_User_Organization" USING btree (organization_id);


--
-- TOC entry 3895 (class 1259 OID 16668)
-- Name: sec_user_organization_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_organization_user_id_index ON public."SEC_User_Organization" USING btree (user_id);


--
-- TOC entry 3898 (class 1259 OID 16669)
-- Name: sec_user_permission_is_assigned_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_permission_is_assigned_index ON public."SEC_User_Permission" USING btree (is_assigned);


--
-- TOC entry 3899 (class 1259 OID 16670)
-- Name: sec_user_permission_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_permission_organization_id_index ON public."SEC_User_Permission" USING btree (organization_id);


--
-- TOC entry 3900 (class 1259 OID 16671)
-- Name: sec_user_permission_permission_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_permission_permission_id_index ON public."SEC_User_Permission" USING btree (permission_id);


--
-- TOC entry 3901 (class 1259 OID 16672)
-- Name: sec_user_permission_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_permission_user_id_index ON public."SEC_User_Permission" USING btree (user_id);


--
-- TOC entry 3904 (class 1259 OID 16673)
-- Name: sec_user_role_organization_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_role_organization_id_index ON public."SEC_User_Role" USING btree (organization_id);


--
-- TOC entry 3905 (class 1259 OID 16674)
-- Name: sec_user_role_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_role_role_id_index ON public."SEC_User_Role" USING btree (role_id);


--
-- TOC entry 3906 (class 1259 OID 16675)
-- Name: sec_user_role_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sec_user_role_user_id_index ON public."SEC_User_Role" USING btree (user_id);


--
-- TOC entry 4159 (class 2606 OID 16676)
-- Name: FILE_File file_file_parent_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."FILE_File"
    ADD CONSTRAINT file_file_parent_file_id_foreign FOREIGN KEY (parent_file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4206 (class 2606 OID 17714)
-- Name: OADH_Context oadh_context_news_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Context"
    ADD CONSTRAINT oadh_context_news_id_foreign FOREIGN KEY (news_id) REFERENCES public."OADH_News"(id);


--
-- TOC entry 4201 (class 2606 OID 17645)
-- Name: OADH_Digital_Media oadh_digital_media_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Digital_Media"
    ADD CONSTRAINT oadh_digital_media_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public."OADH_Digital_Media"(id);


--
-- TOC entry 4231 (class 2606 OID 26252)
-- Name: OADH_FREE_Accused_Liberty_Deprivation oadh_free_accused_liberty_deprivation_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Accused_Liberty_Deprivation"
    ADD CONSTRAINT oadh_free_accused_liberty_deprivation_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4230 (class 2606 OID 26235)
-- Name: OADH_FREE_Accused_Liberty_Deprivation_Temp oadh_free_accused_liberty_deprivation_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Accused_Liberty_Deprivation_Temp"
    ADD CONSTRAINT oadh_free_accused_liberty_deprivation_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4239 (class 2606 OID 26385)
-- Name: OADH_FREE_Acute_Diseases oadh_free_acute_diseases_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Acute_Diseases"
    ADD CONSTRAINT oadh_free_acute_diseases_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4238 (class 2606 OID 26371)
-- Name: OADH_FREE_Acute_Diseases_Temp oadh_free_acute_diseases_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Acute_Diseases_Temp"
    ADD CONSTRAINT oadh_free_acute_diseases_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4227 (class 2606 OID 26184)
-- Name: OADH_FREE_Capture_Orders oadh_free_capture_orders_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Capture_Orders"
    ADD CONSTRAINT oadh_free_capture_orders_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4226 (class 2606 OID 26167)
-- Name: OADH_FREE_Capture_Orders_Temp oadh_free_capture_orders_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Capture_Orders_Temp"
    ADD CONSTRAINT oadh_free_capture_orders_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4241 (class 2606 OID 26419)
-- Name: OADH_FREE_Chronic_Diseases oadh_free_chronic_diseases_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Chronic_Diseases"
    ADD CONSTRAINT oadh_free_chronic_diseases_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4240 (class 2606 OID 26402)
-- Name: OADH_FREE_Chronic_Diseases_Temp oadh_free_chronic_diseases_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Chronic_Diseases_Temp"
    ADD CONSTRAINT oadh_free_chronic_diseases_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4221 (class 2606 OID 26088)
-- Name: OADH_FREE_Criminal_Cases_Trials oadh_free_criminal_cases_trials_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Criminal_Cases_Trials"
    ADD CONSTRAINT oadh_free_criminal_cases_trials_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4220 (class 2606 OID 26071)
-- Name: OADH_FREE_Criminal_Cases_Trials_Temp oadh_free_criminal_cases_trials_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Criminal_Cases_Trials_Temp"
    ADD CONSTRAINT oadh_free_criminal_cases_trials_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4237 (class 2606 OID 26354)
-- Name: OADH_FREE_Deprived_Persons oadh_free_deprived_persons_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Deprived_Persons"
    ADD CONSTRAINT oadh_free_deprived_persons_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4236 (class 2606 OID 26337)
-- Name: OADH_FREE_Deprived_Persons_Temp oadh_free_deprived_persons_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Deprived_Persons_Temp"
    ADD CONSTRAINT oadh_free_deprived_persons_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4229 (class 2606 OID 26218)
-- Name: OADH_FREE_Disappearances_Victims oadh_free_disappearances_victims_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Disappearances_Victims"
    ADD CONSTRAINT oadh_free_disappearances_victims_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4228 (class 2606 OID 26201)
-- Name: OADH_FREE_Disappearances_Victims_Temp oadh_free_disappearances_victims_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Disappearances_Victims_Temp"
    ADD CONSTRAINT oadh_free_disappearances_victims_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4234 (class 2606 OID 26303)
-- Name: OADH_FREE_Forced_Disappearance_Temps oadh_free_forced_disappearance_temps_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearance_Temps"
    ADD CONSTRAINT oadh_free_forced_disappearance_temps_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4235 (class 2606 OID 26320)
-- Name: OADH_FREE_Forced_Disappearances oadh_free_forced_disappearances_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Forced_Disappearances"
    ADD CONSTRAINT oadh_free_forced_disappearances_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4217 (class 2606 OID 26020)
-- Name: OADH_FREE_Habeas_Corpus_Request oadh_free_habeas_corpus_request_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Habeas_Corpus_Request"
    ADD CONSTRAINT oadh_free_habeas_corpus_request_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4216 (class 2606 OID 26006)
-- Name: OADH_FREE_Habeas_Corpus_Request_Temp oadh_free_habeas_corpus_request_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Habeas_Corpus_Request_Temp"
    ADD CONSTRAINT oadh_free_habeas_corpus_request_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4243 (class 2606 OID 26453)
-- Name: OADH_FREE_Jails_Occupation oadh_free_jails_occupation_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Jails_Occupation"
    ADD CONSTRAINT oadh_free_jails_occupation_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4242 (class 2606 OID 26436)
-- Name: OADH_FREE_Jails_Occupation_Temp oadh_free_jails_occupation_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Jails_Occupation_Temp"
    ADD CONSTRAINT oadh_free_jails_occupation_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4223 (class 2606 OID 26116)
-- Name: OADH_FREE_Mass_Trials oadh_free_mass_trials_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Mass_Trials"
    ADD CONSTRAINT oadh_free_mass_trials_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4222 (class 2606 OID 26102)
-- Name: OADH_FREE_Mass_Trials_Temp oadh_free_mass_trials_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Mass_Trials_Temp"
    ADD CONSTRAINT oadh_free_mass_trials_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4219 (class 2606 OID 26054)
-- Name: OADH_FREE_People_Detained oadh_free_people_detained_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_People_Detained"
    ADD CONSTRAINT oadh_free_people_detained_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4218 (class 2606 OID 26037)
-- Name: OADH_FREE_People_Detained_Temp oadh_free_people_detained_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_People_Detained_Temp"
    ADD CONSTRAINT oadh_free_people_detained_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4225 (class 2606 OID 26150)
-- Name: OADH_FREE_Procedural_Fraud oadh_free_procedural_fraud_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Procedural_Fraud"
    ADD CONSTRAINT oadh_free_procedural_fraud_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4224 (class 2606 OID 26133)
-- Name: OADH_FREE_Procedural_Fraud_Temp oadh_free_procedural_fraud_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Procedural_Fraud_Temp"
    ADD CONSTRAINT oadh_free_procedural_fraud_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4233 (class 2606 OID 26286)
-- Name: OADH_FREE_Victims_Liberty_Deprivation oadh_free_victims_liberty_deprivation_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Victims_Liberty_Deprivation"
    ADD CONSTRAINT oadh_free_victims_liberty_deprivation_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4232 (class 2606 OID 26269)
-- Name: OADH_FREE_Victims_Liberty_Deprivation_Temp oadh_free_victims_liberty_deprivation_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_FREE_Victims_Liberty_Deprivation_Temp"
    ADD CONSTRAINT oadh_free_victims_liberty_deprivation_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4207 (class 2606 OID 17732)
-- Name: OADH_Human_Right oadh_human_right_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Human_Right"
    ADD CONSTRAINT oadh_human_right_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public."OADH_Human_Right"(id);


--
-- TOC entry 4273 (class 2606 OID 26921)
-- Name: OADH_INT_Age_Injuries oadh_int_age_injuries_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Injuries"
    ADD CONSTRAINT oadh_int_age_injuries_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4272 (class 2606 OID 26907)
-- Name: OADH_INT_Age_Injuries_Temp oadh_int_age_injuries_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Injuries_Temp"
    ADD CONSTRAINT oadh_int_age_injuries_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4269 (class 2606 OID 26859)
-- Name: OADH_INT_Age_Sexual oadh_int_age_sexual_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Sexual"
    ADD CONSTRAINT oadh_int_age_sexual_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4268 (class 2606 OID 26842)
-- Name: OADH_INT_Age_Sexual_Temp oadh_int_age_sexual_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Sexual_Temp"
    ADD CONSTRAINT oadh_int_age_sexual_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4255 (class 2606 OID 26639)
-- Name: OADH_INT_Age_Trafficking oadh_int_age_trafficking_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Trafficking"
    ADD CONSTRAINT oadh_int_age_trafficking_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4254 (class 2606 OID 26625)
-- Name: OADH_INT_Age_Trafficking_Temp oadh_int_age_trafficking_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Trafficking_Temp"
    ADD CONSTRAINT oadh_int_age_trafficking_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4259 (class 2606 OID 26701)
-- Name: OADH_INT_Age_Women oadh_int_age_women_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Women"
    ADD CONSTRAINT oadh_int_age_women_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4258 (class 2606 OID 26687)
-- Name: OADH_INT_Age_Women_Temp oadh_int_age_women_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Age_Women_Temp"
    ADD CONSTRAINT oadh_int_age_women_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4277 (class 2606 OID 26989)
-- Name: OADH_INT_Agent_Investigation oadh_int_agent_investigation_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Agent_Investigation"
    ADD CONSTRAINT oadh_int_agent_investigation_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4276 (class 2606 OID 26972)
-- Name: OADH_INT_Agent_Investigation_Temp oadh_int_agent_investigation_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Agent_Investigation_Temp"
    ADD CONSTRAINT oadh_int_agent_investigation_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4247 (class 2606 OID 26521)
-- Name: OADH_INT_Crimes_Accused oadh_int_crimes_accused_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Accused"
    ADD CONSTRAINT oadh_int_crimes_accused_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4246 (class 2606 OID 26504)
-- Name: OADH_INT_Crimes_Accused_Temp oadh_int_crimes_accused_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Accused_Temp"
    ADD CONSTRAINT oadh_int_crimes_accused_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4245 (class 2606 OID 26487)
-- Name: OADH_INT_Crimes_Victims oadh_int_crimes_victims_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Victims"
    ADD CONSTRAINT oadh_int_crimes_victims_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4244 (class 2606 OID 26470)
-- Name: OADH_INT_Crimes_Victims_Temp oadh_int_crimes_victims_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Crimes_Victims_Temp"
    ADD CONSTRAINT oadh_int_crimes_victims_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4267 (class 2606 OID 26825)
-- Name: OADH_INT_Gender_Sexual oadh_int_gender_sexual_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Sexual"
    ADD CONSTRAINT oadh_int_gender_sexual_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4266 (class 2606 OID 26811)
-- Name: OADH_INT_Gender_Sexual_Temp oadh_int_gender_sexual_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Sexual_Temp"
    ADD CONSTRAINT oadh_int_gender_sexual_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4251 (class 2606 OID 26583)
-- Name: OADH_INT_Gender_Trafficking oadh_int_gender_trafficking_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Trafficking"
    ADD CONSTRAINT oadh_int_gender_trafficking_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4250 (class 2606 OID 26569)
-- Name: OADH_INT_Gender_Trafficking_Temp oadh_int_gender_trafficking_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Gender_Trafficking_Temp"
    ADD CONSTRAINT oadh_int_gender_trafficking_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4279 (class 2606 OID 27023)
-- Name: OADH_INT_Integrity_Violation oadh_int_integrity_violation_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Integrity_Violation"
    ADD CONSTRAINT oadh_int_integrity_violation_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4278 (class 2606 OID 27006)
-- Name: OADH_INT_Integrity_Violation_Temp oadh_int_integrity_violation_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Integrity_Violation_Temp"
    ADD CONSTRAINT oadh_int_integrity_violation_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4257 (class 2606 OID 26673)
-- Name: OADH_INT_Movement_Freedom oadh_int_movement_freedom_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Movement_Freedom"
    ADD CONSTRAINT oadh_int_movement_freedom_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4256 (class 2606 OID 26656)
-- Name: OADH_INT_Movement_Freedom_Temp oadh_int_movement_freedom_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Movement_Freedom_Temp"
    ADD CONSTRAINT oadh_int_movement_freedom_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4275 (class 2606 OID 26955)
-- Name: OADH_INT_Schedule_Injuries oadh_int_schedule_injuries_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Schedule_Injuries"
    ADD CONSTRAINT oadh_int_schedule_injuries_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4274 (class 2606 OID 26938)
-- Name: OADH_INT_Schedule_Injuries_Temp oadh_int_schedule_injuries_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Schedule_Injuries_Temp"
    ADD CONSTRAINT oadh_int_schedule_injuries_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4271 (class 2606 OID 26893)
-- Name: OADH_INT_State_Injuries oadh_int_state_injuries_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Injuries"
    ADD CONSTRAINT oadh_int_state_injuries_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4270 (class 2606 OID 26876)
-- Name: OADH_INT_State_Injuries_Temp oadh_int_state_injuries_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Injuries_Temp"
    ADD CONSTRAINT oadh_int_state_injuries_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4263 (class 2606 OID 26763)
-- Name: OADH_INT_State_Women oadh_int_state_women_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Women"
    ADD CONSTRAINT oadh_int_state_women_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4262 (class 2606 OID 26746)
-- Name: OADH_INT_State_Women_Temp oadh_int_state_women_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_State_Women_Temp"
    ADD CONSTRAINT oadh_int_state_women_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4265 (class 2606 OID 26797)
-- Name: OADH_INT_Type_Sexual oadh_int_type_sexual_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Sexual"
    ADD CONSTRAINT oadh_int_type_sexual_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4264 (class 2606 OID 26780)
-- Name: OADH_INT_Type_Sexual_Temp oadh_int_type_sexual_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Sexual_Temp"
    ADD CONSTRAINT oadh_int_type_sexual_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4253 (class 2606 OID 26611)
-- Name: OADH_INT_Type_Trafficking oadh_int_type_trafficking_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Trafficking"
    ADD CONSTRAINT oadh_int_type_trafficking_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4252 (class 2606 OID 26597)
-- Name: OADH_INT_Type_Trafficking_Temp oadh_int_type_trafficking_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Trafficking_Temp"
    ADD CONSTRAINT oadh_int_type_trafficking_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4261 (class 2606 OID 26729)
-- Name: OADH_INT_Type_Women oadh_int_type_women_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Women"
    ADD CONSTRAINT oadh_int_type_women_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4260 (class 2606 OID 26715)
-- Name: OADH_INT_Type_Women_Temp oadh_int_type_women_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Type_Women_Temp"
    ADD CONSTRAINT oadh_int_type_women_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4249 (class 2606 OID 26555)
-- Name: OADH_INT_Working_Officials oadh_int_working_officials_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Working_Officials"
    ADD CONSTRAINT oadh_int_working_officials_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4248 (class 2606 OID 26538)
-- Name: OADH_INT_Working_Officials_Temp oadh_int_working_officials_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_INT_Working_Officials_Temp"
    ADD CONSTRAINT oadh_int_working_officials_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4202 (class 2606 OID 17661)
-- Name: OADH_News oadh_news_digital_media_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News"
    ADD CONSTRAINT oadh_news_digital_media_id_foreign FOREIGN KEY (digital_media_id) REFERENCES public."OADH_Digital_Media"(id);


--
-- TOC entry 4212 (class 2606 OID 17765)
-- Name: OADH_News_Human_Rights oadh_news_human_rights_news_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_Human_Rights"
    ADD CONSTRAINT oadh_news_human_rights_news_id_foreign FOREIGN KEY (news_id) REFERENCES public."OADH_News"(id);


--
-- TOC entry 4209 (class 2606 OID 17750)
-- Name: OADH_News_Human_Rights oadh_news_human_rights_right_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_Human_Rights"
    ADD CONSTRAINT oadh_news_human_rights_right_id_foreign FOREIGN KEY (right_id) REFERENCES public."OADH_Human_Right"(id);


--
-- TOC entry 4210 (class 2606 OID 17755)
-- Name: OADH_News_Human_Rights oadh_news_human_rights_topic_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_Human_Rights"
    ADD CONSTRAINT oadh_news_human_rights_topic_id_foreign FOREIGN KEY (topic_id) REFERENCES public."OADH_Human_Right"(id);


--
-- TOC entry 4208 (class 2606 OID 17745)
-- Name: OADH_News_Human_Rights oadh_news_human_rights_tracing_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_Human_Rights"
    ADD CONSTRAINT oadh_news_human_rights_tracing_type_id_foreign FOREIGN KEY (tracing_type_id) REFERENCES public."OADH_Human_Right"(id);


--
-- TOC entry 4211 (class 2606 OID 17760)
-- Name: OADH_News_Human_Rights oadh_news_human_rights_violated_fact_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_Human_Rights"
    ADD CONSTRAINT oadh_news_human_rights_violated_fact_id_foreign FOREIGN KEY (violated_fact_id) REFERENCES public."OADH_Human_Right"(id);


--
-- TOC entry 4215 (class 2606 OID 25992)
-- Name: OADH_News_News oadh_news_news_news_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_News"
    ADD CONSTRAINT oadh_news_news_news_id_foreign FOREIGN KEY (news_id) REFERENCES public."OADH_News"(id);


--
-- TOC entry 4214 (class 2606 OID 25987)
-- Name: OADH_News_News oadh_news_news_related_news_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News_News"
    ADD CONSTRAINT oadh_news_news_related_news_id_foreign FOREIGN KEY (related_news_id) REFERENCES public."OADH_News"(id);


--
-- TOC entry 4203 (class 2606 OID 17666)
-- Name: OADH_News oadh_news_section_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_News"
    ADD CONSTRAINT oadh_news_section_id_foreign FOREIGN KEY (section_id) REFERENCES public."OADH_Digital_Media"(id);


--
-- TOC entry 4213 (class 2606 OID 17779)
-- Name: OADH_People oadh_people_news_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_People"
    ADD CONSTRAINT oadh_people_news_id_foreign FOREIGN KEY (news_id) REFERENCES public."OADH_News"(id);


--
-- TOC entry 4281 (class 2606 OID 34193)
-- Name: OADH_POP_Age_Population oadh_pop_age_population_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Age_Population"
    ADD CONSTRAINT oadh_pop_age_population_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4280 (class 2606 OID 34179)
-- Name: OADH_POP_Age_Population_Temp oadh_pop_age_population_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Age_Population_Temp"
    ADD CONSTRAINT oadh_pop_age_population_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4283 (class 2606 OID 34227)
-- Name: OADH_POP_Territory_Population oadh_pop_territory_population_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Territory_Population"
    ADD CONSTRAINT oadh_pop_territory_population_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4282 (class 2606 OID 34210)
-- Name: OADH_POP_Territory_Population_Temp oadh_pop_territory_population_temp_file_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_POP_Territory_Population_Temp"
    ADD CONSTRAINT oadh_pop_territory_population_temp_file_id_foreign FOREIGN KEY (file_id) REFERENCES public."FILE_File"(id);


--
-- TOC entry 4205 (class 2606 OID 17700)
-- Name: OADH_Producedure oadh_producedure_news_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Producedure"
    ADD CONSTRAINT oadh_producedure_news_id_foreign FOREIGN KEY (news_id) REFERENCES public."OADH_News"(id);


--
-- TOC entry 4204 (class 2606 OID 17683)
-- Name: OADH_Source oadh_source_news_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."OADH_Source"
    ADD CONSTRAINT oadh_source_news_id_foreign FOREIGN KEY (news_id) REFERENCES public."OADH_News"(id);


--
-- TOC entry 4160 (class 2606 OID 16681)
-- Name: ORG_Organization org_organization_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization"
    ADD CONSTRAINT org_organization_country_id_foreign FOREIGN KEY (country_id) REFERENCES public."SYS_Country"(id);


--
-- TOC entry 4161 (class 2606 OID 16686)
-- Name: ORG_Organization org_organization_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization"
    ADD CONSTRAINT org_organization_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4162 (class 2606 OID 16691)
-- Name: ORG_Organization org_organization_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."ORG_Organization"
    ADD CONSTRAINT org_organization_currency_id_foreign FOREIGN KEY (currency_id) REFERENCES public."SYS_Currency"(id);


--
-- TOC entry 4165 (class 2606 OID 16696)
-- Name: SEC_Journal_Detail sec_journal_detail_journal_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal_Detail"
    ADD CONSTRAINT sec_journal_detail_journal_id_foreign FOREIGN KEY (journal_id) REFERENCES public."SEC_Journal"(id);


--
-- TOC entry 4163 (class 2606 OID 16701)
-- Name: SEC_Journal sec_journal_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal"
    ADD CONSTRAINT sec_journal_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 4164 (class 2606 OID 16706)
-- Name: SEC_Journal sec_journal_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Journal"
    ADD CONSTRAINT sec_journal_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4166 (class 2606 OID 16711)
-- Name: SEC_Menu sec_menu_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu"
    ADD CONSTRAINT sec_menu_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4167 (class 2606 OID 16716)
-- Name: SEC_Menu sec_menu_module_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu"
    ADD CONSTRAINT sec_menu_module_id_foreign FOREIGN KEY (module_id) REFERENCES public."SEC_Module"(id);


--
-- TOC entry 4168 (class 2606 OID 16721)
-- Name: SEC_Menu sec_menu_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Menu"
    ADD CONSTRAINT sec_menu_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public."SEC_Menu"(id);


--
-- TOC entry 4169 (class 2606 OID 16726)
-- Name: SEC_Module sec_module_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Module"
    ADD CONSTRAINT sec_module_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4170 (class 2606 OID 16731)
-- Name: SEC_Permission sec_permission_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Permission"
    ADD CONSTRAINT sec_permission_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4171 (class 2606 OID 16736)
-- Name: SEC_Permission sec_permission_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Permission"
    ADD CONSTRAINT sec_permission_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public."SEC_Menu"(id);


--
-- TOC entry 4172 (class 2606 OID 16741)
-- Name: SEC_Role sec_role_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role"
    ADD CONSTRAINT sec_role_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4174 (class 2606 OID 16746)
-- Name: SEC_Role_Menu sec_role_menu_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu"
    ADD CONSTRAINT sec_role_menu_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4175 (class 2606 OID 16751)
-- Name: SEC_Role_Menu sec_role_menu_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu"
    ADD CONSTRAINT sec_role_menu_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public."SEC_Menu"(id);


--
-- TOC entry 4176 (class 2606 OID 16756)
-- Name: SEC_Role_Menu sec_role_menu_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Menu"
    ADD CONSTRAINT sec_role_menu_role_id_foreign FOREIGN KEY (role_id) REFERENCES public."SEC_Role"(id);


--
-- TOC entry 4173 (class 2606 OID 16761)
-- Name: SEC_Role sec_role_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role"
    ADD CONSTRAINT sec_role_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 4177 (class 2606 OID 16766)
-- Name: SEC_Role_Permission sec_role_permission_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission"
    ADD CONSTRAINT sec_role_permission_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4178 (class 2606 OID 16771)
-- Name: SEC_Role_Permission sec_role_permission_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission"
    ADD CONSTRAINT sec_role_permission_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public."SEC_Permission"(id);


--
-- TOC entry 4179 (class 2606 OID 16776)
-- Name: SEC_Role_Permission sec_role_permission_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_Role_Permission"
    ADD CONSTRAINT sec_role_permission_role_id_foreign FOREIGN KEY (role_id) REFERENCES public."SEC_Role"(id);


--
-- TOC entry 4180 (class 2606 OID 16781)
-- Name: SEC_User sec_user_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User"
    ADD CONSTRAINT sec_user_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4181 (class 2606 OID 16786)
-- Name: SEC_User sec_user_default_organization_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User"
    ADD CONSTRAINT sec_user_default_organization_foreign FOREIGN KEY (default_organization) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 4182 (class 2606 OID 16791)
-- Name: SEC_User_Menu sec_user_menu_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT sec_user_menu_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4183 (class 2606 OID 16796)
-- Name: SEC_User_Menu sec_user_menu_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT sec_user_menu_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public."SEC_Menu"(id);


--
-- TOC entry 4184 (class 2606 OID 16801)
-- Name: SEC_User_Menu sec_user_menu_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT sec_user_menu_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 4185 (class 2606 OID 16806)
-- Name: SEC_User_Menu sec_user_menu_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Menu"
    ADD CONSTRAINT sec_user_menu_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4186 (class 2606 OID 16811)
-- Name: SEC_User_Organization sec_user_organization_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization"
    ADD CONSTRAINT sec_user_organization_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4187 (class 2606 OID 16816)
-- Name: SEC_User_Organization sec_user_organization_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization"
    ADD CONSTRAINT sec_user_organization_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 4188 (class 2606 OID 16821)
-- Name: SEC_User_Organization sec_user_organization_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Organization"
    ADD CONSTRAINT sec_user_organization_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4189 (class 2606 OID 16826)
-- Name: SEC_User_Permission sec_user_permission_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT sec_user_permission_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4190 (class 2606 OID 16831)
-- Name: SEC_User_Permission sec_user_permission_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT sec_user_permission_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 4191 (class 2606 OID 16836)
-- Name: SEC_User_Permission sec_user_permission_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT sec_user_permission_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public."SEC_Permission"(id);


--
-- TOC entry 4192 (class 2606 OID 16841)
-- Name: SEC_User_Permission sec_user_permission_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Permission"
    ADD CONSTRAINT sec_user_permission_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4193 (class 2606 OID 16846)
-- Name: SEC_User_Role sec_user_role_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT sec_user_role_created_by_foreign FOREIGN KEY (created_by) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4194 (class 2606 OID 16851)
-- Name: SEC_User_Role sec_user_role_organization_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT sec_user_role_organization_id_foreign FOREIGN KEY (organization_id) REFERENCES public."ORG_Organization"(id);


--
-- TOC entry 4195 (class 2606 OID 16856)
-- Name: SEC_User_Role sec_user_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT sec_user_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public."SEC_Role"(id);


--
-- TOC entry 4196 (class 2606 OID 16861)
-- Name: SEC_User_Role sec_user_role_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SEC_User_Role"
    ADD CONSTRAINT sec_user_role_user_id_foreign FOREIGN KEY (user_id) REFERENCES public."SEC_User"(id);


--
-- TOC entry 4197 (class 2606 OID 16866)
-- Name: SYS_Account sys_account_account_chart_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account"
    ADD CONSTRAINT sys_account_account_chart_type_id_foreign FOREIGN KEY (account_chart_type_id) REFERENCES public."SYS_Account_Chart_Type"(id);


--
-- TOC entry 4198 (class 2606 OID 16871)
-- Name: SYS_Account_Chart_Type sys_account_chart_type_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Account_Chart_Type"
    ADD CONSTRAINT sys_account_chart_type_country_id_foreign FOREIGN KEY (country_id) REFERENCES public."SYS_Country"(id);


--
-- TOC entry 4199 (class 2606 OID 16876)
-- Name: SYS_Country sys_country_currency_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Country"
    ADD CONSTRAINT sys_country_currency_id_foreign FOREIGN KEY (currency_id) REFERENCES public."SYS_Currency"(id);


--
-- TOC entry 4200 (class 2606 OID 16881)
-- Name: SYS_Region sys_region_country_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SYS_Region"
    ADD CONSTRAINT sys_region_country_id_foreign FOREIGN KEY (country_id) REFERENCES public."SYS_Country"(id);


-- Completed on 2019-04-08 14:47:41 CST

--
-- PostgreSQL database dump complete
--

